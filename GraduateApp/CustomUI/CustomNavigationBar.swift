//
//  CustomNavigationBar.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 7/8/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import UIKit

class CustomNavigationBar: UIView {

    @IBOutlet weak var menuBtn: AnimatedButton!
    @IBOutlet weak var uniBtn: UIButton!
    @IBOutlet weak var createbtn: UIButton!
    @IBOutlet weak var searchBtn: UIButton!
     

    class func instanceFromNib() -> CustomNavigationBar {
        return UINib(nibName: "CustomNavigationBar", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! CustomNavigationBar
    }
    static func instantiate(message: String) -> CustomNavigationBar {
           let view: CustomNavigationBar = instanceFromNib()
           view.searchBtn.setImage(UIImage(named: "iconSearch"), for: .normal)
        view.searchBtn.imageView?.contentMode = .center
        view.menuBtn.setImage(UIImage(named: "iconMenu"), for: .normal)
        view.menuBtn.imageView?.contentMode = .center
           return view
           }
   
        
//        searchBtn.setImage(UIImage(named: "iconSearch"), for: .normal)
//        menuBtn.setImage(UIImage(named: "iconMenu"), for: .normal)
    
}
