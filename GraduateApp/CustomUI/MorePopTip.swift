//
//  MorePopTip.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 7/10/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import UIKit

class MorePopTip: UIView {

    @IBOutlet weak var containerView: CustomView!
    var view: UIView!
          
    @IBOutlet weak var topView: CustomView!
    
         override init(frame: CGRect) {
             super.init(frame: frame)
             setupViews()
           }
           required init?(coder aDecoder: NSCoder) {
             super.init(coder: aDecoder)
           
         setupViews()
           }
           
          
           func setupViews() {
            
             func commonInit() {
                 let bundle = Bundle.init(for: MorePopTip.self)
                 if let viewsToAdd = bundle.loadNibNamed("MorePopTip", owner: self, options: nil), let contentView = viewsToAdd.first as? UIView {
                     addSubview(contentView)
                     contentView.frame = self.bounds
                     contentView.autoresizingMask = [.flexibleHeight,
                                                     .flexibleWidth]
                    self.clipsToBounds = true
                    self.layer.masksToBounds = true
                 }
             }
}
}
