//
//  GraduateMenu.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 7/17/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import UIKit

class GraduateMenu: UIView {
    @IBOutlet weak var view: UIView!
 
    @IBOutlet weak var tableView: UITableView!
     let kCONTENT_XIB_NAME = "GraduateMenu"
    var universityDataSource: [CheckBox] = []
    override init(frame: CGRect) {
             super.init(frame: frame)
             setupViews()
           }
           required init?(coder aDecoder: NSCoder) {
             super.init(coder: aDecoder)
               
               
             
            addData()
         setupViews()
           }
    func addData() {
        universityDataSource = [CheckBox(title: "Univerisity 1", selected: false),CheckBox(title: "University 2", selected: false), CheckBox(title: "University 3", selected: false)]
    }
    func setupViews() {
        
//        view = loadViewFromNib()
//        view.frame = bounds
//        view.autoresizingMask = [.flexibleHeight,
//                                                         .flexibleWidth]
//               //view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
//               //view.translatesAutoresizingMaskIntoConstraints = true
//
//               addSubview(view)
        
        //MARK: TIMELIneCreate METHOD
        func commonInit() {
            let bundle = Bundle.init(for: GraduateMenu.self)
            if let viewsToAdd = bundle.loadNibNamed("GraduateMenu", owner: self, options: nil), let contentView = viewsToAdd.first as? UIView {
                addSubview(contentView)
                view.frame = self.bounds
                view.autoresizingMask = [.flexibleHeight,
                                                .flexibleWidth]
              
             //self.clipsToBounds = true
             // contentView.layer.masksToBounds = true
              view.clipsToBounds = true
             // contentView.layer.masksToBounds = true
              //containerVIew.clipsToBounds = true
             // contentView.layer.masksToBounds = true
              //containerVIew.layer.masksToBounds = true
            }
        }

       
        
    }
     func loadViewFromNib() -> UIView {
                 let bundle = Bundle(for: type(of: self))
                 let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
                 let nibView = nib.instantiate(withOwner: self, options: nil).first as! UIView

                 return nibView
             }
    
}
extension GraduateMenu: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return universityDataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ImageAndLabelTableViewCell", for: indexPath) as! ImageAndLabelTableViewCell
        cell.title.text = universityDataSource[indexPath.row].title
        cell.rightImage.isHidden = true
        cell.mainImage.image = UIImage(named: "iconUnchecked")
        return cell
    }
    
    
}
extension GraduateMenu: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if universityDataSource[indexPath.row].selected == false {
            tableView.reloadRows(at: [indexPath], with: .automatic)
            universityDataSource[indexPath.row].selected = true
        } else {
            tableView.reloadRows(at: [indexPath], with: .automatic)
                       universityDataSource[indexPath.row].selected = true
        }
    }
}
struct CheckBox {
    var title: String?
    var selected: Bool?
}
extension UIView
{
    func fixInView(_ container: UIView!) -> Void{
        self.translatesAutoresizingMaskIntoConstraints = false;
        self.frame = container.frame;
        container.addSubview(self);
        NSLayoutConstraint(item: self, attribute: .leading, relatedBy: .equal, toItem: container, attribute: .leading, multiplier: 1.0, constant: 0).isActive = true
        NSLayoutConstraint(item: self, attribute: .trailing, relatedBy: .equal, toItem: container, attribute: .trailing, multiplier: 1.0, constant: 0).isActive = true
        NSLayoutConstraint(item: self, attribute: .top, relatedBy: .equal, toItem: container, attribute: .top, multiplier: 1.0, constant: 0).isActive = true
        NSLayoutConstraint(item: self, attribute: .bottom, relatedBy: .equal, toItem: container, attribute: .bottom, multiplier: 1.0, constant: 0).isActive = true
    }
}
