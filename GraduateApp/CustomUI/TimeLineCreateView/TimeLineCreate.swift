//
//  TimeLineCreate.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 6/26/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import UIKit
import AMPopTip
protocol RecevingActionFromTimelineCreate: class {
    func postAction()
    func goLiveAction()
    func gradutaeSpotAction()
    func graduateUniAction()
    func dealAction()
}
class TimeLineCreate: UIView {
//    var clipsToBounds.ini
    @IBOutlet weak var containerVIew: CustomView!
    var view: UIView!
    weak var delagate: RecevingActionFromTimelineCreate?
       
    
      override init(frame: CGRect) {
          super.init(frame: frame)
          //setupViews()
        }
        required init?(coder aDecoder: NSCoder) {
          super.init(coder: aDecoder)
            clipsToBounds = false
            
           // self.view.layer.masksToBounds = true
//            constraintsInCon
      //setupViews()
        }
        
       
        func setupViews() {
          func commonInit() {
              let bundle = Bundle.init(for: TimeLineCreate.self)
              if let viewsToAdd = bundle.loadNibNamed("TimeLineCreate", owner: self, options: nil), let contentView = viewsToAdd.first as? UIView {
                  addSubview(contentView)
                  contentView.frame = self.bounds
                  contentView.autoresizingMask = [.flexibleHeight,
                                                  .flexibleWidth]
                
               //self.clipsToBounds = true
               // contentView.layer.masksToBounds = true
                contentView.clipsToBounds = true
               // contentView.layer.masksToBounds = true
                //containerVIew.clipsToBounds = true
               // contentView.layer.masksToBounds = true
                //containerVIew.layer.masksToBounds = true
              }
          }
    }
    @IBAction func postAction(_ sender: Any) {
        delagate?.postAction()
    }
    @IBAction func liveAction(_ sender: Any) {
        delagate?.goLiveAction()
    }
    @IBAction func spotAction(_ sender: Any) {
        delagate?.gradutaeSpotAction()
    }
    @IBAction func uniAction(_ sender: Any) {
        delagate?.graduateUniAction()
    }
    @IBAction func dealAction(_ sender: Any) {
        delagate?.dealAction()
    }
}
