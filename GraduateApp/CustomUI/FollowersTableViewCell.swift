//
//  FollowersTableViewCell.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 7/31/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import UIKit
protocol ActionFromFollowersTVC: class {
    func sendingFollowBtnAction(id: Int, buttonTitle: String)
}
class FollowersTableViewCell: UITableViewCell {

    @IBOutlet weak var followBtn: AnimatedButton!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var mainImage: UIImageView!
    weak var delegate: ActionFromFollowersTVC?
    var id:Int?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        mainImage.layer.cornerRadius = 8
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func followBtnAction(_ sender: UIButton) {
        delegate?.sendingFollowBtnAction(id: id ?? 0, buttonTitle: sender.titleLabel?.text ?? "")
    }
    
}
