//
//  ErrorChatMsgViewController.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 7/21/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import UIKit
protocol SendingActionFromErrorChat: class {
    func sendingAction()
    func sendingCancelAction()
    func adctionwithType(type: ErrorType)
   
}
class ErrorChatMsgViewController: UIViewController {
    
    @IBOutlet weak var cancelBtn: AnimatedButton!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var okBtn: AnimatedButton!
    @IBOutlet weak var headerLabel: UILabel!
    weak var delegaet: SendingActionFromErrorChat?
    var headerString: String?
    var messageString: String?
    var popupType: ErrorType?
    var id: Int?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        organizePopUpTypes()
        
    }
    
    @IBAction func okAction(_ sender: Any) {
        switch popupType {
        case .loginEmptyFields:
            dismiss()
        case .registerEmptyField:
            dismiss()
        case .loginUnAuthorised:
            dismiss()
        case .emailVerification:
            dismiss()
        case .termsAndCondition:
            dismiss()
        case .emailAuth:
            dismiss()
        case .pohneAuth:
            dismiss()
        case .passwordChanged:
            dismiss()
        case .unFollowOther:
            dismiss()
            delegaet?.sendingAction()
        case .universityRegistered:
            dismiss()
            delegaet?.sendingAction()
        case .deleteUniversity:
            dismiss()
            delegaet?.adctionwithType(type: .deleteUniversity)
            
        case .deleteAccout:
            
            AccountSettingResponse.requestToDeleteUser { (response) in
                UserDefaultsHandler.removeUD(key: .token)
                UserDefaultsHandler.removeUD(key: .deviceToken)
                let vc = UIStoryboard.loginStoryboard.instantiateWelcomeViewController()
                let nav = CustomNavigationController(rootViewController: vc)
                if #available(iOS 13.0, *) {
                    
                    let mySceneDelegate = self.view.window?.windowScene?.delegate as? SceneDelegate
                    
                    mySceneDelegate?.changingRootViewController(vc: nav)
                } else {
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    appDelegate.changingRootViewController(vc: nav)
                    // Fallback on earlier versions
                }
            }
        case .profilePictureSelected:
            dismiss()
            delegaet?.sendingAction()
        case .coverPictureSelected:
            dismiss()
            delegaet?.sendingCancelAction()
        case .offerCreated:
            dismiss()
            delegaet?.sendingAction()
        case .eventCreated:
            dismiss()
            delegaet?.sendingAction()
        case .postCreated:
            dismiss()
            delegaet?.sendingAction()
        case .deleteComment:
            dismiss()
            delegaet?.sendingAction()
        case .portFolioCreated:
        dismiss()
            delegaet?.adctionwithType(type: .portFolioCreated)
        case .postEdited:
            dismiss()
            delegaet?.adctionwithType(type: .postEdited)
        case .portFolioEdited:
            dismiss()
            delegaet?.adctionwithType(type: .portFolioEdited)
        case .spotEdited:
            dismiss()
            delegaet?.adctionwithType(type: .spotEdited)
        case .deletePost:
            dismiss()
            delegaet?.adctionwithType(type: .deletePost)
        case .deletePortFolio:
            dismiss()
            delegaet?.adctionwithType(type: .deletePortFolio)
        case .deleteOffer:
            dismiss()
            delegaet?.adctionwithType(type: .deleteOffer)
        case .deleteEvent:
            dismiss()
            delegaet?.adctionwithType(type: .deleteEvent)
        case .uploadDefaultMusic:
            dismiss()
            delegaet?.adctionwithType(type: .uploadDefaultMusic)
        case .profileUpdated:
            dismiss()
            delegaet?.adctionwithType(type: .uploadDefaultMusic)
        default:
            dismiss()
        }
    }
    
    
    
    @IBAction func cancelBtnAction(_ sender: Any) {
        switch popupType {
        case .universityRegistered:
            dismiss()
            delegaet?.sendingCancelAction()
        default:
            dismiss()
        }
    }
    func organizePopUpTypes() {
        switch popupType {
        case .loginEmptyFields:
            headerLabel.text = "Error"
            messageLabel.text = "Please enter your email and password"
            okBtn.setTitle("Try again", for: .normal)
            cancelBtn.isHidden = true
        case .registerEmptyField:
            headerLabel.text = "Error"
            messageLabel.text = "Please fill up all the fields"
            okBtn.setTitle("Try again", for: .normal)
            cancelBtn.isHidden = true
        case .loginUnAuthorised:
            headerLabel.text = "Error"
            messageLabel.text = "Username and password is incorrect"
            okBtn.setTitle("Try again", for: .normal)
            cancelBtn.isHidden = true
            okBtn.isHidden = false
        case .emailVerification:
            headerLabel.text = "Error"
            messageLabel.text = "Your email is still not verified"
            okBtn.setTitle("Try again", for: .normal)
            cancelBtn.isHidden = true
            okBtn.isHidden = false
        case .termsAndCondition:
            headerLabel.text = "Error"
            messageLabel.text = "Your must agree terms and conditions before proceeding"
            okBtn.setTitle("Try again", for: .normal)
            cancelBtn.isHidden = true
            okBtn.isHidden = false
        case .emailAuth:
            headerLabel.text = "Error"
            messageLabel.text = messageString
            okBtn.setTitle("Try again", for: .normal)
            cancelBtn.isHidden = true
            okBtn.isHidden = false
        case .pohneAuth:
            headerLabel.text = "Error"
            messageLabel.text = messageString
            okBtn.setTitle("Try again", for: .normal)
            cancelBtn.isHidden = true
            okBtn.isHidden = false
        case .passwordChanged:
            headerLabel.text = "Success"
            messageLabel.text = "Password successfully changed"
            okBtn.setTitle("Ok", for: .normal)
            cancelBtn.isHidden = true
            okBtn.isHidden = false
        case .oldPasswordNotMatched:
            headerLabel.text = "Error"
            messageLabel.text = "Old password not matched"
            okBtn.setTitle("Try again", for: .normal)
            cancelBtn.isHidden = true
            okBtn.isHidden = false
        case .passwordValidation:
            headerLabel.text = "Error"
            messageLabel.text = messageString
            okBtn.setTitle("Try again", for: .normal)
            cancelBtn.isHidden = true
            okBtn.isHidden = false
        case .profileUpdated:
            headerLabel.text = "Success"
            messageLabel.text = "Profile has been successfully updated !"
            okBtn.setTitle("Ok", for: .normal)
            cancelBtn.isHidden = true
            okBtn.isHidden = false
        case .deleteAccout:
            cancelBtn.isHidden = false
            okBtn.isHidden = false
            headerLabel.text = ""
            messageLabel.text = "If you do not think you will use Graduation again and would like your account deleted.We can take care of this for you.Keep in that you will not be able to reactivate your account or retrieve any of the cotent or information you have added.If you would like your account deleted, then click Continue"
            okBtn.setTitle("Continue", for: .normal)
            cancelBtn.setTitle("Cancel", for: .normal)
        case .musicDeleted:
            headerLabel.text = "Success"
            messageLabel.text = "Music has been successfully deleted !"
            okBtn.setTitle("Ok", for: .normal)
            cancelBtn.isHidden = true
            okBtn.isHidden = false
            
        case .unFollowOther:
            headerLabel.text = ""
            messageLabel.text = "Are your sure you want to unfollow this user?"
            okBtn.setTitle("Ok", for: .normal)
            cancelBtn.isHidden = false
            okBtn.isHidden = false
        case .somethingWentWrong:
            headerLabel.text = "Error"
            messageLabel.text = "Something went wrong. Please try again !"
            okBtn.setTitle("Try again", for: .normal)
            cancelBtn.isHidden = false
            okBtn.isHidden = false
        case .usernameUpdated:
            headerLabel.text = "Success"
            messageLabel.text = "Username updated successfully!"
            okBtn.setTitle("OK", for: .normal)
            cancelBtn.isHidden = true
            okBtn.isHidden = false
        case .universityUserName:
            headerLabel.text = "Error"
            messageLabel.text = "Unable to create uni \n The business name field is required"
            okBtn.setTitle("Try again", for: .normal)
            cancelBtn.isHidden = true
            okBtn.isHidden = false
        case .univeristyFieldAllEmpty:
            headerLabel.text = "Error"
            messageLabel.text = "Unable to create uni \n The business name field is required \n The about business field is required"
            okBtn.setTitle("Try again", for: .normal)
            cancelBtn.isHidden = true
            okBtn.isHidden = false
        case .universityCategoryEmplty:
            headerLabel.text = "Error"
            messageLabel.text = "The category field must not be empty"
            okBtn.setTitle("Try again", for: .normal)
            cancelBtn.isHidden = true
            okBtn.isHidden = false
        case .universityRegistered:
            headerLabel.text = "Success"
            messageLabel.text = "Your university is registered successfully.Do you wish to see your university?"
            okBtn.isHidden = false
            okBtn.setTitle("Yes", for: .normal)
            cancelBtn.isHidden = false
            cancelBtn.setTitle("Cancel")
            okBtn.isHidden = false
        case .profilePictureSelected:
            headerLabel.text = ""
            messageLabel.text = "Are you sure you want to upload this photo as Profile picture?"
            okBtn.isHidden = false
            okBtn.setTitle("Upload", for: .normal)
            cancelBtn.isHidden = false
            cancelBtn.setTitle("Cancel")
            okBtn.isHidden = false
        case .coverPictureSelected:
            headerLabel.text = ""
            messageLabel.text = "Are you sure you want to upload this photo as Cover picture?"
            okBtn.isHidden = false
            okBtn.setTitle("Upload", for: .normal)
            cancelBtn.isHidden = false
            cancelBtn.setTitle("Cancel")
            okBtn.isHidden = false
        case .deleteUniversity:
            headerLabel.text = ""
            messageLabel.text = "This will delete university"
            okBtn.isHidden = false
            okBtn.setTitle("Ok", for: .normal)
            cancelBtn.isHidden = false
            cancelBtn.setTitle("Cancel")
            okBtn.isHidden = false
        case .portFolioCreated:
            headerLabel.text = "Success"
            messageLabel.text = "PortFolio created"
            okBtn.isHidden = false
            okBtn.setTitle("Ok", for: .normal)
            cancelBtn.isHidden = true
            okBtn.isHidden = false
        case .nullDealDescInDeal:
            headerLabel.text = "Error"
            messageLabel.text = "Deal description must not be empty"
            okBtn.isHidden = false
            okBtn.setTitle("Try again", for: .normal)
            cancelBtn.isHidden = true
            okBtn.isHidden = false
        case .nullDiscountType:
            headerLabel.text = "Error"
            messageLabel.text = "ERROR CREATING DEAL \n Discount type field is required"
            okBtn.isHidden = false
            okBtn.setTitle("Try again", for: .normal)
            cancelBtn.isHidden = true
            okBtn.isHidden = false
        case .emptyDiscount:
            headerLabel.text = "Error"
            messageLabel.text = "ERROR CREATING DEAL \n Discount amount or percentage field is required"
            okBtn.isHidden = false
            okBtn.setTitle("Try again", for: .normal)
            cancelBtn.isHidden = true
            okBtn.isHidden = false
        case .emptyDate:
            headerLabel.text = "Error"
            messageLabel.text = "ERROR CREATING DEAL \n Date field is required"
            okBtn.isHidden = false
            okBtn.setTitle("Try again", for: .normal)
            cancelBtn.isHidden = true
            okBtn.isHidden = false
        case .dateError:
            headerLabel.text = "Error"
            messageLabel.text = ("The expiry date must be a date after or equal to " + (messageString ?? ""))
            okBtn.isHidden = false
            okBtn.setTitle("Try again", for: .normal)
            cancelBtn.isHidden = true
            okBtn.isHidden = false
        case .offerCreated:
            headerLabel.text = "Success"
            messageLabel.text = ("Offer Created")
            okBtn.isHidden = false
            okBtn.setTitle("Ok", for: .normal)
            cancelBtn.isHidden = true
            okBtn.isHidden = false
        case .eventName:
            headerLabel.text = "Error"
            messageLabel.text = ("Event name field is required")
            okBtn.isHidden = false
            okBtn.setTitle("Try again", for: .normal)
            cancelBtn.isHidden = true
            okBtn.isHidden = false
        case .createPostEmpty:
            headerLabel.text = "Error"
            messageLabel.text = ("The message field is required when none of images/videos are present")
            okBtn.isHidden = false
            okBtn.setTitle("Try again", for: .normal)
            cancelBtn.isHidden = true
            okBtn.isHidden = false
        case .eventCreated:
            headerLabel.text = "Success"
            messageLabel.text = ("Spot added successfully")
            okBtn.isHidden = false
            okBtn.setTitle("Ok", for: .normal)
            cancelBtn.isHidden = true
            okBtn.isHidden = false
            case .postCreated:
                       headerLabel.text = "Success"
                       messageLabel.text = ("Post added successfully")
                       okBtn.isHidden = false
                       okBtn.setTitle("Ok", for: .normal)
                       cancelBtn.isHidden = true
                       okBtn.isHidden = false
            case .reported:
                                  headerLabel.text = "Success"
                                  messageLabel.text = ("Reported successfully")
                                  okBtn.isHidden = false
                                  okBtn.setTitle("Ok", for: .normal)
                                  cancelBtn.isHidden = true
                                  okBtn.isHidden = false
        case .unableChat:
            headerLabel.text = "Error"
                       messageLabel.text = ("You cannot chat with this user")
                       okBtn.isHidden = false
                       okBtn.setTitle("Try again", for: .normal)
                       cancelBtn.isHidden = true
                       okBtn.isHidden = false
        case .emptyPostAs:
            headerLabel.text = "Error"
                       messageLabel.text = messageString
                       okBtn.isHidden = false
                       okBtn.setTitle("Try again", for: .normal)
                       cancelBtn.isHidden = true
                       okBtn.isHidden = false
        case .deleteComment:
            headerLabel.text = ""
                       messageLabel.text = "Are you sure you want to delete this comment?"
                       okBtn.isHidden = false
                       okBtn.setTitle("Ok", for: .normal)
                       cancelBtn.isHidden = false
            cancelBtn.setTitle("Cancel", for: .normal)
                       okBtn.isHidden = false
        case .commentDeleted:
            headerLabel.text = ""
                       messageLabel.text = "Comment deleted successfully!"
                       okBtn.isHidden = false
                       okBtn.setTitle("Ok", for: .normal)
                       cancelBtn.isHidden = true
                       okBtn.isHidden = false
        case .postEdited:
            headerLabel.text = "Success"
                       messageLabel.text = "Post has been updated successfully!"
                       okBtn.isHidden = false
                       okBtn.setTitle("Ok", for: .normal)
                       cancelBtn.isHidden = true
                       okBtn.isHidden = false
        case .portFolioEdited:
            headerLabel.text = "Success"
                       messageLabel.text = "PortFolio has been updated successfully!"
                       okBtn.isHidden = false
                       okBtn.setTitle("Ok", for: .normal)
                       cancelBtn.isHidden = true
                       okBtn.isHidden = false
        case .spotEdited:
            headerLabel.text = "Success"
                       messageLabel.text = "Spot has been updated successfully!"
                       okBtn.isHidden = false
                       okBtn.setTitle("Ok", for: .normal)
                       cancelBtn.isHidden = true
                       okBtn.isHidden = false
        case .deletePost:
            headerLabel.text = ""
                       messageLabel.text = "Are you sure!!"
                       okBtn.isHidden = false
                       okBtn.setTitle("Ok", for: .normal)
                       cancelBtn.isHidden = false
                       okBtn.isHidden = false
        case .deletePortFolio:
            headerLabel.text = ""
                       messageLabel.text = "Are you sure!!"
                       okBtn.isHidden = false
                       okBtn.setTitle("Ok", for: .normal)
                       cancelBtn.isHidden = false
                       okBtn.isHidden = false
        case .deleteOffer:
            headerLabel.text = ""
                       messageLabel.text = "Are you sure!!"
                       okBtn.isHidden = false
                       okBtn.setTitle("Ok", for: .normal)
                       cancelBtn.isHidden = false
                       okBtn.isHidden = false
        case .offerDeleted:
            headerLabel.text = "Success"
                       messageLabel.text = "Offer deleted successfully!!"
                       okBtn.isHidden = false
                       okBtn.setTitle("Ok", for: .normal)
                       cancelBtn.isHidden = true
                       okBtn.isHidden = false
        case .portfolioDeleted:
            headerLabel.text = "Success"
                       messageLabel.text = "Portfolio deleted successfully!!"
                       okBtn.isHidden = false
                       okBtn.setTitle("Ok", for: .normal)
                       cancelBtn.isHidden = true
                       okBtn.isHidden = false
        case .deleteEvent:
            headerLabel.text = ""
                       messageLabel.text = "Are you sure!!"
                       okBtn.isHidden = false
                       okBtn.setTitle("Ok", for: .normal)
                       cancelBtn.isHidden = false
                       okBtn.isHidden = false
        case .uploadDefaultMusic:
            headerLabel.text = ""
                       messageLabel.text = "Are you sure you want to upload this as default music !!"
                       okBtn.isHidden = false
                       okBtn.setTitle("Upload", for: .normal)
                       cancelBtn.isHidden = false
                       okBtn.isHidden = false
        default:
            break
            
        }
    }
    func dismiss() {
        let transition: CATransition = CATransition()
        transition.duration = 0.5
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        transition.type = CATransitionType.fade
        transition.subtype = CATransitionSubtype.fromBottom
        self.view.window!.layer.add(transition, forKey: nil)
        self.dismiss(animated: false, completion: nil)
    }
}
