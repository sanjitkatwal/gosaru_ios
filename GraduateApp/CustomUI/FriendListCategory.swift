//
//  FriendListCategory.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 7/29/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import UIKit
protocol ActionsFromFriednsCatView: class {
    func friendsAction()
    func allAction()
    func requestAction()
    func pendingAction()
    func declinedAction()
    func blockedAction()
}
class FriendListCategory: UIView {
    @IBOutlet weak var blockedImage: UIImageView!
    @IBOutlet weak var pendingImage: UIImageView!
    @IBOutlet weak var declinedImage: UIImageView!
    @IBOutlet weak var requestImage: UIImageView!
    @IBOutlet weak var allImage: UIImageView!
    @IBOutlet weak var friendsImage: UIImageView!
    @IBOutlet weak var declinedBtn: UIButton!
    @IBOutlet weak var blockedBtn: UIButton!
    @IBOutlet weak var pendingBtn: UIButton!
    @IBOutlet weak var friendsBtn: UIButton!
    @IBOutlet weak var requestbtn: UIButton!
    @IBOutlet weak var allBtn: UIButton!
    weak var delegate: ActionsFromFriednsCatView?
    override init(frame: CGRect) {
              super.init(frame: frame)
              setupViews()
            }
    @IBAction func friendsBtn(_ sender: Any) {
    }
    @IBOutlet weak var view: UIView!
    required init?(coder aDecoder: NSCoder) {
              super.init(coder: aDecoder)
                clipsToBounds = false
                
               // self.view.layer.masksToBounds = true
    //            constraintsInCon
          setupViews()
            }
    override class func awakeFromNib() {
        super.awakeFromNib()
        print("")
    }
    
            
           
            func setupViews() {
              func commonInit() {
                  let bundle = Bundle.init(for: FriendListCategory.self)
                  if let viewsToAdd = bundle.loadNibNamed("FriendListCategory", owner: self, options: nil), let contentView = viewsToAdd.first as? UIView {
                      addSubview(contentView)
                      contentView.frame = self.bounds
                      contentView.autoresizingMask = [.flexibleHeight,
                                                      .flexibleWidth]
                    clipsToBounds = false
                    contentView.clipsToBounds = false
                    view.clipsToBounds = false
                   
                  }
              }
        }
    @IBAction func friendsAction(_ sender: Any) {
        changingBtnImage(image: friendsImage)
        delegate?.friendsAction()
    }
    @IBAction func AllAction(_ sender: Any) {
        changingBtnImage(image: allImage)
        delegate?.allAction()
    }
    
    @IBAction func requestAction(_ sender: Any) {
        changingBtnImage(image: requestImage)
        delegate?.requestAction()
    }
    //    @IBAction func friendsAction(_ sender: Any) {
//        print("")
//    }
    @IBAction func pedingAction(_ sender: Any) {
        changingBtnImage(image: pendingImage)
        delegate?.pendingAction()
    }
    @IBAction func declinedAction(_ sender: Any) {
        changingBtnImage(image: declinedImage)
        delegate?.declinedAction()
        
    }
    @IBAction func blockedAction(_ sender: Any) {
        changingBtnImage(image: blockedImage)
        delegate?.blockedAction()
    }
    func changingBtnImage(image: UIImageView) {
        friendsImage.image = UIImage(named: "iconUnchecked")
        allImage.image = UIImage(named: "iconUnchecked")
        requestImage.image = UIImage(named: "iconUnchecked")
        pendingImage.image = UIImage(named: "iconUnchecked")
        declinedImage.image = UIImage(named: "iconUnchecked")
        blockedImage.image = UIImage(named: "iconUnchecked")
        image.image =  UIImage(named: "iconChecked")

    }
}
extension UIButton {
    func leftImage(image: UIImage, renderMode: UIImage.RenderingMode) {
        self.setImage(image.withRenderingMode(renderMode), for: .normal)
        self.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: image.size.width / 2)
        self.contentHorizontalAlignment = .left
        self.imageView?.contentMode = .scaleAspectFit
    }

    func rightImage(image: UIImage, renderMode: UIImage.RenderingMode){
        self.setImage(image.withRenderingMode(renderMode), for: .normal)
        self.imageEdgeInsets = UIEdgeInsets(top: 0, left:image.size.width / 2, bottom: 0, right: 0)
        self.contentHorizontalAlignment = .right
        self.imageView?.contentMode = .scaleAspectFit
    }
}
extension UIButton {

  /// Add image on left view
  func leftImage(image: UIImage) {
    self.setImage(image, for: .normal)
    self.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: image.size.width)
  }
}
extension UIButton {
    func moveImageLeftTextCenter(image : UIImage, imagePadding: CGFloat, renderingMode: UIImage.RenderingMode){
        self.setImage(image.withRenderingMode(renderingMode), for: .normal)
        guard let imageViewWidth = self.imageView?.frame.width else{return}
        guard let titleLabelWidth = self.titleLabel?.intrinsicContentSize.width else{return}
        self.contentHorizontalAlignment = .left
        let imageLeft = imagePadding - imageViewWidth / 2
        let titleLeft = (bounds.width - titleLabelWidth) / 2 - imageViewWidth
        imageEdgeInsets = UIEdgeInsets(top: 0.0, left: imageLeft, bottom: 0.0, right: 0.0)
        titleEdgeInsets = UIEdgeInsets(top: 0.0, left: titleLeft , bottom: 0.0, right: 0.0)
    }
}
