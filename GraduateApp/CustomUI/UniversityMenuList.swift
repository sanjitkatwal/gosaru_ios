//
//  UniversityMenuList.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 8/3/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import UIKit
import Foundation
import AMPopTip

class UniversityMenuList: UIView {
         
   
    
    @IBOutlet weak var universityTableView: UITableView!
    
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var containerView: UIView!
    var view: UIView!
    override init(frame: CGRect) {
            super.init(frame: frame)
            //setupViews()
          }
          required init?(coder aDecoder: NSCoder) {
            super.init(coder: aDecoder)
              clipsToBounds = false

//    setupViews()
          }
          
         
          func setupViews() {
               universityTableView.register(UINib.init(nibName: "UniversityListTableViewCell", bundle: nil), forCellReuseIdentifier: "UniversityListTableViewCell")

            universityTableView.dataSource = self
            universityTableView.delegate = self
            universityTableView.reloadData()
                   

//                let bundle = Bundle.init(for: UniversityMenuList.self)
//                if let viewsToAdd = bundle.loadNibNamed("UniversityMenuList", owner: self, options: nil), let contentView = viewsToAdd.first as? UIView {
//                    addSubview(contentView)
//                    contentView.frame = self.bounds
//                    contentView.autoresizingMask = [.flexibleHeight,
//                                                    .flexibleWidth]
//
//
//                  contentView.clipsToBounds = true
               
                }
            

//}
    @IBAction func OkBtnAction(_ sender: Any) {
        print("OK")
    }
}
extension UniversityMenuList: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "UniversityListTableViewCell", for: indexPath) as! UniversityListTableViewCell
        return cell
    }


}
extension UniversityMenuList: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 25
    }
}
