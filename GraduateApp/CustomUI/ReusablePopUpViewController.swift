//
//  ReusablePopUpViewController.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 7/30/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import UIKit
import Popover
protocol SendingActionFromReusablepopUp: class {
    func sendingVlaue(value: Bool)
}
class ReusablePopUpViewController: UIViewController {
    @IBOutlet weak var headerLabel: UILabel!
    weak var delegate: SendingActionFromReusablepopUp?
    var type: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.layer.cornerRadius = 12
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        view.isOpaque = false
                              view.backgroundColor = UIColor.black.withAlphaComponent(0.2)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
       
    }
    
    @IBAction func okbtnAction(_ sender: Any) {
        if type == "Select Profile Image" {
            delegate?.sendingVlaue(value: true)
            self.dismiss(animated: true, completion: nil)
        } else {
              self.dismiss(animated: true, completion: nil)
        }
      
    }
    @IBAction func cancelBtnAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)

    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
