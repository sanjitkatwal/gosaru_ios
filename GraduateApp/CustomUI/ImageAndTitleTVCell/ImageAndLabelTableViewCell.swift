//
//  ImageAndLabelTableViewCell.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 6/30/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import UIKit

class ImageAndLabelTableViewCell: UITableViewCell {
   
    @IBOutlet weak var rightImage: UIImageView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var mainImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
       super.setSelected(selected, animated: animated)
       // update UI
       accessoryType = selected ? .checkmark : .none
    }
    
}
