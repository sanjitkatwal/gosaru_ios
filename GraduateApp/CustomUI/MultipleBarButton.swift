//
//  MultipleBarButton.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 6/24/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import Foundation

enum BarButtonItemPosition {
    case right, left
}

enum BarButtonItemType {
    case menu(BarButtonItemPosition)
    case close(BarButtonItemPosition)
    case notification(BarButtonItemPosition)
}

/// Has default implementation on UIViewControllers that conform to BarButtonActions.
protocol BarButtonItemConfiguration: class {

    func addBarButtonItem(ofType type: BarButtonItemType)
}

/// Hate that we're forced to expose button targets to objc runtime :(
/// but I don't know any other way for the time being, maybe in Swift 6 :)
@objc protocol BarButtonActions {
    @objc func presentLeftMenu(_ sender:AnyObject)
    @objc func dismissController(_ sender:AnyObject)
    @objc func showNotification(_ sender:AnyObject)
}

extension BarButtonItemConfiguration where Self: UIViewController, Self: BarButtonActions {

    func addBarButtonItem(ofType type: BarButtonItemType) {

        func newButton(imageName: String, position: BarButtonItemPosition, action: Selector?) {
            let button = UIBarButtonItem(image: UIImage(named: imageName), style: .plain, target: self, action: action)
            switch position {
            case .left: self.navigationItem.leftBarButtonItem = button
            case .right: self.navigationItem.rightBarButtonItem = button
            }
        }

        switch type {
        case .menu(let p): newButton(imageName: "", position: p, action: #selector(Self.presentLeftMenu(_:)))
        case .notification(let p): newButton(imageName: "", position: p, action: #selector(Self.showNotification(_:)))
        case .close(let p): newButton(imageName: "", position: p, action: #selector(Self.dismissController(_:)))
        }
    }
}
protocol BarButtonConfigarable: BarButtonItemConfiguration, BarButtonActions {}
