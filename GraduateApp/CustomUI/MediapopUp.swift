//
//  MediapopUp.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 8/19/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import UIKit
import AMPopTip
protocol SendingActionToChatVC: class {
    func sendingMediaAction(type: MediaTyope) 
    
    
}
class MediapopUp: UIView {
    weak var delegate: SendingActionToChatVC?
     override init(frame: CGRect) {
              super.init(frame: frame)
              //setupViews()
            }
            required init?(coder aDecoder: NSCoder) {
              super.init(coder: aDecoder)
                clipsToBounds = false
                
               // self.view.layer.masksToBounds = true
    //            constraintsInCon
          //setupViews()
            }

    @IBAction func galleryAction(_ sender: Any) {
        delegate?.sendingMediaAction(type: .galley)
    }
    @IBAction func cameraAction(_ sender: Any) {
      delegate?.sendingMediaAction(type: .camera)
    }
    @IBAction func videoAction(_ sender: Any) {
          delegate?.sendingMediaAction(type: .video)
    }
}
