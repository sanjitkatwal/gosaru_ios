//
//  ProfileTableViewCell.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 7/15/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import UIKit
import FlagPhoneNumber
protocol SendingActionFromProfileTVC: class {
    func sendingActionFromTxtField(textField: UITextField)
    func sendingTextFieldText(text: String, textFiledType: universityProfileInfo)
    func requestingForupdateProfile(params: [String: Any])
    func deleteBtnAction()
}
class ProfileTableViewCell: UITableViewCell {
    @IBOutlet var footerBtnStackView: UIStackView!
    @IBOutlet var universityTypeContainerView: CustomView!
    @IBOutlet var descContainerView: CustomView!
    @IBOutlet var nameContainerView: CustomView!
    @IBOutlet weak var editBtn: AnimatedButton!
    @IBOutlet weak var deleteBtn: AnimatedButton!
    @IBOutlet weak var facebookTxtField: UITextField!
    @IBOutlet weak var emailTxtField: UITextField!
    @IBOutlet weak var locationTxtField: UITextField!
    @IBOutlet weak var openingHourTxtField: UITextField!
    @IBOutlet weak var websiteTxtFeidl: UITextField!
    @IBOutlet weak var phoneNumberTxtField: UITextField!
    @IBOutlet weak var uniTypeTxtField: UITextField!
    @IBOutlet weak var aboutBusinessTxtView: UITextView!
    @IBOutlet weak var businessNameTxtField: UITextField!
    weak var delegate: SendingActionFromProfileTVC?
     var listController: FPNCountryListViewController = FPNCountryListViewController(style: .grouped)
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        makingTxtFieldDisabled(boolValue: false)
        HelperFunctions.addingImageInRightTxtField(textField: uniTypeTxtField, imageName: "iconArrowDown")
        phoneNumberTxtField.delegate = self
        businessNameTxtField.delegate = self
        uniTypeTxtField.delegate = self
        aboutBusinessTxtView.delegate = self
        websiteTxtFeidl.delegate = self
        locationTxtField.delegate = self
        openingHourTxtField.delegate = self
        emailTxtField.delegate = self
        facebookTxtField.delegate = self
        aboutBusinessTxtView.delegate = self
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func editAction(_ sender: Any) {
        if editBtn.titleLabel?.text == "Edit" {
            deleteBtn.setTitle("Cancel")
                   editBtn.setTitle("Update")
                   makingTxtFieldDisabled(boolValue: true)
            delegate?.sendingTextFieldText(text: "\(0)", textFiledType: .businessNameHidden)
        } else if editBtn.titleLabel?.text == "Update"{
            let params: [String : Any] = ["business_name" : (businessNameTxtField.text ?? ""), "business_type" : (uniTypeTxtField.text ?? ""), "address_string" : locationTxtField.text ?? "", "phone" : phoneNumberTxtField.text ?? "", "email" : emailTxtField.text ?? "", "website_link" : websiteTxtFeidl.text ?? "", "about_business" : aboutBusinessTxtView.text ?? "", "facebook" : (facebookTxtField.text ?? "")] 
            delegate?.requestingForupdateProfile(params: params)
        }
       
        
    }
    @IBAction func deleteAction(_ sender: Any) {
        if deleteBtn.titleLabel?.text == "Delete" {
            delegate?.deleteBtnAction()
        } else {
            makingTxtFieldDisabled(boolValue: false)
        }
    }
    func makingTxtFieldDisabled(boolValue: Bool) {
        facebookTxtField.isUserInteractionEnabled = boolValue
        emailTxtField.isUserInteractionEnabled = boolValue
        locationTxtField.isUserInteractionEnabled = boolValue
        openingHourTxtField.isUserInteractionEnabled = boolValue
        websiteTxtFeidl.isUserInteractionEnabled = boolValue
        phoneNumberTxtField.isUserInteractionEnabled = boolValue
        uniTypeTxtField.isUserInteractionEnabled = boolValue
        aboutBusinessTxtView.isUserInteractionEnabled = boolValue
        businessNameTxtField.isUserInteractionEnabled = boolValue
    }
}
extension ProfileTableViewCell: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == phoneNumberTxtField {
            delegate?.sendingActionFromTxtField(textField: phoneNumberTxtField)
        }
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == businessNameTxtField {
            delegate?.sendingTextFieldText(text: businessNameTxtField.text ?? "", textFiledType: .businessname)
        }
        if textField == uniTypeTxtField {
            delegate?.sendingTextFieldText(text: uniTypeTxtField.text ?? "", textFiledType: .businesType)
        }
        if textField == phoneNumberTxtField {
            delegate?.sendingTextFieldText(text: phoneNumberTxtField.text ?? "", textFiledType: .phone)
               }
        if textField == websiteTxtFeidl {
            delegate?.sendingTextFieldText(text: websiteTxtFeidl.text ?? "", textFiledType: .website)
                      }
        if textField == locationTxtField {
            delegate?.sendingTextFieldText(text: locationTxtField.text ?? "", textFiledType: .address)
                            }
        if textField == openingHourTxtField {
            delegate?.sendingTextFieldText(text: openingHourTxtField.text ?? "", textFiledType: .openingHour)
                                 }
        if textField == emailTxtField {
            delegate?.sendingTextFieldText(text: emailTxtField.text ?? "", textFiledType: .email)
                                       }
        if textField == facebookTxtField {
            delegate?.sendingTextFieldText(text: facebookTxtField.text ?? "", textFiledType: .facebook)

        }
    }
}
extension ProfileTableViewCell: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView == aboutBusinessTxtView {
            aboutBusinessTxtView.textColor = UIColor.init(hexString: Appcolors.text)
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView == aboutBusinessTxtView {
            delegate?.sendingTextFieldText(text: aboutBusinessTxtView.text, textFiledType: .aboutbusinesss)
        }
    }
}


enum universityProfileInfo {
    case businessname
    case aboutbusinesss
    case businesType
    case phone
    case website
    case address
    case openingHour
    case email
    case facebook
    case businessNameHidden
}

