//
//  SceneDelegate.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 6/22/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import UIKit
import Firebase
class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?
    var tab: AZTabBarController?
   
    @available(iOS 13.0, *)
    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        // Use this method to optionally configure and attach the UIWindow `window` to the provided UIWindowScene `scene`.
        // If using a storyboard, the `window` property will automatically be initialized and attached to the scene.
        // This delegate does not imply the connecting scene or session are new (see `application:configurationForConnectingSceneSession` instead).
        if let windowScene = scene as? UIWindowScene {
                       let window = UIWindow(windowScene: windowScene)
            window.rootViewController = CustomNavigationController(rootViewController: UIStoryboard.loginStoryboard.instantiateWelcomeViewController())
//            FirebaseApp.configure()
//            let manager = PushNotificationManager(userID: "")
//            manager.registerForPushNotification(application: scene)
//                       self.window = window
                       window.makeKeyAndVisible()
                   }
        guard let _ = (scene as? UIWindowScene) else { return }
    }

    @available(iOS 13.0, *)
    func sceneDidDisconnect(_ scene: UIScene) {
        // Called as the scene is being released by the system.
        // This occurs shortly after the scene enters the background, or when its session is discarded.
        // Release any resources associated with this scene that can be re-created the next time the scene connects.
        // The scene may re-connect later, as its session was not neccessarily discarded (see `application:didDiscardSceneSessions` instead).
    }

    @available(iOS 13.0, *)
    func sceneDidBecomeActive(_ scene: UIScene) {
        // Called when the scene has moved from an inactive state to an active state.
        // Use this method to restart any tasks that were paused (or not yet started) when the scene was inactive.
    }

    @available(iOS 13.0, *)
    func sceneWillResignActive(_ scene: UIScene) {
        // Called when the scene will move from an active state to an inactive state.
        // This may occur due to temporary interruptions (ex. an incoming phone call).
    }

    @available(iOS 13.0, *)
    func sceneWillEnterForeground(_ scene: UIScene) {
        // Called as the scene transitions from the background to the foreground.
        // Use this method to undo the changes made on entering the background.
    }

    @available(iOS 13.0, *)
    func sceneDidEnterBackground(_ scene: UIScene) {
        // Called as the scene transitions from the foreground to the background.
        // Use this method to save data, release shared resources, and store enough scene-specific state information
        // to restore the scene back to its current state.
    }
    func changeRootViewController(with desiredViewController: UIViewController) {
      let snapshot: UIView = (self.window?.snapshotView(afterScreenUpdates: true))!
      desiredViewController.view.addSubview(snapshot)
      self.window?.rootViewController = desiredViewController
      UIView.animate(withDuration: 0.3, animations: {() in
        snapshot.layer.opacity = 0
        snapshot.layer.transform = CATransform3DMakeScale(1.5, 1.5, 1.5)
      }, completion: { (_: Bool) in
        snapshot.removeFromSuperview()
      })
    }
    func loadTabbar() {
      var mainWindow: UIWindow?
      if #available(iOS 13.0, *) {
        let windowScene = UIApplication.shared.connectedScenes.first as? UIWindowScene
        mainWindow = (windowScene?.delegate as? SceneDelegate)?.window
      } else {
        mainWindow = AppDelegate.shared()?.window
      }
    //MARK: BEFORE TAB BAR
      //let controller = CustomTabbarController()
        
       // MARK: AFTER TAB BAR
       
            let controller = CustomManyTabBar()
        //controller.tabController.setIndex(3)
      //    changeRootViewController(with: drawerContainer!)
       /// mainWindow?.rootViewController = controller
       changeRootViewController(with: controller)
     // mainWindow?.rootViewController = controller
      mainWindow?.makeKeyAndVisible()
    }
    func loadTabbarWithIndex(index: Int) {
      var mainWindow: UIWindow?
      if #available(iOS 13.0, *) {
        let windowScene = UIApplication.shared.connectedScenes.first as? UIWindowScene
        mainWindow = (windowScene?.delegate as? SceneDelegate)?.window
      } else {
        mainWindow = AppDelegate.shared()?.window
      }
    //MARK: BEFORE TAB BAR
      //let controller = CustomTabbarController()
        
       // MARK: AFTER TAB BAR
       
            let controller = CustomManyTabBar()
        controller.chhosetab = true
        //controller.tabController.setIndex(2)
        //controller.tabBarController?.selectedIndex = index
        
      //    changeRootViewController(with: drawerContainer!)
       /// mainWindow?.rootViewController = controller
       changeRootViewController(with: controller)
     // mainWindow?.rootViewController = controller
      mainWindow?.makeKeyAndVisible()
    }
    func loadLoginPage() {
      var mainWindow: UIWindow?
      if #available(iOS 13.0, *) {
        let windowScene = UIApplication.shared.connectedScenes.first as? UIWindowScene
        mainWindow = (windowScene?.delegate as? SceneDelegate)?.window
      } else {
        mainWindow = AppDelegate.shared()?.window
      }
    //MARK: BEFORE TAB BAR
      //let controller = CustomTabbarController()
        
       // MARK: AFTER TAB BAR
       
                  let controller = UIStoryboard.loginStoryboard.instantiateLoginViewController()
        let vc = CustomNavigationController(rootViewController: controller)

      //    changeRootViewController(with: drawerContainer!)
       /// mainWindow?.rootViewController = controller
       changeRootViewController(with: vc)
     // mainWindow?.rootViewController = controller
      mainWindow?.makeKeyAndVisible()
    }
    func changingRootViewController(vc: UIViewController) {
            var mainWindow: UIWindow?
            if #available(iOS 13.0, *) {
              let windowScene = UIApplication.shared.connectedScenes.first as? UIWindowScene
              mainWindow = (windowScene?.delegate as? SceneDelegate)?.window
            } else {
              mainWindow = AppDelegate.shared()?.window
            }
         
              let controller = vc
           
             changeRootViewController(with: controller)
            mainWindow?.makeKeyAndVisible()
          }
}
extension SceneDelegate: MessagingDelegate {
  func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
    print("Firebase registration token: \(fcmToken)")
    UserDefaultsHandler.setToUD(key: .deviceToken, value: fcmToken)
    print(UserDefaultsHandler.setToUD(key: .deviceToken, value: fcmToken))
    let dataDict: [String: String] = ["token": fcmToken]
    // Note: This callback is fired at each app startup and whenever a new token is generated.
  }
  // The callback to handle data message received via FCM for devices running iOS 10 or above.
  func application(received remoteMessage: MessagingRemoteMessage) {
    print(remoteMessage.appData)
  }
}



