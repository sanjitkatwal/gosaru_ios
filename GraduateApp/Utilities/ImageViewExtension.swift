//
//  ImageViewExtension.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 9/14/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import Foundation
import UIKit
import SDWebImage
extension UIImageView {
func setURLImage(imageURL: String?, placeHolder defaultImage: String = "iconLogoSmall") {
  //    SDImageCache.shared().clearMemory()
  if let image = imageURL, image != "" {
    if image.isValidURL() {
      self.sd_imageTransition = .fade
      let properURL = image.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
      let url = URL(string: properURL ?? "")
      self.sd_imageIndicator = SDWebImageActivityIndicator.gray
      let placeHolderImage = UIImage(named: defaultImage)
      self.sd_setImage(with: url, placeholderImage: placeHolderImage)
    } else {
      self.image = UIImage(named: image)
    }
  } else {
    self.image = UIImage(named: "iconPlaceholder")
  }
}
}
