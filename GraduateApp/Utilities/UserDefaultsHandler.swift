//
//  UserDefaultsHandler.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 9/10/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import Foundation

enum UDkey: String {
  case userID = "UserID"
  case token = "Token"
  case userEmail = "Email"
  case fullName = "FullName"
  case location = "Location"
  case countryCode = "CountryCode"
  case appliedForServiceProvider = "Apply For Service Provider"
  case userRoles = "User Roles"
  case login = "login"
  case dob = "Date Of Birth"
  case cityName = "City Name"
  case countryId = "locationId"
  case secondaryContactNum = "contactNumber"
  case cityId = "cityId"
  case stateId = "stateId"
  case stateName = "stateName"
  case zipCode = "zipCode"
  case gender = "gender"
  case addressOne = "addressOne"
  case addressTwo = "addressTwo"
  case experience = "Experience"
  case notificationCount = "NotificationCount"
  case favouriteCount = "FavouriteCount"
  case cartCount = "CartCount"
  case userName = "UserName"
  case password = "Password"
  case profileImage = "ProfileImage"
  case temToken = "TempToken"
  case deviceToken = "DeviceToken"
}

struct UserDefaultsHandler {
  static let userDefault = UserDefaults.standard
  static func setToUD(key: UDkey, value: Any) {
    self.userDefault.setValue(value, forKeyPath: key.rawValue)
    self.userDefault.synchronize()
  }
  static func setDataUD(key: UDkey, value: Any) {
    self.userDefault.set(value, forKey: key.rawValue)
    self.userDefault.synchronize()
  }
  static  func removeUD(key: UDkey) {
    self.userDefault.removeObject(forKey: key.rawValue)
  }
  static func getUDValue(key: UDkey) -> Any? {
    return self.userDefault.value(forKey: key.rawValue)
  }
  static func userSetToData(userData: LoginResponseModel?) {
    if let token = userData?.success?.token {
       UserDefaultsHandler.setToUD(key: UDkey.token, value: token)
     }
    if let userID = userData?.success?.user?.internalIdentifier{
        UserDefaultsHandler.setToUD(key: UDkey.userID, value: userID)
      }
    if let email = userData?.success?.user?.email {
        UserDefaultsHandler.setToUD(key: UDkey.userEmail, value: email)
      }
    if let fullName = userData?.success?.user?.name {
        UserDefaultsHandler.setToUD(key: UDkey.fullName, value: fullName)
      }
    if let image = userData?.success?.user?.profile?.profileImage {
        UserDefaultsHandler.setToUD(key: UDkey.profileImage, value: image)
      }
    if let gender = userData?.success?.user?.profile?.gender {
        UserDefaultsHandler.setToUD(key: UDkey.gender, value: gender)
      }
  }
}

