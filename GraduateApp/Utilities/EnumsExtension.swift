//
//  EnumsExtension.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 7/2/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import Foundation
enum SettingsList: String {
    case AccountSetting = "Account Settings"
    case Password = "Password"
    case PrivacyPolicy = "Privacy Policy"
    case FAQ = "FAQ"
    case TermsAndConditions = "Terms & Conditions"
    case TermsOfUse = "Terms Of Use"
    case ContactUs = "Contact Us"
    case FeedBack = "Feedback"
    case AdvertiseWithUs = "Advertise With Us"
    case AboutUs = "About Us"
}
enum MoreList: String {
    case MyUni = "My Uni"
    case MyPost = "My Post"
    case Notification = "Notification"
    case Chats = "Chats"
    case InviteFriends = "Invite Friends"
    case Ads = "Ads"
    case RateGraduation = "Rate Graduation"
    case ReportAnIssue = "Report An Issue"
    case SettingAndPricay = "Settings and Privacy"
    case LogOut = "LOG OUT"
}
enum TimelineViews: String {
    case PostCreation = "Post Creation"
    case PortfolioCreation = "Portfolio Creation"
    case LiveCreation = "Live Creation"
    case EventCreation = "Event Creation"
    case DisocuntCreation = "Discount Creation"
}
enum bottomPopUpType: String {
    case feedback = "Feedback"
    case postas = "PostAs"
    case percentageValue = "Discount Percentage"
    case discountAmt = "Discount Amount"
    case reviewRating = "Review Rating"
}
enum GraduateDetails: String {
    case profile = "Profile"
    case portfolio = "Port Folio"
    case offer = "Offer"
    case gradSpot = "Grad Spot"
    case myPost = "My Post"
    case live = "Live"
}
enum ProfileInfoType {
    case about
    case post
    case chat
    case friends
}
enum MediaTyope {
    case galley
    case camera
    case video
    
}
enum PopOverType {
    case share
    case edit
    case delete
    case report
}
enum ErrorType: String {
    case loginEmptyFields = "LoginError"
    case registerEmptyField = "RegisterFormEmptyFields"
    case loginUnAuthorised = "UnknownCrendential"
    case emailVerification = "EmailVerficationAfterRegistration"
    case termsAndCondition = "TermsAndConditionNotAgreed"
    case emailAuth = "RegisterEmailAuthentication"
    case pohneAuth = "RegisterPhoneAuthentication"
    case passwordValidation = "RegisterPasswordValidation"
    case changePasswordEmpty = "ChangePasswordEmptyFields"
    case passwordChanged = "SuccessFullypasswordChanged"
    case oldPasswordNotMatched = "OldpasswordNotMatched"
    case profileUpdated = "ProfileUpdated"
    case deleteAccout = "DeleteUserAccount"
    case musicDeleted = "MusicDeleted"
    case unFollowOther = "UnfolloOtherUser"
    case somethingWentWrong = "SomethingWentWrong"
    case usernameUpdated = "UsernameUpdated"
    case universityUserName = "UniversityUsernameEmpty"
    case univeristyFieldAllEmpty = "CreateUniversityFieldsAllEmpty"
    case universityCategoryEmplty = "CreateUniversityCategoryTextFieldEmpty"
    case universityRegistered = "UniversityRegistered"
    case profilePictureSelected = "ProfilePictureselected"
    case coverPictureSelected = "CoverPictureSelected"
    case deleteUniversity = "DeleteTheUniversityProfile"
    case portFolioCreated = "PortFolioCretedInUniversity"
    case nullDealDescInDeal = "NilDescriptionWhileCreatngDeal"
    case nullDiscountType = "NillDiscountTypeWhileCreateingDeal"
    case emptyDiscount = "NillDiscountAmountOrPercentage"
    case emptyDate = "Emptydate whileCreatingOffer"
    case offerCreated = "OfferCreatedSuccessfully"
    case dateError = "Date must be higher than todays"
    case eventName = "EventNameIsRequired"
    case createPostEmpty = "WhenAllFieldsAreEmptyWhileCreatingPost"
    case eventCreated = "EventCretedSuccessFully"
    case postCreated = "PostCreatedInUniversity"
    case reported = "PostReportedSuccessfully"
    case unableChat = "UnableToChatWithUser"
    case emptyPostAs = "EmptyPOstAsWhilePosting"
    case deleteComment = "DeleteComment"
    case commentDeleted = "CommentDeleted"
    case postEdited = "PostEdited"
    case portFolioEdited = "PortFolioEdited"
    case spotEdited = "SpotEdited"
    case deletePost = "DeletePost"
    case deletePortFolio = "DeletePortfolio"
    case deleteOffer = "DeleteOffer"
    case offerDeleted = "OfferDeleted"
    case portfolioDeleted = "PortfolioDeleted"
    case deleteEvent = "deleteEvent"
    case uploadDefaultMusic = "uploadDefaultMusic"

    
}
enum ProfileInfoListType {
    case name
    case nickname
    case description
    case mail
    case phoneNum
    case gender
    case profession
    case date
    case address
    case status
    case country
}
enum CreateSpotVCTypes {
    case addPortFolio
    case createDeal
    case createEvent
    case createPost
    case createDealFromOutside
    case createSpotFromOutside
    case createPostFromOutside
    case editPost
    case editSpot
    case editWasLiveVideo
    case editPortFolio
    case editOffer
    
}
enum VideoType: String {
    case liveVideo = "LiveVideo"
    case postVideo = "PostVideo"
}
//MARK:- ENUM USED FOR RATING TVC DELEGATE
enum RatingActionTypes {
    case requestforRating
    case reuestForUnratingPost
}

enum friendListAction {
    case unfriend
    case block
    case unfollow
}

enum interactionType {
    case comment
    case attend
}
