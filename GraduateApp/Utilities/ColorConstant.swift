//
//  ColorConstant.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 6/24/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import Foundation
class Appcolors: NSObject {
    static let  primary = "#A58E52"
    static let light = "#f5f1e5"
    static let placeholder = "#D7DBDD"
    static let light2 = "#E0D7C2"
    static let  text = primary
    static let outlines = primary
    static let buttons = primary
    static let background = light
    static let tabBarSelectedColor = "#575245"
    static let ownMessageBubbleColor = "E8E8E8"
}
