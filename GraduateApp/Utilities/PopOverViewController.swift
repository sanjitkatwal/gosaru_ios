//
//  PopOverViewController.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 8/3/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import UIKit
import Popover
class PopOverViewController: UIViewController {
    var popupTableView = UITableView(frame: CGRect(x: 0, y: 0, width: 90, height: 130))
    var texts = ["Share", "Edit", "Delete"]
    var popover: Popover!
    fileprivate var popoverOptions: [PopoverOption] = [
      .type(.left),
      .blackOverlayColor(UIColor(white: 0.0, alpha: 0.6))
    ]
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    func sharePopOver(sender: UIButton) {
           popupTableView.reloadData()
           if texts.count == 2 {
               popupTableView.frame = CGRect(x: 0, y: 0, width: 90, height: 80)

           } else {
               popupTableView.frame = CGRect(x: 0, y: 0, width: 90, height: 120)

           }
                     popupTableView.delegate = self
                     popupTableView.dataSource = self
                     popupTableView.isScrollEnabled = false
                     self.popover = Popover(options: self.popoverOptions)
                     self.popover.willShowHandler = {
                       print("willShowHandler")
                     }
                     self.popover.didShowHandler = {
                       print("didDismissHandler")
                     }
                     self.popover.willDismissHandler = {
                       print("willDismissHandler")
                     }
                     self.popover.didDismissHandler = {
                       print("didDismissHandler")
                     }
           
                  self.popover.show(popupTableView, fromView: sender)
       }
}
extension PopOverViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return texts.count
    }
    
      func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .default, reuseIdentifier: nil)
                      cell.textLabel?.text = self.texts[(indexPath as NSIndexPath).row]
                   cell.textLabel?.textColor = UIColor.init(hexString: Appcolors.text)
                       return cell
    }
    
    
    
}
extension PopOverViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == popupTableView {
            return 40
        } else {
            return UITableView.automaticDimension
        }

    }
    
}
