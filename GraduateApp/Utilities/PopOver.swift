//
//  PopOver.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 8/27/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import Foundation
import Popover
class ShowPopOver: UIViewController {
   let popupTableView = UITableView(frame: CGRect(x: 0, y: 0, width: 90, height: 120))
    var data: PopOverType?
    let texts = ["Share", "Edit", "Delete"]

        var popover: Popover!
        var popoverOptions: [PopoverOption] = [
             .type(.left),
             .blackOverlayColor(UIColor(white: 0.0, alpha: 0.6))
         ]
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    public func popOver(sender: UIButton) {
        
       
        
   popupTableView.delegate = self
          popupTableView.dataSource = self
        popupTableView.reloadData()
          popupTableView.isScrollEnabled = false
          self.popover = Popover(options: self.popoverOptions)
          self.popover.willShowHandler = {
              print("willShowHandler")
          }
          self.popover.didShowHandler = {
              print("didDismissHandler")
          }
          self.popover.willDismissHandler = {
              print("willDismissHandler")
          }
          self.popover.didDismissHandler = {
              print("didDismissHandler")
          }
        
          self.popover.show(popupTableView, fromView: sender)
    }
    
}
extension ShowPopOver: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return texts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == popupTableView {
         let cell = UITableViewCell(style: .default, reuseIdentifier: nil)
                   cell.textLabel?.text = self.texts[(indexPath as NSIndexPath).row]
                   cell.textLabel?.textColor = UIColor.init(hexString: Appcolors.text)
                   return cell
        } else {
            return UITableViewCell()
        }
    }
}
extension ShowPopOver: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch data {
        case .share:
               print("share")
        case .edit:
              print("edit")
        case .report:
                  print("report")
        case .delete:
                   print("delete")
           
        default:
            break
        }
    }
}
