//
//  ImageExtension.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 6/25/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import Foundation
import UIKit
import SDWebImage
extension UIImage {

    func maskWithColor(color: UIColor) -> UIImage? {
        let maskImage = cgImage!

        let width = size.width
        let height = size.height
        let bounds = CGRect(x: 0, y: 0, width: width, height: height)

        let colorSpace = CGColorSpaceCreateDeviceRGB()
        let bitmapInfo = CGBitmapInfo(rawValue: CGImageAlphaInfo.premultipliedLast.rawValue)
        let context = CGContext(data: nil, width: Int(width), height: Int(height), bitsPerComponent: 8, bytesPerRow: 0, space: colorSpace, bitmapInfo: bitmapInfo.rawValue)!

        context.clip(to: bounds, mask: maskImage)
        context.setFillColor(color.cgColor)
        context.fill(bounds)

        if let cgImage = context.makeImage() {
            let coloredImage = UIImage(cgImage: cgImage)
            return coloredImage
        } else {
            return nil
        }
    }
    
    

}
extension UIImage {
  convenience init?(url: URL?) {
    guard let url = url else { return nil }

    do {
      self.init(data: try Data(contentsOf: url))
    } catch {
      print("Cannot load image from url: \(url) with error: \(error)")
      return nil
    }
  }
}
//extension UIImageView {
//
//    func setImage(imageString: String, localUri: String = "", placeHolderImage: UIImage? = nil, loader: Bool = true,  loaderType : UIActivityIndicatorView.Style = .white) {
//
//        self.sd_setShowActivityIndicatorView(loader)
//        self.sd_setIndicatorStyle(loaderType)
//
//        if let url = URL(string: localUri), let data = try? Data(contentsOf: url), let img = UIImage(data: data) {
//
//            self.sd_setImage(with: URL(string: imageString), placeholderImage: img, options: SDWebImageOptions(), progress: { (progress, total, url) in
//
//            }) { (img, err, type, url) in
//
//            }
//
//
//        } else {
//            self.sd_setImage(with: URL(string: imageString), placeholderImage: placeHolderImage, options: SDWebImageOptions(), progress: { (progress, total, url) in
//
//            }) { (img, err, type, url) in
//
//            }
//        }
//    }
//}
extension UIImageView {
    func applyshadowWithCorner(containerView : UIView, cornerRadious : CGFloat){
        containerView.clipsToBounds = false
        containerView.layer.shadowColor = UIColor.black.cgColor
        containerView.layer.shadowOpacity = 0.5
        containerView.layer.shadowOffset = CGSize.zero
        containerView.layer.shadowRadius = 4
        containerView.layer.cornerRadius = cornerRadious
        containerView.layer.shadowPath = UIBezierPath(roundedRect: containerView.bounds, cornerRadius: cornerRadious).cgPath
        self.clipsToBounds = true
        self.layer.cornerRadius = cornerRadious
    }
}
