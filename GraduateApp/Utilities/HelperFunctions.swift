//
//  HelperFunctions.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 7/6/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import Foundation
import UIKit
class HelperFunctions: NSObject {
   class func addingImageInbtn(ratingBtn: UIButton, messageBtn: UIButton) {
           messageBtn.setImage(UIImage(named: "iconMessage"), for: .normal)
                  messageBtn.contentMode = .center
                  messageBtn.imageView?.contentMode = .scaleAspectFit
                  messageBtn.imageView?.tintColor = UIColor.init(hexString: Appcolors.buttons)
                  ratingBtn.setImage(UIImage(named: "star2"), for: .normal)
                  ratingBtn.contentMode = .center
                  ratingBtn.imageView?.contentMode = .scaleAspectFit
                   ratingBtn.imageView?.tintColor = UIColor.init(hexString: Appcolors.buttons)
       }
    class func addingShadowInImage(avatarImageView:UIImageView, cornerRadius: CGFloat, containerView: UIView) {
        containerView.layer.cornerRadius = cornerRadius
          containerView.layer.shadowColor = UIColor.darkGray.cgColor
          containerView.layer.shadowOffset = CGSize(width: 1, height: 1.0)
       containerView.layer.shadowRadius = 3
       containerView.layer.shadowOpacity = 0.5
       
       avatarImageView.layer.cornerRadius = cornerRadius
       avatarImageView.clipsToBounds = true
       
       containerView.layer.shadowPath = UIBezierPath(roundedRect: avatarImageView.bounds, cornerRadius: cornerRadius).cgPath
    }
    class func addingCentreButtonInNavigation() {
        
    }
    class func addingImageInRightTxtField(textField: UITextField, imageName: String) {
        textField.rightViewMode = .always
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        let image = UIImage(named: imageName)
        imageView.image = image
        imageView.tintColor = UIColor.init(hexString: Appcolors.buttons)
        textField.rightView = imageView
    }
    class func addingImageInArrayTxtField(textField: [UITextField], imageName: String) {
        for item in textField {
        item.rightViewMode = .always
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        let image = UIImage(named: imageName)
        imageView.image = image
        imageView.tintColor = UIColor.init(hexString: Appcolors.buttons)
        item.rightView = imageView
        }
        
      
    }
    class func addingImageInArrayTxtFieldWithSize(textField: [UITextField], imageName: String,width: Int, height: Int) {
           for item in textField {
           item.rightViewMode = .always
           let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: width, height: height))
           let image = UIImage(named: imageName)
           imageView.image = image
           imageView.tintColor = UIColor.init(hexString: Appcolors.buttons)
           item.rightView = imageView
           }
           
         
       }
    class func unhidingViewFromTabBar(height: CGFloat, tableView: UITableView) {
               let adjustForTabbarInsets: UIEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: height, right: 0)
                     tableView.contentInset = adjustForTabbarInsets
                     tableView.scrollIndicatorInsets = adjustForTabbarInsets
           }
    class func ratingSelection(id: Int, starredVlaue: String, reloadingIndex: Int) -> RatingViewController {
        let popupVC = UIStoryboard.timeLineStoryboard.instantiateRatingVC()
               //popupVC.delegate = self
               //popupVC.selectedIndex = 3
        popupVC.id = id
        popupVC.indexToReload = reloadingIndex
        popupVC.selectedIndex = Int(starredVlaue)
       
               popupVC.height = 200
               popupVC.topCornerRadius = 15
               popupVC.presentDuration = 0.25
               popupVC.dismissDuration = 0.25
               popupVC.shouldDismissInteractivelty = true
               // popupVC.popupDelegate = self
    return popupVC
              // present(popupVC, animated: true, completion: nil)
    }
    class func pushingToErrorMsgVC(popUptype: ErrorType,vc: UIViewController, message: String) {
        let vc = UIStoryboard.timeLineStoryboard.instantiateErrorCarMsgPopVC()
        vc.popupType = popUptype
        vc.messageString = message
        let cardPopup = SBCardPopupViewController(contentViewController: vc)
                       cardPopup.show(onViewController: vc)
    }
//   class func convertDateFormat(inputDate: String) -> String {
//
//       func setLabelFromDate(_ date: Date) {
//    let df = DateFormatter()
//    df.dateStyle = .none
//    df.timeStyle = .medium
//    timeTxtField.text = df.string(from: date)
//}
//    }
//    class func addingEyeBtn(textField: UITextField) {
//        let button = UIButton(type: .custom)
//        let image = UIImage(named: "secured")?.withRenderingMode(.alwaysTemplate)
//        button.setImage(image, for: .normal)
//        button.tintColor = UIColor(hexString: "A58E52")
//        button.imageEdgeInsets = UIEdgeInsets(top: 0, left: -16, bottom: 0, right: 0)
//        button.frame = CGRect(x: CGFloat(passwordTxtField.frame.size.width - 25), y: CGFloat(5), width: CGFloat(25), height: CGFloat(25))
//        button.addTarget(self, action: #selector(self.refresh), for: .touchUpInside)
//        textField.rightView = button
//        textField.rightViewMode = .always
//    }
    class func timeInterval(timeAgo:String) -> String
    {
        let dateFormat = "yyyy-MM-dd'T'HH:mm:ss.sZ"
        let df = DateFormatter()

        df.dateFormat = dateFormat
        let dateWithTime = df.date(from: timeAgo)

        let interval = Calendar.current.dateComponents([.year, .month, .day, .hour, .minute, .second], from: dateWithTime!, to: Date())

        if let year = interval.year, year > 0 {
            return year == 1 ? "\(year)" + " " + "year ago" : "\(year)" + " " + "years ago"
        } else if let month = interval.month, month > 0 {
            return month == 1 ? "\(month)" + " " + "month ago" : "\(month)" + " " + "months ago"
        } else if let day = interval.day, day > 0 {
            return day == 1 ? "\(day)" + " " + "day ago" : "\(day)" + " " + "days ago"
        }else if let hour = interval.hour, hour > 0 {
            return hour == 1 ? "\(hour)" + " " + "hour ago" : "\(hour)" + " " + "hours ago"
        }else if let minute = interval.minute, minute > 0 {
            return minute == 1 ? "\(minute)" + " " + "minute ago" : "\(minute)" + " " + "minutes ago"
        }else if let second = interval.second, second > 0 {
            return second == 1 ? "\(second)" + " " + "second ago" : "\(second)" + " " + "seconds ago"
        } else {
            return "a moment ago"

        }
    }
    class func getDate(input: String) -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.sZ"
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.locale = Locale.current
        return dateFormatter.date(from: input) // replace Date String
    }
   class func getDateFromApi(currentDateFormate: String) -> String
        {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = currentDateFormate as String
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.sZ"
           // let finalDate = formatter.dateFromString(convertDate)
        let date = formatter.date(from: currentDateFormate)
            
        let dateString = formatter.string(from: date!)

            return dateString
        }
    class func getCurrentData() -> String {
        let todaydsDate = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/yyyy"
        let result = formatter.string(from: todaydsDate)
        return result
    }
    class func randomTextGenerator(length: Int) -> String {
        let letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
          return String((0..<length).map{ _ in letters.randomElement()! })
    }
//MARK:- C0NVERT IMAGE TO STRING
    public static func  convertImageToBase64String(image : UIImage ) -> String
    {
        let strBase64 =  image.pngData()?.base64EncodedString()
        return strBase64!
    }
    //MARK:- PUSH TO PROFILE FUNC
    class func pushToProfile(id: Int, userType: String, isOwner: Bool, postAsType: String,nav: UINavigationController?) {
            if postAsType == "user" {
                        if isOwner == true {
                                     let vc = UIStoryboard.timeLineStoryboard.instantiateUserProfileVC()
                            nav?.pushViewController(vc, animated: true)
                                     } else {
                                         let vc = UIStoryboard.timeLineStoryboard.instantiateOtherUserProfileVC()
                                   vc.userId = id
                                        nav?.pushViewController(vc, animated: true)
                                     }
                        } else {
                            let vc = UIStoryboard.timeLineStoryboard.instantiateGraduateDetailsViewController()
                            vc.businessId = id
                            nav?.pushViewController(vc, animated: true)
                        }
        }
     //MARK:- THOUGTS DETAILS
    class func thoughtsDetails(commentId: Int, index: IndexPath!,nav: UINavigationController?,vc: UIViewController) -> UIViewController {
       let vc = UIStoryboard.timeLineStoryboard.instantiateThoughtsVC()
       vc.commentId = commentId
       vc.index = index
      //  vc.delegate = vc
        return vc
   }
    
}

extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }

    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}
