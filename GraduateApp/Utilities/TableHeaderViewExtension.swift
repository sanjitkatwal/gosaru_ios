//
//  TableHeaderViewExtension.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 8/7/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import Foundation
import UIKit
extension UITableView {

//Variable-height UITableView tableHeaderView with autolayout
func layoutTableHeaderView() {

guard let headerView = self.tableHeaderView else { return }
headerView.translatesAutoresizingMaskIntoConstraints = false

let headerWidth = headerView.bounds.size.width;
    let temporaryWidthConstraints = NSLayoutConstraint.constraints(withVisualFormat: "[headerView(width)]", options: NSLayoutConstraint.FormatOptions(rawValue: UInt(0)), metrics: ["width": headerWidth], views: ["headerView": headerView])

headerView.addConstraints(temporaryWidthConstraints)

headerView.setNeedsLayout()
headerView.layoutIfNeeded()

    let headerSize = headerView.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize)
let height = headerSize.height
var frame = headerView.frame

frame.size.height = height
headerView.frame = frame

self.tableHeaderView = headerView

headerView.removeConstraints(temporaryWidthConstraints)
headerView.translatesAutoresizingMaskIntoConstraints = true

}
}
