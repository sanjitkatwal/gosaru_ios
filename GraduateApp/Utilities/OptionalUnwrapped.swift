//
//  OptionalUnwrapped.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 8/17/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import Foundation
extension Optional where Wrapped: CustomStringConvertible {
    var optionalUnWrapped: String {
        switch self {
        case .none:
            return "NA"
        case let .some(wrappedValue):
            return wrappedValue.description
        }
    }
}

