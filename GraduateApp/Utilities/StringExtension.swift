//
//  StringExtension.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 7/8/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import Foundation
import UIKit
extension String {
    static func changinColor(text: String, color: String) -> NSMutableAttributedString {
        let value: NSMutableAttributedString?
        let color = UIColor.init(hexString: color)
        let textToFind = "redword"
                
        let attrsString =  NSMutableAttributedString(string: text);
                
        // search for word occurrence
        let range = (text as NSString).range(of: textToFind)
        if (range.length > 0) {
            attrsString.addAttribute(NSAttributedString.Key.foregroundColor,value:color,range:range)
        }
                
        // set attributed text
        value = attrsString
        return value!
    }
    
     
}

extension String {
  func removeSpaceFromString () -> String {
    return self.replacingOccurrences(of: " ", with: "")
  }
  var isEmptyStr: Bool {
    return self.trimmingCharacters(in: NSCharacterSet.whitespaces).isEmpty
  }
  var isValidEmail: Bool {
    let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
    let emailTest = NSPredicate(format: "SELF MATCHES %@", emailRegEx)
    return emailTest.evaluate(with: self)
  }
  var removeSpace: String {
    let okayChars = Set("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ1234567890+=().!_")
    return self.filter {okayChars.contains($0) }
  }
  var htmlToAttributedString: NSAttributedString? {
    guard let data = data(using: .utf8) else { return NSAttributedString() }
    do {
      return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)
    } catch {
      return NSAttributedString()
    }
  }
  var htmlToString: String {
    return htmlToAttributedString?.string ?? ""
  }
  private func matches(pattern: String) -> Bool {
    let regex = try! NSRegularExpression(
      pattern: pattern,
      options: [.caseInsensitive])
    return regex.firstMatch(
      in: self,
      options: [],
      range: NSRange(location: 0, length: utf16.count)) != nil
  }
  func isValidURL() -> Bool {
    guard let url = URL(string: self) else { return false }
    if !UIApplication.shared.canOpenURL(url) {
      return false
    }
    let urlPattern = "^(http|https|ftp)\\://([a-zA-Z0-9\\.\\-]+(\\:[a-zA-Z0-9\\.&amp;%\\$\\-]+)*@)*((25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9])\\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|"
    let urlPattern2 = "[0-9])|localhost|([a-zA-Z0-9\\-]+\\.)*[a-zA-Z0-9\\-]+\\.(com|edu|gov|int|mil|net|org|biz|arpa|info|name|pro|aero|coop|museum|[a-zA-Z]{2}))(\\:[0-9]+)*(/($|[a-zA-Z0-9\\.\\,\\?\\'\\\\\\+&amp;%\\$#\\=~_\\-]+))*$"
    return self.matches(pattern: urlPattern + urlPattern2)
  }
}

