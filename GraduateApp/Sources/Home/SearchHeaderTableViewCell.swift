//
//  SearchHeaderTableViewCell.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 12/9/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import UIKit

class SearchHeaderTableViewCell: UITableViewCell {

    @IBOutlet var titleLbel: UILabel!
    @IBOutlet var headerView: CustomView!
    @IBOutlet var mainImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        headerView.layer.cornerRadius = 12
        headerView.layer.maskedCorners = [.layerMaxXMinYCorner,.layerMinXMinYCorner]
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        headerView.roundCorners(corners: [.topLeft, .topRight], radius: 4.0)
    }
    
}
