//
//  FooterTableViewCell.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 12/9/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import UIKit

class FooterTableViewCell: UITableViewCell {
    @IBOutlet var footerView: CustomView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
//        footerView.removeBorder(toSide: .Top)
//        footerView.addBorder(toSide: .Left, withColor: UIColor.lightGray.cgColor, andThickness: 0.5)
//        footerView.addBorder(toSide: .Right, withColor: UIColor.lightGray.cgColor, andThickness: 0.5)
//        footerView.addBorder(toSide: .Bottom, withColor: UIColor.lightGray.cgColor, andThickness: 0.5)
        footerView.layer.cornerRadius = 12
        footerView.layer.maskedCorners = [.layerMinXMaxYCorner,.layerMaxXMaxYCorner]
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        
    }
}
