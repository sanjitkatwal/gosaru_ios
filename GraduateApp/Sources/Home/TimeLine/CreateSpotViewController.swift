//
//  CreateSpotViewController.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 6/29/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import UIKit
import iOSDropDown
import YYCalendar
import MobileCoreServices
import AVFoundation
import GooglePlaces
import Alamofire
struct CreateDealOutside {
    var data: GraduateUniversityResponseModel?
    var selectedCell: Int?
    var selfAdPoster: Bool
}
struct PostFromInside {
    var data: UniversityData?
    var selectedCell: Int?
    var selfAdPoster: Bool
}
protocol ActionFromCreateSpotVC: class {
    func reloadTableViewData()
    func reloadWithType(type: ErrorType)
}
class CreateSpotViewController: UIViewController {
    
    @IBOutlet var discountAmtTxtField: UITextField!
    
    @IBOutlet var txtFieldStackView: UIStackView!
    @IBOutlet var discountPercentageTxtField: UITextField!
    @IBOutlet var postBtnsStackView: UIStackView!
    @IBOutlet var nameTxtField: UITextField!
    @IBOutlet var nameView: CustomView!
    @IBOutlet var footerPostBtn: UIButton!
    @IBOutlet var postStackView: UIStackView!
    @IBOutlet var descView: CustomView!
    @IBOutlet weak var updateBtn: AnimatedButton!
    @IBOutlet weak var postAsStackView: UIStackView!
    @IBOutlet weak var termsAndCoditonTxtView: UITextView!
    @IBOutlet weak var termsAndConditionTxtView: CustomView!
    @IBOutlet weak var discountAmtView: CustomView!
    @IBOutlet weak var discountTypeTxtField: DropDown!
    @IBOutlet weak var seenBtn: UIButton!
    @IBOutlet weak var midStackView: UIStackView!
    @IBOutlet weak var seenStackView: UIStackView!
    @IBOutlet weak var endDateView: CustomView!
    @IBOutlet weak var timeView: CustomView!
    @IBOutlet weak var dateView: CustomView!
    @IBOutlet weak var addressView: CustomView!
    @IBOutlet weak var eventNameView: CustomView!
    @IBOutlet weak var percentValueView: CustomView!
    @IBOutlet weak var discountPercentView: CustomView!
    @IBOutlet weak var topTxtView: UITextView!
    @IBOutlet weak var addressTxtField: UITextField!
    @IBOutlet weak var postAs: AnimatedButton!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var endDateTxtField: UITextField!
    @IBOutlet weak var timeTxtField: UITextField!
    @IBOutlet weak var dateTxtField: UITextField!
    @IBOutlet weak var eventTxtField: UITextField!
    var postFromInside: PostFromInside?
    var seenByEveryOne = false
    var editData: uniMyPostData?//data which user wants to edit comes from other vc
    var userID: Int?
    var businesName: String?
    var businessProfilePic: String? {
        didSet {
            addingDataInEventDataSource()
        }
       
    }
    var profileData: CreateUniversityResponseModel?
    var businesId: Int? {
        didSet {
            posterId = "\(businesId ?? 0)" + ":business"
        }
    }
    var reloadDataAfterApiCall: Bool?
    var profileImage: String?
    var universityName: String?
    var postersIdArray: [Int]? = []
    var posterDic: [[String : Any]]? = [[String : Any]]()
    var posterIdsFromInside: [[String : Any]]? = [[String : Any]]()//holds ids for posting from inside
    var posterId: String?  //id required to make a create spot and deal
    var imagesDataSource: ImagesData? = ImagesData() {
        didSet {
            collectionView.reloadData()
        }
    }
    weak var delegate: ActionFromCreateSpotVC?
    var callbackClosure: (() -> Void)?
    var currentTab: AZTabBarController?
    var controllerType: String?
    var multipleSelectedValues: [String]? = []
    var camImage = UIImage()
    let timePicker = UIDatePicker()
    var vcType: CreateSpotVCTypes?
    var postasData: CreateDealOutside?
    var postAsUniData: GraduateUniversityResponseModel?
    var createEventData: CreateEventModel?
    var postFromInsideData: UniversityData?
    var imagePickerController = CustomImagePicker()
    override func viewDidLoad() {
        super.viewDidLoad()
    
        //self.tabBarController?.tabBar.isHidden = true
        self.title = "Create Post"
       print(profileData)
        if controllerType == "Create Deal" {
            self.title = "Create Deal"
            timeView.isHidden = true
            endDateView.isHidden = true
            eventNameView.isHidden = true
            discountAmtView.isHidden = true
            percentValueView.isHidden = true
            eventNameView.isHidden = true
            self.navigationController?.isNavigationBarHidden = false
            termsAndConditionTxtView.isHidden = false
            updateBtn.isHidden = true
            postStackView.isHidden = false
        }  else if controllerType == "Create Post" {
            midStackView.isHidden = true
            topTxtView.text = "Write in timeline"
            topTxtView.textAlignment = .center
            updateBtn.isHidden = true
            postStackView.isHidden = false
        }  else if controllerType == "Edit Post" {
            midStackView.isHidden = true
            topTxtView.text = "Write in timeline"
            topTxtView.textAlignment = .center
            postStackView.isHidden = true
            updateBtn.isHidden = false
        } else {
            termsAndConditionTxtView.isHidden = true
            discountAmtView.isHidden = true
            discountPercentView.isHidden = true
            percentValueView.isHidden = true
            updateBtn.isHidden = true
            postStackView.isHidden = false
        }
        addingDelegateToTxtField()
        eventTxtField.enableMode = .disabled
        topTxtView.delegate = self
        discountPercentageTxtField.delegate = self
        termsAndCoditonTxtView.delegate = self
        
        
        discountTypeTxtField.enableMode = .disabled
        
        collectionView.dataSource = self
        collectionView.delegate = self
        
        self.imagePickerController.delegate = self
        
        // Do any additional setup after loading the view.
        HelperFunctions.addingImageInRightTxtField(textField: discountTypeTxtField, imageName: "iconArrowDown")
        managingViewAccordingToTypes()
        
    }
    //    override func viewWillLayoutSubviews() {
    //        super.viewWillLayoutSubviews()
    //        imagePickerController.navigationBar.topItem?.rightBarButtonItem?.tintColor = .black
    //        imagePickerController.navigationBar.topItem?.rightBarButtonItem?.isEnabled = true
    //    }
    override func viewWillAppear(_ animated: Bool) {
        
        HelperFunctions.addingImageInArrayTxtField(textField: [eventTxtField,dateTxtField,timeTxtField], imageName: "iconArrowDown")
        navigationController?.isNavigationBarHidden = false
        currentTabBar?.setBar(hidden: true, animated: true)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        callbackClosure?()
        if reloadDataAfterApiCall == true {
            delegate?.reloadTableViewData()
        }
      
    }
    func addingDelegateToTxtField() {
        dateTxtField.delegate = self
        timeTxtField.delegate = self
        discountTypeTxtField.delegate = self
        eventTxtField.delegate = self
        endDateTxtField.delegate = self
        addressTxtField.delegate = self
    }
//MARK:- TIMEPICKER
    func openTimePicker()  {
        
        let timeSelector = TimeSelector()
        timeSelector.timeSelected = {
            (timeSelector) in
            self.setLabelFromDate(timeSelector.date)
        }
        timeSelector.overlayAlpha = 0.8
        timeSelector.clockTint = timeSelector_rgb(0, 230, 0)
        timeSelector.minutes = 30
        timeSelector.hours = 5
        timeSelector.isAm = false
        timeSelector.presentOnView(view: self.view)
        
    }
    func setLabelFromDate(_ date: Date) {
        let df = DateFormatter()
        df.dateStyle = .none
        df.timeStyle = .medium
        timeTxtField.text = df.string(from: date)
    }
    //MARK:- MANAGING VIEWS
    func managingViewAccordingToTypes() {
        switch vcType {
        case .addPortFolio:
            self.title = "Create PortFolio"
            postStackView.isHidden = false
            descView.isHidden = false
            discountAmtView.isHidden = true
            discountPercentView.isHidden = true
            percentValueView.isHidden = true
            nameView.isHidden = false
            addressView.isHidden = false
            eventNameView.isHidden = false
            dateView.isHidden = false
            timeView.isHidden = true
            endDateView.isHidden = true
            termsAndCoditonTxtView.isHidden = true
            seenStackView.isHidden = true
            updateBtn.isHidden = true
            postBtnsStackView.isHidden = true
            nameTxtField.placeholder = "Name"
            topTxtView.text = "Description"
            eventTxtField.placeholder = "Select Graduation Type"
        case .createDeal:
            discountPercentView.isHidden = false
            topTxtView.text = "Write a deal description *"
            dateTxtField.placeholder = "Expiry Date *"
            self.title = "Create Deal"
            discountAmtView.isHidden = true
            percentValueView.isHidden = true
            eventNameView.isHidden = true
            nameView.isHidden = true
            timeView.isHidden = true
            endDateView.isHidden = true
            seenStackView.isHidden = true
            updateBtn.isHidden = true
            footerPostBtn.isHidden = true
            termsAndConditionTxtView.isHidden = false
            addingDataInEventDataSource()
        case .createEvent:
            self.title = "Create Spot"
            eventNameView.isHidden = false
            nameView.isHidden = false
            dateView.isHidden = false
            timeView.isHidden = false
            endDateView.isHidden = false
            discountPercentView.isHidden = true
            discountAmtView.isHidden = true
            percentValueView.isHidden = true
            seenStackView.isHidden = true
            updateBtn.isHidden = true
            footerPostBtn.isHidden = true
            topTxtView.text = "Event Description..."
            nameTxtField.placeholder = "Event Name*"
            eventTxtField.placeholder = "Event types*"
            timeTxtField.placeholder = "Select Time"
            endDateTxtField.placeholder = "End Date"
            addingDataInEventDataSource()
        case .createPost:
            midStackView.isHidden = true
            topTxtView.text = "Write in timeline"
            getUserUniversity()///get the uuniversities to select in post as
            topTxtView.textAlignment = .center
            updateBtn.isHidden = true
            postStackView.isHidden = false
            topTxtView.text = "Write in timeline"
            topTxtView.textColor = .lightGray
            topTxtView.textAlignment = .center
            txtFieldStackView.isHidden = true
            footerPostBtn.isHidden = true
            addingDataInEventDataSource()
        case .createPostFromOutside:
            midStackView.isHidden = true
            topTxtView.text = "Write in timeline"
            getUserUniversity()///get the uuniversities to select in post as
            topTxtView.textAlignment = .center
            updateBtn.isHidden = true
            postStackView.isHidden = false
            topTxtView.text = "Write in timeline"
            topTxtView.textColor = .lightGray
            topTxtView.textAlignment = .center
            txtFieldStackView.isHidden = true
            footerPostBtn.isHidden = true
           // addingDataInEventDataSource()
            getUserUniversity()
            addingDataInPostAs()
        case .createDealFromOutside:
            discountPercentView.isHidden = false
            topTxtView.text = "Write a deal description *"
            dateTxtField.placeholder = "Expiry Date *"
            self.title = "Create Deal"
            discountAmtView.isHidden = true
            percentValueView.isHidden = true
            eventNameView.isHidden = true
            nameView.isHidden = true
            timeView.isHidden = true
            endDateView.isHidden = true
            seenStackView.isHidden = true
            updateBtn.isHidden = true
            footerPostBtn.isHidden = true
            termsAndConditionTxtView.isHidden = false
            footerPostBtn.isHidden = true
            getUserUniversity()
            addingDataInPostAs()
        case .createSpotFromOutside:
            self.title = "Create Spot"
            eventNameView.isHidden = false
            nameView.isHidden = false
            dateView.isHidden = false
            timeView.isHidden = false
            endDateView.isHidden = false
            discountPercentView.isHidden = true
            discountAmtView.isHidden = true
            percentValueView.isHidden = true
            seenStackView.isHidden = true
            updateBtn.isHidden = true
            footerPostBtn.isHidden = true
            topTxtView.text = "Event Description..."
            nameTxtField.placeholder = "Event Name*"
            eventTxtField.placeholder = "Event types*"
            timeTxtField.placeholder = "Select Time"
            endDateTxtField.placeholder = "End Date"
            termsAndConditionTxtView.isHidden = true
            getUserUniversity()
            addingDataInPostAs()
            footerPostBtn.isHidden = true
        case .editPost:
            self.title = "Edit Post"
            midStackView.isHidden = true
            if editData?.message != nil {
                topTxtView.text = editData?.message
                topTxtView.textColor = UIColor.init(hexString: Appcolors.text)
            } else {
                topTxtView.text = "Write in timeline"
                topTxtView.textColor = .lightGray
            }
            updateBtn.isHidden = true
            postStackView.isHidden = false
            topTxtView.textAlignment = .center
            txtFieldStackView.isHidden = true
            addingDataInEventDataSource()
            postBtnsStackView.isHidden = true
            footerPostBtn.isHidden = true
            updateBtn.isHidden = false
            if (editData?.galleries?.count ?? 0) > 0 {
                let filrurls = NSURL(string: (GraduateUrl.imageUrl + (editData?.galleries?[0].filepath ?? "")))
                imagesDataSource?.videos?.append(filrurls! as URL)
                collectionView.reloadData()
            }
           
        case .editSpot:
            self.title = "Edit Spot"
            eventNameView.isHidden = false
            nameView.isHidden = false
            dateView.isHidden = false
            timeView.isHidden = false
            endDateView.isHidden = false
            discountPercentView.isHidden = true
            discountAmtView.isHidden = true
            percentValueView.isHidden = true
            seenStackView.isHidden = true
            updateBtn.isHidden = true
            footerPostBtn.isHidden = true
            topTxtView.text = "Event Description..."
            nameTxtField.placeholder = "Event Name*"
            eventTxtField.placeholder = "Event types*"
            timeTxtField.placeholder = "Select Time"
            endDateTxtField.placeholder = "End Date"
            termsAndConditionTxtView.isHidden = true
            addingDataInPostAs()
            postAsStackView.isHidden = true
            updateBtn.isHidden = true
            footerPostBtn.isHidden = false
            nameTxtField.text = editData?.event?.eventTitle
            addressTxtField.text = editData?.event?.addressString
            eventTxtField.text = editData?.event?.eventType
            timeTxtField.text = editData?.event?.eventStartTime
            dateTxtField.text = editData?.event?.eventStartDate
            if editData?.event?.descriptionValue == nil {
                topTxtView.text = "Event Description..."
                topTxtView.textColor = .lightGray
            } else {
                topTxtView.text = editData?.event?.descriptionValue
            }
            endDateTxtField.text = editData?.event?.eventEndDate
        case .editWasLiveVideo:
            self.title = "Edit Spot"
            midStackView.isHidden = true
            topTxtView.textAlignment = .center
            updateBtn.isHidden = true
            postStackView.isHidden = false
            topTxtView.textAlignment = .center
            txtFieldStackView.isHidden = true
            addingDataInEventDataSource()
            postBtnsStackView.isHidden = true
            footerPostBtn.isHidden = true
            updateBtn.isHidden = false
            if (editData?.galleries?.count ?? 0) > 0 {
                let filrurls = NSURL(string: (GraduateUrl.imageUrl + (editData?.galleries?[0].filepath ?? "")))
                imagesDataSource?.videos?.append(filrurls! as URL)
                collectionView.reloadData()
            }
        case .editPortFolio:
            self.title = "Edit PortFolio"
            postStackView.isHidden = false
            descView.isHidden = false
            discountAmtView.isHidden = true
            discountPercentView.isHidden = true
            percentValueView.isHidden = true
            nameView.isHidden = false
            addressView.isHidden = false
            eventNameView.isHidden = false
            dateView.isHidden = false
            timeView.isHidden = true
            endDateView.isHidden = true
            termsAndCoditonTxtView.isHidden = true
            seenStackView.isHidden = true
            updateBtn.isHidden = true
            postBtnsStackView.isHidden = true
            postAsStackView.isHidden = true
            footerPostBtn.isHidden = true
            updateBtn.isHidden = false
            topTxtView.text = editData?.portfolio?.descriptionValue
            topTxtView.textColor = UIColor.init(hexString: Appcolors.text)
            nameTxtField.text = editData?.portfolio?.name
            nameTxtField.textColor = UIColor.init(hexString: Appcolors.text)
            addressTxtField.text = editData?.portfolio?.addressString
            addressTxtField.textColor = UIColor.init(hexString: Appcolors.text)
            eventTxtField.text = editData?.portfolio?.business?.businessType
            eventTxtField.textColor = UIColor.init(hexString: Appcolors.text)
            dateTxtField.text = editData?.portfolio?.date
            dateTxtField.textColor = UIColor.init(hexString: Appcolors.text)
//            nameTxtField.placeholder = "Name"
//            eventTxtField.placeholder = "Select Graduation Type"
        case .editOffer:
            discountPercentView.isHidden = false
            topTxtView.text = "Write a deal description *"
            dateTxtField.placeholder = "Expiry Date *"
            self.title = "Create Deal"
            discountAmtView.isHidden = true
            percentValueView.isHidden = true
            eventNameView.isHidden = true
            nameView.isHidden = true
            timeView.isHidden = true
            endDateView.isHidden = true
            seenStackView.isHidden = true
            updateBtn.isHidden = true
            footerPostBtn.isHidden = true
            termsAndConditionTxtView.isHidden = false
            topTxtView.text = editData?.message
            topTxtView.textColor = UIColor.init(hexString: Appcolors.text)
            discountPercentView.isHidden = false
            if editData?.offer?.discountAmount == nil {
                discountAmtView.isHidden = true
            } else {
                discountAmtView.isHidden = false
                discountAmtTxtField.text = editData?.offer?.discountAmount
                discountTypeTxtField.text = "Discount Amount"
            }
            if editData?.offer?.discountPercentage == nil {
                discountPercentView.isHidden = true
            } else {
                discountPercentView.isHidden = false
                discountPercentageTxtField.text = editData?.offer?.discountPercentage
                discountTypeTxtField.text = "Discount Percentage"
            }
            addressTxtField.text = editData?.offer?.addressString
            addressTxtField.textColor = UIColor.init(hexString: Appcolors.text)
            endDateTxtField.text = editData?.offer?.expiryDate
            endDateTxtField.textColor = UIColor.init(hexString: Appcolors.text)
            endDateView.isHidden = false
            termsAndCoditonTxtView.text = editData?.offer?.terms
            termsAndCoditonTxtView.textColor = UIColor.init(hexString: Appcolors.text)
            dateView.isHidden = true
        default:
            break
        }
    }
   
    
    
//MARK:- REQUEST FOR USER UNIVERSITIES
    func getUserUniversity() {
           GraduateUniversityResponse.requestToGetUserUni { [weak self](response) in
               guard let strongSelf = self else { return }
               print(response)
               strongSelf.postAsUniData = response
            if case .createPostFromOutside = strongSelf.vcType {
                strongSelf.addingDataInPostAs()
            } else if case .createSpotFromOutside = strongSelf.vcType {
                strongSelf.addingDataInPostAs()
            } else if case .createDealFromOutside = strongSelf.vcType {
                strongSelf.addingDataInPostAs()
            }
           }
}
    func popUp(type: ErrorType, message: String) {
        let vc = UIStoryboard.timeLineStoryboard.instantiateErrorCarMsgPopVC()
        vc.popupType = type
        vc.messageString = message
        let cardPopup = SBCardPopupViewController(contentViewController: vc)
                       cardPopup.show(onViewController: self)
    }
//MARK:- ADDING DATA FOR POST AS
    func addingDataInEventDataSource() {
        print(businesId,businesName,businessProfilePic)
        if case .createDeal = vcType {
            createEventData = CreateEventModel(data: [EventModel(name: profileData?.data?.businessName, profileImage: profileData?.data?.businessProfileImage, id: profileData?.data?.internalIdentifier, isSelected: true)])
        } else {
        createEventData = CreateEventModel(data: [EventModel(name: UserDefaultsHandler.getUDValue(key: .fullName) as? String, profileImage: UserDefaultsHandler.getUDValue(key: .profileImage) as? String, id: UserDefaultsHandler.getUDValue(key: .userID) as? Int, isSelected: false),EventModel(name: profileData?.data?.businessName, profileImage: profileData?.data?.businessProfileImage, id: profileData?.data?.internalIdentifier, isSelected: true)])
        print(createEventData)
        }
//        createEventData = CreateEventModel(data: [EventModel(name: UserDefaultsHandler.getUDValue(key: .fullName) as? String, profileImage: UserDefaultsHandler.getUDValue(key: .profileImage) as? String, id: UserDefaultsHandler.getUDValue(key: .userID) as? Int), EventModel(name: businesName, profileImage: businessProfilePic, id: businesId)], selectedCell: 1)
    }
//MARK:- ADDING POSTAS DATA
    func addingDataInPostAs() {
       
        postasData = CreateDealOutside(data: postAsUniData, selectedCell: nil, selfAdPoster: true)
    }
    @IBAction func updateAction(_ sender: Any) {
        
        if case .editPost =  vcType {
            let paramss: [String : Any] = ["message" : topTxtView.text ?? "", "_method" : "PUT","visibility_type" : "public"]
            //abd(params: params)
            CreateSpotResponse.requestUpdate(param: paramss, url: GraduateUrl.baseURL + "post/" + "\(editData?.internalIdentifier ?? 0)", imageName: "images[]", videos: imagesDataSource?.videos ?? [], multipleImages: imagesDataSource?.images ?? [], showHud: true) { [weak self](response) in
                guard let strongSelf = self else { return }
                strongSelf.popUp(type: .postEdited, message: "")
            }
        }
        if case .editPortFolio = vcType {
            if eventTxtField.text == "" || addressTxtField.text == "" || eventTxtField.text == "" || dateTxtField.text == "" {
                popUp(type: .registerEmptyField, message: "")
            } else {
                let paramss: [String : Any] = ["description" : topTxtView.text ?? "", "_method" : "PUT","visibility_type" : "public","type" : eventTxtField.text ?? "", "date" : dateTxtField.text ?? "", "address_string" : addressTxtField.text ?? "", "name" : nameTxtField.text ?? ""]
                CreateSpotResponse.requestUpdate(param: paramss, url: (GraduateUrl.updatePortFolioURL + "\(editData?.portfolio?.internalIdentifier ?? 0)"), imageName: "images[]", videos: imagesDataSource?.videos ?? [], multipleImages: imagesDataSource?.images ?? []) { [weak self](response) in
                    guard let strongSelf = self else { return }
                    strongSelf.popUp(type: .portFolioEdited, message: "")
            }
        }
        }
//        if imagesDataSource?.ima ges.count == 0 || imagesDataSource?.videos?.count == 0 {
//            let alert = UIAlertController(title: "", message: "Please select an image or video", preferredStyle: UIAlertController.Style.alert)
//            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
//            self.present(alert, animated: true, completion: nil)
//        } else {
//            let vc = UIStoryboard.timeLineStoryboard.instantiateDeletePostPopUpVC()
//            vc.popUpType = "Post Edit"
//            vc.delegate = self
//            let cardPopup = SBCardPopupViewController(contentViewController: vc)
//            
//            cardPopup.show(onViewController: self)
//        }
    }
//    func abd(params: [String : Any]) {
//        ImageUploaderFinal.uploadImage(url: GraduateUrl.baseURL + "post/" + "\(editData?.internalIdentifier)", videoName: "videos[]", videos: imagesDataSource?.videos, imageName: "images[]", parameters: params as? [String : Any], imagesDataSource: imagesDataSource?.images, showHUD: true) { (response) in
//
//        } completionError: {
//
//        }
//
//    }
    
    
    @IBAction func addMediaAction(_ sender: Any) {
        bottomSelection(type: "Add Media", height: 150, data: CreateEventModel())
        //        let pictureMessage = "Take Picture"
        //
        //        let optionMenu = UIAlertController(title: nil, message: "Choose Option", preferredStyle: .actionSheet)
        //
        //        let picAction = UIAlertAction(title: "Take Picture", style: .default, handler:
        //        {
        //            (alert: UIAlertAction!) -> Void in
        //            self.openCamera()
        //        })
        //        let imagesAction = UIAlertAction(title: "Select Images", style: .default, handler:
        //        {
        //            (alert: UIAlertAction!) -> Void in
        //            self.getImage(fromSourceType: .photoLibrary, mediaType: "public.image")
        //        })
        //        let videoAction = UIAlertAction(title: "Select Video", style: .default, handler:
        //        {
        //            (alert: UIAlertAction!) -> Void in
        //            self.getImage(fromSourceType: .photoLibrary, mediaType: "public.movie")
        //        })
        //        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler:
        //               {
        //                   (alert: UIAlertAction!) -> Void in
        ////                   self.getImage(fromSourceType: .photoLibrary, mediaType: "public.movie")
        //               })
        //        optionMenu.addAction(picAction)
        //        optionMenu.addAction(imagesAction)
        //        optionMenu.addAction(videoAction)
        //         optionMenu.addAction(cancelAction)
        //        self.present(optionMenu, animated: true, completion: nil)
    }
    @IBAction func footerPostAction(_ sender: Any) {
        if case .editSpot = vcType {
            let param: [String : Any] = ["_method" : "PUT","description" : topTxtView.text ?? "","event_title" : nameTxtField.text ?? "","event_type" : eventTxtField.text ?? "", "event_start_date" : dateTxtField.text ?? "","event_end_date" : endDateTxtField.text ?? "",  "visibility_type" : "public", "event_start_time" : timeTxtField.text ?? ""]
            CreateSpotResponse.requestUpdate(param: param, url: (GraduateUrl.baseURL + "graduate_spot/" + "\(editData?.event?.internalIdentifier ?? 0)"), imageName: "images[]", videos: imagesDataSource?.videos ?? [], multipleImages: imagesDataSource?.images ?? [], showHud: true) { [weak self](response) in
                guard let strongSelf = self else { return }
                strongSelf.popUp(type: .spotEdited, message: "")
            }
        } else {
        if topTxtView.text == "" && nameTxtField.text == "" && addressTxtField.text == "" && eventTxtField.text == "" && dateTxtField.text == "" {
            popUp(type: .registerEmptyField, message: "")
        } else {
            let param: [String : Any] = ["name" : nameTxtField.text ?? "", "description" : topTxtView.text ?? "", "address_string" : addressTxtField.text ??
                "", "type" : eventTxtField.text ?? "" ,"date" : dateTxtField.text ?? ""]
            let url = (GraduateUrl.universityDetalsURL + "\(userID ?? 0)" + "/add/portfolio")
//MARK:- PORTFOLIO CREATE API
            requestApi(param: param, url: url, type: .portFolioCreated)
        }
        
    }
    }
    
    //MARK:- REQUEST API
    func requestApi(param: [String: Any],url: String, type: ErrorType) {
        
        print(url)
        var header: [String : Any]
        if let token = UserDefaultsHandler.getUDValue(key: UDkey.token) as? String {
            header = ["Authorization": "Bearer \(token)", "Accept": "application/json"]
        } else {
            header = ["Accept": "application/json"]
            
        }
        Alamofire.upload(multipartFormData: { multipartFormData in
            // import image to request
            
            ProgressHud.showProgressHUD()
            if self.imagesDataSource?.images != nil {
                for imageData in self.imagesDataSource!.images {
                    let data = imageData.jpegData(compressionQuality: 0.9)
                    multipartFormData.append(data!, withName: "images[]", fileName: "\(Date().timeIntervalSince1970).jpeg", mimeType: "image/jpeg")
                    
                }
            }
            
            for (key, value) in param {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
        }, to: url, method: .post , headers: (header as! HTTPHeaders) ,
           
           encodingCompletion: { encodingResult in
            
            switch encodingResult {
            case .success(let upload, _, _):
                upload.responseJSON { [weak self]response in
                    guard let strongSelf = self else { return }
                    ProgressHud.hideProgressHUD()
                    strongSelf.popUp(type: type, message: "")
                    print(response)
                }
            case .failure(let error):
                ProgressHud.hideProgressHUD()
                print(error)
            }
            
        })
    }
    func getImage(fromSourceType sourceType: UIImagePickerController.SourceType, mediaType: String) {
        
        //Check is source type available
        
        
        
        imagePickerController.sourceType = sourceType
        imagePickerController.mediaTypes = [mediaType]
        imagePickerController.delegate = self
        imagePickerController.modalPresentationStyle = .overCurrentContext
        
        
        self.present(imagePickerController, animated: true, completion: nil)
        
    }
    func openCamera() {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera) {
            
            imagePickerController.delegate = self
            imagePickerController.sourceType = UIImagePickerController.SourceType.camera
            imagePickerController.allowsEditing = false
            self.present(imagePickerController, animated: true, completion: nil)
        } else {
            print("Your dont have a camera")
        }
    }
    
    //MARK:- CREATE PORTFOLIO API
    func createPortFolio() {
        //        let url = (GraduateUrl.universityDetalsURL + "\(userID ?? 0)" + "/add/portfolio")
        //        CreateSpotResponse.requestToCreatePortFolio(url: url) { (response) in
        //
        //        }
    }
    func bottomSelection(type: String, height: Int, data: CreateEventModel) {
        let popupVC = UIStoryboard.timeLineStoryboard.instantiateSelectionBottomPopUpVC()
        popupVC.delegate = self
        popupVC.textFieldType = type
        popupVC.height = CGFloat(height)
        popupVC.profileImage = profileImage
        popupVC.uniName = universityName
        popupVC.createEventPostAs = data
        print(postasData?.selectedCell)
        popupVC.dealPostAsData = postasData
        popupVC.topCornerRadius = 15
        popupVC.presentDuration = 0.25
        popupVC.dismissDuration = 0.25
        popupVC.shouldDismissInteractivelty = true
        popupVC.postAsData = profileData
        popupVC.universityList = postAsUniData
        // popupVC.popupDelegate = self
        present(popupVC, animated: true, completion: nil)
    }
//MARK:- SELECTION BOTTOM WITH TEXTFIELD TYPE
    func presentBottomSelection(textFieldType: String) {
        let popupVC = UIStoryboard.timeLineStoryboard.instantiateSelectionBottomPopUpVC()
        popupVC.delegate = self
        popupVC.textFieldType = textFieldType
        popupVC.height = 300
        popupVC.topCornerRadius = 15
        popupVC.presentDuration = 0.25
        popupVC.dismissDuration = 0.25
        popupVC.shouldDismissInteractivelty = true
        // popupVC.popupDelegate = self
        present(popupVC, animated: true, completion: nil)
    }
    
    @IBAction func postasAction(_ sender: Any) {
        switch vcType {
        case .createEvent:
           // CreateEventPostAs
            bottomSelection(type: "CreateEventPostAs", height: 150, data: createEventData ?? CreateEventModel())
        case .createPost:
            bottomSelection(type: "CreateDealFromInside", height: 150, data: createEventData ?? CreateEventModel())
        case .createDealFromOutside:
            bottomSelection(type: "CreateDealFromOutside", height: 250, data: createEventData ?? CreateEventModel())
        case .createSpotFromOutside:
            bottomSelection(type: "CreateSpotFromOutside", height: 250, data: CreateEventModel()
            )
        case .createDeal:
            posterIdsFromInside?.removeAll()
            bottomSelection(type: "CreateDealFromInside", height: 150, data: createEventData ?? CreateEventModel())
        case .createPostFromOutside:
            bottomSelection(type: "CreatePostFromOutside", height: 250, data: CreateEventModel())
        default:
            bottomSelection(type: "Post As", height: 150, data: CreateEventModel())
        }
    }
    func checkDate() {
        let date = Date()
        let dat = dateTxtField.text
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX") // set locale to reliable US_POSIX
        dateFormatter.dateFormat = "MM-dd-yyyy"
        print(dateTxtField.text)
        let date2 = dateFormatter.date(from:dateTxtField.text ?? "")
        print(date2)
        if date2! < date {
            popUp(type: .dateError, message: "\( date)")
        } else {
            return
        }
    }
    @IBAction func postAction(_ sender: Any) {
        switch vcType {
        case .createDeal:
            createDeal(id: userID)
        case .createEvent:
            createEvent()
        case .createPost:
            createPost()
        case .createDealFromOutside:
            createDealFrouOutside()
            //createDeal(id: Int(posterId ?? ""))
        case .createSpotFromOutside:
            createEventFromOutside()
        case .createPostFromOutside:
            createPostFromOutside()
        default:
            break
        }
        
        
    }
    //MARK:- CREATE POST API
    func createPost() {
        var message: String?
        var params: [String : Any]
        if topTxtView.text == "Write in timeline" {
        } else {
            message = topTxtView.text
        }
        if seenByEveryOne == false {
            params = ["message" :
                message ?? "", "post_as[]" : posterId ?? "","visibility_type" : "private"]
        } else {
            params = ["message" :
                message ?? "", "post_as[]" : posterId ?? "" ,"visibility_type" : "public"]
        }
        if message == "" && imagesDataSource?.images == [] && imagesDataSource?.videos == [] {
            popUp(type: .createPostEmpty, message: "")
        } else {
            CreateSpotResponse.requestToCreatePost(param: params, url: GraduateUrl.createPostURL,imageName: "images[]", videos: imagesDataSource?.videos ?? [], multipleImages: imagesDataSource?.images ?? []) { [weak self](response) in
                guard let strongSelf = self else { return }
                
                strongSelf.popUp(type: .postCreated, message: "")
            }
            
        }
    }
    @IBAction func seenBtnAction(_ sender: UIButton) {
        if seenByEveryOne == true{
            seenByEveryOne = false
            seenBtn.setImage(UIImage(named: "iconUnchecked"), for: .normal)
            seenBtn.tintColor = UIColor.init(hexString: Appcolors.buttons)
            seenBtn.imageView?.tintColor = UIColor.init(hexString: Appcolors.buttons)
            sender.isSelected = false
        } else {
            seenByEveryOne = true
            seenBtn.tintColor = UIColor.init(hexString: Appcolors.buttons)
            
            seenBtn.setImage(UIImage(named: "iconChecked"), for: .normal)
            sender.isSelected = true
        }
    }
    //MARK:- CREATE DEAL
    func createDeal(id: Int?) {
        print(id)
        if topTxtView.text == "Write a deal description *" && discountTypeTxtField.text == "" && addressTxtField.text == "" && dateTxtField.text == "" && termsAndCoditonTxtView.text == "Terms & Conditions" {
            popUp(type: .registerEmptyField, message: "")
        } else if topTxtView.text == "Write a deal description *" || topTxtView.text == "" {
            popUp(type: .nullDealDescInDeal, message: "")
        } else if discountTypeTxtField.text == "" {
            popUp(type: .nullDiscountType, message: "")
        } else if discountAmtTxtField.text == "" && discountPercentageTxtField.text == "" {
            popUp(type: .emptyDiscount, message: "")
        }else if dateTxtField.text == "" {
            popUp(type: .emptyDate, message: "")
        } else {
            let terms: String?
            let desc = topTxtView.text
            if termsAndCoditonTxtView.text == "Terms & Conditions" {
                terms = ""
            } else {
                terms = termsAndCoditonTxtView.text
            }
            
            var param: [String : Any] = ["description" : desc ?? "", "type" : discountTypeTxtField.text ?? "", "expiry_date" : dateTxtField.text ?? "", "terms" : terms ?? "", "address_string" : addressTxtField.text ?? "", "post_as" : "\(id ?? 0)"]
            if discountTypeTxtField.text == "Discount percentage" {
                param["discount_percentage"] = discountPercentageTxtField.text ?? ""
            } else {
                param["discount_amount"] = discountAmtTxtField.text ?? ""
            }
            checkDate()
            let url = (GraduateUrl.universityDetalsURL + "\(userID ?? 0)" + "/add/offer")
            print(param)
            // requestApi(param: param, url: url, type: .offerCreated)
            requesToCreateDeal()
            
        }
        
    }
    //MARK:- CREATE DEAL FROM OUTSIDE
    func createDealFrouOutside() {
       
        if topTxtView.text == "Write a deal description *" && discountTypeTxtField.text == "" && addressTxtField.text == "" && dateTxtField.text == "" && termsAndCoditonTxtView.text == "Terms & Conditions" {
            popUp(type: .registerEmptyField, message: "")
        } else if topTxtView.text == "Write a deal description *" || topTxtView.text == "" {
            popUp(type: .nullDealDescInDeal, message: "")
        } else if discountTypeTxtField.text == "" {
            popUp(type: .nullDiscountType, message: "")
        } else if discountAmtTxtField.text == "" && discountPercentageTxtField.text == "" {
            popUp(type: .emptyDiscount, message: "")
        }else if dateTxtField.text == "" {
            popUp(type: .emptyDate, message: "")
        } else {
            let terms: String?
            let desc = topTxtView.text
            if termsAndCoditonTxtView.text == "Terms & Conditions" {
                terms = ""
            } else {
                terms = termsAndCoditonTxtView.text
            }
            
            var param: [String : Any] = ["description" : desc ?? "", "type" : discountTypeTxtField.text ?? "", "expiry_date" : dateTxtField.text ?? "", "terms" : terms ?? "", "address_string" : addressTxtField.text ?? ""]
            if discountTypeTxtField.text == "Discount percentage" {
                param["discount_percentage"] = discountPercentageTxtField.text ?? ""
            } else {
                param["discount_amount"] = discountAmtTxtField.text ?? ""
            }
            checkDate()
            let url = (GraduateUrl.universityDetalsURL + "\(userID ?? 0)" + "/add/offer")
            print(param)
            // requestApi(param: param, url: url, type: .offerCreated)
            print(postersIdArray)
            if (postersIdArray?.count ?? 0) > 0 {
                postersIdArray?.forEach{
                    print($0)
                    posterDic?.append(["post_as[]" : "\($0)" + ":business"])
                    
                }
            } else {
                popUp(type: .emptyPostAs, message: "Please select the post as")
            }
           
            requesToCreateDeal()
            
        }
        
    }
    
    
    func requesToCreateDeal() {
       
        var param: [String : Any] = ["description" : topTxtView.text ?? "", "type" : discountTypeTxtField.text ?? "", "expiry_date" : dateTxtField.text ?? "", "terms" : termsAndCoditonTxtView.text ?? "", "address_string" : addressTxtField.text ?? "","post_as[]" : "\(profileData?.data?.internalIdentifier)" + ":business"]
        if discountTypeTxtField.text == "Discount percentage" {
            param["discount_percentage"] = discountPercentageTxtField.text ?? ""
        } else {
            param["discount_amount"] = discountAmtTxtField.text ?? ""
        }
      
        let url = (GraduateUrl.baseURL + "graduate_university/offers")
        print(param)
        print(posterDic)
        if case .createDeal = vcType {
            if userID == nil {
                popUp(type: .emptyPostAs, message: "The post as field is required")
                return
            } else {
                param["post_as[]"] = "\(userID ?? 0)" + ":business"
            }
            CreateSpotResponse.requestToCreateOffer(param: param, url: url, images: imagesDataSource?.images ?? [UIImage()], arrayObjects: []) { [weak self](response) in
                guard let strongSelf = self else { return }
                print(response)
                strongSelf.popUp(type: .offerCreated, message: "")
                strongSelf.reloadDataAfterApiCall = true
            }
        } else {
        CreateSpotResponse.requestToCreateOffer(param: param, url: url, images: imagesDataSource?.images ?? [UIImage()], arrayObjects: posterDic ?? []) { [weak self](response) in
            guard let strongSelf = self else { return }
            print(response)
            strongSelf.popUp(type: .offerCreated, message: "")
            strongSelf.reloadDataAfterApiCall = true
        }
        }

    }
    
    
    
    //MARK:- CREATE EVENT
    func createEvent() {
        let desc: String
        if topTxtView.text == "Event Description..." {
            desc = ""
        } else {
            desc = topTxtView.text
        }
        
        let param: [String : Any] = ["event_title" : nameTxtField.text ?? "", "event_end_date" : endDateTxtField.text ?? "", "event_start_date" : dateTxtField.text ?? "", "event_type" : eventTxtField.text ?? "" , "address_string" : addressTxtField.text ?? "", "visibility_type" : "public", "message" : desc, "post_as[]" : posterId ?? "","event_start_time" : timeTxtField.text ?? ""]
        
        if topTxtView.text == "" && nameTxtField.text == "" && addressTxtField.text == "" && dateTxtField.text == "" && timeTxtField.text == "" && endDateTxtField.text == "" {
            popUp(type: .registerEmptyField, message: "")
        } else {
            CreateSpotResponse.requestToCreateEvent(param: param, url: GraduateUrl.addEventURL, images: imagesDataSource?.images ?? [UIImage()], imageName: "images[]") { [weak self](response) in
                guard let strongSelf = self else { return }
                if response?.errors?.eventStartTime != nil {
                    strongSelf.popUp(type: .passwordValidation, message: response?.errors?.eventStartTime?[0] ?? "")
                } else if response?.errors?.eventTitle != nil {
                    strongSelf.popUp(type: .passwordValidation, message: response?.errors?.eventTitle?[0] ?? "")
                } else if response?.errors?.eventStartDate != nil {
                    strongSelf.popUp(type: .passwordValidation, message: response?.errors?.eventStartDate?[0] ?? "")
                } else {
                    strongSelf.popUp(type: .eventCreated, message: "")
                    strongSelf.reloadDataAfterApiCall = true
                }
            }
        }
    }


//MARK:- CREATE EVENT FROM OUTSIDE
func createEventFromOutside() {
    let desc: String
           if topTxtView.text == "Event Description..." {
               desc = ""
           } else {
               desc = topTxtView.text
           }
           
           let param: [String : Any] = ["event_title" : nameTxtField.text ?? "", "event_end_date" : endDateTxtField.text ?? "", "event_start_date" : dateTxtField.text ?? "", "event_type" : eventTxtField.text ?? "" , "address_string" : addressTxtField.text ?? "", "visibility_type" : "public", "message" : desc,"event_start_time" : timeTxtField.text ?? ""]
           
           if topTxtView.text == "" && nameTxtField.text == "" && addressTxtField.text == "" && dateTxtField.text == "" && timeTxtField.text == "" && endDateTxtField.text == "" {
               popUp(type: .registerEmptyField, message: "")
           } else {
            print(postasData?.selfAdPoster)
            if postasData?.selfAdPoster == true {
                posterDic?.append(["post_as[]" : "\(UserDefaultsHandler.getUDValue(key: .userID) ?? 0)" + ":User"])
            }
           
            postersIdArray?.forEach{
                print($0)
                 posterDic?.append(["post_as[]" : "\($0)" + ":business"])
                
            }
             print(posterDic)
            CreateSpotResponse.requestToCreateEventFromOutside(param: param, url: GraduateUrl.addEventURL, imageName: "images[]", videos: imagesDataSource?.videos ?? [], multipleImages: imagesDataSource?.images ?? [], arrayObjects: posterDic ?? []) { [weak self](response) in
                guard let strongSelf = self else { return }
                print(response?.errors?.eventStartDate)
                if response?.errors?.eventStartDate != nil {
                    strongSelf.popUp(type: .passwordValidation, message: response?.errors?.eventStartDate?[0] ?? "")
                } else if response?.errors?.eventStartTime != nil {
                    strongSelf.popUp(type: .passwordValidation, message: response?.errors?.eventStartTime?[0] ?? "")
                } else if response?.errors?.eventTitle != nil {
                    strongSelf.popUp(type: .passwordValidation, message: response?.errors?.eventTitle?[0] ?? "")
                } else {
                    strongSelf.popUp(type: .eventCreated, message: "")
                }
                
            }
    }
}
    
//MARK:- CREATE POST FROM OUTSIDE
    func createPostFromOutside() {
        var message: String?
        var params: [String : Any]
        if topTxtView.text == "Write in timeline" {
        } else {
            message = topTxtView.text
        }
        if seenByEveryOne == false {
            params = ["message" :
                message ?? "","visibility_type" : "private"]
        } else {
            params = ["message" :
                message ?? "","visibility_type" : "public"]
        }
        if message == "" && imagesDataSource?.images == [] && imagesDataSource?.videos == [] {
            popUp(type: .createPostEmpty, message: "")
        } else {
            print(postasData?.selfAdPoster)
            if postasData?.selfAdPoster == true {
                posterDic?.append(["post_as[]" : "\(UserDefaultsHandler.getUDValue(key: .userID) ?? 0)" + ":User"])
            }
           
            postersIdArray?.forEach{
                print($0)
                 posterDic?.append(["post_as[]" : "\($0)" + ":business"])
                
            }
          
//            CreateSpotResponse.requestToCreatePost(param: params, url: GraduateUrl.createPostURL,imageName: "images[]", videos: imagesDataSource?.videos ?? [], multipleImages: imagesDataSource?.images ?? []) { [weak self](response) in
//                guard let strongSelf = self else { return }
//
//                strongSelf.popUp(type: .postCreated, message: "")
//            }
//
            CreateSpotResponse.requestToCreatePostFromOutside(param: params, url: GraduateUrl.createPostURL, imageName: "images[]", videos: imagesDataSource?.videos ?? [], multipleImages: imagesDataSource?.images ?? [], arrayObjects: posterDic ?? [], showHud: true) { [weak self](response) in
                guard let strongSelf = self else { return }
                print(response?.postAsErrors?.postAsErrors)
                if response?.postAsErrors?.postAsErrors
                    != nil {
                    strongSelf.popUp(type: .emptyPostAs, message: response?.postAsErrors?.postAsErrors?[0] ?? "")
                } else {
                    strongSelf.popUp(type: .postCreated, message: "")
                }
               
            }
        }
    }
}

//MARK:- TEXTFIELD DELEGATE
extension CreateSpotViewController: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == eventTxtField {
            if case .addPortFolio = vcType {
                eventTxtField.resignFirstResponder()
                self.presentBottomSelection(textFieldType: "GraduationType")
                return false
            } else {
            eventTxtField.resignFirstResponder()
            self.presentBottomSelection(textFieldType: "Event")
            return false
            }
        } else if textField == dateTxtField {
            
            let currentDate = HelperFunctions.getCurrentData()
            let calendar = YYCalendar(normalCalendarLangType: .ENG,
                                      date: currentDate,
                                      format: "MM/dd/yyyy") { [weak self] date in
                                        self?.dateTxtField.text = date
            }
            
            calendar.show()
            return false
        } else if textField == timeTxtField {
            textField.resignFirstResponder()
            openTimePicker()
            textField.resignFirstResponder()
            return false
        } else if textField == addressTxtField {
            let acController = GMSAutocompleteViewController()
            acController.delegate = self
            present(acController, animated: true, completion: nil)
            textField.resignFirstResponder()
        } else if textField == endDateTxtField {
            let currentDate = HelperFunctions.getCurrentData()
            let calendar = YYCalendar(normalCalendarLangType: .ENG,
                                      date: currentDate,
                                      format: "MM/dd/yyyy") { [weak self] date in
                                        self?.endDateTxtField.text = date
                                        
            }
            calendar.show()
            return false
        } else if textField == discountTypeTxtField {
            textField.resignFirstResponder()
            let popupVC = UIStoryboard.timeLineStoryboard.instantiateSelectionBottomPopUpVC()
            popupVC.textFieldType = "Create Deal"
            popupVC.delegate = self
            popupVC.height = 100
            popupVC.topCornerRadius = 15
            popupVC.presentDuration = 0.25
            popupVC.dismissDuration = 0.25
            popupVC.shouldDismissInteractivelty = true
            // popupVC.popupDelegate = self
            present(popupVC, animated: true, completion: nil)
            return false
        } else if textField == discountPercentageTxtField {
            discountPercentageTxtField.resignFirstResponder()
            bottomSelection(type: "DiscountPercentage", height: 250, data: CreateEventModel())
            return false
        }
        
        
        return true
    }
    //    func textFieldDidBeginEditing(_ textField: UITextField) {
    //        if textField == dateTxtField {
    //
    //        }
    //
    //
    //    } else if textField == timeTxtField {
    //
    //    } else if textField == addressTxtField {
    //
    //
    //    } else if textField == endDateTxtField {
    //
    //    } else if textField == discountTypeTxtField {
    //
    //    } else if textField == eventTxtField {
    //
    //
    //    } else if textField == timeTxtField {
    //    timeTxtField.resignFirstResponder()
    //    }
    //}
}
//MARK:- IMAGEPICKER CONTROLLER
extension CreateSpotViewController:UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        self.dismiss(animated: true) { [weak self] in
            let videoURL = info[.mediaURL] as? NSURL
            print(videoURL)
            if videoURL != nil {self?.imagesDataSource?.images.removeAll()
                // self?.camImage = (self?.thumbnailForVideo(url: videoURL! as URL)!)!
                
                self?.imagesDataSource?.videos?.append(videoURL! as URL)
                self?.imagesDataSource?.images.removeAll()
                self?.collectionView.reloadData()
            } else {
                if let selectedImage = info[.originalImage] as? UIImage  {
                    print(selectedImage)
                    
                    self?.imagesDataSource?.images.append((selectedImage as? UIImage)!)
                    print(self?.imagesDataSource?.images[0])
                    
                    self?.imagesDataSource?.videos?.removeAll()
                    self?.collectionView.reloadData()
                    
                }
            }
            
            
            
            
            //  print(selectedImage)
            
            //imageTake.image = selectedImage
        }
    }
    //    func thumbnailForVideo(url: URL) -> UIImage? {
    //        let asset = AVAsset(url: url)
    //        let assetImageGenerator = AVAssetImageGenerator(asset: asset)
    //        assetImageGenerator.appliesPreferredTrackTransform = true
    //
    //        var time = asset.duration
    //        time.value = min(time.value, 2)
    //
    //        do {
    //            let imageRef = try assetImageGenerator.copyCGImage(at: time, actualTime: nil)
    //            return UIImage(cgImage: imageRef)
    //        } catch {
    //            print("failed to create thumbnail")
    //            return nil
    //        }
    //    }
    
}
//MARK:- COLLECTION VIEW DATASOURCE
extension CreateSpotViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        print(imagesDataSource?.images.count)
        if (imagesDataSource?.images.count ?? 0) > 0 {
            return imagesDataSource?.images.count ?? 0
        } else {
            return imagesDataSource?.videos?.count ?? 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if imagesDataSource?.images.count ?? 0 > 0 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MediaPostUICollectionViewCell", for: indexPath) as! MediaPostUICollectionViewCell
            cell.mainImage.image = imagesDataSource?.images[indexPath.row]
            cell.indexPath = indexPath
            cell.delegate = self
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MediaVideosCollectionViewCell", for: indexPath) as! MediaVideosCollectionViewCell
            cell.playVideo(url: (imagesDataSource?.videos?[indexPath.row])!)
            cell.index = indexPath
            cell.playerView.isMuted = true
            cell.delegate = self
            return cell
        }
    }
    
    
}
//MARK: COLLECTIONVEW DELEGATE
extension CreateSpotViewController: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 70, height: 70)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
}
//MARK: DELEGATE FUNCTIONS FOR GOOGLE PLCACES

extension CreateSpotViewController: GMSAutocompleteViewControllerDelegate {
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        // Get the place name from 'GMSAutocompleteViewController'
        // Then display the name in textField
        addressTxtField.text = place.name
        // Dismiss the GMSAutocompleteViewController when something is selected
        dismiss(animated: true, completion: nil)
    }
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // Handle the error
        print("Error: ", error.localizedDescription)
    }
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        // Dismiss when the user canceled the action
        dismiss(animated: true, completion: nil)
    }
}

//MARK: REMOVING IMAGES FROM THE CLOSE BUTTON FROM COLLECTION VIEW

extension CreateSpotViewController: RemovingImagesFromPost {
    func removingImages(index: IndexPath) {
        imagesDataSource?.images.remove(at: index.row)
        collectionView.reloadData()
    }
}
//MARK: REMOVING POST VIDEOS FROM COLLECTION VIEW

extension CreateSpotViewController: RemovingPostVideos {
    func removingVideos(index: IndexPath) {
        imagesDataSource?.videos?.remove(at: index.row)
    }
}
//MARK: RECEIVING MULTIPLES VALUES FROM BOTTOM VC

extension CreateSpotViewController: SendingMultipleSeclections {
    func postAsActionFromInside(value: Int?, bool: Bool, index: Int?, type: CreateSpotVCTypes, posterIds: [Int], postData: CreateEventModel) {
        switch type {
        case .createDeal:
            print(posterIds)
            createEventData = postData
            for item in posterIds {
                if item == UserDefaultsHandler.getUDValue(key: .userID) as! Int {
                    posterIdsFromInside?.append(["post_as" : "\(item)" + ":user"])
                } else {
                    posterIdsFromInside?.append(["post_as" : "\(item)" + ":business"])
                }
            }
        default:
            break
        }
      print(posterIdsFromInside)
      
    }
    
    func gettingTheSelectedValueFromTheList(value: Int?, bool: Bool, index: Int?, type: CreateSpotVCTypes, posterIds: [Int]) {
        switch type {
                case .createDealFromOutside:
                    posterId = "\(value)"
                           print(index)
                           postasData?.selectedCell = index
                           print(postasData?.selectedCell)
                    postersIdArray = posterIds
                case .createSpotFromOutside:
                    print(bool)
                    print(value)
                    print(index)
                    posterId = "\(value)"
                    postasData?.selectedCell = index
                   // let filtered = postasData?.data?.data?.filter{ $0.isSelected == true }
                 
        //            postasData?.data?.data?.forEach { element in
        //                print(element.internalIdentifier)
        //                if element.isSelected == true {
        //                     postersId?["post_as[]"] = element.internalIdentifier
        //                } else {
        //
        //                }
        //
        //            }
//                    if postasData?.selfAdPoster == false {
//                     postasData?.data?.data?.forEach { element in
//                        if element.isSelected == true {
//                            postersIdArray?.append(value)
//                        } else {
//                            postersIdArray?.remove(at: index!)
//                        }
//                    }
//                    } else {
//                        return
//                    }
                   print(posterIds,bool)
                    postersIdArray = posterIds
                   print(postersIdArray)
                print(posterDic)
                    print(postasData?.selfAdPoster)
        case .createDeal:
           print(posterIds)
            userID = value
        case .createPostFromOutside:
            print(posterIds)
            posterId = "\(value)"
            postasData?.selectedCell = index
            postersIdArray = posterIds
        case .createPost:
            posterId = "\(value!)"
                default:
                    break
                }
    }
   
    func gettingSelectedVaueFromProfileMoreBtn(value: Int, bool: Bool, index: Int) {
        print(index)
        if index == 0 {//MARK:- USERID FOR CREATE EVENT AS POSTER ID
            posterId = ("\(UserDefaultsHandler.getUDValue(key: .userID) ?? 0)" + ":user")
        } else if index == 1 {//MARK:- BUSINESSID FOR CREATE EVENT AS POSTER ID
            posterId = "\(value)" + ":business"
        }
        //createEventData?.selectedCell = index
    }
    
    
    
    func sendingUserId(id: Int, index: Int) {
        discountPercentageTxtField.text = "\(id)"
    }
    func sendingEvnetType(value: String, type: String) {
        eventTxtField.text = value
    }
    func sendingMediaType(value: Int) {
        print(value)
        switch value {
        case 0:
            //Mark:- Taking image from camera
            
            imagePickerController.delegate = self
            imagePickerController.sourceType = UIImagePickerController.SourceType.camera
            imagePickerController.modalPresentationStyle = .overCurrentContext
            self.present(imagePickerController, animated: true, completion: nil)
        case 1:
            //Mark:- Taking image from gallery
            getImage(fromSourceType: .savedPhotosAlbum, mediaType: "public.image")
        case 2:
            //Mark:- Taking video from gallery
            getImage(fromSourceType: .savedPhotosAlbum, mediaType: "public.movie")
        default:
            break
        }
    }
    func gettingFullScreenCoverPic() {
        
    }
    func sendingUniCategory(value: String) {
        if value == "true" {
            postasData?.selfAdPoster = true
        } else {
             postasData?.selfAdPoster = false
        }
         
    }
    func sendingDiscountType(value: String) {
        discountTypeTxtField.text = value
        if value == "Discount percentage"{
            percentValueView.isHidden = false
            discountAmtView.isHidden = true
        } else {
            discountAmtView.isHidden = false
            percentValueView.isHidden = true
            
        }
    }
    
    func sendingMultipleVlaues(values: [String]) {
        multipleSelectedValues = values
    }
//    func postAsActionFromInside(value: Int?, bool: Bool, index: Int?, type: CreateSpotVCTypes, posterIds: [Int], postData: PostFromInside) {
//        createEventData
//    }
    
}
//MARK:- TEXTVIEWDELEGATE
extension CreateSpotViewController: UITextViewDelegate {
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        if textView == topTxtView {
            if textView.text == "Write in timeline" || textView.text == "Event Description" || textView.text == "Write a deal description *" || textView.text == "Event Description..." || textView.text == "Write in timeline" || textView.text == "Description"{
                topTxtView.text = ""
                topTxtView.textColor = UIColor.init(hexString: Appcolors.text)
                return true
            } else {
                
                topTxtView.textColor = UIColor.init(hexString: Appcolors.text)
                return true
            }
        } else if textView == termsAndCoditonTxtView {
            if textView.text == "Terms & Conditions" {
                termsAndCoditonTxtView.text = ""
                termsAndCoditonTxtView.textColor = UIColor.init(hexString: Appcolors.text)
                return true
            } else {
                
                termsAndCoditonTxtView.textColor = UIColor.init(hexString: Appcolors.text)
                return true
            }
        }
        return false
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView == topTxtView {
            if case  .editSpot = vcType {
                if textView.text == "" {
                    textView.text = "Event Description..."
                    textView.textColor = .lightGray
                }
            }
            if case  .editPost = vcType {
            if textView.text == "" {
                textView.text = "Write in timeline"
                textView.textColor = .lightGray
            }
            }
        }
    }
}
extension CreateSpotViewController: SendingActionFromDeletePostPopUP {
    func reportNowResult(value: Bool) {
        if !value {
            
        } else {
            navigationController?.popViewController(animated: true)
        }
    }
}
//MARK:- EROR CHAT DELEGATE
extension CreateSpotViewController: SendingActionFromErrorChat {
    func sendingAction() {
        reloadDataAfterApiCall = true
        navigationController?.popViewController(animated: true)
    }
    func sendingCancelAction() {
        
    }
    func adctionwithType(type: ErrorType) {
        reloadDataAfterApiCall = true
        delegate?.reloadWithType(type: type)
        navigationController?.popViewController(animated: true)
    }
}
struct ImagesData {
    var images: [UIImage] = []
    var videos: [URL]? = [URL]()
}
extension Array {
    public func toDictionary<Key: Hashable>(with selectKey: (Element) -> Key) -> [Key:Element] {
        var dict = [Key:Element]()
        for element in self {
            dict[selectKey(element)] = element
        }
        return dict
    }
}

