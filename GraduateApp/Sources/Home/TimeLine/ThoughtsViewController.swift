//
//  ThoughtsViewController.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 7/24/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import UIKit
import Popover

protocol SendingDataFromThoughtsVC: class {
    func sendingData(index: IndexPath,thoughtsCount: Int, commentsData: [Comments])
}
class ThoughtsViewController: UIViewController {
    struct ThoughtModel {
        var name: String?
        var image: String?
    }
    @IBOutlet var tableViewheight: NSLayoutConstraint!
    @IBOutlet var footerBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var sendBtn: UIButton!
    @IBOutlet var parentScrollview: UIScrollView!
    @IBOutlet var footerView: UIView!
    @IBOutlet var moreBtn: UIButton!
    @IBOutlet weak var thoughtsTxtView: UITextView!
    @IBOutlet weak var tableView: UITableView!
    weak var delegate: SendingDataFromThoughtsVC?
    var popupTableView = UITableView(frame: CGRect(x: 0, y: 0, width: 90, height: 120))
    var texts = ["Share", "Edit", "Delete"]
    var keyboardOpen = false
    fileprivate var popoverOptions: [PopoverOption] = [
        .type(.left),
        .blackOverlayColor(UIColor(white: 0.0, alpha: 0.6))
    ]
    var popover: Popover!
    var index: IndexPath!
    var commentIDToDelete: Int?
    var commentIndexToEdit: IndexPath!
    var editMode: Bool = false
    var hasCommented: Bool = false
    var dataSource: ThoughtsResponseModel? {
        didSet {
            tableView.reloadData()
        }
    }
    var commentId: Int? {
        didSet {
            requestToThoughtsList()
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        tableView.dataSource = self
        tableView.delegate = self
        
        tableView.reloadData()
        thoughtsTxtView.text = "Write a comment"
        thoughtsTxtView.textColor = .lightGray
        thoughtsTxtView.delegate = self
        dataSource?.data = []
        parentScrollview.isScrollEnabled = false
        currentTabBar?.setBar(hidden: true, animated: true)
       // currentTabBar?.tabBarHeight =  -50
    }
    override func viewWillAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector:#selector(keyboardWillAppear), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector:#selector(keyboardWillDisappear), name: UIResponder.keyboardWillHideNotification, object: nil)
        self.title = "Thoughts"
        thoughtsTxtView.layer.cornerRadius = 16
        thoughtsTxtView.text = "Write a comment"
        currentTabBar?.setBar(hidden: true, animated: true)
        sendBtn.setImage(UIImage(named: "iconSend"), for: .normal)
        sendBtn.imageView?.tintColor = UIColor.init(hexString: Appcolors.buttons)
        navigationController?.setNavigationBarHidden(false, animated: true)
        currentTabBar?.setBar(hidden: true, animated: true)
    }
    override func viewWillDisappear(_ animated: Bool) {
                currentTabBar?.setBar(hidden: false, animated: true)
        navigationController?.setNavigationBarHidden(false, animated: true)
        print(index,dataSource?.data)
        if hasCommented == true {
            delegate?.sendingData(index: index, thoughtsCount: 0, commentsData: dataSource?.data ?? [])
        }
    }
    @objc func keyboardWillAppear(notification: NSNotification){
        // Do something here
//        keyboardOpen = true
       // if let userInfo = notification.userInfo,
//           // 3
//           let keyboardRectangle = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? CGRect {
//        print(Int(keyboardRectangle.height))
//            let view = UIView()
//                           view.frame = CGRect(x: 0, y: 0, width: 414, height: Int(keyboardRectangle.height))
//                           tableView.tableFooterView = view
//        }
       // parentScrollview.isScrollEnabled = true
       // parentScrollview.isScrollEnabled = false
        let info = notification.userInfo!
        
        let keyboardFrame: CGRect = (info[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        print(keyboardFrame.size.height)
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
               let keyboardHeight = keyboardSize.height
               print(keyboardHeight)
            self.footerBottomConstraint.constant = keyboardHeight
           }
       
        //self.navigationController?.setNavigationBarHidden(true, animated: true)
        view.layoutIfNeeded()
       // currentTabBar?.tabBarHeight = 0
      //  currentTabBar?.tabBarHeight =  0
      //  self.footerBottomConstraint.constant = 300
//        UIView.animate(withDuration: 0.1, animations: { () -> Void in
//               self.footerBottomConstraint.constant = (keyboardFrame.size.height - 250)
//           })
       
       // self.view.frame.origin.y -= keyboardFrame.size.height
    }
    @objc func keyboardDidAppear(notification: NSNotification) {
        print("")
    }

    @objc func keyboardWillDisappear(notification: NSNotification){
        // Do something here
        let info = notification.userInfo!
        let keyboardFrame: CGRect = (info[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        self.footerBottomConstraint.isActive = false
        self.footerBottomConstraint.constant = 8
        self.footerBottomConstraint.isActive = true
        view.layoutIfNeeded()
       // self.navigationController?.setNavigationBarHidden(false, animated: true)
//        UIView.animate(withDuration: 0.1, animations: { () -> Void in
//              // self.footerBottomConstraint.constant = 8
//           })
        //self.footerBottomConstraint.constant -= keyboardFrame.size.height
        view.layoutIfNeeded()
       // currentTabBar?.setBar(hidden: true, animated: true)
           keyboardOpen = false
        
        currentTabBar?.setBar(hidden: true, animated: true)
      //  currentTabBar?.tabBarHeight =  -50
      

       // parentScrollview.isScrollEnabled = false
        
    }
//    func addData() {
//        dataSource = [ThoughtModel(name: "Sk", image: "profilePicture2"), ThoughtModel(name: "Sam", image: "profileImage1")]
//    }
    //MARK:- POPING RATING VC
    func popingBottomVC(id: Int, starValue: String, selectedindexToReload: Int, selectionType: GraduateDetails) {
      let popupVC = UIStoryboard.timeLineStoryboard.instantiateRatingVC()
             //popupVC.delegate = self
          //popupVC.selectedIndex = 3
        popupVC.id = id
        popupVC.selectedIndex = Int(starValue)
        popupVC.indexToReload = selectedindexToReload
        popupVC.selectionType = selectionType
                    popupVC.height = 200
                    popupVC.topCornerRadius = 15
             popupVC.presentDuration = 0.25
                    popupVC.dismissDuration = 0.25
                    popupVC.shouldDismissInteractivelty = true
                    popupVC.delegate = self
                    present(popupVC, animated: true, completion: nil)
      }
    
    //MARK:- REQUESTING TO THOUGHTS LIST
    func requestToThoughtsList() {
        ThoughtsResponse.requestToThoughtsList(id: "\(commentId ?? 0)") { [weak self](response) in
            guard let StrongSelf = self else { return }
            StrongSelf.dataSource = response
        }
    }
        
//MARK:- REQUESTING TO RATE COMMENT
        func requestToRateComment(id: Int, ratingNum: Int,reloadingIndex: Int) {
            ThoughtsResponse.requestToRateComment(id: id, ratingNum: (ratingNum + 1), showHud: false) { [weak self](response) in
                guard let strongSelf = self else { return }
                print(ratingNum)
                print(response)
                strongSelf.dataSource?.data?[reloadingIndex].starredValue = response?.data
                strongSelf.dataSource?.data?[reloadingIndex].hasStarred = true
                let indexPaths = IndexPath(row: reloadingIndex, section: 0)
                strongSelf.tableView.reloadRows(at: [indexPaths], with: .none)
                strongSelf.hasCommented = true
            }
        
    }
//MARK:- MORE POPOVER
    func sharePopOver(sender: UIButton) {
        popupTableView.reloadData()
        if texts.count == 2 {
            popupTableView.frame = CGRect(x: 0, y: 0, width: 90, height: 80)
            
        } else {
            popupTableView.frame = CGRect(x: 0, y: 0, width: 90, height: 120)
            
        }
        popupTableView.delegate = self
        popupTableView.dataSource = self
        popupTableView.isScrollEnabled = false
        self.popover = Popover(options: self.popoverOptions)
        self.popover.willShowHandler = {
            print("willShowHandler")
        }
        self.popover.didShowHandler = {
            print("didDismissHandler")
        }
        self.popover.willDismissHandler = {
            print("willDismissHandler")
        }
        self.popover.didDismissHandler = {
            print("didDismissHandler")
        }
        
        self.popover.show(popupTableView, fromView: sender)
    }
    //MARK:- POPUP
    func popUp(type: ErrorType, message: String) {
        let vc = UIStoryboard.timeLineStoryboard.instantiateErrorCarMsgPopVC()
        vc.popupType = type
        vc.messageString = message
        vc.delegaet = self
        let cardPopup = SBCardPopupViewController(contentViewController: vc)
        cardPopup.show(onViewController: self)
    }
    @IBAction func sendBtnAction(_ sender: Any) {
        if editMode == true {
            requestToEditComment()
        } else {
        if thoughtsTxtView.text == "Write a comment" || thoughtsTxtView.text == "" {
            return
        } else {
            let param:[String : Any] = ["comment_body" : thoughtsTxtView.text ?? ""]
            print(commentId)
            ThoughtsResponse.requestToComment(id: "\(commentId ?? 0)", param: param) { [weak self](response) in
                guard let strongSelf = self else { return }
                
                strongSelf.hasCommented = true
                print(response?.thoughtsData?.body)
                strongSelf.dataSource?.data?.append((response?.thoughtsData)!)
                strongSelf.tableView.reloadData()
                
            }
        }
        }
        thoughtsTxtView.text = "Write a comment"
        thoughtsTxtView.textColor = .lightGray
        thoughtsTxtView.resignFirstResponder()
    }
    
    @IBAction func moreBtnAction(_ sender: Any) {
    }
    

}
extension ThoughtsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == popupTableView {
            return texts.count ?? 0
        } else {
        return dataSource?.data?.count ?? 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == popupTableView {
            let cell = UITableViewCell(style: .default, reuseIdentifier: nil)
            cell.textLabel?.text = self.texts[(indexPath as NSIndexPath).row]
            cell.textLabel?.textColor = UIColor.init(hexString: Appcolors.text)
            return cell
        } else {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ThoughtsTableViewCell", for: indexPath) as! ThoughtsTableViewCell
       let tapGesture = UITapGestureRecognizer(target: self, action: #selector(ratingDetailsAction))
        cell.mainImag.setURLImage(imageURL: ((GraduateUrl.imageUrl) + (dataSource?.data?[indexPath.row].user?.profile?.profileImage ?? "")))
        cell.nameLabel.text = dataSource?.data?[indexPath.row].user?.name
        if dataSource?.data?[indexPath.row].hasStarred == true {
            cell.ratingBtn.setImage(UIImage(named: "star3"))
        } else {
            cell.ratingBtn.setImage(UIImage(named: "smallStar"))
        }
        if dataSource?.data?[indexPath.row].isOwnComment == true {
            cell.moreBtn.isHidden = false
        } else {
            cell.moreBtn.isHidden = true
        }
        cell.thoughtLabel.text = dataSource?.data?[indexPath.row].body
        
        cell.ratingLabel.text = "\(dataSource?.data?[indexPath.row].starsCount ?? 0)"
        cell.ratingStackView.addGestureRecognizer(tapGesture)
        cell.delegate = self
        cell.data = dataSource?.data?[indexPath.row]
        cell.index = indexPath
        cell.commentId = dataSource?.data?[indexPath.row].internalIdentifier
            cell.dateLabel.text = HelperFunctions.timeInterval(timeAgo: dataSource?.data?[indexPath.row].createdAt ?? "")
        return cell
        }
    }
    @objc func ratingDetailsAction() {
           let vc = UIStoryboard.timeLineStoryboard.instantiateRatingDetailsVC()
           navigationController?.pushViewController(vc, animated: true)
       }
    
    
}
//MARK:- TABLE VIEW DELEGATE
extension ThoughtsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == popupTableView {
            return 40
        } else {
        return UITableView.automaticDimension
    }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == popupTableView {
            if texts[indexPath.row] == "Delete" {
                popover.dismiss()
                popUp(type: .deleteComment, message: "")
            } else if texts[indexPath.row] == "Edit" {
                popover.dismiss()
                thoughtsTxtView.text = dataSource?.data?[commentIndexToEdit.row].body
                editMode = true
                thoughtsTxtView.textColor = .black
                
            }
        }
    }
}
extension ThoughtsViewController: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
//        if editMode == true {
//            tableView.tableFooterView = footerView
//        } else {
        if textView == thoughtsTxtView {
           // tableView.tableFooterView = footerView
            tableView.setContentOffset(.zero, animated: true)
            thoughtsTxtView.textColor = UIColor.init(hexString: Appcolors.text)
            thoughtsTxtView.text = dataSource?.data?[commentIndexToEdit.row].body
       // }
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if thoughtsTxtView.text.isEmpty {
            thoughtsTxtView.text = "Write a comment"
            thoughtsTxtView.textColor = UIColor.lightGray
        }
    }
//    func popingBottomVC(id: Int, starValue: String, selectedindexToReload: Int, selectionType: GraduateDetails) {
//                let popupVC = UIStoryboard.timeLineStoryboard.instantiateRatingVC()
//                       popupVC.delegate = self
//        //popupVC.selectedIndex = dataSource.
//                              popupVC.height = 200
//                              popupVC.topCornerRadius = 15
//                       popupVC.presentDuration = 0.25
//                              popupVC.dismissDuration = 0.25
//                              popupVC.shouldDismissInteractivelty = true
//                             // popupVC.popupDelegate = self
//                              present(popupVC, animated: true, completion: nil)
//                }
}
extension ThoughtsViewController: SendingActionsFromThoughtsTVC {
    func moreBtnAction(sender: UIButton, id: Int, index: IndexPath, cell: ThoughtsTableViewCell, isOwner: Bool, postType: GraduateDetails) {
        commentIDToDelete = id
        commentIndexToEdit = index
        texts.removeAll()
        texts = ["Delete", "Edit"]
        sharePopOver(sender: sender)
    }
    
    func ratingPopUp(id: Int, starValue: String, selectedindexToReload: Int, selectionType: GraduateDetails) {
        popingBottomVC(id: id, starValue: starValue, selectedindexToReload: selectedindexToReload, selectionType: selectionType)
        
    }
    func tappingProfileName(index: IndexPath) {
        if dataSource?.data?[index.row].isOwnComment == true {
            let vc = UIStoryboard.timeLineStoryboard.instantiateUserProfileVC()
            navigationController?.pushViewController(vc, animated: true)
        } else {
            let vc = UIStoryboard.timeLineStoryboard.instantiateOtherUserProfileVC()
            vc.userId = dataSource?.data?[index.row].userId
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func tappingProfileName() {
        let vc = UIStoryboard.timeLineStoryboard.instantiateUserProfileVC()
        navigationController?.pushViewController(vc, animated: true)
    }
}
//MARK:- DELEGATE FORM RATING VC
extension ThoughtsViewController: SendingActionFromRatingVC {
    func sendingActionWithType(type: RatingActionTypes, id: Int, reloadingIndex: Int, ratingNum: Int, selectionType: GraduateDetails) {
        switch type {
        case .requestforRating:
            requestToRateComment(id: id, ratingNum: ratingNum, reloadingIndex: reloadingIndex)
        case .reuestForUnratingPost:
        requestToUnrateComment(id: id, ratingNum: ratingNum, reloadingIndex: reloadingIndex)
        default:
            break
        }
      
    }
    func requestToUnrateComment(id: Int, ratingNum: Int, reloadingIndex: Int) {
            ThoughtsResponse.requestToRateComment(id: id, ratingNum: (ratingNum + 1), showHud: false) { [weak self](response) in
                guard let strongSelf = self else { return }
                print(ratingNum)
                print(response)
                strongSelf.dataSource?.data?[reloadingIndex].starredValue = response?.data
                strongSelf.dataSource?.data?[reloadingIndex].hasStarred = true
                let indexPaths = IndexPath(row: reloadingIndex, section: 0)
                strongSelf.tableView.reloadRows(at: [indexPaths], with: .none)
                strongSelf.hasCommented = true
            }
        
    
    }

}
extension ThoughtsViewController: SendingActionFromErrorChat {
    func sendingAction() {
        dataSource?.data?.remove(at: commentIndexToEdit.row)
        requestToDeleteComment()
    }
    
    func sendingCancelAction() {
        
    }
    
    func adctionwithType(type: ErrorType) {
        
    }
    
//MARK:- DELETE COMMENT
    func requestToDeleteComment() {
        ThoughtsResponse.requestToDeleteComment(id: commentIDToDelete ?? 0, showHud: true) { [weak self](response) in
            guard let strongSelf = self else { return }
            print(response)
                strongSelf.popUp(type: .commentDeleted, message: "")
            strongSelf.tableView.reloadData()
        }
    }
//MARK:- EDIT COMMENT
    func requestToEditComment() {
        let params: [String : Any] = ["comment_body" : thoughtsTxtView.text ?? ""]
        ThoughtsResponse.requestToEditComment(id: commentIDToDelete ?? 0, params: params, showHud: true) { [weak self](response) in
            guard let strongSelf = self else {
                 return
            }
            print(response)
            strongSelf.requestToThoughtsList()
        }
    }
    
}

