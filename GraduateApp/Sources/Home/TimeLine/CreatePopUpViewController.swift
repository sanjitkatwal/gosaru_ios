//
//  CreatePopUpViewController.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 6/29/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import UIKit
protocol pushingToVC: class{
    func selectingThePopUpVc(vcName: CreateVCList)
}

class CreatePopUpViewController: UIViewController {
    weak var delegate: pushingToVC?
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    static func create() -> CreatePopUpViewController {
        let storyboard = UIStoryboard.timeLineStoryboard.instantiateCreatePopUpViewController()
           return storyboard
       }
    @IBAction func actionCollections(_ sender: UIButton) {
        self.dismiss(animated: true) {
            
        }
        switch sender.tag {
        case 1:
            delegate?.selectingThePopUpVc(vcName: .Post)
            case 2:
             delegate?.selectingThePopUpVc(vcName: .GoLive)
            case 3:
             delegate?.selectingThePopUpVc(vcName: .GraduateSpot)
            case 4:
             delegate?.selectingThePopUpVc(vcName: .GraduateUni)
            case 5:
             delegate?.selectingThePopUpVc(vcName: .Deal)
        default:
            break
        }
    }
   
}
enum CreateVCList {
    case Post
    case GoLive
    case GraduateSpot
    case GraduateUni
    case Deal
}
