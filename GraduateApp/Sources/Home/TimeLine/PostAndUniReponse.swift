//
//  PostAndUniReponse.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 11/13/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import Foundation
import ObjectMapper

class PostAndUniResponse: DefaultResponse {
    var data: LoginResponseModel?
override func mapping(map: Map) {
  super.mapping(map: map)

}
// MARK: - POST LIST REQUEST
    class func requestToMyPost(params: [String : Any],showHud: Bool = true ,completionHandler:@escaping ((TimeLineResponseModel?) -> Void)) {
        APIManager(urlString: GraduateUrl.baseURL + "post/list", parameters: params, method: .get ).handleResponse(showProgressHud: showHud,completionHandler: { (response: TimeLineResponseModel) in
              print(response)
                  completionHandler(response)
              }, failureBlock: {
                completionHandler(nil)
              })
      }
}


