//
//  PostAndUniViewController.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 8/2/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import UIKit
import Popover
import AVFoundation
import Alamofire
import ESPullToRefresh
struct PostAndUniModel {
    var uniData: GraduateUniversityResponseModel?
    var myPost: TimeLineResponseModel?
    var numOfPage: Int
    var loadMore: Bool
}
class PostAndUniViewController: UIViewController {
    
    @IBOutlet weak var headerbtn: AnimatedButton!
    @IBOutlet var headerView: UIView!
    @IBOutlet weak var tableView: UITableView!
    var popupTableView = UITableView(frame: CGRect(x: 0, y: 0, width: 90, height: 130))
    var controllerType: String?
    var texts = ["Share", "Edit", "Delete"]
    var popover: Popover!
    fileprivate var popoverOptions: [PopoverOption] = [
        .type(.left),
        .blackOverlayColor(UIColor(white: 0.0, alpha: 0.6))
    ]
    var dataSource: PostAndUniModel? {
        didSet {
            tableView.reloadData()
        }
    }
    var postTypes: GraduateDetails? //used for the editing the post
    var editedIndex: IndexPath! //index path that is edited
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        registeriViews()
        dataSource = PostAndUniModel(uniData: nil, myPost: nil, numOfPage: 1, loadMore: false)
        if controllerType == "Uni" {
            requestingToMyUni(pageNo: 1, isLoadMore: false, showHud: true)
        } else if controllerType == "Post"{
            gettingAllPost(pageNo: 1, showLoader: true, isLoadMore: false)
        }
        tableView.reloadData()
        pullToRefresh()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if controllerType == "Uni" {
            self.title = "My Uni"
            headerbtn.setTitle("Create Uni")
        } else if controllerType == "Post" {
            self.title = "My Posts"
            headerbtn.setTitle("Create Posts")
        }
        navigationController?.setNavigationBarHidden(false, animated: true)
        currentTabBar?.setBar(hidden: true, animated: true)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        currentTabBar?.setBar(hidden: true, animated: true)
    }
//MARK:- PULL TO REFRESH
    func pullToRefresh() {
        
        self.tableView.es.addPullToRefresh {
            [unowned self] in
            if controllerType == "Post" {
                gettingAllPost(pageNo: 1, showLoader: false, isLoadMore: false)
            } else {
                requestingToMyUni(pageNo: 1, isLoadMore: false, showHud: false)
            }
        }
        
    }
    //MARK:- REGISTERING VIEWS
    func registeriViews() {
        tableView.register(UINib(nibName: "GraduateTableViewCell", bundle: nil), forCellReuseIdentifier: "GraduateTableViewCell")
        tableView.register(UINib(nibName: "PostCreationTableViewCell", bundle: nil), forCellReuseIdentifier: "PostCreationTableViewCell")
        tableView.register(UINib(nibName: "PostCreationTableViewCell", bundle: nil), forCellReuseIdentifier: "PostCreationTableViewCell")
        tableView.register(UINib(nibName: "PortFolioTableViewCell", bundle: nil), forCellReuseIdentifier: "PortFolioTableViewCell")
        tableView.register(UINib(nibName: "EventTableViewCell", bundle: nil), forCellReuseIdentifier: "EventTableViewCell")
        tableView.register(UINib(nibName: "DiscountTableViewCell", bundle: nil), forCellReuseIdentifier: "DiscountTableViewCell")
        tableView.register(UINib(nibName: "LiveTableViewCell", bundle: nil), forCellReuseIdentifier: "LiveTableViewCell")
        tableView.dataSource = self
        tableView.tableHeaderView = headerView
        tableView.delegate = self
    }
    //MARK:- THOUGHTS DETAILS
    func thoughtsDetails(commentId: Int, index: IndexPath!) {
       let vc = UIStoryboard.timeLineStoryboard.instantiateThoughtsVC()
       vc.commentId = commentId
       vc.index = index
       vc.delegate = self
       navigationController?.pushViewController(vc, animated: true)
   }
    
    //MARK:- RATING DETAILS
    func ratingDetailsAction(ratingId: Int, starredVlaue: String, starCount: Int) {
           let vc = UIStoryboard.timeLineStoryboard.instantiateRatingDetailsVC()
           vc.ratingId = ratingId
       vc.starredValue = starredVlaue
       vc.starCount = starCount
           navigationController?.pushViewController(vc, animated: true)
          }
    //MARK:- RATING POP UP
    func popingBottomVC(id: Int, indexToReload: Int, selectionType: GraduateDetails,starredValue: String) {
        let vc = HelperFunctions.ratingSelection(id: id, starredVlaue: "", reloadingIndex: indexToReload)
        vc.delegate = self
        vc.selectionType = selectionType
        vc.id = id
        vc.selectedIndex = Int(starredValue)
        vc.indexToReload = indexToReload
        present(vc, animated: true, completion: nil)
    }
    //MARK:- REQUESTING TO UNI
    func requestingToMyUni(pageNo: Int,isLoadMore: Bool = false, showHud: Bool = true) {
        GraduateUniversityResponse.requestToGetUserUni { [weak self](response) in
            guard let strongSelf = self else { return }
            strongSelf.tableView.es.stopPullToRefresh()
            print(response)
            strongSelf.dataSource?.uniData = response
            strongSelf.tableView.reloadData()
        }
    }
    
    //MARK:- GETTING USER POSTS
    func gettingAllPost(pageNo: Int = 1, showLoader: Bool = true, isLoadMore: Bool = false) {
        let posterId = UserDefaultsHandler.getUDValue(key: .userID)
        let params: [String : Any] = ["poster_type" : "user", "poster_id" : "\(posterId ?? 0)"]
        
        PostAndUniResponse.requestToMyPost(params: params, showHud: showLoader) { [weak self](response) in
            guard let strongSelf = self else { return }
            strongSelf.tableView.es.stopPullToRefresh()
            print(response,isLoadMore)
            strongSelf.tableView.tableFooterView = nil
            strongSelf.tableView.tableFooterView?.isHidden = true
            print(strongSelf.dataSource?.loadMore)
            if !isLoadMore {
                strongSelf.dataSource?.myPost = response
                strongSelf.tableView.reloadData()
            } else {
                if let allData = response?.data {
                    for data in allData {
                        strongSelf.dataSource?.myPost?.data?.append(data)
                    }
                }
                
            }
            if strongSelf.dataSource?.myPost?.data?.count ?? 0 < response?.meta?.total ?? 0 {
                strongSelf.dataSource?.loadMore = true
                strongSelf.dataSource?.numOfPage += 1
            } else {
                strongSelf.dataSource?.loadMore = false
            }
        }
    }
    @IBAction func btnAction(_ sender: Any) {
        if controllerType == "Uni" {
            let vc = UIStoryboard.timeLineStoryboard.instantiateCreateUniversityVC()
            
            navigationController?.pushViewController(vc, animated: true)
        } else {
            let vc = UIStoryboard.timeLineStoryboard.instantiateCreateSpotViewController()
            vc.currentTab?.setBar(hidden: true, animated: true)
            vc.vcType = .createPostFromOutside
            navigationController?.pushViewController(vc, animated: true)
        }
    }
    //MARK:- POPUP
    func popUp(type: ErrorType, message: String) {
        let vc = UIStoryboard.timeLineStoryboard.instantiateErrorCarMsgPopVC()
        vc.popupType = type
        vc.messageString = message
        vc.delegaet = self
        let cardPopup = SBCardPopupViewController(contentViewController: vc)
        cardPopup.show(onViewController: self)
    }
//MARK:- POP OVER
    func sharePopOver(sender: UIButton) {
        popupTableView.reloadData()
        if texts.count == 2 {
            popupTableView.frame = CGRect(x: 0, y: 0, width: 90, height: 80)
            
        } else {
            popupTableView.frame = CGRect(x: 0, y: 0, width: 90, height: 120)
            
        }
        popupTableView.delegate = self
        popupTableView.dataSource = self
        popupTableView.isScrollEnabled = false
        self.popover = Popover(options: self.popoverOptions)
        self.popover.willShowHandler = {
            print("willShowHandler")
        }
        self.popover.didShowHandler = {
            print("didDismissHandler")
        }
        self.popover.willDismissHandler = {
            print("willDismissHandler")
        }
        self.popover.didDismissHandler = {
            print("didDismissHandler")
        }
        
        self.popover.show(popupTableView, fromView: sender)
    }
    //MARK:- REQUEST TO DELTE POST
        func requestToDeletePost() {
            WatchResponse.requestToDeletePost(id: dataSource?.myPost?.data?[editedIndex.row].internalIdentifier ?? 0,showHud: true) { [weak self](response) in
                guard let strongSelf = self else { return }
                strongSelf.gettingAllPost(pageNo: 1, showLoader: true, isLoadMore: false)
            }
        }
//MARK:- PUSHING TO DETAILS
    func pushingToDetials(index: IndexPath) {
        tableView.deselectRow(at: index, animated: true)
        let vc = UIStoryboard.graduateStoryboard.instantiateGraduateDetailsViewController()
        vc.businessId = dataSource?.uniData?.data?[index.row].internalIdentifier
       // vc.delegate = self
        navigationController?.pushViewController(vc, animated: true)
    }
    //MARK:- FOLLOW API FOR UNIVERSITY
    func followUniversity(id: Int) {
        GraduateUniversityResponse.requestToFollowUniversity(userId: id) { (response) in
            
        }
    }
        //MARK:- UNFOLLOW API FOR UNIVERSITY
        func unfollowUniversity(id: Int) {
            GraduateUniversityResponse.requestToUnfollowUniversity(userId: id) { [weak self](response) in
               
                
            }
        }

        
////MARK:- RATING
//    func popingBottomVC(id: Int, indexToReload: Int, selectionType: GraduateDetails,starredValue: String) {
//        let vc = HelperFunctions.ratingSelection(id: id, starredVlaue: "", reloadingIndex: indexToReload)
//        vc.delegate = self
//        vc.selectionType = selectionType
//        vc.id = id
//        vc.selectedIndex = Int(starredValue)
//        vc.indexToReload = indexToReload
//        present(vc, animated: true, completion: nil)
//    }
    
}

//MARK:- TABLE VIEW DATASOURCE
extension PostAndUniViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == popupTableView {
            return texts.count
        } else {
            if controllerType == "Uni" {
                return dataSource?.uniData?.data?.count ?? 0
            } else  if controllerType == "Post" {
                return dataSource?.myPost?.data?.count ?? 0
            }
            
        }
        return 0
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == popupTableView {
            let cell = UITableViewCell(style: .default, reuseIdentifier: nil)
            cell.textLabel?.text = self.texts[(indexPath as NSIndexPath).row]
            cell.textLabel?.textColor = UIColor.init(hexString: Appcolors.text)
            return cell
        } else {
            if controllerType == "Uni" {
                headerbtn.setTitle("Create Uni", for: .normal)
                let cell = tableView.dequeueReusableCell(withIdentifier: "GraduateTableViewCell", for: indexPath) as! GraduateTableViewCell
                cell.titleLabel.text = dataSource?.uniData?.data?[indexPath.row].businessName
                cell.typeLabel.text = dataSource?.uniData?.data?[indexPath.row].businessType
                if dataSource?.uniData?.data?[indexPath.row].businessCoverImage == nil {
                    cell.mainImage.image = UIImage(named: "defaultPicture")
                } else {
                    cell.mainImage.setURLImage(imageURL: (GraduateUrl.imageUrl + (dataSource?.uniData?.data?[indexPath.row].businessCoverImage ?? "")))
                }
                
                cell.index = indexPath.row
               cell.delegate = self
                cell.userid = dataSource?.uniData?.data?[indexPath.row].internalIdentifier
                if dataSource?.uniData?.data?[indexPath.row].isFollowed == true {
                    cell.followBtn.setTitle("Followed")
                    cell.followBtn.backgroundColor = UIColor.init(hexString: Appcolors.buttons)
                    cell.followBtn.setTitleColor(.white, for: .normal)
                } else if dataSource?.uniData?.data?[indexPath.row].isOwnBusiness == true {
                    cell.followBtn.setTitle("Edit")
                    cell.followBtn.backgroundColor = .white
                    cell.followBtn.setTitleColor(UIColor.init(hexString: Appcolors.buttons), for: .normal)
                } else if dataSource?.uniData?.data?[indexPath.row].isOwnBusiness == false {
                    cell.followBtn.setTitle("Follow")
                    cell.followBtn.backgroundColor = .white
                    cell.followBtn.setTitleColor(UIColor.init(hexString: Appcolors.buttons), for: .normal)
                }
               // cell.delegate = self
                return cell
            } else {
                //MARK:- POST CREATION CELL
                if dataSource?.myPost?.data?[indexPath.row].portfolio != nil {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "PortFolioTableViewCell", for: indexPath) as! PortFolioTableViewCell
                
                    var portData = dataSource?.myPost?.data?.filter({ $0.portfolio != nil })
                    cell.subHeaderlabel.text = dataSource?.myPost?.data?[indexPath.row].portfolio?.addressString
                    
                    cell.delegate = self
                    cell.selectionType = .myPost
                    cell.postType = .portfolio
                    return cell.portFolioCell(cell: cell, indexPath: indexPath, dataSource: dataSource?.myPost?.data)
                } else if dataSource?.myPost?.data?[indexPath.row].offer != nil {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "DiscountTableViewCell", for: indexPath) as! DiscountTableViewCell
                    
                    cell.delegate = self
                    cell.selectionType = .offer
                    cell.postType = .offer
                    return cell.DiscountTVC(cell: cell, indexPath: indexPath, dataSource: dataSource?.myPost?.data)
                } else if dataSource?.myPost?.data?[indexPath.row].event != nil {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "EventTableViewCell", for: indexPath) as! EventTableViewCell
                    let eventData = dataSource?.myPost?.data?.filter {
                        $0.event != nil
                    }
                   // print(dataSource?.gradSpot?.data)
                    cell.delegate = self
                    cell.postType = .gradSpot
                    cell.selectionType = .gradSpot
                    cell.eventTypeLabel.text = dataSource?.myPost?.data?[indexPath.row].event?.eventType
                    return cell.eventTVC(cell: cell, indexPath: indexPath, dataSource: dataSource?.myPost?.data)
                } else if dataSource?.myPost?.data?[indexPath.row].hasLiveEnded == true {
                    print(dataSource?.myPost?.data?[indexPath.row].hasLiveEnded)
                     //MARK:- MYPOST CELL WITH LIVE TYPE VIDEOS
                     print("")
                     let cell = tableView.dequeueReusableCell(withIdentifier: "LiveTableViewCell", for: indexPath) as! LiveTableViewCell
                      cell.videoType = .liveVideo
                     cell.name = dataSource?.myPost?.data?[indexPath.row].postingAs?.name
                     
                     cell.delegate = self
                     cell.selectionType = .myPost
                      cell.postType = .live
                     let newDate = HelperFunctions.timeInterval(timeAgo: dataSource?.myPost?.data?[indexPath.row].createdAt ?? "")
                                            print(newDate)
                     cell.timeStampLabel.text = newDate
                    if dataSource?.myPost?.data?[indexPath.row].hasStarred == true {
                        cell.ratingBtn.setImage(UIImage(named: "star3"))
                    } else {
                        cell.ratingBtn.setImage(UIImage(named: "star"))
                    }
                     cell.locationLabel.text = dataSource?.myPost?.data?[indexPath.row].business?.businessName
                     cell.viewersLabel.text = "\(dataSource?.myPost?.data?[indexPath.row].viewsCount ?? 0)"
                    let filrurls = NSURL(string: (GraduateUrl.imageUrl + (dataSource?.myPost?.data?[indexPath.row].liveVideoMp4 ?? "")))
                               let firstAsset = AVURLAsset(url: filrurls! as URL)
                     cell.videoPlayer.videoAssets = [firstAsset]
                     cell.postData = dataSource?.myPost?.data?[indexPath.row]
                      cell.ratingNumLabel.text = "\(dataSource?.myPost?.data?[indexPath.row].starsCount ?? 0)"
                      cell.thoughtsNumLabel.text = "\(dataSource?.myPost?.data?[indexPath.row].comments?.count ?? 0)"
                      cell.commentId = dataSource?.myPost?.data?[indexPath.row].internalIdentifier
                      cell.index = indexPath
                      cell.captionLabel.text = dataSource?.myPost?.data?[indexPath.row].message
                     return cell
                     
                 } else if dataSource?.myPost?.data?[indexPath.row].hasLiveEnded == false {
                     let cell = tableView.dequeueReusableCell(withIdentifier: "LiveStreamingTableViewCell", for: indexPath) as! LiveStreamingTableViewCell
                  cell.camera = Camera(name: dataSource?.myPost?.data?[indexPath.row].message ?? "", isConnected: true, isLive: true, url: dataSource?.myPost?.data?[indexPath.row].liveVideoRtmp ?? "")
                  cell.setupPlayer()
                  cell.delegate = self
                  cell.selectionType = .live
                  cell.videoType = .liveVideo
                  cell.backgroundColor = .black
                  return cell.liveStreamingTVC(cell: cell, indexPath: indexPath, dataSource: dataSource?.myPost?.data)
                
            
        
      
    }else if dataSource?.myPost?.data?[indexPath.row].event == nil && dataSource?.myPost?.data?[indexPath.row].offer == nil && dataSource?.myPost?.data?[indexPath.row].portfolio == nil && dataSource?.myPost?.data?[indexPath.row].business == nil {
     //MARK:- CREATE POST WITH GALLERIES VIDEOS CELL
                    print(dataSource?.myPost?.data?[indexPath.row].galleries?.count)
                    
                    if dataSource?.myPost?.data?[indexPath.row].galleries?.count != 0 {
                        
    //MARK:- POST CREATION GALLERTIES VIDEO POST
                        if let index = dataSource?.myPost?.data?[indexPath.row].galleries?.firstIndex(where: { $0.mediaType == "Video"
                        }) {
                            let cell = tableView.dequeueReusableCell(withIdentifier: "LiveTableViewCell", for: indexPath) as! LiveTableViewCell
                            cell.index = indexPath
                            cell.postType = .myPost
                            cell.postData = dataSource?.myPost?.data?[indexPath.row]
                            cell.delegate = self
                            cell.videoType = .postVideo
                            cell.starCount = dataSource?.myPost?.data?[indexPath.row].starsCount
                            print(dataSource?.myPost?.data?[indexPath.row].starredValue)
                             cell.starreVlaue = dataSource?.myPost?.data?[indexPath.row].starredValue
                            let newDate = HelperFunctions.timeInterval(timeAgo: dataSource?.myPost?.data?[indexPath.row].createdAt ?? "")
                                                   print(newDate)
                            cell.timeStampLabel.text = newDate
                            cell.universityId = dataSource?.myPost?.data?[indexPath.row].internalIdentifier
                           
                            if dataSource?.myPost?.data?[indexPath.row].postingAsType == "business" {
                                  cell.name = dataSource?.myPost?.data?[indexPath.row].postingAs?.businessName
                            } else if dataSource?.myPost?.data?[indexPath.row].postingAsType == "user" {
                                 cell.name = dataSource?.myPost?.data?[indexPath.row].postingAs?.name
                            }
                            if dataSource?.myPost?.data?[indexPath.row].hasStarred == true {
                                  cell.ratingBtn.setImage(UIImage(named: "star3"))
                            } else {
                                 cell.ratingBtn.setImage(UIImage(named: "star"))
                            }
                          
                            cell.selectionType = .myPost
                            cell.locationLabel.text = dataSource?.myPost?.data?[indexPath.row].business?.businessName
                            cell.viewersLabel.text = "\(dataSource?.myPost?.data?[indexPath.row].viewsCount ?? 0)"
                            //cell.videoType = .postVideo
                            cell.captionLabel.text = dataSource?.myPost?.data?[indexPath.row].message
                            
                            let filrurls = NSURL(string: (GraduateUrl.imageUrl + (dataSource?.myPost?.data?[indexPath.row].galleries?[index].filepath ?? "")))
                            print(filrurls)
                            
                            //                            let url = (GraduateUrl.imageUrl + (dataSource?.myPost?.data?[indexPath.row].galleries?[index].filepath ?? ""))
                            let firstAsset = AVURLAsset(url: filrurls! as URL)
                            cell.videoPlayer.videoAssets = [firstAsset]
                            //                            let newDate = HelperFunctions.getDate(input: dataSource?[indexPath.row].portfolio?.business?.createdAt ?? "")
                            // cell.timeStampLabel.text = newDate?.timeAgo()
                            //cell.timeStampLabel.text = newDate
                            cell.ratingNumLabel.text = "\(dataSource?.myPost?.data?[indexPath.row].starsCount ?? 0)"
                            cell.thoughtsNumLabel.text = "\(dataSource?.myPost?.data?[indexPath.row].comments?.count ?? 0)"
                            cell.proImage.setURLImage(imageURL: (GraduateUrl.imageUrl + (dataSource?.myPost?.data?[indexPath.row].postingAs?.profile?.profileImage ?? "")))
                            cell.delegate = self
                            return cell
                            
                        }
    //MARK: GALLERY IMAGE POST
                        if let _ = dataSource?.myPost?.data?[indexPath.row].galleries?.firstIndex(where: { $0.mediaType == "Image"
                        }) {
                            let cell = tableView.dequeueReusableCell(withIdentifier: "PostCreationTableViewCell", for: indexPath) as! PostCreationTableViewCell
                            cell.delegate = self
                            cell.imageDataSource = dataSource?.myPost?.data?[indexPath.row].galleries ?? []
                            cell.selectionType = .myPost
                            cell.postType = .myPost
                            return cell.postTVC(cell: cell, indexPath: indexPath, dataSource: dataSource?.myPost?.data)
                        }
                    } else if dataSource?.myPost?.data?[indexPath.row].liveVideoMp4 != nil {
                        //MARK:- MYPOST CELL WITH LIVE TYPE VIDEOS
                        print("")
                        let cell = tableView.dequeueReusableCell(withIdentifier: "LiveTableViewCell", for: indexPath) as! LiveTableViewCell
                        cell.delegate = self
                        cell.postType = .myPost
                        cell.selectionType = .myPost
                        let newDate = HelperFunctions.timeInterval(timeAgo: dataSource?.myPost?.data?[indexPath.row].createdAt ?? "")
                                               print(newDate)
                        cell.timeStampLabel.text = newDate
                        cell.name = dataSource?.myPost?.data?[indexPath.row].postingAs?.name
                        cell.locationLabel.text = dataSource?.myPost?.data?[indexPath.row].business?.businessName
                        cell.viewersLabel.text = "\(dataSource?.myPost?.data?[indexPath.row].viewsCount ?? 0)"
                       let filrurls = NSURL(string: (GraduateUrl.imageUrl + (dataSource?.myPost?.data?[indexPath.row].liveVideoMp4 ?? "")))
                                  let firstAsset = AVURLAsset(url: filrurls! as URL)
                        cell.videoPlayer.videoAssets = [firstAsset]
                        cell.videoType = .liveVideo
                        cell.postData = dataSource?.myPost?.data?[indexPath.row]
                        return cell
                    } else {
                        let cell = tableView.dequeueReusableCell(withIdentifier: "PostCreationTableViewCell", for: indexPath) as! PostCreationTableViewCell
                        cell.delegate = self
                        cell.postType = .myPost
                        cell.selectionType = .myPost
                        return cell.postTVC(cell: cell, indexPath: indexPath, dataSource: dataSource?.myPost?.data)
                    }
                    
                }
            
          
            }
        }
        return UITableViewCell()
        
    }
    
    
}
//MARK:- TABLE VIEW DELEGATE
extension PostAndUniViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == popupTableView {
            return 40
        } else {
            return UITableView.automaticDimension
        }
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == popupTableView {
            if texts[indexPath.row] ==  "Share"{
                let shareText = "Hello, world!"
                
                let image = UIImage(named: "iconArrowDown")
                let vc = UIActivityViewController(activityItems: [shareText, image!], applicationActivities: [])
                present(vc, animated: true)
                self.popover.dismiss()
            } else if texts[indexPath.row] == "Edit" {
                self.popover.dismiss()
                switch postTypes {
                case .myPost:
                    let vc = UIStoryboard.timeLineStoryboard.instantiateCreateSpotViewController()
                    vc.vcType = .editPost
                    vc.editData = dataSource?.myPost?.data?[editedIndex.row]
                    navigationController?.pushViewController(vc, animated: true)
                case .gradSpot:
                    let vc = UIStoryboard.timeLineStoryboard.instantiateCreateSpotViewController()
                    vc.vcType = .editSpot
                    vc.editData = dataSource?.myPost?.data?[editedIndex.row]
                    navigationController?.pushViewController(vc, animated: true)
                case .live:
                    let vc = UIStoryboard.timeLineStoryboard.instantiateCreateSpotViewController()
                    vc.vcType = .editPost
                    vc.editData = dataSource?.myPost?.data?[editedIndex.row]
                    navigationController?.pushViewController(vc, animated: true)
                default:
                    break
                }
               
                
            } else if texts[indexPath.row] == "Delete" {
                popUp(type: .deletePost, message: "")
            }else {
                
                let vc = UIStoryboard.graduateStoryboard.instantiateReportVC()
                vc.modalPresentationStyle = .overFullScreen
                present(vc, animated: true, completion: nil)
                popover.dismiss()
            }
        } else if controllerType == "Uni"{
           pushingToDetials(index: indexPath)
        }
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if controllerType == "Post" {
            let allData = self.dataSource
            print(allData?.loadMore ?? false)
            print(indexPath.row,"indexPath",indexPath.section)
            print(dataSource?.uniData?.data?.count,"dataCount")
            guard allData?.loadMore == true, let dataCount = allData?.myPost?.data?.count, indexPath.row == dataCount - 1 else {return}
            print(dataCount)
            let lastSectionIndex = tableView.numberOfSections - 1
            print(lastSectionIndex)
            let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
            if indexPath.section == lastSectionIndex && indexPath.row == lastRowIndex {
                // print("this is the last cell")
                let spinner = UIActivityIndicatorView(style: .gray)
                spinner.startAnimating()
                spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
                tableView.tableFooterView = spinner
                tableView.tableFooterView?.isHidden = false
            }
            // print(allData.url)
            
            gettingAllPost(pageNo: dataSource!.numOfPage, showLoader: false, isLoadMore: dataSource!.loadMore)
        } else {
            
        }
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 800
    }
}
//MARK:- DELEGATE FROM  POST
extension PostAndUniViewController: SendingRatingFromPCTVC {
    func moreOptionFromPostTVC(sender: UIButton, id: Int, index: IndexPath, cell: PostCreationTableViewCell, isOwner: Bool, postType: GraduateDetails) {
        postTypes = postType
        editedIndex = index
        if isOwner == true {
            texts.removeAll()
            texts = ["Share", "Edit","Delete"]
            sharePopOver(sender: sender)
        } else {
            texts.removeAll()
            texts = ["Share", "Report"]
            sharePopOver(sender: sender)
        }
    }
    
  
  
    func ratingDetailFromPCTVC(ratingId: Int, starredValue: String, starCount: Int) {
        ratingDetailsAction(ratingId: ratingId, starredVlaue: "\(starCount)", starCount: starCount)
    }
    
    func outsideCommentPostTVC(comment: Comments?, index: IndexPath) {
        dataSource?.myPost?.data?[index.row].comments?.append(comment!)
        tableView.reloadRows(at: [index], with: .automatic)
    }
    func pushingTOProfileVC(id: Int, userType: String, isOwner: Bool, postAsType: String) {
        pushToProfile(id: id, userType: userType, isOwner: isOwner, postAsType: postAsType)
    }
    
   
    
    
    func PCTVCRating(id: Int, starValue: String, selectedindexToReload: Int, selectionType: GraduateDetails) {
        popingBottomVC(id: id, indexToReload: selectedindexToReload, selectionType: selectionType, starredValue: starValue)
    }
    func feedbackActionPCTVC(commentId: Int, outsideComment: Bool, index: IndexPath) {
        if outsideComment == false {
            thoughtsDetails(commentId: commentId, index: index)
        } else {
            
        }
    }
    
  
    func fullScreenImage(vc: UIViewController) {
        currentTabBar?.setBar(hidden: true, animated: true, duration: 0.0, completion: { (true) in
            self.present(vc, animated: true, completion: nil)
        })
        //  currentTabBar?.setBar(hidden: true, animated: true)
        
    }
    
    func thoughtsAction() {
//        let vc = UIStoryboard.timeLineStoryboard.instantiateThoughtsVC()
//        navigationController?.pushViewController(vc, animated: true)
    }
    
  
//MARK:- PUSH TO PROFILE FUNC
    func pushToProfile(id: Int, userType: String, isOwner: Bool, postAsType: String) {
        if postAsType == "user" {
            if isOwner == true {
                let vc = UIStoryboard.timeLineStoryboard.instantiateUserProfileVC()
                navigationController?.pushViewController(vc, animated: true)
            } else {
                let vc = UIStoryboard.timeLineStoryboard.instantiateOtherUserProfileVC()
                vc.userId = id
                navigationController?.pushViewController(vc, animated: true)
            }
        } else {
            let vc = UIStoryboard.timeLineStoryboard.instantiateGraduateDetailsViewController()
            vc.businessId = id
            navigationController?.pushViewController(vc, animated: true)
        }
    }
    
}
//MARK: REVCEIVING RATING FROM THE DISCOUNTTVC
extension PostAndUniViewController: SedingRatingsToTimeLineVC {
    func moreOptionFromDiscountTVC(sender: UIButton, id: Int, isOwnBusiness: Bool, postType: GraduateDetails, index: IndexPath) {
        self.postTypes = postType
        self.editedIndex = index
        if isOwnBusiness == true {
            sharePopOver(sender: sender)
        } else {
            texts.removeAll()
            texts = ["Share", "Report"]
            sharePopOver(sender: sender)
        }
    }
 
    func commentOutsideDiscountTVC(comment: Comments?, index: IndexPath) {
        dataSource?.myPost?.data?[index.row].comments?.append(comment!)
        tableView.reloadRows(at: [index], with: .automatic)
    }
    func pushingTOProfileVCFromDisc(id: Int, userType: String, isOwner: Bool, postAsType: String) {
        pushToProfile(id: id, userType: userType, isOwner: isOwner, postAsType: postAsType)
    }
    func feedbackAction(commentId: Int, outsideComment: Bool, index: IndexPath) {
        if outsideComment == false {
                  thoughtsDetails(commentId: commentId, index: index)
                  
              } else {
                  
              }
    }
    

    func sendingRating(id: Int, starValue: String, selectedindexToReload: Int, selectionType: GraduateDetails) {
        popingBottomVC(id: id, indexToReload: selectedindexToReload, selectionType: selectionType, starredValue: starValue)
    }
    func discratingDetails(ratingId: Int, starredValue: String, starCount: Int) {
       ratingDetailsAction(ratingId: ratingId, starredVlaue: starredValue, starCount: starCount)
    }
    func fullscreen(vc: UIViewController) {
        currentTabBar?.setBar(hidden: true, animated: true)
        present(vc, animated: true, completion: nil)
    }
   
}
//MARK:- DELEGATE RATING ACTION

extension PostAndUniViewController: SendingActionFromRatingVC {
    func sendingActionWithType(type: RatingActionTypes, id: Int, reloadingIndex: Int, ratingNum: Int, selectionType: GraduateDetails) {
        switch type{
        case .requestforRating:
            reqestForPostRating(id: id, reloadingIndex: reloadingIndex, ratingNum: ratingNum, selectionType: selectionType)
        case .reuestForUnratingPost:
            reqestForPostUnRating(id: id, reloadingIndex: reloadingIndex, ratingNum: ratingNum, selectionType: selectionType)
     
        }
    }
//MARK:- RATE POST
          func reqestForPostRating(id: Int,reloadingIndex: Int,ratingNum: Int,selectionType: GraduateDetails) {
            
              var headers: [String : Any]?
              if let token = UserDefaultsHandler.getUDValue(key: UDkey.token) as? String {
                  if headers == nil {
                      headers = ["Authorization": "Bearer \(token)", "Accept": "application/json"]
                  } else {
                      headers!["Authorization"] = "Bearer \(token)"
                      headers!["Accept"] = "application/json"
                  }
              }
              let url = (GraduateUrl.ratePostURL + "\(id)")
               print(url,ratingNum,selectionType)
              Alamofire.request(url,
                                method: .post,
                                parameters: ["star_value": "\(ratingNum + 1)"],headers: (headers as! HTTPHeaders))
                  .validate()
                  .responseJSON { [weak self]response in
                      guard let strongSelf = self else { return }
                      if let value = response.result.value as? [String : Any] {
                        strongSelf.ratingAfterApiCallOfEvent(reloadingIndex: reloadingIndex, ratingNum: ratingNum)
//                        switch selectionType {
//                        case .gradSpot:
//                             strongSelf.ratingAfterApiCallOfEvent(reloadingIndex: reloadingIndex, ratingNum: ratingNum)
//                        case .offer:
//                            ratingAfterApiCallOFOffer(reloadingIndex: reloadingIndex, ratingNum: ratingNum)
//                        default:
//                            break
//                        }
                      
                          
                      }
                      
              }
          }
       
//MARK:- REQUEST FOR UNRATING POST
       func reqestForPostUnRating(id: Int,reloadingIndex: Int,ratingNum: Int,selectionType: GraduateDetails) {
           var headers: [String : Any]?
           if let token = UserDefaultsHandler.getUDValue(key: UDkey.token) as? String {
               if headers == nil {
                   headers = ["Authorization": "Bearer \(token)", "Accept": "application/json"]
               } else {
                   headers!["Authorization"] = "Bearer \(token)"
                   headers!["Accept"] = "application/json"
               }
           }
           let url = (GraduateUrl.unratePostURL + "\(id)")
           print(url,ratingNum,selectionType)
           Alamofire.request(url,method: .delete,headers: (headers as! HTTPHeaders))
               .validate()
               .responseJSON { [weak self]response in
                   guard let strongSelf = self else { return }
                   if let value = response.result.value as? [String : Any] {
                    strongSelf.unratingAfterApiCallOfEvent(reloadingIndex: reloadingIndex, ratingNum: ratingNum)
//                    switch selectionType {
//                    case .gradSpot:
//                         strongSelf.unratingAfterApiCallOfEvent(reloadingIndex: reloadingIndex, ratingNum: ratingNum)
//                    case .offer:
//                        strongSelf.unratingAfterApiCallOfOffer(reloadingIndex: reloadingIndex, ratingNum: ratingNum)
//                    default:
//                        break
//                    }
                      
                       print(value)
                       print(reloadingIndex)
                       
                   }
                   
           }
       }
       func ratingAfterApiCallOfEvent(reloadingIndex: Int, ratingNum: Int) {
           print(dataSource?.myPost?.data?[reloadingIndex].hasStarred)
           if dataSource?.myPost?.data?[reloadingIndex].hasStarred == true {
              
           } else {
               
               dataSource?.myPost?.data?[reloadingIndex].starsCount! += 1
           }
           dataSource?.myPost?.data?[reloadingIndex].starredValue = "\((ratingNum + 1))"
           print(dataSource?.myPost?.data?[reloadingIndex].starredValue)
           print(reloadingIndex)
           dataSource?.myPost?.data?[reloadingIndex].hasStarred = true
           tableView.reloadData()
       }
       func unratingAfterApiCallOfEvent(reloadingIndex: Int, ratingNum: Int) {
           dataSource?.myPost?.data?[reloadingIndex].hasStarred = false
           if  dataSource?.myPost?.data?[reloadingIndex].starsCount ?? 0 > 0 {
               dataSource?.myPost?.data?[reloadingIndex].starsCount! -= 1
           } else {
               
           }
           dataSource?.myPost?.data?[reloadingIndex].starredValue = "\(0)"
           tableView.reloadData()
       }
//    func ratingAfterApiCallOFOffer(reloadingIndex: Int, ratingNum: Int) {
//        if dataSource?.data?.data?[reloadingIndex].hasStarred == true {
//            return
//        } else {
//
//            dataSource?.data?.data?[reloadingIndex].starsCount! += 1
//        }
//        dataSource?.Offer?.data?[reloadingIndex].starredValue = "\((ratingNum + 1))"
//        print(reloadingIndex)
//        dataSource?.Offer?.data?[reloadingIndex].hasStarred = true
//        tableView.reloadData()
//    }
//    func unratingAfterApiCallOfOffer(reloadingIndex: Int, ratingNum: Int) {
//        dataSource?.Offer?.data?[reloadingIndex].hasStarred = false
//        if  dataSource?.Offer?.data?[reloadingIndex].starsCount ?? 0 > 0 {
//            dataSource?.Offer?.data?[reloadingIndex].starsCount! -= 1
//        } else {
//
//        }
//        dataSource?.Offer?.data?[reloadingIndex].starredValue = "\(0)"
//        tableView.reloadData()
//    }
}

//MARK: RECEIVING RATING FROM LIVE TVC
extension PostAndUniViewController: SendingRatingFromLiveTVC {
    func moreOptionsFromLiveTVC(sender: UIButton, index: IndexPath, isOwner: Bool, postType: GraduateDetails) {
        texts.removeAll()
        
          postTypes = postType
          editedIndex = index
          if isOwner == true {
              texts.removeAll()
              texts = ["Share", "Edit","Delete"]
              sharePopOver(sender: sender)
          } else {
              texts.removeAll()
              texts = ["Share", "Report"]
              sharePopOver(sender: sender)
          }
    }
    
    
    func commentOutsideLiveTVC(comment: Comments?, index: IndexPath) {
        dataSource?.myPost?.data?[index.row].comments?.append(comment!)
        tableView.reloadRows(at: [index], with: .automatic)
    }
    func pushingTOProfileVC(id: Int, userType: String, isOwner: Bool, postasType: String) {
        print(id,userType,isOwner)
        if postasType == "user" {
        if isOwner == true {
                     let vc = UIStoryboard.timeLineStoryboard.instantiateUserProfileVC()
                     navigationController?.pushViewController(vc, animated: true)
                     } else {
                         let vc = UIStoryboard.timeLineStoryboard.instantiateOtherUserProfileVC()
                   vc.userId = id
                                navigationController?.pushViewController(vc, animated: true)
                     }
        } else {
            let vc = UIStoryboard.timeLineStoryboard.instantiateGraduateDetailsViewController()
            vc.businessId = id
            navigationController?.pushViewController(vc, animated: true)
        }
    }
    
   
    
    func feedbackActionLiveTVC(commentId: Int, index: IndexPath!) {
       thoughtsDetails(commentId: commentId, index: index)
                  
    }
    func sendingRatings(id: Int, starValue: String, selectedindexToReload: Int, selectionType: GraduateDetails) {
        popingBottomVC(id: id, indexToReload: selectedindexToReload, selectionType: selectionType, starredValue: starValue)
    }
    
    
    func liveRatingDetails(ratingId: Int, starredVlaue: String, starCount: Int) {
      ratingDetailsAction(ratingId: ratingId, starredVlaue: starredVlaue, starCount: starCount)
    }
    
 
    
    func sendingVideoUrlForFullScreen() {
        
    }

    
    //MARK:- RATING DETAILS
//    func ratingDetails(ratingId: Int, starredVlaue: String, starCount: Int) {
////           let vc = UIStoryboard.timeLineStoryboard.instantiateRatingDetailsVC()
////           vc.ratingId = ratingId
////       vc.starredValue = starredVlaue
////       vc.starCount = starCount
////        self.navigationController?.pushViewController(vc, animated: true)
//        let navc = UIStoryboard.timeLineStoryboard.instantiateRatingDetailsVC()
//        navigationController?.pushViewController(navc, animated: true)
//    }
}
//MARK: RECEIVING RATINGS FROM THE EVENT TVC
extension PostAndUniViewController: SendingRatingFromEventTVC {
    func moreOPtionFromEventTVC(sender: UIButton, id: Int, index: IndexPath, cell: EventTableViewCell, isOwner: Bool, postType: GraduateDetails) {
        postTypes = postType
        editedIndex = index
        if isOwner == true {
            texts.removeAll()
            texts = ["Share", "Edit","Delete"]
            sharePopOver(sender: sender)
        } else {
            texts.removeAll()
            texts = ["Share", "Report"]
            sharePopOver(sender: sender)
        }
    }
    
    func outsideCommentEventTVC(comment: Comments?, index: IndexPath) {
        dataSource?.myPost?.data?[index.row].comments?.append(comment!)
        tableView.reloadRows(at: [index], with: .automatic)
    }
    
    func pushingToProfileFromSpot(id: Int, userType: String, isOwner: Bool, postAsType: String) {
        pushToProfile(id: id, userType: userType, isOwner: isOwner, postAsType: postAsType)
       
    }
    func eventFeedbackAction(commentId: Int, index: IndexPath) {
         thoughtsDetails(commentId: commentId, index: index)
    }
   
    func sendingRatingEventTVC(id: Int, starValue: String, selectedindexToReload: Int, selectionType: GraduateDetails) {
        print(id)
        popingBottomVC(id: id, indexToReload: selectedindexToReload, selectionType: selectionType, starredValue: starValue)
    }
    
    func attendIgnoreEventTVC(universityId: Int, attendState: Bool, index: IndexPath!, attendeesCount: Int) {
        let url: String?
        print(universityId)
        let id = dataSource?.myPost?.data?[index.row].event?.internalIdentifier
        var attendeeCount = attendeesCount
        if attendState == true {
            url = (GraduateUrl.attendEventURL + "\(id ?? 0)")
            
            //attendeeCount = attendeesCount + 1
            
        } else {
            url = (GraduateUrl.unAttendEventURL + "\(id ?? 0)")
            //attendeeCount = attendeesCount - 1
        }
        
        GraduateDetailsResponse.requestToAttendIgnoreEvents(url: url ?? "") { [weak self](response) in
            guard let strongSelf = self else { return }
           print(response)
            
        }
    }
  
    func eventRatingDetails(ratingId: Int, starredVlaue: String, starCount: Int) {
       ratingDetailsAction(ratingId: ratingId, starredVlaue: starredVlaue, starCount: starCount)
    }
 
    func fullScreenAction(vc: UIViewController) {
        currentTabBar?.setBar(hidden: true, animated: true)
        present(vc, animated: true, completion: nil)
    }
   
    func feedbackActionEventTVC() {
        thoughtsDetails(commentId: 0, index: [])
    }
    func sendingRatingEventTVC() {
        
    }
}
//MARK: RECEIVING RATING FROM THE PFTVC
extension PostAndUniViewController: SendingRatingFromPFTVC{
    func pftvcthoughtsDetails(commentId: Int, outsideComment: Bool, index: IndexPath) {
        if outsideComment == false {
            thoughtsDetails(commentId: commentId, index: index)
        } else {
            
        }
    }
    func moreOPtionFromPortTVC(sender: UIButton, id: Int, index: IndexPath, cell: PortFolioTableViewCell, isOwner: Bool, postType: GraduateDetails) {
        postTypes = postType
        editedIndex = index
        if isOwner == true {
            texts.removeAll()
            texts = ["Share", "Edit","Delete"]
            sharePopOver(sender: sender)
        } else {
            texts.removeAll()
            texts = ["Share", "Report"]
            sharePopOver(sender: sender)
        }
    }
    func outsideCommentPortFolioTVC(comment: Comments?, index: IndexPath) {
        dataSource?.myPost?.data?[index.row].comments?.append(comment!)
        tableView.reloadRows(at: [index], with: .automatic)
    }
    
    func pushingTOProfileVCFromPFTVC(id: Int, userType: String, isOwner: Bool, postAsType: String) {
        pushToProfile(id: id, userType: userType, isOwner: isOwner, postAsType: postAsType)
    }
    
//    func moreOPtionFromPortTVC(sender: UIButton, id: Int, index: IndexPath, cell: PortFolioTableViewCell, isOwner: Bool) {
//        if isOwner == true {
//            sharePopOver(sender: sender)
//        } else {
//            texts.removeAll()
//            texts = ["Share", "Report"]
//            sharePopOver(sender: sender)
//        }
//    }
    func pftvcRating(id: Int, starValue: String, selectedindexToReload: Int, selectionType: GraduateDetails) {
        popingBottomVC(id: id, indexToReload: selectedindexToReload, selectionType: .portfolio, starredValue: starValue)
    }
    func pftvcratingDetails(ratingId: Int, starredValue: String, starCount: Int) {
        let vc = UIStoryboard.timeLineStoryboard.instantiateRatingDetailsVC()
        vc.ratingId = ratingId
        vc.starredValue = starredValue
        vc.starCount = starCount
        navigationController?.pushViewController(vc, animated: true)
    }
   
    func sendingThoughts(id: Int, param: [String : Any]) {
        
    }
    func pftvcthoughtsDetails(commentId: Int) {
        thoughtsDetails(commentId: commentId, index: [])
    }
   
    
    func fullScreen(vc: UIViewController) {
        currentTabBar?.setBar(hidden: true, animated: false)
        present(vc, animated: true, completion: nil)
    }
    func feedbackActionPFTVC() {
        thoughtsDetails(commentId: 0, index: [])
    }
    
   
}



//MARK:- THOUGHTS DELEGATE
extension PostAndUniViewController: SendingDataFromThoughtsVC {
    func sendingData(index: IndexPath, thoughtsCount: Int, commentsData: [Comments]) {
        
        dataSource?.myPost?.data?[index.row].comments = commentsData
        tableView.reloadRows(at: [index], with: .automatic)
    }
    
    
}
//MARK:- DELEGATE FROM LIVE STREAMING
extension PostAndUniViewController: SendingRatingFromLiveStreamingTVC {
    func sendingRatingsFromLiveStreamingTVC(id: Int, starValue: String, selectedindexToReload: Int, selectionType: GraduateDetails) {
        popingBottomVC(id: id, indexToReload: selectedindexToReload, selectionType: selectionType, starredValue: starValue)
    }
    func moreOptionsFromLiveStreamingTVC(sender: UIButton, index: IndexPath, isOwner: Bool, postType: GraduateDetails) {
        postTypes = postType
        editedIndex = index
        if isOwner == true {
            texts.removeAll()
            texts = ["Share", "Edit","Delete"]
            sharePopOver(sender: sender)
        } else {
            texts.removeAll()
            texts = ["Share", "Report"]
            sharePopOver(sender: sender)
        }
    }
    func feedbackActionFromLiveStreamingTVC(commentId: Int, index: IndexPath!) {
//        if outsideComment == false {
            thoughtsDetails(commentId: commentId, index: index)
//        } else {
//
//        }
    }
    func sendingVideoUrlForFullScreenFromLiveStreamingTVC() {
        
    }
    func pushingTOProfileVCFromLiveStreamingTVC(id: Int, userType: String, isOwner: Bool, postasType: String) {
        pushToProfile(id: id, userType: userType, isOwner: isOwner, postAsType: postasType)
    }
    func liveRatingDetailsFromLiveStreamingTVC(ratingId: Int, starredVlaue: String, starCount: Int) {
        ratingDetailsAction(ratingId: ratingId, starredVlaue: starredVlaue, starCount: starCount)
       
    }
    func commentOutsideFromLiveStreamingTVC(comment: Comments?, index: IndexPath) {
        dataSource?.myPost?.data?[index.row].comments?.append(comment!)
        tableView.reloadRows(at: [index], with: .automatic)
    }
}
//MARK:- ERROR CHAT VC DELEGATE
extension PostAndUniViewController: SendingActionFromErrorChat {
    func sendingAction() {
        
    }
    func sendingCancelAction() {
        
    }
    func adctionwithType(type: ErrorType) {
        switch type {
        case .deletePost:
            requestToDeletePost()
        default:
            break
        }
    }
}

extension PostAndUniViewController: FollowBtnActionFrmGTBC {
    func followAndEditAction() {
        
    }
    
    func indexPath(index: Int, btnState: String, userId: Int) {
          if btnState == "Follow" {
            followUniversity(id: userId)
        } else if btnState == "Followed" {
            unfollowUniversity(id: userId)
        } else if btnState == "Edit" {
            let vc = UIStoryboard.graduateStoryboard.instantiateGraduateDetailsViewController()
            vc.businessId = dataSource?.uniData?.data?[index].internalIdentifier
                   navigationController?.pushViewController(vc, animated: true)
            
        }
    }
    
   
    
}
