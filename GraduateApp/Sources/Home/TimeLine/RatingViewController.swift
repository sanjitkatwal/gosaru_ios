//
//  RatingViewController.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 7/14/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import UIKit
protocol SendingActionFromRatingVC: class {
    func sendingActionWithType(type: RatingActionTypes,id: Int, reloadingIndex: Int, ratingNum: Int, selectionType: GraduateDetails)
}
class RatingViewController: BottomPopupViewController {
    
    @IBOutlet weak var collectionVIew: UICollectionView!
    @IBOutlet weak var tableVIew: UITableView!
    var height: CGFloat?
    var topCornerRadius: CGFloat?
    var presentDuration: Double?
    var dismissDuration: Double?
    var shouldDismissInteractivelty: Bool?
    var type: bottomPopUpType?
    var controllerType: String?
    var dataSource: [RatingModel] = [RatingModel]()
    var selectedRow: Bool?
    var id: Int?
    var selectedIndex: Int?
    var indexToReload: Int?
    var selectionType: GraduateDetails? //for recognizing the cells for rating and unrating post
    weak var delegate: SendingActionFromRatingVC?
    override func viewDidLoad() {
        super.viewDidLoad()
        dataSource = [RatingModel(numOfStars: 1, selectedRow: false),RatingModel(numOfStars: 2, selectedRow: false), RatingModel(numOfStars: 3, selectedRow: false), RatingModel(numOfStars: 4, selectedRow: false), RatingModel(numOfStars: 5, selectedRow: false)]
        tableVIew.dataSource = self
        tableVIew.delegate = self
        // Do any additional setup after loading the view.
        //selectedIndex = UserDefaults.standard.integer(forKey: "ratingValue")
        print(selectedIndex)
        tableVIew.reloadData()
        tableVIew.tableFooterView = UIView()
        print(selectedIndex)
    }
    override var popupHeight: CGFloat { return height ?? CGFloat(300) }
    
    override var popupTopCornerRadius: CGFloat { return topCornerRadius ?? CGFloat(10) }
    
    override var popupPresentDuration: Double { return presentDuration ?? 1.0 }
    
    override var popupDismissDuration: Double { return dismissDuration ?? 1.0 }
    
    override var popupShouldDismissInteractivelty: Bool { return shouldDismissInteractivelty ?? true }
    
    override var popupDimmingViewAlpha: CGFloat { return BottomPopupConstants.kDimmingViewDefaultAlphaValue }
    
    //MARK:- RATE POST API
    func requestForRatingPost(id: Int, raingNum: Int) {
        delegate?.sendingActionWithType(type: .requestforRating, id: id, reloadingIndex: indexToReload ?? 0, ratingNum: selectedIndex ?? 0, selectionType: selectionType!)
//        RatingResponse.requestToRatePost(id: id, ratingNum: raingNum) { [weak self](response) in
//            guard let strongSelf = self else { return }
//  print(strongSelf.indexToReload)
//            strongSelf.delegate?.sendingActionWithType(type: .requestforRating, id: id, reloadingIndex: strongSelf.indexToReload ?? 0)
//               }
    }
    //MARK:- UNRATE POST API
    func requestForUnRatingPost(id: Int) {
        
        delegate?.sendingActionWithType(type: .reuestForUnratingPost, id: id, reloadingIndex: indexToReload ?? 0, ratingNum: selectedIndex ?? 0, selectionType: selectionType!)
        
    }
    
}
extension RatingViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RatingTableViewCell", for: indexPath) as! RatingTableViewCell
       
      if controllerType == "Rating Review" {
          cell.likeCountLabel.isHidden = true
      }
        
        if indexPath.row == 0 {
            if selectedIndex == 1{
                cell.collectionDataSource = 1
                           cell.likeCountLabel.text = "\(1)"
                           cell.selectedRow = true
                           return cell
            } else {
                cell.collectionDataSource = 1
                                          cell.likeCountLabel.text = "\(0)"
                                          cell.selectedRow = false
                                          return cell
            }
           
        } else if indexPath.row == 1 {
            if selectedIndex == 2{
                           cell.collectionDataSource = 2
                                      cell.likeCountLabel.text = "\(2)"
                                      cell.selectedRow = true
                                      return cell
                       } else {
                           cell.collectionDataSource = 2
                                                     cell.likeCountLabel.text = "\(0)"
                                                     cell.selectedRow = false
                                                     return cell
                       }
        } else if indexPath.row == 2 {
            if selectedIndex == 3{
                                      cell.collectionDataSource = 3
                                                 cell.likeCountLabel.text = "\(3)"
                                                 cell.selectedRow = true
                                                 return cell
                                  } else {
                                      cell.collectionDataSource = 3
                                                                cell.likeCountLabel.text = "\(0)"
                                                                cell.selectedRow = false
                                                                return cell
                                  }
        } else if indexPath.row == 3 {
            if selectedIndex == 4{
                                                 cell.collectionDataSource = 4
                                                            cell.likeCountLabel.text = "\(4)"
                                                            cell.selectedRow = true
                                                            return cell
                                             } else {
                                                 cell.collectionDataSource = 4
                                                                           cell.likeCountLabel.text = "\(0)"
                                                                           cell.selectedRow = false
                                                                           return cell
                                             }
        } else if indexPath.row == 4 {
            if selectedIndex == 5{
                                                 cell.collectionDataSource = 5
                                                            cell.likeCountLabel.text = "\(5)"
                                                            cell.selectedRow = true
                                                            return cell
                                             } else {
                                                 cell.collectionDataSource = 5
                                                                           cell.likeCountLabel.text = "\(0)"
                                                                           cell.selectedRow = false
                                                                           return cell
                                             }
//
    }
        
    return UITableViewCell()
    }
}
extension RatingViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(dataSource)
        print(selectedIndex)
        
        if selectedIndex == indexPath.row + 1{
            requestForUnRatingPost(id: id ?? 0)
        } else {
             selectedIndex = indexPath.row
            requestForRatingPost(id: id ?? 0, raingNum: selectedIndex ?? 0)
        }
        tableView.reloadData()
        self.dismiss(animated: true, completion: nil)
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
}
struct RatingModel {
    var numOfStars: Int?
    var selectedRow: Bool?
}

