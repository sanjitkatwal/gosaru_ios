//
//  DeletePostPopUp.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 7/28/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import UIKit
protocol SendingActionFromDeletePostPopUP: class {
    func reportNowResult(value: Bool)
}
class DeletePostPopUp: UIViewController {
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var cancelbtn: AnimatedButton!
    @IBOutlet weak var okBtn: AnimatedButton!
    weak var delegate: SendingActionFromDeletePostPopUP?
    var popUpType: String?
    var message: String?
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        if popUpType == "Logout" {
            okBtn.isHidden = false
            cancelbtn.isHidden = false
            okBtn.setTitle("Logout", for: .normal)
            headerLabel.text = ""
            messageLabel.text = "Are you sure you want to log out?"
        } else if popUpType == "Report Now"{
            cancelbtn.isHidden = true
            okBtn.isHidden = false
            messageLabel.text = "Reported Successfully!!!"
        } else if popUpType == "Post Edit" {
            cancelbtn.isHidden = true
            okBtn.isHidden = false
            messageLabel.text = "Updated Successfully!!!"
        } else if popUpType == "Delete Account" {
            cancelbtn.isHidden = true
            okBtn.isHidden = false
            messageLabel.text = "Deleted Successfully"
        } else if popUpType == "LoginError" {
            cancelbtn.isHidden = true
            okBtn.isHidden = false
            messageLabel.text = message
        }
    }
    
    @IBAction func cancelAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
        
    }
    
    @IBAction func okaction(_ sender: Any) {
        if popUpType == "Logout" {
            UserDefaultsHandler.removeUD(key: .token)
            print(UserDefaultsHandler.getUDValue(key: .token))
            if #available(iOS 13.0, *) {
                                 let mySceneDelegate = self.view.window?.windowScene?.delegate as? SceneDelegate
                                  mySceneDelegate?.loadLoginPage()
                             } else {
                                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                          appDelegate.loadLoginPage()
                                 // Fallback on earlier versions
                             }
            self.dismiss(animated: true, completion: nil)
        } else if popUpType == "Report Now"{
        self.dismiss(animated: true, completion: nil)
            delegate?.reportNowResult(value: true)
        } else if popUpType == "Post Edit" {
            self.dismiss(animated: true, completion: nil)
            delegate?.reportNowResult(value: true)
        } else if popUpType == "Delete Account" {
            self.dismiss(animated: true, completion: nil)
            delegate?.reportNowResult(value: true)
        } else if popUpType == "LoginError" {
            let transition: CATransition = CATransition()
            transition.duration = 0.5
            transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
            transition.type = CATransitionType.fade
            transition.subtype = CATransitionSubtype.fromBottom
            self.view.window!.layer.add(transition, forKey: nil)
            self.dismiss(animated: false, completion: nil)
           // self.dismiss(animated: true, completion: nil)
        }

    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
