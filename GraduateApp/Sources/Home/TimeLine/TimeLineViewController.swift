//
//  TimeLineViewController.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 6/24/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import UIKit
import AMPopTip
import Popover
import AVFoundation
import Alamofire
import ESPullToRefresh
struct TimeLineViewModel {
    var data: TimeLineResponseModel?
    var numOfPage: Int?
    var loadMore: Bool?
}
class TimeLineViewController: UIViewController {
    
    @IBOutlet weak var keyboardHeightLayoutConstraint: NSLayoutConstraint!
    @IBOutlet weak var addBtn: UIButton!
    @IBOutlet weak var messageBtn: UIButton!
    @IBOutlet weak var notificationBtn: UIButton!
    @IBOutlet weak var searchBtn: UIButton!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    @IBOutlet weak var topStackView: UIStackView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var privateBtn: AnimatedButton!
    @IBOutlet weak var publicBtn: AnimatedButton!
    @IBOutlet weak var infoBtn: AnimatedButton!
    @IBOutlet weak var infoView: UIView!
   // public var type: ESRefreshExampleType = .defaulttype
    var videoTVC: Bool?
    var keyboardOpen = false
    var cellHeights: [IndexPath : CGFloat] = [:]
    var currentOffset: CGFloat?
    var params = [String : Any]()
    var dataSource: TimeLineViewModel? {
        didSet {
            tableView.reloadData()
        }
    }
    var reportId: Int? //post id
    var tableViewDataSource: TimeLineViewModel? {
        didSet {
            tableView.reloadData()
        }
    }
    var postTypes: GraduateDetails? //used for the editing the post
    var editedIndex: IndexPath! //index path that is edited
    var timelinTypes: TimelineViews?
    var refreshControl = UIRefreshControl()
    var pvBtn = false
    var pubBtn = false
    var visibilityType: String? = "public"
    var square: UIView!
    var timeLinCustomView: TimeLineCreate?
    var timelineMoreView: UIView?
    var direction = PopTipDirection.down
    var topRightDirection = PopTipDirection.down
    private var lastContentOffset: CGFloat = 0
    let minimumConstantValue = CGFloat(-96)
    let popTip = PopTip()
    let createPoptip = PopTip()
    let morePopTip = PopTip()
    var createPopOpen: Bool = false
    var fullScreen: Bool?
    var popupTableView = UITableView(frame: CGRect(x: 0, y: 0, width: 90, height: 120))
    var texts = ["Share", "Edit", "Delete"]
    var popover: Popover!
    var privatePopTip = PopTip()
    fileprivate var popoverOptions: [PopoverOption] = [
        .type(.left),
        .blackOverlayColor(UIColor(white: 0.0, alpha: 0.6))
    ]
    override func viewDidLoad() {
        super.viewDidLoad()
       
        sendingDeviceToken()
        statusBarSetup()
        addButtonsOntop()
        registeringViews()
        //addData()
        initialPopTipAndOtherUISetup()
        
        publicDefaultSelection()
        popTipHandler()
        tableView.dataSource = self
        tableView.delegate = self
        
        self.tableView.es.addPullToRefresh {
            [unowned self] in
          
            gettingAllPost(pageNo: 1, showLoader: false, isLoadMore: false)
        }
        
                
        
    }
    
    @objc func keyboardWillAppear(notification: NSNotification){
        // Do something here
        keyboardOpen = true
        if let userInfo = notification.userInfo,
            // 3
            let keyboardRectangle = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? CGRect {
            print(Int(keyboardRectangle.height))
            let view = UIView()
            view.frame = CGRect(x: 0, y: 0, width: Int(UIScreen.main.bounds.width), height: Int(keyboardRectangle.height))
            tableView.tableFooterView = view
            // tableView.scrollToRowAtIndexPath(editingIndexPath, atScrollPosition: .Top, animated: true)
            
            tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: CGFloat(Int(keyboardRectangle.height)), right: 0)
            tableView.scrollIndicatorInsets = tableView.contentInset
        }
    }
    
    @objc func keyboardWillDisappear(notification: NSNotification){
        // Do something here
        currentTabBar?.setBar(hidden: true, animated: true)
        tableView.tableFooterView = UIView()
        tableView.contentInset = .zero
        tableView.layoutIfNeeded()
        keyboardOpen = false
        //tableView.scroll(to: .bottom, animated: true)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        createPopOpen = false
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
       

        NotificationCenter.default.removeObserver(self)
        createPoptip.hide()
        //stopVideo()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        params["visibility_type"] = ["public"]
        print(params)

        NotificationCenter.default.addObserver(self, selector:#selector(keyboardWillAppear), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector:#selector(keyboardWillDisappear), name: UIResponder.keyboardWillHideNotification, object: nil)
        navigationController?.isNavigationBarHidden = true
        currentTabBar?.setBar(hidden: false, animated: true)
        
        // addingBarButtons()
        infoBtn.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
        infoBtn.layer.shadowOffset = CGSize(width: 0, height: 3)
        infoBtn.layer.shadowOpacity = 1.0
        infoBtn.layer.shadowRadius = 10.0
        infoBtn.layer.masksToBounds = false
        //infoBtn.setImage(UIImage(named: "information")?.scaleTo(CGSize(width: 20, height: 20)), for: .normal)
        infoBtn.setImage(UIImage(named: "info3"), for: .normal)
        infoBtn.imageView?.tintColor = UIColor.init(hexString: Appcolors.buttons)
        // infoBtn.currentImage?.withTintColor(UIColor.init(hexString: Appcolors.buttons))
        
        infoBtn.imageView?.tintColor = UIColor.init(hexString: Appcolors.buttons)
        
    }
    //MARK:- SENDING TTHE DEVICE TOKEN GENERATED FROM FIREBASE
    func sendingDeviceToken() {
        print(UserDefaultsHandler.getUDValue(key: .deviceToken))
        let userToken = UserDefaultsHandler.getUDValue(key: .token)
        print(userToken)
        if userToken != nil {
            SendingDeviceTokenResponse.requestToSendingDeviceToken(token: UserDefaultsHandler.getUDValue(key: .deviceToken) ?? "") { [weak self](response) in
                guard let strongSelf = self else { return }
                print(response?.xmsg)
                strongSelf.dataSource = TimeLineViewModel(data: nil, numOfPage: 1, loadMore: false)
                strongSelf.gettingAllPost(pageNo: 1, showLoader: true, isLoadMore: false)
            }
        } else {
            return
        }
    }
    //MARK:- POPUP
    func popUp(type: ErrorType, message: String) {
        let vc = UIStoryboard.timeLineStoryboard.instantiateErrorCarMsgPopVC()
        vc.popupType = type
        vc.messageString = message
        vc.delegaet = self
        let cardPopup = SBCardPopupViewController(contentViewController: vc)
        cardPopup.show(onViewController: self)
    }
    //MARK:- GETTING ALL THE POST/FEED
    func gettingAllPost(pageNo: Int = 1, showLoader: Bool = true, isLoadMore: Bool = false) {
       
         params = ["business_post" : true, "event_post" : true, "my_post" : true, "offer_post" : true, "page" : pageNo]
//        params["business_post"] = true
//        params["event_post"] = true
//        params["my_post"] = true
//        params["offer_post"] = true
//        params["page"] = pageNo
        print(params)
        TimeLineResponse.requestToAllPost(params: params,showHud: showLoader) { [weak self](response) in
            guard let strongSelf = self else { return }
            print(response,isLoadMore)
            strongSelf.tableView.tableFooterView = nil
            strongSelf.tableView.tableFooterView?.isHidden = true
            strongSelf.tableView.es.stopPullToRefresh()
            if !isLoadMore {
                strongSelf.dataSource?.data = response
                strongSelf.tableView.reloadData()
            } else {
                if let allData = response?.data {
                    for data in allData {
                        strongSelf.dataSource?.data?.data?.append(data)
                    }
                }
            }
            if strongSelf.dataSource?.data?.data?.count ?? 0 < response?.meta?.total ?? 0 {
                                    strongSelf.dataSource?.loadMore = true
                strongSelf.dataSource?.numOfPage! += 1
                                } else {
                                    strongSelf.dataSource?.loadMore = false
                                }
        }
    }
    
    
    //MARK :- STOP VIDEO WHEN THE CONTROLLER IS CHANGED
    func stopVideo() {
        if let indexPath = tableView.indexPathsForVisibleRows {
            for i in indexPath {
                if let cell = tableView.cellForRow(at: i) as? LiveTableViewCell {
                    
                    
                    // cell = tableView.cellForRow(at: i) as! LiveTableViewCell
                    
                    cell.videoPlayer.videoPlayerControls.stop()
                    
                    
                } else {
                    print(" liveTVC is not displayed")
                }
            }
        }
    }
    func popTipHandler() {
        popTip.appearHandler = { popTip in
            print("\(popTip) appeared")
            self.tableView.isScrollEnabled = false
        }
        popTip.dismissHandler = { popTip in
            print("\(popTip) dismissed")
            self.tableView.isScrollEnabled = true
        }
        privatePopTip.appearHandler = { popTip in
            print("\(popTip) appeared")
            self.tableView.isScrollEnabled = false
        }
        privatePopTip.dismissHandler = { popTip in
            print("\(popTip) dismissed")
            self.tableView.isScrollEnabled = true
        }
        
    }
    func statusBarSetup() {
        let statusBarFrame = UIApplication.shared.statusBarFrame
        let statusBarView = UIView(frame: statusBarFrame)
        statusBarView.backgroundColor = UIColor.init(hexString: Appcolors.background)
        self.view.addSubview(statusBarView)
        statusBarView.backgroundColor = .white
    }
    
    func initialPopTipAndOtherUISetup() {
        timeLinCustomView = Bundle.main.loadNibNamed(String(describing: TimeLineCreate.self), owner: self, options: nil)?.first as? TimeLineCreate
        timeLinCustomView?.delagate = self
        timeLinCustomView?.frame.size = CGSize(width: 220, height: 250)
        popTip.shouldDismissOnTap = true
        popTip.tapOutsideHandler = { _ in
            print("tap outside")
            self.self.infoBtn.isSelected = false
            self.infoBtn.setImage(UIImage(named: "info3"), for: .normal)
            self.infoBtn.contentMode = .center
            self.infoBtn.imageView?.contentMode = .scaleAspectFit
        }
        createPoptip.tapOutsideHandler = { _ in
            print("tap outside")
        }
        //infoBtn.setImage(UIImage(named: "iconInfoFilled2"), for: .normal)
        // infoBtn.imageView?.contentMode = .scaleAspectFill
        popTip.backgroundColor = .white
        popTip.textColor = UIColor.init(hexString: Appcolors.text)
        popTip.bubbleColor = .white
        popTip.borderWidth = 0.5
        popTip.borderColor = .lightGray
        popTip.textAlignment = .center
        
    }
    
    
    
    
    
    //MARK: REGISTERING TABLE VIEW CELLS
    func registeringViews() {
        tableView.register(UINib(nibName: "PostCreationTableViewCell", bundle: nil), forCellReuseIdentifier: "PostCreationTableViewCell")
        tableView.register(UINib(nibName: "PortFolioTableViewCell", bundle: nil), forCellReuseIdentifier: "PortFolioTableViewCell")
        tableView.register(UINib(nibName: "EventTableViewCell", bundle: nil), forCellReuseIdentifier: "EventTableViewCell")
        tableView.register(UINib(nibName: "DiscountTableViewCell", bundle: nil), forCellReuseIdentifier: "DiscountTableViewCell")
        tableView.register(UINib(nibName: "LiveTableViewCell", bundle: nil), forCellReuseIdentifier: "LiveTableViewCell")
        tableView.register(UINib(nibName: "LiveStreamingTableViewCell", bundle: nil), forCellReuseIdentifier: "LiveStreamingTableViewCell")
        //tableView.bounces = false
        //        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        //        refreshControl.addTarget(self, action: #selector(self.refresh(_:)), for: .valueChanged)
        //          tableView.addSubview(refreshControl) // not required when using UITableViewController
        
        
    }
    @objc func refresh(_ sender: AnyObject) {
        DispatchQueue.main.async {
            self.topConstraint.constant = 0
        }
        tableView.reloadData()
        
        refreshControl.endRefreshing()
        
        print(topConstraint.constant)
    }
//    func addData() {
//        dataSource = [TimeLineModel(title: "Post Creation", data: PostDataStruct(tableViewCells: 10, collectionVIewCells: 2)), TimeLineModel(title: "Portfolio Creation", data: PortFolioDataStruct(tableViewCells: 4, collectionVIewCells: 1)),TimeLineModel(title: "Post Creation", data: PostDataStruct(tableViewCells: 10, collectionVIewCells: 2)), TimeLineModel(title: "Event Creation", data: EventDataStruct(tableViewCells: 2, collectionVIewCells: 3)),TimeLineModel(title: "Portfolio Creation", data: PortFolioDataStruct(tableViewCells: 4, collectionVIewCells: 1)), TimeLineModel(title: "Discount Creation", data: DiscountDataStruct(tableViewCells: 4, collectionVIewCells: 3)),TimeLineModel(title: "Event Creation", data: EventDataStruct(tableViewCells: 2, collectionVIewCells: 3)), TimeLineModel.init(title: "Live", data: LiveDataStruct(tableViewCells: 3, collectionVIewCells: 3))]
//        texts = ["Share", "Edit", "Delete"]
//
//
//    }
    func addButtonsOntop() {
        searchBtn.setImage(UIImage(named: "searchVector"), for: .normal)
        searchBtn.adjustsImageWhenHighlighted = false
        searchBtn.imageView?.contentMode = .center
        notificationBtn.setImage(UIImage(named: "iconBell"), for: .normal)
        notificationBtn.imageView?.contentMode = .center
        
        messageBtn.setImage(UIImage(named: "comment"), for: .normal)
        messageBtn.imageView?.contentMode = .center
        
        addBtn.setImage(UIImage(named: "iconAddVector"), for: .normal)
        addBtn.imageView?.contentMode = .center
        addBtn.showsTouchWhenHighlighted = false
        addBtn.adjustsImageWhenHighlighted = false
        
        
    }
    func publicDefaultSelection() {
        pubBtn = true
        publicBtn.backgroundColor = UIColor.init(hexString: Appcolors.buttons)
        publicBtn.titleLabel?.textColor = .white
    }
    
    func didTapaddBtn(sender: UIButton) {
        //MARK: POPTIP
        
        createPoptip.offset = -37
        createPoptip.clipsToBounds = true
        createPoptip.constrainInContainerView = false
        createPoptip.shouldDismissOnTap = true
        createPoptip.tapOutsideHandler = { _ in
            print("tap outside")
            self.createPopOpen = false
        }
        createPoptip.shouldDismissOnTapOutside = true
        if createPopOpen == false {
            
            
            
            // createPoptip.clipsToBounds = false
            createPoptip.cornerRadius = 20
            createPoptip.padding = 0
            createPoptip.shouldDismissOnTap = true
            createPoptip.padding = CGFloat(0.0)
            createPoptip.borderWidth = 0.2
            createPoptip.shouldDismissOnTap = false
            createPoptip.arrowSize = CGSize(width: 0, height: 0)
            createPoptip.constrainInContainerView = false
            let windw = UIApplication.shared.windows.first { $0.isKeyWindow }
            var frame = sender.convert(sender.bounds, to: windw)
            frame.origin.y += 130
            
            createPoptip.show(customView: timeLinCustomView!, direction: .left, in: windw!, from: frame)
            createPopOpen = true
        } else {
            createPoptip.hide()
            createPopOpen = false
        }
        
        
        
        
    }
    
    
    @IBAction func infoAction(_ sender: UIButton) {
        popTip.offset = 50
        let imageSize:CGSize = CGSize(width: 32, height: 32)
        if infoBtn.isSelected == true {
            popTip.hide()
            infoBtn.setImage(UIImage(named: "info3"), for: .normal)
            infoBtn.imageView?.tintColor = UIColor.init(hexString: Appcolors.buttons)
            infoBtn.image(for: .normal)?.withRenderingMode(.alwaysTemplate)
            infoBtn.isSelected = false
        } else {
            
            //popTip.offset = -50
            //popTip.bubbleOffset = 50
            infoBtn.setImage(UIImage(named: "information3"), for: .normal)
            infoBtn.adjustsImageWhenHighlighted = false
            infoBtn.imageEdgeInsets = UIEdgeInsets(
                top: 0,
                left: 0,
                bottom: 0,
                right: 0)
            // infoBtn.imageView?.contentMode = .scaleAspectFill
            infoBtn.imageView?.tintColor = UIColor.init(hexString: Appcolors.buttons)
            popTip.show(text: "Public: View worldwide posts. \n Private: To view your and your friend's posts", direction: .down, maxWidth: view.bounds.width, in: topStackView, from: sender.frame)
            infoBtn.isSelected = true
            
        }
        
        popTip.clipsToBounds = false
        popTip.constrainInContainerView = false
        
        
    }
    
    @IBAction func action(_ sender: Any) {
        print("asdasf")
    }
    @IBAction func publicBtnAction(_ sender: UIButton) {
        print(pubBtn, pvBtn)
        infoView.layer.masksToBounds = false
        if pubBtn == true && pvBtn == false {
            popTip.constrainInContainerView = false
            popTip.offset = 50
            
            popTip.show(text: "Cannot unselect both at once", direction: .down, maxWidth: view.bounds.width , in: topStackView,from: sender.frame, duration: 0.90)
            
        } else if pvBtn == true && pubBtn == false {
            publicBtn.backgroundColor = UIColor.init(hexString: Appcolors.buttons)
            publicBtn.setTitleColor(UIColor.white, for: .normal)
            pubBtn = true
            params["visibility_type"] = ["public"]
            gettingAllPost(pageNo: 1, showLoader: true, isLoadMore: false)
        } else if pvBtn == true && pubBtn == true{
            publicBtn.backgroundColor = UIColor.white
            publicBtn.setTitleColor(UIColor.init(hexString: Appcolors.buttons), for: .normal)
            pubBtn = false
            params["visibility_type"] = ["private"]
            gettingAllPost(pageNo: 1, showLoader: true, isLoadMore: false)

        } else {
            if (pubBtn == true)
            {
                publicBtn.backgroundColor = UIColor.white
                publicBtn.setTitleColor(UIColor.init(hexString: Appcolors.buttons), for: .normal)
                pubBtn = false
            }
            else {
                publicBtn.backgroundColor = UIColor.init(hexString: Appcolors.buttons)
                publicBtn.setTitleColor(UIColor.white, for: .normal)
                
                pubBtn = true
                
            }
            
        }
    }
    @IBAction func privateBtnAction(_ sender: UIButton) {
        popTip.textAlignment = .center
        
        
        if pvBtn == true && pubBtn == false {
            
            // popTip.edgeInsets = .init(top: 0, left: 0, bottom:
            //  0, right: 20)
            privatePopTip.bubbleOffset = 0
            ///popTip.offset = 50
            privatePopTip.backgroundColor = .white
            privatePopTip.textColor = UIColor.init(hexString: Appcolors.text)
            privatePopTip.bubbleColor = .white
            privatePopTip.borderWidth = 0.5
            privatePopTip.borderColor = .lightGray
            privatePopTip.textAlignment = .center
            
            let windw = UIApplication.shared.windows.first { $0.isKeyWindow }
            var frame = sender.convert(sender.bounds, to: windw)
            // frame.origin.y += 130
            
            privatePopTip.show(text: "Cannot unselect both at once", direction: .autoVertical, maxWidth: UIScreen.main.bounds.width, in: windw!,from: frame, duration: 0.90)
            params["visibility_type"] = ["public","private"]
            gettingAllPost(pageNo: 1, showLoader: true, isLoadMore: false)
        }  else if pvBtn == true && pubBtn == true {
            privateBtn.backgroundColor = UIColor.white
            privateBtn.setTitleColor(UIColor.init(hexString: Appcolors.buttons), for: .normal)
            pvBtn = false
            params["visibility_type"] = ["public"]
            gettingAllPost(pageNo: 1, showLoader: true, isLoadMore: false)
        } else if pvBtn == false && pubBtn == true {
            privateBtn.backgroundColor = UIColor.init(hexString: Appcolors.buttons)
            privateBtn.setTitleColor(UIColor.white, for: .normal)
            pvBtn = true
            params["visibility_type"] = ["public","private"]
            gettingAllPost(pageNo: 1, showLoader: true, isLoadMore: false)
        } else {
            if (pvBtn == true)
            {
                privateBtn.backgroundColor = UIColor.white
                privateBtn.setTitleColor(UIColor.init(hexString: Appcolors.buttons), for: .normal)
                pvBtn = false
                
            }
            else {
                privateBtn.backgroundColor = UIColor.init(hexString: Appcolors.buttons)
                privateBtn.setTitleColor(UIColor.white, for: .normal)
                // privateBtn.titleLabel?.textColor = .white
                pvBtn = true
                
            }
            //sender.isSelected  = !sender.isSelected
            
        }
        // pvBtn = !pvBtn
    }
    func popingBottomVC(id: Int, indexToReload: Int, selectionType: GraduateDetails,starredValue: String) {
        let vc = HelperFunctions.ratingSelection(id: id, starredVlaue: "", reloadingIndex: indexToReload)
        vc.delegate = self
        vc.selectionType = selectionType
        vc.id = id
        vc.selectedIndex = Int(starredValue)
        vc.indexToReload = indexToReload
        present(vc, animated: true, completion: nil)
    }
    func sharePopOver(sender: UIButton) {
        popupTableView.reloadData()
        if texts.count == 2 {
            popupTableView.frame = CGRect(x: 0, y: 0, width: 90, height: 80)
        } else {
            popupTableView.frame = CGRect(x: 0, y: 0, width: 90, height: 120)
        }
        popupTableView.delegate = self
        popupTableView.dataSource = self
        registeringViews()
        popupTableView.isScrollEnabled = false
        self.popover = Popover(options: self.popoverOptions)
        self.popover.willShowHandler = {
            print("willShowHandler")
        }
        self.popover.didShowHandler = {
            print("didDismissHandler")
        }
        self.popover.willDismissHandler = {
            print("willDismissHandler")
        }
        self.popover.didDismissHandler = {
            print("didDismissHandler")
        }
        self.popover.show(popupTableView, fromView: sender)
    }
    @objc func ratingDetailsAction() {
        let vc = UIStoryboard.timeLineStoryboard.instantiateRatingDetailsVC()
        navigationController?.pushViewController(vc, animated: true)
    }
    //    func btnUp(value: Bool)
    @IBAction func addBtnAction(_ sender: UIButton) {
        didTapaddBtn(sender: sender)
    }
    @IBAction func notificationBtnAction(_ sender: Any) {
        let vc = UIStoryboard.timeLineStoryboard.instantiateMessageVC()
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func searchBtnAction(_ sender: Any) {
        let vc = UIStoryboard.timeLineStoryboard.instantiateSearchVC()
        navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func messageBtnAction(_ sender: Any) {
        let vc = UIStoryboard.moreStoryboard.instantiateNotificationViewController()
        vc.currentTabBar?.setBar(hidden: true, animated: true)
        navigationController?.pushViewController(vc, animated: true)
        
    }
}


//MARK:- TABLE VIEW DATASOURCE
extension TimeLineViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print(dataSource)
        
        //SECOND METHOD
        if tableView == popupTableView {
            return texts.count
        } else  {
            print( tableViewDataSource?.data?.data?.count)
            return dataSource?.data?.data?.count ?? 0
            //return dataSource?.count ?? 0
            
        }
        
        
        
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == popupTableView {
            let cell = UITableViewCell(style: .default, reuseIdentifier: nil)
            cell.textLabel?.text = self.texts[(indexPath as NSIndexPath).row]
            cell.textLabel?.textColor = UIColor.init(hexString: Appcolors.text)
            return cell
        }  else if tableView == tableView {
            
            //MARK:- POST CREATION CELL
            print(dataSource?.data?.data?[indexPath.row].message)
            if dataSource?.data?.data?[indexPath.row].portfolio != nil {
                let cell = tableView.dequeueReusableCell(withIdentifier: "PortFolioTableViewCell", for: indexPath) as! PortFolioTableViewCell
            
                var portData = dataSource?.data?.data?.filter({ $0.portfolio != nil })
                cell.subHeaderlabel.text = dataSource?.data?.data?[indexPath.row].portfolio?.addressString
                cell.postType = .portfolio
                cell.delegate = self
                cell.selectionType = .myPost
                return cell.portFolioCell(cell: cell, indexPath: indexPath, dataSource: dataSource?.data?.data)
            } else if dataSource?.data?.data?[indexPath.row].offer != nil {
                let cell = tableView.dequeueReusableCell(withIdentifier: "DiscountTableViewCell", for: indexPath) as! DiscountTableViewCell
                
                cell.delegate = self
                cell.selectionType = .offer
                cell.postType = .offer
                return cell.DiscountTVC(cell: cell, indexPath: indexPath, dataSource: dataSource?.data?.data)
            } else if dataSource?.data?.data?[indexPath.row].event != nil {
                let cell = tableView.dequeueReusableCell(withIdentifier: "EventTableViewCell", for: indexPath) as! EventTableViewCell
                let eventData = dataSource?.data?.data?.filter {
                    $0.event != nil
                }
               // print(dataSource?.gradSpot?.data)
                cell.postType = .gradSpot
                cell.delegate = self
                cell.selectionType = .gradSpot
                cell.eventTypeLabel.text = dataSource?.data?.data?[indexPath.row].event?.eventType
                return cell.eventTVC(cell: cell, indexPath: indexPath, dataSource: dataSource?.data?.data)
            } else if dataSource?.data?.data?[indexPath.row].hasLiveEnded == true {
                print(dataSource?.data?.data?[indexPath.row].hasLiveEnded)
                 //MARK:- MYPOST CELL WITH LIVE TYPE VIDEOS
                 print("")
                 let cell = tableView.dequeueReusableCell(withIdentifier: "LiveTableViewCell", for: indexPath) as! LiveTableViewCell
                  cell.videoType = .liveVideo
                 cell.name = dataSource?.data?.data?[indexPath.row].postingAs?.name
                 
                 cell.delegate = self
                 cell.selectionType = .myPost
                  cell.postType = .live
                 let newDate = HelperFunctions.timeInterval(timeAgo: dataSource?.data?.data?[indexPath.row].createdAt ?? "")
                                        print(newDate)
                 cell.timeStampLabel.text = newDate
                if dataSource?.data?.data?[indexPath.row].hasStarred == true {
                    cell.ratingBtn.setImage(UIImage(named: "star3"))
                } else {
                    cell.ratingBtn.setImage(UIImage(named: "star"))
                }
                 cell.locationLabel.text = dataSource?.data?.data?[indexPath.row].business?.businessName
                 cell.viewersLabel.text = "\(dataSource?.data?.data?[indexPath.row].viewsCount ?? 0)"
                let filrurls = NSURL(string: (GraduateUrl.imageUrl + (dataSource?.data?.data?[indexPath.row].liveVideoMp4 ?? "")))
                           let firstAsset = AVURLAsset(url: filrurls! as URL)
                 cell.videoPlayer.videoAssets = [firstAsset]
                 cell.postData = dataSource?.data?.data?[indexPath.row]
                  cell.ratingNumLabel.text = "\(dataSource?.data?.data?[indexPath.row].starsCount ?? 0)"
                  cell.thoughtsNumLabel.text = "\(dataSource?.data?.data?[indexPath.row].comments?.count ?? 0)"
                  cell.commentId = dataSource?.data?.data?[indexPath.row].internalIdentifier
                  cell.index = indexPath
                  cell.captionLabel.text = dataSource?.data?.data?[indexPath.row].message
                 return cell
                 
             } else if dataSource?.data?.data?[indexPath.row].hasLiveEnded == false {
                 let cell = tableView.dequeueReusableCell(withIdentifier: "LiveStreamingTableViewCell", for: indexPath) as! LiveStreamingTableViewCell
              cell.camera = Camera(name: dataSource?.data?.data?[indexPath.row].message ?? "", isConnected: true, isLive: true, url: dataSource?.data?.data?[indexPath.row].liveVideoRtmp ?? "")
              cell.setupPlayer()
              cell.delegate = self
              cell.selectionType = .live
              cell.videoType = .liveVideo
              cell.backgroundColor = .black
              return cell.liveStreamingTVC(cell: cell, indexPath: indexPath, dataSource: dataSource?.data?.data)
            
        
    
  
} else if dataSource?.data?.data?[indexPath.row].event == nil && dataSource?.data?.data?[indexPath.row].offer == nil && dataSource?.data?.data?[indexPath.row].portfolio == nil && dataSource?.data?.data?[indexPath.row].business == nil {
 //MARK:- CREATE POST WITH GALLERIES VIDEOS CELL
                print(dataSource?.data?.data?[indexPath.row].galleries?.count)
                
                if dataSource?.data?.data?[indexPath.row].galleries?.count != 0 {
                    
//MARK:- POST CREATION GALLERTIES VIDEO POST
                    if dataSource?.data?.data?[indexPath.row].galleries?.first?.mediaType == "Video" {
                        
                    
                    if let index = dataSource?.data?.data?[indexPath.row].galleries?.firstIndex(where: { $0.mediaType == "Video"
                    }) {
                        let cell = tableView.dequeueReusableCell(withIdentifier: "LiveTableViewCell", for: indexPath) as! LiveTableViewCell
                        cell.postType = .myPost
                        cell.index = indexPath
                        cell.postData = dataSource?.data?.data?[indexPath.row]
                        cell.delegate = self
                        cell.videoType = .postVideo
                        cell.starCount = dataSource?.data?.data?[indexPath.row].starsCount
                        print(dataSource?.data?.data?[indexPath.row].starredValue)
                         cell.starreVlaue = dataSource?.data?.data?[indexPath.row].starredValue
                        let newDate = HelperFunctions.timeInterval(timeAgo: dataSource?.data?.data?[indexPath.row].createdAt ?? "")
                                               print(newDate)
                        cell.timeStampLabel.text = newDate
                        cell.universityId = dataSource?.data?.data?[indexPath.row].internalIdentifier
                       
                        if dataSource?.data?.data?[indexPath.row].postingAsType == "business" {
                              cell.name = dataSource?.data?.data?[indexPath.row].postingAs?.businessName
                        } else if dataSource?.data?.data?[indexPath.row].postingAsType == "user" {
                             cell.name = dataSource?.data?.data?[indexPath.row].postingAs?.name
                        }
                        if dataSource?.data?.data?[indexPath.row].hasStarred == true {
                              cell.ratingBtn.setImage(UIImage(named: "star3"))
                        } else {
                             cell.ratingBtn.setImage(UIImage(named: "star"))
                        }
                      
                        cell.selectionType = .myPost
                        cell.locationLabel.text = dataSource?.data?.data?[indexPath.row].business?.businessName
                        cell.viewersLabel.text = "\(dataSource?.data?.data?[indexPath.row].viewsCount ?? 0)"
                        //cell.videoType = .postVideo
                        cell.captionLabel.text = dataSource?.data?.data?[indexPath.row].message
                        
                        let filrurls = NSURL(string: (GraduateUrl.imageUrl + (dataSource?.data?.data?[indexPath.row].galleries?[index].filepath ?? "")))
                        print(filrurls)
                        
                        //                            let url = (GraduateUrl.imageUrl + (dataSource?.myPost?.data?[indexPath.row].galleries?[index].filepath ?? ""))
                        let firstAsset = AVURLAsset(url: filrurls! as URL)
                        cell.videoPlayer.videoAssets = [firstAsset]
                        //                            let newDate = HelperFunctions.getDate(input: dataSource?[indexPath.row].portfolio?.business?.createdAt ?? "")
                        // cell.timeStampLabel.text = newDate?.timeAgo()
                        //cell.timeStampLabel.text = newDate
                        cell.ratingNumLabel.text = "\(dataSource?.data?.data?[indexPath.row].starsCount ?? 0)"
                        cell.thoughtsNumLabel.text = "\(dataSource?.data?.data?[indexPath.row].comments?.count ?? 0)"
                        cell.proImage.setURLImage(imageURL: (GraduateUrl.imageUrl + (dataSource?.data?.data?[indexPath.row].postingAs?.profile?.profileImage ?? "")))
                        return cell
                    }
                    } else if dataSource?.data?.data?[indexPath.row].galleries?.first?.mediaType == "Image" {
//MARK: GALLERY IMAGE POST
                    if let _ = dataSource?.data?.data?[indexPath.row].galleries?.firstIndex(where: { $0.mediaType == "Image"
                    }) {
                        let cell = tableView.dequeueReusableCell(withIdentifier: "PostCreationTableViewCell", for: indexPath) as! PostCreationTableViewCell
                        cell.postType = .myPost
                        cell.delegate = self
                        cell.imageDataSource = dataSource?.data?.data?[indexPath.row].galleries ?? []
                        cell.selectionType = .myPost
                        return cell.postTVC(cell: cell, indexPath: indexPath, dataSource: dataSource?.data?.data)
                    }
                    }
                } else if dataSource?.data?.data?[indexPath.row].galleries?.count == 0{
                   
//                       print(dataSource?.data?.data?[indexPath.row].hasLiveEnded)
//                       cell.postType = .myPost
//                       cell.delegate = self
//                       cell.selectionType = .myPost
//                       return cell.postTVC(cell: cell, indexPath: indexPath, dataSource: dataSource?.data?.data)
//                   } else {
                       print(dataSource?.data?.data?[indexPath.row].hasLiveEnded)
                    let cell = tableView.dequeueReusableCell(withIdentifier: "PostCreationTableViewCell", for: indexPath) as! PostCreationTableViewCell
                    cell.selectionType = .myPost
                    cell.delegate = self
                    cell.postType = .myPost
                    return cell.postTVC(cell: cell, indexPath: indexPath, dataSource: dataSource?.data?.data)
                   }
                }
      
    }
        return UITableViewCell()
    }

    
     func thoughtsDetails(commentId: Int, index: IndexPath!) {
        let vc = UIStoryboard.timeLineStoryboard.instantiateThoughtsVC()
        vc.commentId = commentId
        vc.index = index
        vc.delegate = self
        navigationController?.pushViewController(vc, animated: true)
    }
    @objc func ratingDetails() {
        let vc = UIStoryboard.timeLineStoryboard.instantiateRatingDetailsVC()
        navigationController?.pushViewController(vc, animated: true)
    }
    @objc func ratingPopUp() {
        popingBottomVC(id: 0, indexToReload: 0, selectionType: .live, starredValue: "")
    }
    @objc func profileImageAction() {
        let vc = UIStoryboard.moreStoryboard.instantiateUserProfileVC()
        navigationController?.pushViewController(vc, animated: true)
    }
    //    func popingBottomVC() {
    //          let popupVC = UIStoryboard.timeLineStoryboard.instantiateRatingVC()
    //                 //popupVC.delegate = self
    //              //popupVC.selectedIndex = 3
    //                        popupVC.height = 200
    //                        popupVC.topCornerRadius = 15
    //                 popupVC.presentDuration = 0.25
    //                        popupVC.dismissDuration = 0.25
    //                        popupVC.shouldDismissInteractivelty = true
    //                       // popupVC.popupDelegate = self
    //                        present(popupVC, animated: true, completion: nil)
    //          }
}

//MARK:- TABLE VIEW DELEGATE
extension TimeLineViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
//        /// cell.videoPlayer.videoPlayerControls.vid
//        if let cell = dataSource?[indexPath.row].data as? LiveDataStruct {
//            print("LIvet TV displayed")
//            videoTVC = true
//        }
//
        
        
        //cellHeights[indexPath] = cell.frame.size.height
        
        ///MARK:- PAGINATION
        let allData = dataSource
        print()
        guard allData?.loadMore == true, let dataCount = allData?.data?.data?.count, indexPath.row == dataCount - 1  else {return}
        print(dataCount)
        let lastSectionIndex = tableView.numberOfSections - 1
        let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
        if indexPath.section == lastSectionIndex && indexPath.row == lastRowIndex {
            // print("this is the last cell")
            let spinner = UIActivityIndicatorView(style: .gray)
            spinner.startAnimating()
            spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
            self.tableView.tableFooterView = spinner
            self.tableView.tableFooterView?.isHidden = false
        }
        gettingAllPost(pageNo: allData?.numOfPage ?? 0, showLoader: false, isLoadMore: allData?.loadMore ?? false)
        // print(allData.url)
        
    }
//    private func tableView(_ tableview: UITableView, didEndDisplaying cell: LiveTableViewCell, forRowAt indexPath: IndexPath) {
//        tableView.reloadRows(at: [indexPath], with: .automatic)
//    //cell.videoPlayer.videoPlayerControls.stop()
////        if let cell = dataSource?[indexPath.row].data as? LiveDataStruct {
////            print("LIvet TV hiddden")
////            videoTVC = false
////        }
//    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        popover.dismiss()
        if tableView == popupTableView {
            popover.dismiss()
            if texts[indexPath.row] ==  "Share"{
                let shareText = "http://test-graduation1.captainau.com.au/"
                
                let image = UIImage(named: "iconArrowDown")
                let vc = UIActivityViewController(activityItems: [shareText, image!], applicationActivities: [])
                present(vc, animated: true)
                self.popover.dismiss()
            } else if texts[indexPath.row] == "Edit" {
                switch postTypes {
                case .myPost:
                    popover.dismiss()
                    let vc = UIStoryboard.timeLineStoryboard.instantiateCreateSpotViewController()
                    vc.vcType = .editPost
                    vc.delegate = self
                    print(editedIndex)
                    vc.editData = dataSource?.data?.data?[editedIndex.row]
                    navigationController?.pushViewController(vc, animated: true)
                case .live:
                    popover.dismiss()
                    let vc = UIStoryboard.timeLineStoryboard.instantiateCreateSpotViewController()
                    vc.vcType = .editPost
                    vc.delegate = self
                    print(editedIndex)
                    vc.editData = dataSource?.data?.data?[editedIndex.row]
                    navigationController?.pushViewController(vc, animated: true)
                case .gradSpot:
                    let vc = UIStoryboard.timeLineStoryboard.instantiateCreateSpotViewController()
                    vc.vcType = .editSpot
                    vc.editData = dataSource?.data?.data?[editedIndex.row]
                    vc.delegate = self
                    navigationController?.pushViewController(vc, animated: true)
                    
                case .portfolio:
                    let vc = UIStoryboard.timeLineStoryboard.instantiateCreateSpotViewController()
                    vc.vcType = .editPortFolio
                    vc.editData = dataSource?.data?.data?[editedIndex.row]
                    vc.delegate = self
                    navigationController?.pushViewController(vc, animated: true)
                case .offer:
                    let vc = UIStoryboard.timeLineStoryboard.instantiateCreateSpotViewController()
                    vc.vcType = .editOffer
                    vc.editData = dataSource?.data?.data?[editedIndex.row]
                    vc.delegate = self
                    navigationController?.pushViewController(vc, animated: true)
                default:
                    break
                }
//                self.popover.dismiss()
//                let vc = UIStoryboard.timeLineStoryboard.instantiateCreateSpotViewController()
//                vc.controllerType = "Edit Post"
//                navigationController?.pushViewController(vc, animated: true)
                
            } else if texts[indexPath.row] == "Delete" {
                switch postTypes {
                case .portfolio:
                    popUp(type: .deletePortFolio, message: "")
                case .myPost:
                    popUp(type: .deletePost, message: "")
                case .offer:
                    popUp(type: .deleteOffer, message: "")
                case .gradSpot:
                    popUp(type: .deleteEvent, message: "")
                case .live:
                    popUp(type: .deletePost, message: "")
                default:
                    break
                }
//                let vc = UIStoryboard.timeLineStoryboard.instantiateDeletePostPopUpVC()
//                let cardPopup = SBCardPopupViewController(contentViewController: vc)
//                cardPopup.show(onViewController: self)
//                self.popover.dismiss()
            }else {
                
                let vc = UIStoryboard.graduateStoryboard.instantiateReportVC()
                vc.reportId = dataSource?.data?.data?[editedIndex.row].internalIdentifier
                vc.modalPresentationStyle = .overFullScreen
                present(vc, animated: true, completion: nil)
                popover.dismiss()
            }
        } else {
            
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == popupTableView {
            return 40
        } else {
            return  UITableView.automaticDimension
        }
        
        
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 505
    }
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if tableView == popupTableView {
            
        } else {
        registeringViews()
        if let cell = tableView.dequeueReusableCell(withIdentifier: "LiveTableViewCell", for: indexPath) as? LiveTableViewCell {
            if let players = cell.videoPlayer {
                if players.preferredRate != 0 {
                    players.videoPlayerControls.pause()
                }
            }
        }

    }
    }
    
}

//MARK:- SCROLL VIEW DELEGATE
extension TimeLineViewController: UIScrollViewDelegate {
    
    //first method
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        //MARK-: FOR NOT BOUNCING WHILE PULL TO REFRESH
        if scrollView.contentOffset.y < 0.0 {
           // gettingAllPost()
         
            print(scrollView.contentOffset.y)
            if scrollView.contentOffset.y > -80.0 {
           // gettingAllPost()
            }
           
            return
        }
        
        if (scrollView.contentOffset.y >= scrollView.contentSize.height - scrollView.frame.size.height) {
            scrollView.setContentOffset(CGPoint(x: scrollView.contentOffset.x, y: scrollView.contentSize.height - scrollView.frame.size.height), animated: false)
            //
        }
        if keyboardOpen == false {
            let delta = scrollView.contentOffset.y - lastContentOffset
            if delta < 0 {
                // the value is negative, so we're scrolling up and the view is moving back into view.
                // take whatever is smaller, the constant minus delta or 0            topView.isHidden = true
                
                
                
                self.topConstraint.constant = min(self.topConstraint.constant - delta, 0)
                
                
                //headerView.frame.origin.y += min(self.topConstraint.constant - delta, 0)
                
                
                
                currentTabBar?.tabBarHeight = min(50, (currentTabBar?.buttonsContainerHeightConstraintInitialConstant)! - delta)
                print(self.topConstraint.constant, currentTabBar?.tabBarHeight, scrollView.contentOffset.y)
                self.view.layoutIfNeeded()
            } else {
                // the value is positive, so we're scrolling down and the view is moving out of sight.
                // take whatever is "larger," the constant minus delta, or the minimumConstantValue.
                //
                //             headerView.frame.origin.y += min(self.topConstraint.constant - delta, 0)
                //            print( headerView.frame.origin.y ,"header view")
                
                
                // tableView.frame.size.height += 96
                DispatchQueue.main.async {
                    // your code here
                    self.topConstraint.constant = max(self.minimumConstantValue, self.topConstraint.constant - delta)
                }
                
                
                
                // topConstraint.constant = -77
                currentTabBar?.tabBarHeight = max(-35, (currentTabBar?.buttonsContainerHeightConstraintInitialConstant)! - delta)
                print(topConstraint.constant, currentTabBar?.tabBarHeight,scrollView.contentOffset.y,delta)
                /// print(delta)
                self.view.layoutIfNeeded()
                
            }
            // This makes the + or - number quite small.
            lastContentOffset = scrollView.contentOffset.y
        } else {
            print("")
        }
    }
}




//MARK: REVCEIVING RATING FROM THE DISCOUNTTVC
extension TimeLineViewController: SedingRatingsToTimeLineVC {
    func moreOptionFromDiscountTVC(sender: UIButton, id: Int, isOwnBusiness: Bool, postType: GraduateDetails, index: IndexPath) {
        self.postTypes = postType
        self.editedIndex = index
        if isOwnBusiness == true {
            sharePopOver(sender: sender)
        } else {
            texts.removeAll()
            texts = ["Share", "Report"]
            sharePopOver(sender: sender)
        }
    }
   
    
    func commentOutsideDiscountTVC(comment: Comments?, index: IndexPath) {
        dataSource?.data?.data?[index.row].comments?.append(comment!)
        tableView.reloadRows(at: [index], with: .automatic)
    }
    func pushingTOProfileVCFromDisc(id: Int, userType: String, isOwner: Bool, postAsType: String) {
        pushToProfile(id: id, userType: userType, isOwner: isOwner, postAsType: postAsType)
    }
    func feedbackAction(commentId: Int, outsideComment: Bool, index: IndexPath) {
        if outsideComment == false {
                  thoughtsDetails(commentId: commentId, index: index)
                  
              } else {
                  
              }
    }
    

    func sendingRating(id: Int, starValue: String, selectedindexToReload: Int, selectionType: GraduateDetails) {
        popingBottomVC(id: id, indexToReload: selectedindexToReload, selectionType: selectionType, starredValue: starValue)
    }
    
    
    
    
    
    func discratingDetails(ratingId: Int, starredValue: String, starCount: Int) {
        ratingDetails(ratingId: ratingId, starredVlaue: starredValue, starCount: starCount)
    }
    
    
    func fullscreen(vc: UIViewController) {
        currentTabBar?.setBar(hidden: true, animated: true)
        present(vc, animated: true, completion: nil)
    }
    
    
    
    //    func popingMoreOP() {
    //        texts.removeAll()
    //        texts = ["Share", "Report"]
    //        sharePopOver(sender: sender)
    //    }
    
    
}
//MARK: RECEIVING RATING FROM LIVE TVC
extension TimeLineViewController: SendingRatingFromLiveTVC {
    func moreOptionsFromLiveTVC(sender: UIButton, index: IndexPath, isOwner: Bool, postType: GraduateDetails) {
      
        postTypes = postType
        editedIndex = index
        if isOwner == true {
            texts.removeAll()
            texts = ["Share", "Edit","Delete"]
            sharePopOver(sender: sender)
        } else {
            texts.removeAll()
            texts = ["Share", "Report"]
            sharePopOver(sender: sender)
        }
    }
    
  
    func commentOutsideLiveTVC(comment: Comments?, index: IndexPath) {
        dataSource?.data?.data?[index.row].comments?.append(comment!)
        tableView.reloadRows(at: [index], with: .automatic)
    }
    func pushingTOProfileVC(id: Int, userType: String, isOwner: Bool, postasType: String) {
        print(id,userType,isOwner)
        if postasType == "user" {
        if isOwner == true {
                     let vc = UIStoryboard.timeLineStoryboard.instantiateUserProfileVC()
                     navigationController?.pushViewController(vc, animated: true)
                     } else {
                         let vc = UIStoryboard.timeLineStoryboard.instantiateOtherUserProfileVC()
                   vc.userId = id
                                navigationController?.pushViewController(vc, animated: true)
                     }
        } else {
            let vc = UIStoryboard.timeLineStoryboard.instantiateGraduateDetailsViewController()
            vc.businessId = id
            navigationController?.pushViewController(vc, animated: true)
        }
    }
    
   
    
    func feedbackActionLiveTVC(commentId: Int, index: IndexPath!) {
        //thoughtsDetails(commentId: commentId, index: index)
//        let param:[String : Any] = ["comment_body" : thoughtsTxtView.text ?? ""]
//                   ThoughtsResponse.requestToComment(id: "\(commentId ?? 0)", param: param) { [weak self](response) in
//                       guard let strongSelf = self else { return }
//                       print(response?.thoughtsData?.body)
//                       strongSelf.dataSource?.data?.append((response?.thoughtsData)!)
//                       strongSelf.tableView.reloadData()
//
//                   }
        print(commentId,index)
    thoughtsDetails(commentId: commentId, index: index)
        
    }
    func sendingRatings(id: Int, starValue: String, selectedindexToReload: Int, selectionType: GraduateDetails) {
        popingBottomVC(id: id, indexToReload: selectedindexToReload, selectionType: selectionType, starredValue: starValue)
    }
    
    
    func liveRatingDetails(ratingId: Int, starredVlaue: String, starCount: Int) {
        self.ratingDetails(ratingId: ratingId, starredVlaue: starredVlaue, starCount: starCount)
    }
    
    
    func moreOptionsFromLiveTVC(sender: UIButton) {
       
    }
    
    func sendingVideoUrlForFullScreen() {
        
    }
    func sendingRatings() {
        popingBottomVC(id: 0, indexToReload: 0, selectionType: .live, starredValue: "")
    }
    
    //MARK:- RATING DETAILS
    func ratingDetails(ratingId: Int, starredVlaue: String, starCount: Int) {
           let vc = UIStoryboard.timeLineStoryboard.instantiateRatingDetailsVC()
           vc.ratingId = ratingId
       vc.starredValue = starredVlaue
       vc.starCount = starCount
        self.navigationController?.pushViewController(vc, animated: true)
          }
    //MARK:- RATING DETAILS
    func ratingFullDetails(ratingId: Int, starredVlaue: String, starCount: Int) {
        let vcc = UIStoryboard.timeLineStoryboard.instantiateRatingDetailsVC()
//           vc.ratingId = ratingId
//       vc.starredValue = starredVlaue
//       vc.starCount = starCount
        self.navigationController?.pushViewController(vcc, animated: true)
          }
}
//MARK: EVENT TVC DELEGATES
extension TimeLineViewController: SendingRatingFromEventTVC {
    func moreOPtionFromEventTVC(sender: UIButton, id: Int, index: IndexPath, cell: EventTableViewCell, isOwner: Bool, postType: GraduateDetails) {
        postTypes = postType
        editedIndex = index
        if isOwner == true {
            texts.removeAll()
            texts = ["Share", "Edit","Delete"]
            sharePopOver(sender: sender)
        } else {
            texts.removeAll()
            texts = ["Share", "Report"]
            sharePopOver(sender: sender)
        }
    }
   
    func outsideCommentEventTVC(comment: Comments?, index: IndexPath) {
        dataSource?.data?.data?[index.row].comments?.append(comment!)
        tableView.reloadRows(at: [index], with: .automatic)
    }
    
    func pushingToProfileFromSpot(id: Int, userType: String, isOwner: Bool, postAsType: String) {
        pushToProfile(id: id, userType: userType, isOwner: isOwner, postAsType: postAsType)
       
    }
    func eventFeedbackAction(commentId: Int, index: IndexPath) {
         thoughtsDetails(commentId: commentId, index: index)
    }
    func sendingRatingEventTVC(id: Int, starValue: String, selectedindexToReload: Int, selectionType: GraduateDetails) {
        print(id)
        popingBottomVC(id: id, indexToReload: selectedindexToReload, selectionType: selectionType, starredValue: starValue)
    }
    
    func attendIgnoreEventTVC(universityId: Int, attendState: Bool, index: IndexPath!, attendeesCount: Int) {
        let url: String?
        print(universityId)
        let id = dataSource?.data?.data?[index.row].event?.internalIdentifier
        var attendeeCount = attendeesCount
        if attendState == true {
            url = (GraduateUrl.attendEventURL + "\(id ?? 0)")
            
            //attendeeCount = attendeesCount + 1
            
        } else {
            url = (GraduateUrl.unAttendEventURL + "\(id ?? 0)")
            //attendeeCount = attendeesCount - 1
        }
        
        GraduateDetailsResponse.requestToAttendIgnoreEvents(url: url ?? "") { [weak self](response) in
            guard let strongSelf = self else { return }
           print(response)
            
        }
    }
  
    func eventRatingDetails(ratingId: Int, starredVlaue: String, starCount: Int) {
        ratingDetails(ratingId: ratingId, starredVlaue: starredVlaue, starCount: starCount)
    }
 
    func fullScreenAction(vc: UIViewController) {
        currentTabBar?.setBar(hidden: true, animated: true)
        present(vc, animated: true, completion: nil)
    }
    
   
    func feedbackActionEventTVC() {
        thoughtsDetails(commentId: 0, index: [])
    }
    func sendingRatingEventTVC() {
        
    }
}
//MARK: RECEIVING RATING FROM THE PFTVC
extension TimeLineViewController: SendingRatingFromPFTVC{
    func pftvcthoughtsDetails(commentId: Int, outsideComment: Bool, index: IndexPath) {
        if outsideComment == false {
            thoughtsDetails(commentId: commentId, index: index)
        } else {
            
        }
    }
    
    func moreOPtionFromPortTVC(sender: UIButton, id: Int, index: IndexPath, cell: PortFolioTableViewCell, isOwner: Bool, postType: GraduateDetails) {
        postTypes = postType
        editedIndex = index
        if isOwner == true {
            texts.removeAll()
            texts = ["Share", "Edit","Delete"]
            sharePopOver(sender: sender)
        } else {
            texts.removeAll()
            texts = ["Share", "Report"]
            sharePopOver(sender: sender)
        }
    }
    
    func outsideCommentPortFolioTVC(comment: Comments?, index: IndexPath) {
        dataSource?.data?.data?[index.row].comments?.append(comment!)
        tableView.reloadRows(at: [index], with: .automatic)
    }
    
    func pushingTOProfileVCFromPFTVC(id: Int, userType: String, isOwner: Bool, postAsType: String) {
        pushToProfile(id: id, userType: userType, isOwner: isOwner, postAsType: postAsType)
    }
    
//    func moreOPtionFromPortTVC(sender: UIButton, id: Int, index: IndexPath, cell: PortFolioTableViewCell, isOwner: Bool) {
//        if isOwner == true {
//            sharePopOver(sender: sender)
//        } else {
//            texts.removeAll()
//            texts = ["Share", "Report"]
//            sharePopOver(sender: sender)
//        }
//    }
    func pftvcRating(id: Int, starValue: String, selectedindexToReload: Int, selectionType: GraduateDetails) {
        popingBottomVC(id: id, indexToReload: selectedindexToReload, selectionType: .portfolio, starredValue: starValue)
    }
    func pftvcratingDetails(ratingId: Int, starredValue: String, starCount: Int) {
        let vc = UIStoryboard.timeLineStoryboard.instantiateRatingDetailsVC()
        vc.ratingId = ratingId
        vc.starredValue = starredValue
        vc.starCount = starCount
        navigationController?.pushViewController(vc, animated: true)
    }
   
    func sendingThoughts(id: Int, param: [String : Any]) {
        
    }
    func pftvcthoughtsDetails(commentId: Int) {
        thoughtsDetails(commentId: commentId, index: [])
    }
   
    
    func fullScreen(vc: UIViewController) {
        currentTabBar?.setBar(hidden: true, animated: false)
        present(vc, animated: true, completion: nil)
    }
    func feedbackActionPFTVC() {
        thoughtsDetails(commentId: 0, index: [])
    }
    
    func moreOPtionFromPortTVC(sender: UIButton, index: IndexPath, cell: PortFolioTableViewCell) {
        
    }
}
//MARK: RECEIVING RATING FROM THE PCTVC
extension TimeLineViewController: SendingRatingFromPCTVC {
    func moreOptionFromPostTVC(sender: UIButton, id: Int, index: IndexPath, cell: PostCreationTableViewCell, isOwner: Bool, postType: GraduateDetails) {
        print(id,index,isOwner,postType)
        postTypes = postType
        editedIndex = index
        if isOwner == true {
            texts.removeAll()
            texts = ["Share", "Edit","Delete"]
            sharePopOver(sender: sender)
        } else {
            texts.removeAll()
            texts = ["Share", "Report"]
            sharePopOver(sender: sender)
        }
    }
    
    func ratingDetailFromPCTVC(ratingId: Int, starredValue: String, starCount: Int) {
        self.ratingDetails(ratingId: ratingId, starredVlaue: starredValue, starCount: starCount)
      
      
    }
    
    func outsideCommentPostTVC(comment: Comments?, index: IndexPath) {
        print(comment,index)
        dataSource?.data?.data?[index.row].comments?.append(comment!)
        tableView.reloadRows(at: [index], with: .automatic)
    }
    
    func pushingTOProfileVC(id: Int, userType: String, isOwner: Bool, postAsType: String) {
       pushToProfile(id: id, userType: userType, isOwner: isOwner, postAsType: postAsType)
    }
   
    func PCTVCRating(id: Int, starValue: String, selectedindexToReload: Int, selectionType: GraduateDetails) {
         popingBottomVC(id: id, indexToReload: selectedindexToReload, selectionType: selectionType, starredValue: starValue)
    }
    
    func feedbackActionPCTVC(commentId: Int, outsideComment: Bool, index: IndexPath) {
        if outsideComment == false {
            thoughtsDetails(commentId: commentId, index: index)
        } else {
            
        }
    }
   
    func thoughtsAction() {
//        let vc = UIStoryboard.timeLineStoryboard.instantiateThoughtsVC()
//        navigationController?.pushViewController(vc, animated: true)
    }
    func fullScreenImage(vc: UIViewController) {
        currentTabBar?.setBar(hidden: true, animated: true)
        present(vc, animated: true, completion: nil)
    }
    func PCTVCRating() {
        popingBottomVC(id: 0, indexToReload: 0, selectionType: .myPost, starredValue: "")
    }
//MARK:- PUSH TO PROFILE FUNC
    func pushToProfile(id: Int, userType: String, isOwner: Bool, postAsType: String) {
        if postAsType == "user" {
                    if isOwner == true {
                                 let vc = UIStoryboard.timeLineStoryboard.instantiateUserProfileVC()
                                 navigationController?.pushViewController(vc, animated: true)
                                 } else {
                                     let vc = UIStoryboard.timeLineStoryboard.instantiateOtherUserProfileVC()
                               vc.userId = id
                                            navigationController?.pushViewController(vc, animated: true)
                                 }
                    } else {
                        let vc = UIStoryboard.timeLineStoryboard.instantiateGraduateDetailsViewController()
                        vc.businessId = id
                        navigationController?.pushViewController(vc, animated: true)
                    }
    }
}

//MARK:- TIMELINE CREATE DELEGATE
extension TimeLineViewController: RecevingActionFromTimelineCreate {
    func postAction() {
        let vc = UIStoryboard.timeLineStoryboard.instantiateCreateSpotViewController()
        vc.currentTab?.setBar(hidden: true, animated: true)
        vc.vcType = .createPostFromOutside
        vc.delegate = self
        navigationController?.pushViewController(vc, animated: true)
        createPoptip.hide()
    }
    
    func goLiveAction() {
        createPoptip.hide()
        let vc = UIStoryboard.watchStoryboard.instantiateLiveViewController()
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func gradutaeSpotAction() {
        let vc = UIStoryboard.timeLineStoryboard.instantiateCreateSpotViewController()
        vc.tabBarController?.tabBar.isHidden = true
        vc.vcType = .createSpotFromOutside
        vc.delegate = self
        navigationController?.pushViewController(vc, animated: true)
        createPoptip.hide()
    }
    
    func graduateUniAction() {
        let vc = UIStoryboard.timeLineStoryboard.instantiateCreateUniversityVC()
        vc.hidesBottomBarWhenPushed = true
        navigationController?.pushViewController(vc, animated: false)
        createPoptip.hide()
    }
    
    func dealAction() {
        let vc = UIStoryboard.timeLineStoryboard.instantiateCreateSpotViewController()
        vc.currentTab?.setBar(hidden: true, animated: true)
        vc.vcType = .createDealFromOutside
        navigationController?.pushViewController(vc, animated: true)
        createPoptip.hide()
    }
    
    
}
//MARK:- DELEGATE RATING ACTION
extension TimeLineViewController: SendingActionFromRatingVC {
    func sendingActionWithType(type: RatingActionTypes, id: Int, reloadingIndex: Int, ratingNum: Int, selectionType: GraduateDetails) {
        switch type{
        case .requestforRating:
           ratingAfterApiCallOfEvent(reloadingIndex: reloadingIndex, ratingNum: ratingNum)
            reqestForPostRating(id: id, reloadingIndex: reloadingIndex, ratingNum: ratingNum, selectionType: selectionType)
        case .reuestForUnratingPost:
           unratingAfterApiCallOfEvent(reloadingIndex: reloadingIndex, ratingNum: ratingNum)
            reqestForPostUnRating(id: id, reloadingIndex: reloadingIndex, ratingNum: ratingNum, selectionType: selectionType)
        default:
            break
        }
    }
//MARK:- RATE POST
          func reqestForPostRating(id: Int,reloadingIndex: Int,ratingNum: Int,selectionType: GraduateDetails) {
              var headers: [String : Any]?
              if let token = UserDefaultsHandler.getUDValue(key: UDkey.token) as? String {
                  if headers == nil {
                      headers = ["Authorization": "Bearer \(token)", "Accept": "application/json"]
                  } else {
                      headers!["Authorization"] = "Bearer \(token)"
                      headers!["Accept"] = "application/json"
                  }
              }
              let url = (GraduateUrl.ratePostURL + "\(id)")
               print(url,ratingNum,selectionType)
              Alamofire.request(url,
                                method: .post,
                                parameters: ["star_value": "\(ratingNum + 1)"],headers: (headers as! HTTPHeaders))
                  .validate()
                  .responseJSON { [weak self]response in
                      guard let strongSelf = self else { return }
                      if let value = response.result.value as? [String : Any] {
                        //strongSelf.ratingAfterApiCallOfEvent(reloadingIndex: reloadingIndex, ratingNum: ratingNum)
//                        switch selectionType {
//                        case .gradSpot:
//                             strongSelf.ratingAfterApiCallOfEvent(reloadingIndex: reloadingIndex, ratingNum: ratingNum)
//                        case .offer:
//                            ratingAfterApiCallOFOffer(reloadingIndex: reloadingIndex, ratingNum: ratingNum)
//                        default:
//                            break
//                        }
                      
                          
                      }
                      
              }
          }
       
//MARK:- REQUEST FOR UNRATING POST
       func reqestForPostUnRating(id: Int,reloadingIndex: Int,ratingNum: Int,selectionType: GraduateDetails) {
           var headers: [String : Any]?
           if let token = UserDefaultsHandler.getUDValue(key: UDkey.token) as? String {
               if headers == nil {
                   headers = ["Authorization": "Bearer \(token)", "Accept": "application/json"]
               } else {
                   headers!["Authorization"] = "Bearer \(token)"
                   headers!["Accept"] = "application/json"
               }
           }
           let url = (GraduateUrl.unratePostURL + "\(id)")
           print(url,ratingNum,selectionType)
           Alamofire.request(url,method: .delete,headers: (headers as! HTTPHeaders))
               .validate()
               .responseJSON { [weak self]response in
                   guard let strongSelf = self else { return }
                   if let value = response.result.value as? [String : Any] {
                   // strongSelf.unratingAfterApiCallOfEvent(reloadingIndex: reloadingIndex, ratingNum: ratingNum)
//                    switch selectionType {
//                    case .gradSpot:
//                         strongSelf.unratingAfterApiCallOfEvent(reloadingIndex: reloadingIndex, ratingNum: ratingNum)
//                    case .offer:
//                        strongSelf.unratingAfterApiCallOfOffer(reloadingIndex: reloadingIndex, ratingNum: ratingNum)
//                    default:
//                        break
//                    }
                      
                       print(value)
                       print(reloadingIndex)
                       
                   }
                   
           }
       }
       func ratingAfterApiCallOfEvent(reloadingIndex: Int, ratingNum: Int) {
           print(dataSource?.data?.data?[reloadingIndex].hasStarred)
           if dataSource?.data?.data?[reloadingIndex].hasStarred == true {
              
           } else {
               
               dataSource?.data?.data?[reloadingIndex].starsCount! += 1
           }
           dataSource?.data?.data?[reloadingIndex].starredValue = "\((ratingNum + 1))"
           print(dataSource?.data?.data?[reloadingIndex].starredValue)
           print(reloadingIndex)
           dataSource?.data?.data?[reloadingIndex].hasStarred = true
           tableView.reloadData()
       }
       func unratingAfterApiCallOfEvent(reloadingIndex: Int, ratingNum: Int) {
           dataSource?.data?.data?[reloadingIndex].hasStarred = false
           if  dataSource?.data?.data?[reloadingIndex].starsCount ?? 0 > 0 {
               dataSource?.data?.data?[reloadingIndex].starsCount! -= 1
           } else {
               
           }
           dataSource?.data?.data?[reloadingIndex].starredValue = "\(0)"
           tableView.reloadData()
       }
//    func ratingAfterApiCallOFOffer(reloadingIndex: Int, ratingNum: Int) {
//        if dataSource?.data?.data?[reloadingIndex].hasStarred == true {
//            return
//        } else {
//
//            dataSource?.data?.data?[reloadingIndex].starsCount! += 1
//        }
//        dataSource?.Offer?.data?[reloadingIndex].starredValue = "\((ratingNum + 1))"
//        print(reloadingIndex)
//        dataSource?.Offer?.data?[reloadingIndex].hasStarred = true
//        tableView.reloadData()
//    }
//    func unratingAfterApiCallOfOffer(reloadingIndex: Int, ratingNum: Int) {
//        dataSource?.Offer?.data?[reloadingIndex].hasStarred = false
//        if  dataSource?.Offer?.data?[reloadingIndex].starsCount ?? 0 > 0 {
//            dataSource?.Offer?.data?[reloadingIndex].starsCount! -= 1
//        } else {
//
//        }
//        dataSource?.Offer?.data?[reloadingIndex].starredValue = "\(0)"
//        tableView.reloadData()
//    }
}

//MARK:- THOUGHTS DELEGATE
extension TimeLineViewController: SendingDataFromThoughtsVC {
    func sendingData(index: IndexPath, thoughtsCount: Int, commentsData: [Comments]) {
        print(index,thoughtsCount,commentsData)
        dataSource?.data?.data?[index.row].comments = commentsData
        tableView.reloadRows(at: [index], with: .automatic)
    }
    
    
}
//MARK:- ACTION FROM CREATESPOTVC
extension TimeLineViewController: ActionFromCreateSpotVC {
    func reloadWithType(type: ErrorType) {
        switch type {
        case .postEdited:
            gettingAllPost(pageNo: 1, showLoader:true, isLoadMore: false)
        case .portFolioEdited:
            gettingAllPost(pageNo: 1, showLoader:true, isLoadMore: false)
        default:
            gettingAllPost(pageNo: 1, showLoader:true, isLoadMore: false)
        }
        
    }
    func reloadTableViewData() {
        gettingAllPost(pageNo: 1, showLoader:true, isLoadMore: false)
    }
    
    
}
//MARK:- DELEGATE FROM LIVE STREAMING
extension TimeLineViewController: SendingRatingFromLiveStreamingTVC {
    func sendingRatingsFromLiveStreamingTVC(id: Int, starValue: String, selectedindexToReload: Int, selectionType: GraduateDetails) {
        popingBottomVC(id: id, indexToReload: selectedindexToReload, selectionType: selectionType, starredValue: starValue)
    }
    func moreOptionsFromLiveStreamingTVC(sender: UIButton, index: IndexPath, isOwner: Bool, postType: GraduateDetails) {
        postTypes = postType
        editedIndex = index
        if isOwner == true {
            texts.removeAll()
            texts = ["Share", "Edit","Delete"]
            sharePopOver(sender: sender)
        } else {
            texts.removeAll()
            texts = ["Share", "Report"]
            sharePopOver(sender: sender)
        }
    }
    func feedbackActionFromLiveStreamingTVC(commentId: Int, index: IndexPath!) {
//        if outsideComment == false {
            thoughtsDetails(commentId: commentId, index: index)
//        } else {
//
//        }
    }
    func sendingVideoUrlForFullScreenFromLiveStreamingTVC() {
        
    }
    func pushingTOProfileVCFromLiveStreamingTVC(id: Int, userType: String, isOwner: Bool, postasType: String) {
        pushToProfile(id: id, userType: userType, isOwner: isOwner, postAsType: postasType)
    }
    func liveRatingDetailsFromLiveStreamingTVC(ratingId: Int, starredVlaue: String, starCount: Int) {
        ratingDetails(ratingId: ratingId, starredVlaue: starredVlaue, starCount: starCount)
    }
    func commentOutsideFromLiveStreamingTVC(comment: Comments?, index: IndexPath) {
        dataSource?.data?.data?[index.row].comments?.append(comment!)
        tableView.reloadRows(at: [index], with: .automatic)
    }
}
extension UIView {
    class func fromNib<T: UIView>() -> T {
        return Bundle.main.loadNibNamed(String(describing: T.self), owner: nil, options: nil)![0] as! T
    }
}
struct TimeLineModel {
    var title: String?
    var data:  Any?
    //    var postCreation: [PostDataStruct]?
    //    var portfolio: [PortFolioDataStruct]?
    //    var event: [EventDataStruct]?
    
}
struct PostDataStruct {
    var tableViewCells: Int?
    var collectionVIewCells: Int?
}
struct PortFolioDataStruct {
    var tableViewCells: Int?
    var collectionVIewCells: Int?
}
struct EventDataStruct {
    var tableViewCells: Int?
    var collectionVIewCells: Int?
}
struct LiveDataStruct {
    var tableViewCells: Int?
    var collectionVIewCells: Int?
}
struct DiscountDataStruct {
    var tableViewCells: Int?
    var collectionVIewCells: Int?
}
extension UIView {
    
    func addToWindow()  {
        let window = UIApplication.shared.windows.filter {$0.isKeyWindow}.first
        self.frame = window!.bounds
        window?.addSubview(self)
    }
    func removeFromWindow()  {
        let window = UIApplication.shared.windows.filter {$0.isKeyWindow}.first
        self.frame = window!.bounds
        window?.removeFromSuperview()
    }
}

extension UIView {
    
    func hideAnimated(in stackView: UIStackView, speed: CGFloat) {
        if !self.isHidden {
            UIView.animate(
                withDuration: 0.5,
                delay: 0,
                usingSpringWithDamping: 0.9,
                initialSpringVelocity: 1,
                options: [],
                animations: {
                    self.alpha = 0
                    self.isHidden = true
                    stackView.layoutIfNeeded()
            },
                completion: nil
            )
        }
    }
   
}
extension TimeLineViewController: SendingActionFromErrorChat {
    func sendingAction() {
            
    }
    
    func sendingCancelAction() {
        
    }
    
    func adctionwithType(type: ErrorType) {
        switch type {
        case .deletePortFolio:
deletePortfolio()
        case .deleteOffer:
            requestToDeleteOFfer()
        case .deleteEvent:
            requestToDeleteEvent()
        case .deletePost:
            requestToDeletePost()
        default:
            break
        }
    }
    //MARK:- DELETE PORTFOLIO
        func deletePortfolio() {
            GraduateDetailsResponse.requestToDeletePortfolio(id: dataSource?.data?.data?[editedIndex.row].portfolio?.internalIdentifier ?? 0, url: "") { [weak self](response) in
                guard let strongSelf = self else { return }
                strongSelf.gettingAllPost(pageNo: 1, showLoader: true, isLoadMore: false)
               // strongSelf.popUp(type: .portfolioDeleted, message: "")
            }
        }
    //MARK:- DELTE OFFER
        func requestToDeleteOFfer() {
            GraduateDetailsResponse.requestToDeleteOffer(id: dataSource?.data?.data?[editedIndex.row].offer?.internalIdentifier ?? 0, url: "") { [weak self](response) in
                guard let strongSelf = self else { return }
                strongSelf.gettingAllPost(pageNo: 1, showLoader: true, isLoadMore: false)
                //strongSelf.popUp(type: .offerDeleted, message: "")
            }
        }
        //MARK:- DELTE EVENT
            func requestToDeleteEvent() {
                print(dataSource?.data?.data?[editedIndex.row].event?.internalIdentifier)
                GraduateDetailsResponse.requestToDeleteOffer(id: dataSource?.data?.data?[editedIndex.row].offer?.internalIdentifier ?? 0, url: "") { [weak self](response) in
                    guard let strongSelf = self else { return }
                    strongSelf.gettingAllPost(pageNo: 1, showLoader: true, isLoadMore: false)
                    //strongSelf.popUp(type: .offerDeleted, message: "")
                }
            }
    //MARK:- REQUEST TO DELTE POST
        func requestToDeletePost() {
            print(dataSource?.data?.data?[editedIndex.row].internalIdentifier ?? 0)
            WatchResponse.requestToDeletePost(id: dataSource?.data?.data?[editedIndex.row].internalIdentifier ?? 0,showHud: true) { [weak self](response) in
                guard let strongSelf = self else { return }
                strongSelf.gettingAllPost(pageNo: 1, showLoader: true, isLoadMore: false)
            }
        }
    
    
}



