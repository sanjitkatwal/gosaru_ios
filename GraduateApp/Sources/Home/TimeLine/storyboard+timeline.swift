//
//  storyboard+timeline.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 6/24/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import Foundation
import UIKit
extension UIStoryboard {
    private struct Constants {
        static let timelineStoryboard = "TimeLine"
        static let timeLineIdentifier = "TimeLineViewController"
        static let createPopUpIdentifier = "CreatePopUpViewController"
        static let createSpotIdentifier = "CreateSpotViewController"
        static let selectionPopUpIdentifier = "SelectionBottomPopUPViewController"
        static let selectionCreateUniversityIdentifier = "CreateUniversityViewController"
        static let ratingIdentifier = "RatingViewController"
        static let fullScreenIdentifier = "FullScreenViewController"
        static let profileIdentifier = "ProfileViewController"
        static let errorChatMsgIdentifier = "ErrorChatMsgViewController"
        static let messageIdentifier = "MessageViewController"
        static let thoughtsIdentifier = "ThoughtsViewController"
        static let searchIdentifier = "SearchViewController"
        static let ratingDetailsIdentifier = "RatingDetailsViewController"
        static let userProfileIdentifier = "UserProfileViewController"
        static let deletePostIdentifier = "DeletePostPopUp"
        static let reusablepopUpIdentifier = "ReusablePopUpViewController"
        static let followesIdentifier = "FollowersViewController"
        static let postAndUniIdentifier = "PostAndUniViewController"
        static let otherUserProfileIdentifier = "OtherUserProfileViewController"
        static let searchDetailsIdentifier = "SearchDetailsViewController"
        
        
        
        
        
        
        
        
    }
    static var timeLineStoryboard: UIStoryboard {
        return UIStoryboard(name: Constants.timelineStoryboard, bundle: nil)
    }
    func instantiateTimeLineViewController() -> TimeLineViewController {
        guard let viewController = UIStoryboard.timeLineStoryboard.instantiateViewController(withIdentifier: Constants.timeLineIdentifier) as? TimeLineViewController else {
            fatalError("Couldn't instantiate TimeLineViewController")
        }
        return viewController
    }
    func instantiateCreatePopUpViewController() -> CreatePopUpViewController {
        guard let viewController = UIStoryboard.timeLineStoryboard.instantiateViewController(withIdentifier: Constants.createPopUpIdentifier) as? CreatePopUpViewController else {
            fatalError("Couldn't instantiate CreatePopUpViewController")
        }
        return viewController
    }
    func instantiateCreateSpotViewController() -> CreateSpotViewController {
        guard let viewController = UIStoryboard.timeLineStoryboard.instantiateViewController(withIdentifier: Constants.createSpotIdentifier) as? CreateSpotViewController else {
            fatalError("Couldn't instantiate CreateSpotViewController")
        }
        return viewController
    }
    func instantiateSelectionBottomPopUpVC() -> SelectionBottomPopUPViewController {
        guard let viewController = UIStoryboard.timeLineStoryboard.instantiateViewController(withIdentifier: Constants.selectionPopUpIdentifier) as? SelectionBottomPopUPViewController else {
            fatalError("Couldn't instantiate SelectionBottomPopUPViewController")
        }
        return viewController
    }
    func instantiateCreateUniversityVC() -> CreateUniversityViewController {
        guard let viewController = UIStoryboard.timeLineStoryboard.instantiateViewController(withIdentifier: Constants.selectionCreateUniversityIdentifier) as? CreateUniversityViewController else {
            fatalError("Couldn't instantiate CreateUniversityViewController")
        }
        return viewController
    }
    func instantiateRatingVC() -> RatingViewController {
        guard let viewController = UIStoryboard.timeLineStoryboard.instantiateViewController(withIdentifier: Constants.ratingIdentifier) as? RatingViewController else {
            fatalError("Couldn't instantiate RatingViewController")
        }
        return viewController
    }
    func instantiateFullScreenVC() -> FullScreenViewController {
        guard let viewController = UIStoryboard.timeLineStoryboard.instantiateViewController(withIdentifier: Constants.fullScreenIdentifier) as? FullScreenViewController else {
            fatalError("Couldn't instantiate FullScreenViewController")
        }
        return viewController
    }
    func instantiateProfileInfoVC() -> ProfileViewController {
        guard let viewController = UIStoryboard.timeLineStoryboard.instantiateViewController(withIdentifier: Constants.profileIdentifier) as? ProfileViewController else {
            fatalError("Couldn't instantiate ProfileViewController")
        }
        return viewController
    }
    func instantiateErrorCarMsgPopVC() -> ErrorChatMsgViewController {
        guard let viewController = UIStoryboard.timeLineStoryboard.instantiateViewController(withIdentifier: Constants.errorChatMsgIdentifier) as? ErrorChatMsgViewController else {
            fatalError("Couldn't instantiate ErrorChatMsgViewController")
        }
        return viewController
    }
    func instantiateMessageVC() -> MessageViewController {
        guard let viewController = UIStoryboard.timeLineStoryboard.instantiateViewController(withIdentifier: Constants.messageIdentifier) as? MessageViewController else {
            fatalError("Couldn't instantiate MessageViewController")
        }
        return viewController
    }
    func instantiateThoughtsVC() -> ThoughtsViewController {
        guard let viewController = UIStoryboard.timeLineStoryboard.instantiateViewController(withIdentifier: Constants.thoughtsIdentifier) as? ThoughtsViewController else {
            fatalError("Couldn't instantiate ThoughtsViewController")
        }
        return viewController
    }
    func instantiateSearchVC() -> SearchViewController {
        guard let viewController = UIStoryboard.timeLineStoryboard.instantiateViewController(withIdentifier: Constants.searchIdentifier) as? SearchViewController else {
            fatalError("Couldn't instantiate SearchViewController")
        }
        return viewController
    }
    func instantiateRatingDetailsVC() -> RatingDetailsViewController {
        guard let viewController = UIStoryboard.timeLineStoryboard.instantiateViewController(withIdentifier: Constants.ratingDetailsIdentifier) as? RatingDetailsViewController else {
            fatalError("Couldn't instantiate RatingDetailsViewController")
        }
        return viewController
    }
    func instantiateUserProfileVC() -> UserProfileViewController {
        guard let viewController = UIStoryboard.timeLineStoryboard.instantiateViewController(withIdentifier: Constants.userProfileIdentifier) as? UserProfileViewController else {
            fatalError("Couldn't instantiate UserProfileViewController")
        }
        return viewController
    }
    func instantiateDeletePostPopUpVC() -> DeletePostPopUp {
        guard let viewController = UIStoryboard.timeLineStoryboard.instantiateViewController(withIdentifier: Constants.deletePostIdentifier) as? DeletePostPopUp else {
            fatalError("Couldn't instantiate DeletePostPopUp")
        }
        return viewController
    }
    func instantiateReusablePopUpVC() -> ReusablePopUpViewController {
        guard let viewController = UIStoryboard.timeLineStoryboard.instantiateViewController(withIdentifier: Constants.reusablepopUpIdentifier) as? ReusablePopUpViewController else {
            fatalError("Couldn't instantiate ReusablePopUpViewController")
        }
        return viewController
    }
    func instantiatePostAndUniVC() -> PostAndUniViewController {
        guard let viewController = UIStoryboard.timeLineStoryboard.instantiateViewController(withIdentifier: Constants.postAndUniIdentifier) as? PostAndUniViewController else {
            fatalError("Couldn't instantiate PostAndUniViewController")
        }
        return viewController
    }
    func instantiateOtherUserProfileVC() -> OtherUserProfileViewController {
        guard let viewController = UIStoryboard.timeLineStoryboard.instantiateViewController(withIdentifier: Constants.otherUserProfileIdentifier) as? OtherUserProfileViewController else {
            fatalError("Couldn't instantiate OtherUserProfileViewController")
        }
        return viewController
    }
    func instantiateSearchDetailsVC() -> SearchDetailsViewController {
        guard let viewController = UIStoryboard.timeLineStoryboard.instantiateViewController(withIdentifier: Constants.searchDetailsIdentifier) as? SearchDetailsViewController else {
            fatalError("Couldn't instantiate SearchDetailsViewController")
        }
        return viewController
    }
    func instantiateRatingListVC() -> RatingListingViewController {
        guard let viewController = UIStoryboard.timeLineStoryboard.instantiateViewController(withIdentifier: "RatingListingViewController") as? RatingListingViewController else {
            fatalError("Couldn't instantiate RatingListingViewController")
        }
        return viewController
    }
    
    
}
