//
//  RatingDetailsResponse.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 10/1/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import Foundation
import ObjectMapper

class RatingDetailssResponse: DefaultResponse {
    var data: LoginResponseModel?
override func mapping(map: Map) {
  super.mapping(map: map)

}
// MARK: - RATING DETAILS  request
    class func requestToRatingDetails(ratingId: String,showHud: Bool = true ,completionHandler:@escaping ((RatingResponseModel?) -> Void)) {
        APIManager(urlString: (GraduateUrl.getStarURL + ratingId), method: .get ).handleResponse(showProgressHud: showHud,completionHandler: { (response: RatingResponseModel) in
              print(response)
                  completionHandler(response)
              }, failureBlock: {
                completionHandler(nil)
              })
      }
}

class RatingResponseModel: DefaultResponse {

// MARK: Declaration for string constants to be used to decode and also serialize.
internal let kRatingResponseModel2Key: String = "2"
internal let kRatingResponseModel5Key: String = "5"


// MARK: Properties
public var one: [RatingStarModel]?
public var two: [RatingStarModel]?
public var three: [RatingStarModel]?
public var four: [RatingStarModel]?
public var five: [RatingStarModel]?



// MARK: ObjectMapper Initalizers
/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/


/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/
    override func mapping(map: Map) {
    one <- map["1"]
    two <- map[kRatingResponseModel2Key]
    three <- map["3"]
    four <- map["4"]
five <- map[kRatingResponseModel5Key]
    

}
}
public class RatingStarModel: Mappable {
    public required init?(map: Map) {
        
    }
    

// MARK: Declaration for string constants to be used to decode and also serialize.
internal let k2CreatedAtKey: String = "created_at"
internal let k2UserIdKey: String = "user_id"
internal let k2ProfileImageKey: String = "profile_image"
internal let k2FriendshipStatusKey: String = "friendship_status"
internal let k2DataKey: String = "data"
internal let k2NameKey: String = "name"


// MARK: Properties
public var createdAt: String?
public var userId: Int?
public var profileImage: String?
public var friendshipStatus: Int?
public var data: String?
public var name: String?



// MARK: ObjectMapper Initalizers
/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/


/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/
    public func mapping(map: Map) {
    createdAt <- map[k2CreatedAtKey]
    userId <- map[k2UserIdKey]
    profileImage <- map[k2ProfileImageKey]
    friendshipStatus <- map[k2FriendshipStatusKey]
    data <- map[k2DataKey]
    name <- map[k2NameKey]

}
}
