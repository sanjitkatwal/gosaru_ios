//
//  TimelineResponse.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 9/14/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import Foundation
import ObjectMapper

class TimeLineResponse: DefaultResponse {
    var data: LoginResponseModel?
override func mapping(map: Map) {
  super.mapping(map: map)

}
// MARK: - POST LIST REQUEST
    class func requestToAllPost(params: [String : Any],showHud: Bool = true ,completionHandler:@escaping ((TimeLineResponseModel?) -> Void)) {
        APIManager(urlString: GraduateUrl.postListURL, parameters: params, method: .get ).handleResponse(showProgressHud: showHud,completionHandler: { (response: TimeLineResponseModel) in
              print(response)
                  completionHandler(response)
              }, failureBlock: {
                completionHandler(nil)
              })
      }
}
class TimeLineResponseModel: DefaultResponse {
   
    

// MARK: Declaration for string constants to be used to decode and also serialize.
internal let kTimeLineResponseModelDataKey: String = "data"
internal let kTimeLineResponseModelLinksKey: String = "links"
internal let kTimeLineResponseModelMetaKey: String = "meta"


// MARK: Properties
public var data: [uniMyPostData]?
public var links: TimeLineLinks?
public var meta: TimeLineMeta?



// MARK: ObjectMapper Initalizers
/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/

/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/
    override func mapping(map: Map) {
    data <- map[kTimeLineResponseModelDataKey]
    links <- map[kTimeLineResponseModelLinksKey]
    meta <- map[kTimeLineResponseModelMetaKey]

}
}
class TimeLineData: Mappable {
    required init?(map: Map) {
        
    }
    

// MARK: Declaration for string constants to be used to decode and also serialize.
internal let kDataHasStarredKey: String = "has_starred"
internal let kDataPostingAsKey: String = "posting_as"
internal let kDataBusinessKey: String = "business"
internal let kDataGalleriesKey: String = "galleries"
internal let kDataHasSharedKey: String = "has_shared"
internal let kDataEventKey: String = "event"
internal let kDataViewsCountKey: String = "views_count"
internal let kDataHasCommentedKey: String = "has_commented"
internal let kDataLiveVideoRtmpKey: String = "live_video_rtmp"
internal let kDataLiveVideoMp4Key: String = "live_video_mp4"
internal let kDataIsSharedPostKey: String = "is_shared_post"
internal let kDataInternalIdentifierKey: String = "id"
internal let kDataUpdatedAtKey: String = "updated_at"
internal let kDataSharedCountKey: String = "shared_count"
internal let kDataCommentsKey: String = "comments"
internal let kDataStarsCountKey: String = "stars_count"
internal let kDataLiveKeyKey: String = "live_key"
internal let kDataPortfolioKey: String = "portfolio"
internal let kDataCreatedAtKey: String = "created_at"
internal let kDataPostingAsTypeKey: String = "posting_as_type"
internal let kDataSourceCountryKey: String = "source_country"
internal let kDataVisibilityTypeKey: String = "visibility_type"
internal let kDataIsOwnerKey: String = "is_owner"
internal let kDataOfferKey: String = "offer"
internal let kDataStarsKey: String = "stars"
internal let kDataStarredValueKey: String = "starred_value"
internal let kDataHasLiveEndedKey: String = "has_live_ended"
internal let kDataMessageKey: String = "message"
internal let kDataUserIdKey: String = "user_id"


// MARK: Properties
public var hasStarred: Bool = false
public var postingAs: TimeLinePostingAs?
public var business: String?
public var galleries: [TimeLineGalleries]?
public var hasShared: Bool = false
public var event: Event?
public var viewsCount: Int?
public var hasCommented: Bool = false
public var liveVideoRtmp: String?
public var liveVideoMp4: String?
public var isSharedPost: Bool = false
public var internalIdentifier: Int?
public var updatedAt: String?
public var sharedCount: Int?
public var comments: [Comments]?
public var starsCount: Int?
public var liveKey: String?
public var portfolio: UniPortfolio?
public var createdAt: String?
public var postingAsType: String?
public var sourceCountry: String?
public var visibilityType: String?
public var isOwner: Bool = false
public var offer: TimeLineOffer?
public var stars: TimeLineStars?
public var starredValue: String?
public var hasLiveEnded: Bool = false
public var message: String?
public var userId: Int?



// MARK: ObjectMapper Initalizers
/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/


/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/
func mapping(map: Map) {
    hasStarred <- map[kDataHasStarredKey]
    postingAs <- map[kDataPostingAsKey]
    business <- map[kDataBusinessKey]
    galleries <- map[kDataGalleriesKey]
    hasShared <- map[kDataHasSharedKey]
    event <- map[kDataEventKey]
    viewsCount <- map[kDataViewsCountKey]
    hasCommented <- map[kDataHasCommentedKey]
    liveVideoRtmp <- map[kDataLiveVideoRtmpKey]
    liveVideoMp4 <- map[kDataLiveVideoMp4Key]
    isSharedPost <- map[kDataIsSharedPostKey]
    internalIdentifier <- map[kDataInternalIdentifierKey]
    updatedAt <- map[kDataUpdatedAtKey]
    sharedCount <- map[kDataSharedCountKey]
    comments <- map[kDataCommentsKey]
    starsCount <- map[kDataStarsCountKey]
    liveKey <- map[kDataLiveKeyKey]
    portfolio <- map[kDataPortfolioKey]
    createdAt <- map[kDataCreatedAtKey]
    postingAsType <- map[kDataPostingAsTypeKey]
    sourceCountry <- map[kDataSourceCountryKey]
    visibilityType <- map[kDataVisibilityTypeKey]
    isOwner <- map[kDataIsOwnerKey]
    offer <- map[kDataOfferKey]
    stars <- map[kDataStarsKey]
    starredValue <- map[kDataStarredValueKey]
    hasLiveEnded <- map[kDataHasLiveEndedKey]
    message <- map[kDataMessageKey]
    userId <- map[kDataUserIdKey]

}
}
public class Event: Mappable {
    required public init?(map: Map) {
        
    }
// MARK: Declaration for string constants to be used to decode and also serialize.
internal let kEventCreatedAtKey: String = "created_at"
internal let kEventIsAttendedKey: String = "is_attended"
internal let kEventCreatorIdKey: String = "creator_id"
internal let kEventCountryKey: String = "country"
internal let kEventEventTitleKey: String = "event_title"
internal let kEventEventStartTimeKey: String = "event_start_time"
internal let kEventAttendCountKey: String = "attend_count"
internal let kEventPostcodeKey: String = "postcode"
internal let kEventEventTypeKey: String = "event_type"
internal let kEventDescriptionValueKey: String = "description"
internal let kEventLatKey: String = "lat"
internal let kEventAddressStringKey: String = "address_string"
internal let kEventInternalIdentifierKey: String = "id"
internal let kEventUpdatedAtKey: String = "updated_at"
internal let kEventStreetKey: String = "street"
internal let kEventEventStartDateKey: String = "event_start_date"
internal let kEventLonKey: String = "lon"
internal let kEventEventEndDateKey: String = "event_end_date"
internal let kEventSuburbKey: String = "suburb"
internal let kEventStateKey: String = "state"


// MARK: Properties
public var createdAt: String?
public var isAttended: Bool = false
public var creatorId: Int?
public var country: String?
public var eventTitle: String?
public var eventStartTime: String?
public var attendCount: Int?
public var postcode: String?
public var eventType: String?
public var descriptionValue: String?
public var lat: String?
public var addressString: String?
public var internalIdentifier: Int?
public var updatedAt: String?
public var street: String?
public var eventStartDate: String?
public var lon: String?
public var eventEndDate: String?
public var suburb: String?
public var state: String?



// MARK: ObjectMapper Initalizers
/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/

/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/
    public func mapping(map: Map) {
    createdAt <- map[kEventCreatedAtKey]
    isAttended <- map[kEventIsAttendedKey]
    creatorId <- map[kEventCreatorIdKey]
    country <- map[kEventCountryKey]
    eventTitle <- map[kEventEventTitleKey]
    eventStartTime <- map[kEventEventStartTimeKey]
    attendCount <- map[kEventAttendCountKey]
    postcode <- map[kEventPostcodeKey]
    eventType <- map[kEventEventTypeKey]
    descriptionValue <- map[kEventDescriptionValueKey]
    lat <- map[kEventLatKey]
    addressString <- map[kEventAddressStringKey]
    internalIdentifier <- map[kEventInternalIdentifierKey]
    updatedAt <- map[kEventUpdatedAtKey]
    street <- map[kEventStreetKey]
    eventStartDate <- map[kEventEventStartDateKey]
    lon <- map[kEventLonKey]
    eventEndDate <- map[kEventEventEndDateKey]
    suburb <- map[kEventSuburbKey]
    state <- map[kEventStateKey]

}
}
class TimeLineOffer: Mappable {
    required init?(map: Map) {
        
    }
    

// MARK: Declaration for string constants to be used to decode and also serialize.
internal let kOfferCreatedAtKey: String = "created_at"
internal let kOfferGalleriesKey: String = "galleries"
internal let kOfferCountryKey: String = "country"
internal let kOfferDescriptionValueKey: String = "description"
internal let kOfferPostcodeKey: String = "postcode"
internal let kOfferBusinessIdKey: String = "business_id"
internal let kOfferLatKey: String = "lat"
internal let kOfferAddressStringKey: String = "address_string"
internal let kOfferTypeKey: String = "type"
internal let kOfferInternalIdentifierKey: String = "id"
internal let kOfferTermsKey: String = "terms"
internal let kOfferStreetKey: String = "street"
internal let kOfferUpdatedAtKey: String = "updated_at"
internal let kOfferLonKey: String = "lon"
internal let kOfferDiscountPercentageKey: String = "discount_percentage"
internal let kOfferDiscountAmountKey: String = "discount_amount"
internal let kOfferExpiryDateKey: String = "expiry_date"
internal let kOfferSuburbKey: String = "suburb"
internal let kOfferStateKey: String = "state"


// MARK: Properties
public var createdAt: String?
public var galleries: [TimeLineGalleries]?
public var country: String?
public var descriptionValue: String?
public var postcode: String?
public var businessId: Int?
public var lat: String?
public var addressString: String?
public var type: String?
public var internalIdentifier: Int?
public var terms: String?
public var street: String?
public var updatedAt: String?
public var lon: String?
public var discountPercentage: String?
public var discountAmount: String?
public var expiryDate: String?
public var suburb: String?
public var state: String?



// MARK: ObjectMapper Initalizers
/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/

/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/
func mapping(map: Map) {
    createdAt <- map[kOfferCreatedAtKey]
    galleries <- map[kOfferGalleriesKey]
    country <- map[kOfferCountryKey]
    descriptionValue <- map[kOfferDescriptionValueKey]
    postcode <- map[kOfferPostcodeKey]
    businessId <- map[kOfferBusinessIdKey]
    lat <- map[kOfferLatKey]
    addressString <- map[kOfferAddressStringKey]
    type <- map[kOfferTypeKey]
    internalIdentifier <- map[kOfferInternalIdentifierKey]
    terms <- map[kOfferTermsKey]
    street <- map[kOfferStreetKey]
    updatedAt <- map[kOfferUpdatedAtKey]
    lon <- map[kOfferLonKey]
    discountPercentage <- map[kOfferDiscountPercentageKey]
    discountAmount <- map[kOfferDiscountAmountKey]
    expiryDate <- map[kOfferExpiryDateKey]
    suburb <- map[kOfferSuburbKey]
    state <- map[kOfferStateKey]

}
}
 class Comments: Mappable {
    public required init?(map: Map) {
        
    }
    

// MARK: Declaration for string constants to be used to decode and also serialize.
internal let kCommentsHasStarredKey: String = "has_starred"
internal let kCommentsCreatedAtKey: String = "created_at"
internal let kCommentsStarredValueKey: String = "starred_value"
internal let kCommentsIsOwnCommentKey: String = "is_own_comment"
internal let kCommentsInternalIdentifierKey: String = "id"
internal let kCommentsUpdatedAtKey: String = "updated_at"
internal let kCommentsBodyKey: String = "body"
internal let kCommentsUserIdKey: String = "user_id"
internal let kCommentsUserKey: String = "user"
internal let kCommentsStarsCountKey: String = "stars_count"
internal let kCommentsStarsKey: String = "stars"


// MARK: Properties
public var hasStarred: Bool = false
public var createdAt: String?
public var starredValue: String?
public var isOwnComment: Bool = false
public var internalIdentifier: Int?
public var updatedAt: String?
public var body: String?
public var userId: Int?
public var user: TimeLineUser?
public var starsCount: Int?
public var stars: [TimeLineStars]?



// MARK: ObjectMapper Initalizers
/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/


/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/
    public func mapping(map: Map) {
    hasStarred <- map[kCommentsHasStarredKey]
    createdAt <- map[kCommentsCreatedAtKey]
    starredValue <- map[kCommentsStarredValueKey]
    isOwnComment <- map[kCommentsIsOwnCommentKey]
    internalIdentifier <- map[kCommentsInternalIdentifierKey]
    updatedAt <- map[kCommentsUpdatedAtKey]
    body <- map[kCommentsBodyKey]
    userId <- map[kCommentsUserIdKey]
    user <- map[kCommentsUserKey]
    starsCount <- map[kCommentsStarsCountKey]
    stars <- map[kCommentsStarsKey]

}
}
public class TimeLineUser: Mappable {
    public required init?(map: Map) {
        
    }
    

// MARK: Declaration for string constants to be used to decode and also serialize.
internal let kUserNameKey: String = "name"
internal let kUserInternalIdentifierKey: String = "id"
internal let kUserPhoneKey: String = "phone"
internal let kUserProfileKey: String = "profile"
internal let kUserIsVerifiedKey: String = "is_verified"
internal let kUserEmailKey: String = "email"
internal let kUserUsernameKey: String = "username"


// MARK: Properties
public var name: String?
public var internalIdentifier: Int?
public var phone: String?
public var profile: TimeLineProfile?
public var isVerified: Bool = false
public var email: String?
public var username: String?



// MARK: ObjectMapper Initalizers
/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/


/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/
    public func mapping(map: Map) {
    name <- map[kUserNameKey]
    internalIdentifier <- map[kUserInternalIdentifierKey]
    phone <- map[kUserPhoneKey]
    profile <- map[kUserProfileKey]
    isVerified <- map[kUserIsVerifiedKey]
    email <- map[kUserEmailKey]
    username <- map[kUserUsernameKey]

}
}
public class TimeLineProfile: Mappable {
    public required init?(map: Map) {
        
    }
    

// MARK: Declaration for string constants to be used to decode and also serialize.
internal let kProfileProfileImageKey: String = "profile_image"
internal let kProfileNicknameKey: String = "nickname"
internal let kProfileGenderKey: String = "gender"


// MARK: Properties
public var profileImage: String?
public var nickname: String?
public var gender: String?



// MARK: ObjectMapper Initalizers
/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/


/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/
    public func mapping(map: Map) {
    profileImage <- map[kProfileProfileImageKey]
    nickname <- map[kProfileNicknameKey]
    gender <- map[kProfileGenderKey]

}
}

    public class TimeLineStars: Mappable {
        required public init?(map: Map) {
            
        }
        

// MARK: Declaration for string constants to be used to decode and also serialize.
internal let kStars4Key: String = "4"
internal let kStars5Key: String = "5"
internal let kStars0Key: String = "0"
internal let kStars3Key: String = "3"
internal let kStars2Key: String = "2"
internal let kStars1Key: String = "1"
      


// MARK: Properties
public var four: Int?
public var five: Int?
public var zero: Int?
public var three: Int?
public var two: Int?
public var one: Int?



// MARK: ObjectMapper Initalizers
/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/


/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/
        public func mapping(map: Map) {
    four <- map[kStars4Key]
    five <- map[kStars5Key]
    zero <- map[kStars0Key]
    three <- map[kStars3Key]
    two <- map[kStars2Key]
    one <- map[kStars1Key]

}
}

class TimeLineGalleries: Mappable {
    required init?(map: Map) {
        
    }
    

// MARK: Declaration for string constants to be used to decode and also serialize.
internal let kGalleriesCreatedAtKey: String = "created_at"
internal let kGalleriesInternalIdentifierKey: String = "id"
internal let kGalleriesUpdatedAtKey: String = "updated_at"
internal let kGalleriesDescriptionValueKey: String = "description"
internal let kGalleriesFilepathKey: String = "filepath"
internal let kGalleriesMediaTypeKey: String = "media_type"
internal let kGalleriesCaptionKey: String = "caption"
internal let kGalleriesBusinessOfferIdKey: String = "business_offer_id"


// MARK: Properties
public var createdAt: String?
public var internalIdentifier: Int?
public var updatedAt: String?
public var descriptionValue: String?
public var filepath: String?
public var mediaType: String?
public var caption: String?
public var businessOfferId: Int?



// MARK: ObjectMapper Initalizers
/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/


/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/
func mapping(map: Map) {
    createdAt <- map[kGalleriesCreatedAtKey]
    internalIdentifier <- map[kGalleriesInternalIdentifierKey]
    updatedAt <- map[kGalleriesUpdatedAtKey]
    descriptionValue <- map[kGalleriesDescriptionValueKey]
    filepath <- map[kGalleriesFilepathKey]
    mediaType <- map[kGalleriesMediaTypeKey]
    caption <- map[kGalleriesCaptionKey]
    businessOfferId <- map[kGalleriesBusinessOfferIdKey]

}
}
public class TimeLinePostingAs: Mappable {
    public required init?(map: Map) {
        
    }
    

// MARK: Declaration for string constants to be used to decode and also serialize.
internal let kPostingAsNameKey: String = "name"
internal let kPostingAsIsOwnBusinessKey: String = "is_own_business"
internal let kPostingAsBusinessProfileImageKey: String = "business_profile_image"
internal let kPostingAsInternalIdentifierKey: String = "id"
internal let kPostingAsBusinessNameKey: String = "business_name"
internal let kPostingAsProfileKey: String = "profile"


// MARK: Properties
public var name: String?
public var isOwnBusiness: Bool = false
public var businessProfileImage: String?
public var internalIdentifier: Int?
public var businessName: String?
 var profile: Profile?



// MARK: ObjectMapper Initalizers
/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/


/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/
    public func mapping(map: Map) {
    name <- map[kPostingAsNameKey]
    isOwnBusiness <- map[kPostingAsIsOwnBusinessKey]
    businessProfileImage <- map[kPostingAsBusinessProfileImageKey]
    internalIdentifier <- map[kPostingAsInternalIdentifierKey]
    businessName <- map[kPostingAsBusinessNameKey]
    profile <- map[kPostingAsProfileKey]

}
}

class TimeLineLinks: Mappable {
    required init?(map: Map) {
        
    }
    

// MARK: Declaration for string constants to be used to decode and also serialize.
internal let kLinksNextKey: String = "next"
internal let kLinksLastKey: String = "last"
internal let kLinksPrevKey: String = "prev"
internal let kLinksFirstKey: String = "first"


// MARK: Properties
public var next: String?
public var last: String?
public var prev: String?
public var first: String?



// MARK: ObjectMapper Initalizers
/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/


/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/
func mapping(map: Map) {
    next <- map[kLinksNextKey]
    last <- map[kLinksLastKey]
    prev <- map[kLinksPrevKey]
    first <- map[kLinksFirstKey]

}
}
class TimeLineMeta: Mappable {
    required init?(map: Map) {
        
    }
    

// MARK: Declaration for string constants to be used to decode and also serialize.
internal let kMetaFromKey: String = "from"
internal let kMetaLastPageKey: String = "last_page"
internal let kMetaPerPageKey: String = "per_page"
internal let kMetaCurrentPageKey: String = "current_page"
internal let kMetaPathKey: String = "path"
internal let kMetaTotalKey: String = "total"
internal let kMetaToKey: String = "to"


// MARK: Properties
public var from: Int?
public var lastPage: Int?
public var perPage: Int?
public var currentPage: Int?
public var path: String?
public var total: Int?
public var to: Int?



// MARK: ObjectMapper Initalizers
/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/


/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/
func mapping(map: Map) {
    from <- map[kMetaFromKey]
    lastPage <- map[kMetaLastPageKey]
    perPage <- map[kMetaPerPageKey]
    currentPage <- map[kMetaCurrentPageKey]
    path <- map[kMetaPathKey]
    total <- map[kMetaTotalKey]
    to <- map[kMetaToKey]

}
}
