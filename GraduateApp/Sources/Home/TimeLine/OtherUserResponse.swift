//
//  OtherUserResponse.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 10/15/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import Foundation
import ObjectMapper

class OtherUserResponse: DefaultResponse {
     var datas: [uniMyPostData]?
    var friendsData: [FullDetailsUserData]? = []
    var meta: TimeLineMeta?
    
override func mapping(map: Map) {
  super.mapping(map: map)
datas <- map["data"]
friendsData <- map["data"]
}
// MARK: - GET USER NOTIFICATIONS
    class func requestToOtherUser(id: Int,showHud: Bool = true ,completionHandler:@escaping ((UserFullDetailsResponseModel?) -> Void)) {
        APIManager(urlString: (GraduateUrl.otherUserDetailsURL + "\(id)"),method: .get).handleResponse(showProgressHud: showHud,completionHandler: { (response: UserFullDetailsResponseModel) in
              print(response)
                  completionHandler(response)
              }, failureBlock: {
                completionHandler(nil)
              })
      }
    
    // MARK: - GET USER POSTS
    class func requestToOtherUserPosts(param: [String : Any],id: Int,showHud: Bool = true ,completionHandler:@escaping ((UniMyPostResponseModel?) -> Void)) {
        APIManager(urlString: GraduateUrl.offersURL,parameters: param,method: .get).handleResponse(showProgressHud: showHud,completionHandler: { (response: UniMyPostResponseModel) in
              print(response)
                  completionHandler(response)
              }, failureBlock: {
                completionHandler(nil)
              })
      }
    // MARK: - GET USER FRIENDS
       class func requestToOtherUserFriends(id: Int,showHud: Bool = true ,completionHandler:@escaping ((FriendsResponseModel?) -> Void)) {
           APIManager(urlString: (GraduateUrl.baseURL + "user/" + "\(id)" + "/get/friends"),method: .get).handleResponse(showProgressHud: showHud,completionHandler: { (response: FriendsResponseModel) in
                 print(response)
                     completionHandler(response)
                 }, failureBlock: {
                   completionHandler(nil)
                 })
         }
    
    // MARK: - UNFRIEND API
          class func requestToUnfriend(id: Int,showHud: Bool = true ,completionHandler:@escaping ((FriendsResponseModel?) -> Void)) {
              APIManager(urlString: (GraduateUrl.baseURL + "user/" + "\(id)" + "/get/friends"),method: .get).handleResponse(showProgressHud: showHud,completionHandler: { (response: FriendsResponseModel) in
                    print(response)
                        completionHandler(response)
                    }, failureBlock: {
                      completionHandler(nil)
                    })
            }
}
