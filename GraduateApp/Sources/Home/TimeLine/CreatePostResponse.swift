//
//  CreatePostResponse.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 9/25/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import Foundation
import ObjectMapper
class CreateSpotResponse: DefaultResponse {
    var data: [UniPortFolioData]?
     var errors: CreateErrors?
override func mapping(map: Map) {
  super.mapping(map: map)
data <- map["data"]
    errors <- map["errors"]
}
// MARK: - REQUEST TO CREATE PORTFOLIO
class func requestToCreatePortFolio(url: String,showHud: Bool = true ,completionHandler:@escaping ((UniMyPostResponseModel?) -> Void)) {
  APIManager(urlString: url,method: .delete ).handleResponse(showProgressHud: showHud,completionHandler: { (response: UniMyPostResponseModel) in
        print(response)
            completionHandler(response)
        }, failureBlock: {
          completionHandler(nil)
        })
}
//MARK:- REQUEST TO CREATE OFFER FROM OUTSIDE
    class func requestToCreateOffer(param: [String: Any],url: String,images: [UIImage],arrayObjects: [[String : Any]],showHud: Bool = true ,completionHandler:@escaping ((CreateSpotResponse?) -> Void)) {
      
     
//       ImageUploader.uploadImage(url: url,imageName: "images[]", parameters: param, imagesDataSource:images, completionSuccess: { (response: CreateSpotResponse) in
//           print(response)
//        completionHandler(response)
//       }) {
//           completionHandler(nil)
//       }
//        ImageUploader.uploadImage(url: url, videos: [], imageName: "images[]", parameters: param, imagesDataSource: images, completionSuccess: { (response: CreateSpotResponse) in
//            completionHandler(response)
//        }) {
//
//        }
        ImageUploaderWithObjectArrayId.uploadImage(url: GraduateUrl.baseURL + "graduate_university/offers", videos: [], imageName: "images[]", parameters: param, arrayObjects: arrayObjects, imagesDataSource: images) { (response: CreateSpotResponse) in
            completionHandler(response)
        } completionError: {
            completionHandler(nil)
        }
//        ImageUploaderWithObjectArrayId.uploadImage(url: url, videos: [], imageName: "images[]", parameters: param, arrayObjects: arrayObjects, imagesDataSource: images) { (response: CreateSpotResponse) in
//            completionHandler(response)
//        } completionError: {
//            completionHandler(nil)
//        }
    
       


    }
    //MARK:- REQUEST TO CREATE EVENT
    class func requestToCreateEvent(param: [String: Any],url: String,images: [UIImage],imageName: String,showHud: Bool = true ,completionHandler:@escaping ((CreateSpotResponse?) -> Void)) {
        ImageUploader.uploadImage(url: url, videos: [], imageName: imageName, parameters: param, imagesDataSource: images, completionSuccess: { (response: CreateSpotResponse) in
                completionHandler(response)
            }) {
                
            }
        }
    //MARK:- CREATE POST
    class func requestToCreatePost(param: [String: Any],url: String,imageName: String,videos: [URL],multipleImages: [UIImage],showHud: Bool = true ,completionHandler:@escaping ((CreateSpotResponse?) -> Void)) {
        ImageUploader.uploadImage(url: url, videos: videos, imageName: imageName, parameters: param, imagesDataSource: multipleImages, completionSuccess: { (response: CreateSpotResponse) in
                   completionHandler(response)
               }) {
                   
               }
           }
    //MARK:- CREATE EVENT FORM OUTSIDE
    class func requestToCreateEventFromOutside(param: [String: Any],url: String,imageName: String,videos: [URL],multipleImages: [UIImage],arrayObjects: [[String : Any]],showHud: Bool = true ,completionHandler:@escaping ((CreateSpotResponse?) -> Void)) {
        ImageUploaderWithObjectArrayId.uploadImage(url: url, videos: videos, imageName: imageName, parameters: param, arrayObjects: arrayObjects, imagesDataSource: multipleImages, completionSuccess: { (response: CreateSpotResponse) in
            completionHandler(response)
        }) {
            completionHandler(nil)
        }
             }
    //MARK:- CREATE POST FORM OUTSIDE
    class func requestToCreatePostFromOutside(param: [String: Any],url: String,imageName: String,videos: [URL],multipleImages: [UIImage],arrayObjects: [[String : Any]],showHud: Bool = true ,completionHandler:@escaping ((CreateSpotResponse?) -> Void)) {
        ImageUploaderWithObjectArrayId.uploadImage(url: url, videos: videos, imageName: imageName, parameters: param, arrayObjects: arrayObjects, imagesDataSource: multipleImages, completionSuccess: { (response: CreateSpotResponse) in
            completionHandler(response)
        }) {
            completionHandler(nil)
        }
             }
    
    //MARK:- UPDATE POST
    class func requestUpdate(param: [String: Any],url: String,imageName: String,videos: [URL],multipleImages: [UIImage],showHud: Bool = true ,completionHandler:@escaping ((CreateSpotResponse?) -> Void)) {
        ImageUploaderWithObjectArrayId.uploadImage(url: url, videos: videos, imageName: imageName, parameters: param, arrayObjects: [], imagesDataSource: multipleImages) { (response: CreateSpotResponse) in
            print(response)
            completionHandler(response)
        } completionError: {
            completionHandler(nil)
        }

           }
}
