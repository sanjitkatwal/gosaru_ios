//
//  OtherUserProfileTableViewCell.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 10/15/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import UIKit
protocol sendingActionFromOtherProfileTVC: class {
    func sendingRatingEventTVC(id: Int, starValue: String,selectedindexToReload: Int, selectionType: GraduateDetails)
       func moreOPtionFromEventTVC(sender: UIButton,id: Int,index: IndexPath,cell: EventTableViewCell, isOwner: Bool)
    func eventRatingDetails(ratingId: Int, starredVlaue: String, starCount: Int)
       func eventFeedbackAction(commentId: Int, index: IndexPath)
    func pushingToProfileVc(id: Int)
}
class OtherUserProfileTableViewCell: UITableViewCell {
    
    @IBOutlet var countryLabel: UILabel!
    @IBOutlet var martialStatusLabel: UILabel!
    @IBOutlet var addressLabel: UILabel!
    @IBOutlet var mailLabel: UILabel!
    @IBOutlet var professionLabel: UILabel!
    @IBOutlet var dobLabel: UILabel!
    @IBOutlet var descTxtView: UITextView!
    @IBOutlet var genderTxtField: UILabel!
    @IBOutlet var phoneLabel: UILabel!
    weak var delegate: sendingActionFromOtherProfileTVC?
    var starredVlaue: Bool?
    var starCount: Int?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
}
