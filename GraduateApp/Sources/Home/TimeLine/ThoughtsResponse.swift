//
//  ThoughtsResponse.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 10/1/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import Foundation
import ObjectMapper
class ThoughtsResponse: DefaultResponse {
    //var data: LoginResponseModel?
    var thoughtsData: Comments?
override func mapping(map: Map) {
  super.mapping(map: map)
thoughtsData <- map["data"]
}
// MARK: - COMMENTS LIST REQUEST
    class func requestToThoughtsList(id: String,showHud: Bool = true ,completionHandler:@escaping ((ThoughtsResponseModel?) -> Void)) {
        APIManager(urlString: (GraduateUrl.getCommentsURL + id), method: .get ).handleResponse(showProgressHud: showHud,completionHandler: { (response: ThoughtsResponseModel) in
              print(response)
                  completionHandler(response)
              }, failureBlock: {
                completionHandler(nil)
              })
      }
    // MARK: - COMMENT
    class func requestToComment(id: String,param: [String : Any],showHud: Bool = false ,completionHandler:@escaping ((ThoughtsResponse?) -> Void)) {
        APIManager(urlString: (GraduateUrl.CommentsURL + id), parameters: param,method: .post ).handleResponse(showProgressHud: showHud,completionHandler: { (response: ThoughtsResponse) in
              print(response)
                  completionHandler(response)
              }, failureBlock: {
                completionHandler(nil)
              })
      }
    
    //MARK:- REQUEST FOR RATING COMMENT
    class func requestToRateComment(id: Int,ratingNum: Int,showHud: Bool = false ,completionHandler:@escaping ((ThoughtsRatingResponse?) -> Void)) {
        let param: [String : Any] = ["star_value" : "\(ratingNum)"]
        APIManager(urlString: (GraduateUrl.rateCommentURL + "\(id)"), parameters: param,method: .post).handleResponse(showProgressHud: showHud,completionHandler: { (response: ThoughtsRatingResponse) in
              print(response)
                  completionHandler(response)
              }, failureBlock: {
                completionHandler(nil)
              })
      }
    //MARK:- REQUEST FOR UNRATING COMMENT
    class func requestToUnRateComment(id: Int,ratingNum: Int,showHud: Bool = false ,completionHandler:@escaping ((ThoughtsRatingResponse?) -> Void)) {
        let param: [String : Any] = ["star_value" : "\(ratingNum)"]
        APIManager(urlString: (GraduateUrl.UnrateCommentURL + "\(id)"), parameters: param,method: .post).handleResponse(showProgressHud: showHud,completionHandler: { (response: ThoughtsRatingResponse) in
              print(response)
                  completionHandler(response)
              }, failureBlock: {
                completionHandler(nil)
              })
      }
    //MARK:- REQUEST FOR DELETING COMMENT
    class func requestToDeleteComment(id: Int,showHud: Bool = false ,completionHandler:@escaping ((ThoughtsRatingResponse?) -> Void)) {
     
        APIManager(urlString: (GraduateUrl.deleteCommentURL + "\(id)"),method: .delete).handleResponse(showProgressHud: showHud,completionHandler: { (response: ThoughtsRatingResponse) in
              print(response)
                  completionHandler(response)
              }, failureBlock: {
                completionHandler(nil)
              })
      }
    
    //MARK:- REQUEST FOR EDITING COMMENT
    class func requestToEditComment(id: Int,params: [String : Any],showHud: Bool = false ,completionHandler:@escaping ((ThoughtsRatingResponse?) -> Void)) {
     
        APIManager(urlString: (GraduateUrl.CommentsURL + "\(id)"),parameters: params,method: .put).handleResponse(showProgressHud: showHud,completionHandler: { (response: ThoughtsRatingResponse) in
              print(response)
                  completionHandler(response)
              }, failureBlock: {
                completionHandler(nil)
              })
      }
}

 class ThoughtsResponseModel: DefaultResponse {
   

// MARK: Declaration for string constants to be used to decode and also serialize.
internal let kThoughtsResponseModelDataKey: String = "data"


// MARK: Properties
public var data: [Comments]?



// MARK: ObjectMapper Initalizers
/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/


/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/
    public override func mapping(map: Map) {
    data <- map[kThoughtsResponseModelDataKey]

}
}
 class ThoughtsData: DefaultResponse {
   
    

// MARK: Declaration for string constants to be used to decode and also serialize.
internal let kDataHasStarredKey: String = "has_starred"
internal let kDataCreatedAtKey: String = "created_at"
internal let kDataStarredValueKey: String = "starred_value"
internal let kDataIsOwnCommentKey: String = "is_own_comment"
internal let kDataInternalIdentifierKey: String = "id"
internal let kDataUpdatedAtKey: String = "updated_at"
internal let kDataBodyKey: String = "body"
internal let kDataUserIdKey: String = "user_id"
internal let kDataUserKey: String = "user"
internal let kDataStarsCountKey: String = "stars_count"
internal let kDataStarsKey: String = "stars"


// MARK: Properties
public var hasStarred: Bool = false
public var createdAt: String?
public var starredValue: String?
public var isOwnComment: Bool = false
public var internalIdentifier: Int?
public var updatedAt: String?
public var body: String?
public var userId: Int?
public var user: TimeLineUser?
public var starsCount: Int?
public var stars: [TimeLineStars]?



// MARK: ObjectMapper Initalizers
/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/


/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/
    public override func mapping(map: Map) {
    hasStarred <- map[kDataHasStarredKey]
    createdAt <- map[kDataCreatedAtKey]
    starredValue <- map[kDataStarredValueKey]
    isOwnComment <- map[kDataIsOwnCommentKey]
    internalIdentifier <- map[kDataInternalIdentifierKey]
    updatedAt <- map[kDataUpdatedAtKey]
    body <- map[kDataBodyKey]
    userId <- map[kDataUserIdKey]
    user <- map[kDataUserKey]
    starsCount <- map[kDataStarsCountKey]
    stars <- map[kDataStarsKey]

}
}
 class ThoughtsRatingResponse: DefaultResponse {

    // MARK: Declaration for string constants to be used to decode and also serialize.
    internal let kThoughtsRatingResponseCreatedAtKey: String = "created_at"
    internal let kThoughtsRatingResponseDataKey: String = "data"
    internal let kThoughtsRatingResponseUpdatedAtKey: String = "updated_at"
    internal let kThoughtsRatingResponseInternalIdentifierKey: String = "id"
    internal let kThoughtsRatingResponseInteractableIdKey: String = "interactable_id"
    internal let kThoughtsRatingResponseInteractionNameKey: String = "interaction_name"
    internal let kThoughtsRatingResponseUserIdKey: String = "user_id"
    internal let kThoughtsRatingResponseInteractableTypeKey: String = "interactable_type"


    // MARK: Properties
    public var createdAt: String?
    public var data: String?
    public var updatedAt: String?
    public var internalIdentifier: Int?
    public var interactableId: Int?
    public var interactionName: String?
    public var userId: Int?
    public var interactableType: String?



    // MARK: ObjectMapper Initalizers
    /**
    Map a JSON object to this class using ObjectMapper
    - parameter map: A mapping from ObjectMapper
    */
   
    /**
    Map a JSON object to this class using ObjectMapper
    - parameter map: A mapping from ObjectMapper
    */
    public override func mapping(map: Map) {
        createdAt <- map[kThoughtsRatingResponseCreatedAtKey]
        data <- map[kThoughtsRatingResponseDataKey]
        updatedAt <- map[kThoughtsRatingResponseUpdatedAtKey]
        internalIdentifier <- map[kThoughtsRatingResponseInternalIdentifierKey]
        interactableId <- map[kThoughtsRatingResponseInteractableIdKey]
        interactionName <- map[kThoughtsRatingResponseInteractionNameKey]
        userId <- map[kThoughtsRatingResponseUserIdKey]
        interactableType <- map[kThoughtsRatingResponseInteractableTypeKey]

    }
}

