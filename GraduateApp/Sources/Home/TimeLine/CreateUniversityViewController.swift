//
//  CreateUniversityViewController.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 7/1/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import UIKit
import iOSDropDown
import GooglePlaces
import Alamofire
import ObjectMapper

class CreateUniversityViewController: UIViewController, BottomPopupDelegate {

   
    
   
    @IBOutlet var createBtn: AnimatedButton!
    @IBOutlet weak var universityNameTxtField: UITextField!
    @IBOutlet weak var descTxtView: UITextView!
    @IBOutlet weak var locationTxtField: UITextField!
    
    @IBOutlet weak var categoryTxtField: UITextField!
    @IBOutlet weak var profileImageCoverBtn: UIButton!
    @IBOutlet weak var coverImageCloseBtn: UIButton!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var coverImage: UIImageView!
    var createdData: CreateUniversityResponseModel?
     var imagePickerController = CustomImagePicker()
    var imageDataSource: Imagetype? = Imagetype()
    var coverImageSelection: Bool?
    var image: UIImage?
     var callbackClosure: (() -> Void)?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Create University"
       
        
        
        
       
      
        categoryTxtField.delegate = self
        //categoryTxtField.enableMode = .disabled
        locationTxtField.delegate = self
        descTxtView.delegate = self
        profileImage.layer.cornerRadius = 16
        coverImage.layer.cornerRadius = 16
        imagePickerController.delegate = self
        // Do any additional setup after loading the view.
        self.hideKeyboardWhenTappedAround()
        initialImageDisappearnce()
       
    }
    override func viewWillAppear(_ animated: Bool) {
       
//        categoryTxtField.inputView = UIView.init(frame: CGRect.zero)
//        categoryTxtField.inputAccessoryView = UIView.init(frame: CGRect.zero)
        HelperFunctions.addingImageInRightTxtField(textField: categoryTxtField, imageName: "iconArrowDown")
        navigationController?.isNavigationBarHidden = false
        currentTabBar?.setBar(hidden: true, animated: true)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
         callbackClosure?()
    }
    func initialImageDisappearnce() {
        profileImage.isHidden = true
        profileImageCoverBtn.isHidden = true
        coverImage.isHidden = true
        coverImageCloseBtn.isHidden = true
    }
    
    @IBAction func coverBtnAction(_ sender: Any) {
        coverImage.image = nil
        coverImage.isHidden = true
        coverImageCloseBtn.isHidden = true
    }
    
    @IBAction func profileBtnAction(_ sender: Any) {
        profileImage.image = nil
        profileImage.isHidden = true
        profileImageCoverBtn.isHidden = true
    }
    @IBAction func coverImageChangeAction(_ sender: Any) {
        bottomSelection(type: "ProfileMoreBtnReloaded", height: 100)
        coverImageSelection = true
         self.imagePickerController.allowsEditing = true
        //openActionSheet()
    }
    @IBAction func profileImageChangeAction(_ sender: Any) {
        self.imagePickerController.allowsEditing = false
         bottomSelection(type: "ProfileMoreBtnReloaded", height: 100)
    }
    func bottomSelection(type: String, height: Int) {
        let popupVC = UIStoryboard.timeLineStoryboard.instantiateSelectionBottomPopUpVC()
               popupVC.delegate = self
               popupVC.textFieldType = type
        popupVC.height = CGFloat(height)
               popupVC.topCornerRadius = 15
               popupVC.presentDuration = 0.25
               popupVC.dismissDuration = 0.25
               popupVC.shouldDismissInteractivelty = true
               //popupVC.popupDelegate = self
               present(popupVC, animated: true, completion: nil)
    }
    func ErrorMessagePopup(message: String, popUptype: ErrorType) {
        let vc = UIStoryboard.timeLineStoryboard.instantiateErrorCarMsgPopVC()
                       vc.popupType = popUptype
        vc.messageString = message
        vc.delegaet = self
                       let cardPopup = SBCardPopupViewController(contentViewController: vc)
                       cardPopup.show(onViewController: self)
    }
    //MARK: OPENING ACTIOIN SHEET FOR PROFILE AND COVER PIC
    func openActionSheet() {
        let optionMenu = UIAlertController(title: nil, message: "Choose Option", preferredStyle: .actionSheet)

               let picAction = UIAlertAction(title: "Take Picture", style: .default, handler:
               {
                   (alert: UIAlertAction!) -> Void in
                   
               })
               let imagesAction = UIAlertAction(title: "Select From Gallery", style: .default, handler:
                      {
                          (alert: UIAlertAction!) -> Void in
                     
                       
                      })
               let videoAction = UIAlertAction(title: "Cancel", style: .cancel, handler:
                             {
                                 (alert: UIAlertAction!) -> Void in
                                
                             })
               optionMenu.addAction(picAction)
                optionMenu.addAction(imagesAction)
                optionMenu.addAction(videoAction)
               self.present(optionMenu, animated: true, completion: nil)
    }
    func getImage(fromSourceType sourceType: UIImagePickerController.SourceType, mediaType: String) {

              //Check is source type available
        if UIImagePickerController.isSourceTypeAvailable(sourceType) {

                  
                  imagePickerController.sourceType = sourceType
               imagePickerController.mediaTypes = [mediaType]
           // imagePickerController.allowsEditing = true
               imagePickerController.delegate = self
            imagePickerController.modalPresentationStyle = .overCurrentContext
                  self.present(imagePickerController, animated: true, completion: nil)
        } else {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
                   alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                   self.present(alert, animated: true, completion: nil)
        }
          }
    @IBAction func createAction(_ sender: Any) {
        if universityNameTxtField.text == "" && locationTxtField.text == "" && categoryTxtField.text == "" && descTxtView.text == "About business"{
            ErrorMessagePopup(message: "", popUptype: .univeristyFieldAllEmpty)
        } else if categoryTxtField.text == "" {
             ErrorMessagePopup(message: "", popUptype: .universityCategoryEmplty)
        } else if universityNameTxtField.text == "" {
            ErrorMessagePopup(message: "", popUptype: .universityUserName)
        } else {
            createuniversity()
        }
    }
    
    //MARK:- CREATE UNIVERSITY API
    func createuniversity() {
        let coverimgData: Data?
        let profileimgData: Data?
        let param: [String : Any] = ["business_name" : universityNameTxtField.text ?? "", "address_string" : locationTxtField.text ?? "", "business_type" : categoryTxtField.text ?? "", "about_business" : descTxtView.text ?? ""]
        if coverImage.image != nil {
            coverimgData = coverImage.image!.jpegData(compressionQuality: 0.2)!
        } else {
            coverimgData = nil
        }
        if profileImage.image != nil {
            profileimgData = profileImage.image?.jpegData(compressionQuality: 0.2)!
        } else {
            profileimgData = nil
        }
            
            

        
            
            //Optional for extra parameter
            var header: [String : Any]
            if let token = UserDefaultsHandler.getUDValue(key: UDkey.token) as? String {
                header = ["Authorization": "Bearer \(token)", "Accept": "application/json"]
            } else {
                header = ["Accept": "application/json"]
                
            }
            
           
            Alamofire.upload(multipartFormData: { [weak self]multipartFormData in
                guard let strongSelf = self else { return }
               

                if coverimgData != nil {
                      multipartFormData.append(coverimgData!, withName: "cover_image", fileName: "file.jpg", mimeType: "image/jpg")
                }
                if profileimgData != nil {
                    multipartFormData.append(profileimgData! , withName: "profile_image",fileName: "file.jpg", mimeType: "image/jpg")
                                 
                }
               
                ProgressHud.showProgressHUD()
                for (key, value) in param {
                    multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
                } //Optional for extra parameters
            }, to: GraduateUrl.createUniURL, method: .post, headers: (header as! HTTPHeaders))
            { (result) in
                
                switch result {
                case .success(let upload, _, _):
                    
                    upload.uploadProgress(closure: { [weak self](progress) in
                        guard let strongSelf = self else {return}
                        print("Upload Progress: \(progress.fractionCompleted)")
                    })
                    upload.responseObject { [weak self](response: DataResponse<CreateUniversityResponseModel>) in
                        guard let strongSelf = self else { return }
                     
                        ProgressHud.hideProgressHUD()
                        
                        strongSelf.createdData = response.result.value
                        print(strongSelf.createdData)
                        print(strongSelf.createdData?.data?.internalIdentifier)
                         strongSelf.ErrorMessagePopup(message: "", popUptype: .universityRegistered)
                    }
//                    upload.responseJSON { [weak self] response in
//                        guard let strongSelf = self else { return }
//                       // print(response.result.value)
//                        strongSelf.createdData = Mapper<[CreateUniversityResponseModel]>().mapArray(JSONObject: response.result.value)
//
//                        print(response)
//                        if let allData = response.result.value as? NSDictionary {
//
//                            strongSelf.createdData?[0].data?.businessName = allData["business_name"] as? String
//                            strongSelf.createdData?.data?.businessType = allData["business_type"] as? String
//                            print(strongSelf.createdData?.data?.businessType)
//
//                        }
//
//
//
//                         let datas = response.result.value as! CreateUniversityResponseModel
//                        print(strongSelf.createdData)
//                        ProgressHud.hideProgressHUD()
//                        strongSelf.ErrorMessagePopup(message: "", popUptype: .universityRegistered)
//                       // var data: UserFullDetailsResponseModel
//
//                    }
                   
                case .failure(let encodingError):
                  
                    print(encodingError)
                    
            }
        }
    }
}

extension CreateUniversityViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
      func imagePickerController(_ picker: UIImagePickerController,
    didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
    self.dismiss(animated: true) { [weak self] in
        print(self!.coverImageSelection)
        if self?.coverImageSelection == true {
            if let editedcoverImage = info[.originalImage] as? UIImage {
//                     self?.coverImage.isHidden = false
//                     self?.coverImageCloseBtn.isHidden = false
//                     self?.coverImage.image = coverImage
                print(editedcoverImage.size)
                self?.image = editedcoverImage
                 }
            if let originalcoverImage = info[.editedImage] as? UIImage {
            //                     self?.coverImage.isHidden = false
            //                     self?.coverImageCloseBtn.isHidden = false
            //                     self?.coverImage.image = coverImage
                            self?.image = originalcoverImage
                             }
            self?.coverImage.image =  self?.image
            self?.coverImage.isHidden = false
                                 self?.coverImageCloseBtn.isHidden = false
            self?.coverImageSelection = false
           
        } else {
            if let profileImage = info[.originalImage] as? UIImage  {
            
                                self?.profileImage.isHidden = false
                                self?.profileImageCoverBtn.isHidden = false
                                self?.profileImage.image = profileImage
            
                               }
             self?.coverImage.isHidden = false
                        self?.profileImageCoverBtn.isHidden = false
                       // self?.coverImage.image = coverImage
        }
}
}
}
extension CreateUniversityViewController: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == locationTxtField {
            locationTxtField.resignFirstResponder()
            let acController = GMSAutocompleteViewController()
            acController.delegate = self
            present(acController, animated: true, completion: nil)
        } else if textField == categoryTxtField {
            categoryTxtField.enableMode = .disabled
            categoryTxtField.resignFirstResponder()
            let popupVC = UIStoryboard.timeLineStoryboard.instantiateSelectionBottomPopUpVC()
                   popupVC.delegate = self
                   popupVC.textFieldType = "Category"
                          popupVC.height = 300
                          popupVC.topCornerRadius = 15
                   popupVC.presentDuration = 0.25
                          popupVC.dismissDuration = 0.25
                          popupVC.shouldDismissInteractivelty = true
                         // popupVC.popupDelegate = self
                          present(popupVC, animated: true, completion: nil)
            return false
        }
        return true
   }
   
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//        if textField == categoryTxtField {
//            return false
//
//    }
        return false
        }

}
extension CreateUniversityViewController: GMSAutocompleteViewControllerDelegate {
  func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
    // Get the place name from 'GMSAutocompleteViewController'
    // Then display the name in textField
    locationTxtField.text = place.name
// Dismiss the GMSAutocompleteViewController when something is selected
    dismiss(animated: true, completion: nil)
  }
func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
    // Handle the error
    print("Error: ", error.localizedDescription)
  }
func wasCancelled(_ viewController: GMSAutocompleteViewController) {
    // Dismiss when the user canceled the action
    dismiss(animated: true, completion: nil)
  }
}
extension CreateUniversityViewController: SendingMultipleSeclections {
    func postAsActionFromInside(value: Int?, bool: Bool, index: Int?, type: CreateSpotVCTypes, posterIds: [Int], postData: CreateEventModel) {
        
    }
    func gettingTheSelectedValueFromTheList(value: Int?, bool: Bool, index: Int?, type: CreateSpotVCTypes, posterIds: [Int]) {
        
    }
       
    
    func sendingUserId(id: Int, index: Int) {
        
    }
    func sendingMediaType(value: Int) {
        
    }
    func gettingFullScreenCoverPic() {
        
    }
    func gettingSelectedVaueFromProfileMoreBtn(value: Int, bool: Bool, index: Int) {
        switch value {
        case 0:
           imagePickerController.delegate = self
                              imagePickerController.sourceType = UIImagePickerController.SourceType.camera
                              imagePickerController.modalPresentationStyle = .overCurrentContext
                              self.present(imagePickerController, animated: true, completion: nil)
            case 1:
                 self.getImage(fromSourceType: .photoLibrary, mediaType: "public.image")
                       
        default:
            break
        }
    }
    
    func sendingMultipleVlaues(values: [String]) {
    }
    
    func sendingDiscountType(value: String) {
    }
    
    func sendingEvnetType(value: String, type: String) {
        
    }
    
    func sendingUniCategory(value: String) {
        categoryTxtField.text = value
    }
    
    
    
}
extension CreateUniversityViewController: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView == descTxtView {
                   if textView.text == "About business"{
                         descTxtView.text = ""
                       descTxtView.textColor = UIColor.init(hexString: Appcolors.text)
                
                   } else {
                 
                   descTxtView.textColor = UIColor.init(hexString: Appcolors.text)
                 
               }
               }
    }
}
//MARK:- ACTION FROM ERROR POP UP
extension CreateUniversityViewController: SendingActionFromErrorChat {
    func adctionwithType(type: ErrorType) {
        
    }
    
    func sendingAction() {
        let vc = UIStoryboard.graduateStoryboard.instantiateGraduateDetailsViewController()
        vc.businessId = createdData?.data?.internalIdentifier
        navigationController?.pushViewController(vc, animated: true)
    }
    func sendingCancelAction() {
        navigationController?.popViewController(animated: true)
    }
    
}
struct Imagetype {
    var coverImage: UIImage? = UIImage()
    var profileImage: UIImage? = UIImage()
}
