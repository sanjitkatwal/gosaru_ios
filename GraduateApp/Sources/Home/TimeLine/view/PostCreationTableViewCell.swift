//
//  PostCreationTableViewCell.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 7/3/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import UIKit
import DTPhotoViewerController
import SDWebImage
protocol SendingRatingFromPCTVC: class {
    func PCTVCRating(id: Int, starValue: String,selectedindexToReload: Int, selectionType: GraduateDetails)
    func moreOptionFromPostTVC(sender: UIButton,id: Int,index: IndexPath,cell: PostCreationTableViewCell, isOwner: Bool,postType: GraduateDetails)
    func feedbackActionPCTVC(commentId: Int,outsideComment: Bool,index: IndexPath)
    func pushingTOProfileVC(id: Int,userType: String, isOwner: Bool,postAsType: String)
    func fullScreenImage(vc: UIViewController)
    func thoughtsAction()
    func ratingDetailFromPCTVC(ratingId: Int, starredValue: String, starCount: Int)
    func outsideCommentPostTVC(comment: Comments?, index: IndexPath)
}
class PostCreationTableViewCell: UITableViewCell {
    @IBOutlet weak var ratingNumLabel: UILabel!
    @IBOutlet var timeStampLabel: UILabel!
    @IBOutlet weak var thoughtsStackView: UIStackView!
    @IBOutlet weak var thoughtsNumLabel: UILabel!
    @IBOutlet weak var ratingStackView: UIStackView!
    @IBOutlet weak var heihgt: UIView!
    @IBOutlet weak var heightCon: UIView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var captionLabel: UILabel!
    @IBOutlet weak var moreBtn: UIButton!
    @IBOutlet weak var imageContainerViw: UIView!
    @IBOutlet weak var proImage: UIImageView!
    @IBOutlet weak var messageBtn: AnimatedButton!
    @IBOutlet weak var ratingBtn: AnimatedButton!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var footerTxtView: UITextView!
    weak var delegate: SendingRatingFromPCTVC?
    var universityId: Int?
    var starredVlaue: String?
    var index: IndexPath!
    var isOwnBusiness: Bool?
    var starCount: Int?
    var selectionType: GraduateDetails?
    var postData: uniMyPostData?
    var postType: GraduateDetails?
    var postId: Int?
    var name: String? {
        didSet {
            attributed()
        }
    }
    let cornerRadius : CGFloat = 4
    var useCustomOverlay = false
    var collectionViewDataSource: Int? {
        didSet {
           DispatchQueue.main.async {
                self.collectionView.reloadData()
              }
        }
    }
    var fullScreen: Bool?
     var images: [UIImage] = []
//    var imageDataSource: [UIImage] = [UIImage(named: "greenary")!,UIImage(named: "academic")!,UIImage(named: "iconSearch")!]
    var imageDataSource: [TimeLineGalleries]? {
        didSet {
            collectionView.reloadData()
        }
    }
//     var imageDataSource: [UIImage] = [UIImage(named: "star2")!,UIImage(named: "star2")!,UIImage(named: "star2")!]
    fileprivate var selectedImageIndex: Int = 0
    @IBOutlet weak var headerTxtView: UITextView!
    private let spacing:CGFloat = 16.0
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
       // textView.frame.size.height = 15
        self.backgroundColor = UIColor.clear; self.collectionView.register(UINib(nibName:"PostCreationCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "PostCreationCollectionViewCell")
       // collectionView.layer.cornerRadius = 25
        collectionView.reloadData()
        collectionView.dataSource = self
        collectionView.delegate = self
        footerTxtView.layer.cornerRadius = 4
          HelperFunctions.addingImageInbtn(ratingBtn: ratingBtn, messageBtn: messageBtn)
       // HelperFunctions.addingShadowInImage(vw: proImage)
        
        HelperFunctions.addingShadowInImage(avatarImageView: proImage, cornerRadius: 6, containerView: imageContainerViw)
        footerTxtView.textColor = .lightGray
        footerTxtView.text = "Add your thought"
        footerTxtView.delegate = self
        moreBtn.setImage(UIImage(named: "more-3"), for: .normal)
        moreBtn.imageView?.tintColor = UIColor.init(hexString: Appcolors.buttons)
               moreBtn.imageView?.contentMode = .center
        
  
        profilePicDidTap()
        thoughtsDetailsDidTap()
        ratingDetailsDidTap()
       
    }
   
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
   func profilePicDidTap() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(profileImagePushAction))
               proImage.addGestureRecognizer(tapGesture)
    }
    func thoughtsDetailsDidTap() {
           let tapGesture = UITapGestureRecognizer(target: self, action: #selector(thoughtsPushAction))
                  thoughtsStackView.addGestureRecognizer(tapGesture)
       }
    @objc func thoughtsPushAction() {
        delegate?.feedbackActionPCTVC(commentId: universityId ?? 0, outsideComment: false, index: index)
    }
    func ratingDetailsDidTap() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(ratingDetailsPushAction))
               ratingStackView.addGestureRecognizer(tapGesture)
    }
    @objc func ratingDetailsPushAction() {
        delegate?.ratingDetailFromPCTVC(ratingId: postData?.internalIdentifier ?? 0, starredValue: starredVlaue ?? "", starCount: starCount ?? 0)
    }
    func attributed() {
        print(name)
        nameLabel.text = ((name ?? "") + " created a post")
           let text = (nameLabel.text)!
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.tapLabel(gesture:)))
        nameLabel.addGestureRecognizer(tap)
        nameLabel.isUserInteractionEnabled = true
           let underlineAttriString = NSMutableAttributedString(string: text)
        let range1 = (text as NSString).range(of: name ?? "")
        underlineAttriString.addAttribute(NSAttributedString.Key.font, value: UIFont.systemFont(ofSize: 17, weight: .medium), range: range1)
           nameLabel.attributedText = underlineAttriString
       }
    @objc func profileImagePushAction() {
        delegate?.pushingTOProfileVC(id: universityId ?? 0, userType: postData?.postingAsType ?? "", isOwner: postData?.isOwner ?? false, postAsType: postData?.postingAsType ?? "")
    }
    @IBAction func tapLabel(gesture: UITapGestureRecognizer) {
    
        let text = (nameLabel.text)!
        let termsRange = (text as NSString).range(of: name ?? "")
       
        if gesture.didTapAttributedTextInLabel(label: nameLabel, inRange: termsRange) {
            delegate?.pushingTOProfileVC(id: universityId ?? 0, userType: postData?.postingAsType ?? "", isOwner: postData?.isOwner ?? false, postAsType: postData?.postingAsType ?? "")
        }
    }
  
    @IBAction func moreMenuBtn(_ sender: UIButton) {
//        let vc = UIViewController()
//        vc.modalPresentationStyle = .fullScreen //or .overFullScreen for transparency
//        self.present(vc, animated: true, completion: nil)
        delegate?.moreOptionFromPostTVC(sender: sender, id: universityId ?? 0, index: index, cell: PostCreationTableViewCell(), isOwner: isOwnBusiness ?? false, postType: postType!)
    }
    @IBAction func ratingBtn(_ sender: Any) {
        delegate?.PCTVCRating(id: postId ?? 0, starValue: starredVlaue ?? "", selectedindexToReload: index.row, selectionType: selectionType!)
    }
    @IBAction func messagebtn(_ sender: Any) {
        delegate?.feedbackActionPCTVC(commentId: postId ?? 0, outsideComment: false, index: index)
    }
    @IBAction func thoughtsAction(_ sender: Any) {
        delegate?.thoughtsAction()
    }
    @IBAction func ratingDetailsAction(_ sender: Any) {
        
//        delegate?.ratingDetail(ratingId: universityId ?? 0, starredValue: starredVlaue ?? "", starCount: starCount ?? 0)
    }
    @IBAction func sendAction(_ sender: Any) {
        if footerTxtView.text == "Add your thoughts" {
                   
               } else {
                   
                   let param:[String : Any] = ["comment_body" : footerTxtView.text ?? ""]
        footerTxtView.text = "Add your thoughts"
                                  footerTxtView.textColor = .lightGray
                                  footerTxtView.resignFirstResponder()
                ThoughtsResponse.requestToComment(id: "\(postData?.internalIdentifier ?? 0)", param: param) { [weak self](response) in
                       guard let strongSelf = self else { return }
                    strongSelf.delegate?.outsideCommentPostTVC(comment: response?.thoughtsData, index: strongSelf.index)
                      
                     
                   }
               }
    }
    @IBAction func profileImageAction(_ sender: Any) {
        delegate?.pushingTOProfileVC(id: universityId ?? 0, userType: postData?.postingAsType ?? "", isOwner: postData?.isOwner ?? false, postAsType: postData?.postingAsType ?? "")
    }
}
extension PostCreationTableViewCell: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return imageDataSource?.count ?? 0
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PostCreationCollectionViewCell", for: indexPath) as! PostCreationCollectionViewCell
      
        cell.mainImage.setURLImage(imageURL: (GraduateUrl.imageUrl + (imageDataSource?[indexPath.row].filepath ?? "")))
         
       
        return cell
    }
    
    
}
extension PostCreationTableViewCell: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
       
//         let bgView = UIView(frame: UIScreen.main.bounds)
//        bgView.addToWindow()
       
       selectedImageIndex = indexPath.row

              if let cell = collectionView.cellForItem(at: indexPath) as? PostCreationCollectionViewCell {
                  let viewController = SimplePhotoViewerController(referencedView: cell.mainImage, image: cell.mainImage.image)
                viewController.modalPresentationStyle = .overCurrentContext
                  viewController.dataSource = self
                  viewController.delegate = self

                delegate?.fullScreenImage(vc: viewController)
              }
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        let numberOfItemsPerRow:CGFloat = 4
//        let spacingBetweenCells:CGFloat = 16
//        //let spacing: CGFloat = 3
//
//        let totalSpacing = (2 * self.spacing) + ((numberOfItemsPerRow - 1) * spacingBetweenCells) //Amount of total spacing in a row
//
//        if let collection = self.collectionView{
//            let width = (collection.bounds.width - totalSpacing)/numberOfItemsPerRow
//            return CGSize(width: width, height: width)
//        }else{
//            return CGSize(width: 0, height: 0)
//
//        if fullScreen == true {
//
//            let screenSize = UIScreen.main.bounds
//            fullScreen = false
//            return CGSize(width: screenSize.width, height: screenSize.height)
//        } else {
//            fullScreen = true
       return CGSize(width: (collectionView.bounds.width - 24), height: (collectionView.bounds.height - 24))
        }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
           return UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
       }
}
extension PostCreationTableViewCell: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView == footerTxtView {
            footerTxtView.textColor = UIColor.init(hexString: Appcolors.text)
        footerTxtView.text = ""
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if footerTxtView.text.isEmpty {
            textView.text = "Add your thoughts"
            textView.textColor = UIColor.lightGray
        }
    }
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
           print("the label is tapped")
           return false
       }
    
}
extension NSMutableAttributedString {
    func setFontFace(font: UIFont, color: UIColor? = nil) {
        beginEditing()
        self.enumerateAttribute(
            .font,
            in: NSRange(location: 0, length: self.length)
        ) { (value, range, stop) in

            if let f = value as? UIFont,
              let newFontDescriptor = f.fontDescriptor
                .withFamily(font.familyName)
                .withSymbolicTraits(f.fontDescriptor.symbolicTraits) {

                let newFont = UIFont(
                    descriptor: newFontDescriptor,
                    size: font.pointSize
                )
                removeAttribute(.font, range: range)
                addAttribute(.font, value: newFont, range: range)
                if let color = color {
                    removeAttribute(
                        .foregroundColor,
                        range: range
                    )
                    addAttribute(
                        .foregroundColor,
                        value: color,
                        range: range
                    )
                }
            }
        }
        endEditing()
    }
}
extension UITapGestureRecognizer {

    func didTapAttributedTextInLabel(label: UILabel, inRange targetRange: NSRange) -> Bool {
        // Create instances of NSLayoutManager, NSTextContainer and NSTextStorage
        let layoutManager = NSLayoutManager()
        let textContainer = NSTextContainer(size: CGSize.zero)
        let textStorage = NSTextStorage(attributedString: label.attributedText!)

        // Configure layoutManager and textStorage
        layoutManager.addTextContainer(textContainer)
        textStorage.addLayoutManager(layoutManager)

        // Configure textContainer
        textContainer.lineFragmentPadding = 0.0
        textContainer.lineBreakMode = label.lineBreakMode
        textContainer.maximumNumberOfLines = label.numberOfLines
        let labelSize = label.bounds.size
        textContainer.size = labelSize

        // Find the tapped character location and compare it to the specified range
        let locationOfTouchInLabel = self.location(in: label)
        let textBoundingBox = layoutManager.usedRect(for: textContainer)
        let textContainerOffset = CGPoint(x: (labelSize.width - textBoundingBox.size.width) * 0.5 - textBoundingBox.origin.x,
                                          y: (labelSize.height - textBoundingBox.size.height) * 0.5 - textBoundingBox.origin.y);
        let locationOfTouchInTextContainer = CGPoint(x: locationOfTouchInLabel.x - textContainerOffset.x,
                                                     y: locationOfTouchInLabel.y - textContainerOffset.y);
        let indexOfCharacter = layoutManager.characterIndex(for: locationOfTouchInTextContainer, in: textContainer, fractionOfDistanceBetweenInsertionPoints: nil)

        return NSLocationInRange(indexOfCharacter, targetRange)
    }

}
extension PostCreationTableViewCell: DTPhotoViewerControllerDataSource {
    func photoViewerController(_ photoViewerController: DTPhotoViewerController, configureCell cell: DTPhotoCollectionViewCell, forPhotoAt index: Int) {
        // Set text for each item
        if let cell = cell as? CustomPhotoCollectionViewCell {
            cell.extraLabel.text = "Image no \(index + 1)"
        }
    }

    func photoViewerController(_ photoViewerController: DTPhotoViewerController, referencedViewForPhotoAt index: Int) -> UIView? {
        let indexPath = IndexPath(item: index, section: 0)
        if let cell = collectionView?.cellForItem(at: indexPath) as? PostCreationCollectionViewCell {
            cell.mainImage.layer.cornerRadius = 25
            return cell.mainImage
        }

        return nil
    }

    func numberOfItems(in photoViewerController: DTPhotoViewerController) -> Int {
        return imageDataSource?.count ?? 0
    }

    func photoViewerController(_ photoViewerController: DTPhotoViewerController, configurePhotoAt index: Int, withImageView imageView: UIImageView) {
        imageView.setURLImage(imageURL: (GraduateUrl.imageUrl + (imageDataSource?[index].filepath ?? "")))
    }
}

// MARK: DTPhotoViewerControllerDelegate
extension PostCreationTableViewCell: SimplePhotoViewerControllerDelegate {
    func photoViewerControllerDidEndPresentingAnimation(_ photoViewerController: DTPhotoViewerController) {
        photoViewerController.scrollToPhoto(at: selectedImageIndex, animated: false)
    }

    func photoViewerController(_ photoViewerController: DTPhotoViewerController, didScrollToPhotoAt index: Int) {
        selectedImageIndex = index
        if let collectionView = collectionView {
            let indexPath = IndexPath(item: selectedImageIndex, section: 0)

            // If cell for selected index path is not visible
            if !collectionView.indexPathsForVisibleItems.contains(indexPath) {
                // Scroll to make cell visible
                collectionView.scrollToItem(at: indexPath, at: UICollectionView.ScrollPosition.bottom, animated: false)
            }
        }
    }

    func simplePhotoViewerController(_ viewController: SimplePhotoViewerController, savePhotoAt index: Int) {
        imageDataSource?.forEach{
         
            if let data = UIImage(url: URL(string: GraduateUrl.imageUrl + ($0.filepath ?? ""))) {
                           images.append(data)
                       }
            
        }
//        for item in imageDataSource {
//
//            if let data = UIImage(url: URL(string: GraduateUrl.imageUrl + (item.filepath ?? ""))) {
//                images.append(data)
//            }
//
////            image.append(((UIImage(url: URL(string: GraduateUrl.imageUrl + (item.filepath ?? ""))))))
//        }
       
        UIImageWriteToSavedPhotosAlbum(images[index], nil, nil, nil)
    }
}
extension PostCreationTableViewCell {
    func postTVC(cell: PostCreationTableViewCell, indexPath: IndexPath, dataSource: [uniMyPostData]?) -> PostCreationTableViewCell {
        if dataSource?[indexPath.row].postingAsType == "business" {
            cell.name = dataSource?[indexPath.row].postingAs?.businessName
        } else if dataSource?[indexPath.row].postingAsType == "user" {
             cell.name = dataSource?[indexPath.row].postingAs?.name
        }
        cell.ratingNumLabel.text = "\(dataSource?[indexPath.row].starsCount ?? 0)"
        cell.thoughtsNumLabel.text = "\(dataSource?[indexPath.row].comments?.count ?? 0)"
        print(dataSource?[indexPath.row].postingAs?.businessName)
        cell.captionLabel.text = dataSource?[indexPath.row].message
        if dataSource?[indexPath.row].galleries?.count == 0 {
            cell.collectionView.isHidden = true
        } else {
            cell.collectionView.isHidden = false
             cell.imageDataSource = dataSource?[indexPath.row].galleries ?? []
        }
        if dataSource?[indexPath.row].hasStarred == true {
            cell.ratingBtn.setImage(UIImage(named: "star3"))
        } else {
             cell.ratingBtn.setImage(UIImage(named: "star"))
        }
        let newDate = HelperFunctions.timeInterval(timeAgo: dataSource?[indexPath.row].createdAt ?? "")
                         print(newDate)
              cell.timeStampLabel.text = newDate
        cell.index = indexPath
        cell.starredVlaue = dataSource?[indexPath.row].starredValue
        cell.isOwnBusiness = dataSource?[indexPath.row].isOwner
        cell.universityId = dataSource?[indexPath.row].postingAs?.internalIdentifier
        cell.postData = dataSource?[indexPath.row]
        cell.postId = dataSource?[indexPath.row].internalIdentifier
        cell.collectionView.reloadData()
        return cell
}
}
