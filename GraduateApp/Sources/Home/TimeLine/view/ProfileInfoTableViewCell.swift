//
//  ProfileInfoTableViewCell.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 7/20/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import UIKit

class ProfileInfoTableViewCell: UITableViewCell {

    @IBOutlet weak var countryTxtField: UITextField!
    @IBOutlet weak var statusTxtField: UITextField!
    @IBOutlet weak var addressTxtField: UITextField!
    @IBOutlet weak var dateTxtField: UITextField!
    @IBOutlet weak var professionTxtField: UITextField!
    @IBOutlet weak var maleTxtField: UITextField!
    @IBOutlet weak var codeTxtField: UITextField!
    @IBOutlet weak var mailTxtField: UITextField!
    @IBOutlet weak var nickNameTxtField: UITextField!
    @IBOutlet weak var nameTxtField: UITextField!
    @IBOutlet weak var cancelBtn: UIButton!
    @IBOutlet weak var editBtn: UIButton!
    @IBOutlet weak var countryContainerView: CustomView!
    @IBOutlet weak var statusContainerView: CustomView!
    @IBOutlet weak var addressContainerView: CustomView!
    @IBOutlet weak var dateContainerView: CustomView!
    @IBOutlet weak var professionContainerView: CustomView!
    @IBOutlet weak var codeContainerView: CustomView!
    @IBOutlet weak var genderContainerView: CustomView!
    @IBOutlet weak var mailContainerView: CustomView!
    @IBOutlet weak var descContainerView: CustomView!
    @IBOutlet weak var nickNameContainerView: CustomView!
    @IBOutlet weak var nameContainerView: CustomView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        cancelBtn.isHidden = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func editBtnAction(_ sender: Any) {
    }
    @IBAction func cancelBtnAction(_ sender: Any) {
    }
}
