//
//  RatingTableViewCell.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 7/14/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import UIKit

class RatingTableViewCell: UITableViewCell {

   
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var likeCountLabel: UILabel!
    var collectionDataSource: Int? {
        didSet {
            collectionView.reloadData()
        }
    }
    var selectedRow: Bool?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        print(collectionDataSource)
        collectionView.dataSource = self
        collectionView.delegate = self
       
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
       
        // Configure the view for the selected state
    }

}
extension RatingTableViewCell: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        print(collectionDataSource)
        if section == 0{
            if collectionDataSource == 1 {
                return 1
            } else {
        return collectionDataSource ?? 0
            }
//
//        } else {
      //  return ((collectionDataSource ?? 0) + 1)
       // }
        
       
    }
        return 0
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
      
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "RatingCollectionViewCell", for: indexPath) as! RatingCollectionViewCell
        if selectedRow == true {
            cell.mainImage.image =  UIImage(named: "star2")
            return cell
        } else {
            cell.mainImage.image =  UIImage(named: "star")
return cell
            
        }
//        if indexPath.row == 0{
//        if selectedRow == true {
//        cell.mainImage.image = UIImage(named: "star2")
//        return cell
//        } else {
//
//            return cell
//        }
//        } else {
//            if selectedRow == true {
//            cell.mainImage.image = UIImage(named: "star2")
//            return cell
//            } else {
//
//                return cell
//            }
//        }
    }
}
extension RatingTableViewCell: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
           return CGSize(width: 15, height: 15)
        
        }
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
            return 2
        }
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
            return 2
        }
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
               return UIEdgeInsets(top: 2, left: 2, bottom: 2, right: 2)
           }
}
