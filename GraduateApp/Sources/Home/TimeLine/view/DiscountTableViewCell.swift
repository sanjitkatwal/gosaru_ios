//
//  DiscountTableViewCell.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 7/6/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import UIKit
import DTPhotoViewerController
protocol SedingRatingsToTimeLineVC: class {
    func sendingRating(id: Int, starValue: String,selectedindexToReload: Int, selectionType: GraduateDetails)
    func moreOptionFromDiscountTVC(sender: UIButton, id: Int, isOwnBusiness: Bool,postType: GraduateDetails, index: IndexPath)
    func feedbackAction(commentId: Int,outsideComment: Bool,index: IndexPath)
    func fullscreen(vc: UIViewController)
    func pushingTOProfileVCFromDisc(id: Int,userType: String, isOwner: Bool,postAsType: String)
    func discratingDetails(ratingId: Int, starredValue: String, starCount: Int)
    func commentOutsideDiscountTVC(comment: Comments?, index: IndexPath)
}

class DiscountTableViewCell: UITableViewCell {
    @IBOutlet var discountLabel: UILabel!
    @IBOutlet weak var timeStampLabel: UILabel!
    @IBOutlet weak var tandCLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var expiryDate: UILabel!
    @IBOutlet weak var discountPercentage: UILabel!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var thoughtsStackView: UIStackView!
    @IBOutlet weak var ratingStackView: UIStackView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var thoughtsValue: UILabel!
    @IBOutlet weak var ratingValue: UILabel!
    @IBOutlet weak var moreBtn: UIButton!
    @IBOutlet weak var footerTxtView: UITextView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var messageBtn: AnimatedButton!
    @IBOutlet weak var ratingBtn: AnimatedButton!
    @IBOutlet weak var proImage: UIImageView!
    @IBOutlet weak var imageContainerVIew: UIView!
    @IBOutlet weak var captionLabel: UILabel!
    weak var delegate: SedingRatingsToTimeLineVC?
    var profileName = "Something University"
    var universityId: Int?
    var offerId: Int?
    var starredValue: String?
    var starCount: Int?
    var creation = "created a post"
    var isOwnBusiness: Bool?
    var selectedIndex: IndexPath!
    var index: Int?
    var selectionType: GraduateDetails?
    var postData: uniMyPostData?
    var postType: GraduateDetails?
    var name: String? {
        didSet {
            attributed()
        }
    }
    fileprivate var selectedImageIndex: Int = 0
    var collectionViewDataSource: Int? {
        didSet {
            DispatchQueue.main.async {
                self.collectionView.reloadData()
            }
            
            
            
        }
    }
    var imageDataSource: [TimeLineGalleries]?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        footerTxtView.layer.cornerRadius = 8
        captionLabel.lineBreakMode = .byWordWrapping
        
        captionLabel.text = "asdjfskdjfalksdjfla;ksjdfl;askdjfas;lkdjfasldkfjsl;dkfjasldkjfasdkfasdkfalskdjflsakdjflaskdjflaskdfjlaskdmflaskdmf"
        
        nameLabel.lineBreakMode = .byWordWrapping
        nameLabel.sizeToFit()
        thoughtsDetails()
        ratingDetials()
        
        
        
        
        
        HelperFunctions.addingImageInbtn(ratingBtn: ratingBtn, messageBtn: messageBtn)
        HelperFunctions.addingShadowInImage(avatarImageView: proImage, cornerRadius: 6, containerView: imageContainerVIew)
        self.collectionView.register(UINib(nibName:"PostCreationCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "PostCreationCollectionViewCell")
        contentView.frame = contentView.frame.inset(by: UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10))
        collectionView.dataSource = self
        collectionView.delegate = self
        footerTxtView.textColor = .lightGray
        footerTxtView.text = "Add your thoughts"
        footerTxtView.delegate = self
        moreBtn.setImage(UIImage(named: "more-3"), for: .normal)
        moreBtn.imageView?.contentMode = .center
        attributed()
        addingActionInProImage()
        addingTopAndBottomRedBorder()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    func addingTopAndBottomRedBorder() {
        topView.borders(for: [.top], width: 2, color: UIColor.init(hexString: Appcolors.buttons))
        bottomView.borders(for: [.bottom], width: 2, color: UIColor.init(hexString: Appcolors.buttons))
        
    }
    func attributed() {
        nameLabel.text = ((name ?? "") + " created a Deal")
        let text = (nameLabel.text)!
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.tapLabel(gesture:)))
        nameLabel.addGestureRecognizer(tap)
        nameLabel.isUserInteractionEnabled = true
        let underlineAttriString = NSMutableAttributedString(string: text)
        let range1 = (text as NSString).range(of: name ?? "")
        underlineAttriString.addAttribute(NSAttributedString.Key.font, value: UIFont.systemFont(ofSize: 17, weight: .medium), range: range1)
        nameLabel.attributedText = underlineAttriString
    }
    func ratingDetials() {
        let tapGestures = UITapGestureRecognizer(target: self, action: #selector(self.ratingDetailsAction))
        ratingStackView.addGestureRecognizer(tapGestures)
    }
    @objc func ratingDetailsAction() {
        delegate?.discratingDetails(ratingId: postData?.internalIdentifier ?? 0 , starredValue: starredValue ?? "", starCount: starCount ?? 0)
    }
    func thoughtsDetails() {
        let tapGestures = UITapGestureRecognizer(target: self, action: #selector(self.thoughstDetailsAction))
        thoughtsStackView.addGestureRecognizer(tapGestures)
    }
    @objc func thoughstDetailsAction() {
        delegate?.feedbackAction(commentId: universityId ?? 0, outsideComment: false, index: selectedIndex)
    }
    func addingActionInProImage() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.tapProfileImage(gesture:)))
        proImage.addGestureRecognizer(tap)
    }
    @IBAction func tapLabel(gesture: UITapGestureRecognizer) {
        
        let text = (nameLabel.text)!
        let termsRange = (text as NSString).range(of: name ?? "")
        
        if gesture.didTapAttributedTextInLabel(label: nameLabel, inRange: termsRange) {
            delegate?.pushingTOProfileVCFromDisc(id: postData?.postingAs?.internalIdentifier ?? 0, userType: postData?.postingAsType ?? "", isOwner: postData?.isOwner ?? false, postAsType: postData?.postingAsType ?? "")
            
        }
    }
    @IBAction func tapProfileImage(gesture: UITapGestureRecognizer) {
        delegate?.pushingTOProfileVCFromDisc(id: postData?.postingAs?.internalIdentifier ?? 0, userType: postData?.postingAsType ?? "", isOwner: postData?.isOwner ?? false, postAsType: postData?.postingAsType ?? "")
        
    }
    
    @IBAction func sendBtnAction(_ sender: Any) {
        if footerTxtView.text == "Add your thoughts" || footerTxtView.text == "" {
            return
        } else {
            let param:[String : Any] = ["comment_body" : footerTxtView.text ?? ""]
            ThoughtsResponse.requestToComment(id: "\(universityId ?? 0)", param: param) { [weak self](response) in
                guard let strongSelf = self else { return }
                strongSelf.footerTxtView.text = "Add your thoughts"
                strongSelf.footerTxtView.textColor = .lightGray
                strongSelf.footerTxtView.resignFirstResponder()
                print(response?.thoughtsData?.body)
                strongSelf.delegate?.commentOutsideDiscountTVC(comment: response?.thoughtsData, index: strongSelf.selectedIndex)
            }
        }
        
    }
    @IBAction func messageBtnAction(_ sender: Any) {
        delegate?.feedbackAction(commentId: universityId ?? 0, outsideComment: false, index: selectedIndex)
    }
    @IBAction func ratingBtnAction(_ sender: Any) {
        delegate?.sendingRating(id: universityId ?? 0, starValue: starredValue ?? "", selectedindexToReload: index ?? 0, selectionType: selectionType!)
    }
    @IBAction func moreBtnAction(_ sender: UIButton) {
        print(universityId)
        delegate?.moreOptionFromDiscountTVC(sender: sender,id: universityId ?? 0,isOwnBusiness: isOwnBusiness ?? false, postType: postType!, index: selectedIndex)
    }
}
extension DiscountTableViewCell: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return imageDataSource?.count ?? 0
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PostCreationCollectionViewCell", for: indexPath) as! PostCreationCollectionViewCell
        DispatchQueue.main.async {
            // cell.mainImage.image = self.imageDataSource[indexPath.row]
            cell.mainImage.setURLImage(imageURL: (GraduateUrl.imageUrl + (self.imageDataSource?[indexPath.row].filepath ?? "")))
        }
        
        return cell
    }
    
    
}
extension DiscountTableViewCell: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedImageIndex = indexPath.row
        //
        if let cell = collectionView.cellForItem(at: indexPath) as? PostCreationCollectionViewCell {
            let viewController = SimplePhotoViewerController(referencedView: cell.mainImage, image: cell.mainImage.image)
            viewController.dataSource = self
            viewController.delegate = self
            viewController.modalPresentationStyle = .overCurrentContext
            delegate?.fullscreen(vc: viewController)
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: collectionView.bounds.width - 24, height: collectionView.bounds.height - 24)
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 8, right: 8)
    }
}
extension DiscountTableViewCell: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView == footerTxtView {
            footerTxtView.textColor = UIColor.init(hexString: Appcolors.text)
            footerTxtView.text = ""
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if footerTxtView.text.isEmpty {
            textView.text = "Add your thoughts"
            textView.textColor = UIColor.lightGray
        }
    }
}
extension NSMutableAttributedString {
    var fontSize:CGFloat { return 14 }
    var boldFont:UIFont { return UIFont(name: "AvenirNext-Bold", size: fontSize) ?? UIFont.boldSystemFont(ofSize: fontSize) }
    var normalFont:UIFont { return UIFont(name: "AvenirNext-Regular", size: fontSize) ?? UIFont.systemFont(ofSize: fontSize)}
    
    func bold(_ value:String) -> NSMutableAttributedString {
        
        let attributes:[NSAttributedString.Key : Any] = [
            .font : boldFont
        ]
        
        self.append(NSAttributedString(string: value, attributes:attributes))
        return self
    }
    
    func normal(_ value:String) -> NSMutableAttributedString {
        
        let attributes:[NSAttributedString.Key : Any] = [
            .font : normalFont,
        ]
        
        self.append(NSAttributedString(string: value, attributes:attributes))
        return self
    }
    /* Other styling methods */
    func orangeHighlight(_ value:String) -> NSMutableAttributedString {
        
        let attributes:[NSAttributedString.Key : Any] = [
            .font :  normalFont,
            .foregroundColor : UIColor.white,
            .backgroundColor : UIColor.orange
        ]
        
        self.append(NSAttributedString(string: value, attributes:attributes))
        return self
    }
    
    func blackHighlight(_ value:String) -> NSMutableAttributedString {
        
        let attributes:[NSAttributedString.Key : Any] = [
            .font :  normalFont,
            .foregroundColor : UIColor.white,
            .backgroundColor : UIColor.black
            
        ]
        
        self.append(NSAttributedString(string: value, attributes:attributes))
        return self
    }
    
    func underlined(_ value:String) -> NSMutableAttributedString {
        
        let attributes:[NSAttributedString.Key : Any] = [
            .font :  normalFont,
            .underlineStyle : NSUnderlineStyle.single.rawValue
            
        ]
        
        self.append(NSAttributedString(string: value, attributes:attributes))
        return self
    }
}
extension DiscountTableViewCell: SimplePhotoViewerControllerDelegate {
    func photoViewerControllerDidEndPresentingAnimation(_ photoViewerController: DTPhotoViewerController) {
        photoViewerController.scrollToPhoto(at: selectedImageIndex, animated: false)
    }
    
    func photoViewerController(_ photoViewerController: DTPhotoViewerController, didScrollToPhotoAt index: Int) {
        selectedImageIndex = index
        if let collectionView = collectionView {
            let indexPath = IndexPath(item: selectedImageIndex, section: 0)
            
            // If cell for selected index path is not visible
            if !collectionView.indexPathsForVisibleItems.contains(indexPath) {
                // Scroll to make cell visible
                collectionView.scrollToItem(at: indexPath, at: UICollectionView.ScrollPosition.bottom, animated: false)
            }
        }
    }
    
    func simplePhotoViewerController(_ viewController: SimplePhotoViewerController, savePhotoAt index: Int) {
        //        UIImageWriteToSavedPhotosAlbum(imageDataSource?[index], nil, nil, nil)
    }
}
extension DiscountTableViewCell: DTPhotoViewerControllerDataSource {
    func photoViewerController(_ photoViewerController: DTPhotoViewerController, configureCell cell: DTPhotoCollectionViewCell, forPhotoAt index: Int) {
        // Set text for each item
        if let cell = cell as? CustomPhotoCollectionViewCell {
            cell.extraLabel.text = "Image no \(index + 1)"
        }
    }
    
    func photoViewerController(_ photoViewerController: DTPhotoViewerController, referencedViewForPhotoAt index: Int) -> UIView? {
        let indexPath = IndexPath(item: index, section: 0)
        if let cell = collectionView?.cellForItem(at: indexPath) as? PostCreationCollectionViewCell {
            cell.mainImage.layer.cornerRadius = 25
            return cell.mainImage
        }
        
        return nil
    }
    
    func numberOfItems(in photoViewerController: DTPhotoViewerController) -> Int {
        return imageDataSource?.count ?? 0
    }
    
    func photoViewerController(_ photoViewerController: DTPhotoViewerController, configurePhotoAt index: Int, withImageView imageView: UIImageView) {
        //        imageView.image = imageDataSource?[index]
        imageView.setURLImage(imageURL: (GraduateUrl.imageUrl + (imageDataSource?[index].filepath ?? "")))
    }
}
extension DiscountTableViewCell {
    func DiscountTVC(cell: DiscountTableViewCell, indexPath: IndexPath, dataSource: [uniMyPostData]?) -> DiscountTableViewCell {
        cell.name = dataSource?[indexPath.row].postingAs?.businessName
        
        if dataSource?[indexPath.row].galleries?.count == 0 {
            cell.collectionView.isHidden = true
        } else {
            cell.collectionView.isHidden = false
            cell.imageDataSource = dataSource?[indexPath.row].offer?.galleries
        }
        cell.proImage.setURLImage(imageURL: (GraduateUrl.imageUrl + (dataSource?[indexPath.row].postingAs?.businessProfileImage ?? "")))
        cell.captionLabel.text = dataSource?[indexPath.row].offer?.descriptionValue
        cell.discountLabel.text = dataSource?[indexPath.row].offer?.discountPercentage
        cell.expiryDate.text = dataSource?[indexPath.row].offer?.expiryDate
        cell.locationLabel.text = dataSource?[indexPath.row].offer?.addressString
        cell.tandCLabel.text = dataSource?[indexPath.row].offer?.terms
        if dataSource?[indexPath.row].hasStarred == true {
            cell.ratingBtn.setImage(UIImage(named: "star3"))
        } else {
            cell.ratingBtn.setImage(UIImage(named: "star"))
        }
        cell.ratingValue.text = "\(dataSource?[indexPath.row].starsCount ?? 0)"
        cell.thoughtsValue.text = "\(dataSource?[indexPath.row].comments?.count ?? 0)"
        cell.universityId = dataSource?[indexPath.row].internalIdentifier
        cell.starCount = dataSource?[indexPath.row].starsCount
        cell.starredValue = dataSource?[indexPath.row].starredValue
        // if dataSource?[indexPath.section].offer?.galleries?.count ?? 0 > 0 {
        //print(dataSource?[indexPath.section].offer?.galleries?[indexPath.row].createdAt ?? "")
        let newDate = HelperFunctions.timeInterval(timeAgo: dataSource?[indexPath.row].offer?.createdAt ?? "")
        print(newDate)
        cell.timeStampLabel.text = newDate
        //}
        
        cell.isOwnBusiness = dataSource?[indexPath.row].isOwner
        cell.index = indexPath.row
        cell.offerId = dataSource?[indexPath.row].postingAs?.internalIdentifier
        cell.selectedIndex = indexPath
        cell.postData = dataSource?[indexPath.row]
        cell.index = indexPath.row
        return cell
        
    }
}




