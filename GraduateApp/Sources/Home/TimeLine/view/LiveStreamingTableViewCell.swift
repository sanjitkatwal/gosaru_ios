//
//  LiveStreamingTableViewCell.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 12/16/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import UIKit
import MobileVLCKit

protocol SendingRatingFromLiveStreamingTVC: class {
    func sendingRatingsFromLiveStreamingTVC(id: Int, starValue: String, selectedindexToReload: Int, selectionType: GraduateDetails)
    func moreOptionsFromLiveStreamingTVC(sender: UIButton,index: IndexPath, isOwner: Bool,postType: GraduateDetails)
    func feedbackActionFromLiveStreamingTVC(commentId: Int, index: IndexPath!)
    func sendingVideoUrlForFullScreenFromLiveStreamingTVC()
    func pushingTOProfileVCFromLiveStreamingTVC(id: Int,userType: String, isOwner: Bool,postasType: String)
    func liveRatingDetailsFromLiveStreamingTVC(ratingId: Int, starredVlaue: String, starCount: Int)
    func commentOutsideFromLiveStreamingTVC(comment: Comments?, index: IndexPath)
}
class LiveStreamingTableViewCell: UITableViewCell, VLCMediaPlayerDelegate {

    @IBOutlet var thoughtsNumLabel: UILabel!
    @IBOutlet var ratingNumLabel: UILabel!
    @IBOutlet var sendBtn: UIButton!
    @IBOutlet var footerTxtView: UITextView!
    @IBOutlet var messageBtn: AnimatedButton!
    @IBOutlet var ratingBtn: AnimatedButton!
    @IBOutlet var thoughtsStackView: UIStackView!
    @IBOutlet var ratingStackView: UIStackView!
    @IBOutlet var moreBtn: UIButton!
    @IBOutlet var captionLabel: UILabel!
    @IBOutlet var timeStampLabel: UILabel!
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var proImage: UIImageView!
    @IBOutlet var imageContainerView: UIView!
    @IBOutlet var playBtn: UIButton!
    @IBOutlet var vlcView: UIView!
    var isPlaying: Bool = false
    var postData: uniMyPostData?
    var videoType: VideoType?
    var index: IndexPath!
    var names: String?
    var universityId: Int?
    var starreVlaue: String?
    var starCount: Int?
    var myView: UIView?
    var thoughtsCount: Int?
    var postType: GraduateDetails?
    var isOwnBusiness: Bool?
    var postId: Int?
    var name: String? {
        didSet {
            attributed()
        }
    }
    var commentId: Int?
    var selectionType: GraduateDetails?
    weak var delegate: SendingRatingFromLiveStreamingTVC?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        playBtn.setImage(UIImage(named: "iconPlay"))
        ratingDetailsDidTap()
        thoughtsDetailsDidTap()
        HelperFunctions.addingImageInbtn(ratingBtn: ratingBtn, messageBtn: messageBtn)
        HelperFunctions.addingImageInbtn(ratingBtn: ratingBtn, messageBtn: messageBtn)
        HelperFunctions.addingShadowInImage(avatarImageView: proImage, cornerRadius: 4, containerView: imageContainerView)
        footerTxtView.textColor = .lightGray
        footerTxtView.text = "Add your thoughts"
        footerTxtView.layer.cornerRadius = 8
        footerTxtView.delegate = self
        moreBtn.setImage(UIImage(named: "more-3"), for: .normal)
        moreBtn.imageView?.contentMode = .center
        // attributed()
        addingActionInProImage()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func ratingDetailsDidTap() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(ratingDetailsPushAction))
        ratingStackView.addGestureRecognizer(tapGesture)
    }
    @objc func ratingDetailsPushAction() {
        delegate?.liveRatingDetailsFromLiveStreamingTVC(ratingId: universityId ?? 0, starredVlaue: starreVlaue ?? "", starCount: starCount ?? 0)
    }
    func thoughtsDetailsDidTap() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(thoughtsPushAction))
        thoughtsStackView.addGestureRecognizer(tapGesture)
    }
    @objc func thoughtsPushAction() {
        delegate?.feedbackActionFromLiveStreamingTVC(commentId: universityId ?? 0, index: index)
    }
    func attributed() {
        print(videoType)
        switch videoType {
        case .liveVideo:
            nameLabel.text = ((name ?? "") + " was live")
        case .postVideo:
            nameLabel.text = ((name ?? "") + " created a post")
        default:
            break
        }
        //
        
        let text = (nameLabel.text)!
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.tapLabel(gesture:)))
        nameLabel.addGestureRecognizer(tap)
        nameLabel.isUserInteractionEnabled = true
        let underlineAttriString = NSMutableAttributedString(string: text)
        let range1 = (text as NSString).range(of: name ?? "")
        underlineAttriString.addAttribute(NSAttributedString.Key.font, value: UIFont.systemFont(ofSize: 17, weight: .medium), range: range1)
        print(underlineAttriString)
        nameLabel.attributedText = underlineAttriString
    }
    func addingActionInProImage() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.tapProfileImage(gesture:)))
        proImage.addGestureRecognizer(tap)
    }
    var didSetup: Bool = false
    
    var camera: Camera!
    
    let cameraTitle: UILabel = {
        
        let label = UILabel()
        
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.boldSystemFont(ofSize: 14)
        label.text = ""
        
        label.textColor = .white
        
        return label
        
    }()
    lazy var playerView: UIView = {
        
        let v = UIView()
        
        v.translatesAutoresizingMaskIntoConstraints = false
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.startPlay))
        v.addGestureRecognizer(tap)
        
        
        return v
        
    }()
    
    let player: VLCMediaPlayer = {
        
        let p = VLCMediaPlayer()
        
        
        return p
        
    }()
    func setupPlayer() -> Void{
        
        if self.didSetup{
            
            return
        }
      //  vlcView.addSubview(cameraTitle)
        vlcView.addSubview(playerView)
//        self.addSubview(cameraTitle)
//        self.addSubview(playerView)
        
        // add autolayout for player view
        let parentView = vlcView
        playerView.topAnchor.constraint(equalTo: parentView!.topAnchor, constant: 0).isActive = true
        playerView.leftAnchor.constraint(equalTo: parentView!.leftAnchor, constant: 0).isActive = true
        playerView.rightAnchor.constraint(equalTo: parentView!.rightAnchor, constant: 0).isActive = true
        playerView.bottomAnchor.constraint(equalTo: parentView!.bottomAnchor, constant: -30).isActive = true
//
        
        // setup autolayout for camera title
        
//        cameraTitle.bottomAnchor.constraint(equalTo: parentView!.bottomAnchor, constant: 0).isActive = true
//        cameraTitle.leftAnchor.constraint(equalTo: parentView!.leftAnchor, constant: 8).isActive = true
//        cameraTitle.rightAnchor.constraint(equalTo: parentView!.rightAnchor, constant: -8).isActive = true
        

        
        self.didSetup = true
        
        let mediaUrl = URL(string: camera.url!)
        let media = VLCMedia(url: mediaUrl!)
        player.media = media
        player.currentAudioTrackIndex = -1
        
        player.delegate = self
        player.drawable = playerView
        
       
       
        
        
        
    }
    
    @objc func startPlay(){
        
        if self.player.isPlaying{
            
            // pause video
            
            self.player.pause()
        }else{
            
            
            self.player.play()
        }
        
    }
    @IBAction func tapLabel(gesture: UITapGestureRecognizer) {
        
        let text = (nameLabel.text)!
        let termsRange = (text as NSString).range(of: name ?? "")
        
        if gesture.didTapAttributedTextInLabel(label: nameLabel, inRange: termsRange) {
            delegate?.pushingTOProfileVCFromLiveStreamingTVC(id: postData?.postingAs?.internalIdentifier ?? 0, userType: postData?.postingAsType ?? "", isOwner: postData?.isOwner ?? false, postasType: postData?.postingAsType ?? "")
        }
    }
    @IBAction func tapProfileImage(gesture: UITapGestureRecognizer) {
        delegate?.pushingTOProfileVCFromLiveStreamingTVC(id: postData?.postingAs?.internalIdentifier ?? 0, userType: postData?.postingAsType ?? "", isOwner: postData?.isOwner ?? false, postasType: postData?.postingAsType ?? "")
    }
    @IBAction func playBtnAction(_ sender: Any) {
        if isPlaying == false {
            player.play()
            isPlaying = true
            playBtn.setImage(UIImage(named: "iconPause-1"))
            playBtn.tintColor = .white
        } else if isPlaying == true {
            player.stop()
            isPlaying = false
            playBtn.setImage(UIImage(named: "iconPlay"))
            playBtn.tintColor = .white
        }
    }
    
    @IBAction func moreBtnAction(_ sender: Any) {
    }
    
}
extension LiveStreamingTableViewCell {
    func liveStreamingTVC(cell: LiveStreamingTableViewCell, indexPath: IndexPath, dataSource: [uniMyPostData]?) -> LiveStreamingTableViewCell {
        if dataSource?[indexPath.row].postingAsType == "business" {
            cell.name = dataSource?[indexPath.row].postingAs?.businessName
        } else if dataSource?[indexPath.row].postingAsType == "user" {
             cell.name = dataSource?[indexPath.row].postingAs?.name
        }
        cell.ratingNumLabel.text = "\(dataSource?[indexPath.row].starsCount ?? 0)"
        cell.thoughtsNumLabel.text = "\(dataSource?[indexPath.row].comments?.count ?? 0)"
        print(dataSource?[indexPath.row].postingAs?.businessName)
        cell.captionLabel.text = dataSource?[indexPath.row].message
        if dataSource?[indexPath.row].hasStarred == true {
            cell.ratingBtn.setImage(UIImage(named: "star3"))
        } else {
             cell.ratingBtn.setImage(UIImage(named: "star"))
        }
        let newDate = HelperFunctions.timeInterval(timeAgo: dataSource?[indexPath.row].createdAt ?? "")
                         print(newDate)
              cell.timeStampLabel.text = newDate
        cell.index = indexPath
        cell.starreVlaue = dataSource?[indexPath.row].starredValue
        cell.isOwnBusiness = dataSource?[indexPath.row].isOwner
        cell.universityId = dataSource?[indexPath.row].postingAs?.internalIdentifier
        cell.postData = dataSource?[indexPath.row]
        cell.postId = dataSource?[indexPath.row].internalIdentifier
        return cell
    }
}
extension LiveStreamingTableViewCell: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView == footerTxtView {
            footerTxtView.textColor = UIColor.init(hexString: Appcolors.buttons)
            footerTxtView.text = ""
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if footerTxtView.text.isEmpty {
            textView.text = "Add your thoughts"
            textView.textColor = UIColor.lightGray
        }
    }
}
