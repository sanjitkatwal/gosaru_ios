//
//  PortFolioTableViewCell.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 7/4/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import UIKit
import DTPhotoViewerController
protocol SendingRatingFromPFTVC: class {
    func pftvcRating(id: Int, starValue: String,selectedindexToReload: Int, selectionType: GraduateDetails)
    func moreOPtionFromPortTVC(sender: UIButton, id: Int,index: IndexPath, cell: PortFolioTableViewCell, isOwner: Bool, postType: GraduateDetails)
    func feedbackActionPFTVC()
    func fullScreen(vc: UIViewController)
    func pushingTOProfileVCFromPFTVC(id: Int,userType: String, isOwner: Bool,postAsType: String)
    func pftvcratingDetails(ratingId: Int, starredValue: String, starCount: Int)
    func pftvcthoughtsDetails(commentId: Int,outsideComment: Bool,index: IndexPath)
    func sendingThoughts(id: Int,param: [String : Any])
    func outsideCommentPortFolioTVC(comment: Comments?, index: IndexPath) 
}
class PortFolioTableViewCell: UITableViewCell {
    @IBOutlet var addressTextField: UILabel!
    @IBOutlet var containerView: UIView!
    @IBOutlet var createdDate: UILabel!
    @IBOutlet var subHeaderlabel: UILabel!
    @IBOutlet weak var ratingNumLabel: UILabel!
    @IBOutlet weak var timeStampLabel: UILabel!
    @IBOutlet weak var thoughtsNumLabel: UILabel!
    @IBOutlet weak var thoughtsStackView: UIStackView!
    @IBOutlet weak var ratingStackView: UIStackView!
    @IBOutlet weak var moreBtn: UIButton!
    @IBOutlet weak var imageContainerView: UIView!
    @IBOutlet weak var messageBtn: AnimatedButton!
    @IBOutlet weak var ratingBtn: AnimatedButton!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var footerTxtView: UITextView!
    @IBOutlet weak var footerView: UIView!
    @IBOutlet weak var midView: UIView!
    @IBOutlet weak var captionLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet var timestamp: UILabel!
    @IBOutlet weak var profileImage: UIImageView!
    fileprivate var selectedImageIndex: Int = 0
    var ratingId: Int?
    var getRatingDetailsId: Int?
    var starredValue: String?
    var starCount: Int?
    var isOwnBusiness: Bool?
    var reloadingIndex: Int?
    var selectionType: GraduateDetails?
    var postData: uniMyPostData?
    var postType: GraduateDetails?
    var collectionViewDataSource: Int? {
        didSet {
            DispatchQueue.main.async {
                self.collectionView.reloadData()
            }
        }
    }
    var commentId: Int?
    var index: IndexPath!
    var name: String? {
        didSet {
            attributed()
        }
    }
    weak var delegate: SendingRatingFromPFTVC?
    //    var imageDataSource: [UIImage] = [UIImage(named: "ceremony")!,UIImage(named: "college")!,UIImage(named: "iconSearch")!]
    var imageDataSource: [TimeLineGalleries]?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        HelperFunctions.addingImageInbtn(ratingBtn: ratingBtn, messageBtn: messageBtn)
        HelperFunctions.addingShadowInImage(avatarImageView: profileImage, cornerRadius: 6, containerView: imageContainerView)
        profileImage.layer.cornerRadius = 4
        self.collectionView.register(UINib(nibName:"PostCreationCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "PostCreationCollectionViewCell")
        collectionView.dataSource = self
        collectionView.delegate = self
        footerTxtView.layer.cornerRadius = 8
        footerTxtView.textColor = .lightGray
        footerTxtView.text = "Add your thoughts"
        footerTxtView.delegate = self
        moreBtn.setImage(UIImage(named: "more-3"), for: .normal)
        moreBtn.imageView?.contentMode = .center
        addingActionInProImage()
        ratingDetials()
        thoughtsDetails()
        addingTopAndBottomRedBorder()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    func addingTopAndBottomRedBorder() {
        containerView.borders(for: [.top], width: 2, color: UIColor.init(hexString: Appcolors.buttons))
        footerView.borders(for: [.bottom], width: 2, color: UIColor.init(hexString: Appcolors.buttons))
        
    }
    func attributed() {
        nameLabel.text = ((name ?? "") + " created a portfolio")
        print(nameLabel.text)
        let text = (nameLabel.text)!
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.tapLabel(gesture:)))
        nameLabel.addGestureRecognizer(tap)
        nameLabel.isUserInteractionEnabled = true
        let underlineAttriString = NSMutableAttributedString(string: text)
        let range1 = (text as NSString).range(of: name ?? "")
        underlineAttriString.addAttribute(NSAttributedString.Key.font, value: UIFont.systemFont(ofSize: 17, weight: .medium), range: range1)
        nameLabel.attributedText = underlineAttriString
    }
    func ratingDetials() {
        let tapGestures = UITapGestureRecognizer(target: self, action: #selector(self.ratingDetailsAction))
        ratingStackView.addGestureRecognizer(tapGestures)
    }
    @objc func ratingDetailsAction() {
        delegate?.pftvcratingDetails(ratingId: postData?.internalIdentifier ?? 0, starredValue: starredValue ?? "", starCount: starCount ?? 0)
    }
    func thoughtsDetails() {
        let tapGestures = UITapGestureRecognizer(target: self, action: #selector(self.thoughstDetailsAction))
        thoughtsStackView.addGestureRecognizer(tapGestures)
    }
    @objc func thoughstDetailsAction() {
        print(commentId)
        delegate?.pftvcthoughtsDetails(commentId: postData?.internalIdentifier ?? 0, outsideComment: false, index: index)
       
    }
    func addingActionInProImage() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.tapProfileImage(gesture:)))
        profileImage.addGestureRecognizer(tap)
    }
    @IBAction func tapLabel(gesture: UITapGestureRecognizer) {
        
        let text = (nameLabel.text)!
        let termsRange = (text as NSString).range(of: name ?? "")
        
        if gesture.didTapAttributedTextInLabel(label: nameLabel, inRange: termsRange) {
            delegate?.pushingTOProfileVCFromPFTVC(id: ratingId ?? 0, userType: postData?.postingAsType ?? "", isOwner: postData?.isOwner ?? false, postAsType: postData?.postingAsType ?? "")
        }
    }
    @IBAction func tapProfileImage(gesture: UITapGestureRecognizer) {
        delegate?.pushingTOProfileVCFromPFTVC(id: ratingId ?? 0, userType: postData?.postingAsType ?? "", isOwner: postData?.isOwner ?? false, postAsType: postData?.postingAsType ?? "")
    }
    
    @IBAction func moreBtnAction(_ sender: UIButton) {
        print(ratingId,index,isOwnBusiness,postType)
        delegate?.moreOPtionFromPortTVC(sender: sender, id: ratingId ?? 0, index: index, cell: self, isOwner: isOwnBusiness ?? false, postType: postType!)
    }
    @IBAction func ratingAction(_ sender: Any) {
        delegate?.pftvcRating(id: postData?.internalIdentifier ?? 0, starValue: starredValue ?? "", selectedindexToReload: reloadingIndex ?? 0, selectionType: selectionType!)
    }
    @IBAction func messageAction(_ sender: Any) {
        delegate?.pftvcthoughtsDetails(commentId: postData?.internalIdentifier ?? 0, outsideComment: false, index: index)
        
    }
    @IBAction func sendBtnAction(_ sender: Any) {
        if footerTxtView.text == "Add your thoughts" {
            
        } else {
            let param:[String : Any] = ["comment_body" : footerTxtView.text ?? ""]
            footerTxtView.text = "Add your thoughts"
            footerTxtView.textColor = .lightGray
            footerTxtView.resignFirstResponder()
            ThoughtsResponse.requestToComment(id: "\(getRatingDetailsId ?? 0)", param: param) { [weak self](response) in
                guard let strongSelf = self else { return }
                
                print(response?.thoughtsData?.body)
                strongSelf.delegate?.outsideCommentPortFolioTVC(comment: response?.thoughtsData, index: strongSelf.index)
            }
        }
        
        
    }
}
extension PortFolioTableViewCell: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imageDataSource?.count ?? 0
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PostCreationCollectionViewCell", for: indexPath) as! PostCreationCollectionViewCell
        DispatchQueue.main.async {
            // cell.mainImage.image = self.imageDataSource[indexPath.row]
            print((GraduateUrl.imageUrl + (self.imageDataSource?[indexPath.row].filepath ?? "")))
            cell.mainImage.setURLImage(imageURL: (GraduateUrl.imageUrl + (self.imageDataSource?[indexPath.row].filepath ?? "")))
        }
        
        return cell
    }
    
    
}
extension PortFolioTableViewCell: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedImageIndex = indexPath.row
        
        if let cell = collectionView.cellForItem(at: indexPath) as? PostCreationCollectionViewCell {
            let viewController = SimplePhotoViewerController(referencedView: cell.mainImage, image: cell.mainImage.image)
            viewController.modalPresentationStyle = .overCurrentContext
            viewController.dataSource = self
            viewController.delegate = self
            
            delegate?.fullScreen(vc: viewController)
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        //        let numberOfItemsPerRow:CGFloat = 4
        //        let spacingBetweenCells:CGFloat = 16
        //        //let spacing: CGFloat = 3
        //
        //        let totalSpacing = (2 * self.spacing) + ((numberOfItemsPerRow - 1) * spacingBetweenCells) //Amount of total spacing in a row
        //
        //        if let collection = self.collectionView{
        //            let width = (collection.bounds.width - totalSpacing)/numberOfItemsPerRow
        //            return CGSize(width: width, height: width)
        //        }else{
        //            return CGSize(width: 0, height: 0)
        
        return CGSize(width: collectionView.bounds.width, height: collectionView.bounds.height - 16)
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
}
extension PortFolioTableViewCell: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView == footerTxtView {
            footerTxtView.textColor = UIColor.init(hexString: Appcolors.text)
            footerTxtView.text = ""
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if footerTxtView.text.isEmpty {
            textView.text = "Add your thoughts"
            textView.textColor = UIColor.lightGray
        }
    }
}
extension PortFolioTableViewCell: DTPhotoViewerControllerDataSource {
    func photoViewerController(_ photoViewerController: DTPhotoViewerController, configureCell cell: DTPhotoCollectionViewCell, forPhotoAt index: Int) {
        // Set text for each item
        if let cell = cell as? CustomPhotoCollectionViewCell {
            cell.extraLabel.text = "Image no \(index + 1)"
        }
    }
    
    func photoViewerController(_ photoViewerController: DTPhotoViewerController, referencedViewForPhotoAt index: Int) -> UIView? {
        let indexPath = IndexPath(item: index, section: 0)
        if let cell = collectionView?.cellForItem(at: indexPath) as? PostCreationCollectionViewCell {
            cell.mainImage.layer.cornerRadius = 25
            return cell.mainImage
        }
        
        return nil
    }
    
    func numberOfItems(in photoViewerController: DTPhotoViewerController) -> Int {
        return imageDataSource?.count ?? 0
    }
    
    func photoViewerController(_ photoViewerController: DTPhotoViewerController, configurePhotoAt index: Int, withImageView imageView: UIImageView) {
        //imageView.image = imageDataSource[index]
        imageView.setURLImage(imageURL: (GraduateUrl.imageUrl + (self.imageDataSource?[index].filepath ?? "")))
    }
}

// MARK: DTPhotoViewerControllerDelegate
extension PortFolioTableViewCell: SimplePhotoViewerControllerDelegate {
    func simplePhotoViewerController(_ viewController: SimplePhotoViewerController, savePhotoAt index: Int) {
        
    }
    
    func photoViewerControllerDidEndPresentingAnimation(_ photoViewerController: DTPhotoViewerController) {
        photoViewerController.scrollToPhoto(at: selectedImageIndex, animated: false)
    }
    
    func photoViewerController(_ photoViewerController: DTPhotoViewerController, didScrollToPhotoAt index: Int) {
        selectedImageIndex = index
        if let collectionView = collectionView {
            let indexPath = IndexPath(item: selectedImageIndex, section: 0)
            
            // If cell for selected index path is not visible
            if !collectionView.indexPathsForVisibleItems.contains(indexPath) {
                // Scroll to make cell visible
                collectionView.scrollToItem(at: indexPath, at: UICollectionView.ScrollPosition.bottom, animated: false)
            }
        }
    }
    
    //    func simplePhotoViewerController(_ viewController: SimplePhotoViewerController, savePhotoAt index: Int) {
    //        UIImageWriteToSavedPhotosAlbum((imageDataSource?[index].filepath)!, nil, nil, nil)
    //    }
}
extension PortFolioTableViewCell {
    func portFolioCell(cell: PortFolioTableViewCell, indexPath: IndexPath, dataSource: [uniMyPostData]?) -> PortFolioTableViewCell {
        if dataSource?[indexPath.row].portfolio?.galleries?.count == 0 {
            cell.collectionView.isHidden = true
        } else {
            cell.collectionView.isHidden = false
            cell.imageDataSource = dataSource?[indexPath.row].portfolio?.galleries
        }
        
        if dataSource?[indexPath.row].hasStarred == true {
            cell.ratingBtn.setImage(UIImage(named: "star3"))
        } else {
            cell.ratingBtn.setImage(UIImage(named: "star"))
        }
        let newDate = HelperFunctions.timeInterval(timeAgo: dataSource?[indexPath.row].portfolio?.createdAt ?? "")
    cell.timestamp.text = newDate
        cell.ratingNumLabel.text = "\(dataSource?[indexPath.row].starsCount ?? 0 )"
        cell.thoughtsNumLabel.text = "\(dataSource?[indexPath.row].comments?.count ?? 0 )"
        cell.collectionViewDataSource = dataSource?[indexPath.row].galleries?.count
        cell.name = dataSource?[indexPath.row].postingAs?.businessName
        
        if dataSource?[indexPath.row].portfolio?.business?.businessProfileImage != nil {
            cell.profileImage.setURLImage(imageURL: (GraduateUrl.imageUrl + ((dataSource?[indexPath.row].portfolio?.business?.businessProfileImage)!)))
        }
        
        
        cell.subHeaderlabel.text = dataSource?[indexPath.row].portfolio?.name
        let date = convertDateFormater(input: dataSource?[indexPath.row].portfolio?.createdAt ?? "")
       // let newDates = HelperFunctions.timeInterval(timeAgo: dataSource?[indexPath.row].offer?.createdAt ?? "")
     //   cell.createdDate.text = newDates
        //        let date = convertDateFormater(input: dataSource?[indexPath.row].portfolio?.createdAt ?? "")
        //        print(date)
        
        
        //cell.createdDate.text = date
        //                cell.addressTextField.text = dataSource?.portfolio?.data?[indexPath.row].addressString
        //cell.delegate = self
        cell.starredValue = dataSource?[indexPath.row].starredValue
        cell.starCount = dataSource?[indexPath.row].starsCount
        //cell.ratingId = dataSource?[indexPath.row].internalIdentifier
        cell.index = indexPath
        cell.ratingId =  dataSource?[indexPath.row].portfolio?.businessId
        cell.addressTextField.text =  dataSource?[indexPath.row].portfolio?.addressString
        cell.captionLabel.text = dataSource?[indexPath.row].message
        cell.isOwnBusiness = dataSource?[indexPath.row].isOwner
        cell.reloadingIndex = indexPath.row
        cell.postData = dataSource?[indexPath.row]
        cell.getRatingDetailsId = dataSource?[indexPath.row].internalIdentifier
       // cell.timeStampLabel.isHidden = false
        return cell
    }
    func convertDateFormater(input: String) -> String
    {
        
        
        let dateFormatter = DateFormatter()
        dateFormatter.calendar = Calendar(identifier: .iso8601)
        dateFormatter.dateFormat = "yyyy-MM-ddTHH:mm:ssZ"
        let newFormat = DateFormatter()
        newFormat.dateFormat = "MM dd yyyy"
        if let date = dateFormatter.date(from: input) {
            return newFormat.string(from: date)
        } else {
            return ""
        }
        
    }
}
extension Date {
    static func getFormattedDate(string: String , formatter:String) -> String{
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "MMM dd,yyyy"
        
        let date: Date? = dateFormatterGet.date(from: "2018-02-01T19:10:04+00:00")
        print("Date",dateFormatterPrint.string(from: date!)) // Feb 01,2018
        return dateFormatterPrint.string(from: date!);
    }
}


