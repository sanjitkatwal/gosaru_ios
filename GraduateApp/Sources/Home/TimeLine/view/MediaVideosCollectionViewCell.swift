//
//  MediaVideosCollectionViewCell.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 6/30/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import UIKit
import GSPlayer
protocol RemovingPostVideos: class {
    func removingVideos(index: IndexPath)
}
class MediaVideosCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var fullScreenView: VideoFullscreenPlayerView!
    @IBOutlet weak var closeBtn: UIButton!
    weak var delegate: RemovingPostVideos?
    @IBOutlet weak var playerView: VideoPlayerView!
    var index: IndexPath?
    override func awakeFromNib() {
         //playerView.play(for: URL(string: "http://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4")!)
        playerView.contentMode = .scaleToFill
        playerView.layer.cornerRadius = 8
        playerView.clipsToBounds = true
       // fullScreenView.con
//        closeBtn.setImage(UIImage(named: "cross"), for: .normal)
//        closeBtn.tintColor = .white
        //playerView.
        let transion = VideoFullscreenTransitioner()
        transion.playerView = playerView
        transion.fullscreenControls = [closeBtn]
        let btn = UIButton()
        btn.setImage(UIImage(named: "cross"), for: .normal)
        btn.frame = CGRect(x: 55, y: 0, width: 15, height: 15)
        let tap = UITapGestureRecognizer(target: self, action: #selector(closeBtnTapped))
        btn.addGestureRecognizer(tap)
        playerView.addSubview(btn)
    
        
    }
    @objc func closeBtnTapped() {
        delegate?.removingVideos(index: index!)
    }
    @IBAction func closeAction(_ sender: Any) {
        print(index)
        delegate?.removingVideos(index: index!)
    }
    
    func playVideo(url: URL) {
        print(url)
        playerView.play(for: url)
  }
    
}
