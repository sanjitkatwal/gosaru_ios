//
//  RatingDetailsTableViewCell.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 7/26/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import UIKit
protocol SendingActionToRDTVC: class {
    func pushingTOProfileVC()
    func alertForSendingRequest()
    func profilePictureDidTap()
    func messageBtnTapped()
}
class RatingDetailsTableViewCell: UITableViewCell {

    @IBOutlet weak var ratingNum: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var addFriendBtn: UIButton!
    @IBOutlet weak var mainImage: UIImageView!
    @IBOutlet weak var messageBtn: UIButton!
    var name: String? {
        didSet {
           // attributed()
        }
    }
    weak var delegate: SendingActionToRDTVC?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
//        mainImage.layer.cornerRadius = 6
//        mainImage.clipsToBounds = true
//        let tap = UITapGestureRecognizer(target: self, action: #selector(tapImage))
//        mainImage.addGestureRecognizer(tap)
//     messageBtn.setImage(UIImage(named: "comment"), for: .normal)
//            addFriendBtn.setImage(UIImage(named: "newuser"), for: .normal)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
       
    }
   func attributed() {
                nameLabel.text = name
                let text = (nameLabel.text)!
    let tap = UITapGestureRecognizer(target: self, action: #selector(self.tapLabel(gesture:)))
             nameLabel.addGestureRecognizer(tap)
             nameLabel.isUserInteractionEnabled = true
                let underlineAttriString = NSMutableAttributedString(string: text)
    let range1 = (text as NSString).range(of: name ?? "")
             underlineAttriString.addAttribute(NSAttributedString.Key.font, value: UIFont.systemFont(ofSize: 17, weight: .medium), range: range1)
                nameLabel.attributedText = underlineAttriString
            }
    @IBAction func tapLabel(gesture: UITapGestureRecognizer) {
    
        let text = (nameLabel.text)!
        let termsRange = (text as NSString).range(of: name ?? "")
        if gesture.didTapAttributedTextInLabel(label: nameLabel, inRange: termsRange) {
            delegate?.pushingTOProfileVC()
        }
    }
//    @IBAction func tapLabel(_ sender: Any) {
//        let text = (nameLabel.text)!
//     let termsRange = (text as NSString).range(of: name ?? "default")
//        delegate?.pushingTOProfileVC()
////        if gesture.didTapAttributedTextInLabel(label: nameLabel, inRange: termsRange) {
////
////        }
//    }
  

    @IBAction func messagebtnAction(_ sender: Any) {
        delegate?.messageBtnTapped()
    }
    @IBAction func addFriendBtnAction(_ sender: Any) {
        addFriendBtn.isHidden = true
        delegate?.alertForSendingRequest()
    }
    @objc func tapImage() {
        delegate?.profilePictureDidTap()
    }
}
