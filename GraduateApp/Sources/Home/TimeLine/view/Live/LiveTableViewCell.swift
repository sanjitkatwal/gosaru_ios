//
//  LiveTableViewCell.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 7/6/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import UIKit
import ASPVideoPlayer
import AVFoundation
protocol SendingRatingFromLiveTVC: class {
    func sendingRatings(id: Int, starValue: String, selectedindexToReload: Int, selectionType: GraduateDetails)
    func moreOptionsFromLiveTVC(sender: UIButton,index: IndexPath, isOwner: Bool,postType: GraduateDetails)
    func feedbackActionLiveTVC(commentId: Int, index: IndexPath!)
    func sendingVideoUrlForFullScreen()
    func pushingTOProfileVC(id: Int,userType: String, isOwner: Bool,postasType: String)
    func liveRatingDetails(ratingId: Int, starredVlaue: String, starCount: Int)
    func commentOutsideLiveTVC(comment: Comments?, index: IndexPath)
}
class LiveTableViewCell: UITableViewCell {
    @IBOutlet var captionLabel: UILabel!
    @IBOutlet weak var timeStampLabel: UILabel!
    @IBOutlet var locationLabel: UILabel!
    @IBOutlet var viewersStackView: UIStackView!
    @IBOutlet weak var viewersLabel: UILabel!
    @IBOutlet weak var thoughtsNumLabel: UILabel!
    @IBOutlet var addressStackView: UIStackView!
    @IBOutlet weak var ratingNumLabel: UILabel!
    @IBOutlet weak var thoughtsStackView: UIStackView!
    @IBOutlet weak var ratingStackView: UIStackView!
    @IBOutlet weak var moreBtn: UIButton!
    @IBOutlet weak var imageContainerView: UIView!
    @IBOutlet weak var videoPlayer: ASPVideoPlayer!
    @IBOutlet weak var videoPlayerBackgroundView: UIView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var meidaView: UIView!
    @IBOutlet weak var messageBtn: AnimatedButton!
    @IBOutlet weak var footerTxtVIew: UITextView!
    @IBOutlet weak var ratingBtn: AnimatedButton!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var proImage: UIImageView!
    weak var delegate: SendingRatingFromLiveTVC?
    var postData: uniMyPostData?
    var videoType: VideoType?
    var index: IndexPath!
    var names: String?
    var videoDatasource: AVURLAsset?
    var universityId: Int?
    var starreVlaue: String?
    var starCount: Int?
    var myView: UIView?
    var thoughtsCount: Int?
    var postType: GraduateDetails?
    var name: String? {
        didSet {
            attributed()
        }
    }
    var commentId: Int?
    var selectionType: GraduateDetails?
    let player = ASPVideoPlayer()
    var windw = UIApplication.shared.windows.first { $0.isKeyWindow }
    
    let overlayview = UIView(frame: CGRect(x:  0, y: 0, width: (UIApplication.shared.windows.first { $0.isKeyWindow }?.bounds.width)!, height: (UIApplication.shared.windows.first { $0.isKeyWindow }?.bounds.height)!))
    
    // let firstLocalVideoURL = Bundle.main.url(forResource: "video", withExtension: "mp4")
    //let secondLocalVideoURL = Bundle.main.url(forResource: "video2", withExtension: "mp4")
    //    let firstNetworkURL = URL(string: "http://devimages.apple.com/iphone/samples/bipbop/bipbopall.m3u8")
    //       let secondNetworkURL = URL(string: "http://www.easy-fit.ae/wp-content/uploads/2014/09/WebsiteLoop.mp4")
    override func awakeFromNib() {
        super.awakeFromNib()
        ratingDetailsDidTap()
        thoughtsDetailsDidTap()
        // Initialization code
        /// videoPlayerBackgroundView.addSubview(videoPlayer)
        HelperFunctions.addingImageInbtn(ratingBtn: ratingBtn, messageBtn: messageBtn)
        //        containerView.clipsToBounds = true
        //              containerView.layer.masksToBounds = true
        //        videoPlayerBackgroundView.clipsToBounds = true
        //
        //        videoPlayerBackgroundView.layer.masksToBounds = true
        //
        //        videoPlayer.layer.masksToBounds = true
        videoPlayer.layer.cornerRadius = 12
        videoPlayer.tintColor = UIColor.init(hexString: Appcolors.buttons)
        
        
        HelperFunctions.addingImageInbtn(ratingBtn: ratingBtn, messageBtn: messageBtn)
        HelperFunctions.addingShadowInImage(avatarImageView: proImage, cornerRadius: 4, containerView: imageContainerView)
        // footerTxtVIew.layer.cornerRadius = 4
        //  addVideo()
        footerTxtVIew.textColor = .lightGray
        footerTxtVIew.text = "Add your thoughts"
        footerTxtVIew.layer.cornerRadius = 8
        footerTxtVIew.delegate = self
        moreBtn.setImage(UIImage(named: "more-3"), for: .normal)
        moreBtn.imageView?.contentMode = .center
        // attributed()
        addingActionInProImage()
        print(names)
        //addVideo()
        videoPlayer.resizeClosure = { [unowned self] isExpanded in
            self.rotate(isExpanded: isExpanded)
        }
        videoPlayer.delegate = self
    }
    func ratingDetailsDidTap() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(ratingDetailsPushAction))
        ratingStackView.addGestureRecognizer(tapGesture)
    }
    @objc func ratingDetailsPushAction() {
        delegate?.liveRatingDetails(ratingId: postData?.internalIdentifier ?? 0, starredVlaue: starreVlaue ?? "", starCount: starCount ?? 0)
    }
    func thoughtsDetailsDidTap() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(thoughtsPushAction))
        thoughtsStackView.addGestureRecognizer(tapGesture)
    }
    @objc func thoughtsPushAction() {
        delegate?.feedbackActionLiveTVC(commentId: universityId ?? 0, index: index)
    }
    
    
    func attributed() {
        print(videoType)
        switch videoType {
        case .liveVideo:
            nameLabel.text = ((name ?? "") + " was live")
        case .postVideo:
            nameLabel.text = ((name ?? "") + " created a post")
        default:
            break
        }
        //
        
        let text = (nameLabel.text)!
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.tapLabel(gesture:)))
        nameLabel.addGestureRecognizer(tap)
        nameLabel.isUserInteractionEnabled = true
        let underlineAttriString = NSMutableAttributedString(string: text)
        let range1 = (text as NSString).range(of: name ?? "")
        underlineAttriString.addAttribute(NSAttributedString.Key.font, value: UIFont.systemFont(ofSize: 17, weight: .medium), range: range1)
        print(underlineAttriString)
        nameLabel.attributedText = underlineAttriString
    }
    
    
    func printName() {
        print(names)
    }
    func addingActionInProImage() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.tapProfileImage(gesture:)))
        proImage.addGestureRecognizer(tap)
    }
    @IBAction func tapLabel(gesture: UITapGestureRecognizer) {
        
        let text = (nameLabel.text)!
        let termsRange = (text as NSString).range(of: name ?? "")
        
        if gesture.didTapAttributedTextInLabel(label: nameLabel, inRange: termsRange) {
            delegate?.pushingTOProfileVC(id: postData?.postingAs?.internalIdentifier ?? 0, userType: postData?.postingAsType ?? "", isOwner: postData?.isOwner ?? false, postasType: postData?.postingAsType ?? "")
        }
    }
    @IBAction func tapProfileImage(gesture: UITapGestureRecognizer) {
        delegate?.pushingTOProfileVC(id: postData?.postingAs?.internalIdentifier ?? 0, userType: postData?.postingAsType ?? "", isOwner: postData?.isOwner ?? false, postasType: postData?.postingAsType ?? "")
    }
    func addVideo() {
        var videoUrls: [URL] = []
        print(videoDatasource)
        let secondVideoURL = Bundle.main.url(forResource: "video2", withExtension: "mp4")
        let firstAsset = AVURLAsset(url: secondVideoURL!)
        videoUrls.append(secondVideoURL!)
        
        videoPlayer.videoURLs = videoUrls
        
        videoPlayer.videoAssets = [firstAsset]
        
        
        //        let firstLocalVideoURL = Bundle.main.url(forResource: "video", withExtension: "mp4")
        //
        //        let firstAsset = AVURLAsset(url: firstLocalVideoURL!)
        //       // let secondAsset = AVURLAsset(url: secondLocalVideoURL!)
        //        let thirdAsset = AVURLAsset(url: firstNetworkURL!)
        //        let fourthAsset = AVURLAsset(url: secondNetworkURL!)
        //        //        videoPlayer.videoURLs = [firstLocalVideoURL!, secondLocalVideoURL!, firstNetworkURL!, secondNetworkURL!]
        //        videoPlayer.videoAssets = [firstAsset, thirdAsset, fourthAsset]
        //                videoPlayer.configuration = ASPVideoPlayer.Configuration(videoGravity: .aspectFit, shouldLoop: false, startPlayingWhenReady: false, controlsInitiallyHidden: true, allowBackgroundPlay: false)
        //
        //videoPlayer.delegate = self
        
        
        
        
    }
    func rotate(isExpanded: Bool) {
        
        let bgView = UIView(frame: UIScreen.main.bounds)
        bgView.backgroundColor = .red
        bgView.tag = 1
        bgView.addToWindow()
        // delegate?.sendingVideoUrlForFullScreen()
        let views: [String:Any] = ["videoPlayer": videoPlayer as Any,
                                   "backgroundView": videoPlayerBackgroundView as Any]
        
        var constraints: [NSLayoutConstraint] = []
        
        if isExpanded == false {
            self.containerView.removeConstraints(self.videoPlayer.constraints)
            
            bgView.addSubview(self.videoPlayerBackgroundView)
            bgView.addSubview(self.videoPlayer)
            self.videoPlayer.frame = self.containerView.frame
            self.videoPlayerBackgroundView.frame = self.containerView.frame
            
            let padding = (bgView.bounds.height - bgView.bounds.width) / 2.0
            
            var bottomPadding: CGFloat = 0
            
            if #available(iOS 11.0, *) {
                if self.contentView.safeAreaInsets != .zero {
                    bottomPadding = self.contentView.safeAreaInsets.bottom
                }
            }
            
            let metrics: [String:Any] = ["padding":padding,
                                         "negativePaddingAdjusted":-(padding - bottomPadding),
                                         "negativePadding":-padding]
            
            constraints.append(contentsOf:
                NSLayoutConstraint.constraints(withVisualFormat: "H:|-(negativePaddingAdjusted)-[videoPlayer]-(negativePaddingAdjusted)-|",
                                               options: [],
                                               metrics: metrics,
                                               views: views))
            constraints.append(contentsOf:
                NSLayoutConstraint.constraints(withVisualFormat: "V:|-(padding)-[videoPlayer]-(padding)-|",
                                               options: [],
                                               metrics: metrics,
                                               views: views))
            
            constraints.append(contentsOf:
                NSLayoutConstraint.constraints(withVisualFormat: "H:|-(negativePadding)-[backgroundView]-(negativePadding)-|",
                                               options: [],
                                               metrics: metrics,
                                               views: views))
            constraints.append(contentsOf:
                NSLayoutConstraint.constraints(withVisualFormat: "V:|-(padding)-[backgroundView]-(padding)-|",
                                               options: [],
                                               metrics: metrics,
                                               views: views))
            
            bgView.addConstraints(constraints)
            self.videoPlayer.layoutIfNeeded()
            bgView.addToWindow()
        } else {
            //bgView.removeFromSuperview()
            bgView.removeFromWindow()
            bgView.removeConstraints(self.previousConstraints)
            
            let targetVideoPlayerFrame = bgView.convert(self.videoPlayer.frame, to: self.containerView)
            let targetVideoPlayerBackgroundViewFrame = bgView.convert(self.contentView.frame, to: self.containerView)
            
            self.containerView.addSubview(self.videoPlayerBackgroundView)
            self.containerView.addSubview(self.videoPlayer)
            
            self.videoPlayer.frame = targetVideoPlayerFrame
            self.videoPlayerBackgroundView.frame = targetVideoPlayerBackgroundViewFrame
            
            constraints.append(contentsOf:
                NSLayoutConstraint.constraints(withVisualFormat: "H:|[videoPlayer]|",
                                               options: [],
                                               metrics: nil,
                                               views: views))
            constraints.append(contentsOf:
                NSLayoutConstraint.constraints(withVisualFormat: "V:|[videoPlayer]|",
                                               options: [],
                                               metrics: nil,
                                               views: views))
            
            constraints.append(contentsOf:
                NSLayoutConstraint.constraints(withVisualFormat: "H:|[backgroundView]|",
                                               options: [],
                                               metrics: nil,
                                               views: views))
            constraints.append(contentsOf:
                NSLayoutConstraint.constraints(withVisualFormat: "V:|[backgroundView]|",
                                               options: [],
                                               metrics: nil,
                                               views: views))
            // bgView.removeFromSuperview()
            self.containerView.addConstraints(constraints)
            //bgView.removeFromWindow()
            bgView.removeFromSuperview()
            var subviews = window!.subviews
            
            for subview : AnyObject in subviews{
                
                // Do what you want to do with the subview
                print(subview)
            }
            
            window?.subviews[1].removeFromSuperview()
            
        }
        self.previousConstraints = constraints
        
        
        UIView.animate(withDuration: 0.25, delay: 0.0, options: [], animations: {
            self.videoPlayer.transform = isExpanded == true ? .identity : CGAffineTransform(rotationAngle: .pi / 2.0)
            self.videoPlayerBackgroundView.transform = isExpanded == true ? .identity : CGAffineTransform(rotationAngle: .pi / 2.0)
            
            bgView.layoutIfNeeded()
        })
        
    }
    @IBAction func moreBtnAction(_ sender: UIButton) {
       print(index)
        print(postData?.isOwner)
        delegate?.moreOptionsFromLiveTVC(sender: sender, index: index, isOwner: postData?.isOwner ?? false, postType: postType!)
        
    }
    var prefersStatusBarHidden: Bool {
        return true
    }
    
    var previousConstraints: [NSLayoutConstraint] = []
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    //    func rotate(isExpanded: Bool) {
    //        delegate?.sendingVideoUrlForFullScreen()
    //
    //
    //    }
    
    @IBAction func sendAction(_ sender: Any) {
        if footerTxtVIew.text == "Add your thoughts" {
            
        } else {
            
            let param:[String : Any] = ["comment_body" : footerTxtVIew.text ?? ""]
            footerTxtVIew.text = "Add your thoughts"
            footerTxtVIew.textColor = .lightGray
            footerTxtVIew.resignFirstResponder()
            print(universityId)
            ThoughtsResponse.requestToComment(id: "\(commentId ?? 0)", param: param) { [weak self](response) in
                guard let strongSelf = self else { return }
               
                print(response?.thoughtsData?.body)
                strongSelf.delegate?.commentOutsideLiveTVC(comment: response?.thoughtsData, index: strongSelf.index)
            }
        }
        
    }
    @IBAction func messageAction(_ sender: Any) {
        print(commentId)
        delegate?.feedbackActionLiveTVC(commentId: commentId ?? 0, index: index)
    }
    @IBAction func ratingActin(_ sender: Any) {
        print(starreVlaue)
        delegate?.sendingRatings(id: postData?.internalIdentifier ?? 0, starValue: postData?.starredValue ?? "", selectedindexToReload: index.row, selectionType: selectionType!)
    }
}
extension LiveTableViewCell: ASPVideoPlayerViewDelegate {
    func startedVideo() {
        print("Started video")
        
    }
    
    func stoppedVideo() {
        print("Stopped video")
    }
    
    func newVideo() {
        print("New Video")
        
    }
    
    func readyToPlayVideo() {
        print("Ready to play video")
        
    }
    
    func playingVideo(progress: Double) {
        //        print("Playing: \(progress)")
        
        
    }
    
    func pausedVideo() {
        print("Paused Video")
    }
    
    func finishedVideo() {
        print("Finished Video")
    }
    
    func seekStarted() {
        print("Seek started")
    }
    
    func seekEnded() {
        print("Seek ended")
    }
    
    func error(error: Error) {
        print("Error: \(error)")
    }
    
    func willShowControls() {
        print("will show controls")
    }
    
    func didShowControls() {
        print("did show controls")
    }
    
    func willHideControls() {
        print("will hide controls")
    }
    
    func didHideControls() {
        print("did hide controls")
    }
}
extension LiveTableViewCell: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView == footerTxtVIew {
            footerTxtVIew.textColor = UIColor.init(hexString: Appcolors.buttons)
            footerTxtVIew.text = ""
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if footerTxtVIew.text.isEmpty {
            textView.text = "Add your thoughts"
            textView.textColor = UIColor.lightGray
        }
    }
}

