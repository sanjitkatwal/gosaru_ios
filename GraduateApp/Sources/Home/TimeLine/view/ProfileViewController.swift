//
//  ProfileViewController.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 7/20/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import UIKit
import AVFoundation
class ProfileViewController: UIViewController {

    @IBOutlet weak var mainImage: UIImageView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var inviteContainerView: UIView!
    
    @IBOutlet weak var audiocontainerView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var parentScrollView: UIScrollView!
    var uni: Bool?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Fake Profile"
        tableView.register(UINib(nibName: "GraduateTableViewCell", bundle: nil), forCellReuseIdentifier: "GraduateTableViewCell")
        tableView.register(UINib(nibName: "PostCreationTableViewCell", bundle: nil), forCellReuseIdentifier: "PostCreationTableViewCell")
        tableView.dataSource = self
        tableView.delegate = self
        tableView.isScrollEnabled = false
         tableView.reloadData()
    }
    
    @IBAction func uniAction(_ sender: Any) {
        mainImage.isHidden = true
        audiocontainerView.isHidden = true
        parentScrollView.isScrollEnabled = false
        tableView.isScrollEnabled = true
        uni = true
        tableView.reloadData()
    }
}
extension ProfileViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if uni == true {
            let cell = tableView.dequeueReusableCell(withIdentifier: "GraduateTableViewCell", for: indexPath) as! GraduateTableViewCell
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "PostCreationTableViewCell", for: indexPath) as! PostCreationTableViewCell
            return cell
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
}
