//
//  ThoughtsTableViewCell.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 7/24/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import UIKit
protocol SendingActionsFromThoughtsTVC: class {
    func ratingPopUp(id: Int, starValue: String, selectedindexToReload: Int, selectionType: GraduateDetails)
    func tappingProfileName(index: IndexPath)
    func moreBtnAction(sender: UIButton,id: Int,index: IndexPath,cell: ThoughtsTableViewCell, isOwner: Bool,postType: GraduateDetails)
    
}
class ThoughtsTableViewCell: UITableViewCell {

    @IBOutlet var moreBtn: UIButton!
    @IBOutlet weak var ratingStackView: UIStackView!
    @IBOutlet weak var ratingBtn: AnimatedButton!
    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var thoughtLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var mainImag: UIImageView!
    weak var delegate: SendingActionsFromThoughtsTVC?
    var name: String?
    var data: Comments?
    var index: IndexPath!
    var commentId: Int?
       
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        ratingBtn.setImage(UIImage(named: "star"), for: .normal)
        ratingBtn.imageView?.tintColor = UIColor.init(hexString: Appcolors.buttons)
        moreBtn.setImage(UIImage(named: "more-3"))
        moreBtn.imageView?.tintColor = UIColor.init(hexString: Appcolors.primary)
        moreBtn.imageView?.contentMode = .center
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(profileImageAction))
        mainImag.addGestureRecognizer(tapGesture)
        attributed()
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func attributed() {
      
        let text = (nameLabel.text)!
     let tap = UITapGestureRecognizer(target: self, action: #selector(self.tapLabel(gesture:)))
     nameLabel.addGestureRecognizer(tap)
     nameLabel.isUserInteractionEnabled = true
        let underlineAttriString = NSMutableAttributedString(string: text)
        let range1 = (text as NSString).range(of: name ?? "")
     underlineAttriString.addAttribute(NSAttributedString.Key.font, value: UIFont.systemFont(ofSize: 17, weight: .medium), range: range1)
        nameLabel.attributedText = underlineAttriString
    }
    @IBAction func tapLabel(gesture: UITapGestureRecognizer) {
        delegate?.tappingProfileName(index: index)
//          let text = (nameLabel.text)!
//        let termsRange = (text as NSString).range(of: name ?? "")
//
//          if gesture.didTapAttributedTextInLabel(label: nameLabel, inRange: termsRange) {
//
//          }
      }
    
    @objc func profileImageAction() {
        delegate?.tappingProfileName(index: index)
    }
    @IBAction func ratingBtnAction(_ sender: Any) {
        delegate?.ratingPopUp(id: data?.internalIdentifier ?? 0, starValue: data?.starredValue ?? "", selectedindexToReload: index.row, selectionType: .live)
    }
    @IBAction func moreBtnAction(_ sender: UIButton) {
        delegate?.moreBtnAction(sender: sender, id: commentId ?? 0, index: index, cell: ThoughtsTableViewCell(), isOwner: true, postType: .profile)
    }
}
