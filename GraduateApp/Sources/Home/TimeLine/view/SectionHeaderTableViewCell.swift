//
//  SectionHeaderTableViewCell.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 7/27/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import UIKit

class SectionHeaderTableViewCell: UITableViewCell {

    @IBOutlet weak var friendsBtn: AnimatedButton!
    @IBOutlet weak var chatBtn: AnimatedButton!
    @IBOutlet weak var postBtn: AnimatedButton!
    @IBOutlet weak var uniBtn: AnimatedButton!
    @IBOutlet weak var aboutBtn: AnimatedButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
