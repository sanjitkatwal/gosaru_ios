//
//  MediaPostUICollectionViewCell.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 6/30/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import UIKit
protocol RemovingImagesFromPost: class {
    func removingImages(index: IndexPath)
}

class MediaPostUICollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var closeBtn: UIButton!
    weak var delegate: RemovingImagesFromPost?
    var indexPath: IndexPath?
    @IBOutlet weak var mainImage: UIImageView!
    
    override func awakeFromNib() {
        closeBtn.tintColor = .red
        mainImage.layer.cornerRadius = 8
    }
    @IBAction func closeAction(_ sender: Any) {
        print(indexPath)
        delegate?.removingImages(index: indexPath!)
    }
}
