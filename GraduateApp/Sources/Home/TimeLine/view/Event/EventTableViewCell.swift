//
//  EventTableViewCell.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 7/5/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import UIKit
import DTPhotoViewerController
protocol SendingRatingFromEventTVC: class {
    func sendingRatingEventTVC(id: Int, starValue: String,selectedindexToReload: Int, selectionType: GraduateDetails)
    func moreOPtionFromEventTVC(sender: UIButton,id: Int,index: IndexPath,cell: EventTableViewCell, isOwner: Bool,postType: GraduateDetails)
    func attendIgnoreEventTVC(universityId: Int, attendState: Bool,index : IndexPath!,attendeesCount: Int)
    func fullScreenAction(vc: UIViewController)
    func pushingToProfileFromSpot(id: Int,userType: String, isOwner: Bool,postAsType: String)
    func eventRatingDetails(ratingId: Int, starredVlaue: String, starCount: Int)
    func eventFeedbackAction(commentId: Int, index: IndexPath)
    func outsideCommentEventTVC(comment: Comments?, index: IndexPath)
}
class EventTableViewCell: UITableViewCell {
    
    @IBOutlet var subheadTxtField: UILabel!
    @IBOutlet var locationStackView: UIStackView!
    @IBOutlet weak var endDateLabel: UILabel!
    @IBOutlet var addressStackView: UIStackView!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var startingTimeLabel: UILabel!
    @IBOutlet weak var startingDateLabel: UILabel!
    @IBOutlet weak var eventTypeLabel: UILabel!
    @IBOutlet weak var thoughtsNumLabel: UILabel!
    @IBOutlet weak var ratingNumLabel: UILabel!
    @IBOutlet weak var timeStampLabel: UILabel!
    @IBOutlet weak var locationImage: UIImageView!
    @IBOutlet weak var captionLabel: UILabel!
    @IBOutlet weak var sendBtn: UIButton!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var thoughtsBtn: AnimatedButton!
    @IBOutlet weak var attendingStackVIew: UIStackView!
    @IBOutlet weak var attendingLabel: UILabel!
    @IBOutlet weak var attendBtn: AnimatedButton!
    @IBOutlet weak var thoughtsStackView: UIStackView!
    @IBOutlet weak var ratingStackView: UIStackView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var moreBtn: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var imageContainerView: UIView!
    @IBOutlet weak var messageBtn: AnimatedButton!
    @IBOutlet weak var ratingBtn: AnimatedButton!
    @IBOutlet weak var footerTxtView: UITextView!
    @IBOutlet weak var proImage: UIImageView!
    weak var delegate: SendingRatingFromEventTVC?
    var starredValue: String?
    var ratingId: Int?
    var starCount: Int?
    var attentState: Bool?
    var attending = 0
    var index: IndexPath!
    var isOwnBusiness: Bool?
    var attendeesCount: Int?
    var eventId: Int?
    var presenterSource: String?
    var selectionType: GraduateDetails?
    var postData: uniMyPostData?
    var postType: GraduateDetails?
    var name: String? {
        didSet {
            attributed()
        }
    }
    var images: [UIImage] = []
    var imageDataSource: [TimeLineGalleries]?
    fileprivate var selectedImageIndex: Int = 0
    var collectionViewDataSource: Int? {
        didSet {
            DispatchQueue.main.async {
                self.collectionView.reloadData()
            }
            
            
        }
    }
    //     var imageDataSource: [UIImage] = [UIImage(named: "graduated")!,UIImage(named: "graduation")!,UIImage(named: "iconSearch")!]
    //var imageDataSource: [UIImage] = [UIImage(named: "people")!,UIImage(named: "people")!,UIImage(named: "people")!]
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.collectionView.register(UINib(nibName:"PostCreationCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "PostCreationCollectionViewCell")
        collectionView.dataSource = self
        collectionView.delegate = self
        footerTxtView.layer.cornerRadius = 8
        HelperFunctions.addingImageInbtn(ratingBtn: ratingBtn, messageBtn: messageBtn)
        HelperFunctions.addingShadowInImage(avatarImageView: proImage, cornerRadius: 6, containerView: imageContainerView)
        footerTxtView.textColor = .lightGray
        footerTxtView.text = "Add your thoughts"
        footerTxtView.delegate = self
        moreBtn.setImage(UIImage(named: "more-3"), for: .normal)
        moreBtn.imageView?.contentMode = .center
        attendingLabel.text = "\(attending)"
        attributed()
        addingActionInProImage()
        ratingDetials()
        thoughtsDetails()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    func attributed() {
        nameLabel.text = ((name ?? "") + " created a spot")
        let text = (nameLabel.text)!
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.tapLabel(gesture:)))
        nameLabel.addGestureRecognizer(tap)
        nameLabel.isUserInteractionEnabled = true
        let underlineAttriString = NSMutableAttributedString(string: text)
        let range1 = (text as NSString).range(of: name ?? "")
        underlineAttriString.addAttribute(NSAttributedString.Key.font, value: UIFont.systemFont(ofSize: 17, weight: .medium), range: range1)
        nameLabel.attributedText = underlineAttriString
    }
    func ratingDetials() {
        let tapGestures = UITapGestureRecognizer(target: self, action: #selector(ratingDetailsEventAction))
        ratingStackView.addGestureRecognizer(tapGestures)
    }
    @objc func ratingDetailsEventAction() {
        delegate?.eventRatingDetails(ratingId: ratingId ?? 0, starredVlaue: starredValue ?? "", starCount: starCount ?? 0)
    }
    func thoughtsDetails() {
        let tapGestures = UITapGestureRecognizer(target: self, action: #selector(self.thoughstDetailsAction))
        thoughtsStackView.addGestureRecognizer(tapGestures)
    }
    @objc func thoughstDetailsAction() {
        delegate?.eventFeedbackAction(commentId: ratingId ?? 0, index: index)
    }
    func addingActionInProImage() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.tapProfileImage(gesture:)))
        proImage.addGestureRecognizer(tap)
    }
    @IBAction func tapLabel(gesture: UITapGestureRecognizer) {
        
        let text = (nameLabel.text)!
        let termsRange = (text as NSString).range(of: name ?? "")
        
        if gesture.didTapAttributedTextInLabel(label: nameLabel, inRange: termsRange) {
            delegate?.pushingToProfileFromSpot(id: postData?.postingAs?.internalIdentifier ?? 0, userType: "", isOwner: postData?.isOwner ?? false, postAsType: postData?.postingAsType ?? "")
        }
    }
    @IBAction func tapProfileImage(gesture: UITapGestureRecognizer) {
        delegate?.pushingToProfileFromSpot(id: postData?.postingAs?.internalIdentifier ?? 0, userType: "", isOwner: postData?.isOwner ?? false, postAsType: postData?.postingAsType ?? "")
    }
    
    @IBAction func messageBtn(_ sender: Any) {
        delegate?.eventFeedbackAction(commentId: ratingId ?? 0, index: index)
    }
    
    @IBAction func ratingBtnAction(_ sender: Any) {
        print(selectionType!)
        delegate?.sendingRatingEventTVC(id: ratingId ?? 0, starValue: starredValue ?? "", selectedindexToReload: index.row, selectionType: selectionType!)
    }
    @IBAction func sendBtnAction(_ sender: Any) {
        if footerTxtView.text == "Add your thought" || footerTxtView.text == "" {
            return
        } else {
            if footerTxtView.text == "Add your thoughts" {
                
            } else {
                
                let param:[String : Any] = ["comment_body" : footerTxtView.text ?? ""]
                print(ratingId!,eventId!)
                ThoughtsResponse.requestToComment(id: "\(ratingId ?? 0)", param: param) { [weak self](response) in
                    guard let strongSelf = self else { return }
                    strongSelf.footerTxtView.text = "Add your thoughts"
                    strongSelf.footerTxtView.textColor = .lightGray
                    strongSelf.footerTxtView.resignFirstResponder()
                    print(response?.thoughtsData?.body!)
                    strongSelf.delegate?.outsideCommentEventTVC(comment: response?.thoughtsData, index: strongSelf.index)
                }
            }
            
        }
       
    }
    @IBAction func moreMenuBtn(_ sender: UIButton) {
        delegate?.moreOPtionFromEventTVC(sender: sender, id: ratingId ?? 0, index: index, cell: self, isOwner: isOwnBusiness ?? false, postType: postType!)
        
    }
    @IBAction func attendBtnAction(_ sender: UIButton) {
        print(attentState)
        if attentState == true {
            attentState = false
            attendBtn.setTitle("Attend", for: .normal)
            
            attendeesCount = ((attendeesCount ?? 0) - 1)
            print(attendeesCount)
            attendingLabel.text = "\((attendeesCount ?? 0))"
            delegate?.attendIgnoreEventTVC(universityId: eventId ?? 0, attendState: false, index: index, attendeesCount: attendeesCount ?? 0)
        } else {
            
            attentState = true
            attendeesCount = ((attendeesCount ?? 0) + 1)
            print(attendeesCount)
            attendingLabel.text = "\((attendeesCount ?? 0))"
            
            attendBtn.setTitle("Ignore", for: .normal)
            delegate?.attendIgnoreEventTVC(universityId: eventId ?? 0, attendState: true, index: index, attendeesCount: attendeesCount ?? 0)
        }
    }
}
extension EventTableViewCell: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imageDataSource?.count ?? 0
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PostCreationCollectionViewCell", for: indexPath) as! PostCreationCollectionViewCell
        DispatchQueue.main.async {
            cell.mainImage.setURLImage(imageURL: (GraduateUrl.imageUrl + (self.imageDataSource?[indexPath.row].filepath ?? "")))
        }
        
        return cell
    }
    
    
}
extension EventTableViewCell: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        selectedImageIndex = indexPath.row
        //
        if let cell = collectionView.cellForItem(at: indexPath) as? PostCreationCollectionViewCell {
            let viewController = SimplePhotoViewerController(referencedView: cell.mainImage, image: cell.mainImage.image)
            viewController.dataSource = self
            viewController.delegate = self
            viewController.modalPresentationStyle = .overCurrentContext
            delegate?.fullScreenAction(vc: viewController)
            
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        //        let numberOfItemsPerRow:CGFloat = 4
        //        let spacingBetweenCells:CGFloat = 16
        //        //let spacing: CGFloat = 3
        //
        //        let totalSpacing = (2 * self.spacing) + ((numberOfItemsPerRow - 1) * spacingBetweenCells) //Amount of total spacing in a row
        //
        //        if let collection = self.collectionView{
        //            let width = (collection.bounds.width - totalSpacing)/numberOfItemsPerRow
        //            return CGSize(width: width, height: width)
        //        }else{
        //            return CGSize(width: 0, height: 0)
        
        return CGSize(width: collectionView.bounds.width - 16, height: collectionView.bounds.height - 16)
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
}
extension EventTableViewCell: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView == footerTxtView {
            footerTxtView.textColor = UIColor.init(hexString: Appcolors.text)
            footerTxtView.text = ""
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if footerTxtView.text.isEmpty {
            textView.text = "Add your thoughts"
            textView.textColor = UIColor.lightGray
        }
    }
}
extension EventTableViewCell: DTPhotoViewerControllerDataSource {
    func photoViewerController(_ photoViewerController: DTPhotoViewerController, configureCell cell: DTPhotoCollectionViewCell, forPhotoAt index: Int) {
        // Set text for each item
        if let cell = cell as? CustomPhotoCollectionViewCell {
            cell.extraLabel.text = "Image no \(index + 1)"
        }
    }
    
    func photoViewerController(_ photoViewerController: DTPhotoViewerController, referencedViewForPhotoAt index: Int) -> UIView? {
        let indexPath = IndexPath(item: index, section: 0)
        if let cell = collectionView?.cellForItem(at: indexPath) as? PostCreationCollectionViewCell {
            cell.mainImage.layer.cornerRadius = 25
            return cell.mainImage
        }
        
        return nil
    }
    
    func numberOfItems(in photoViewerController: DTPhotoViewerController) -> Int {
        return imageDataSource?.count ?? 0
    }
    
    func photoViewerController(_ photoViewerController: DTPhotoViewerController, configurePhotoAt index: Int, withImageView imageView: UIImageView) {
        imageView.setURLImage(imageURL: (GraduateUrl.imageUrl + (imageDataSource?[index].filepath ?? "")))
    }
}

// MARK: DTPhotoViewerControllerDelegate
extension EventTableViewCell: SimplePhotoViewerControllerDelegate {
    func photoViewerControllerDidEndPresentingAnimation(_ photoViewerController: DTPhotoViewerController) {
        photoViewerController.scrollToPhoto(at: selectedImageIndex, animated: false)
    }
    
    func photoViewerController(_ photoViewerController: DTPhotoViewerController, didScrollToPhotoAt index: Int) {
        selectedImageIndex = index
        if let collectionView = collectionView {
            let indexPath = IndexPath(item: selectedImageIndex, section: 0)
            
            // If cell for selected index path is not visible
            if !collectionView.indexPathsForVisibleItems.contains(indexPath) {
                // Scroll to make cell visible
                collectionView.scrollToItem(at: indexPath, at: UICollectionView.ScrollPosition.bottom, animated: false)
            }
        }
    }
    
    func simplePhotoViewerController(_ viewController: SimplePhotoViewerController, savePhotoAt index: Int) {
        //        if let data = imageDataSource?[index].filepath {
        ////        for item in data {
        //
        //                    if let data = UIImage(url: URL(string: GraduateUrl.imageUrl + data)) {
        //                        images.append(data)
        //                    }
        //
        //        //            image.append(((UIImage(url: URL(string: GraduateUrl.imageUrl + (item.filepath ?? ""))))))
        //                }
    }
    
}
extension EventTableViewCell {
    func eventTVC(cell: EventTableViewCell,indexPath: IndexPath, dataSource: [uniMyPostData]?) -> EventTableViewCell{
        if dataSource?[indexPath.row].postingAsType == "business" {
             cell.name = dataSource?[indexPath.row].postingAs?.businessName
           cell.proImage.setURLImage(imageURL: (GraduateUrl.imageUrl + (dataSource?[indexPath.row].postingAs?.businessProfileImage ?? "")))
        } else if dataSource?[indexPath.row].postingAsType == "user" {
             cell.name = dataSource?[indexPath.row].postingAs?.name
            cell.proImage.setURLImage(imageURL: (GraduateUrl.imageUrl + (dataSource?[indexPath.row].postingAs?.profile?.profileImage ?? "")))
        }
       
       
        cell.captionLabel.text = dataSource?[indexPath.row].event?.descriptionValue
        if dataSource?[indexPath.row].galleries?.count == 0 {
            cell.collectionView.isHidden = true
        } else {
            cell.collectionView.isHidden = false
            cell.imageDataSource = dataSource?[indexPath.row].galleries
            cell.collectionView.reloadData()
        }
        
        cell.eventTypeLabel.text = dataSource?[indexPath.row].event?.eventType
        cell.subheadTxtField.text = dataSource?[indexPath.row].event?.eventTitle
        cell.startingDateLabel.text = dataSource?[indexPath.row].event?.eventStartDate
        cell.endDateLabel.text = dataSource?[indexPath.row].event?.eventEndDate
        //                cell.timeStampLabel.text = dataSource?.gradSpot?.data?[indexPath.row].event?.createdAt
        cell.startingTimeLabel.text = dataSource?[indexPath.row].event?.eventStartTime
        cell.attendingLabel.text = "\(dataSource?[indexPath.row].event?.attendCount ?? 0)"
        if dataSource?[indexPath.row].hasStarred == true {
            cell.ratingBtn.setImage(UIImage(named: "star3"))
        } else {
            cell.ratingBtn.setImage(UIImage(named: "star"))
        }
        cell.ratingNumLabel.text = "\(dataSource?[indexPath.row].starsCount ?? 0)"
        cell.thoughtsNumLabel.text = "\(dataSource?[indexPath.row].comments?.count ?? 0)"
        print(dataSource?[indexPath.row].starredValue)
        cell.starredValue = dataSource?[indexPath.row].starredValue
        cell.ratingId = dataSource?[indexPath.row].internalIdentifier
        cell.starCount = dataSource?[indexPath.row].starsCount
        cell.locationStackView.isHidden = true
        cell.index = indexPath
        cell.attendeesCount = dataSource?[indexPath.row].event?.attendCount
        cell.eventId = dataSource?[indexPath.row].event?.internalIdentifier
        if  dataSource?[indexPath.row].event?.isAttended == true {
            cell.attendBtn.setTitle("Ignore")
        } else {
              cell.attendBtn.setTitle("Attend")
        }
        cell.attentState = dataSource?[indexPath.row].event?.isAttended
        let newDate = HelperFunctions.timeInterval(timeAgo: dataSource?[indexPath.row].createdAt ?? "")
               cell.timeStampLabel.text = newDate
        cell.isOwnBusiness = dataSource?[indexPath.row].isOwner
        if dataSource?[indexPath.row].event?.addressString == nil {
            cell.addressStackView.isHidden = true
        } else {
            cell.addressStackView.isHidden = false
              cell.addressLabel.text = dataSource?[indexPath.row].event?.addressString
        }
        cell.postData = dataSource?[indexPath.row]
        
        cell.isOwnBusiness = dataSource?[indexPath.row].isOwner
        return cell
    }
}

