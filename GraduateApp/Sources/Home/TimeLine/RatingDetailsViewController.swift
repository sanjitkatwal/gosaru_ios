//
//  RatingDetailsViewController.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 7/26/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import UIKit
struct RatingDetailsModel {
    var name: String?
    var profileImage: String?
    var ratingNum: Int?
    var messageIcon: Bool?
    var requestIcon: Bool?
}
class RatingDetailsViewController: UIViewController {
    
   
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var segmentView: WMSegment!
    @IBOutlet weak var segmentContainerView: UIView!
    var ratingType: raitingList?
    var dataSource: RatingResponseModel? {
        didSet {
            tableView.reloadData()
        }
    }
    var ratingValue: raitingList?
    var starCount: Int?
    let notificaitonBtn = UIButton()
    var ratingNum = UserDefaults.standard.integer(forKey: "ratingValue")
    //var ratingImages
    var ratingId: Int? {
        didSet {
            getStarDetails()
        }
    }
    var starredValue: String?
    
    let cartBtn = UIButton()
    let searchBtn = UIButton()
    override func viewDidLoad() {
        super.viewDidLoad()
        //setRigthBarButtonItem()
        
        ratingType = .allRatin
        segmentView.selectorType = .bottomBar
        segmentView.buttonTitles = "All,1,2,3,4,5"

        // segmentView.buttonImages.ali
        // segmentView.
        segmentView.SelectedFont = UIFont.systemFont(ofSize: 18, weight: .semibold)

        tableView.dataSource = self
        tableView.delegate = self
        configuringSegmentBar()
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.isNavigationBarHidden = false
        currentTabBar?.setBar(hidden: true, animated: true)
    }
    override func viewDidAppear(_ animated: Bool) {
        self.title = "Ratings"
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.isNavigationBarHidden = true
        currentTabBar?.setBar(hidden: false, animated: true)
    }
    
    
    //MARK:- REQUESTING TO RATING STAR DETAILS
    func getStarDetails() {
        RatingDetailssResponse.requestToRatingDetails(ratingId: "\(ratingId ?? 0)") { [weak self](response) in
            guard let strongSelf = self else { return }
            strongSelf.dataSource = response
        }
    }
    func assigningImagesAcordingToRating() {
        if ratingNum == 1{
            //segmentView.buttonImages = ratingli
        }
        
    }
    func configuringSegmentBar() {
        print(starredValue)
        
        if starredValue == "1" {
            segmentView.buttonImages = " ,star2,smallStar,smallStar,smallStar,smallStar"
        } else if starredValue == "2" {
            segmentView.buttonImages = " ,smallStar,star2,smallStar,smallStar,smallStar"
        } else if starredValue == "3" {
            segmentView.buttonImages = " ,smallStar,smallStar,star2,smallStar,smallStar"
        } else if starredValue == "4" {
            segmentView.buttonImages = " ,smallStar,smallStar,smallStar,star2,smallStar"
        } else if starredValue == "5"{
            segmentView.buttonImages =  " ,smallStar,smallStar,smallStar,smallStar,star2"
        } else{
            segmentView.buttonImages = " ,smallStar,smallStar,smallStar,smallStar,smallStar"
        }
    }
    @IBAction func segementValueChanged(_ sender: WMSegment) {
        //        let cell = tableView.dequeueReusableCell(withIdentifier: "RatingDetailsTableViewCell") as! RatingDetailsTableViewCell
        switch sender.selectedSegmentIndex {
        case 0:
            ratingType = .allRatin
            tableView.reloadData()
        case 1:
            ratingType = .firstRating
            tableView.reloadData()
            
        case 2:
            ratingType = .secondRating
            tableView.reloadData()
        case 3:
            
            ratingType = .thirdRating
            tableView.reloadData()
            
        case 4:
            ratingType = .fourthRating
            tableView.reloadData()
        case 5:
            
            ratingType = .fifthRating
            tableView.reloadData()
        case 6:
            tableView.reloadData()
        default:
            break
        }
    }
    private func setRigthBarButtonItem() {
        notificaitonBtn.setImage(UIImage(named: "searchVector"), for: .normal)
        notificaitonBtn.frame = CGRect(x: 0, y: 0, width: 70, height: 70)
        notificaitonBtn.addTarget(self, action: #selector(notificaitonAction), for: .touchUpInside)
        cartBtn.frame = CGRect(x: 0, y: 0, width: 70, height: 70)
        cartBtn.setImage(UIImage(named: "iconCommentVector"), for: .normal)
        cartBtn.addTarget(self, action: #selector(cartAction), for: .touchUpInside)
        let searchBtn = AnimatedButton(type: .custom)
        searchBtn.frame = CGRect(x: 0, y: 0, width: 70, height: 70)
        searchBtn.setImage(UIImage(named: "iconAddVector"), for: .normal)
        searchBtn.tintColor = UIColor.white
        searchBtn.addTarget(self, action: #selector(searchAction), for: .touchUpInside)
        let addBtn = AnimatedButton(type: .custom)
        //addBtn.frame = CGRect(x: 0, y: 0, width: 70, height: 70)
        addBtn.setImage(UIImage(named: "iconAddVector"), for: .normal)
        //  addBtn.tintColor = UIColor.white
        addBtn.addTarget(self, action: #selector(searchAction2), for: .touchUpInside)
        let button = [ notificaitonBtn, cartBtn, searchBtn, addBtn]
        
        //       if self is HomeViewController {
        //         button.remove(at: 0)
        //       }
        //       if Utilities.isGuestLogin() {
        //         button = [cartBtn]
        //       }
        let stackview = UIStackView.init(arrangedSubviews: button)
        stackview.distribution = .equalSpacing
        stackview.axis = .horizontal
        //stackview.alignment = .center
        stackview.spacing = 32
        let barBtn = UIBarButtonItem(customView: stackview)
        barBtn.customView?.translatesAutoresizingMaskIntoConstraints = false
        //        barBtn.customView?.hie
        //barBtn.customView?.widthAnchor.constraint(equalToConstant: size.width).isActive = true
        
        self.navigationItem.rightBarButtonItem = barBtn
    }
    @objc func cartAction() {
        
    }
    @objc func notificaitonAction() {
    }
    @objc func searchAction() {
        
    }
    @objc func searchAction2() {
        
    }
    
}
extension RatingDetailsViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        switch ratingType {
        case .allRatin:
            return 5
        default:
            return 1
        }
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch ratingType {
        case .allRatin:
            if section == 0 {
                return dataSource?.one?.count ?? 0
            } else if section == 1 {
                return dataSource?.two?.count ?? 0
            } else if section == 2 {
                return dataSource?.three?.count ?? 0
            }else if section == 3 {
                return dataSource?.four?.count ?? 0
            }else if section == 4 {
                return dataSource?.five?.count ?? 0
            } else {
                return 0
            }
        case .firstRating:
            return dataSource?.one?.count ?? 0
        case .secondRating:
            return dataSource?.two?.count ?? 0
        case .thirdRating:
            return dataSource?.three?.count ?? 0
        case .fourthRating:
            return dataSource?.four?.count ?? 0
        case .fifthRating:
            return dataSource?.five?.count ?? 0
        default:
            return 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RatingDetailsTableViewCell", for: indexPath) as! RatingDetailsTableViewCell
        cell.imageView?.clipsToBounds = true
        cell.delegate = self
        switch ratingType {
        case .allRatin:
            if indexPath.section == 0 {
                return cell.cellforRatingDetails(cell: cell, dataSource: dataSource?.one ?? [], indexPath: indexPath)
            }
            if indexPath.section == 1 {
                return cell.cellforRatingDetails(cell: cell, dataSource: dataSource?.two ?? [] , indexPath: indexPath)
            }
            if indexPath.section == 2 {
                return cell.cellforRatingDetails(cell: cell, dataSource: dataSource?.three ?? [] , indexPath: indexPath)
            }
            if indexPath.section == 3 {
                return cell.cellforRatingDetails(cell: cell, dataSource: dataSource?.four ?? [] , indexPath: indexPath)
            }
            if indexPath.section == 4 {
                return cell.cellforRatingDetails(cell: cell, dataSource: dataSource?.five ?? [] , indexPath: indexPath)
            }
        case .firstRating:
            return cell.cellforRatingDetails(cell: cell, dataSource: dataSource?.one ?? [] , indexPath: indexPath)
        case .secondRating:
            return cell.cellforRatingDetails(cell: cell, dataSource: dataSource?.two ?? [] , indexPath: indexPath)
        case .thirdRating:
            return cell.cellforRatingDetails(cell: cell, dataSource: dataSource?.three ?? [] , indexPath: indexPath)
        case .fourthRating:
            return cell.cellforRatingDetails(cell: cell, dataSource: dataSource?.four ?? [] , indexPath: indexPath)
        case .fifthRating:
            return cell.cellforRatingDetails(cell: cell, dataSource: dataSource?.five ?? [] , indexPath: indexPath)
        default:
            return UITableViewCell()
        }
        return UITableViewCell()
    }
    func pushingToProfile() {
        let vc = UIStoryboard.moreStoryboard.instantiateUserProfileVC()
        navigationController?.pushViewController(vc, animated: true)
    }
    
}
extension RatingDetailsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
extension RatingDetailsViewController: SendingActionToRDTVC {
    func messageBtnTapped() {
        let vc = UIStoryboard.timeLineStoryboard.instantiateMessageVC()
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func profilePictureDidTap() {
        pushingToProfile()
    }
    
    func pushingTOProfileVC() {
        pushingToProfile()
    }
    func alertForSendingRequest() {
        let alert = UIAlertController(title: "Success", message: "Friend request has been successfully send!!!", preferredStyle: UIAlertController.Style.alert)
        
        // add an action (button)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        self.dismiss(animated: true, completion: nil)
        // show the alert
        self.present(alert, animated: true, completion: nil)
    }
}
extension RatingDetailsTableViewCell {
    //    func ratingDetalsTVC(cell: RatingDetailsTableViewCell, indexPath: IndexPath, dataSource: RatingResponseModel?, ratingType: raitingList) -> RatingDetailsTableViewCell {
    //        switch  ratingType{
    //        case .allRatin:
    //            cell.mainImage.setURLImage(imageURL: ((GraduateUrl.imageUrl) + (dataSource?.five?[indexPath.row].profileImage ?? "")))
    //                   cell.nameLabel.text = dataSource?.five?[indexPath.row].name
    //                   if dataSource?.five?[indexPath.row].friendshipStatus == 1 {
    //                       cell.messageBtn.isHidden = false
    //                      cell.addFriendBtn.isHidden = true
    //                   } else if dataSource?.five?[indexPath.row].friendshipStatus == nil {
    //                        cell.messageBtn.isHidden = true
    //                     cell.addFriendBtn.isHidden = true
    //                   }
    //            cell.ratingNum.text = dataSource?.five?[indexPath.row].data
    //             if indexPath.section == 0{
    //                    if dataSource?.one != nil {
    //                        return cell.cellforRatingDetails(cell: cell, dataSource: (dataSource?.one![indexPath.row])!, indexPath: indexPath)
    ////                        cell.mainImage.setURLImage(imageURL: ((GraduateUrl.imageUrl) + (dataSource?.one?[indexPath.row].profileImage ?? "")))
    ////                        cell.nameLabel.text = dataSource?.one?[indexPath.row].name
    ////                        if dataSource?.one?[indexPath.row].friendshipStatus == 1 {
    ////                            cell.messageBtn.isHidden = false
    ////                            cell.addFriendBtn.isHidden = true
    ////                        } else if dataSource?.one?[indexPath.row].friendshipStatus == nil {
    ////                             cell.messageBtn.isHidden = true
    ////                            cell.addFriendBtn.isHidden = true
    ////                        }
    ////                        cell.ratingNum.text = dataSource?.one?[indexPath.row].data
    ////                          return cell
    //                    }
    //                    }
    //                     if indexPath.section == 1{
    //                    if dataSource?.two != nil {
    //            //            if dataSource?.two?.contains(where: {$0.name == "foo"}) {
    //            //               // it exists, do something
    //            //            } else {
    //            //               //item could not be found
    //            //            }
    //            //          if let index = dataSource?.two?.firstIndex(where: { $0.userId != nil
    //            //          }) {
    //                        cell.mainImage.setURLImage(imageURL: ((GraduateUrl.imageUrl) + (dataSource?.two?[indexPath.row].profileImage ?? "")))
    //                                   cell.nameLabel.text = dataSource?.two?[indexPath.row].name
    //                        if dataSource?.two?[indexPath.row].friendshipStatus == 1 {
    //                                       cell.messageBtn.isHidden = false
    //                                         cell.addFriendBtn.isHidden = true
    //                                   } else if dataSource?.two?[indexPath.row].friendshipStatus == nil {
    //                                        cell.messageBtn.isHidden = true
    //                                        cell.addFriendBtn.isHidden = true
    //                                   }
    //                        cell.ratingNum.text = dataSource?.two?[indexPath.row].data
    //                                     return cell
    //                        //}
    //
    //
    //                    }
    //                     }
    //                     if indexPath.section == 2{
    //                      if dataSource?.three != nil {
    //                        cell.mainImage.setURLImage(imageURL: ((GraduateUrl.imageUrl) + (dataSource?.three?[indexPath.row].profileImage ?? "")))
    //                        cell.nameLabel.text = dataSource?.three?[indexPath.row].name
    //                        if dataSource?.three?[indexPath.row].friendshipStatus == 1 {
    //                            cell.messageBtn.isHidden = false
    //                              cell.addFriendBtn.isHidden = true
    //                        } else if dataSource?.three?[indexPath.row].friendshipStatus == nil {
    //                             cell.messageBtn.isHidden = true
    //                             cell.addFriendBtn.isHidden = true
    //                        }
    //                        cell.ratingNum.text = dataSource?.three?[indexPath.row].data
    //                          return cell
    //                    }
    //
    //                     }
    //                         if indexPath.section == 3{
    //                      if dataSource?.four != nil {
    //                               cell.mainImage.setURLImage(imageURL: ((GraduateUrl.imageUrl) + (dataSource?.four?[indexPath.row].profileImage ?? "")))
    //                               cell.nameLabel.text = dataSource?.four?[indexPath.row].name
    //                               if dataSource?.four?[indexPath.row].friendshipStatus == 1 {
    //                                   cell.messageBtn.isHidden = false
    //                                  cell.addFriendBtn.isHidden = true
    //                               } else if dataSource?.four?[indexPath.row].friendshipStatus == nil {
    //                                    cell.messageBtn.isHidden = true
    //                                 cell.addFriendBtn.isHidden = true
    //                               }
    //                        cell.ratingNum.text = dataSource?.four?[indexPath.row].data
    //                        return cell
    //                           }
    //                         }
    //                          if indexPath.section == 4{
    //                    if dataSource?.five != nil {
    //                               cell.mainImage.setURLImage(imageURL: ((GraduateUrl.imageUrl) + (dataSource?.five?[indexPath.row].profileImage ?? "")))
    //                               cell.nameLabel.text = dataSource?.five?[indexPath.row].name
    //                               if dataSource?.five?[indexPath.row].friendshipStatus == 1 {
    //                                   cell.messageBtn.isHidden = false
    //                                  cell.addFriendBtn.isHidden = true
    //                               } else if dataSource?.five?[indexPath.row].friendshipStatus == nil {
    //                                    cell.messageBtn.isHidden = true
    //                                 cell.addFriendBtn.isHidden = true
    //                               }
    //                        cell.ratingNum.text = dataSource?.five?[indexPath.row].data
    //                          return cell
    //                    }
    //                    }
    //        case .firstRating:
    //
    //        default:
    //            <#code#>
    //        }
    //
    //        return RatingDetailsTableViewCell()
    //    }
    func cellforRatingDetails(cell: RatingDetailsTableViewCell,dataSource: [RatingStarModel], indexPath: IndexPath) -> RatingDetailsTableViewCell{
        cell.mainImage.setURLImage(imageURL: ((GraduateUrl.imageUrl) + (dataSource[indexPath.row].profileImage ?? "")))
        cell.nameLabel.text = dataSource[indexPath.row].name
        if dataSource[indexPath.row].friendshipStatus == 1 {
            cell.messageBtn.isHidden = false
            cell.addFriendBtn.isHidden = true
        } else if dataSource[indexPath.row].friendshipStatus == nil {
            cell.messageBtn.isHidden = true
            cell.addFriendBtn.isHidden = true
        }
        cell.ratingNum.text = dataSource[indexPath.row].data
        return cell
    }
}
enum raitingList: String {
    case allRatin = " ,smallStar,smallStar,smallStar,smallStar,smallStar"
    case firstRating = " ,star2,smallStar,smallStar,smallStar,smallStar"
    case secondRating = " ,smallStar,star2,smallStar,smallStar,smallStar"
    case thirdRating = " ,smallStar,smallStar,star2,smallStar,smallStar"
    case fourthRating = " ,smallStar,smallStar,smallStar,star2,smallStar"
    case fifthRating = " ,smallStar,smallStar,smallStar,smallStar,star2"
}

