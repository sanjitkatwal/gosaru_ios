//
//  MessageListResponse.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 9/16/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import Foundation
import ObjectMapper

class MessageListResponse: DefaultResponse {
    var data: LoginResponseModel?
override func mapping(map: Map) {
  super.mapping(map: map)

}
// MARK: - RECENT CONVO request
    class func requestToMessageList(showHud: Bool = true ,completionHandler:@escaping ((MessageResponseModel?) -> Void)) {
        APIManager(urlString: GraduateUrl.messagesURL, method: .get ).handleResponse(showProgressHud: showHud,completionHandler: { (response: MessageResponseModel) in
              print(response)
                  completionHandler(response)
              }, failureBlock: {
                completionHandler(nil)
              })
      }
    
    // MARK: - RECENT CONVO USINIG ID
    class func requestToGetRecentConvoUsingId(id: Int,showHud: Bool = true ,completionHandler:@escaping ((MessageResponseModel?) -> Void)) {
        APIManager(urlString: (GraduateUrl.getBusinessRecentMessageIdURL + "\(id )"), method: .get ).handleResponse(showProgressHud: showHud,completionHandler: { (response: MessageResponseModel) in
              print(response)
                  completionHandler(response)
              }, failureBlock: {
                completionHandler(nil)
              })
      }
}
 class MessageResponseModel: DefaultResponse {

// MARK: Declaration for string constants to be used to decode and also serialize.
internal let kMessageResponseModelDataKey: String = "data"
internal let kMessageResponseModelLinksKey: String = "links"
internal let kMessageResponseModelMetaKey: String = "meta"


// MARK: Properties
public var data: [RecentMessageData]?
 var links: TimeLineLinks?
 var meta: TimeLineMeta?



// MARK: ObjectMapper Initalizers
/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/


/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/
    public override func mapping(map: Map) {
    data <- map[kMessageResponseModelDataKey]
    links <- map[kMessageResponseModelLinksKey]
    meta <- map[kMessageResponseModelMetaKey]

}
}
public class RecentMessageData: Mappable {
    public required init?(map: Map) {
        
    }
    

// MARK: Declaration for string constants to be used to decode and also serialize.
internal let kDataCreatedAtKey: String = "created_at"
internal let kDataTypeKey: String = "type"
internal let kDataLastMessageKey: String = "last_message"
internal let kDataInternalIdentifierKey: String = "id"
internal let kDataDataKey: String = "data"
internal let kDataUpdatedAtKey: String = "updated_at"
internal let kDataPartnerNameKey: String = "partner_name"
internal let kDataUsersKey: String = "users"
internal let kDataPartnerIdKey: String = "partner_id"
internal let kDataPartnerPicKey: String = "partner_pic"


// MARK: Properties
public var createdAt: String?
public var type: String?
public var lastMessage: LastMessage?
public var internalIdentifier: Int?
public var data: [Data]?
public var updatedAt: String?
public var partnerName: String?
public var users: [RecentMessageUsers]?
public var partnerId: Int?
public var partnerPic: String?



// MARK: ObjectMapper Initalizers
/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/


/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/
    public func mapping(map: Map) {
    createdAt <- map[kDataCreatedAtKey]
    type <- map[kDataTypeKey]
    lastMessage <- map[kDataLastMessageKey]
    internalIdentifier <- map[kDataInternalIdentifierKey]
    data <- map[kDataDataKey]
    updatedAt <- map[kDataUpdatedAtKey]
    partnerName <- map[kDataPartnerNameKey]
    users <- map[kDataUsersKey]
    partnerId <- map[kDataPartnerIdKey]
    partnerPic <- map[kDataPartnerPicKey]

}
}
public class LastMessage: Mappable {
    public required init?(map: Map) {
        
    }
    

// MARK: Declaration for string constants to be used to decode and also serialize.
internal let kLastMessageCreatedAtKey: String = "created_at"
internal let kLastMessageInternalIdentifierKey: String = "id"
internal let kLastMessageBodyKey: String = "body"
internal let kLastMessageSenderKey: String = "sender"
internal let kLastMessageConversationIdKey: String = "conversation_id"
internal let kLastMessageMessageIdKey: String = "message_id"
internal let kLastMessageIsSeenKey: String = "is_seen"
internal let kLastMessageUserIdKey: String = "user_id"
internal let kLastMessageUpdatedAtKey: String = "updated_at"
internal let kLastMessageIsSenderKey: String = "is_sender"
internal let kLastMessageTypeKey: String = "type"


// MARK: Properties
public var createdAt: String?
public var internalIdentifier: Int?
public var body: String?
public var sender: RecentMessageSender?
public var conversationId: Int?
public var messageId: Int?
public var isSeen: Int?
public var userId: Int?
public var updatedAt: String?
public var isSender: Int?
public var type: String?



// MARK: ObjectMapper Initalizers
/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/


/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/
    public func mapping(map: Map) {
    createdAt <- map[kLastMessageCreatedAtKey]
    internalIdentifier <- map[kLastMessageInternalIdentifierKey]
    body <- map[kLastMessageBodyKey]
    sender <- map[kLastMessageSenderKey]
    conversationId <- map[kLastMessageConversationIdKey]
    messageId <- map[kLastMessageMessageIdKey]
    isSeen <- map[kLastMessageIsSeenKey]
    userId <- map[kLastMessageUserIdKey]
    updatedAt <- map[kLastMessageUpdatedAtKey]
    isSender <- map[kLastMessageIsSenderKey]
    type <- map[kLastMessageTypeKey]

}
}
public class RecentMessageSender: Mappable {
    public required init?(map: Map) {
        
    }
    

// MARK: Declaration for string constants to be used to decode and also serialize.
internal let kSenderCreatedAtKey: String = "created_at"
internal let kSenderModeratedAtKey: String = "moderated_at"
internal let kSenderNameKey: String = "name"
internal let kSenderInternalIdentifierKey: String = "id"
internal let kSenderChatableIdKey: String = "chatable_id"
internal let kSenderUpdatedAtKey: String = "updated_at"
internal let kSenderChatabaleTypeKey: String = "chatabale_type"
internal let kSenderStatusKey: String = "status"
internal let kSenderProfileImageKey: String = "profile_image"


// MARK: Properties
public var createdAt: String?
public var moderatedAt: String?
public var name: String?
public var internalIdentifier: Int?
public var chatableId: Int?
public var updatedAt: String?
public var chatabaleType: String?
public var status: Int?
public var profileImage: String?



// MARK: ObjectMapper Initalizers
/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/

/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/
    public func mapping(map: Map) {
    createdAt <- map[kSenderCreatedAtKey]
    moderatedAt <- map[kSenderModeratedAtKey]
    name <- map[kSenderNameKey]
    internalIdentifier <- map[kSenderInternalIdentifierKey]
    chatableId <- map[kSenderChatableIdKey]
    updatedAt <- map[kSenderUpdatedAtKey]
    chatabaleType <- map[kSenderChatabaleTypeKey]
       status <- map[kSenderStatusKey]
    profileImage <- map[kSenderProfileImageKey]

}

}
public class RecentMessageUsers: Mappable {
    public required init?(map: Map) {
        
    }
    

// MARK: Declaration for string constants to be used to decode and also serialize.
internal let kUsersCreatedAtKey: String = "created_at"
internal let kUsersModeratedAtKey: String = "moderated_at"
internal let kUsersNameKey: String = "name"
internal let kUsersInternalIdentifierKey: String = "id"
internal let kUsersChatableIdKey: String = "chatable_id"
internal let kUsersUpdatedAtKey: String = "updated_at"
internal let kUsersChatabaleTypeKey: String = "chatabale_type"
internal let kUsersStatusKey: String = "status"
internal let kUsersProfileImageKey: String = "profile_image"


// MARK: Properties
public var createdAt: String?
public var moderatedAt: String?
public var name: String?
public var internalIdentifier: Int?
public var chatableId: Int?
public var updatedAt: String?
public var chatabaleType: String?
public var status: Int?
public var profileImage: String?



// MARK: ObjectMapper Initalizers
/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/


/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/
    public func mapping(map: Map) {
    createdAt <- map[kUsersCreatedAtKey]
    moderatedAt <- map[kUsersModeratedAtKey]
    name <- map[kUsersNameKey]
    internalIdentifier <- map[kUsersInternalIdentifierKey]
    chatableId <- map[kUsersChatableIdKey]
    updatedAt <- map[kUsersUpdatedAtKey]
    chatabaleType <- map[kUsersChatabaleTypeKey]
    status <- map[kUsersStatusKey]
    profileImage <- map[kUsersProfileImageKey]

}
}
