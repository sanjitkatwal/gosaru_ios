//
//  MessageViewController.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 7/24/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import UIKit

class MessageViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
   
    var id: Int? {
        didSet {
           
        }
    }
    var dataSource: MessageResponseModel? = MessageResponseModel() {
        didSet {
            tableView.reloadData()
        }
    }
     var callbackClosure: (() -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
      
        // Do any additional setup after loading the view.
        tableView.dataSource = self
        tableView.delegate = self
        if id != nil {
              gettingRecentMessageUsingId()
        } else {
             requestToRecentMessages()
        }
        print(UserDefaultsHandler.getUDValue(key: .userID))
    
       
    }
    override func viewWillAppear(_ animated: Bool) {
        self.title = "Message"
         navigationController?.isNavigationBarHidden = false
              currentTabBar?.setBar(hidden: true, animated: true)
    }
    override func viewDidAppear(_ animated: Bool) {


    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        currentTabBar?.setBar(hidden: false, animated: true)
               callbackClosure?()
        
    }
    
    //MARK:- GET RECENT CONVERSATION
    func requestToRecentMessages() {
        MessageListResponse.requestToMessageList { [weak self](response) in
            guard let strongSelf = self else { return }
            
            let data = response?.data?.filter({ $0.lastMessage?.body != nil })
            strongSelf.dataSource?.data = data
            strongSelf.tableView.reloadData()
            
        }
    }
  //MARK:- GET RECENT CONVO USING ID
    func gettingRecentMessageUsingId() {
        MessageListResponse.requestToGetRecentConvoUsingId(id: id ?? 0) { [weak self](response) in
            guard let strongSelf = self else { return }
            strongSelf.dataSource = response
        }
    }
  

}
extension MessageViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (dataSource?.data?.count ?? 0) > 0 {
            //self.tableView.backgroundView = nil
              return dataSource?.data?.count ?? 0
        } else if dataSource?.data?.count == 0{
            let noDataLabel: UILabel = UILabel(frame: CGRect(x: 0, y: 16, width: self.tableView.bounds.size.width, height: 30))
                      noDataLabel.text = "No Messages for this university"
            noDataLabel.textColor = UIColor.init(hexString: Appcolors.primary)
            noDataLabel.textAlignment = NSTextAlignment.center
                      self.tableView.backgroundView = noDataLabel
        }
        return 0
      
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MessageTableViewCell", for: indexPath) as! MessageTableViewCell
        cell.nameLabel.text = dataSource?.data?[indexPath.row].partnerName
        cell.messageLabel.text = dataSource?.data?[indexPath.row].lastMessage?.body
        if dataSource?.data?[indexPath.row].partnerPic == nil {
            cell.mainImage.image = UIImage(named: "defaultPicture")
        } else {
            cell.mainImage.setURLImage(imageURL:  (GraduateUrl.imageUrl + (dataSource?.data?[indexPath.row].partnerPic ?? "defaultPicture")))
        }
      
        return cell
    }
    
    
}
extension MessageViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let vc = UIStoryboard.moreStoryboard.instantiateChatVC()
        print(dataSource?.data?[indexPath.row].lastMessage?.conversationId)
        vc.id = dataSource?.data?[indexPath.row].lastMessage?.conversationId
        vc.name = dataSource?.data?[indexPath.row].partnerName
        vc.image = dataSource?.data?[indexPath.row].partnerPic
        vc.lastSeen = dataSource?.data?[indexPath.row].updatedAt
        vc.conversationId = dataSource?.data?[indexPath.row].lastMessage?.conversationId
        vc.userId = dataSource?.data?[indexPath.row].partnerId
        vc.chattableType = dataSource?.data?[indexPath.row].type
        navigationController?.pushViewController(vc, animated: true)
    }
}
