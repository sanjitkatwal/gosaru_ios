//
//  OtherUserProfileViewController.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 10/15/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import UIKit
import Popover
import Alamofire
import AVFoundation

struct OtherUserProfile {
    var about: UserFullDetailsResponseModel?
    var post: UniMyPostResponseModel?
    var friends: FriendsResponseModel?
    var numOfPage: Int
    var loadMore: Bool
    
}
class OtherUserProfileViewController: UIViewController {
    @IBOutlet var parentScrollView: UIScrollView!
    @IBOutlet var tableViewHeight: NSLayoutConstraint!
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var followBtn: UIButton!
    @IBOutlet var headerView: UIView!
    @IBOutlet var chatBtn: UIButton!
    @IBOutlet var friendsBtn: UIButton!
    @IBOutlet var tableview: UITableView!
    @IBOutlet var aboutBtn: UIButton!
    @IBOutlet var followersNumLabel: UILabel!
    @IBOutlet var postBtn: UIButton!
    @IBOutlet var profileImage: UIImageView!
    @IBOutlet var followersView: UIView!
    @IBOutlet var buttonsView: UIView!
     var popupTableView = UITableView(frame: CGRect(x: 0, y: 0, width: 90, height: 80))
      var popover: Popover!
    fileprivate var popoverOptions: [PopoverOption] = [
            .type(.left),
            .blackOverlayColor(UIColor(white: 0.0, alpha: 0.6))
          ]
     var texts = ["Share", "Edit", "Delete"]
    var screenheight = UIScreen.main.bounds.height
    var type: TableDataType?
    var reportId: Int?
    var userId: Int? {
        didSet {
             dataSource = OtherUserProfile(about: nil, post: nil, friends: nil, numOfPage: 1, loadMore: false)
        getOtherUserProfileDetails(pageNo: 1, isLoadMore: false, showHud: true)
        }
    }
    var dataSource: OtherUserProfile?
    var postTypes: GraduateDetails? //used for the editing the post
    var editedIndex: IndexPath! //index path that is edited
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        shadowInNameLabel()
        type = .about
        self.title = "Profiles"
        //tableview.tableHeaderView = headerView
        tableview.isScrollEnabled = false
        registerView()
       
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        currentTabBar?.setBar(hidden: true, animated: true)
              navigationController?.setNavigationBarHidden(false, animated: true)
    }
   
    
    override func viewWillLayoutSubviews() {
          super.viewWillLayoutSubviews()
        switch type {
        case .about:
            tableViewHeight.constant = tableview.contentSize.height
            highlightBtnWhenPressed(button: aboutBtn)
        case .post:
               tableViewHeight.constant = screenheight - 100
        case .friends:
             tableViewHeight.constant = screenheight - 100
        default:
            break
        }
    }
    
    func shadowInNameLabel() {
        nameLabel.layer.shadowColor = UIColor.black.cgColor
               nameLabel.layer.shadowRadius = 3.0
               nameLabel.layer.shadowOpacity = 1.0
               nameLabel.layer.shadowOffset = CGSize(width: 4, height: 4)
               nameLabel.layer.masksToBounds = false
    }
    
    func registerView() {
         tableview.register(UINib(nibName: "OtherUserProfileTableViewCell", bundle: nil), forCellReuseIdentifier: "OtherUserProfileTableViewCell")
         tableview.register(UINib(nibName: "PostCreationTableViewCell", bundle: nil), forCellReuseIdentifier: "PostCreationTableViewCell")
          tableview.register(UINib(nibName: "FriendListTableViewCell", bundle: nil), forCellReuseIdentifier: "FriendListTableViewCell")
        tableview.register(UINib(nibName: "PortFolioTableViewCell", bundle: nil), forCellReuseIdentifier: "PortFolioTableViewCell")
              tableview.register(UINib(nibName: "EventTableViewCell", bundle: nil), forCellReuseIdentifier: "EventTableViewCell")
              tableview.register(UINib(nibName: "DiscountTableViewCell", bundle: nil), forCellReuseIdentifier: "DiscountTableViewCell")
              tableview.register(UINib(nibName: "LiveTableViewCell", bundle: nil), forCellReuseIdentifier: "LiveTableViewCell")
        tableview.dataSource = self
        tableview.delegate = self
    }
    
    func highlightBtnWhenPressed(button: UIButton) {
        aboutBtn.backgroundColor = .white
             aboutBtn.setTitleColor(UIColor.init(hexString: Appcolors.buttons), for: .normal)
        postBtn.backgroundColor = .white
                   postBtn.setTitleColor(UIColor.init(hexString: Appcolors.buttons), for: .normal)
        chatBtn.backgroundColor = .white
                   chatBtn.setTitleColor(UIColor.init(hexString: Appcolors.buttons), for: .normal)
        friendsBtn.backgroundColor = .white
                   friendsBtn.setTitleColor(UIColor.init(hexString: Appcolors.buttons), for: .normal)
        button.backgroundColor = UIColor.init(hexString: Appcolors.primary)
        button.setTitleColor(.white, for: .normal)
    }
    
    //MARK:- THOUGHTS DETAILS
    func thoughtsDetails(id: Int,index: IndexPath) {
            let vc = UIStoryboard.timeLineStoryboard.instantiateThoughtsVC()
          vc.commentId = id
        vc.index = index
         navigationController?.pushViewController(vc, animated: true)
        }
    
    //MARK:- RATING DETAIL
    func ratingDetails(ratingId: Int, starredValue: String, starCount: Int) {
           let vc = UIStoryboard.timeLineStoryboard.instantiateRatingDetailsVC()
           vc.ratingId = ratingId
           vc.starredValue = starredValue
           vc.starCount = starCount
           navigationController?.pushViewController(vc, animated: true)
       }
    //MARK:- POPING RATING VC
    func poppingRatingVC(id: Int, starredVlaue: String,indexToReload: Int, selectionType: GraduateDetails) {
        let vc = HelperFunctions.ratingSelection(id: id, starredVlaue: starredVlaue, reloadingIndex: indexToReload )
        vc.delegate = self
        vc.indexToReload = indexToReload
        vc.selectedIndex = Int(starredVlaue)
        vc.selectionType = selectionType
        present(vc, animated: true, completion: nil)
    }
    
    func sharePopOver(sender: UIButton) {
        popupTableView.reloadData()
                  popupTableView.delegate = self
                  popupTableView.dataSource = self
                  popupTableView.isScrollEnabled = false
                  self.popover = Popover(options: self.popoverOptions)
                  self.popover.willShowHandler = {
                    print("willShowHandler")
                  }
                  self.popover.didShowHandler = {
                    print("didDismissHandler")
                  }
                  self.popover.willDismissHandler = {
                    print("willDismissHandler")
                  }
                  self.popover.didDismissHandler = {
                    print("didDismissHandler")
                  }
              
               self.popover.show(popupTableView, fromView: sender)
    }
    //MARK:- REUSABLE POP UP
    func popUPErrorMessageVC(message: String, popType: ErrorType) {
           let vc = UIStoryboard.timeLineStoryboard.instantiateErrorCarMsgPopVC()
           vc.popupType = popType
        vc.delegaet = self
                  vc.messageString = message
                                 let cardPopup = SBCardPopupViewController(contentViewController: vc)
                                 cardPopup.show(onViewController: self)
       }
    //MARK:- BOTTOM POP UP
    func bottomSelection(type: String, height: Int, data: CreateEventModel) {
           let popupVC = UIStoryboard.timeLineStoryboard.instantiateSelectionBottomPopUpVC()
           popupVC.delegate = self
           popupVC.textFieldType = type
           popupVC.height = CGFloat(height)
           popupVC.createEventPostAs = data
           popupVC.topCornerRadius = 15
           popupVC.presentDuration = 0.25
           popupVC.dismissDuration = 0.25
           popupVC.shouldDismissInteractivelty = true
           // popupVC.popupDelegate = self
           present(popupVC, animated: true, completion: nil)
       }
    
    //MARK:- GET USER PROFILE DETAILS API
    func getOtherUserProfileDetails(pageNo: Int, isLoadMore: Bool = false, showHud: Bool = true) {
       
        OtherUserResponse.requestToOtherUser(id: userId ?? 0) { [weak self](response) in
            guard let strongSelf = self else { return }
            strongSelf.dataSource?.about = response
            strongSelf.tableview.reloadData()
            strongSelf.nameLabel.text = strongSelf.dataSource?.about?.data?.name
            print(((GraduateUrl.imageUrl) + (strongSelf.dataSource?.about?.data?.profile?.profileImage ?? "")))
            strongSelf.profileImage.setURLImage(imageURL: ((GraduateUrl.imageUrl) + (strongSelf.dataSource?.about?.data?.profile?.profileImage ?? "")))
            strongSelf.nameLabel.text = strongSelf.dataSource?.about?.data?.name
            if strongSelf.dataSource?.about?.data?.isFollowed == true {
                strongSelf.followBtn.setTitle("Unfollow", for: .normal)
            } else {
                strongSelf.followBtn.setTitle("follow", for: .normal)
            }
            strongSelf.followersNumLabel.text = "\(strongSelf.dataSource?.about?.data?.followsCount ?? 0)" + "Followers"
            
        }
    }
    
    //MARK:- GET USER POSTS
    func getUserPosts(pageNo: Int,isLoadMore: Bool = false, showHud: Bool = true) {
        let param: [String : Any] = ["poster_type" : "user", "page" : "\(pageNo)", "poster_id" : "\(userId ?? 0)"]
        OtherUserResponse.requestToOtherUserPosts(param: param, id: userId ?? 0) { [weak self](response) in
            guard let strongSelf = self else { return }
            strongSelf.dataSource?.post = response
            strongSelf.tableview.tableFooterView = nil
                       strongSelf.tableview.tableFooterView?.isHidden = true
                       if !isLoadMore {
                           strongSelf.dataSource?.post = response
                        
                         strongSelf.tableview.reloadData()
                       } else {
                         if  var allData = response?.data {
                           print(allData)
                           for data in allData {
                               strongSelf.dataSource?.post?.data?.append(data)
                           }
                           strongSelf.tableview.reloadData()
                         }
                       }
                       print(strongSelf.dataSource?.post?.data?.count, response?.meta?.total)
                       if strongSelf.dataSource?.post?.data?.count ?? 0 < response?.meta?.total ?? 0 {
                                strongSelf.dataSource?.loadMore = true
                                strongSelf.dataSource?.numOfPage += 1
                              } else {
                                strongSelf.dataSource?.loadMore = false
                              }
                   }
        }
    
    //MARK:- GET USER FRIENDS
    func getUserFriends(pageNo: Int,isLoadMore: Bool = false, showHud: Bool = true) {
        OtherUserResponse.requestToOtherUserFriends(id: userId ?? 0, showHud: true) { [weak self](response) in
            guard let strongSelf = self else { return }
            
            strongSelf.tableview.tableFooterView = nil
                       strongSelf.tableview.tableFooterView?.isHidden = true
            print(isLoadMore)
                       if !isLoadMore {
                        strongSelf.dataSource?.friends = response
                       
                         strongSelf.tableview.reloadData()
                       } else {
                        if  var allData = response?.data {
                           print(allData)
                           for data in allData {
                            strongSelf.dataSource?.friends?.data?.append(data)
                           }
                           strongSelf.tableview.reloadData()
                         }
                       }
            print(strongSelf.dataSource?.post?.data?.count, response?.meta?.total)
                       if strongSelf.dataSource?.friends?.data?.count ?? 0 < response?.meta?.total ?? 0 {
                                strongSelf.dataSource?.loadMore = true
                                strongSelf.dataSource?.numOfPage += 1
                              } else {
                                strongSelf.dataSource?.loadMore = false
                              }
        }
    }
    
    //MARK:- FOLLOW ACTION
    @IBAction func followersAction(_ sender: Any) {
        if followBtn.titleLabel?.text == "follow" {
            FollowersResponse.requestFollowUnfollowOthers(url: (GraduateUrl.baseURL + "user/" + "\(userId ?? 0)" + "/follow")) { [weak self](response) in
                guard let strongSelf = self else { return }
                strongSelf.followersNumLabel.text = "\(((strongSelf.dataSource?.about?.data?.followsCount ?? 0) - 1))" + " Followers"
                strongSelf.followBtn.setTitle("Unfollow")
            }
        } else {
            popUPErrorMessageVC(message: "", popType: .unFollowOther)
           
        }
    }
    
    //MARK:- ABOUT ACTION
    @IBAction func aboutAction(_ sender: Any) {
        type = .about
        highlightBtnWhenPressed(button: aboutBtn)
        profileImage.isHidden = false
        followersView.isHidden = false
        buttonsView.isHidden = false
        parentScrollView.isScrollEnabled = true
        tableview.isScrollEnabled = false
        getOtherUserProfileDetails(pageNo: 1, isLoadMore: false, showHud: false)
    }
    
    //MARK: POST ACTION
    @IBAction func postAction(_ sender: Any) {
        parentScrollView.setContentOffset(.zero, animated: true)
       // tableview.isScrollEnabled = true
//        let bottomOffset = CGPoint(x: 0, y: parentScrollView.contentSize.height - parentScrollView.bounds.size.height)
//        parentScrollView.setContentOffset(bottomOffset, animated: true)
        type = .post
        highlightBtnWhenPressed(button: postBtn)
        viewWillLayoutSubviews()
        profileImage.isHidden = true
        nameLabel.isHidden = true
        followersView.isHidden = true
        buttonsView.isHidden = false
        tableview.isScrollEnabled = true
        
        let indexPath = NSIndexPath(item: 0, section: 0)
                   tableview.scrollToRow(at: indexPath as IndexPath, at: UITableView.ScrollPosition.top, animated: true)
          parentScrollView.isScrollEnabled = false
    getUserPosts(pageNo: 1, isLoadMore: false, showHud: true)
         tableview.layoutTableHeaderView()
      
    }
    
    //MARK:- CHAT ACTION
    @IBAction func chatAction(_ sender: Any) {
        type = .chats
        if dataSource?.about?.data?.friendStatus == nil {
            popUPErrorMessageVC(message: "", popType: .unableChat)
        }
        highlightBtnWhenPressed(button: chatBtn)
        let vc = UIStoryboard.timeLineStoryboard.instantiateChatVC()
        navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK:- FRIENDS ACTION
    @IBAction func friendsAction(_ sender: Any) {
        type = .friends
        highlightBtnWhenPressed(button: friendsBtn)
        viewWillLayoutSubviews()
               profileImage.isHidden = true
               nameLabel.isHidden = true
               followersView.isHidden = true
               buttonsView.isHidden = false
               tableview.isScrollEnabled = true
//                 self.tableview.setContentOffset( CGPoint(x: 0, y: 0) , animated: true)
//               let indexPath = NSIndexPath(item: 0, section: 0)
//                          tableview.scrollToRow(at: indexPath as IndexPath, at: UITableView.ScrollPosition.top, animated: true)
                 parentScrollView.isScrollEnabled = false
        getUserFriends(pageNo: 1, isLoadMore: false, showHud: true)
        
    }
    
    //MARK:- RATING POPUP
  
    func popingBottomVC(id: Int, starredValue: String, indexToReload: Int, selectionType: GraduateDetails) {
           let vc = HelperFunctions.ratingSelection(id: id, starredVlaue: starredValue, reloadingIndex: indexToReload)
                          // popupVC.popupDelegate = self
      vc.id = id
               vc.selectedIndex = Int(starredValue)
               vc.indexToReload = indexToReload
              // vc.delegate = self
             
           vc.selectionType = selectionType
vc.delegate = self
           
                           present(vc, animated: true, completion: nil)
             }
}
extension OtherUserProfileViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == popupTableView {
            
        } else {
        switch type {
        case .about:
            return 1
        case .post:
            return dataSource?.post?.data?.count ?? 0
        case .friends:
           
            return dataSource?.friends?.data?.count ?? 0
        default:
            break
        }
      
    }
          return 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == popupTableView {
            
        } else {
        switch type {
        case .about:
            let cell = tableView.dequeueReusableCell(withIdentifier: "OtherUserProfileTableViewCell", for: indexPath) as! OtherUserProfileTableViewCell
           // cell.delegate = self
            cell.descTxtView.text = dataSource?.about?.data?.profile?.summary
            cell.genderTxtField.text = dataSource?.about?.data?.profile?.gender
            cell.professionLabel.text = dataSource?.about?.data?.profile?.profession
            cell.dobLabel.text = dataSource?.about?.data?.profile?.dob
            cell.mailLabel.text = dataSource?.about?.data?.email
            cell.phoneLabel.text = dataSource?.about?.data?.phone
            cell.addressLabel.text = dataSource?.about?.data?.profile?.addressString
            cell.martialStatusLabel.text = dataSource?.about?.data?.profile?.maritalStatus
            cell.countryLabel.text = dataSource?.about?.data?.profile?.country
            return cell
    
        case .post:
            if dataSource?.post?.data?[indexPath.row].portfolio != nil {
                let cell = tableView.dequeueReusableCell(withIdentifier: "PortFolioTableViewCell", for: indexPath) as! PortFolioTableViewCell
            
                var portData = dataSource?.post?.data?.filter({ $0.portfolio != nil })
                cell.subHeaderlabel.text = dataSource?.post?.data?[indexPath.row].portfolio?.addressString
                
                cell.delegate = self
                cell.selectionType = .myPost
                return cell.portFolioCell(cell: cell, indexPath: indexPath, dataSource: dataSource?.post?.data)
            } else if dataSource?.post?.data?[indexPath.row].offer != nil {
                let cell = tableView.dequeueReusableCell(withIdentifier: "DiscountTableViewCell", for: indexPath) as! DiscountTableViewCell
                
                cell.delegate = self
                cell.selectionType = .offer
                return cell.DiscountTVC(cell: cell, indexPath: indexPath, dataSource: dataSource?.post?.data)
            } else if dataSource?.post?.data?[indexPath.row].event != nil {
                let cell = tableView.dequeueReusableCell(withIdentifier: "EventTableViewCell", for: indexPath) as! EventTableViewCell
                let eventData = dataSource?.post?.data?.filter {
                    $0.event != nil
                }
               // print(dataSource?.gradSpot?.data)
                cell.delegate = self
                cell.selectionType = .gradSpot
                cell.eventTypeLabel.text = dataSource?.post?.data?[indexPath.row].event?.eventType
                return cell.eventTVC(cell: cell, indexPath: indexPath, dataSource: dataSource?.post?.data)
            } else if dataSource?.post?.data?[indexPath.row].event == nil && dataSource?.post?.data?[indexPath.row].offer == nil && dataSource?.post?.data?[indexPath.row].portfolio == nil && dataSource?.post?.data?[indexPath.row].business == nil {
                //MARK:- CREATE POST WITH GALLERIES VIDEOS CELL
                print(dataSource?.post?.data?[indexPath.row].galleries?.count)
                
                if dataSource?.post?.data?[indexPath.row].galleries?.count != 0 {
                    
                    //MARK:- POST CREATION GALLERTIES VIDEO POST
                    if let index = dataSource?.post?.data?[indexPath.row].galleries?.firstIndex(where: { $0.mediaType == "Video"
                    }) {
                        let cell = tableView.dequeueReusableCell(withIdentifier: "LiveTableViewCell", for: indexPath) as! LiveTableViewCell
                        cell.videoType = .postVideo
                        cell.selectionType = .live
                        cell.index = indexPath
                        cell.delegate = self
                        cell.videoType = .postVideo
                        cell.starCount = dataSource?.post?.data?[indexPath.row].starsCount
                        print(dataSource?.post?.data?[indexPath.row].starredValue)
                         cell.starreVlaue = dataSource?.post?.data?[indexPath.row].starredValue
                        cell.universityId = dataSource?.post?.data?[indexPath.row].internalIdentifier
                       
                        if dataSource?.post?.data?[indexPath.row].postingAsType == "business" {
                              cell.name = dataSource?.post?.data?[indexPath.row].postingAs?.businessName
                        } else if dataSource?.post?.data?[indexPath.row].postingAsType == "user" {
                             cell.name = dataSource?.post?.data?[indexPath.row].postingAs?.name
                        }
                        if dataSource?.post?.data?[indexPath.row].hasStarred == true {
                              cell.ratingBtn.setImage(UIImage(named: "star3"))
                        } else {
                             cell.ratingBtn.setImage(UIImage(named: "star"))
                        }
                      
                        cell.selectionType = .myPost
                        cell.locationLabel.text = dataSource?.post?.data?[indexPath.row].business?.businessName
                        cell.viewersLabel.text = "\(dataSource?.post?.data?[indexPath.row].viewsCount ?? 0)"
                        //cell.videoType = .postVideo
                        cell.captionLabel.text = dataSource?.post?.data?[indexPath.row].message
                        
                        let filrurls = NSURL(string: (GraduateUrl.imageUrl + (dataSource?.post?.data?[indexPath.row].galleries?[index].filepath ?? "")))
                        
                        //                            let url = (GraduateUrl.imageUrl + (dataSource?.myPost?.data?[indexPath.row].galleries?[index].filepath ?? ""))
                        let firstAsset = AVURLAsset(url: filrurls! as URL)
                        cell.videoPlayer.videoAssets = [firstAsset]
                        //                            let newDate = HelperFunctions.getDate(input: dataSource?[indexPath.row].portfolio?.business?.createdAt ?? "")
                        // cell.timeStampLabel.text = newDate?.timeAgo()
                        //cell.timeStampLabel.text = newDate
                        cell.ratingNumLabel.text = "\(dataSource?.post?.data?[indexPath.row].starsCount ?? 0)"
                        cell.thoughtsNumLabel.text = "\(dataSource?.post?.data?[indexPath.row].comments?.count ?? 0)"
                        return cell
                        
                    }
                    //MARK: GALLERY IMAGE POST
                    if let imageIndex = dataSource?.post?.data?[indexPath.row].galleries?.firstIndex(where: { $0.mediaType == "Image"
                    }) {
                        let cell = tableView.dequeueReusableCell(withIdentifier: "PostCreationTableViewCell", for: indexPath) as! PostCreationTableViewCell
                        cell.delegate = self
                        cell.imageDataSource = dataSource?.post?.data?[indexPath.row].galleries ?? []
                        cell.selectionType = .myPost
                        return cell.postTVC(cell: cell, indexPath: indexPath, dataSource: dataSource?.post?.data)
                    }
                } else if dataSource?.post?.data?[indexPath.row].liveVideoMp4 != nil {
                    //MARK:- MYPOST CELL WITH LIVE TYPE VIDEOS
                    print("")
                    let cell = tableView.dequeueReusableCell(withIdentifier: "LiveTableViewCell", for: indexPath) as! LiveTableViewCell
                    cell.delegate = self
                    cell.index = indexPath
                    cell.selectionType = .live
                    cell.videoType = .liveVideo
                    cell.name = dataSource?.post?.data?[indexPath.row].sourceCountry as? String
                    cell.locationLabel.text = dataSource?.post?.data?[indexPath.row].business?.businessName
                    cell.viewersLabel.text = "\(dataSource?.post?.data?[indexPath.row].viewsCount ?? 0)"
//                    let fileUrl = URL(fileURLWithPath: ((GraduateUrl.imageUrl) + (dataSource?.post?.data?[indexPath.row].liveVideoMp4 ?? "")))
//                    print(fileUrl)
//                    cell.videoPlayer.videoURLs.append(fileUrl)
                    let filrurls = NSURL(string: (GraduateUrl.imageUrl + (dataSource?.post?.data?[indexPath.row].liveVideoMp4 ?? "")))
                    let firstAsset = AVURLAsset(url: filrurls! as URL)
                    cell.videoPlayer.videoAssets = [firstAsset]
                    return cell
                } else {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "PostCreationTableViewCell", for: indexPath) as! PostCreationTableViewCell
                    cell.delegate = self
                    cell.selectionType = .myPost
                    return cell.postTVC(cell: cell, indexPath: indexPath, dataSource: dataSource?.post?.data)
                }
            }
        case .friends:
            
             let cell = tableView.dequeueReusableCell(withIdentifier: "FriendListTableViewCell", for: indexPath) as! FriendListTableViewCell
             cell.delegate = self
             print(dataSource?.friends?.data?[indexPath.row].profile?.nickname)
             cell.mainImage.setURLImage(imageURL: (GraduateUrl.imageUrl + (dataSource?.friends?.data?[indexPath.row].profile?.profileImage ?? "")))
             cell.nameLabel.text = dataSource?.friends?.data?[indexPath.row].name
             cell.userid = dataSource?.friends?.data?[indexPath.row].internalIdentifier
             cell.requestStacView.isHidden = true
             
            return cell
        default:
            break
        }
        }
        return UITableViewCell()
    }
    
    
}

//MARK:- TABLE VIEW DELEGATE
extension OtherUserProfileViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == popupTableView {
            if texts[indexPath.row] == "Share" {
                let shareText = "http://test-graduation1.captainau.com.au/"
                
                let image = UIImage(named: "iconArrowDown")
                let vc = UIActivityViewController(activityItems: [shareText, image!], applicationActivities: [])
                present(vc, animated: true)
                self.popover.dismiss()
            } else if texts[indexPath.row] == "Report" {
                let vc = UIStoryboard.graduateStoryboard.instantiateReportVC()
                vc.reportId = reportId
                               vc.modalPresentationStyle = .overFullScreen
                               present(vc, animated: true, completion: nil)
                               popover.dismiss()
                           }
            }
        }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        switch type {
        case .post:
            let allData = self.dataSource
                   print(allData?.loadMore ?? false)
                   print(indexPath.row,"indexPath")
                   print(dataSource?.post?.data?.count,"dataCount")
                   guard allData?.loadMore == true, let dataCount = allData?.post?.data?.count, indexPath.row == dataCount - 1  else {return}
                     print(dataCount)
                     let lastSectionIndex = tableView.numberOfSections - 1
                     let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
                     if indexPath.section == lastSectionIndex && indexPath.row == lastRowIndex {
                       // print("this is the last cell")
                       let spinner = UIActivityIndicatorView(style: .gray)
                       spinner.startAnimating()
                       spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
                       self.tableview.tableFooterView = spinner
                       self.tableview.tableFooterView?.isHidden = false
                     }
            getUserPosts(pageNo: dataSource!.numOfPage, isLoadMore: dataSource!.loadMore, showHud: false)
        case .friends:
            let allData = self.dataSource
                              print(allData?.loadMore ?? false)
                              print(indexPath.row,"indexPath")
                              print(dataSource?.post?.data?.count,"dataCount")
                              guard allData?.loadMore == true, let dataCount = allData?.friends?.data?.count, indexPath.row == dataCount - 1  else {return}
                                print(dataCount)
                                let lastSectionIndex = tableView.numberOfSections - 1
                                let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
                                if indexPath.section == lastSectionIndex && indexPath.row == lastRowIndex {
                                  // print("this is the last cell")
                                  let spinner = UIActivityIndicatorView(style: .gray)
                                  spinner.startAnimating()
                                  spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
                                  self.tableview.tableFooterView = spinner
                                  self.tableview.tableFooterView?.isHidden = false
                                }
            getUserFriends(pageNo: dataSource!.numOfPage, isLoadMore: dataSource!.loadMore, showHud: false)
        default:
            break
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 300
    }
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if tableView == popupTableView {
            
        } else {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "LiveTableViewCell", for: indexPath) as? LiveTableViewCell {
            if let players = cell.videoPlayer {
                if players.preferredRate != 0 {
                    players.videoPlayerControls.pause()
                }
            }
        }
        }
    }
}

//MARK:- DELEGATE FROM POST
extension OtherUserProfileViewController: SendingRatingFromPCTVC {
    func moreOptionFromPostTVC(sender: UIButton, id: Int, index: IndexPath, cell: PostCreationTableViewCell, isOwner: Bool, postType: GraduateDetails) {
        reportId = id
        postTypes = postType
        editedIndex = index
        if isOwner == true {
            texts.removeAll()
            texts = ["Share", "Edit","Delete"]
            sharePopOver(sender: sender)
        } else {
            texts.removeAll()
            texts = ["Share", "Report"]
            sharePopOver(sender: sender)
        }
    }
    
    func ratingDetailFromPCTVC(ratingId: Int, starredValue: String, starCount: Int) {
        ratingDetails(ratingId: ratingId, starredValue: starredValue, starCount: starCount)
    }
    func outsideCommentPortFolioTVC(comment: Comments?, index: IndexPath) {
        dataSource?.post?.data?[index.row].comments?.append(comment!)
        tableview.reloadRows(at: [index], with: .automatic)
    }
    func pushingTOProfileVC(id: Int, userType: String, isOwner: Bool, postAsType: String) {
        pushingTOProfileVC(id: id, userType: userType, isOwner: isOwner, postasType: postAsType)
    }
//    func pushingTOProfileVC(id: Int, userType: String, isOwner: Bool) {
//        let vc = UIStoryboard.graduateStoryboard.instantiateGraduateDetailsViewController()
//              vc.userid = id
//                     navigationController?.pushViewController(vc, animated: true)
//    }
 
    
    func PCTVCRating(id: Int, starValue: String, selectedindexToReload: Int, selectionType: GraduateDetails) {
        popingBottomVC(id: id, starredValue: starValue, indexToReload: selectedindexToReload, selectionType: selectionType)
    }
    
    func feedbackActionPCTVC(commentId: Int, outsideComment: Bool, index: IndexPath) {
        if outsideComment == false {
            thoughtsDetails(id: commentId, index: index)
                      } else {
                          
                      }
    }
    
   
    
    func fullScreenImage(vc: UIViewController) {
        currentTabBar?.setBar(hidden: true, animated: true)
               present(vc, animated: true, completion: nil)
    }
    
    func thoughtsAction() {
    
    }
    
  
    
   
}

//MARK:- DELEGATE FROM RATING VC
extension OtherUserProfileViewController: SendingActionFromRatingVC {
    func sendingActionWithType(type: RatingActionTypes, id: Int, reloadingIndex: Int, ratingNum: Int, selectionType: GraduateDetails) {
        switch type {
               case .requestforRating:
                   reqestForPostRating(id: id, reloadingIndex: reloadingIndex, ratingNum: ratingNum, selectionType: .offer)
               case .reuestForUnratingPost:
                   reqestForPostUnRating(id: id, reloadingIndex: reloadingIndex, ratingNum: ratingNum, selectionType: .offer)
               }
    }
    
    //MARK:- POST RATING
    func reqestForPostRating(id: Int,reloadingIndex: Int,ratingNum: Int,selectionType: GraduateDetails) {
        var headers: [String : Any]?
        if let token = UserDefaultsHandler.getUDValue(key: UDkey.token) as? String {
            if headers == nil {
                headers = ["Authorization": "Bearer \(token)", "Accept": "application/json"]
            } else {
                headers!["Authorization"] = "Bearer \(token)"
                headers!["Accept"] = "application/json"
            }
        }
        let url = (GraduateUrl.ratePostURL + "\(id)")
         print(url,ratingNum,selectionType)
        Alamofire.request(url,
                          method: .post,
                          parameters: ["star_value": "\(ratingNum + 1)"],headers: (headers as! HTTPHeaders))
            .validate()
            .responseJSON { [weak self]response in
                guard let strongSelf = self else { return }
                if let value = response.result.value as? [String : Any] {
                    print(value)
                    switch selectionType {
                    case .gradSpot:
                        strongSelf.ratingAfterApiCallOfEvent(reloadingIndex: reloadingIndex, ratingNum: ratingNum)
                    case .offer:
                         strongSelf.ratingAfterApiCallOFOffer(reloadingIndex: reloadingIndex, ratingNum: ratingNum)
                    case .portfolio:
                         strongSelf.ratingAfterApiCallOfPortFolio(reloadingIndex: reloadingIndex, ratingNum: ratingNum)
                    default:
                        break
                    }
                    strongSelf.ratingAfterApiCallOFUserPost(reloadingIndex: reloadingIndex, ratingNum: ratingNum)
                    
                }
                
        }
        
        
    }
    
    //MARK:- POST UNRATE
        func reqestForPostUnRating(id: Int,reloadingIndex: Int,ratingNum: Int,selectionType: GraduateDetails) {
               var headers: [String : Any]?
               if let token = UserDefaultsHandler.getUDValue(key: UDkey.token) as? String {
                   if headers == nil {
                       headers = ["Authorization": "Bearer \(token)", "Accept": "application/json"]
                   } else {
                       headers!["Authorization"] = "Bearer \(token)"
                       headers!["Accept"] = "application/json"
                   }
               }
               let url = (GraduateUrl.unratePostURL + "\(id)")
               print(url,ratingNum,selectionType)
               Alamofire.request(url,method: .delete,headers: (headers as! HTTPHeaders))
                   .validate()
                   .responseJSON { [weak self]response in
                       guard let strongSelf = self else { return }
                       if let value = response.result.value as? [String : Any] {
                          
                           print(value)
                           print(reloadingIndex)
                        switch selectionType {
                        case .gradSpot:
                             strongSelf.unratingAfterApiCallOfEvent(reloadingIndex: reloadingIndex, ratingNum: ratingNum)
                        case .offer:
                             strongSelf.unratingAfterApiCallOfOffer(reloadingIndex: reloadingIndex, ratingNum: ratingNum)
                        case .portfolio:
                             strongSelf.unratingAfterApiCallOfPortFolio(reloadingIndex: reloadingIndex, ratingNum: ratingNum)
                        default:
                            break
                        }
                        strongSelf.unratingAfterApiCallOfUserPost(reloadingIndex: reloadingIndex, ratingNum: ratingNum)
                           
                       }
                       
               }
    
    
}
    func unratingAfterApiCallOfUserPost(reloadingIndex: Int, ratingNum: Int) {
           dataSource?.post?.data?[reloadingIndex].hasStarred = false
           if  dataSource?.post?.data?[reloadingIndex].starsCount ?? 0 > 0 {
               dataSource?.post?.data?[reloadingIndex].starsCount! -= 1
           } else {
               
           }
           dataSource?.post?.data?[reloadingIndex].starredValue = "\(0)"
           tableview.reloadData()
       }
       func ratingAfterApiCallOFUserPost(reloadingIndex: Int, ratingNum: Int) {
           if dataSource?.post?.data?[reloadingIndex].hasStarred == true {
              
           } else {
               
               dataSource?.post?.data?[reloadingIndex].starsCount! += 1
           }
           dataSource?.post?.data?[reloadingIndex].starredValue = "\((ratingNum + 1))"
           print(reloadingIndex)
           dataSource?.post?.data?[reloadingIndex].hasStarred = true
           tableview.reloadData()
       }
    func ratingAfterApiCallOfEvent(reloadingIndex: Int, ratingNum: Int) {
           if dataSource?.post?.data?[reloadingIndex].hasStarred == true {
             
           } else {
               
               dataSource?.post?.data?[reloadingIndex].starsCount! += 1
           }
           dataSource?.post?.data?[reloadingIndex].starredValue = "\((ratingNum + 1))"
           print(reloadingIndex)
           dataSource?.post?.data?[reloadingIndex].hasStarred = true
           tableview.reloadData()
       }
       func unratingAfterApiCallOfEvent(reloadingIndex: Int, ratingNum: Int) {
           dataSource?.post?.data?[reloadingIndex].hasStarred = false
           if  dataSource?.post?.data?[reloadingIndex].starsCount ?? 0 > 0 {
               dataSource?.post?.data?[reloadingIndex].starsCount! -= 1
           } else {
               
           }
           dataSource?.post?.data?[reloadingIndex].starredValue = "\(0)"
           tableview.reloadData()
       }
    func ratingAfterApiCallOFOffer(reloadingIndex: Int, ratingNum: Int) {
           if dataSource?.post?.data?[reloadingIndex].hasStarred == true {
               
           } else {
               
               dataSource?.post?.data?[reloadingIndex].starsCount! += 1
           }
           dataSource?.post?.data?[reloadingIndex].starredValue = "\((ratingNum + 1))"
           print(reloadingIndex)
           dataSource?.post?.data?[reloadingIndex].hasStarred = true
           tableview.reloadData()
       }
       func unratingAfterApiCallOfOffer(reloadingIndex: Int, ratingNum: Int) {
           dataSource?.post?.data?[reloadingIndex].hasStarred = false
           if  dataSource?.post?.data?[reloadingIndex].starsCount ?? 0 > 0 {
               dataSource?.post?.data?[reloadingIndex].starsCount! -= 1
           } else {
               
           }
           dataSource?.post?.data?[reloadingIndex].starredValue = "\(0)"
           tableview.reloadData()
}
    func ratingAfterApiCallOfPortFolio(reloadingIndex: Int, ratingNum: Int) {
        if dataSource?.post?.data?[reloadingIndex].hasStarred == true {
           
        } else {
            
            dataSource?.post?.data?[reloadingIndex].starsCount! += 1
        }
        dataSource?.post?.data?[reloadingIndex].starredValue = "\((ratingNum + 1))"
        print(reloadingIndex)
        dataSource?.post?.data?[reloadingIndex].hasStarred = true
        tableview.reloadData()
    }
    func unratingAfterApiCallOfPortFolio(reloadingIndex: Int, ratingNum: Int) {
        dataSource?.post?.data?[reloadingIndex].hasStarred = false
        if  dataSource?.post?.data?[reloadingIndex].starsCount ?? 0 > 0 {
            dataSource?.post?.data?[reloadingIndex].starsCount! -= 1
        } else {
            
        }
        dataSource?.post?.data?[reloadingIndex].starredValue = "\(0)"
        tableview.reloadData()
    }
}
//MARK:- DELEGATE FROM THOUGHTS VC
extension OtherUserProfileViewController: SendingDataFromThoughtsVC {
    func sendingData(index: IndexPath, thoughtsCount: Int, commentsData: [Comments]) {
        dataSource?.post?.data?[index.row].comments = commentsData
             tableview.reloadRows(at: [index], with: .automatic)
    }
}

extension OtherUserProfileViewController: ActionFromFLTVC {
    func bottomPopUp(userId: Int?) {
       bottomSelection(type: "FriendsCategoryAction", height: 150, data: CreateEventModel())
    }
}
//MARK:- DELEGATE ACTION FROM SELECTION BOTTOM
extension OtherUserProfileViewController: SendingMultipleSeclections {
    func postAsActionFromInside(value: Int?, bool: Bool, index: Int?, type: CreateSpotVCTypes, posterIds: [Int], postData: CreateEventModel) {
        
    }
    func sendingMultipleVlaues(values: [String]) {
        
    }
    
    func sendingDiscountType(value: String) {
        
    }
    
    func sendingEvnetType(value: String, type: String) {
        
    }
    
    func sendingUniCategory(value: String) {
        
    }
    
    func gettingTheSelectedValueFromTheList(value: Int?, bool: Bool, index: Int?, type: CreateSpotVCTypes, posterIds: [Int]) {
        
    }
    
    func gettingSelectedVaueFromProfileMoreBtn(value: Int, bool: Bool, index: Int) {
        
    }
    
    func gettingFullScreenCoverPic() {
        
    }
    
    func sendingMediaType(value: Int) {
        
    }
    
    func sendingUserId(id: Int, index: Int) {
        if index == 0 {
            
        } else if index == 1 {
            
        } else if index == 2 {
        
    }
    
    
}
}//MARK:- DELEGATE ACTION FROM ERORRCHATMSG
extension OtherUserProfileViewController: SendingActionFromErrorChat {
    func sendingAction() {
        FollowersResponse.requestFollowUnfollowOthers(url: (GraduateUrl.baseURL + "user/" + "\(userId ?? 0)" + "/remove-follow")) { [weak self](response) in
                        guard let strongSelf = self else { return }
                       strongSelf.followersNumLabel.text = "\(((strongSelf.dataSource?.about?.data?.followsCount ?? 0) + 1))" + " Followers"
                        strongSelf.followBtn.setTitle("follow")
                   }
    }
    
    func sendingCancelAction() {
        
    }
    
    func adctionwithType(type: ErrorType) {
        
    }
    
    
}

//MARK:- DELEGATE FROM PORT FOLIO
extension OtherUserProfileViewController: SendingRatingFromPFTVC {
    func pftvcthoughtsDetails(commentId: Int, outsideComment: Bool, index: IndexPath) {
        if outsideComment == false {
            thoughtsDetails(id: commentId, index: index)
        }
    }
    func moreOPtionFromPortTVC(sender: UIButton, id: Int, index: IndexPath, cell: PortFolioTableViewCell, isOwner: Bool, postType: GraduateDetails) {
        texts = ["Share", "Report"]
        sharePopOver(sender: sender)
    }
    func outsideCommentPostTVC(comment: Comments?, index: IndexPath) {
        
    }
    func pushingTOProfileVCFromPFTVC(id: Int, userType: String, isOwner: Bool, postAsType: String) {
      
    }
    func pftvcRating(id: Int, starValue: String, selectedindexToReload: Int, selectionType: GraduateDetails) {
        
    }
    
    func moreOPtionFromPortTVC(sender: UIButton, id: Int, index: IndexPath, cell: PortFolioTableViewCell, isOwner: Bool) {
        
    }
    
    func feedbackActionPFTVC() {
        
    }
    
    func fullScreen(vc: UIViewController) {
        
    }
  
    
    func pftvcratingDetails(ratingId: Int, starredValue: String, starCount: Int) {
       
        ratingDetails(ratingId: ratingId, starredValue: starredValue, starCount: starCount)
    }
    
 
    
    func sendingThoughts(id: Int, param: [String : Any]) {
        
    }
    
    
}
//MARK:- DELEGATE FROM
extension OtherUserProfileViewController: SedingRatingsToTimeLineVC {
    func moreOptionFromDiscountTVC(sender: UIButton, id: Int, isOwnBusiness: Bool, postType: GraduateDetails, index: IndexPath) {
        
    }
    
  
    func commentOutsideDiscountTVC(comment: Comments?, index: IndexPath) {
        
    }
    
    func pushingTOProfileVCFromDisc(id: Int, userType: String, isOwner: Bool, postAsType: String) {
        
    }
   
    
    func sendingRating(id: Int, starValue: String, selectedindexToReload: Int, selectionType: GraduateDetails) {
        
    }
   
    
    func feedbackAction(commentId: Int, outsideComment: Bool, index: IndexPath) {
        thoughtsDetails(id: commentId, index: index)
    }
    
    func fullscreen(vc: UIViewController) {
        
    }
    
    func discratingDetails(ratingId: Int, starredValue: String, starCount: Int) {
       
        ratingDetails(ratingId: ratingId, starredValue: starredValue, starCount: starCount)
    }
    
    
}

//MARK:- DELEGATE FROM EVENT
extension OtherUserProfileViewController: SendingRatingFromEventTVC {
    func moreOPtionFromEventTVC(sender: UIButton, id: Int, index: IndexPath, cell: EventTableViewCell, isOwner: Bool, postType: GraduateDetails) {
       
            reportId = id
                   if isOwner == true {
                               sharePopOver(sender: sender)
                          } else {
                              texts.removeAll()
                              texts = ["Share", "Report"]
                              sharePopOver(sender: sender)
                          }
        }
    
    
    func outsideCommentEventTVC(comment: Comments?, index: IndexPath) {
        
    }
    func pushingToProfileFromSpot(id: Int, userType: String, isOwner: Bool, postAsType: String) {
        
    }
    
    func sendingRatingEventTVC(id: Int, starValue: String, selectedindexToReload: Int, selectionType: GraduateDetails) {
    poppingRatingVC(id: id, starredVlaue: starValue, indexToReload: selectedindexToReload, selectionType: selectionType)
    }
    
   
    func attendIgnoreEventTVC(universityId: Int, attendState: Bool, index: IndexPath!, attendeesCount: Int) {
        let url: String?
               print(universityId)
               var attendeeCount = attendeesCount
               if attendState == true {
                   url = (GraduateUrl.attendEventURL + "\(universityId)")
                   
                   //attendeeCount = attendeesCount + 1
                   
               } else {
                   url = (GraduateUrl.unAttendEventURL + "\(universityId)")
                   //attendeeCount = attendeesCount - 1
               }
               
               GraduateDetailsResponse.requestToAttendIgnoreEvents(url: url ?? "") { [weak self](response) in
                   guard let strongSelf = self else { return }
                   //            let cell = strongSelf.tableView.dequeueReusableCell(withIdentifier: "EventTableViewCell", for: index) as! EventTableViewCell
                   //            cell.attendingLabel.text = "\(attendeeCount)"
                   //            strongSelf.tableView.rectForRow(at: index)
                   
               }
    }
    
    func fullScreenAction(vc: UIViewController) {
        
    }
  
    func eventRatingDetails(ratingId: Int, starredVlaue: String, starCount: Int) {
       
        ratingDetails(ratingId: ratingId, starredValue: starredVlaue, starCount: starCount)
    }
    
    func eventFeedbackAction(commentId: Int, index: IndexPath) {
        thoughtsDetails(id: commentId, index: index)
    }
    
    
}

//MARK:- DELEGATE FROM LIVE TVC
extension OtherUserProfileViewController: SendingRatingFromLiveTVC {
    func moreOptionsFromLiveTVC(sender: UIButton, index: IndexPath, isOwner: Bool, postType: GraduateDetails) {
        
    }
    
   
    func commentOutsideLiveTVC(comment: Comments?, index: IndexPath) {
        
    }
    func pushingTOProfileVC(id: Int, userType: String, isOwner: Bool, postasType: String) {
        
    }
    
    func sendingRatings(id: Int, starValue: String, selectedindexToReload: Int, selectionType: GraduateDetails) {
        
    }
    
 
    
    func feedbackActionLiveTVC(commentId: Int, index: IndexPath!) {
        thoughtsDetails(id: commentId, index: index)
    }
    
    func sendingVideoUrlForFullScreen() {
        
    }
    
    func liveRatingDetails(ratingId: Int, starredVlaue: String, starCount: Int) {
      
        ratingDetails(ratingId: ratingId, starredValue: starredVlaue, starCount: starCount)
    }
    
    
}
class SelfSizingTableView: UITableView {
    override var contentSize: CGSize {
        didSet {
            invalidateIntrinsicContentSize()
            setNeedsLayout()
        }
    }

    override var intrinsicContentSize: CGSize {
        let height = min(.infinity, contentSize.height)
        return CGSize(width: contentSize.width, height: height)
    }
}


