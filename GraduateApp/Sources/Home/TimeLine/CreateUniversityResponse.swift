//
//  CreateUniversityResponse.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 9/22/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import Foundation
import ObjectMapper
import AlamofireObjectMapper
class UniversityResponse: DefaultResponse {
    var data: LoginResponseModel?
override func mapping(map: Map) {
  super.mapping(map: map)

}
// MARK: - USER FOLLOWERS request
    class func requestToUniversityDetails(userid: Int,showHud: Bool = true ,completionHandler:@escaping ((CreateUniversityResponseModel?) -> Void)) {
        APIManager(urlString: GraduateUrl.universityDetalsURL + "\(userid)", method: .get ).handleResponse(showProgressHud: showHud,completionHandler: { (response: CreateUniversityResponseModel) in
              print(response)
                  completionHandler(response)
              }, failureBlock: {
                completionHandler(nil)
              })
      }
    
    // MARK: - UPDATING THE UNIVERSITY PROFILE
    class func requestToUpdateUniPro(coverImageName: String,profileImageName: String,coverImage: UIImage,profileImage: UIImage,params: [String : Any],userid: Int,showHud: Bool = true ,completionHandler:@escaping ((CreateUniversityResponseModel?) -> Void)) {
        PhotoUploadManager.uploadPhoto(url: GraduateUrl.universityDetalsURL + "\(userid)", method: .put, profileImage: profileImage, coverImg: coverImage, profileImageName: profileImageName, coverImageName: coverImageName, param: params, multiplePhoto: [], completionHandler: { (response: CreateUniversityResponseModel) in
        print(response)
            print(response.data?.email)
        }) {
            //failure block
        }
      }
}
 class CreateUniversityResponseModel: DefaultResponse {
   
    

// MARK: Declaration for string constants to be used to decode and also serialize.
internal let kCreateUniversityResponseModelDataKey: String = "data"


// MARK: Properties
public var data: UniversityData?
public var businessNameHidden: Bool? = true
    var success: Bool?
    var isSelected: Bool? = true



// MARK: ObjectMapper Initalizers
/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/


/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/
    public override func mapping(map: Map) {
    data <- map[kCreateUniversityResponseModelDataKey]
        success <- map["success"]

}
}

public class UniversityData: Mappable {
    public required init?(map: Map) {
        
    }
// MARK: Declaration for string constants to be used to decode and also serialize.
internal let kDataHasStarredKey: String = "has_starred"
internal let kDataBusinessTypeKey: String = "business_type"
internal let kDataBusinessCoverImageKey: String = "business_cover_image"
internal let kDataFacebookKey: String = "facebook"
internal let kDataEstablishedKey: String = "established"
internal let kDataFollowsCountKey: String = "follows_count"
internal let kDataStateKey: String = "state"
internal let kDataOpeningHourKey: String = "opening_hour"
internal let kDataOperatingRadiusKey: String = "operating_radius"
internal let kDataWebsiteLinkKey: String = "website_link"
internal let kDataViewsCountKey: String = "views_count"
internal let kDataAddressStringKey: String = "address_string"
internal let kDataInstagramKey: String = "instagram"
internal let kDataBusinessNameKey: String = "business_name"
internal let kDataDefaultMusicKey: String = "default_music"
internal let kDataInternalIdentifierKey: String = "id"
internal let kDataPhoneKey: String = "phone"
internal let kDataLonKey: String = "lon"
internal let kDataMaxCustomerHandleKey: String = "max_customer_handle"
internal let kDataStarsCountKey: String = "stars_count"
internal let kDataTwitterKey: String = "twitter"
internal let kDataAboutBusinessKey: String = "about_business"
internal let kDataStreetAddressKey: String = "street_address"
internal let kDataCountryKey: String = "country"
internal let kDataPostcodeKey: String = "postcode"
internal let kDataAbnAcnKey: String = "abn_acn"
internal let kDataEmailKey: String = "email"
internal let kDataLatKey: String = "lat"
internal let kDataStarsKey: String = "stars"
internal let kDataIsFollowedKey: String = "is_followed"
internal let kDataStarredValueKey: String = "starred_value"
internal let kDataIsOwnBusinessKey: String = "is_own_business"
internal let kDataTotalEmployeesKey: String = "total_employees"
internal let kDataSuburbKey: String = "suburb"
internal let kDataBusinessProfileImageKey: String = "business_profile_image"


// MARK: Properties
public var hasStarred: Bool = false
public var businessType: String?
public var businessCoverImage: String?
public var facebook: String?
public var established: String?
public var followsCount: Int?
public var state: String?
public var openingHour: String?
public var operatingRadius: String?
public var websiteLink:String?
public var viewsCount: String?
public var addressString: String?
public var instagram: String?
public var businessName: String?
public var defaultMusic: String?
public var internalIdentifier: Int?
public var phone: String?
public var lon: String?
public var maxCustomerHandle: String?
public var starsCount: Int?
public var twitter: String?
public var aboutBusiness: String?
public var streetAddress: String?
public var country: String?
public var postcode: String?
public var abnAcn: String?
public var email: String?
public var lat: String?
public var stars: String?
public var isFollowed: Bool = false
public var starredValue: String?
public var isOwnBusiness: Bool = false
public var totalEmployees: String?
public var suburb: String?
public var businessProfileImage: String?



// MARK: ObjectMapper Initalizers
/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/

/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/
    public func mapping(map: Map) {
    hasStarred <- map[kDataHasStarredKey]
    businessType <- map[kDataBusinessTypeKey]
    businessCoverImage <- map[kDataBusinessCoverImageKey]
    facebook <- map[kDataFacebookKey]
    established <- map[kDataEstablishedKey]
    followsCount <- map[kDataFollowsCountKey]
    state <- map[kDataStateKey]
    openingHour <- map[kDataOpeningHourKey]
    operatingRadius <- map[kDataOperatingRadiusKey]
    websiteLink <- map[kDataWebsiteLinkKey]
    viewsCount <- map[kDataViewsCountKey]
    addressString <- map[kDataAddressStringKey]
    instagram <- map[kDataInstagramKey]
    businessName <- map[kDataBusinessNameKey]
    defaultMusic <- map[kDataDefaultMusicKey]
    internalIdentifier <- map[kDataInternalIdentifierKey]
    phone <- map[kDataPhoneKey]
    lon <- map[kDataLonKey]
    maxCustomerHandle <- map[kDataMaxCustomerHandleKey]
    starsCount <- map[kDataStarsCountKey]
    twitter <- map[kDataTwitterKey]
    aboutBusiness <- map[kDataAboutBusinessKey]
    streetAddress <- map[kDataStreetAddressKey]
    country <- map[kDataCountryKey]
    postcode <- map[kDataPostcodeKey]
    abnAcn <- map[kDataAbnAcnKey]
    email <- map[kDataEmailKey]
    lat <- map[kDataLatKey]
    stars <- map[kDataStarsKey]
    isFollowed <- map[kDataIsFollowedKey]
    starredValue <- map[kDataStarredValueKey]
    isOwnBusiness <- map[kDataIsOwnBusinessKey]
    totalEmployees <- map[kDataTotalEmployeesKey]
    suburb <- map[kDataSuburbKey]
    businessProfileImage <- map[kDataBusinessProfileImageKey]

}
}
public class Business: Mappable {
    public required init?(map: Map) {
           
       }
// MARK: Declaration for string constants to be used to decode and also serialize.
internal let kBusinessBusinessTypeKey: String = "business_type"
internal let kBusinessFacebookKey: String = "facebook"
internal let kBusinessBusinessCoverImageKey: String = "business_cover_image"
internal let kBusinessStateKey: String = "state"
internal let kBusinessEstablishedKey: String = "established"
internal let kBusinessOpeningHourKey: String = "opening_hour"
internal let kBusinessOperatingRadiusKey: String = "operating_radius"
internal let kBusinessWebsiteLinkKey: String = "website_link"
internal let kBusinessInstagramKey: String = "instagram"
internal let kBusinessAddressStringKey: String = "address_string"
internal let kBusinessBusinessNameKey: String = "business_name"
internal let kBusinessModeratedAtKey: String = "moderated_at"
internal let kBusinessDefaultMusicKey: String = "default_music"
internal let kBusinessInternalIdentifierKey: String = "id"
internal let kBusinessUpdatedAtKey: String = "updated_at"
internal let kBusinessPhoneKey: String = "phone"
internal let kBusinessLonKey: String = "lon"
internal let kBusinessMaxCustomerHandleKey: String = "max_customer_handle"
internal let kBusinessTwitterKey: String = "twitter"
internal let kBusinessAboutBusinessKey: String = "about_business"
internal let kBusinessStatusKey: String = "status"
internal let kBusinessCreatedAtKey: String = "created_at"
internal let kBusinessStreetAddressKey: String = "street_address"
internal let kBusinessCountryKey: String = "country"
internal let kBusinessPostcodeKey: String = "postcode"
internal let kBusinessAbnAcnKey: String = "abn_acn"
internal let kBusinessEmailKey: String = "email"
internal let kBusinessLatKey: String = "lat"
internal let kBusinessUsernameKey: String = "username"
internal let kBusinessCanFeatureKey: String = "can_feature"
internal let kBusinessTotalEmployeesKey: String = "total_employees"
internal let kBusinessUserIdKey: String = "user_id"
internal let kBusinessSuburbKey: String = "suburb"
internal let kBusinessBusinessProfileImageKey: String = "business_profile_image"


// MARK: Properties
public var businessType: String?
public var facebook: String?
public var businessCoverImage: String?
public var state: String?
public var established: String?
public var openingHour: String?
public var operatingRadius: String?
public var websiteLink: String?
public var instagram: String?
public var addressString: String?
public var businessName: String?
public var moderatedAt: String?
public var defaultMusic: String?
public var internalIdentifier: Int?
public var updatedAt: String?
public var phone: String?
public var lon: String?
public var maxCustomerHandle: String?
public var twitter: String?
public var aboutBusiness: String?
public var status: Int?
public var createdAt: String?
public var streetAddress: String?
public var country: String?
public var postcode: String?
public var abnAcn: String?
public var email: String?
public var lat: String?
public var username: String?
public var canFeature: Int?
public var totalEmployees: String?
public var userId: Int?
public var suburb: String?
public var businessProfileImage: String?



// MARK: ObjectMapper Initalizers
/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/


/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/
    public func mapping(map: Map) {
    businessType <- map[kBusinessBusinessTypeKey]
    facebook <- map[kBusinessFacebookKey]
    businessCoverImage <- map[kBusinessBusinessCoverImageKey]
    state <- map[kBusinessStateKey]
    established <- map[kBusinessEstablishedKey]
    openingHour <- map[kBusinessOpeningHourKey]
    operatingRadius <- map[kBusinessOperatingRadiusKey]
    websiteLink <- map[kBusinessWebsiteLinkKey]
    instagram <- map[kBusinessInstagramKey]
    addressString <- map[kBusinessAddressStringKey]
    businessName <- map[kBusinessBusinessNameKey]
    moderatedAt <- map[kBusinessModeratedAtKey]
    defaultMusic <- map[kBusinessDefaultMusicKey]
    internalIdentifier <- map[kBusinessInternalIdentifierKey]
    updatedAt <- map[kBusinessUpdatedAtKey]
    phone <- map[kBusinessPhoneKey]
    lon <- map[kBusinessLonKey]
    maxCustomerHandle <- map[kBusinessMaxCustomerHandleKey]
    twitter <- map[kBusinessTwitterKey]
    aboutBusiness <- map[kBusinessAboutBusinessKey]
    status <- map[kBusinessStatusKey]
    createdAt <- map[kBusinessCreatedAtKey]
    streetAddress <- map[kBusinessStreetAddressKey]
    country <- map[kBusinessCountryKey]
    postcode <- map[kBusinessPostcodeKey]
    abnAcn <- map[kBusinessAbnAcnKey]
    email <- map[kBusinessEmailKey]
    lat <- map[kBusinessLatKey]
    username <- map[kBusinessUsernameKey]
    canFeature <- map[kBusinessCanFeatureKey]
    totalEmployees <- map[kBusinessTotalEmployeesKey]
    userId <- map[kBusinessUserIdKey]
    suburb <- map[kBusinessSuburbKey]
    businessProfileImage <- map[kBusinessBusinessProfileImageKey]

}
}

