//
//  RatingReponse.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 10/5/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import Foundation
import ObjectMapper
class RatingResponse: DefaultResponse {
    var data: LoginResponseModel?
    var success: Bool?
    var occupationList: [String] = []
override func mapping(map: Map) {
  super.mapping(map: map)
success <- map["success"]
}
// MARK: - SEND RATING OF THE POST
    class func requestToRatePost(id: Int,ratingNum: Int,showHud: Bool = false ,completionHandler:@escaping ((RatingResponse?) -> Void)) {
        let param: [String : Any] = ["star_value" : "\(ratingNum)"]
        APIManager(urlString: (GraduateUrl.ratePostURL + "\(id)"), parameters: param,method: .post).handleResponse(showProgressHud: showHud,completionHandler: { (response: RatingResponse) in
              print(response)
                  completionHandler(response)
              }, failureBlock: {
                completionHandler(nil)
              })
      }
    
    // MARK: - SEND UnRATING OF THE POST
    class func requestToUnRatePost(id: Int,showHud: Bool = false ,completionHandler:@escaping ((RatingResponse?) -> Void)) {
        APIManager(urlString: (GraduateUrl.unratePostURL + "\(id)") ,method: .delete).handleResponse(showProgressHud: showHud,completionHandler: { (response: RatingResponse) in
              print(response)
                  completionHandler(response)
              }, failureBlock: {
                completionHandler(nil)
              })
      }
}
