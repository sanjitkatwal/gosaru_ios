//
//  SelectionBottomPopUPViewController.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 6/30/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import UIKit
struct CreateEventModel {
    var data: [EventModel]?
    var isSelected: Bool!
    
}

struct EventModel {
    var name: String?
       var profileImage: String?
       var id: Int?
    var isSelected: Bool?
}
protocol SendingMultipleSeclections: class {
    func sendingMultipleVlaues(values: [String])
    func sendingDiscountType(value: String)
    func sendingEvnetType(value: String, type: String)
    func sendingUniCategory(value: String)
    func gettingTheSelectedValueFromTheList(value: Int?, bool: Bool, index: Int?,type: CreateSpotVCTypes, posterIds: [Int])
    func gettingSelectedVaueFromProfileMoreBtn(value: Int, bool: Bool, index: Int)
    func gettingFullScreenCoverPic()
    func sendingMediaType(value: Int)
    func sendingUserId(id: Int, index: Int)
    func postAsActionFromInside(value: Int?, bool: Bool, index: Int?,type: CreateSpotVCTypes, posterIds: [Int],postData: CreateEventModel)

}
class SelectionBottomPopUPViewController: BottomPopupViewController {
    @IBOutlet var universityProImage: UIImageView!
    @IBOutlet var universityNameLabel: UILabel!
    @IBOutlet weak var topViewBtn: UIButton!
    @IBOutlet weak var topView: UIView!
    @IBOutlet var footerView: UIView!
    @IBOutlet weak var tableView: UITableView!
    // type: bottomPopUpType?
     var numArray: [Int] = []
    var postersIdArray: [Int] = []
    var createEventPostAs: CreateEventModel?
    var topViewseleciton: Bool? = false
    var friendsCategoryAction = ["Unfriend","Block","Unfollow"]
    var genderData = ["Male", "Female", "Others"]
    var statusData = ["Single", "In Relationship", "Married", "Divorced", "Widowed"]
    var graduationType = ["Graduation1","Graduation2","Graduation3","Graduation4","Graduation5","Graduation6","Graduation7","Graduation8","Graduation9","Graduation10"]
    var professionData: [String]?
    var stringDataSource: [String]?
    var profileBtnData = ["Change Profile Picture", "Edit Default Music"]
    var profileBtnReloadedData = ["Take Picture", "Select from gallery"]
    var textFieldType: String?
    var eventData = ["Event1","Event2","Event3","Event4","Event5","Event6"]
    var categoryData = ["University1","University2","University1","University2","University1","University2","University1","University2", "University1","University2","University1","University2","University1","University2","University1","University2"]
    var addMediaData = ["Take Picture","Select Images","Select Video"]
    var musicDataSelection = ["Select Music","Delete Default Music"]
    var graduateDetailsMore = ["Change Cover Picture","Show Cover Picture"]
    var profileChangeData = ["Change Profile Picture", "Show Profile Picture"]
//    var postAsDataDealFromOutsiDe: GraduateUniversityResponseModel?{
//        didSet {
//            dealPostAsData = PostAsModel(data: postAsDataDealFromOutsiDe, selectedCell: )
//        }
//    }
    var dealPostAsData: CreateDealOutside?
    var cellSelection: Bool? {
        didSet {
            tableView.reloadData()
        }
    }
    var postAsData: CreateUniversityResponseModel?
    var selectedIndex = [Int]()
    var discountData = ["Discount percentage", "Discount amount"]
    var deselectedIndex: NSInteger!
    weak var delegate: SendingMultipleSeclections?
    var dataSource = [SelectionModel]()
    var filteredItems: [String]? = []
    var items: [String]? = []
    var finalItems: [String]? = []
    var height: CGFloat?
    var topCornerRadius: CGFloat?
    var presentDuration: Double?
    var dismissDuration: Double?
    var shouldDismissInteractivelty: Bool?
    var userId: Int?
    var profileImage: String?
    var uniName: String?
    var selectedCell: Int!
    var universityList: GraduateUniversityResponseModel?
    override func viewDidLoad() {
        super.viewDidLoad()
        tableViewSetup()
        //tableView.dataSource = self
        print(textFieldType)
        if textFieldType == "Post As" {
            print(uniName,profileImage)
            universityProImage.layer.cornerRadius = 4
            universityNameLabel.text = uniName
            universityProImage.setURLImage(imageURL: ((GraduateUrl.imageUrl + (profileImage ?? ""))))
            topView.isHidden = false
        }  else if textFieldType == "CreateSpotFromOutside" || textFieldType == "CreatePostFromOutside" {
            topView.isHidden = false
            universityNameLabel.text = UserDefaultsHandler.getUDValue(key: .fullName) as? String
            universityProImage.setURLImage(imageURL: ((GraduateUrl.imageUrl) + (UserDefaultsHandler.getUDValue(key: .profileImage) as! String)))
            print(dealPostAsData?.selfAdPoster)
            if dealPostAsData?.selfAdPoster == false {
                 topViewBtn.setImage(UIImage(named: "iconUnchecked"))
            } else {
                 topViewBtn.setImage(UIImage(named: "iconChecked"))
            }
           
        } else if textFieldType == "CreateEventPostAs" {
            topView.isHidden = true
            tableView.tableHeaderView = nil
            print(createEventPostAs)
            tableView.reloadData()
        } else {
            topView.isHidden = true
        }
        
        // Do any additional setup after loading the view.
       
        tableView.reloadData()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        var ids = [Int]()
        if textFieldType == "CreateDealFromInside" {
            if postAsData?.isSelected == true {
                delegate?.gettingTheSelectedValueFromTheList(value: postAsData?.data?.internalIdentifier ?? 0, bool: false, index: 0, type: .createDeal, posterIds: [])
            } else {
                delegate?.gettingTheSelectedValueFromTheList(value: nil, bool: false, index: 0, type: .createDeal, posterIds: [])
            }
            
        } else if textFieldType == "CreateEventPostAs" {
            createEventPostAs?.data?.forEach() {
                if $0.isSelected == true {
                   
                    ids.append($0.id ?? 0)
                }
            }
                delegate?.postAsActionFromInside(value: 0, bool: false, index: 0, type: .createDeal, posterIds: ids, postData: createEventPostAs ?? CreateEventModel())
            
        }
    }
    func tableViewSetup() {
        
        tableView?.estimatedRowHeight = 25
        tableView?.rowHeight = UITableView.automaticDimension
        tableView?.allowsMultipleSelection = true
        tableView?.dataSource = self
        tableView?.delegate = self
        //tableView.tableFooterView = footerView
        tableView?.separatorStyle = .none
        dataSource = [SelectionModel(title: "Item1", selected: false),SelectionModel(title: "item2", selected: false), SelectionModel(title: "Item3", selected: false), SelectionModel(title: "Item4", selected: false), SelectionModel(title: "Item5", selected: false), SelectionModel(title: "Item6", selected: false)]
        
        
        tableView.register(UINib(nibName: "ImageAndLabelTableViewCell", bundle: nil), forCellReuseIdentifier: "ImageAndLabelTableViewCell")
        //               viewModel.didToggleSelection = { [weak self] hasSelection in
        //                   self?.nextButton?.isEnabled = hasSelection
        //               }
    }
    @IBAction func topviewAction(_ sender: Any) {
        if dealPostAsData?.selfAdPoster == true {
            topViewBtn.setImage(UIImage(named: "iconUnchecked"), for: .normal)
            topViewseleciton = false
//            delegate?.gettingTheSelectedValueFromTheList(value: UserDefaultsHandler.getUDValue(key: .userID) as! Int, bool: false, index: nil, type: .createSpotFromOutside, posterIds: postersIdArray)
            delegate?.sendingUniCategory(value: "false")
        } else {
            topViewBtn.setImage(UIImage(named: "iconChecked"), for: .normal)
            topViewseleciton = true
//            delegate?.gettingTheSelectedValueFromTheList(value: UserDefaultsHandler.getUDValue(key: .userID) as! Int, bool:  true, index: nil, type: .createSpotFromOutside, posterIds: postersIdArray)
            delegate?.sendingUniCategory(value: "true")
        }
    }
    
    override var popupHeight: CGFloat { return height ?? CGFloat(300) }
    
    override var popupTopCornerRadius: CGFloat { return topCornerRadius ?? CGFloat(10) }
    
    override var popupPresentDuration: Double { return presentDuration ?? 1.0 }
    
    override var popupDismissDuration: Double { return dismissDuration ?? 1.0 }
    
    override var popupShouldDismissInteractivelty: Bool { return shouldDismissInteractivelty ?? true }
    
    override var popupDimmingViewAlpha: CGFloat { return BottomPopupConstants.kDimmingViewDefaultAlphaValue }
    
    @IBAction func okAction(_ sender: Any) {
        //MARK: SENDING MULTIPLE VALUES TO THE CREATESPOTVC
        delegate?.sendingMultipleVlaues(values: finalItems ?? [""])
    }
    @IBAction func closeAction(_ sender: Any) {
        dismiss(animated: true) {
            
        }
        
    }
}
extension SelectionBottomPopUPViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if textFieldType == "Create Deal" {
            return discountData.count
        } else if textFieldType == "Event" {
            return eventData.count
        } else if textFieldType == "Category" {
            return categoryData.count
        } else if textFieldType == "ProfileMoreBtn" {
            return profileBtnData.count
        }  else if textFieldType == "ProfileMoreBtnReloaded" || textFieldType == "Upload Photo In Report"{
            return profileBtnReloadedData.count
        }  else if textFieldType == "genderTextField" {
            return genderData.count
        }  else if textFieldType == "professionTextField" {
            return stringDataSource?.count ?? 0
        } else if textFieldType == "statusTextField" {
            return statusData.count
        } else if textFieldType == "Graduate Details More Selection" {
            return graduateDetailsMore.count
        } else if textFieldType == "FriendsCategoryAction" {
            return friendsCategoryAction.count
        } else if textFieldType == "Add Media" {
            return addMediaData.count
        } else if textFieldType == "Notification Delete" {
            return 1
        } else if textFieldType == "Music Selection" {
            return musicDataSelection.count
        } else if textFieldType == "Change Profile Picture" {
            return profileChangeData.count
        }  else if textFieldType == "ProfilePictureSelection" {
                  return profileBtnReloadedData.count
        } else if textFieldType == "Post As" {
            return 0
        } else if textFieldType == "DiscountPercentage" {
           
            for n in 0...20 {
                numArray.append(n * 5)
            }
            return numArray.count
        } else if textFieldType == "CreateEventPostAs" {
            print(createEventPostAs?.data?.count)
            return createEventPostAs?.data?.count ?? 0
           // return dealPostAsData?.data?.data?.count ?? 0
        }  else if textFieldType == "CreatePostFromOutside" || textFieldType == "CreateDealFromOutside"{
//            if let data = dealPostAsData as? GraduateUniversityResponseModel {
//                return data.data?.count ?? 0
//            }
           
            return dealPostAsData?.data?.data?.count ?? 0
           
               } else if textFieldType == "CreateSpotFromOutside" {
//                if let data = dealPostAsData as? GraduateUniversityResponseModel {
//                    return data.data?.count ?? 0
//                }
                return dealPostAsData?.data?.data?.count ?? 0
               
               } else if textFieldType == "CreateDealFromInside" {
                return 1
               } else if textFieldType == "GraduationType" {
                return graduationType.count
               }
    
    return dataSource.count
}

func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    if textFieldType == "Create Deal" {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DiscountTypeTableViewCell", for: indexPath) as! DiscountTypeTableViewCell
        cell.titleLabel.text = discountData[indexPath.row]
        
        return cell
    } else if textFieldType == "Event" {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DiscountTypeTableViewCell", for: indexPath) as! DiscountTypeTableViewCell
        cell.titleLabel.text = eventData[indexPath.row]
        return cell
    } else if textFieldType == "GraduationType" {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DiscountTypeTableViewCell", for: indexPath) as! DiscountTypeTableViewCell
        cell.titleLabel.text = graduationType[indexPath.row]
        return cell
    } else if textFieldType == "Category" {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DiscountTypeTableViewCell", for: indexPath) as! DiscountTypeTableViewCell
        cell.titleLabel.text = categoryData[indexPath.row]
        return cell
    }  else if textFieldType == "ProfileMoreBtn" {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DiscountTypeTableViewCell", for: indexPath) as! DiscountTypeTableViewCell
        cell.titleLabel.text = profileBtnData[indexPath.row]
        
        return cell
        
    } else if textFieldType == "ProfileMoreBtnReloaded" || textFieldType == "Upload Photo In Report" {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DiscountTypeTableViewCell", for: indexPath) as! DiscountTypeTableViewCell
        cell.titleLabel.text = profileBtnReloadedData[indexPath.row]
        
        return cell
        
    } else if textFieldType == "genderTextField" {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DiscountTypeTableViewCell", for: indexPath) as! DiscountTypeTableViewCell
        cell.titleLabel.text = genderData[indexPath.row]
        
        return cell
        
    } else if textFieldType == "CreateSpotFromOutside"{
        let cell = tableView.dequeueReusableCell(withIdentifier: "PostAsTableViewCell", for: indexPath) as! PostAsTableViewCell
        print(dealPostAsData?.data?.data?[indexPath.row].businessName)
        cell.titleLabel.text = dealPostAsData?.data?.data?[indexPath.row].businessName
        cell.profileImage.setURLImage(imageURL: ((GraduateUrl.imageUrl) + (dealPostAsData?.data?.data?[indexPath.row].businessCoverImage ?? "")))
        if dealPostAsData?.data?.data?[indexPath.row].isSelected == true {
             cell.checkMarkbtn.setImage(UIImage(named: "iconChecked"))
        } else {
             cell.checkMarkbtn.setImage(UIImage(named: "iconUnchecked"))
        }
        
        return cell
        
    }  else if textFieldType == "CreatePostFromOutside" || textFieldType == "CreateDealFromOutside"{
//        let cell = tableView.dequeueReusableCell(withIdentifier: "PostAsTableViewCell", for: indexPath) as! PostAsTableViewCell
//        print(dealPostAsData?.data?.data?[indexPath.row].businessName)
//        cell.titleLabel.text = dealPostAsData?.data?.data?[indexPath.row].businessName
//        cell.profileImage.setURLImage(imageURL: ((GraduateUrl.imageUrl) + (dealPostAsData?.data?.data?[indexPath.row].businessCoverImage ?? "")))
//        if indexPath.row == dealPostAsData?.selectedCell {
//             cell.checkMarkbtn.setImage(UIImage(named: "iconChecked"))
//        } else {
//             cell.checkMarkbtn.setImage(UIImage(named: "iconUnchecked"))
//        }
//
//        return cell
        let cell = tableView.dequeueReusableCell(withIdentifier: "PostAsTableViewCell", for: indexPath) as! PostAsTableViewCell
        print(dealPostAsData?.data?.data?[indexPath.row].businessName)
        cell.titleLabel.text = dealPostAsData?.data?.data?[indexPath.row].businessName
        cell.profileImage.setURLImage(imageURL: ((GraduateUrl.imageUrl) + (dealPostAsData?.data?.data?[indexPath.row].businessCoverImage ?? "")))
        if dealPostAsData?.data?.data?[indexPath.row].isSelected == true {
             cell.checkMarkbtn.setImage(UIImage(named: "iconChecked"))
        } else {
             cell.checkMarkbtn.setImage(UIImage(named: "iconUnchecked"))
        }
        
        return cell
    } else if textFieldType == "professionTextField" {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DiscountTypeTableViewCell", for: indexPath) as! DiscountTypeTableViewCell
        cell.titleLabel.text = stringDataSource?[indexPath.row]
        
        return cell
        
    } else if textFieldType == "statusTextField" {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DiscountTypeTableViewCell", for: indexPath) as! DiscountTypeTableViewCell
        cell.titleLabel.text = statusData[indexPath.row]
        
        return cell
        
    }else if textFieldType == "Graduate Details More Selection" {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DiscountTypeTableViewCell", for: indexPath) as! DiscountTypeTableViewCell
        cell.titleLabel.text = graduateDetailsMore[indexPath.row]
        
        return cell
        
    } else if textFieldType == "FriendsCategoryAction" {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DiscountTypeTableViewCell", for: indexPath) as! DiscountTypeTableViewCell
        cell.titleLabel.text = friendsCategoryAction[indexPath.row]
        cell.titleLabel.textAlignment = .center
        
        return cell
        
    }  else if textFieldType == "Add Media" {
           let cell = tableView.dequeueReusableCell(withIdentifier: "DiscountTypeTableViewCell", for: indexPath) as! DiscountTypeTableViewCell
        cell.titleLabel.text = addMediaData[indexPath.row]
           cell.titleLabel.textAlignment = .center
           return cell
           
       }  else if textFieldType == "ProfilePictureSelection" {
           let cell = tableView.dequeueReusableCell(withIdentifier: "DiscountTypeTableViewCell", for: indexPath) as! DiscountTypeTableViewCell
        cell.titleLabel.text = profileBtnReloadedData[indexPath.row]
           cell.titleLabel.textAlignment = .center
           return cell
           
       } else if textFieldType == "Notification Delete" {
                 let cell = tableView.dequeueReusableCell(withIdentifier: "DiscountTypeTableViewCell", for: indexPath) as! DiscountTypeTableViewCell
              cell.titleLabel.text = "Delete"
                 cell.titleLabel.textAlignment = .center
                 return cell
                 
    }   else if textFieldType == "Change Profile Picture"{
        let cell = tableView.dequeueReusableCell(withIdentifier: "DiscountTypeTableViewCell", for: indexPath) as! DiscountTypeTableViewCell
        cell.titleLabel.text = profileChangeData[indexPath.row]
                             cell.titleLabel.textAlignment = .center
                             return cell
        } else if textFieldType == "Music Selection" {
                             let cell = tableView.dequeueReusableCell(withIdentifier: "DiscountTypeTableViewCell", for: indexPath) as! DiscountTypeTableViewCell
        cell.titleLabel.text = musicDataSelection[indexPath.row]
                             cell.titleLabel.textAlignment = .center
                             return cell
                             
    } else if textFieldType == "Post As"{
        return UITableViewCell()
    } else if textFieldType == "DiscountPercentage"{
           let cell = tableView.dequeueReusableCell(withIdentifier: "DiscountTypeTableViewCell", for: indexPath) as! DiscountTypeTableViewCell
        cell.titleLabel.text = "\(numArray[indexPath.row])"
        return cell
    } else if textFieldType == "CreateEventPostAs" {
      print(createEventPostAs?.data?[indexPath.row].id)
         let cell = tableView.dequeueReusableCell(withIdentifier: "PostAsTableViewCell", for: indexPath) as! PostAsTableViewCell
        cell.titleLabel.text = createEventPostAs?.data?[indexPath.row].name
        cell.profileImage.setURLImage(imageURL: ((GraduateUrl.imageUrl + (createEventPostAs?.data?[indexPath.row].profileImage ?? ""))))
//        if createEventPostAs?[indexPath.row].selected == true {
//            cell.checkMarkbtn.setImage(UIImage(named: "iconChecked"))
//        } else {
//             cell.checkMarkbtn.setImage(UIImage(named: "iconUnchecked"))
//        }
        if createEventPostAs?.data?[indexPath.row].isSelected == true {
        cell.checkMarkbtn.setImage(UIImage(named: "iconChecked"))
        } else {
              cell.checkMarkbtn.setImage(UIImage(named: "iconUnchecked"))
        }
        return cell
    }else if textFieldType == "CreateDealFromInside"{
        let cell = tableView.dequeueReusableCell(withIdentifier: "PostAsTableViewCell", for: indexPath) as! PostAsTableViewCell
        print(postAsData)
        print(postAsData?.data?.businessName)
        cell.titleLabel.text = postAsData?.data?.businessName
        cell.profileImage.setURLImage(imageURL: ((GraduateUrl.imageUrl + (postAsData?.data?.businessProfileImage ?? ""))))
        if postAsData?.isSelected == false {
            cell.checkMarkbtn.setImage(UIImage(named: "iconUnchecked"), for: .normal)
        } else {
            cell.checkMarkbtn.setImage(UIImage(named: "iconChecked"), for: .normal)
        }
       
        return cell
    }else {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PostAsTableViewCell", for: indexPath) as! PostAsTableViewCell
        
        if dataSource[indexPath.row].selected == true {
            
            cell.checkMarkbtn.setImage(UIImage(named: "iconChecked"), for: .normal)
        } else {
            
            cell.checkMarkbtn.setImage(UIImage(named: "iconUnchecked"), for: .normal)
            
        }
        cell.titleLabel.text = dataSource[indexPath.row].title
        
        deselectedIndex = nil
        return cell
        
        
    }
}


}
extension SelectionBottomPopUPViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if textFieldType == "Create Deal" {
            delegate?.sendingDiscountType(value: discountData[indexPath.row])
            self.dismiss(animated: true, completion: nil)
        }  else if textFieldType == "Event"{
            delegate?.sendingEvnetType(value: eventData[indexPath.row], type: "")
            self.dismiss(animated: true, completion: nil)
        }   else if textFieldType == "GraduationType"{
            delegate?.sendingEvnetType(value: graduationType[indexPath.row],type: "")
            self.dismiss(animated: true, completion: nil)
        }else if textFieldType == "Notification Delete"{
            delegate?.sendingDiscountType(value: "Delete")
            self.dismiss(animated: true, completion: nil)
        }else if textFieldType == "Category"{
            delegate?.sendingUniCategory(value: categoryData[indexPath.row])
            self.dismiss(animated: true, completion: nil)
        }  else if textFieldType == "ProfileMoreBtn"{
            if indexPath.row == 0 {
                textFieldType = "ProfileMoreBtnReloaded"
                           tableView.reloadData()
            } else {
                textFieldType = "Music Selection"
                tableView.reloadData()
            }
           
        } else if textFieldType == "ProfileMoreBtnReloaded"{
            self.dismiss(animated: true, completion: nil)
            delegate?.gettingSelectedVaueFromProfileMoreBtn(value: indexPath.row, bool: false, index: 0)
        }   else if textFieldType == "CreatePostFromOutside" {
            dealPostAsData?.selectedCell = indexPath.row
//                   // selectedCell = indexPath.row
//                              tableView.reloadData()
//            delegate?.gettingTheSelectedValueFromTheList(value: dealPostAsData?.data?.data?[indexPath.row].internalIdentifier ?? 0, bool: false, index: indexPath.row, type: .createDealFromOutside, posterIds: [])
//            self.dismiss(animated: true, completion: nil)
           // dealPostAsData?.selectedCell = indexPath.row
                   // selectedCell = indexPath.row
if  dealPostAsData?.data?.data?[indexPath.row].isSelected == true {
 
  if let index = postersIdArray.firstIndex(of: dealPostAsData?.data?.data?[indexPath.row].internalIdentifier ?? 0) {
      postersIdArray.remove(at: index)
  }
  dealPostAsData?.data?.data?[indexPath.row].isSelected = false
} else {
  postersIdArray.append(dealPostAsData?.data?.data?[indexPath.row].internalIdentifier ?? 0)
 
dealPostAsData?.data?.data?[indexPath.row].isSelected = true
}
                              tableView.reloadData()
delegate?.gettingTheSelectedValueFromTheList(value: dealPostAsData?.data?.data?[indexPath.row].internalIdentifier ?? 0, bool: Bool(), index: indexPath.row, type: .createPostFromOutside, posterIds: postersIdArray)
                  
        }  else if textFieldType == "CreateDealFromOutside"{
            dealPostAsData?.selectedCell = indexPath.row
if  dealPostAsData?.data?.data?[indexPath.row].isSelected == true {
 
  if let index = postersIdArray.firstIndex(of: dealPostAsData?.data?.data?[indexPath.row].internalIdentifier ?? 0) {
      postersIdArray.remove(at: index)
  }
  dealPostAsData?.data?.data?[indexPath.row].isSelected = false
} else {
  postersIdArray.append(dealPostAsData?.data?.data?[indexPath.row].internalIdentifier ?? 0)
 
dealPostAsData?.data?.data?[indexPath.row].isSelected = true
}
                              tableView.reloadData()
delegate?.gettingTheSelectedValueFromTheList(value: dealPostAsData?.data?.data?[indexPath.row].internalIdentifier ?? 0, bool: Bool(), index: indexPath.row, type: .createDealFromOutside, posterIds: postersIdArray)
            
        }else if textFieldType == "CreateSpotFromOutside" {
                          dealPostAsData?.selectedCell = indexPath.row
                                 // selectedCell = indexPath.row
            if  dealPostAsData?.data?.data?[indexPath.row].isSelected == true {
               
                if let index = postersIdArray.firstIndex(of: dealPostAsData?.data?.data?[indexPath.row].internalIdentifier ?? 0) {
                    postersIdArray.remove(at: index)
                }
                dealPostAsData?.data?.data?[indexPath.row].isSelected = false
            } else {
                postersIdArray.append(dealPostAsData?.data?.data?[indexPath.row].internalIdentifier ?? 0)
               
            dealPostAsData?.data?.data?[indexPath.row].isSelected = true
            }
                                            tableView.reloadData()
            delegate?.gettingTheSelectedValueFromTheList(value: dealPostAsData?.data?.data?[indexPath.row].internalIdentifier ?? 0, bool: Bool(), index: indexPath.row, type: .createSpotFromOutside, posterIds: postersIdArray)
                         // self.dismiss(animated: true, completion: nil)
                                
                             } else if textFieldType == "genderTextField"{
            self.dismiss(animated: true, completion: nil)
            delegate?.sendingEvnetType(value: genderData[indexPath.row], type: "genderTextField")
        }  else if textFieldType == "Upload Photo In Report" {
            self.dismiss(animated: true, completion: nil)
            delegate?.gettingSelectedVaueFromProfileMoreBtn(value: indexPath.row, bool: false, index: 0)
                   
            
               } else if textFieldType == "FriendsCategoryAction"{
            delegate?.sendingUserId(id: userId ?? 0, index: indexPath.row)
                   self.dismiss(animated: true, completion: nil)
        } else if textFieldType == "professionTextField" {
            self.dismiss(animated: true, completion: nil)
            delegate?.sendingEvnetType(value: stringDataSource?[indexPath.row] ?? "",type: "professionTextField")
        } else if textFieldType == "statusTextField" {
            self.dismiss(animated: true, completion: nil)
            delegate?.sendingEvnetType(value: statusData[indexPath.row], type: "statusTextField")
        }  else if textFieldType == "Graduate Details More Selection"{
            if indexPath.row == 0 {
                textFieldType = "ProfileMoreBtnReloaded"
                               tableView.reloadData()
               
            } else {
               self.dismiss(animated: true, completion: nil)
                                          delegate?.sendingMediaType(value: indexPath.row)
            }
           
        } else if textFieldType == "Music Selection"{
              self.dismiss(animated: true, completion: nil)
            delegate?.sendingMediaType(value: indexPath.row)
                    
        } else if textFieldType == "CreateEventPostAs"{
            //self.dismiss(animated: true, completion: nil)
            if createEventPostAs?.data?[indexPath.row].isSelected == true {
                createEventPostAs?.data?[indexPath.row].isSelected = false
            } else {
                createEventPostAs?.data?[indexPath.row].isSelected = true
            }
            tableView.reloadData()
//            selectedCell = indexPath.row
//            tableView.reloadData()
//            delegate?.gettingSelectedVaueFromProfileMoreBtn(value: createEventPostAs?.data?[indexPath.row].id ?? 0, bool: false, index: indexPath.row)
        }else if textFieldType == "Change Profile Picture"{
            if indexPath.row == 0 {
                textFieldType = "ProfilePictureSelection"
                                              tableView.reloadData()
            } else {
                delegate?.sendingMultipleVlaues(values: [])
            }
           // delegate?.sendingUserId(id: indexPath.row, index: 0)
            
        } else if textFieldType == "Add Media"{
             self.dismiss(animated: true, completion: nil)
            delegate?.sendingMediaType(value: indexPath.row)
                     
                  }  else if textFieldType == "ProfilePictureSelection"{
                              self.dismiss(animated: true, completion: nil)
            delegate?.sendingDiscountType(value: profileBtnReloadedData[indexPath.row])
                                      
        } else if textFieldType == "DiscountPercentage"{
            self.dismiss(animated: true, completion: nil)
            delegate?.sendingUserId(id: numArray[indexPath.row], index: 0)
        } else if textFieldType == "CreateDealFromInside"{
            if postAsData?.isSelected == true {
                postAsData?.isSelected = false
               
            } else {
                postAsData?.isSelected = true
            }
            tableView.reloadData()
        } else {
            if dataSource[indexPath.row].selected == true {
                dataSource[indexPath.row].selected = false
                tableView.reloadRows(at: [indexPath], with: .automatic)
            } else {
                dataSource[indexPath.row].selected = true
                tableView.reloadRows(at: [indexPath], with: .automatic)
            }
            
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if textFieldType == "Create Deal" {
            return 50
        } else {
            return 50
        }
    }
}
extension Array where Element: Hashable {
    func removingDuplicates() -> [Element] {
        var addedDict = [Element: Bool]()
        
        return filter {
            addedDict.updateValue(true, forKey: $0) == nil
        }
    }
    
    mutating func removeDuplicates() {
        self = self.removingDuplicates()
    }
}
struct SelectionModel {
    var title: String?
    var selected: Bool?
}

