//
//  SearchViewController.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 7/24/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import UIKit

class SearchViewController: UIViewController {
    var searchMessage: String?
    var dataSource: SearchResponseModel? {
        didSet {
            tableview.reloadData()
        }
    }
    @IBOutlet var imageContainerView: UIView!
    @IBOutlet var tableview: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
      
        addingSearcBarInNav()
        registeringView()
        tableview.dataSource = self
        tableview.delegate = self
    }
    func addingSearcBarInNav() {
        var offset: CGFloat = 20

           // If VC is pushed, back button should be visible
           if navigationController?.navigationBar.backItem != nil {
               offset = 40
           }

           let customFrame = CGRect(x: 0, y: 0, width: view.frame.size.width - 75, height: 44.0)
           let searchBarContainer = UIView(frame: customFrame)
          let searchBar = UISearchBar(frame: customFrame)
        searchBar.delegate = self
        if #available(iOS 13.0, *) {
            searchBar.searchTextField.backgroundColor = .white
            searchBar.searchTextField.textColor = UIColor.init(hexString: Appcolors.text)
        } else {
           if let textField = searchBar.value(forKey: "searchField") as? UITextField {
            textField.backgroundColor = .white
            textField.textColor = UIColor.init(hexString: Appcolors.text)
                //textField.font = myFont
                //textField.textColor = myTextColor
                //textField.tintColor = myTintColor
                // And so on...
                
                let backgroundView = textField.subviews.first
                if #available(iOS 11.0, *) { // If `searchController` is in `navigationItem`
                    backgroundView?.backgroundColor = UIColor.white.withAlphaComponent(0.3) //Or any transparent color that matches with the `navigationBar color`
                    backgroundView?.subviews.forEach({ $0.removeFromSuperview() }) // Fixes an UI bug when searchBar appears or hides when scrolling
                }
                backgroundView?.layer.cornerRadius = 10.5
                backgroundView?.layer.masksToBounds = true
                //Continue changing more properties...
            }
        }
        searchBar.placeholder = "Search"
           searchBarContainer.addSubview(searchBar)
           navigationItem.titleView = searchBarContainer
        searchBar.becomeFirstResponder()

    }
//MARK:- REGISTERING VIEW
    func registeringView() {
        tableview.register(UINib(nibName: "SearchSectionView", bundle: nil), forCellReuseIdentifier: "SearchSectionView")

//        let nib = UINib(nibName: "SearchSectionView", bundle: nil)
//        tableview.register(nib, forHeaderFooterViewReuseIdentifier: "SearchSectionView")
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.isNavigationBarHidden = false
        currentTabBar?.setBar(hidden: true, animated: true)
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
       
    }
//MARK:- REQUEST FOR SAERCH API
    func getSearchApi() {
        SearchResponse.requestToSearch(search: searchMessage ?? "", showHud: false) { [weak self](response) in
            guard let strongSelf = self else { return }
            strongSelf.dataSource = response
        }
    }

}
//MARK:- TABLE VIEW DATASOURCE
extension SearchViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return dataSource?.posts?.count ?? 0
        } else if section == 1 {
            return dataSource?.users?.count ?? 0
        } else if section == 2 {
            return dataSource?.business?.count ?? 0
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableview.dequeueReusableCell(withIdentifier: "SearchResultTableViewCell", for: indexPath) as! SearchResultTableViewCell
        if indexPath.section == 0 {
            let cell = tableview.dequeueReusableCell(withIdentifier: "PostOnlyTableViewCell", for: indexPath) as! PostOnlyTableViewCell
        if (dataSource?.posts?.count ?? 0) > 0 {
           
            cell.titleLabel.text = dataSource?.posts?[indexPath.row].message
            return cell
        }
        } else if indexPath.section == 1{
            
        if (dataSource?.users?.count ?? 0) > 0 {
            cell.mainImage.isHidden = false
            cell.mainImage.setURLImage(imageURL: (GraduateUrl.imageUrl + (dataSource?.users?[indexPath.row
            ].profile ?? "defaultPicture")))
            cell.titleLabel.text = dataSource?.users?[indexPath.row].name
            return cell
        }
        } else if indexPath.section == 2 {
            if (dataSource?.business?.count ?? 0) > 0  {
            let cell = tableview.dequeueReusableCell(withIdentifier: "SearchResultTableViewCell", for: indexPath) as! SearchResultTableViewCell
            cell.mainImage.isHidden = false
            cell.titleLabel.text = dataSource?.business?[indexPath.row].businessName
            let image = ((GraduateUrl.imageUrl) + (dataSource?.business?[indexPath.row
            ].businessProfileImage ?? "defaultPicture"))
            print(image)
            cell.mainImage.setURLImage(imageURL: image)
            return cell
            }
        } else {
            return UITableViewCell()
        }
        return UITableViewCell()
    }

    
    
}
//MARK:- TABLE VIEW DELEGATE
extension SearchViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            let vc = UIStoryboard.timeLineStoryboard.instantiateSearchDetailsVC()
            vc.postID = dataSource?.posts?[indexPath.row].internalIdentifier
            navigationController?.pushViewController(vc, animated: true)
        } else if indexPath.section == 1 {
            let vc = UIStoryboard.timeLineStoryboard.instantiateOtherUserProfileVC()
            vc.userId = dataSource?.users?[indexPath.row].internalIdentifier
            navigationController?.pushViewController(vc, animated: true)
        } else if indexPath.section == 2 {
            let vc = UIStoryboard.graduateStoryboard.instantiateGraduateDetailsViewController()
            print(indexPath.row,dataSource?.business?[indexPath.row].internalIdentifier)
            vc.businessId = dataSource?.business?[indexPath.row].internalIdentifier
            navigationController?.pushViewController(vc, animated: true)
        }
    }
//    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
//        if section == 0 {
//            return "Post"
//        } else if section == 1 {
//            return "Users"
//        } else if section == 2 {
//            return "Business"
//        } else {
//            return ""
//        }
//    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            if (dataSource?.posts?.count ?? 0) > 0  {
                return 40
            } else {
                return CGFloat.leastNormalMagnitude
            }
        } else if section == 1{
            if (dataSource?.users?.count ?? 0) > 0  {
                return 40
            } else {
                return CGFloat.leastNormalMagnitude
            }
        } else if section == 2 {
            if (dataSource?.business?.count ?? 0) > 0  {
                return 40
            } else {
                return CGFloat.leastNormalMagnitude
            }
        } else {
            return 0
        }
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
      //  let header = self.tableview.dequeueReusableHeaderFooterView(withIdentifier: "SearchSectionView") as! SearchSectionView
        let header = Bundle.main.loadNibNamed("SearchHeaderTableViewCell", owner: self, options: nil)?.first as! SearchHeaderTableViewCell
        if section == 0 {
            header.titleLbel.text = "Posts"
            header.mainImage.image = UIImage(named: "iconSpot")
            return header
            } else if section == 1{
                header.titleLbel.text = "User"
                header.mainImage.image = UIImage(named: "defaultPicture")
                return header
            } else if section == 2 {
                header.titleLbel.text = "Business"
                header.mainImage.image = UIImage(named: "iconGraduate")
                return header
        }
        return header
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footer = Bundle.main.loadNibNamed("FooterTableViewCell", owner: self, options: nil)?.first as! FooterTableViewCell
        if section == 0 {
            return footer
            } else if section == 1{
                return footer
            } else if section == 2 {
                return footer
        }
        return footer
    }
  
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if section == 0 {
            if (dataSource?.posts?.count ?? 0) > 0  {
                return 40
            } else {
                return CGFloat.leastNormalMagnitude
            }
        } else if section == 1{
            if (dataSource?.users?.count ?? 0) > 0  {
                return 40
            } else {
                return CGFloat.leastNormalMagnitude
            }
        } else if section == 2 {
            if (dataSource?.business?.count ?? 0) > 0  {
                return 40
            } else {
                return CGFloat.leastNormalMagnitude
            }
        } else {
            return CGFloat.leastNormalMagnitude
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
extension SearchViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        print(searchBar.text)
        searchMessage = searchBar.text
        getSearchApi()
        tableview.reloadData()
   }
//    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
//       // print(searchBar.text)
//        searchMessage = searchBar.text
//        getSearchApi()
//    }
   
}
