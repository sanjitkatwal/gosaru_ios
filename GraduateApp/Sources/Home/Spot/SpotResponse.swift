//
//  SpotResponse.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 10/8/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import Foundation
import ObjectMapper

class SpotResponse: DefaultResponse {
    var data: LoginResponseModel?
    var success: Bool?
    var occupationList: [String] = []
override func mapping(map: Map) {
  super.mapping(map: map)

}
// MARK: - GET SPOT ONLY
    class func requestToSpot(pageNo: Int,showHud: Bool ,completionHandler:@escaping ((SpotReponseModel?) -> Void)) {
         let param:[String : Any] = ["pageNo" : pageNo, "only_event_post" : "true"]
        APIManager(urlString: GraduateUrl.offersURL,parameters: param ,method: .get).handleResponse(showProgressHud: showHud,completionHandler: { (response: SpotReponseModel) in
              print(response)
                  completionHandler(response)
              }, failureBlock: {
                completionHandler(nil)
              })
      }
}
 class SpotReponseModel: DefaultResponse {

// MARK: Declaration for string constants to be used to decode and also serialize.
internal let kSpotDataKey: String = "data"
internal let kSpotLinksKey: String = "links"
internal let kSpotMetaKey: String = "meta"


// MARK: Properties
public var data: [uniMyPostData]?
public var links: TimeLineLinks?
public var meta: TimeLineMeta?



// MARK: ObjectMapper Initalizers
/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/


/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/
    override func mapping(map: Map) {
    data <- map[kSpotDataKey]
    links <- map[kSpotLinksKey]
    meta <- map[kSpotMetaKey]

}
}

