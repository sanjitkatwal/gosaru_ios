//
//  SpotViewController.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 6/24/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import UIKit
import Popover
import Alamofire

struct SpotModel {
    var data: SpotReponseModel?
    var numOfPage: Int
    var loadMore: Bool
}
class SpotViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var popupTableView = UITableView(frame: CGRect(x: 0, y: 0, width: 90, height: 120))
    var texts = ["Share", "Report"]
      var keyboardOpen = false
     var postAdData: GraduateUniversityResponseModel?
     var posterdataSource: OffersModel?
    var dataSource: SpotModel?
    var reportId: Int?//post Id
      var cellHeights: [IndexPath : CGFloat] = [:]
       var popover: Popover!
       fileprivate var popoverOptions: [PopoverOption] = [
         .type(.left),
         .blackOverlayColor(UIColor(white: 0.0, alpha: 0.6))
       ]
    var postTypes: GraduateDetails? //used for the editing the post
    var editedIndex: IndexPath! //index path that is edited
     var name = "Sk"
    private var lastContentOffset: CGFloat = 0

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
       // self.navigationController?.isNavigationBarHidden = true
       navigationController?.isNavigationBarHidden = true

        registeringTableViewCell()
        currentTabBar?.selectedColor = UIColor.init(hexString: Appcolors.tabBarSelectedColor)
       dataSource = SpotModel(data: nil, numOfPage: 1, loadMore: false)
         requestForSpot(pageNo: 1, isLoadMore: false, showHud: true)
    getUserUniversity()
    pullToRefresh()
        
    }
    @objc func keyboardWillAppear(notification: NSNotification){
        // Do something here
        keyboardOpen = true
        if let userInfo = notification.userInfo,
           // 3
           let keyboardRectangle = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? CGRect {
        print(Int(keyboardRectangle.height))
            let view = UIView()
                           view.frame = CGRect(x: 0, y: 0, width: 414, height: Int(keyboardRectangle.height))
                           tableView.tableFooterView = view
        }
    }

    @objc func keyboardWillDisappear(notification: NSNotification){
        // Do something here
         tableView.tableFooterView = nil
        currentTabBar?.setBar(hidden: true, animated: true)
           keyboardOpen = false
    }

   
    override func viewWillAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector:#selector(keyboardWillAppear), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector:#selector(keyboardWillDisappear), name: UIResponder.keyboardWillHideNotification, object: nil)
        currentTabBar?.setBar(hidden: false, animated: true)
        navigationController?.isNavigationBarHidden = true
    }
//MARK:- PULL TO REFRESH
    func pullToRefresh() {
        self.tableView.es.addPullToRefresh {
            [unowned self] in
          
          requestForSpot(pageNo: 1, isLoadMore: false, showHud: false)
        }
    }
   
//MARK:- REGISTERING VIEW
    func registeringTableViewCell() {
        tableView.register(UINib(nibName: "EventTableViewCell", bundle: nil), forCellReuseIdentifier: "EventTableViewCell")
        tableView.dataSource = self
        tableView.delegate = self
       
    }
    override func viewWillDisappear(_ animated: Bool) {
        currentTabBar?.setBar(hidden: false, animated: true)
NotificationCenter.default.removeObserver(self)
        navigationController?.isNavigationBarHidden = false

    }
//MARK:- POP OVER
    func sharePopOver(sender: UIButton) {
        popupTableView.reloadData()
        tableView.reloadData()
                  popupTableView.delegate = self
                  popupTableView.dataSource = self
        if texts.count == 2 {
            popupTableView.frame = CGRect(x: 0, y: 0, width: 90, height: 80)
        } else {
            popupTableView.frame = CGRect(x: 0, y: 0, width: 90, height: 120)
            
        }
                  popupTableView.isScrollEnabled = false
                  self.popover = Popover(options: self.popoverOptions)
                  self.popover.willShowHandler = {
                    print("willShowHandler")
                  }
                  self.popover.didShowHandler = {
                    print("didDismissHandler")
                  }
                  self.popover.willDismissHandler = {
                    print("willDismissHandler")
                  }
                  self.popover.didDismissHandler = {
                    print("didDismissHandler")
                  }
              
               self.popover.show(popupTableView, fromView: sender)
    }

    //MARK:- REQUEST TO DELTE POST
        func requestToDeletePost() {
            WatchResponse.requestToDeletePost(id: dataSource?.data?.data?[editedIndex.row].internalIdentifier ?? 0,showHud: true) { [weak self](response) in
                guard let strongSelf = self else { return }
                strongSelf.requestForSpot(pageNo: 1, isLoadMore: false, showHud: true)
            }
        }
        
    @IBAction func createSpotAction(_ sender: Any) {
        let vc = UIStoryboard.timeLineStoryboard.instantiateCreateSpotViewController()
        vc.controllerType = "Create Deal"
        vc.vcType = .createSpotFromOutside
         vc.postAsUniData = postAdData
        vc.currentTabBar?.hidesBottomBarWhenPushed = true
        navigationController?.pushViewController(vc, animated: true)
    }
    //MARK:- REQUEST FOR GRAD SPOT ONLY
    func requestForSpot(pageNo: Int,isLoadMore: Bool = false, showHud: Bool = true) {
        SpotResponse.requestToSpot(pageNo: pageNo, showHud: showHud) { [weak self](response) in
            guard let strongSelf = self else { return }
            strongSelf.dataSource?.data = response
            strongSelf.tableView.es.stopPullToRefresh()
            if !isLoadMore {
            strongSelf.dataSource?.data = response
                          
                          strongSelf.tableView.reloadData()
                      } else {
                
                          if var allData = response?.data {
                              print(allData)
                              for data in allData {
                                strongSelf.dataSource?.data?.data?.append(data)
                              }
                              strongSelf.tableView.reloadData()
                          }
                      }
            if strongSelf.dataSource?.data?.data?.count ?? 0 < response?.meta?.total ?? 0 {
                          strongSelf.dataSource?.loadMore = true
                          strongSelf.dataSource?.numOfPage += 1
                      } else {
                          strongSelf.dataSource?.loadMore = false
                      }
            
        }
    }
//MARK:- POPUP
    func popUp(type: ErrorType, message: String) {
        let vc = UIStoryboard.timeLineStoryboard.instantiateErrorCarMsgPopVC()
        vc.popupType = type
        vc.messageString = message
        vc.delegaet = self
        let cardPopup = SBCardPopupViewController(contentViewController: vc)
        cardPopup.show(onViewController: self)
    }
    //MARK:- REQUEST FOR USER UNIVERSITIES
    func getUserUniversity() {
           GraduateUniversityResponse.requestToGetUserUni { [weak self](response) in
               guard let strongSelf = self else { return }
               print(response)
               strongSelf.postAdData = response
           }
}
}
//MARK:- TABLE VIEW DATASOURCE
extension SpotViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == popupTableView {
                   return texts.count
               } else {
            return dataSource?.data?.data?.count ?? 0
               }
    }
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        if tableView == popupTableView {
//            let cell = UITableViewCell(style: .default, reuseIdentifier: nil)
//                          cell.textLabel?.text = self.texts[(indexPath as NSIndexPath).row]
//                       cell.textLabel?.textColor = UIColor.init(hexString: Appcolors.text)
//                           return cell
//        } else {
//        let cell = tableView.dequeueReusableCell(withIdentifier: "EventTableViewCell", for: indexPath) as! EventTableViewCell
//        cell.collectionViewDataSource = 4
//           /// cell.collectionView.reloadData()
//            cell.presenterSource = "Spot"
//
//            //MARK: TAPPING NAME SO IT CAN BE PUSHED TO PROFILE VC
//            cell.nameLabel.text = name + " created a post"
//            let text = (cell.nameLabel.text)!
//                   let tap = UITapGestureRecognizer(target: self, action: #selector(self.tapLabel(gesture:)))
//            cell.nameLabel.addGestureRecognizer(tap)
//            cell.nameLabel.isUserInteractionEnabled = true
//                      let underlineAttriString = NSMutableAttributedString(string: text)
//                   let range1 = (text as NSString).range(of: name)
//                   underlineAttriString.addAttribute(NSAttributedString.Key.font, value: UIFont.systemFont(ofSize: 17, weight: .medium), range: range1)
//            cell.nameLabel.attributedText = underlineAttriString
//            cell.delegate = self
//            //MARK: TAPPING RATING POP UP
//            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(ratingPopUp))
//            cell.ratingBtn.addGestureRecognizer(tapGesture)
//
//            //MARK: TAPPING RATING DETAILS
//              let tapGesture2 = UITapGestureRecognizer(target: self, action: #selector(ratingDetails))
//            cell.ratingStackView.addGestureRecognizer(tapGesture2)
//
//
//            //MARK: TAPPING TO THOUGHTS VC
//            let tapGesture3 = UITapGestureRecognizer(target: self, action: #selector(thoughtsDetails))
//                       cell.thoughtsStackView.addGestureRecognizer(tapGesture3)
//
//            //  MARK: TAPPING TO ATTENDING LABEL
//        let tapGesture4 = UITapGestureRecognizer(target: self, action: #selector(attendingDetails))
//                              cell.attendingStackVIew.addGestureRecognizer(tapGesture4)
//
//            //MARK: TAPPING TO THOUGHTS BUTTON
//            cell.thoughtsBtn.addTarget(self, action: #selector(thoghtBtnAction), for: .touchUpInside)
//        return cell
//    }
//    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == popupTableView {
             let cell = UITableViewCell(style: .default, reuseIdentifier: nil)
                                      cell.textLabel?.text = self.texts[(indexPath as NSIndexPath).row]
                                  cell.textLabel?.textColor = UIColor.init(hexString: Appcolors.text)
                                       return cell
        } else {
              let cell = tableView.dequeueReusableCell(withIdentifier: "EventTableViewCell", for: indexPath) as! EventTableViewCell
            cell.postType = .gradSpot
            cell.delegate = self
            cell.selectionType = .gradSpot
            cell.index = indexPath
            return cell.eventTVC(cell: cell, indexPath: indexPath, dataSource: dataSource?.data?.data)
        }
    }
    @objc func attendingDetails() {
        let vc = UIStoryboard.graduateStoryboard.instantiateAttendingVC()
        vc.dataSource = 1
                      let cardPopup = SBCardPopupViewController(contentViewController: vc)
        
                      cardPopup.show(onViewController: self)
        //self.present(vc, animated: true, completion: nil)
       }
    @objc func thoghtBtnAction() {
           let vc = UIStoryboard.timeLineStoryboard.instantiateThoughtsVC()
        navigationController?.pushViewController(vc, animated: true)
       }
    @objc func ratingPopUp() {
        popingBottomVC(id: 0, starredVlaue: "", indexToReload: 0, selectionType: .gradSpot)
    }
//MARK:- RATING DETAILS
   private func ratingDetails(ratingId: Int, starredVlaue: String, starCount: Int) {
        let vc = UIStoryboard.timeLineStoryboard.instantiateRatingDetailsVC()
        vc.ratingId = ratingId
    vc.starredValue = starredVlaue
    vc.starCount = starCount
        navigationController?.pushViewController(vc, animated: true)
   
       }
    func thoughtsDetails(id: Int,index: IndexPath) {
        let vc = UIStoryboard.timeLineStoryboard.instantiateThoughtsVC()
        vc.commentId = id
        vc.delegate = self
        vc.index = index
     navigationController?.pushViewController(vc, animated: true)
    }
    func popingBottomVC(id: Int, starredVlaue: String,indexToReload: Int, selectionType: GraduateDetails) {
        let vc = HelperFunctions.ratingSelection(id: id, starredVlaue: starredVlaue, reloadingIndex: indexToReload)
        vc.id = id
        vc.selectedIndex = Int(starredVlaue)
        vc.indexToReload = indexToReload
        vc.delegate = self
        vc.selectionType = .gradSpot
                     present(vc, animated: true, completion: nil)
       }
    @IBAction func tapLabel(gesture: UITapGestureRecognizer) {
      
//          let text = (nameLabel.text)!
//          let termsRange = (text as NSString).range(of: name)
//
//          if gesture.didTapAttributedTextInLabel(label: nameLabel, inRange: termsRange) {
//              delegate?.pushingToProfileFromSpot()
//          }
//      }
        let vc = UIStoryboard.graduateStoryboard.instantiateUserProfileVC()
        navigationController?.pushViewController(vc, animated: true)
      
}
}
//MARK:- TABLE VIEW DELEGATE
extension SpotViewController: UITableViewDelegate {
  
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
         if tableView == popupTableView {
                    return
                } else if tableView == tableView{
                let allData = dataSource
                           print(allData?.loadMore ?? false)
                           print(indexPath.row,"indexPath")
                           print(dataSource?.data?.data?.count,"dataCount")
                           guard allData?.loadMore == true, let dataCount = allData?.data?.data?.count, indexPath.row == dataCount - 1 else {return}
                           print(dataCount)
                           let lastSectionIndex = tableView.numberOfSections - 1
                           let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
                           if indexPath.section == lastSectionIndex && indexPath.row == lastRowIndex {
                               // print("this is the last cell")
                               let spinner = UIActivityIndicatorView(style: .gray)
                               spinner.startAnimating()
                               spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
                               self.tableView.tableFooterView = spinner
                               self.tableView.tableFooterView?.isHidden = false
                           }
                           // print(allData.url)
        //                   requestingForPortfolio(pageNo: dataSource?.numOfPage ?? 0, isLoadMore: dataSource!.loadMore, showHud: false)
            requestForSpot(pageNo: dataSource?.numOfPage ?? 0, isLoadMore: dataSource!.loadMore, showHud: false)
    }
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == popupTableView {
            return 40
        } else {
        return 505
    }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        popover.dismiss()
        if tableView == popupTableView {
            popover.dismiss()
            if texts[indexPath.row] ==  "Share"{
                let shareText = "http://test-graduation1.captainau.com.au/"
                let image = UIImage(named: "iconArrowDown")
                let vc = UIActivityViewController(activityItems: [shareText, image!], applicationActivities: [])
                present(vc, animated: true)
                self.popover.dismiss()
            } else if texts[indexPath.row] == "Edit" {
                let vc = UIStoryboard.timeLineStoryboard.instantiateCreateSpotViewController()
                vc.vcType = .editSpot
                vc.editData = dataSource?.data?.data?[editedIndex.row]
                vc.delegate = self
                navigationController?.pushViewController(vc, animated: true)
                self.popover.dismiss()
            } else if texts[indexPath.row] == "Delete" {
                popUp(type: .deletePost, message: "")
            } else {
                let vc = UIStoryboard.graduateStoryboard.instantiateReportVC()
                vc.reportId = dataSource?.data?.data?[editedIndex.row].internalIdentifier
                vc.view.isOpaque = false
                vc.view.backgroundColor = UIColor.black.withAlphaComponent(0.8)
                vc.modalPresentationStyle = .custom
                self.present(vc, animated: true, completion: nil)
                popover.dismiss()
            }
//        if tableView == popupTableView {
//            if indexPath.row == 0 {
//                let shareText = "Hello, world!"
//
//                 let image = UIImage(named: "iconArrowDown")
//                let vc = UIActivityViewController(activityItems: [shareText, image!], applicationActivities: [])
//                    present(vc, animated: true)
//                self.popover.dismiss()
//            } else {
//                let vc = UIStoryboard.graduateStoryboard.instantiateReportVC()
//                vc.reportId = reportId
//                vc.view.isOpaque = false
//                vc.view.backgroundColor = UIColor.black.withAlphaComponent(0.8)
//                vc.modalPresentationStyle = .custom
//                self.present(vc, animated: true, completion: nil)
//                popover.dismiss()
//
//            }
//        } else {
//
//        }
    }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == popupTableView {
            return 40
        } else {
        return  UITableView.automaticDimension
        }
    
}
}
extension SpotViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        //MARK-: FOR NOT BOUNCING WHILE PULL TO REFRESH
        if scrollView.contentOffset.y < 0.0 {
            return
        }
        
        if (scrollView.contentOffset.y >= scrollView.contentSize.height - scrollView.frame.size.height) {
            scrollView.setContentOffset(CGPoint(x: scrollView.contentOffset.x, y: scrollView.contentSize.height - scrollView.frame.size.height), animated: false)
            //
        }
        if keyboardOpen == false {
        let delta = scrollView.contentOffset.y - lastContentOffset
        if delta < 0 {
            currentTabBar?.tabBarHeight = min(50, (currentTabBar?.buttonsContainerHeightConstraintInitialConstant)! - delta)
          
            self.view.layoutIfNeeded()
        } else {
           
            currentTabBar?.tabBarHeight = max(-35, (currentTabBar?.buttonsContainerHeightConstraintInitialConstant)! - delta)
           
            self.view.layoutIfNeeded()
            
        }
        // This makes the + or - number quite small.
        lastContentOffset = scrollView.contentOffset.y
    } else{
    
    }
    }
}
//MARK:- EVENT TVC DELEGATE
extension SpotViewController: SendingRatingFromEventTVC {
    func moreOPtionFromEventTVC(sender: UIButton, id: Int, index: IndexPath, cell: EventTableViewCell, isOwner: Bool, postType: GraduateDetails) {
        reportId = id
        postTypes = postType
        editedIndex = index
        if isOwner == true {
            texts.removeAll()
            texts = ["Share", "Edit","Delete"]
            sharePopOver(sender: sender)
        } else {
            texts.removeAll()
            texts = ["Share", "Report"]
            sharePopOver(sender: sender)
        }
    }
   
    func outsideCommentEventTVC(comment: Comments?, index: IndexPath) {
        dataSource?.data?.data?[index.row].comments?.append(comment!)
        tableView.reloadRows(at: [index], with: .automatic)
    }
    func pushingToProfileFromSpot(id: Int, userType: String, isOwner: Bool, postAsType: String) {
       pushToProfile(id: id, userType: userType, isOwner: isOwner, postAsType: postAsType)
    }
    func eventFeedbackAction(commentId: Int, index: IndexPath) {
        thoughtsDetails(id: commentId, index: index)
    }
   
   
    func sendingRatingEventTVC(id: Int, starValue: String, selectedindexToReload: Int, selectionType: GraduateDetails) {
        print(starValue)
        popingBottomVC(id: id, starredVlaue: starValue, indexToReload: selectedindexToReload, selectionType: .gradSpot)
    }
    func attendIgnoreEventTVC(universityId: Int, attendState: Bool, index: IndexPath!, attendeesCount: Int) {
        let url: String?
        print(universityId)
        let id = dataSource?.data?.data?[index.row].event?.internalIdentifier
        var attendeeCount = attendeesCount
        if attendState == true {
            url = (GraduateUrl.attendEventURL + "\(id ?? 0)")
            
            //attendeeCount = attendeesCount + 1
            
        } else {
            url = (GraduateUrl.unAttendEventURL + "\(id ?? 0)")
            //attendeeCount = attendeesCount - 1
        }
        
        GraduateDetailsResponse.requestToAttendIgnoreEvents(url: url ?? "") { [weak self](response) in
            guard let strongSelf = self else { return }
           print(response)
            
        }
    }
 
   
    func eventRatingDetails(ratingId: Int, starredVlaue: String, starCount: Int) {
        self.ratingDetails(ratingId: ratingId, starredVlaue: starredVlaue, starCount: starCount)
    }
    
   
    
  
//    func feedbackAction() {
//        thoughtsDetails()
//    }
//
   
    func fullScreenAction(vc: UIViewController) {
         currentTabBar?.setBar(hidden: true, animated: true)
         present(vc, animated: true, completion: nil)
    }
    
//    func feedbackActionEventTVC() {
//        thoughtsDetails()
//    }
    func pushToProfile(id: Int, userType: String, isOwner: Bool, postAsType: String) {
        if postAsType == "user" {
                    if isOwner == true {
                                 let vc = UIStoryboard.timeLineStoryboard.instantiateUserProfileVC()
                                 navigationController?.pushViewController(vc, animated: true)
                                 } else {
                                     let vc = UIStoryboard.timeLineStoryboard.instantiateOtherUserProfileVC()
                               vc.userId = id
                                            navigationController?.pushViewController(vc, animated: true)
                                 }
                    } else {
                        let vc = UIStoryboard.timeLineStoryboard.instantiateGraduateDetailsViewController()
                        vc.businessId = id
                        navigationController?.pushViewController(vc, animated: true)
                    }
    }
    
}
extension SpotViewController: SendingActionFromRatingVC {
    func sendingActionWithType(type: RatingActionTypes, id: Int, reloadingIndex: Int, ratingNum: Int, selectionType: GraduateDetails) {
        switch type {
        case .requestforRating:
           ratingAfterApiCallOfEvent(reloadingIndex: reloadingIndex, ratingNum: ratingNum)
     
            reqestForPostRating(id: id, reloadingIndex: reloadingIndex, ratingNum: ratingNum, selectionType: selectionType)
        case .reuestForUnratingPost:
            unratingAfterApiCallOfEvent(reloadingIndex: reloadingIndex, ratingNum: ratingNum)
            reqestForPostUnRating(id: id, reloadingIndex: reloadingIndex, ratingNum: ratingNum, selectionType: selectionType)
        default:
            break
        }
    }
    //MARK:- RATE POST
       func reqestForPostRating(id: Int,reloadingIndex: Int,ratingNum: Int,selectionType: GraduateDetails) {
           var headers: [String : Any]?
           if let token = UserDefaultsHandler.getUDValue(key: UDkey.token) as? String {
               if headers == nil {
                   headers = ["Authorization": "Bearer \(token)", "Accept": "application/json"]
               } else {
                   headers!["Authorization"] = "Bearer \(token)"
                   headers!["Accept"] = "application/json"
               }
           }
           let url = (GraduateUrl.ratePostURL + "\(id)")
            print(url,ratingNum,selectionType)
           Alamofire.request(url,
                             method: .post,
                             parameters: ["star_value": "\(ratingNum + 1)"],headers: (headers as! HTTPHeaders))
               .validate()
               .responseJSON { [weak self]response in
                   guard let strongSelf = self else { return }
                   if let value = response.result.value as? [String : Any] {
                       print(value)
//                    strongSelf.ratingAfterApiCallOfEvent(reloadingIndex: reloadingIndex, ratingNum: ratingNum)
                       
                   }
                   
           }
       }
    
    //MARK:- REQUEST FOR UNRATING POST
    func reqestForPostUnRating(id: Int,reloadingIndex: Int,ratingNum: Int,selectionType: GraduateDetails) {
        var headers: [String : Any]?
        if let token = UserDefaultsHandler.getUDValue(key: UDkey.token) as? String {
            if headers == nil {
                headers = ["Authorization": "Bearer \(token)", "Accept": "application/json"]
            } else {
                headers!["Authorization"] = "Bearer \(token)"
                headers!["Accept"] = "application/json"
            }
        }
        let url = (GraduateUrl.unratePostURL + "\(id)")
        print(url,ratingNum,selectionType)
        Alamofire.request(url,method: .delete,headers: (headers as! HTTPHeaders))
            .validate()
            .responseJSON { [weak self]response in
                guard let strongSelf = self else { return }
                if let value = response.result.value as? [String : Any] {
//                    strongSelf.unratingAfterApiCallOfEvent(reloadingIndex: reloadingIndex, ratingNum: ratingNum)
                    print(value)
                    print(reloadingIndex)
                    
                }
                
        }
    }
    func ratingAfterApiCallOfEvent(reloadingIndex: Int, ratingNum: Int) {
        print(dataSource?.data?.data?[reloadingIndex].hasStarred)
        if dataSource?.data?.data?[reloadingIndex].hasStarred == true {
           
        } else {
            
            dataSource?.data?.data?[reloadingIndex].starsCount! += 1
        }
        dataSource?.data?.data?[reloadingIndex].starredValue = "\((ratingNum + 1))"
        print(dataSource?.data?.data?[reloadingIndex].starredValue)
        print(reloadingIndex)
        dataSource?.data?.data?[reloadingIndex].hasStarred = true
        tableView.reloadData()
    }
    func unratingAfterApiCallOfEvent(reloadingIndex: Int, ratingNum: Int) {
        dataSource?.data?.data?[reloadingIndex].hasStarred = false
        if  dataSource?.data?.data?[reloadingIndex].starsCount ?? 0 > 0 {
            dataSource?.data?.data?[reloadingIndex].starsCount! -= 1
        } else {
            
        }
        dataSource?.data?.data?[reloadingIndex].starredValue = "\(0)"
        tableView.reloadData()
    }
    
    
}
//MARK:- THOUGHTS VC DELEGATES
extension SpotViewController: SendingDataFromThoughtsVC {
    func sendingData(index: IndexPath, thoughtsCount: Int, commentsData: [Comments]) {
        dataSource?.data?.data?[index.row].comments = commentsData
        tableView.reloadRows(at: [index], with: .automatic)
    }
    
    
}
extension SpotViewController: ActionFromCreateSpotVC {
    func reloadTableViewData() {
        
    }
    
    func reloadWithType(type: ErrorType) {
        
    }
    
    
}
//MARK:- ERROR CHAT DELEGATE
extension SpotViewController: SendingActionFromErrorChat {
    func sendingAction() {
    }
    
    func sendingCancelAction() {
    }
    
    func adctionwithType(type: ErrorType) {
        requestToDeletePost()
    }
}



