//
//  ReportViewController.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 7/27/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import UIKit

class ReportViewController: UIViewController {

    @IBOutlet weak var reportTxtView: UITextView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var closeBtn: UIButton!
    var imagePickerController = CustomImagePicker()
    var reportId: Int?
    var imageDataSource:[UIImage] = [] {
        didSet {
            if imageDataSource.count == 0 {
                collectionView.isHidden = true
            } else {
            collectionView.isHidden = false
            collectionView.dataSource = self
            collectionView.delegate = self
            collectionView.reloadData()
            }
        }
    }
        

        

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        collectionView.isHidden = true
        closeBtn.setImage(UIImage(named: "cross"), for: .normal)
        view.backgroundColor = UIColor.black.withAlphaComponent(0.8)
        collectionView.register(UINib(nibName:"PostCreationCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "PostCreationCollectionViewCell")
       textViewInitialSetup()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
      
            }
    
    @IBAction func closeBtnAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
       
    }
 
    @IBAction func uploadPhotoAction(_ sender: Any) {
//
                   let popupVC = UIStoryboard.timeLineStoryboard.instantiateSelectionBottomPopUpVC()
               popupVC.delegate = self
               popupVC.textFieldType = "Category"
                      popupVC.height = 100
                      popupVC.topCornerRadius = 15
               popupVC.presentDuration = 0.25
                      popupVC.dismissDuration = 0.25
                      popupVC.shouldDismissInteractivelty = true
        popupVC.textFieldType = "Upload Photo In Report"
                     // popupVC.popupDelegate = self
                      present(popupVC, animated: true, completion: nil)

    }
    func textViewInitialSetup() {
        reportTxtView.delegate = self
               reportTxtView.text = "Write about your offer"
               reportTxtView.textColor = .lightGray
    }
   
    func getImage(fromSourceType sourceType: UIImagePickerController.SourceType, mediaType: String) {
      
            //Check is source type available
            if sourceType == .savedPhotosAlbum {
                imagePickerController.delegate = self
                          imagePickerController.sourceType = .savedPhotosAlbum
                          imagePickerController.allowsEditing = true
                //imagePickerController.modalPresentationStyle = .overFullScreen
                   self.present(self.imagePickerController, animated: true, completion: nil)

            } else if sourceType == .camera{

    if UIImagePickerController.isSourceTypeAvailable(.camera) {
                imagePickerController.sourceType = .camera
                //imagePickerController.mediaTypes = [mediaType]
                imagePickerController.delegate = self
        imagePickerController.allowsEditing = true
        self.present(self.imagePickerController, animated: true, completion: nil)
        
                
            } else {
                print("You do not have a camera")
            }
            } else {
    //            if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
    //                    imagePickerController.sourceType = .photoLibrary
    //                    imagePickerController.mediaTypes = [mediaType]
    //                    imagePickerController.delegate = self
    //            imagePickerController.allowsEditing = true
    //                    self.present(imagePickerController, animated: true, completion: nil)
    //            }
            }
    
}
    @IBAction func reportNowAction(_ sender: Any) {
        if reportTxtView.text == "" || reportTxtView.text == "Write about your offer" {
            return
        } else {
            let param: [String : Any] = ["message" : reportTxtView.text ?? ""]
            ReportResponse.requestToReportPost(param: param, images: imageDataSource, reportId: reportId ?? 0) { [weak self](response) in
                guard let strongSelf = self else { return }
                print(response)
                strongSelf.popUp(type: .reported, messsage: "")
            }
            
        }
       
        
    }
    func popUp(type: ErrorType, messsage: String) {
        let vc = UIStoryboard.timeLineStoryboard.instantiateErrorCarMsgPopVC()
        vc.popupType = type
       // vc.delegaet = self
        vc.messageString = messsage
        let cardPopup = SBCardPopupViewController(contentViewController: vc)
        cardPopup.show(onViewController: self)
    }
}
extension ReportViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
    self.dismiss(animated: true) { [weak self] in
        guard let strongSelf = self else {
            return
        }
       
            if let selectedImage = info[.originalImage] as? UIImage  {
                print(selectedImage)
               
                strongSelf.imageDataSource.append(selectedImage)
                
                
                                
                
            }
        }
    }
   
    }
extension ReportViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imageDataSource.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ReportCollectionViewCell", for: indexPath) as! ReportCollectionViewCell
        cell.mainImage.image = imageDataSource[indexPath.row]
        cell.index = indexPath.row
        cell.closeBtn.setImage(UIImage(named: "cross"), for: .normal)
        cell.delegate = self
        return cell
    }
    
    
}
extension ReportViewController: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.bounds.width - 30, height: collectionView.bounds.height - 4)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
              return UIEdgeInsets(top: 2, left: 0, bottom: 2, right: 8)
          }
}

extension ReportViewController: SendingMultipleSeclections {
    func postAsActionFromInside(value: Int?, bool: Bool, index: Int?, type: CreateSpotVCTypes, posterIds: [Int], postData: CreateEventModel) {
        
    }
    func gettingTheSelectedValueFromTheList(value: Int?, bool: Bool, index: Int?, type: CreateSpotVCTypes, posterIds: [Int]) {
        
    }
    func gettingSelectedVaueFromProfileMoreBtn(value: Int, bool: Bool, index: Int) {
        //self.dismiss(animated: true, completion: nil)
               if value == 0 {
                   getImage(fromSourceType: .camera, mediaType: "")
               } else {
                   getImage(fromSourceType: .savedPhotosAlbum, mediaType: "public.image")
               }
    }
    func sendingUserId(id: Int, index: Int) {
        
    }
    func sendingEvnetType(value: String, type: String) {
        
    }
    func sendingMediaType(value: Int) {
        
    }
    func sendingMultipleVlaues(values: [String]) {
        
    }
    
    func sendingDiscountType(value: String) {
        
    }
    
    func sendingEvnetType(value: String) {
        
    }
    
    func sendingUniCategory(value: String) {
        
    }
    
   
    func gettingFullScreenCoverPic() {
        
    }
}
extension ReportViewController: SendingActionFromReportCVC {
    func closeAction(index: Int?) {
        print(index)
        imageDataSource.remove(at: index ?? 0)
    }
    
}
extension ReportViewController: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView == reportTxtView {
            if reportTxtView.text == "Write about your offer" {
                reportTxtView.text = ""
                reportTxtView.textColor = UIColor.init(hexString: Appcolors.text)
            }
        }
    }
}
extension ReportViewController: SendingActionFromDeletePostPopUP {
    func reportNowResult(value: Bool) {
        if value == true {
        textViewInitialSetup()
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    
}
