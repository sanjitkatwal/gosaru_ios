//
//  ReportCollectionViewCell.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 8/24/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import UIKit
protocol SendingActionFromReportCVC: class {
    func closeAction(index: Int?)
}
class ReportCollectionViewCell: UICollectionViewCell {
    weak var delegate: SendingActionFromReportCVC?
    @IBOutlet weak var mainImage: UIImageView!
    @IBOutlet weak var closeBtn: UIButton!
    var index: Int?
    override class func awakeFromNib() {
        super.awakeFromNib()
    }
    @IBAction func closeBtnAction(_ sender: Any) {
        delegate?.closeAction(index: index)
}
}
