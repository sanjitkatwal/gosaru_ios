//
//  ReportReponse.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 10/6/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import Foundation
import ObjectMapper
class ReportResponse: DefaultResponse {
    var data: LoginResponseModel?
    var success: Bool?
    var occupationList: [String] = []
override func mapping(map: Map) {
  super.mapping(map: map)
success <- map["success"]
}
// MARK: - REPORT POST
    class func requestToReportPost(param: [String : Any],images: [UIImage],reportId: Int,showHud: Bool = true ,completionHandler:@escaping ((ReportResponse?) -> Void)) {
        let url = (GraduateUrl.reportPostURL + "\(reportId)")
        ImageUploader.uploadImage(url: url, videos: [], imageName: "images[]", parameters: param, imagesDataSource: images, completionSuccess: { (response: ReportResponse?) in
            print(response)
          completionHandler(response)
      }) {
          completionHandler(nil)
      }
      }
}
