//
//  storyboard+Spot.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 6/24/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import Foundation
import UIKit
extension UIStoryboard {
private struct Constants {
    static let spotStoryboard = "Spot"
    static let spotIdentifier = "SpotViewController"
   
}
static var spotStoryboard: UIStoryboard {
    return UIStoryboard(name: Constants.spotStoryboard, bundle: nil)
}
func instantiateSpotViewController() -> SpotViewController {
    guard let viewController = UIStoryboard.spotStoryboard.instantiateViewController(withIdentifier: Constants.spotIdentifier) as? SpotViewController else {
        fatalError("Couldn't instantiate SpotViewController")
    }
    return viewController
}
}
