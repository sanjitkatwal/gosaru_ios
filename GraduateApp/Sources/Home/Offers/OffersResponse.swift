//
//  OffersResponse.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 10/6/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import Foundation
import ObjectMapper
class OffersResponse: DefaultResponse {
    var data: LoginResponseModel?
    var success: Bool?
    var occupationList: [String] = []
override func mapping(map: Map) {
  super.mapping(map: map)
success <- map["success"]
}
// MARK: - GET USER FULL DETAILS
    class func requestToOffers(pageNo: Int,showHud: Bool ,completionHandler:@escaping ((OffersResponseModel?) -> Void)) {
         let param:[String : Any] = ["pageNo" : pageNo, "only_offer_post" : "true"]
        APIManager(urlString: GraduateUrl.offersURL,parameters: param ,method: .get).handleResponse(showProgressHud: showHud,completionHandler: { (response: OffersResponseModel) in
              print(response)
                  completionHandler(response)
              }, failureBlock: {
                completionHandler(nil)
              })
      }
}
 class OffersResponseModel: DefaultResponse {

// MARK: Declaration for string constants to be used to decode and also serialize.
internal let kOffersResponseModelDataKey: String = "data"
internal let kOffersResponseModelLinksKey: String = "links"
internal let kOffersResponseModelMetaKey: String = "meta"


// MARK: Properties
public var data: [uniMyPostData]?
public var links: TimeLineLinks?
public var meta: TimeLineMeta?



// MARK: ObjectMapper Initalizers
/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/


/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/
    override func mapping(map: Map) {
    data <- map[kOffersResponseModelDataKey]
    links <- map[kOffersResponseModelLinksKey]
    meta <- map[kOffersResponseModelMetaKey]

}
}
