//
//  OffersViewController.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 6/24/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import UIKit
import Popover
import Alamofire
struct OffersModel {
    var data: OffersResponseModel?
    var numOfPage: Int
       var loadMore: Bool
}
class OffersViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var createBtn: AnimatedButton!
      var keyboardOpen = false
    var popupTableView = UITableView(frame: CGRect(x: 0, y: 0, width: 90, height: 80))
    var texts = ["Share", "Report"]
       var popover: Popover!
    var posterId: Int?//for creating deal
    var dataSource: OffersModel?
    var postAdData: GraduateUniversityResponseModel?
       fileprivate var popoverOptions: [PopoverOption] = [
         .type(.left),
         .blackOverlayColor(UIColor(white: 0.0, alpha: 0.6))
       ]
    private var lastContentOffset: CGFloat = 0
    var postTypes: GraduateDetails? //used for the editing the post
    var editedIndex: IndexPath! //index path that is edited
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
       currentTabBar?.selectedColor = UIColor.init(hexString: Appcolors.tabBarSelectedColor)
        // Do any additional setup after loading the view.
        dataSource = OffersModel(data: nil, numOfPage: 1, loadMore: false)
        requestForOffers(pageNo: 1, isLoadMore: false, showHud: true)
        getUserUniversity()
        NotificationCenter.default.addObserver(self, selector:#selector(keyboardWillAppear), name: UIResponder.keyboardWillShowNotification, object: nil)
               NotificationCenter.default.addObserver(self, selector:#selector(keyboardWillDisappear), name: UIResponder.keyboardWillHideNotification, object: nil)
        
    }
   
    override func viewWillAppear(_ animated: Bool) {
//        HelperFunctions.unhidingViewFromTabBar(height: currentTabBar!.tabBarHeight + 32, tableView: tableView)
        NotificationCenter.default.addObserver(self, selector:#selector(keyboardWillAppear), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector:#selector(keyboardWillDisappear), name: UIResponder.keyboardWillHideNotification, object: nil)
        navigationController?.isNavigationBarHidden = true
        currentTabBar?.setBar(hidden: false, animated: true)
        registeringTableViewCell()
        pullToRefresh()
    }
    override func viewWillDisappear(_ animated: Bool) {
        navigationController?.isNavigationBarHidden = false
        NotificationCenter.default.removeObserver(self)

    }
    @objc func keyboardWillAppear(notification: NSNotification){
           // Do something here
           keyboardOpen = true
           if let userInfo = notification.userInfo,
              // 3
              let keyboardRectangle = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? CGRect {
           print(Int(keyboardRectangle.height))
               let view = UIView()
                              view.frame = CGRect(x: 0, y: 0, width: 414, height: Int(keyboardRectangle.height))
                              tableView.tableFooterView = view
           }
       }

       @objc func keyboardWillDisappear(notification: NSNotification){
           // Do something here
            tableView.tableFooterView = nil
           currentTabBar?.setBar(hidden: true, animated: true)
           keyboardOpen = false
       }
    
    func registeringTableViewCell() {
//        tableView.register(UINib(nibName: "DiscountTableViewCell", bundle: nil), forCellReuseIdentifier: "DiscountTableViewCell")
//        tableView.dataSource = self
//        tableView.delegate = self
//        tableView.reloadData()
        tableView.register(UINib(nibName: "DiscountTableViewCell", bundle: nil), forCellReuseIdentifier: "DiscountTableViewCell")
//          tableView.register(UINib(nibName: "TestTableViewCell", bundle: nil), forCellReuseIdentifier: "TestTableViewCell")
        tableView.dataSource = self
        tableView.delegate = self
        
    }
    //MARK:- PULL TO REFRESH
        func pullToRefresh() {
            self.tableView.es.addPullToRefresh {
                [unowned self] in
              requestForOffers(pageNo: 1, isLoadMore: false, showHud: false)
            }
        }
//MARK:- POPUP
        func popUp(type: ErrorType, message: String) {
            let vc = UIStoryboard.timeLineStoryboard.instantiateErrorCarMsgPopVC()
            vc.popupType = type
            vc.messageString = message
            vc.delegaet = self
            let cardPopup = SBCardPopupViewController(contentViewController: vc)
            cardPopup.show(onViewController: self)
        }
    
    //MARK:- REQUEST FOR OFFERS
    func requestForOffers(pageNo: Int,isLoadMore: Bool = false, showHud: Bool = true) {
       
        OffersResponse.requestToOffers(pageNo: pageNo,showHud: showHud) { [weak self](response) in
            guard let strongSelf = self else { return }
            strongSelf.tableView.es.stopPullToRefresh()
            //strongSelf.dataSource?.data = response
            strongSelf.tableView.tableFooterView = nil
                       strongSelf.tableView.tableFooterView?.isHidden = true
           if !isLoadMore {
            strongSelf.dataSource?.data = response
                          
                          strongSelf.tableView.reloadData()
                      } else {
                          if var allData = response?.data {
                              print(allData)
                              for data in allData {
                                strongSelf.dataSource?.data?.data?.append(data)
                              }
                              strongSelf.tableView.reloadData()
                          }
                      }
            if strongSelf.dataSource?.data?.data?.count ?? 0 < response?.meta?.total ?? 0 {
                          strongSelf.dataSource?.loadMore = true
                          strongSelf.dataSource?.numOfPage += 1
                      } else {
                          strongSelf.dataSource?.loadMore = false
                      }
            
        }
    }
    
    
    //MARK:- GET USER UNIVERSITY
    func getUserUniversity() {
        GraduateUniversityResponse.requestToGetUserUni { [weak self](response) in
            guard let strongSelf = self else { return }
            print(response)
            strongSelf.postAdData = response
        }
    }
    //MARK:- REQUEST TO DELTE POST
        func requestToDeletePost() {
            WatchResponse.requestToDeletePost(id: dataSource?.data?.data?[editedIndex.row].internalIdentifier ?? 0,showHud: true) { [weak self](response) in
                guard let strongSelf = self else { return }
                strongSelf.requestForOffers(pageNo: 1, isLoadMore: false, showHud: true)
            }
        }
        
//MARK:- RATING DETAILS
       private func ratingDetails(ratingId: Int, starredVlaue: String, starCount: Int) {
            let vc = UIStoryboard.timeLineStoryboard.instantiateRatingDetailsVC()
            vc.ratingId = ratingId
        vc.starredValue = starredVlaue
        vc.starCount = starCount
            navigationController?.pushViewController(vc, animated: true)
       
           }
//MARK:- POPOVER
    func sharePopOver(sender: UIButton) {
             popupTableView.reloadData()
        if texts.count == 2 {
            popupTableView.frame = CGRect(x: 0, y: 0, width: 90, height: 80)
            
        } else {
            popupTableView.frame = CGRect(x: 0, y: 0, width: 90, height: 120)
            
        }
                       popupTableView.delegate = self
                       popupTableView.dataSource = self
                       popupTableView.isScrollEnabled = false
                       self.popover = Popover(options: self.popoverOptions)
                       self.popover.willShowHandler = {
                         print("willShowHandler")
                       }
                       self.popover.didShowHandler = {
                         print("didDismissHandler")
                       }
                       self.popover.willDismissHandler = {
                         print("willDismissHandler")
                       }
                       self.popover.didDismissHandler = {
                         print("didDismissHandler")
                       }
                   
                    self.popover.show(popupTableView, fromView: sender)
         }
    @IBAction func createDealAction(_ sender: Any) {
        let vc = UIStoryboard.timeLineStoryboard.instantiateCreateSpotViewController()
        vc.vcType = .createDealFromOutside
        print(postAdData?.data?[0].businessName)
        vc.postAsUniData = postAdData
        vc.currentTab = currentTabBar
        navigationController?.pushViewController(vc, animated: true)
    }
    
}
//MARK:- TABLE VIEW DATA SOURCE
extension OffersViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == popupTableView {
                   return texts.count
               } else {
            print(dataSource?.data?.data?.count)
            return dataSource?.data?.data?.count ?? 0
               }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == popupTableView {
            let cell = UITableViewCell(style: .default, reuseIdentifier: nil)
                          cell.textLabel?.text = self.texts[(indexPath as NSIndexPath).row]
                       cell.textLabel?.textColor = UIColor.init(hexString: Appcolors.text)
                           return cell
        } else {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DiscountTableViewCell", for: indexPath) as! DiscountTableViewCell
      //  cell.collectionViewDataSource = nil
           // cell.collectionView.reloadData()
            cell.delegate = self
            cell.selectionType = .offer
            cell.postType = .offer
            return cell.DiscountTVC(cell: cell, indexPath: indexPath, dataSource: dataSource?.data?.data)
           
        }
    }
    @objc func ratingPopUp() {
//        popingBottomVC(id: 0, indexToReload: "", selectionType: 0, starredValue: .offer)
       }
//MARK:- RATING DETAILS
     func ratingDetails() {
           let vc = UIStoryboard.timeLineStoryboard.instantiateRatingDetailsVC()
           navigationController?.pushViewController(vc, animated: true)
          }
//MARK:- THOUGHTS DETAILS
    func thoughtsDetails(id: Int,index: IndexPath) {
          let vc = UIStoryboard.timeLineStoryboard.instantiateThoughtsVC()
        vc.commentId = id
        vc.index = index
        vc.delegate = self
       navigationController?.pushViewController(vc, animated: true)
      }
    
        
    func popingBottomVC(id: Int, indexToReload: Int, selectionType: GraduateDetails,starredValue: String) {
        let vc = HelperFunctions.ratingSelection(id: id, starredVlaue: "", reloadingIndex: indexToReload)
        vc.delegate = self
        vc.selectionType = selectionType
        vc.id = id
        vc.selectedIndex = Int(starredValue)
        vc.indexToReload = indexToReload
        present(vc, animated: true, completion: nil)
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 273
    }
}

//MARK:- TABLE VIEW DELEGATE
extension OffersViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        popover.dismiss()
        if tableView == popupTableView {
            popover.dismiss()
            if texts[indexPath.row] ==  "Share"{
                let shareText = "http://test-graduation1.captainau.com.au/"
                let image = UIImage(named: "iconArrowDown")
                let vc = UIActivityViewController(activityItems: [shareText, image!], applicationActivities: [])
                present(vc, animated: true)
                self.popover.dismiss()
            } else if texts[indexPath.row] == "Edit" {
                let vc = UIStoryboard.timeLineStoryboard.instantiateCreateSpotViewController()
                vc.vcType = .editOffer
                vc.editData = dataSource?.data?.data?[editedIndex.row]
                vc.delegate = self
                navigationController?.pushViewController(vc, animated: true)
                self.popover.dismiss()
            } else if texts[indexPath.row] == "Delete" {
                popUp(type: .deletePost, message: "")
            } else {
                let vc = UIStoryboard.graduateStoryboard.instantiateReportVC()
                vc.reportId = dataSource?.data?.data?[editedIndex.row].internalIdentifier
                vc.view.isOpaque = false
                vc.view.backgroundColor = UIColor.black.withAlphaComponent(0.8)
                vc.modalPresentationStyle = .custom
                self.present(vc, animated: true, completion: nil)
                popover.dismiss()
            }
        }
//        if tableView == popupTableView {
//            popover.dismiss()
//            if indexPath.row == 0 {
//                let shareText = "Hello, world!"
//
//                 let image = UIImage(named: "iconArrowDown")
//                let vc = UIActivityViewController(activityItems: [shareText, image!], applicationActivities: [])
//                    present(vc, animated: true)
//                self.popover.dismiss()
//            } else {
//                let vc = UIStoryboard.graduateStoryboard.instantiateReportVC()
//                vc.view.isOpaque = false
//                vc.view.backgroundColor = UIColor.black.withAlphaComponent(0.8)
//                vc.modalPresentationStyle = .custom
//                self.present(vc, animated: true, completion: nil)
//                popover.dismiss()
//
//            }
//        } else {
//
//        }
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if tableView == popupTableView {
            return
        } else if tableView == tableView{
        let allData = dataSource
                   print(allData?.loadMore ?? false)
                   print(indexPath.row,"indexPath")
                   print(dataSource?.data?.data?.count,"dataCount")
                   guard allData?.loadMore == true, let dataCount = allData?.data?.data?.count, indexPath.row == dataCount - 1 else {return}
                   print(dataCount)
                   let lastSectionIndex = tableView.numberOfSections - 1
                   let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
                   if indexPath.section == lastSectionIndex && indexPath.row == lastRowIndex {
                       // print("this is the last cell")
                       let spinner = UIActivityIndicatorView(style: .gray)
                       spinner.startAnimating()
                       spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
                       self.tableView.tableFooterView = spinner
                       self.tableView.tableFooterView?.isHidden = false
                   }
                   // print(allData.url)
//                   requestingForPortfolio(pageNo: dataSource?.numOfPage ?? 0, isLoadMore: dataSource!.loadMore, showHud: false)
        requestForOffers(pageNo:  dataSource?.numOfPage ?? 0, isLoadMore: dataSource!.loadMore, showHud: false)
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == popupTableView {
            return 40
        } else {
        return  UITableView.automaticDimension
        }
    }
    
}

//MARK:- SCROLL VIEW DELEGATE
//extension OffersViewController: UIScrollViewDelegate {
//    func scrollViewDidScroll(_ scrollView: UIScrollView) {
//        //MARK-: FOR NOT BOUNCING WHILE PULL TO REFRESH
//        if scrollView.contentOffset.y < 0.0 {
//            return
//        }
//
//        if (scrollView.contentOffset.y >= scrollView.contentSize.height - scrollView.frame.size.height) {
//            scrollView.setContentOffset(CGPoint(x: scrollView.contentOffset.x, y: scrollView.contentSize.height - scrollView.frame.size.height), animated: false)
//            //
//        }
//        if keyboardOpen == false {
//        let delta = scrollView.contentOffset.y - lastContentOffset
//        if delta < 0 {
//            currentTabBar?.tabBarHeight = min(50, (currentTabBar?.buttonsContainerHeightConstraintInitialConstant)! - delta)
//
//            self.view.layoutIfNeeded()
//        } else {
//
//            currentTabBar?.tabBarHeight = max(-35, (currentTabBar?.buttonsContainerHeightConstraintInitialConstant)! - delta)
//
//            self.view.layoutIfNeeded()
//
//        }
//        // This makes the + or - number quite small.
//        lastContentOffset = scrollView.contentOffset.y
//        } else {
//
//        }
//    }
//
//    }

//MARK:- DELEGATE DISCOUNT VC
extension OffersViewController: SedingRatingsToTimeLineVC {
    func moreOptionFromDiscountTVC(sender: UIButton, id: Int, isOwnBusiness: Bool, postType: GraduateDetails, index: IndexPath) {
        self.postTypes = postType
        self.editedIndex = index
        if isOwnBusiness == true {
            texts.removeAll()
            texts = ["Share", "Edit","Delete"]
             sharePopOver(sender: sender)
        } else {
            texts.removeAll()
            texts = ["Share", "Report"]
            sharePopOver(sender: sender)
        }
    }
    func commentOutsideDiscountTVC(comment: Comments?, index: IndexPath) {
        dataSource?.data?.data?[index.row].comments?.append(comment!)
        tableView.reloadRows(at: [index], with: .automatic)
    }
    func pushingTOProfileVCFromDisc(id: Int, userType: String, isOwner: Bool, postAsType: String) {
   pushToProfile(id: id, userType: userType, isOwner: isOwner, postAsType: postAsType)
    }
    func feedbackAction(commentId: Int, outsideComment: Bool, index: IndexPath) {
        if outsideComment == false {
            thoughtsDetails(id: commentId, index: index)
               } else {
                   
               }
    }
    func sendingRating(id: Int, starValue: String, selectedindexToReload: Int, selectionType: GraduateDetails) {
        popingBottomVC(id: id, indexToReload: selectedindexToReload, selectionType: selectionType, starredValue: starValue)
    }
    func discratingDetails(ratingId: Int, starredValue: String, starCount: Int) {
        ratingDetails(ratingId: ratingId, starredVlaue: starredValue, starCount: starCount)
    }
    func fullscreen(vc: UIViewController) {
        currentTabBar?.setBar(hidden: true, animated: true)
        present(vc, animated: true, completion: nil)
    }
    func pushToProfile(id: Int, userType: String, isOwner: Bool, postAsType: String) {
           if postAsType == "user" {
                       if isOwner == true {
                                    let vc = UIStoryboard.timeLineStoryboard.instantiateUserProfileVC()
                                    navigationController?.pushViewController(vc, animated: true)
                                    } else {
                                        let vc = UIStoryboard.timeLineStoryboard.instantiateOtherUserProfileVC()
                                  vc.userId = id
                                               navigationController?.pushViewController(vc, animated: true)
                                    }
                       } else {
                           let vc = UIStoryboard.timeLineStoryboard.instantiateGraduateDetailsViewController()
                           vc.businessId = id
                           navigationController?.pushViewController(vc, animated: true)
                       }
       }
}
//MARK:- DELEGATE RATING VC
extension OffersViewController: SendingActionFromRatingVC {
    func sendingActionWithType(type: RatingActionTypes, id: Int, reloadingIndex: Int, ratingNum: Int, selectionType: GraduateDetails) {
        switch type {
        case .requestforRating:
            print(type,id,reloadingIndex,ratingNum,selectionType)
            ratingAfterApiCallOFOffer(reloadingIndex: reloadingIndex, ratingNum: ratingNum)
            reqestForPostRating(id: id, reloadingIndex: reloadingIndex, ratingNum: ratingNum, selectionType: .offer)
        case .reuestForUnratingPost:
           unratingAfterApiCallOfOffer(reloadingIndex: reloadingIndex, ratingNum: ratingNum)
            reqestForPostUnRating(id: id, reloadingIndex: reloadingIndex, ratingNum: ratingNum, selectionType: .offer)
        }
    }
    func reqestForPostRating(id: Int,reloadingIndex: Int,ratingNum: Int,selectionType: GraduateDetails) {
    var headers: [String : Any]?
    if let token = UserDefaultsHandler.getUDValue(key: UDkey.token) as? String {
        if headers == nil {
            headers = ["Authorization": "Bearer \(token)", "Accept": "application/json"]
        } else {
            headers!["Authorization"] = "Bearer \(token)"
            headers!["Accept"] = "application/json"
        }
    }
    let url = (GraduateUrl.ratePostURL + "\(id)")
     print(url,ratingNum,selectionType)
    Alamofire.request(url,
                      method: .post,
                      parameters: ["star_value": "\(ratingNum + 1)"],headers: (headers as! HTTPHeaders))
        .validate()
        .responseJSON { [weak self]response in
            guard let strongSelf = self else { return }
            if let value = response.result.value as? [String : Any] {
                print(value)
//                strongSelf.ratingAfterApiCallOFOffer(reloadingIndex: reloadingIndex, ratingNum: ratingNum)
                
            }
            
    }
    
    
}
    func reqestForPostUnRating(id: Int,reloadingIndex: Int,ratingNum: Int,selectionType: GraduateDetails) {
           var headers: [String : Any]?
           if let token = UserDefaultsHandler.getUDValue(key: UDkey.token) as? String {
               if headers == nil {
                   headers = ["Authorization": "Bearer \(token)", "Accept": "application/json"]
               } else {
                   headers!["Authorization"] = "Bearer \(token)"
                   headers!["Accept"] = "application/json"
               }
           }
           let url = (GraduateUrl.unratePostURL + "\(id)")
           print(url,ratingNum,selectionType)
           Alamofire.request(url,method: .delete,headers: (headers as! HTTPHeaders))
               .validate()
               .responseJSON { [weak self]response in
                   guard let strongSelf = self else { return }
                   if let value = response.result.value as? [String : Any] {
                      
                       print(value)
                       print(reloadingIndex)
//                    strongSelf.unratingAfterApiCallOfOffer(reloadingIndex: reloadingIndex, ratingNum: ratingNum)
                       
                   }
                   
           }
    }
    func unratingAfterApiCallOfOffer(reloadingIndex: Int, ratingNum: Int) {
        dataSource?.data?.data?[reloadingIndex].hasStarred = false
        if  dataSource?.data?.data?[reloadingIndex].starsCount ?? 0 > 0 {
            dataSource?.data?.data?[reloadingIndex].starsCount! -= 1
        } else {
            
        }
        dataSource?.data?.data?[reloadingIndex].starredValue = "\(0)"
        tableView.reloadData()
    }
    func ratingAfterApiCallOFOffer(reloadingIndex: Int, ratingNum: Int) {
        if dataSource?.data?.data?[reloadingIndex].hasStarred == true {
           
        } else {
            
            dataSource?.data?.data?[reloadingIndex].starsCount! += 1
        }
        dataSource?.data?.data?[reloadingIndex].starredValue = "\((ratingNum + 1))"
        print(reloadingIndex)
        dataSource?.data?.data?[reloadingIndex].hasStarred = true
        tableView.reloadData()
    }
}
extension OffersViewController: SendingMultipleSeclections {
    func postAsActionFromInside(value: Int?, bool: Bool, index: Int?, type: CreateSpotVCTypes, posterIds: [Int], postData: CreateEventModel) {
        
    }
    func gettingTheSelectedValueFromTheList(value: Int?, bool: Bool, index: Int?, type: CreateSpotVCTypes, posterIds: [Int]) {
        
    }
   
    
    func gettingTheSelectedValueFromTheList(value: Int, bool: Bool, index: Int?, type: CreateSpotVCTypes) {
        
    }
    
    func sendingMultipleVlaues(values: [String]) {
        
    }
    
    func sendingDiscountType(value: String) {
        
    }
    
    func sendingEvnetType(value: String, type: String) {
        
    }
    
    func sendingUniCategory(value: String) {
        
    }
    
    
    
    func gettingSelectedVaueFromProfileMoreBtn(value: Int, bool: Bool, index: Int) {
        
    }
    
    func gettingFullScreenCoverPic() {
        
    }
    
    func sendingMediaType(value: Int) {
        
    }
    
    func sendingUserId(id: Int, index: Int) {
        
    }
}
//MARK:- SCROLL VIEW DELEGATE
extension OffersViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        //MARK-: FOR NOT BOUNCING WHILE PULL TO REFRESH
        if scrollView.contentOffset.y < 0.0 {
            return
        }
        
        if (scrollView.contentOffset.y >= scrollView.contentSize.height - scrollView.frame.size.height) {
            scrollView.setContentOffset(CGPoint(x: scrollView.contentOffset.x, y: scrollView.contentSize.height - scrollView.frame.size.height), animated: false)
            //
        }
        if keyboardOpen == false {
            let delta = scrollView.contentOffset.y - lastContentOffset
            if delta < 0 {
                currentTabBar?.tabBarHeight = min(50, (currentTabBar?.buttonsContainerHeightConstraintInitialConstant)! - delta)
                
                //self.view.layoutIfNeeded()
            } else {
                
                currentTabBar?.tabBarHeight = max(-35, (currentTabBar?.buttonsContainerHeightConstraintInitialConstant)! - delta)
                
                //  self.view.layoutIfNeeded()
                
            }
            // This makes the + or - number quite small.
            lastContentOffset = scrollView.contentOffset.y
        } else {
            
        }
    }
}
//MARK:- THOUGHTS VC DELEGATES
extension OffersViewController: SendingDataFromThoughtsVC {
    func sendingData(index: IndexPath, thoughtsCount: Int, commentsData: [Comments]) {
        dataSource?.data?.data?[index.row].comments = commentsData
        tableView.reloadRows(at: [index], with: .automatic)
    }
    
}
//MARK:- CREATE SPOT DELEGATE
extension OffersViewController: ActionFromCreateSpotVC {
    func reloadTableViewData() {
    }
    func reloadWithType(type: ErrorType) {
     
    }
}
//MARK:- ERROR CHAT DELEGATES
extension OffersViewController: SendingActionFromErrorChat {
    func sendingAction() {
        
    }
    
    func sendingCancelAction() {
        
    }
    
    func adctionwithType(type: ErrorType) {
        requestToDeletePost()
    }
    
    
}

