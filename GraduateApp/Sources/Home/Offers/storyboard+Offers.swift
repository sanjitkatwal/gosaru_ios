//
//  storyboard+Offers.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 6/24/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import Foundation
import UIKit
extension UIStoryboard {
private struct Constants {
    static let offersStoryboard = "Offers"
    static let offersIdentifier = "OffersViewController"
   
}
static var offersStoryboard: UIStoryboard {
    return UIStoryboard(name: Constants.offersStoryboard, bundle: nil)
}
func instantiateOffersViewController() -> OffersViewController {
    guard let viewController = UIStoryboard.offersStoryboard.instantiateViewController(withIdentifier: Constants.offersIdentifier) as? OffersViewController else {
        fatalError("Couldn't instantiate OffersViewController")
    }
    return viewController
}
}
