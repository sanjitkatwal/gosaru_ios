//
//  SearchDetailsViewController.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 12/9/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import UIKit
import AVFoundation
import Popover
import Alamofire
class SearchDetailsViewController: UIViewController {
    var postID: Int? {
        didSet {
        
            requestForDetails()
        }
    }
    @IBOutlet var tableView: UITableView!
    var postTypes: GraduateDetails? //used for the editing the post
    var editedIndex: IndexPath! //index path that is edited
    var popupTableView = UITableView(frame: CGRect(x: 0, y: 0, width: 90, height: 120))
    var texts = ["Share", "Edit", "Delete"]
    var popover: Popover!
    fileprivate var popoverOptions: [PopoverOption] = [
        .type(.left),
        .blackOverlayColor(UIColor(white: 0.0, alpha: 0.6))
    ]
    var data: SearchDetailsResponseModel? {
        didSet {
            tableView.reloadData()
        }
    }
    var dataSource = [uniMyPostData]()
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        registeringViews()
        // Do any additional setup after loading the view.
        
    }
    //MARK: REGISTERING TABLE VIEW CELLS
    func registeringViews() {
        tableView.register(UINib(nibName: "PostCreationTableViewCell", bundle: nil), forCellReuseIdentifier: "PostCreationTableViewCell")
        tableView.register(UINib(nibName: "PortFolioTableViewCell", bundle: nil), forCellReuseIdentifier: "PortFolioTableViewCell")
        tableView.register(UINib(nibName: "EventTableViewCell", bundle: nil), forCellReuseIdentifier: "EventTableViewCell")
        tableView.register(UINib(nibName: "DiscountTableViewCell", bundle: nil), forCellReuseIdentifier: "DiscountTableViewCell")
        tableView.register(UINib(nibName: "LiveTableViewCell", bundle: nil), forCellReuseIdentifier: "LiveTableViewCell")
        //tableView.bounces = false
        //        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        //        refreshControl.addTarget(self, action: #selector(self.refresh(_:)), for: .valueChanged)
        //          tableView.addSubview(refreshControl) // not required when using UITableViewController
        
        
    }
    
//MARK:- REQUEST FOR DETAILS BY ID
    func requestForDetails() {
        SearchDetailsResponse.requestToPostById(id: postID ?? 0, showHud: true) { [weak self](response) in
            guard let strongSelf = self else { return }
            strongSelf.data = response
            strongSelf.dataSource.append(response!.data!)
            print(strongSelf.dataSource)
            strongSelf.tableView.reloadData()
        }
    }
//MARK:- SHARE POP OVER
    func sharePopOver(sender: UIButton) {
        popupTableView.reloadData()
        if texts.count == 2 {
            popupTableView.frame = CGRect(x: 0, y: 0, width: 90, height: 80)
            
        } else {
            popupTableView.frame = CGRect(x: 0, y: 0, width: 90, height: 120)
            
        }
        popupTableView.delegate = self
        popupTableView.dataSource = self
        popupTableView.isScrollEnabled = false
        self.popover = Popover(options: self.popoverOptions)
        self.popover.willShowHandler = {
            print("willShowHandler")
        }
        self.popover.didShowHandler = {
            print("didDismissHandler")
        }
        self.popover.willDismissHandler = {
            print("willDismissHandler")
        }
        self.popover.didDismissHandler = {
            print("didDismissHandler")
        }
        
        self.popover.show(popupTableView, fromView: sender)
    }
//MARK:- THOUGTS DETAILS
    func thoughtsDetails(commentId: Int, index: IndexPath!) {
       let vc = UIStoryboard.timeLineStoryboard.instantiateThoughtsVC()
       vc.commentId = commentId
       vc.index = index
       vc.delegate = self
       navigationController?.pushViewController(vc, animated: true)
   }
    func popingBottomVC(id: Int, indexToReload: Int, selectionType: GraduateDetails,starredValue: String) {
        let vc = HelperFunctions.ratingSelection(id: id, starredVlaue: "", reloadingIndex: indexToReload)
        vc.delegate = self
        vc.selectionType = selectionType
        vc.id = id
        vc.selectedIndex = Int(starredValue)
        vc.indexToReload = indexToReload
        present(vc, animated: true, completion: nil)
    }
    func ratingDetails(ratingId: Int, starredVlaue: String, starCount: Int) {
         let vc = UIStoryboard.timeLineStoryboard.instantiateRatingDetailsVC()
         vc.ratingId = ratingId
     vc.starredValue = starredVlaue
     vc.starCount = starCount
         navigationController?.pushViewController(vc, animated: true)
        }
}
//MARK:- TABLE DATASOURCE
extension SearchDetailsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }

    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //MARK:- POST CREATION CELL
        if dataSource[indexPath.row].portfolio != nil {
            let cell = tableView.dequeueReusableCell(withIdentifier: "PortFolioTableViewCell", for: indexPath) as! PortFolioTableViewCell
        
//            var portData = dataSource?.data?.data?.filter({ $0.portfolio != nil })
            cell.subHeaderlabel.text = dataSource[indexPath.row].portfolio?.addressString
            cell.postType = .portfolio
           // cell.delegate = self
            cell.selectionType = .myPost
            return cell.portFolioCell(cell: cell, indexPath: indexPath, dataSource: dataSource)
        } else if dataSource[indexPath.row].offer != nil {
            let cell = tableView.dequeueReusableCell(withIdentifier: "DiscountTableViewCell", for: indexPath) as! DiscountTableViewCell
            
            cell.delegate = self
            cell.selectionType = .offer
            cell.postType = .offer
            return cell.DiscountTVC(cell: cell, indexPath: indexPath, dataSource: dataSource)
        } else if dataSource[indexPath.row].event != nil {
            let cell = tableView.dequeueReusableCell(withIdentifier: "EventTableViewCell", for: indexPath) as! EventTableViewCell
          
            cell.postType = .gradSpot
          //  cell.delegate = self
            cell.selectionType = .gradSpot
            cell.eventTypeLabel.text = dataSource[indexPath.row].event?.eventType
            return cell.eventTVC(cell: cell, indexPath: indexPath, dataSource: dataSource)
        } else if dataSource[indexPath.row].event == nil && dataSource[indexPath.row].offer == nil && dataSource[indexPath.row].portfolio == nil && dataSource[indexPath.row].business == nil {
//MARK:- CREATE POST WITH GALLERIES VIDEOS CELL
            print(dataSource[indexPath.row].galleries?.count)
            
            if dataSource[indexPath.row].galleries?.count != 0 {
                
//MARK:- POST CREATION GALLERTIES VIDEO POST
                if let index = dataSource[indexPath.row].galleries?.firstIndex(where: { $0.mediaType == "Video"
                }) {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "LiveTableViewCell", for: indexPath) as! LiveTableViewCell
                    cell.postType = .myPost
                    cell.index = indexPath
                    //cell.postData = dataSource?.data?.data?[indexPath.row]
                   // cell.delegate = self
                    cell.videoType = .postVideo
                    cell.starCount = dataSource[indexPath.row].starsCount
                   
                     cell.starreVlaue = dataSource[indexPath.row].starredValue
                    let newDate = HelperFunctions.timeInterval(timeAgo: dataSource[indexPath.row].createdAt ?? "")
                                           print(newDate)
                    cell.timeStampLabel.text = newDate
                    cell.universityId = dataSource[indexPath.row].internalIdentifier
                   
                    if dataSource[indexPath.row].postingAsType == "business" {
                          cell.name = dataSource[indexPath.row].postingAs?.businessName
                    } else if dataSource[indexPath.row].postingAsType == "user" {
                         cell.name = dataSource[indexPath.row].postingAs?.name
                    }
                    if dataSource[indexPath.row].hasStarred == true {
                          cell.ratingBtn.setImage(UIImage(named: "star3"))
                    } else {
                         cell.ratingBtn.setImage(UIImage(named: "star"))
                    }
                  
                    cell.selectionType = .myPost
                    cell.locationLabel.text = dataSource[indexPath.row].business?.businessName
                    cell.viewersLabel.text = "\(dataSource[indexPath.row].viewsCount ?? 0)"
                    //cell.videoType = .postVideo
                    cell.captionLabel.text = dataSource[indexPath.row].message
                    
                    let filrurls = NSURL(string: (GraduateUrl.imageUrl + (dataSource[indexPath.row].galleries?[index].filepath ?? "")))
                    print(filrurls)
                    
                    //                            let url = (GraduateUrl.imageUrl + (dataSource?.myPost?.data?[indexPath.row].galleries?[index].filepath ?? ""))
                    let firstAsset = AVURLAsset(url: filrurls! as URL)
                    cell.videoPlayer.videoAssets = [firstAsset]
                    //                            let newDate = HelperFunctions.getDate(input: dataSource?[indexPath.row].portfolio?.business?.createdAt ?? "")
                    // cell.timeStampLabel.text = newDate?.timeAgo()
                    //cell.timeStampLabel.text = newDate
                    cell.ratingNumLabel.text = "\(dataSource[indexPath.row].starsCount ?? 0)"
                    cell.thoughtsNumLabel.text = "\(dataSource[indexPath.row].comments?.count ?? 0)"
                    cell.proImage.setURLImage(imageURL: (GraduateUrl.imageUrl + (dataSource[indexPath.row].postingAs?.profile?.profileImage ?? "")))
                    return cell
                    
                }
//MARK: GALLERY IMAGE POST
                if let _ = dataSource[indexPath.row].galleries?.firstIndex(where: { $0.mediaType == "Image"
                }) {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "PostCreationTableViewCell", for: indexPath) as! PostCreationTableViewCell
                    cell.postType = .myPost
                    cell.delegate = self
                    cell.imageDataSource = dataSource[indexPath.row].galleries ?? []
                    cell.selectionType = .myPost
                    return cell.postTVC(cell: cell, indexPath: indexPath, dataSource: dataSource)
                }
            } else if dataSource[indexPath.row].galleries?.count == 0{
                if dataSource[indexPath.row].hasLiveEnded == true {
                 
                   //MARK:- MYPOST CELL WITH LIVE TYPE VIDEOS
                   print("")
                   let cell = tableView.dequeueReusableCell(withIdentifier: "LiveTableViewCell", for: indexPath) as! LiveTableViewCell
                    cell.videoType = .liveVideo
                   cell.name = dataSource[indexPath.row].postingAs?.name
                   
                   //cell.delegate = self
                   cell.selectionType = .myPost
                   let newDate = HelperFunctions.timeInterval(timeAgo: dataSource[indexPath.row].createdAt ?? "")
                                          print(newDate)
                   cell.timeStampLabel.text = newDate
                 
                   cell.locationLabel.text = dataSource[indexPath.row].business?.businessName
                   cell.viewersLabel.text = "\(dataSource[indexPath.row].viewsCount ?? 0)"
                  let filrurls = NSURL(string: (GraduateUrl.imageUrl + (dataSource[indexPath.row].liveVideoMp4 ?? "")))
                             let firstAsset = AVURLAsset(url: filrurls! as URL)
                   cell.videoPlayer.videoAssets = [firstAsset]
                   cell.postData = dataSource[indexPath.row]
                    cell.ratingNumLabel.text = "\(dataSource[indexPath.row].starsCount ?? 0)"
                    cell.thoughtsNumLabel.text = "\(dataSource[indexPath.row].comments?.count ?? 0)"
                    cell.commentId = dataSource[indexPath.row].internalIdentifier
                    cell.index = indexPath
                   return cell
                   
               } else if dataSource[indexPath.row].hasLiveEnded == false {
                   let cell = tableView.dequeueReusableCell(withIdentifier: "PostCreationTableViewCell", for: indexPath) as! PostCreationTableViewCell
                   print(dataSource[indexPath.row].hasLiveEnded)
                   cell.postType = .myPost
                   //cell.delegate = self
                   cell.selectionType = .myPost
                   return cell.postTVC(cell: cell, indexPath: indexPath, dataSource: dataSource)
               } else {
                   print(dataSource[indexPath.row].hasLiveEnded)
                return UITableViewCell()
               }
            }
            
        }
    
return UITableViewCell()
}
    
   
    }
//MARK:- TABLE VIEW DELEGATE
extension SearchDetailsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == popupTableView {
            return 40
        } else {
            return  UITableView.automaticDimension
        }
        
        
    }
}
//MARK:- DICOUNT VC DELEGATE
extension SearchDetailsViewController: SedingRatingsToTimeLineVC {
    func moreOptionFromDiscountTVC(sender: UIButton, id: Int, isOwnBusiness: Bool, postType: GraduateDetails, index: IndexPath) {
        if isOwnBusiness == true {
            sharePopOver(sender: sender)
        } else {
            texts.removeAll()
            texts = ["Share", "Report"]
            sharePopOver(sender: sender)
        }
    }
   
    func commentOutsideDiscountTVC(comment: Comments?, index: IndexPath) {
        dataSource[index.row].comments?.append(comment!)
        tableView.reloadRows(at: [index], with: .automatic)
    }
    func pushingTOProfileVCFromDisc(id: Int, userType: String, isOwner: Bool, postAsType: String) {
        HelperFunctions.pushToProfile(id: id, userType: userType, isOwner: isOwner, postAsType: postAsType, nav: self.navigationController)
       // pushToProfile(id: id, userType: userType, isOwner: isOwner, postAsType: postAsType)
    }
    func feedbackAction(commentId: Int, outsideComment: Bool, index: IndexPath) {
        if outsideComment == false {
                  thoughtsDetails(commentId: commentId, index: index)
                  
              } else {
                  
              }
    }
    

    func sendingRating(id: Int, starValue: String, selectedindexToReload: Int, selectionType: GraduateDetails) {
        popingBottomVC(id: id, indexToReload: selectedindexToReload, selectionType: selectionType, starredValue: starValue)
    }
    
    
    
    
    
    func discratingDetails(ratingId: Int, starredValue: String, starCount: Int) {
        ratingDetails(ratingId: ratingId, starredVlaue: starredValue, starCount: starCount)
    }
    
    
    func fullscreen(vc: UIViewController) {
        currentTabBar?.setBar(hidden: true, animated: true)
        present(vc, animated: true, completion: nil)
    }
    
    
    
    //    func popingMoreOP() {
    //        texts.removeAll()
    //        texts = ["Share", "Report"]
    //        sharePopOver(sender: sender)
    //    }
    
    
}
//MARK:- THOUGHTS DELEGATE
extension SearchDetailsViewController: SendingDataFromThoughtsVC {
    func sendingData(index: IndexPath, thoughtsCount: Int, commentsData: [Comments]) {
        print(index,thoughtsCount,commentsData)
        dataSource[index.row].comments = commentsData
        tableView.reloadRows(at: [index], with: .automatic)
    }
}
//MARK:- DELEGATE RATING ACTION
extension SearchDetailsViewController: SendingActionFromRatingVC {
    func sendingActionWithType(type: RatingActionTypes, id: Int, reloadingIndex: Int, ratingNum: Int, selectionType: GraduateDetails) {
        switch type{
        case .requestforRating:
           ratingAfterApiCallOfEvent(reloadingIndex: reloadingIndex, ratingNum: ratingNum)
            reqestForPostRating(id: id, reloadingIndex: reloadingIndex, ratingNum: ratingNum, selectionType: selectionType)
        case .reuestForUnratingPost:
           unratingAfterApiCallOfEvent(reloadingIndex: reloadingIndex, ratingNum: ratingNum)
            reqestForPostUnRating(id: id, reloadingIndex: reloadingIndex, ratingNum: ratingNum, selectionType: selectionType)
        default:
            break
        }
    }
//MARK:- RATE POST
          func reqestForPostRating(id: Int,reloadingIndex: Int,ratingNum: Int,selectionType: GraduateDetails) {
              var headers: [String : Any]?
              if let token = UserDefaultsHandler.getUDValue(key: UDkey.token) as? String {
                  if headers == nil {
                      headers = ["Authorization": "Bearer \(token)", "Accept": "application/json"]
                  } else {
                      headers!["Authorization"] = "Bearer \(token)"
                      headers!["Accept"] = "application/json"
                  }
              }
              let url = (GraduateUrl.ratePostURL + "\(id)")
               print(url,ratingNum,selectionType)
              Alamofire.request(url,
                                method: .post,
                                parameters: ["star_value": "\(ratingNum + 1)"],headers: (headers as! HTTPHeaders))
                  .validate()
                  .responseJSON { [weak self]response in
                      guard let strongSelf = self else { return }
                      if let value = response.result.value as? [String : Any] {
                        //strongSelf.ratingAfterApiCallOfEvent(reloadingIndex: reloadingIndex, ratingNum: ratingNum)
//                        switch selectionType {
//                        case .gradSpot:
//                             strongSelf.ratingAfterApiCallOfEvent(reloadingIndex: reloadingIndex, ratingNum: ratingNum)
//                        case .offer:
//                            ratingAfterApiCallOFOffer(reloadingIndex: reloadingIndex, ratingNum: ratingNum)
//                        default:
//                            break
//                        }
                      
                          
                      }
                      
              }
          }
       
//MARK:- REQUEST FOR UNRATING POST
       func reqestForPostUnRating(id: Int,reloadingIndex: Int,ratingNum: Int,selectionType: GraduateDetails) {
           var headers: [String : Any]?
           if let token = UserDefaultsHandler.getUDValue(key: UDkey.token) as? String {
               if headers == nil {
                   headers = ["Authorization": "Bearer \(token)", "Accept": "application/json"]
               } else {
                   headers!["Authorization"] = "Bearer \(token)"
                   headers!["Accept"] = "application/json"
               }
           }
           let url = (GraduateUrl.unratePostURL + "\(id)")
           print(url,ratingNum,selectionType)
           Alamofire.request(url,method: .delete,headers: (headers as! HTTPHeaders))
               .validate()
               .responseJSON { [weak self]response in
                   guard let strongSelf = self else { return }
                   if let value = response.result.value as? [String : Any] {
                   // strongSelf.unratingAfterApiCallOfEvent(reloadingIndex: reloadingIndex, ratingNum: ratingNum)
//                    switch selectionType {
//                    case .gradSpot:
//                         strongSelf.unratingAfterApiCallOfEvent(reloadingIndex: reloadingIndex, ratingNum: ratingNum)
//                    case .offer:
//                        strongSelf.unratingAfterApiCallOfOffer(reloadingIndex: reloadingIndex, ratingNum: ratingNum)
//                    default:
//                        break
//                    }
                      
                       print(value)
                       print(reloadingIndex)
                       
                   }
                   
           }
       }
       func ratingAfterApiCallOfEvent(reloadingIndex: Int, ratingNum: Int) {
           print(dataSource[reloadingIndex].hasStarred)
        if dataSource[reloadingIndex].hasStarred == true {
              
           } else {
               
               dataSource[reloadingIndex].starsCount! += 1
           }
           dataSource[reloadingIndex].starredValue = "\((ratingNum + 1))"
           print(dataSource[reloadingIndex].starredValue)
           print(reloadingIndex)
           dataSource[reloadingIndex].hasStarred = true
           tableView.reloadData()
       }
       func unratingAfterApiCallOfEvent(reloadingIndex: Int, ratingNum: Int) {
           dataSource[reloadingIndex].hasStarred = false
        if  dataSource[reloadingIndex].starsCount ?? 0 > 0 {
               dataSource[reloadingIndex].starsCount! -= 1
           } else {
               
           }
           dataSource[reloadingIndex].starredValue = "\(0)"
           tableView.reloadData()
       }
//    func ratingAfterApiCallOFOffer(reloadingIndex: Int, ratingNum: Int) {
//        if dataSource?.data?.data?[reloadingIndex].hasStarred == true {
//            return
//        } else {
//
//            dataSource?.data?.data?[reloadingIndex].starsCount! += 1
//        }
//        dataSource?.Offer?.data?[reloadingIndex].starredValue = "\((ratingNum + 1))"
//        print(reloadingIndex)
//        dataSource?.Offer?.data?[reloadingIndex].hasStarred = true
//        tableView.reloadData()
//    }
//    func unratingAfterApiCallOfOffer(reloadingIndex: Int, ratingNum: Int) {
//        dataSource?.Offer?.data?[reloadingIndex].hasStarred = false
//        if  dataSource?.Offer?.data?[reloadingIndex].starsCount ?? 0 > 0 {
//            dataSource?.Offer?.data?[reloadingIndex].starsCount! -= 1
//        } else {
//
//        }
//        dataSource?.Offer?.data?[reloadingIndex].starredValue = "\(0)"
//        tableView.reloadData()
//    }
}
//MARK: RECEIVING RATING FROM THE PCTVC
extension SearchDetailsViewController: SendingRatingFromPCTVC {
    func moreOptionFromPostTVC(sender: UIButton, id: Int, index: IndexPath, cell: PostCreationTableViewCell, isOwner: Bool, postType: GraduateDetails) {
        postTypes = postType
        editedIndex = index
        if isOwner == true {
            texts.removeAll()
            texts = ["Share", "Edit","Delete"]
            sharePopOver(sender: sender)
        } else {
            texts.removeAll()
            texts = ["Share", "Report"]
            sharePopOver(sender: sender)
        }
    }
    
    func ratingDetailFromPCTVC(ratingId: Int, starredValue: String, starCount: Int) {
        ratingDetails(ratingId: ratingId, starredVlaue: starredValue, starCount: starCount)
      
    }
    
    func outsideCommentPostTVC(comment: Comments?, index: IndexPath) {
        print(comment,index)
        dataSource[index.row].comments?.append(comment!)
        tableView.reloadRows(at: [index], with: .automatic)
    }
    
    func pushingTOProfileVC(id: Int, userType: String, isOwner: Bool, postAsType: String) {
       pushToProfile(id: id, userType: userType, isOwner: isOwner, postAsType: postAsType)
    }
   
    func PCTVCRating(id: Int, starValue: String, selectedindexToReload: Int, selectionType: GraduateDetails) {
         popingBottomVC(id: id, indexToReload: selectedindexToReload, selectionType: selectionType, starredValue: starValue)
    }
    
    func feedbackActionPCTVC(commentId: Int, outsideComment: Bool, index: IndexPath) {
        if outsideComment == false {
            //help
            thoughtsDetails(commentId: commentId, index: index)
        } else {
            
        }
    }
   
    func thoughtsAction() {
       
//        let vc = UIStoryboard.timeLineStoryboard.instantiateThoughtsVC()
//        navigationController?.pushViewController(vc, animated: true)
    }
    func fullScreenImage(vc: UIViewController) {
        currentTabBar?.setBar(hidden: true, animated: true)
        present(vc, animated: true, completion: nil)
    }
    func PCTVCRating() {
        popingBottomVC(id: 0, indexToReload: 0, selectionType: .myPost, starredValue: "")
    }
//MARK:- PUSH TO PROFILE FUNC
    func pushToProfile(id: Int, userType: String, isOwner: Bool, postAsType: String) {
        HelperFunctions.pushToProfile(id: id, userType: userType, isOwner: isOwner, postAsType: postAsType, nav: self.navigationController)
//        if postAsType == "user" {
//                    if isOwner == true {
//                                 let vc = UIStoryboard.timeLineStoryboard.instantiateUserProfileVC()
//                                 navigationController?.pushViewController(vc, animated: true)
//                                 } else {
//                                     let vc = UIStoryboard.timeLineStoryboard.instantiateOtherUserProfileVC()
//                               vc.userId = id
//                                            navigationController?.pushViewController(vc, animated: true)
//                                 }
//                    } else {
//                        let vc = UIStoryboard.timeLineStoryboard.instantiateGraduateDetailsViewController()
//                        vc.businessId = id
//                        navigationController?.pushViewController(vc, animated: true)
//                    }
    }
}


    

