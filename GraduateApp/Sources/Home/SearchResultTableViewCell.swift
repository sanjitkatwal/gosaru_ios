//
//  SearchResultTableViewCell.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 12/8/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import UIKit

class SearchResultTableViewCell: UITableViewCell {

    @IBOutlet var backView: CustomView!
    @IBOutlet var imageContainerView: UIView!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var mainImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        HelperFunctions.addingShadowInImage(avatarImageView: mainImage, cornerRadius: 4, containerView: imageContainerView)
//        backView.removeBorder(toSide: .Top)
//        backView.removeBorder(toSide: .Bottom)
        backView.addBorder(toSide: .Top, withColor: UIColor.lightGray.cgColor, andThickness: 0.5)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
