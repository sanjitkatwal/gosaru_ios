//
//  SearchDetailsResponse.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 12/10/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import Foundation
import ObjectMapper

class SearchDetailsResponse: DefaultResponse {
   
override func mapping(map: Map) {
  super.mapping(map: map)

}
// MARK: - GET MESSAGE CONVERSATION
    class func requestToPostById(id: Int,showHud: Bool,completionHandler:@escaping ((SearchDetailsResponseModel?) -> Void)) {
     
        APIManager(urlString: (GraduateUrl.navigatePostURL + "\(id)"),method: .get).handleResponse(showProgressHud: showHud,completionHandler: { (response: SearchDetailsResponseModel) in
              print(response)
                  completionHandler(response)
              }, failureBlock: {
                completionHandler(nil)
              })
      }
}

 class SearchDetailsResponseModel: DefaultResponse {
   

    // MARK: Declaration for string constants to be used to decode and also serialize.
    internal let kSearchDetailsResponseDataKey: String = "data"


    // MARK: Properties
    public var data: uniMyPostData?



    // MARK: ObjectMapper Initalizers
    /**
    Map a JSON object to this class using ObjectMapper
    - parameter map: A mapping from ObjectMapper
    */
  

    /**
    Map a JSON object to this class using ObjectMapper
    - parameter map: A mapping from ObjectMapper
    */
    public override func mapping(map: Map) {
        data <- map[kSearchDetailsResponseDataKey]

    }
}
