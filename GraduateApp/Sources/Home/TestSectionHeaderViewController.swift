//
//  TestSectionHeaderViewController.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 8/5/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import UIKit


class TestSectionHeaderViewController: UIViewController {
    @IBOutlet weak var height: NSLayoutConstraint!
    
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    @IBOutlet weak var tableView: UITableView!
 var range: Range<CGFloat> = (-87..<0)
    let minimumConstantValue = CGFloat(-87)
     let maximumConstantValue = CGFloat(50)
    let tabbarminimunConstantVlaue = CGFloat(-50)
    private var lastContentOffset: CGFloat = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        print(currentTabBar?.tabBarHeight)
        // Do any additional setup after loading the view.
        let statusBarFrame = UIApplication.shared.statusBarFrame
        let statusBarView = UIView(frame: statusBarFrame)
        self.view.addSubview(statusBarView)
        statusBarView.backgroundColor = .white
        tableView.bounces = false
        tableView.dataSource = self
        tableView.delegate = self
        tableView.reloadData()
        navigationController?.isNavigationBarHidden = true
        registerCells()
        //navigationController?.navigationBar.barTintColor = .blue
    }
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.isNavigationBarHidden = false
        currentTabBar?.tabBarHeight = 50.0
    }
    override func viewDidDisappear(_ animated: Bool) {
        currentTabBar?.setBar(hidden: false, animated: true)
    }
    func registerCells() {
        tableView.register(UINib(nibName: "PostCreationTableViewCell", bundle: nil), forCellReuseIdentifier: "PostCreationTableViewCell")
    }
    


}
extension TestSectionHeaderViewController: UITableViewDataSource {
   
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PostCreationTableViewCell", for: indexPath) as! PostCreationTableViewCell
      return cell
    }
//    func changeTabBar(hidden:Bool, animated: Bool){
//        guard let tabBar = currentTabBar else { return; }
//        if tabBar.isHidden == hidden{ return }
//        let frame = tabBar.frame
//        let offset = hidden ? frame.size.height : -frame.size.height
//        let duration:TimeInterval = (animated ? 0.5 : 0.0)
//        tabBar.isHidden = false
//
//        UIView.animate(withDuration: duration, animations: {
//            tabBar.frame = frame.offsetBy(dx: 0, dy: offset)
//        }, completion: { (true) in
//            tabBar.isHidden = hidden
//        })
//    }
    
    
}
extension TestSectionHeaderViewController: UITableViewDelegate {
   

//    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//        let view = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 18))
//        view.backgroundColor = .red
//        let label = UILabel(frame: CGRect(x: 20, y: 20, width: 50, height: 50))
//        label.text = "TEST TEXT"
//        label.textColor = .white
//
//        self.view.addSubview(view)
//
//        return view
//    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        print(scrollView.contentOffset.y)
        let delta = scrollView.contentOffset.y - lastContentOffset
       if delta < 0 {
          // the value is negative, so we're scrolling up and the view is moving back into view.
          // take whatever is smaller, the constant minus delta, or the upperBound of the range. (0)
          topConstraint.constant = min(topConstraint.constant - delta, range.upperBound)
        } else {
          // the value is positive, so we're scrolling down and the view is moving out of sight.
          // take whatever is "larger," the constant minus delta, or the lowerBound of the range.
          topConstraint.constant = max(range.lowerBound, topConstraint.constant - delta)
        }
        lastContentOffset = scrollView.contentOffset.y
    }
    private func scrollViewDidBeginDragging(_ scrollView: UIScrollView) {
       // Where lastContentOffset is a class variable of type CGFloat
       lastContentOffset = scrollView.contentOffset.y
     }
    
//       func scrollViewDidScroll(_ scrollView: UIScrollView) {
//           let delta = scrollView.contentOffset.y - lastContentOffset
//         if delta < 0 {
//            // the value is negative, so we're scrolling up and the view is moving back into view.
//            // take whatever is smaller, the constant minus delta or 0            topView.isHidden = true
//
//            UIView.animate(withDuration: 1.0, delay: 0, options: .transitionCrossDissolve, animations: {
//               // self.topView.isHidden = false
//                self.topView.backgroundColor = .red
//                self.topView.isOpaque = true
//                self.topView.alpha = 1
//                self.topConstraint.constant = min(self.topConstraint.constant - delta, 0)
//
//               // self.topConstraint.constant = 0
//            }, completion: { finished in
//            })
//
//
// //height.constant = 77
//              //currentTabBar?.tabBarHeight = min((currentTabBar?.buttonsContainerHeightConstraintInitialConstant)! - delta, 0)
//             currentTabBar?.tabBarHeight = min(50, (currentTabBar?.buttonsContainerHeightConstraintInitialConstant)! - delta)
//            print(self.topConstraint.constant, currentTabBar?.tabBarHeight)
//          } else {
//            // the value is positive, so we're scrolling down and the view is moving out of sight.
//            // take whatever is "larger," the constant minus delta, or the minimumConstantValue.
//           // topView.isHidden = true
//          //  topView.backgroundColor = .clear
//           // topView.isOpaque = false
//         //   topView.alpha = 0
//          //  height.constant = 0
//
//            topConstraint.constant = max(minimumConstantValue, topConstraint.constant - delta)
//
//            // topConstraint.constant = -77
//            currentTabBar?.tabBarHeight = max(tabbarminimunConstantVlaue, (currentTabBar?.buttonsContainerHeightConstraintInitialConstant)! - delta)
//             print(topConstraint.constant, currentTabBar?.tabBarHeight)
//           /// print(delta)
//
//
//          }
//           // This makes the + or - number quite small.
//           lastContentOffset = scrollView.contentOffset.y
//       }
//    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
//           // Where lastContentOffset is a class variable of type CGFloat
//           lastContentOffset = scrollView.contentOffset.y
//         }
}

