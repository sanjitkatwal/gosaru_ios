//
//  ChatViewController.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 8/19/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import AMPopTip
import AVFoundation
import DTPhotoViewerController
struct MessageSender {
    var profileImage: String?
    var chattableId: Int?
}

struct NewMessage {
    var data: ChatData?
    var title: String?
}
struct MessageModel {
    var body: String?
    var type: String?
    var createdAt: String?
    var sender: MessageSender?
}
struct ChatModel {
    var paginationData: ChatResponseModel?
    var groupedMessagedData = [[ChatData]]()
    var numOfPage: Int
    var loadMore: Bool
    var totalData: ChatResponseModel?
}

class ChatViewController: UIViewController {
    @IBOutlet weak var topView: UIView!
    var id: Int? {
        didSet {
            //dataSource = []
            print(id)
            
        }
    }
    var loadWillDiplay: Bool = false
    var messgaeDataSource = [MessageModel]()
    var newMessage: ChatData? = ChatData(JSON: ["":""])
    var totalDataCount: Int?
    var pagination: Bool = true
    var dataSource: ChatModel? {
        didSet {
            tableView.reloadData()
        }
    }
    var data: [String : [ChatData]]?
    //var dataSource = [[ChatData]]()
    var sectionTitles = [String]()
    var lastMessage = [String]()
    var recentMessage = String()
    var senderImage: String?
    var conversationId: Int?
    var videos = [URL]()
    var sentImages: [UIImage] = []
    var newMSg = [ChatData]()
    var msg: ChatData?
    var senderText: ChatData?
    var a: ChatData?
    var messgeText: String?
    var message:[Any]? = [] {
        didSet {
            print(message)
            
        }
    }
    @IBOutlet weak var topStackView: UIStackView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var addBtn: UIButton!
    @IBOutlet weak var lastSeenLabel: UILabel!
    @IBOutlet weak var messageTxtView: UITextView!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var sendBtn: UIButton!
    fileprivate var selectedImageIndex: Int = 0
    fileprivate var tableViewCellCoordinator: [Int: IndexPath] = [:]
    var imagePickerController = CustomImagePicker()
    var mediaView: MediapopUp?
    var name: String?
    var userOwnId: Int?
    var lastSeen: String?
    var image: String?
    let popTip = PopTip()
    var userId: Int?
    var chattableType: String?
    var verticalContentOffset: CGPoint? = CGPoint(x: 0, y: 200)
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        print((GraduateUrl.imageUrl + (image ?? "")))
        print(HelperFunctions.timeInterval(timeAgo: lastSeen ?? ""))
        profileImage.setURLImage(imageURL: (GraduateUrl.imageUrl + (image ?? "")))
        print(lastSeen)
        lastSeenLabel.text = "last seen " + HelperFunctions.timeInterval(timeAgo: lastSeen ?? "")
        tableView.dataSource = self
        tableView.delegate = self
        registerView()
        dataSource = ChatModel(paginationData: nil, groupedMessagedData: [[ChatData]](), numOfPage: 1, loadMore: false, totalData: nil)
        getConversationMessages(pageNo: 1, isLoadMore: false, showHud: true)
        registerView()
        tableView.dataSource = self
        tableView.delegate = self
        currentTabBar?.setBar(hidden: true, animated: true)
        attributed()
        userOwnId = UserDefaultsHandler.getUDValue(key: .userID) as? Int
        msg = ChatData(chatSender: ChatSender())
       // senderText?.sender = ChatSender(JSON: ["": ""])
       // a = ChatData(body: "", createdAt: "", userId: 0)
       // a = ChatData()
        senderText = ChatData(chatSender: ChatSender())
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        profileImage.layer.cornerRadius = 4
        // messageTxtView.text = "Type a message here..."
        
        messageTxtView.textColor = .lightGray
        messageTxtView.delegate = self
        backBtn.setImage(UIImage(named: "iconBack"), for: .normal)
        backBtn.tintColor = UIColor.init(hexString: Appcolors.buttons)
        backBtn.imageView?.contentMode = .center
        addBtn.setImage(UIImage(named: "add"), for: .normal)
        addBtn.tintColor = UIColor.init(hexString: Appcolors.buttons)
        
        addBtn.imageView?.contentMode = .center
        //sendBtn.setImage(UIImage(named: "iconSend"), for: .normal)
        // sendBtn.tintColor = UIColor.init(hexString: Appcolors.buttons)
        
        // sendBtn.imageView?.contentMode = .center
        currentTabBar?.setBar(hidden: true, animated: true)
       // currentTabBar?.tabBarHeight = 0
        navigationController?.isNavigationBarHidden = false
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        currentTabBar?.setBar(hidden: false, animated: true)
    }
    func convertDateFormater(_ date: String) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.sZ"
        let date = dateFormatter.date(from: date)
        dateFormatter.dateFormat = "yyyy-MM-dd"
        return  dateFormatter.string(from: date!)
    }
    
    func getTimeByApi(_ date: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.sZ"
        let date = dateFormatter.date(from: date)
        dateFormatter.dateFormat = "HH:mm a"
        return  dateFormatter.string(from: date!)
    }
    
    //MARK:- GET THE CONVERSATION MESSAGES BY ID
    func getConversationMessages(pageNo: Int,isLoadMore: Bool = false, showHud: Bool = true) {
        print(pageNo)
        dataSource?.totalData?.data?.removeAll()
        
        ChatResponse.requestToConversationMessages(id: id ?? 0,pageNo: pageNo , showHud: showHud) { [weak self](response) in
            guard let strongSelf = self else { return }
          
            if !isLoadMore {
                strongSelf.dataSource?.paginationData = response
                strongSelf.dataSource?.totalData = response
                strongSelf.totalDataCount = strongSelf.dataSource?.paginationData?.data?.count
                // strongSelf.tableView.reloadData()
                strongSelf.groupMessages()
            } else {
                strongSelf.dataSource?.paginationData?.data?.removeAll()
                if let allData = response?.data {
                    print(allData)
                    if let count: Int = response?.data?.count {
                        strongSelf.totalDataCount = ((strongSelf.totalDataCount ?? 0) + count)
                    }
                   
                    for data in allData {
                        strongSelf.dataSource?.paginationData?.data?.append(data)
                        //strongSelf.dataSource?.totalData?.data?.append(data)
                        
                    }
                   
                    //strongSelf.tableView.reloadData()
                }
                strongSelf.groupAfterPagination()
            }
            print(strongSelf.dataSource?.paginationData?.data?.count ?? 0,strongSelf.dataSource?.totalData?.data?.count ?? 0, response?.meta?.total ?? 0)
            print(strongSelf.dataSource?.totalData?.meta?.total)
            if (strongSelf.totalDataCount ?? 0) < response?.meta?.total ?? 0 {
                strongSelf.dataSource?.loadMore = true
                strongSelf.dataSource?.numOfPage += 1
            } else {
                strongSelf.tableView.tableHeaderView?.isHidden = true
                strongSelf.pagination = false
                strongSelf.dataSource?.loadMore = false
                print(strongSelf.dataSource?.loadMore)
            }
            //            for item in strongSelf.dataSource?.paginationData?.data ?? []{
            //                strongSelf.messgaeDataSource[0].body = item.body
            //                print(strongSelf.messgaeDataSource[0].body)
            //            }
            //            strongSelf.dataSource?.paginationData?.data?.forEach {
            //
            //                strongSelf.messgaeDataSource.app
            //            }
            //            for (index, element) in dataSource?.paginationData?.data?.enumerated()  {
            //              print("Item \(index): \(element)")
            //            }
            
            
        }
    }
    //MARK:- GROUP MSG AFTER PAGINATION
    func groupAfterPagination() {
        let groupedMessages = Dictionary(grouping: dataSource?.paginationData?.data ?? []) { (element) -> String in
            return (convertDateFormater(element.createdAt ?? ""))
        }
        print(groupedMessages.count)
        //  let sortedKeys = groupedMessages.keys.sorted()
        
        let sortedKeys = groupedMessages.keys.sorted()
        print(sortedKeys)
        let reversedKeys = sortedKeys.reversed()
        //  print(reversedKeys)
        //        groupedMessages.keys.forEach { (key) in
        //            print(key)
        //            sectionTitles.append(key)
        //            print(sectionTitles)
        //            let values = groupedMessages[key]
        //            print(values)
        //            dataSource?.groupedMessagedData.append(values ?? [])
        //           // dataSource?.reversedData.reversed()
        //        }
        
        var datas = [ChatData]()
        dataSource?.groupedMessagedData.reverse()
        print(sectionTitles)
        sectionTitles.reverse()
        reversedKeys.forEach { (key) in
            print(key)
            sectionTitles.append(key)
            let values = groupedMessages[key]
            
            datas.append(contentsOf: values?.reversed() ?? [])
            dataSource?.groupedMessagedData.append(values?.reversed() ?? [])
            print(dataSource)
            // dataSource.append(values ?? [])
        }
        print(sectionTitles)
        sectionTitles.reverse()
        // sectionTitles.reverse()
        // let sections = uniq(source: sectionTitles)
        // print(sections)
        dataSource?.groupedMessagedData.reverse()
        datas.reverse()
        
        // dataSource?.groupedMessagedData.append(datas)
        // dataSource?.groupedMessagedData.reverse()
        
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        var ready = sectionTitles.sorted(by: { dateFormatter.date(from:$0)?.compare(dateFormatter.date(from:$1)!) == .orderedDescending })
        print(dataSource)
        //        tableView.beginUpdates()
        //        tableView.endUpdates()
        //        DispatchQueue.main.async {
        //            self.tableView.layoutIfNeeded()
        //            self.tableView.beginUpdates()
        //            self.tableView.setContentOffset(self.verticalContentOffset!, animated: false)
        //            self.tableView.endUpdates()
        //            self.tableView.reloadData()
        //        }
        
        tableView.reloadData()
        
        
        //  tableView.setContentOffset = CGPointMake(0, verticalContentOffset!)
        // groupedMessages.count,dataSource.groupedMessages.datas
        let scrollSection: Int = (groupedMessages.count)
        tableView.scrollToRow(at: [scrollSection,0], at: .top, animated: false)
    }
    
    //MARK:- GROUP MESSAGES
    func groupMessages() {
        let groupedMessages = Dictionary(grouping: dataSource?.paginationData?.data ?? []) { (element) -> String in
            return (convertDateFormater(element.createdAt ?? ""))
        }
        print(groupedMessages)
        let sortedKeys = groupedMessages.keys.sorted()
        print(sortedKeys)
        let reversedKeys = sortedKeys.reversed()
        reversedKeys.forEach { (key) in
            print(key)
            sectionTitles.append(key)
            let values = groupedMessages[key]
            dataSource?.groupedMessagedData.append(values?.reversed() ?? [])
            
        }
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
       
        dataSource?.groupedMessagedData.reverse()
        sectionTitles.reverse()
        tableView.reloadData()
        let rowCount: Int = dataSource?.groupedMessagedData[sectionTitles.count - 1].count ?? 0
        print(rowCount)
        
        let indexPath: IndexPath = [(dataSource?.groupedMessagedData.count ?? 0) - 1,rowCount - 1]
        tableView.scrollToRow(at: indexPath, at: .top, animated: false)
    }
    
    //MARK:- REMOVING DUPLICATED ITEMS FROM ARRAY
    func uniq<S : Sequence, T : Hashable>(source: S) -> [T] where S.Iterator.Element == T {
        var buffer = [T]()
        var added = Set<T>()
        for elem in source {
            if !added.contains(elem) {
                buffer.append(elem)
                added.insert(elem)
            }
        }
        return buffer
    }
    @IBAction func backBtnAction(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
    }
    @IBAction func addBtnAction(_ sender: UIButton) {
        popTip.shouldDismissOnTap = false
        let windw = UIApplication.shared.windows.first { $0.isKeyWindow }
        var frame = sender.convert(sender.bounds, to: windw)
        frame.origin.y += -8
        frame.origin.x += 30
        popTip.textAlignment = .center
        
        popTip.offset = 0
        popTip.arrowSize = CGSize(width: 0, height: 0)
        popTip.bubbleColor = UIColor.init(hexString: Appcolors.light2)
        
        mediaView = Bundle.main.loadNibNamed(String(describing: MediapopUp.self), owner: self, options: nil)?.first as? MediapopUp
        mediaView?.delegate = self
        mediaView?.frame.size = CGSize(width: 90, height: 30)
        popTip.show(customView: mediaView!, direction: .up
            , in: windw!, from: frame)
        
    }
    
    //MARK:- SEND BUTTON ACTION
    @IBAction func sendBtnAction(_ sender: Any) {
       // senderText = ChatData(map: ChatData)
        if let text = messageTxtView.text {
            if text.isEmpty {
            } else {
                print(messageTxtView.text)
                messgeText = messageTxtView.text
                messageTxtView.text = ""
                print(dataSource)
                dataSource?.paginationData?.data?.removeAll()
                addingNewMessage()
            }
        }
        
    }
    //MARK:- ADDING NEW MESSAGE
    func addingNewMessage() {
       
    
        var currentDateOne = Date.getCurrentDate()
        var currentDateTwo = Date.getCurrentDateTwo()
        print(currentDateOne)
        currentDateOne = currentDateOne + "000000Z"
       // let newData = HelperFunctions.getDate(input: currentDate)
       // print(newData)
        senderText?.body = messgeText
        senderText?.createdAt = currentDateOne
        senderText?.type = "text"
        senderText?.sender?.chatableId = UserDefaultsHandler.getUDValue(key: .userID) as? Int
        msg?.body = messgeText
        msg?.createdAt = currentDateOne
        msg?.type = "text"
        msg?.sender?.chatableId = UserDefaultsHandler.getUDValue(key: .userID) as? Int
        newMSg.append(msg ?? ChatData(chatSender: ChatSender()))
        
        print(a)
        print(senderText?.createdAt,sectionTitles.last)
        if currentDateTwo == sectionTitles.last {
            dataSource?.groupedMessagedData[sectionTitles.count - 1].append(senderText!)
            tableView.reloadData()
            let rowCount: Int = ((dataSource?.groupedMessagedData[sectionTitles.count - 1].count ?? 0) - 1)
            print(rowCount)
            //let index: IndexPath = [sectionTitles.count - 1, rowCount - 1]
            let index: IndexPath = [sectionTitles.count - 1, rowCount]
print(index)
            tableView.scrollToRow(at: index, at: .bottom, animated: true)
            requestToSendMessages()
            print(dataSource)
        } else {
            sectionTitles.append(currentDateTwo)
            print(newMSg)
            dataSource?.groupedMessagedData.append(newMSg)
            tableView.reloadData()
            let rowCount: Int = dataSource?.groupedMessagedData[sectionTitles.count - 1].count ?? 0
            let index: IndexPath = [sectionTitles.count - 1, rowCount - 1]
            tableView.scrollToRow(at: index, at: .bottom, animated: true)
            requestToSendMessages()
        }
       
        
    }
    
    //MARK:- REQUEST TO SEND MSG
    
    func requestToSendMessages() {
        let param: [String : Any] = ["message" : messgeText ?? ""]
        ChatResponse.requestToSendMessage(param: param, url: GraduateUrl.baseURL + "user/chat/send-message/" + "\(conversationId ?? 0)", imageName: "image", videos: videos, multipleImages: sentImages) { [weak self](response) in
            guard let strongSelf = self else { return }
            print(response?.sentData?.body)
            print(strongSelf.dataSource)
            //strongSelf.tableView.reloadData()
        }
    }
//MARK:- ATTRIBUTED
    func attributed() {
        let text = name
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.tapLabel))
        nameLabel.addGestureRecognizer(tap)
        nameLabel.isUserInteractionEnabled = true
        let underlineAttriString = NSMutableAttributedString(string: text ?? "")
        let range1 = (text! as NSString).range(of: name ?? "")
        underlineAttriString.addAttribute(NSAttributedString.Key.font, value: UIFont.systemFont(ofSize: 17, weight: .medium), range: range1)
        nameLabel.attributedText = underlineAttriString
    }
    @objc func tapLabel() {
        pushingTOUserProfile()
    }
//MARK:- PUSHING TO PROFILE
    func pushingToProfile() {
        if chattableType == "business" {
            let vc = UIStoryboard.graduateStoryboard.instantiateGraduateDetailsViewController()
            vc.businessId = userId
            navigationController?.pushViewController(vc, animated: true)
        } else if chattableType == "user" {
            let vc = UIStoryboard.moreStoryboard.instantiateOtherUserProfileVC()
            vc.userId = userId
                      navigationController?.pushViewController(vc, animated: true)
        }
          
       }
    
    func registerView() {
        tableView.register(UINib(nibName: "TextChatTableViewCell", bundle: nil), forCellReuseIdentifier: "TextChatTableViewCell")
        tableView.register(UINib(nibName: "ImageChatTableViewCell", bundle: nil), forCellReuseIdentifier: "ImageChatTableViewCell")
        tableView.register(UINib(nibName: "VideoMessageTableViewCell", bundle: nil), forCellReuseIdentifier: "VideoMessageTableViewCell")
        tableView.register(UINib(nibName: "SenderTextTableViewCell", bundle: nil), forCellReuseIdentifier: "SenderTextTableViewCell")
        tableView.register(UINib(nibName: "SenderImageTableViewCell", bundle: nil), forCellReuseIdentifier: "SenderImageTableViewCell")
        tableView.register(UINib(nibName: "SenderVideoTableViewCell", bundle: nil), forCellReuseIdentifier: "SenderVideoTableViewCell")
        // tableView.register(ChatSectionView.self, forHeaderFooterViewReuseIdentifier: "ChatSectionView")
        
    }
    func updateTableContentInset() {
        let numRows = self.tableView.numberOfRows(inSection: dataSource?.groupedMessagedData.count ?? 0)
        var contentInsetTop = self.tableView.bounds.size.height
        for i in 0..<numRows {
            let rowRect = self.tableView.rectForRow(at: IndexPath(item: i, section: sectionTitles.count))
            contentInsetTop -= rowRect.size.height
            if contentInsetTop <= 0 {
                contentInsetTop = 0
            }
        }
        self.tableView.contentInset = UIEdgeInsets(top: contentInsetTop,left: 0,bottom: 0,right: 0)
    }
    
    
    
    //MARK:- GET THE SENDING MESSAGE TIME
    func printTimestamp() -> String {
        let timestamp = DateFormatter.localizedString(from: NSDate() as Date, dateStyle: .none, timeStyle: .short)
        print(timestamp)
        return timestamp
    }
    
    @IBAction func didTapProfilePicture(_ sender: Any) {
        pushingToProfile()
    }
   
    
}

//MARK:- UITABLEVIEW DATASOURCE
extension ChatViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        print(sectionTitles.count)
        return sectionTitles.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print(dataSource?.groupedMessagedData[section].count ?? 0)
        return dataSource?.groupedMessagedData[section].count ?? 0
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if dataSource?.groupedMessagedData[indexPath.section][indexPath.row].sender?.chatableId == userOwnId {
            if dataSource?.groupedMessagedData[indexPath.section][indexPath.row].type == "text" {
                let cell = tableView.dequeueReusableCell(withIdentifier: "TextChatTableViewCell", for: indexPath) as! TextChatTableViewCell
                print(dataSource?.groupedMessagedData[indexPath.section][indexPath.row].body)
                cell.messageLabel.text = (dataSource?.groupedMessagedData[indexPath.section][indexPath.row].body ?? "")
                print((dataSource?.groupedMessagedData[indexPath.section][indexPath.row].createdAt ?? ""))
                cell.dateLabel.text = getTimeByApi(dataSource?.groupedMessagedData[indexPath.section][indexPath.row].createdAt ?? "")
                return cell
            } else if dataSource?.groupedMessagedData[indexPath.section][indexPath.row].type == "image" {
                let cell = tableView.dequeueReusableCell(withIdentifier: "ImageChatTableViewCell", for: indexPath) as! ImageChatTableViewCell
                
                if dataSource?.groupedMessagedData[indexPath.section][indexPath.row].body == "senderImage" {
                    cell.senderImage = dataSource?.groupedMessagedData[indexPath.section][indexPath.row].image
                    cell.imageType = "senderImage"
                    cell.timeLabel.text = printTimestamp()
                    cell.collectionView.reloadData()
                    cell.delegate = self
                    return cell
                } else {
                cell.sentImages = dataSource?.groupedMessagedData[indexPath.section][indexPath.row].body
                    cell.imageType = ""
                cell.timeLabel.text = printTimestamp()
                cell.collectionView.reloadData()
                cell.delegate = self
                return cell
                }
            } else if dataSource?.groupedMessagedData[indexPath.section][indexPath.row].type == "video" {
                let cell = tableView.dequeueReusableCell(withIdentifier: "VideoMessageTableViewCell", for: indexPath) as! VideoMessageTableViewCell
                if dataSource?.groupedMessagedData[indexPath.section][indexPath.row].videoType == "senderVideo" {
                    let filrurls = NSURL(string: ((dataSource?.groupedMessagedData[indexPath.section][indexPath.row].body ?? "")))
                    print(filrurls)
                    let firstAsset = AVURLAsset(url: filrurls! as URL)
                    cell.videoPlayer.videoAssets = [firstAsset]
                    
                    cell.timeLabel.text = printTimestamp()
                    /////MARK:  FOR DISBABLING THE REPEATING PLAYING VIDEO
                    if let player = cell.videoPlayer.videoPlayerControls.videoPlayer {
                        if (player.playingVideo != nil) {
                            player.stopVideo()
                        }                }
                    cell.videoPlayer.configuration = .init(videoGravity: .aspectFit, shouldLoop: false, startPlayingWhenReady: false, controlsInitiallyHidden: true, allowBackgroundPlay: false)
                    
                    
                    return cell
                } else {
                let filrurls = NSURL(string: (GraduateUrl.shorImageUrl + (dataSource?.groupedMessagedData[indexPath.section][indexPath.row].body ?? "")))
                print(filrurls)
                let firstAsset = AVURLAsset(url: filrurls! as URL)
                cell.videoPlayer.videoAssets = [firstAsset]
                
                cell.timeLabel.text = printTimestamp()
                /////MARK:  FOR DISBABLING THE REPEATING PLAYING VIDEO
                if let player = cell.videoPlayer.videoPlayerControls.videoPlayer {
                    if (player.playingVideo != nil) {
                        player.stopVideo()
                    }                }
                cell.videoPlayer.configuration = .init(videoGravity: .aspectFit, shouldLoop: false, startPlayingWhenReady: false, controlsInitiallyHidden: true, allowBackgroundPlay: false)
                
                
                return cell
                }
            }
        } else {
            if dataSource?.groupedMessagedData[indexPath.section][indexPath.row].type == "text" {
                let cell = tableView.dequeueReusableCell(withIdentifier: "SenderTextTableViewCell", for: indexPath) as! SenderTextTableViewCell
                cell.delegate = self
                cell.leftMessageLabel.text = dataSource?.groupedMessagedData[indexPath.section][indexPath.row].body
                cell.leftTimeLabel.text = printTimestamp()
                // cell.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi))
                cell.senderImage.setURLImage(imageURL: (GraduateUrl.imageUrl + (dataSource?.groupedMessagedData[indexPath.section][indexPath.row].sender?.profileImage ?? "")))
                
                return cell
            } else if dataSource?.groupedMessagedData[indexPath.section][indexPath.row].type == "image" {
                let cell = tableView.dequeueReusableCell(withIdentifier: "SenderImageTableViewCell", for: indexPath) as! SenderImageTableViewCell
                cell.sentImages = dataSource?.groupedMessagedData[indexPath.section][indexPath.row].body
                
                cell.leftTimeLabel.text = printTimestamp()
                cell.collectionView.reloadData()
                cell.delegate = self
                cell.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi))
                return cell
            } else if dataSource?.groupedMessagedData[indexPath.section][indexPath.row].type == "video" {
                let cell = tableView.dequeueReusableCell(withIdentifier: "SenderVideoTableViewCell", for: indexPath) as! SenderVideoTableViewCell
                let filrurls = NSURL(string: (GraduateUrl.shorImageUrl + (dataSource?.groupedMessagedData[indexPath.section][indexPath.row].body ?? "")))
                print(filrurls)
                let firstAsset = AVURLAsset(url: filrurls! as URL)
                cell.videoPlayer.videoAssets = [firstAsset]
                cell.leftTimeLabel.text = printTimestamp()
                /////MARK:  FOR DISBABLING THE REPEATING PLAYING VIDEO
                if let player = cell.videoPlayer.videoPlayerControls.videoPlayer {
                    if (player.playingVideo != nil) {
                        player.stopVideo()
                    }                }
                cell.videoPlayer.configuration = .init(videoGravity: .aspectFit, shouldLoop: false, startPlayingWhenReady: false, controlsInitiallyHidden: true, allowBackgroundPlay: false)
                //cell.videoPlayer.configuration = .
                cell.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi))
                return cell
            }
        }
        return UITableViewCell()
    }
    
    
    
}

//MARK:- UITABLEVIEW DELEGATE
extension ChatViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 15))
        
        let label = UILabel()
        label.frame = CGRect.init(x: 5, y: 5, width: headerView.frame.width-10, height: 15)
        print(sectionTitles[section])
        label.text = sectionTitles[section]
        
        label.textAlignment = .center
        label.textColor = UIColor.init(hexString: Appcolors.primary)
        label.textColor = UIColor.init(hexString: Appcolors.ownMessageBubbleColor)
        
        headerView.addSubview(label)
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        print("")
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if loadWillDiplay == true {
            let allData = dataSource
            print(allData?.loadMore ?? false)
            print(indexPath.row,"indexPath",indexPath.section)
            
            let rows: Int = dataSource?.groupedMessagedData[sectionTitles.count - 1].count ?? 0
            
            print(indexPath.row,(rows))
            guard allData?.loadMore == true, let dataCount = allData?.groupedMessagedData[sectionTitles.count - 1].count, indexPath.row == 0, indexPath.section == 0 else {return}
            print(dataCount)
            print(tableView.numberOfSections)
            let lastSectionIndex = tableView.numberOfSections - 1
            print(lastSectionIndex)
            print(indexPath.section)
            let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
            if indexPath.section == 0 && indexPath.row == 0 {
                print("this is the last cell")
                let spinner = UIActivityIndicatorView(style: .gray)
                spinner.startAnimating()
                spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
                self.tableView.tableHeaderView = spinner
                self.tableView.tableHeaderView?.isHidden = false
            }
            print(dataSource!.numOfPage,dataSource!.loadMore)
            print(tableView.contentOffset.y)
            //verticalContentOffset = tableView.contentOffset
            print(verticalContentOffset)
            getConversationMessages(pageNo: dataSource!.numOfPage, isLoadMore: dataSource!.loadMore, showHud: false)
            print(indexPath)
            
        }
        loadWillDiplay = true
    }
}

//MARK:- TEXTVIEW DELEGATE
extension ChatViewController: UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView == messageTxtView {
            IQKeyboardManager.shared.disabledToolbarClasses = [ChatViewController.self]
            
            messageTxtView.text = ""
            messageTxtView.textColor = .black
            
        }
    }
}
extension ChatViewController: SendingActionToChatVC {
    func sendingMediaAction(type: MediaTyope) {
        switch type {
        case .galley:
            popTip.hide()
            self.getImage(fromSourceType: .savedPhotosAlbum, mediaType: "public.image")
            
            //  if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
            print("Button capture")
            
            
        // }
        case .camera:
            popTip.hide()
            self.openCamera()
        case .video:
            popTip.hide()
            self.getImage(fromSourceType: .camera, mediaType: "public.movie")
            
        default:
            break
        }
    }
    func getImage(fromSourceType sourceType: UIImagePickerController.SourceType, mediaType: String) {
        
        //Check is source type available
        imagePickerController.modalPresentationStyle = .overCurrentContext
        if sourceType == .savedPhotosAlbum {
            imagePickerController.delegate = self
            imagePickerController.sourceType = .savedPhotosAlbum
            imagePickerController.allowsEditing = true
            
            present(imagePickerController, animated: true, completion: nil)
        } else if sourceType == .camera{
            
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                imagePickerController.sourceType = .camera
                imagePickerController.mediaTypes = [mediaType]
                imagePickerController.delegate = self
                imagePickerController.allowsEditing = true
                self.present(imagePickerController, animated: true, completion: nil)
            } else {
                print("You do not have a camera")
            }
        } else {
            //            if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            //                    imagePickerController.sourceType = .photoLibrary
            //                    imagePickerController.mediaTypes = [mediaType]
            //                    imagePickerController.delegate = self
            //            imagePickerController.allowsEditing = true
            //                    self.present(imagePickerController, animated: true, completion: nil)
            //            }
        }
    }
    func openCamera() {
        let cameraVc = UIImagePickerController()
        cameraVc.sourceType = UIImagePickerController.SourceType.camera
        cameraVc.delegate = self
        cameraVc.modalPresentationStyle = .overCurrentContext
        self.present(cameraVc, animated: true, completion: nil)
    }
    
    //printTimestamp() // Prints "Sep 9, 2014, 4:30 AM"
    
}
//MARK:- IMAGE PICKER DELEGATE
extension ChatViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        self.dismiss(animated: true) { [weak self] in
            guard let strongSelf = self else {
                return
            }
            let videoURL = info[.mediaURL] as? NSURL
            if videoURL != nil {
                var currentDateOne = Date.getCurrentDate()
                var currentDateTwo = Date.getCurrentDateTwo()
                print(currentDateOne)
                currentDateOne = currentDateOne + "000000Z"
               
                strongSelf.msg?.body = "\(videoURL!)"
                strongSelf.msg?.type = "video"
                strongSelf.msg?.videoType = "senderVideo"
                self?.msg?.sender?.chatableId = UserDefaultsHandler.getUDValue(key: .userID) as? Int
                self?.msg?.createdAt = currentDateOne
                self?.videos.append(videoURL! as URL)
                
                //strongSelf.message?.append(videoURL ?? "")
                
                if currentDateTwo == strongSelf.sectionTitles.last {
                    strongSelf.dataSource?.groupedMessagedData[strongSelf.sectionTitles.count - 1].append(strongSelf.msg ?? ChatData(chatSender: ChatSender()))
                    strongSelf.tableView.reloadData()
                    let rowCount: Int = strongSelf.dataSource?.groupedMessagedData[strongSelf.sectionTitles.count - 1].count ?? 0
                    print(rowCount)
                    let index: IndexPath = [strongSelf.sectionTitles.count - 1, rowCount - 1]
                    strongSelf.tableView.scrollToRow(at: index, at: .top, animated: true)
                } else {
                    strongSelf.sectionTitles.append(currentDateTwo)
                    print(strongSelf.newMSg)
                    strongSelf.dataSource?.groupedMessagedData.append(strongSelf.newMSg)
                }
                // self?.requestToSendMessages()
                strongSelf.tableView.reloadData()
                strongSelf.requestToSendMessages()
                
            } else {
                
                if let selectedImage = info[.originalImage] as? UIImage  {
                    print(selectedImage)
                    var currentDateOne = Date.getCurrentDate()
                    var currentDateTwo = Date.getCurrentDateTwo()
                    print(currentDateOne)
                    currentDateOne = currentDateOne + "000000Z"
                    let theImage = HelperFunctions.convertImageToBase64String(image: selectedImage)
                    self?.msg?.body = "senderImage"
                    self?.msg?.image = selectedImage
                    self?.msg?.type = "image"
                    self?.msg?.sender?.chatableId = UserDefaultsHandler.getUDValue(key: .userID) as? Int
                    self?.sentImages.append(selectedImage)
                    let currentDate = Date.getCurrentDate()
                    self?.msg?.createdAt = currentDateOne
                    print(strongSelf.msg)
                    print(strongSelf.newMSg)
                    if currentDateTwo == strongSelf.sectionTitles.last {
                        strongSelf.dataSource?.groupedMessagedData[strongSelf.sectionTitles.count - 1].append(strongSelf.msg ?? ChatData(chatSender: ChatSender()))
                        strongSelf.tableView.reloadData()
                        let index: IndexPath = [strongSelf.sectionTitles.count - 1, 0]
                        strongSelf.tableView.scrollToRow(at: index, at: .bottom, animated: true)
                    } else {
                        strongSelf.sectionTitles.append(currentDateTwo)
                        print(strongSelf.newMSg)
                        strongSelf.dataSource?.groupedMessagedData.append(strongSelf.newMSg)
                    }
                    print(strongSelf.dataSource)
                    // strongSelf.requestToSendMessages()
                    strongSelf.tableView.reloadData()
//                    let rowCount: Int = strongSelf.dataSource?.groupedMessagedData[strongSelf.sectionTitles.count - 1].count ?? 0
//                    print(rowCount)
//
//                    let indexPath: IndexPath = [(strongSelf.dataSource?.groupedMessagedData.count ?? 0) - 1,rowCount]
//                    strongSelf.tableView.scrollToRow(at: indexPath, at: .bottom, animated: false)
                    let rowCount: Int = ((strongSelf.dataSource?.groupedMessagedData[strongSelf.sectionTitles.count - 1].count ?? 0) - 1)
                    let index: IndexPath = [strongSelf.sectionTitles.count - 1, rowCount]
        print(index)
            strongSelf.tableView.scrollToRow(at: index, at: .bottom, animated: true)
                        //strongSelf.tableView.scrollToRow(at: index, at: .bottom, animated: true)
                    
                }
                strongSelf.requestToSendMessages()
            }
            
            
            
            
            
        }
    }
}

// MARK: DTPhotoViewerControllerDelegate

extension ChatViewController: SendingActionFromImageChatTVC {
    func fullscreen(vc: UIViewController) {
        currentTabBar?.setBar(hidden: true, animated: true)
        present(vc, animated: true, completion: nil)
    }
    
    func didTapImage() {
        pushingToProfile()
    }
}
extension ChatViewController: SendingActionFromSenderTextTVC {
    func pushingTOUserProfile() {
        pushingToProfile()
    }
}
extension ChatViewController: DelegateFromSenderVideoTVC {
   
}
extension ChatViewController: SendingActionFromSenderImageChatTVC {
    
}




