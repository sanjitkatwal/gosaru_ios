//
//  ChatResponse.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 11/2/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import Foundation
import ObjectMapper

class ChatResponse: DefaultResponse {
     var data: uniMyPostData?
    var sentData: ChatData?
override func mapping(map: Map) {
  super.mapping(map: map)
data <- map["data"]
sentData <- map["data"]
}
// MARK: - GET MESSAGE CONVERSATION
    class func requestToConversationMessages(id: Int,pageNo: Int,showHud: Bool,completionHandler:@escaping ((ChatResponseModel?) -> Void)) {
        let param = ["page" : "\(pageNo)"]
        APIManager(urlString: (GraduateUrl.getConverationMessageURL + "\(id)"),parameters: param,method: .get).handleResponse(showProgressHud: showHud,completionHandler: { (response: ChatResponseModel) in
              print(response)
                  completionHandler(response)
              }, failureBlock: {
                completionHandler(nil)
              })
      }
    //MARK:- SEND MESSAGES IN CHAT
    class func requestToSendMessage(param: [String: Any],url: String,imageName: String,videos: [URL],multipleImages: [UIImage],showHud: Bool = false ,completionHandler:@escaping ((ChatResponse?) -> Void)) {
        ImageUploaderInChat.uploadImage(url: url, videos: videos, imageName: imageName, parameters: param, imagesDataSource: multipleImages, showHUD: false, completionSuccess: { (response: ChatResponse) in
             completionHandler(response)
        }) {
            
        }
          
              }
}
 class ChatResponseModel: DefaultResponse {

// MARK: Declaration for string constants to be used to decode and also serialize.
internal let kBaseClassDataKey: String = "data"
internal let kBaseClassLinksKey: String = "links"
internal let kBaseClassMetaKey: String = "meta"


// MARK: Properties
public var data: [ChatData]?
public var links: TimeLineLinks?
public var meta: TimeLineMeta?



// MARK: ObjectMapper Initalizers
/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/


/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/
    override func mapping(map: Map) {
    data <- map[kBaseClassDataKey]
    links <- map[kBaseClassLinksKey]
    meta <- map[kBaseClassMetaKey]

}
}
struct ChatData: Mappable {
    init?(map: Map) {
        
    }
    
//    required init?(map: Map) {
//
//    }
    
  
  
   
    

// MARK: Declaration for string constants to be used to decode and also serialize.
internal let kDataCreatedAtKey: String = "created_at"
internal let kDataInternalIdentifierKey: String = "id"
internal let kDataBodyKey: String = "body"
internal let kDataMessageIdKey: String = "message_id"
internal let kDataConversationIdKey: String = "conversation_id"
internal let kDataSenderKey: String = "sender"
internal let kDataIsSeenKey: String = "is_seen"
internal let kDataUserIdKey: String = "user_id"
internal let kDataUpdatedAtKey: String = "updated_at"
internal let kDataIsSenderKey: String = "is_sender"
internal let kDataTypeKey: String = "type"


// MARK: Properties
public var createdAt: String?
public var internalIdentifier: Int?
public var body: String?
public var image: UIImage?
public var messageId: Int?
public var conversationId: Int?
public var sender: ChatSender?
public var isSeen: Bool?
public var userId: Int?
public var updatedAt: String?
public var isSender: Bool?
public var type: String?
    public var videoType: String?



// MARK: ObjectMapper Initalizers
/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/




/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/
    mutating func mapping(map: Map) {
    createdAt <- map[kDataCreatedAtKey]
    internalIdentifier <- map[kDataInternalIdentifierKey]
    body <- map[kDataBodyKey]
    messageId <- map[kDataMessageIdKey]
    conversationId <- map[kDataConversationIdKey]
    sender <- map[kDataSenderKey]
    isSeen <- map[kDataIsSeenKey]
    userId <- map[kDataUserIdKey]
    updatedAt <- map[kDataUpdatedAtKey]
    isSender <- map[kDataIsSenderKey]
    type <- map[kDataTypeKey]

}
//    init(body: String, createdAt: String, userId: Int) {
//        self.createdAt = createdAt
//        self.body = body
//        self.userId = userId
//    }
    init(chatSender: ChatSender) {
        self.sender = chatSender
    }
    
 
}

public class ChatSender: Mappable {
    public required init?(map: Map) {
        
    }
    
   
   
    

// MARK: Declaration for string constants to be used to decode and also serialize.
internal let kSenderCreatedAtKey: String = "created_at"
internal let kSenderModeratedAtKey: String = "moderated_at"
internal let kSenderNameKey: String = "name"
internal let kSenderInternalIdentifierKey: String = "id"
internal let kSenderChatableIdKey: String = "chatable_id"
internal let kSenderUpdatedAtKey: String = "updated_at"
internal let kSenderChatabaleTypeKey: String = "chatabale_type"
internal let kSenderStatusKey: String = "status"
internal let kSenderProfileImageKey: String = "profile_image"


// MARK: Properties
public var createdAt: String?
public var moderatedAt: String?
public var name: String?
public var internalIdentifier: Int?
public var chatableId: Int?
public var updatedAt: String?
public var chatabaleType: String?
public var status: Int?
public var profileImage: String?



// MARK: ObjectMapper Initalizers
/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/

/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/
    public func mapping(map: Map) {
    createdAt <- map[kSenderCreatedAtKey]
    moderatedAt <- map[kSenderModeratedAtKey]
    name <- map[kSenderNameKey]
    internalIdentifier <- map[kSenderInternalIdentifierKey]
    chatableId <- map[kSenderChatableIdKey]
    updatedAt <- map[kSenderUpdatedAtKey]
    chatabaleType <- map[kSenderChatabaleTypeKey]
    status <- map[kSenderStatusKey]
    profileImage <- map[kSenderProfileImageKey]

}
    init() {
        
    }
}
