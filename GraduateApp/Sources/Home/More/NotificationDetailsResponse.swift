//
//  NotificationDetailsResponse.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 10/15/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import Foundation
import ObjectMapper

class NotificationsDetailsResponse: DefaultResponse {
     var data: uniMyPostData?
override func mapping(map: Map) {
  super.mapping(map: map)
data <- map["data"]
}
// MARK: - GET USER NOTIFICATIONS
    class func requestToUserNotifications(id: Int,showHud: Bool = true ,completionHandler:@escaping ((NotificationsDetailsResponse?) -> Void)) {
        APIManager(urlString: (GraduateUrl.navigatePostURL + "\(id)"),method: .get).handleResponse(showProgressHud: showHud,completionHandler: { (response: NotificationsDetailsResponse) in
              print(response)
                  completionHandler(response)
              }, failureBlock: {
                completionHandler(nil)
              })
      }
}
