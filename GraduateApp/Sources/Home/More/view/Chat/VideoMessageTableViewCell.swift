//
//  VideoMessageTableViewCell.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 8/20/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import UIKit
import ASPVideoPlayer

class VideoMessageTableViewCell: UITableViewCell {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var senderImage: UIImageView!
    @IBOutlet weak var rightVideoView: CustomView!
    @IBOutlet weak var leftVideoView: CustomView!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var videoPlayer: ASPVideoPlayer!
    @IBOutlet weak var videoPlayerBackgroundView: UIView!
     var previousConstraints: [NSLayoutConstraint] = []
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
       videoPlayer.resizeClosure = { [unowned self] isExpanded in
           self.rotate(isExpanded: isExpanded)
       }
        videoPlayer.delegate = self
        videoPlayer.tintColor = UIColor.init(hexString: Appcolors.buttons)

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    func rotate(isExpanded: Bool) {
     
     let bgView = UIView(frame: UIScreen.main.bounds)
     bgView.backgroundColor = .red
     bgView.tag = 1
            bgView.addToWindow()
     // delegate?.sendingVideoUrlForFullScreen()
        let views: [String:Any] = ["videoPlayer": videoPlayer as Any,
                                   "backgroundView": videoPlayerBackgroundView as Any]

        var constraints: [NSLayoutConstraint] = []

        if isExpanded == false {
            self.containerView.removeConstraints(self.videoPlayer.constraints)

            bgView.addSubview(self.videoPlayerBackgroundView)
          bgView.addSubview(self.videoPlayer)
            self.videoPlayer.frame = self.containerView.frame
            self.videoPlayerBackgroundView.frame = self.containerView.frame

            let padding = (bgView.bounds.height - bgView.bounds.width) / 2.0

            var bottomPadding: CGFloat = 0

            if #available(iOS 11.0, *) {
                if self.contentView.safeAreaInsets != .zero {
                    bottomPadding = self.contentView.safeAreaInsets.bottom
                }
            }

            let metrics: [String:Any] = ["padding":padding,
                                         "negativePaddingAdjusted":-(padding - bottomPadding),
                                         "negativePadding":-padding]

            constraints.append(contentsOf:
                NSLayoutConstraint.constraints(withVisualFormat: "H:|-(negativePaddingAdjusted)-[videoPlayer]-(negativePaddingAdjusted)-|",
                                               options: [],
                                               metrics: metrics,
                                               views: views))
            constraints.append(contentsOf:
                NSLayoutConstraint.constraints(withVisualFormat: "V:|-(padding)-[videoPlayer]-(padding)-|",
                                               options: [],
                                               metrics: metrics,
                                               views: views))

            constraints.append(contentsOf:
                NSLayoutConstraint.constraints(withVisualFormat: "H:|-(negativePadding)-[backgroundView]-(negativePadding)-|",
                                               options: [],
                                               metrics: metrics,
                                               views: views))
            constraints.append(contentsOf:
                NSLayoutConstraint.constraints(withVisualFormat: "V:|-(padding)-[backgroundView]-(padding)-|",
                                               options: [],
                                               metrics: metrics,
                                               views: views))

            bgView.addConstraints(constraints)
         self.videoPlayer.layoutIfNeeded()
         bgView.addToWindow()
        } else {
         //bgView.removeFromSuperview()
         bgView.removeFromWindow()
            bgView.removeConstraints(self.previousConstraints)

            let targetVideoPlayerFrame = bgView.convert(self.videoPlayer.frame, to: self.containerView)
            let targetVideoPlayerBackgroundViewFrame = bgView.convert(self.contentView.frame, to: self.containerView)

            self.containerView.addSubview(self.videoPlayerBackgroundView)
            self.containerView.addSubview(self.videoPlayer)

            self.videoPlayer.frame = targetVideoPlayerFrame
            self.videoPlayerBackgroundView.frame = targetVideoPlayerBackgroundViewFrame

            constraints.append(contentsOf:
                NSLayoutConstraint.constraints(withVisualFormat: "H:|[videoPlayer]|",
                                               options: [],
                                               metrics: nil,
                                               views: views))
            constraints.append(contentsOf:
                NSLayoutConstraint.constraints(withVisualFormat: "V:|[videoPlayer]|",
                                               options: [],
                                               metrics: nil,
                                               views: views))

            constraints.append(contentsOf:
                NSLayoutConstraint.constraints(withVisualFormat: "H:|[backgroundView]|",
                                               options: [],
                                               metrics: nil,
                                               views: views))
            constraints.append(contentsOf:
                NSLayoutConstraint.constraints(withVisualFormat: "V:|[backgroundView]|",
                                               options: [],
                                               metrics: nil,
                                               views: views))
        // bgView.removeFromSuperview()
            self.containerView.addConstraints(constraints)
         //bgView.removeFromWindow()
          bgView.removeFromSuperview()
         var subviews = window!.subviews

         for subview : AnyObject in subviews{

             // Do what you want to do with the subview
             print(subview)
         }

         window?.subviews[1].removeFromSuperview()
         
        }
      self.previousConstraints = constraints
       

        UIView.animate(withDuration: 0.25, delay: 0.0, options: [], animations: {
            self.videoPlayer.transform = isExpanded == true ? .identity : CGAffineTransform(rotationAngle: .pi / 2.0)
         self.videoPlayerBackgroundView.transform = isExpanded == true ? .identity : CGAffineTransform(rotationAngle: .pi / 2.0)

            bgView.layoutIfNeeded()
        })
    
    }
}
extension VideoMessageTableViewCell: ASPVideoPlayerViewDelegate {
     func startedVideo() {
            print("Started video")
           
        }

        func stoppedVideo() {
            print("Stopped video")
        }

        func newVideo() {
            print("New Video")
            
        }

        func readyToPlayVideo() {
            print("Ready to play video")
           
        }

        func playingVideo(progress: Double) {
    //        print("Playing: \(progress)")
            

        }

        func pausedVideo() {
            print("Paused Video")
        }

        func finishedVideo() {
            print("Finished Video")
        }

        func seekStarted() {
            print("Seek started")
        }

        func seekEnded() {
            print("Seek ended")
        }

        func error(error: Error) {
            print("Error: \(error)")
        }

        func willShowControls() {
            print("will show controls")
        }

        func didShowControls() {
            print("did show controls")
        }

        func willHideControls() {
            print("will hide controls")
        }

        func didHideControls() {
            print("did hide controls")
        }
}
