//
//  SenderImageTableViewCell.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 8/21/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import UIKit
import DTPhotoViewerController

protocol SendingActionFromSenderImageChatTVC: class {
    func didTapImage()
    func fullscreen(vc: UIViewController)
}
class SenderImageTableViewCell: UITableViewCell {

    @IBOutlet var senderImage: UIImageView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var leftTimeLabel: UILabel!
      fileprivate var selectedImageIndex: Int = 0
    weak var delegate: SendingActionFromSenderImageChatTVC?
    var sentImages: String? {
        didSet {
           print(sentImages)
            collectionView.delegate = self
             collectionView.register(UINib(nibName:"VideoMessageCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "VideoMessageCollectionViewCell")
             collectionView.dataSource = self
            collectionView.reloadData()
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
extension SenderImageTableViewCell: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if sentImages != nil {
            return 1
        } else {
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "VideoMessageCollectionViewCell", for: indexPath) as! VideoMessageCollectionViewCell
       
        cell.sentImage.setURLImage(imageURL: (GraduateUrl.imageUrl + (sentImages ?? "")))
        return cell
    }
  
}

extension SenderImageTableViewCell: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

//         let bgView = UIView(frame: UIScreen.main.bounds)
//        bgView.addToWindow()

       selectedImageIndex = indexPath.row

              if let cell = collectionView.cellForItem(at: indexPath) as? VideoMessageCollectionViewCell
              {
                  let viewController = SimplePhotoViewerController(referencedView: cell.sentImage, image: cell.sentImage.image)
                viewController.modalPresentationStyle = .overCurrentContext
                delegate?.fullscreen(vc: viewController)
                  //viewController.dataSource = self
            



              }

    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        let numberOfItemsPerRow:CGFloat = 4
//        let spacingBetweenCells:CGFloat = 16
//        //let spacing: CGFloat = 3
//
//        let totalSpacing = (2 * self.spacing) + ((numberOfItemsPerRow - 1) * spacingBetweenCells) //Amount of total spacing in a row
//
//        if let collection = self.collectionView{
//            let width = (collection.bounds.width - totalSpacing)/numberOfItemsPerRow
//            return CGSize(width: width, height: width)
//        }else{
//            return CGSize(width: 0, height: 0)
//
//        if fullScreen == true {
//
//            let screenSize = UIScreen.main.bounds
//            fullScreen = false
//            return CGSize(width: screenSize.width, height: screenSize.height)
//        } else {
//            fullScreen = true
       return CGSize(width: (collectionView.bounds.width), height: (collectionView.bounds.height))
        }

//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
//        return 4
//    }
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
//        return 4
//    }
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
//           return UIEdgeInsets(top: 4, left: 4, bottom: 4, right: 4)
//       }
}
extension SenderImageTableViewCell: DTPhotoViewerControllerDataSource {
    func photoViewerController(_ photoViewerController: DTPhotoViewerController, configureCell cell: DTPhotoCollectionViewCell, forPhotoAt index: Int) {
        // Set text for each item
        if let cell = cell as? CustomPhotoCollectionViewCell {
            cell.extraLabel.text = "Image no \(index + 1)"
        }
    }

    func photoViewerController(_ photoViewerController: DTPhotoViewerController, referencedViewForPhotoAt index: Int) -> UIView? {
        let indexPath = IndexPath(item: index, section: 0)
        if let cell = collectionView?.cellForItem(at: indexPath) as? VideoMessageCollectionViewCell {
            cell.sentImage.layer.cornerRadius = 25
            return cell.sentImage
        }

        return nil
    }

    func numberOfItems(in photoViewerController: DTPhotoViewerController) -> Int {
        if sentImages != nil {
            return 1
        } else {
            return 0
        }
       
    }

    func photoViewerController(_ photoViewerController: DTPhotoViewerController, configurePhotoAt index: Int, withImageView imageView: UIImageView) {
        imageView.setURLImage(imageURL: (GraduateUrl.imageUrl + (sentImages ?? "")))
       
    }
}

// MARK: DTPhotoViewerControllerDelegate
extension SenderImageTableViewCell: SimplePhotoViewerControllerDelegate {
    func simplePhotoViewerController(_ viewController: SimplePhotoViewerController, savePhotoAt index: Int) {
        //UIImageWriteToSavedPhotosAlbum((sentImages ?? "", nil, nil, nil)


    }
    
    func photoViewerControllerDidEndPresentingAnimation(_ photoViewerController: DTPhotoViewerController) {
        photoViewerController.scrollToPhoto(at: selectedImageIndex, animated: false)
    }

    func photoViewerController(_ photoViewerController: DTPhotoViewerController, didScrollToPhotoAt index: Int) {
        selectedImageIndex = index
        if let collectionView = collectionView {
            let indexPath = IndexPath(item: selectedImageIndex, section: 0)

            // If cell for selected index path is not visible
            if !collectionView.indexPathsForVisibleItems.contains(indexPath) {
                // Scroll to make cell visible
                collectionView.scrollToItem(at: indexPath, at: UICollectionView.ScrollPosition.bottom, animated: false)
            }
        }
    }
}
