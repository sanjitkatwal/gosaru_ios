//
//  SenderVideoTableViewCell.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 8/21/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import UIKit
import ASPVideoPlayer
protocol DelegateFromSenderVideoTVC: class {
     func didTapImage()
}
class SenderVideoTableViewCell: UITableViewCell {

    @IBOutlet var senderImage: UIImageView!
    @IBOutlet var leftTimeLabel: UILabel!
    @IBOutlet weak var videoPlayerBackground: UIView!
    @IBOutlet weak var videoPlayer: ASPVideoPlayer!
    weak var delegate: DelegateFromSenderVideoTVC?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
