//
//  SenderTextTableViewCell.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 8/21/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import UIKit
protocol SendingActionFromSenderTextTVC: class {
    func pushingTOUserProfile()
}
class SenderTextTableViewCell: UITableViewCell {

    @IBOutlet weak var senderImage: UIImageView!
    @IBOutlet weak var leftTimeLabel: UILabel!
    @IBOutlet var messageBubble: CustomView!
    @IBOutlet weak var leftMessageLabel: UILabel!
    weak var delegate: SendingActionFromSenderTextTVC?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        senderImage.layer.cornerRadius = 2
          let tapGesture = UITapGestureRecognizer(target: self, action: #selector(profileImagePushAction))
        senderImage.addGestureRecognizer(tapGesture)
        senderImage.isUserInteractionEnabled = true
        senderImage.layer.cornerRadius = 8
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @objc func profileImagePushAction() {
        delegate?.pushingTOUserProfile()
    }
    
}
