//
//  FriendListTableViewCell.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 7/29/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import UIKit
protocol ActionFromFLTVC: class {
    func bottomPopUp(userId: Int?)
}
class FriendListTableViewCell: UITableViewCell {
    
    @IBOutlet weak var requestStacView: UIStackView!
    @IBOutlet weak var morebtn: UIButton!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var mainImage: UIImageView!
    weak var delegate: ActionFromFLTVC?
    var userid: Int?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        morebtn.setImage(UIImage(named: "more-3"), for: .normal)
        morebtn.imageView?.contentMode = .center
        morebtn.imageView?.tintColor = UIColor.init(hexString: Appcolors.buttons)
        mainImage.layer.cornerRadius = 12
    
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func moreBtnAction(_ sender: Any) {
        delegate?.bottomPopUp(userId: userid)
    }
    
}
