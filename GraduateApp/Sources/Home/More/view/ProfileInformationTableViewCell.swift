//
//  ProfileInformationTableViewCell.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 7/29/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import UIKit
import YYCalendar
import GooglePlaces
protocol SendingTxtFieldActionFromProfileInformation: class {
    func textFieldAction(textField: String)
    func requestingUpdateProfile(params: [String : Any])
    func changingtheInfoValue(text: String, type: ProfileInfoListType)
}
class ProfileInformationTableViewCell: UITableViewCell {
    @IBOutlet weak var addressTxtField: UITextField!
    
    @IBOutlet weak var cancelBtn: UIButton!
    @IBOutlet weak var editBtn: UIButton!
    @IBOutlet weak var countryTxtField: UITextField!
    @IBOutlet weak var countryView: UIView!
    @IBOutlet weak var statusTxtField: UITextField!
    @IBOutlet weak var statusView: UIView!
    @IBOutlet weak var addressView: UIView!
    @IBOutlet weak var dateView: UIView!
    @IBOutlet weak var dateTxtField: UITextField!
    @IBOutlet weak var professionView: UIView!
    @IBOutlet weak var professionTxtField: UITextField!
    @IBOutlet weak var genderTxtField: UITextField!
    @IBOutlet weak var genderView: UIView!
    @IBOutlet weak var codeTxtField: UITextField!
    @IBOutlet weak var codeView: UIView!
    @IBOutlet weak var mailTxtField: UITextField!
    @IBOutlet weak var mailView: UIView!
    @IBOutlet weak var descView: UIView!
    @IBOutlet weak var descTxtiew: UITextView!
    @IBOutlet weak var nickNameTxtField: UITextField!
    @IBOutlet weak var nickNameContainerView: UIView!
    @IBOutlet weak var nameTxtField: UITextField!
    @IBOutlet weak var nameContainerView: UIView!
    weak var delegate: SendingTxtFieldActionFromProfileInformation?
    var professionList: [String]?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        descTxtiew.text = "Description"
       // descTxtiew.textColor = .lightGray
        descTxtiew.delegate = self
        genderTxtField.delegate = self
        professionTxtField.delegate = self
        dateTxtField.delegate = self
        statusTxtField.delegate = self
        addressTxtField.delegate = self
        countryTxtField.delegate = self
         cancelBtn.isHidden = true
    }
    func emablingUserInput(boolValue: Bool){
        nameTxtField.isUserInteractionEnabled = boolValue
        nickNameTxtField.isUserInteractionEnabled = boolValue
        descTxtiew.isUserInteractionEnabled = boolValue
        mailTxtField.isUserInteractionEnabled = boolValue
        codeTxtField.isUserInteractionEnabled = boolValue
        dateTxtField.isUserInteractionEnabled = boolValue
        professionTxtField.isUserInteractionEnabled = boolValue
        addressTxtField.isUserInteractionEnabled = boolValue
        statusTxtField.isUserInteractionEnabled = boolValue
        countryTxtField.isUserInteractionEnabled = boolValue
        genderTxtField.isUserInteractionEnabled = boolValue
        
        
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
       
    }
    @IBAction func editBtnAction(_ sender: Any) {
        emablingUserInput(boolValue: true)
        cancelBtn.isHidden = false
        UIView.transition(with: editBtn,duration: 0.5, options: .transitionCrossDissolve, animations: {
            self.editBtn.setTitle("Update", for: .normal)
            self.editBtn.setTitleColor(UIColor.init(hexString: Appcolors.buttons), for: .normal)
        }, completion: nil)
        HelperFunctions.addingImageInArrayTxtFieldWithSize(textField: [genderTxtField,professionTxtField,dateTxtField,statusTxtField], imageName: "iconArrowDown", width: 5, height: 5)
        if editBtn.titleLabel?.text == "Update" {
            var params: [String : Any] = ["name" : nameTxtField.text ?? "", "nickname" : nickNameTxtField.text ?? "", "gender" : genderTxtField.text ?? "", "marital_status" : statusTxtField.text ?? "", "dob" : dateTxtField.text ?? "", "phone" : codeTxtField.text ?? "", "profession" : professionTxtField.text ?? "", "address_string" : addressTxtField.text ?? "", "country" : countryTxtField.text ?? "", "summary" : descTxtiew.text ?? ""]
            delegate?.requestingUpdateProfile(params: params)
        }
    }
    @IBAction func cancelBtnAction(_ sender: Any) {
        emablingUserInput(boolValue: false)
        cancelBtn.isHidden = true
        editBtn.setTitle("Edit", for: .normal)
         HelperFunctions.addingImageInArrayTxtFieldWithSize(textField: [genderTxtField,professionTxtField,dateTxtField,statusTxtField], imageName: "", width: 5, height: 5)
        delegate?.textFieldAction(textField: "cancelButtonPressed")
        
        
    }
    func bottomPresentingVC(height: Int) {
        
    }
    
}
extension ProfileInformationTableViewCell: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView == descTxtiew {
            descTxtiew.textColor = UIColor.init(hexString: Appcolors.text)
            descTxtiew.text = ""
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView == descTxtiew {
            delegate?.changingtheInfoValue(text: descTxtiew.text, type: .description)
        }
    }
    
}
extension ProfileInformationTableViewCell: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == genderTxtField {
           
            delegate?.textFieldAction(textField: "genderTextField")
            return false
            
        } else if textField == professionTxtField {
            genderTxtField.resignFirstResponder()
            delegate?.textFieldAction(textField: "professionTextField")
            return false
        } else if textField == statusTxtField {
            genderTxtField.resignFirstResponder()
            delegate?.textFieldAction(textField: "statusTextField")
            return false
        } else if textField == dateTxtField {
            let calendar = YYCalendar(normalCalendarLangType: .ENG,
                                                     date: "07/01/2019",
                                                     format: "MM/dd/yyyy") { [weak self] date in
                                                       self?.dateTxtField.text = date
            }
             calendar.show()
//            genderTxtField.resignFirstResponder()
//            delegate?.textFieldAction(textField: "dateTextField")
            return false
        } else if textField == countryTxtField {
            delegate?.textFieldAction(textField: "countryTxtField")
        }
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == addressTxtField {
            delegate?.changingtheInfoValue(text: addressTxtField.text ?? "", type: .address)
           
        } else if textField == nameTxtField {
            delegate?.changingtheInfoValue(text: nameTxtField.text ?? "", type: .name)
          
        } else if textField == nickNameTxtField {
            delegate?.changingtheInfoValue(text: nickNameTxtField.text ?? "", type: .nickname)
        } else if textField == codeTxtField {
            delegate?.changingtheInfoValue(text: codeTxtField.text ?? "", type: .phoneNum)
           

        }
    }
}
