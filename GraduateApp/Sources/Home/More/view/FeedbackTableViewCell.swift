//
//  FeedbackTableViewCell.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 8/10/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import UIKit
protocol SendingActionFromFeedBackTVC: class {
    func pushingTOProfileVC()
}
class FeedbackTableViewCell: UITableViewCell {
    weak var delegate: SendingActionFromFeedBackTVC?
    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var feedbackLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var profileImage: UIImageView!
    var name = "Somebody"
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        profileImage.layer.cornerRadius = 12
        attributed()
        let tap = UITapGestureRecognizer(target: self, action: #selector(tapImage))
               profileImage.addGestureRecognizer(tap)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func attributed() {
                   nameLabel.text = name
                   let text = (nameLabel.text)!
                let tap = UITapGestureRecognizer(target: self, action: #selector(self.tapLabel(gesture:)))
                nameLabel.addGestureRecognizer(tap)
                nameLabel.isUserInteractionEnabled = true
                   let underlineAttriString = NSMutableAttributedString(string: text)
                let range1 = (text as NSString).range(of: "Somebody")
                underlineAttriString.addAttribute(NSAttributedString.Key.font, value: UIFont.systemFont(ofSize: 17, weight: .medium), range: range1)
                   nameLabel.attributedText = underlineAttriString
               }
    @IBAction func tapLabel(gesture: UITapGestureRecognizer) {
          
              let text = (nameLabel.text)!
           let termsRange = (text as NSString).range(of: name ?? "default")
             
              if gesture.didTapAttributedTextInLabel(label: nameLabel, inRange: termsRange) {
                  delegate?.pushingTOProfileVC()
              }
          }
    @objc func tapImage() {
           delegate?.pushingTOProfileVC()
       }

}
