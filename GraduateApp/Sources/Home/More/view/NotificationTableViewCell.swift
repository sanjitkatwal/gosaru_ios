//
//  NotificationTableViewCell.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 7/2/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import UIKit
protocol RemovingNotificationFromList: class {
    func deletingNotification(index: IndexPath,id: String?)
}
class NotificationTableViewCell: UITableViewCell {

    @IBOutlet weak var moreBtn: UIButton!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var mainImage: UIImageView!
    weak var delegate: RemovingNotificationFromList?
    var index: IndexPath!
    var notificationId: String?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        moreBtn.setImage(UIImage(named: "more-3"), for: .normal)
        mainImage.layer.cornerRadius = 8
    }
   
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func notificationMoreBtnAction(_ sender: Any) {
        
        delegate?.deletingNotification(index: index, id: notificationId ?? "")
    }
    
}
