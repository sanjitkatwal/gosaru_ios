//
//  FaqTableViewCell.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 8/11/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import UIKit

class FaqTableViewCell: UITableViewCell {

    @IBOutlet weak var questionLabel: UILabel!
    @IBOutlet weak var questionView: UIView!
    @IBOutlet weak var answerLabel: UILabel!
    @IBOutlet weak var dropDownImage: UIImageView!
    @IBOutlet weak var answerView: UIView!
    var selection: Bool?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        dropDownImage.image = UIImage(named: "iconArrowDown")
        dropDownImage.tintColor = UIColor.init(hexString: Appcolors.buttons)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        print(selected)
        if selected == true {
            //dropDownImage.transform = dropDownImage.transform.rotated(by: .pi)
        } else {
//             dropDownImage.transform = dropDownImage.transform.rotated(by: (180.0 * .pi) / 180.0)
        }
//        if selected == true {
//            dropDownImage.transform = CGAffineTransform(rotationAngle: (selected ? .pi : 0))
//        // Configure the view for the selected state
//            self.isSelected = false
//        } else {
//            self.isSelected = true
//            dropDownImage.image = UIImage(named: "iconArrowDown")
//        }
    }

}
