//
//  FollowersViewController.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 7/31/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import UIKit

class FollowersViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
     var url: String?
    var dataSource: FollowersReponseModel? {
        didSet {
            tableView.reloadData()
          
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        resgisteringView()
        gettingUserFollowers()
        // Do any additional setup after loading the view.
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = "Followers"
        navigationController?.isNavigationBarHidden = false
        currentTabBar?.setBar(hidden: true, animated: true)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        navigationController?.isNavigationBarHidden = false
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        navigationController?.isNavigationBarHidden = true

        currentTabBar?.setBar(hidden: false, animated: true)

    }
    func resgisteringView() {
        tableView.register(UINib(nibName: "FollowersTableViewCell", bundle: nil), forCellReuseIdentifier: "FollowersTableViewCell")
        tableView.dataSource = self
        
    }
    func gettingUserFollowers() {
        FollowersResponse.requestToAFollowersList { [weak self](response) in
            guard let strongSelf = self else {return}
            strongSelf.dataSource = response
        }
    }
    
}
extension FollowersViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource?.data?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FollowersTableViewCell", for: indexPath) as! FollowersTableViewCell
        cell.mainImage.setURLImage(imageURL: (GraduateUrl.imageUrl + (dataSource?.data?[indexPath.row].profileImage ?? "")))
        cell.nameLabel.text = dataSource?.data?[indexPath.row].name
        if dataSource?.data?[indexPath.row].isFollowed == true {
            cell.followBtn.setTitle("Unfollow", for: .normal)
        } else {
             cell.followBtn.setTitle("follow", for: .normal)
        }
        cell.id = dataSource?.data?[indexPath.row].internalIdentifier
        cell.delegate = self
        return cell
}
}
//MARK:- GETTING ACTION FORM FOLLOWERS TVC
extension FollowersViewController: ActionFromFollowersTVC {
    func sendingFollowBtnAction(id: Int, buttonTitle: String) {
       
        if buttonTitle == "Unfollow" {
            let vc = UIStoryboard.timeLineStoryboard.instantiateErrorCarMsgPopVC()
            vc.popupType = .unFollowOther
            vc.delegaet = self
                                  let cardPopup = SBCardPopupViewController(contentViewController: vc)
                                  cardPopup.show(onViewController: self)
            url = GraduateUrl.FollowURL + "\(id)" + "/remove-follow"
           
        } else {
             url = GraduateUrl.removeFollowURL + "\(id)" + "/follow"
            print(id,buttonTitle,url)
            FollowersResponse.requestFollowUnfollowOthers(url: url ?? "") { [weak self](response) in
                guard let strongSelf = self else { return }
                strongSelf.gettingUserFollowers()
            }
        }
        
    }
}
extension FollowersViewController: SendingActionFromErrorChat {
    func adctionwithType(type: ErrorType) {
        
    }
    
    
    func sendingCancelAction() {
        
    }
    func sendingAction() {
       FollowersResponse.requestFollowUnfollowOthers(url: url ?? "") { [weak self](response) in
            guard let strongSelf = self else { return }
            strongSelf.gettingUserFollowers()
        }
    }
    
    
}
