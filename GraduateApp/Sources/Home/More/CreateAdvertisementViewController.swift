//
//  CreateAdvertisementViewController.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 7/21/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import UIKit

class CreateAdvertisementViewController: UIViewController {

    @IBOutlet weak var desxTxtView: UITextView!
    @IBOutlet weak var locationView: CustomView!
    @IBOutlet weak var phoneView: CustomView!
    @IBOutlet weak var companyNameView: CustomView!
    @IBOutlet weak var emailView: CustomView!
    @IBOutlet weak var fullNameView: CustomView!
    var controlleryType: String?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Create Advertisement"
        desxTxtView.layer.cornerRadius = 16
        // Do any additional setup after loading the view.
        if controlleryType == "Contact" {
            companyNameView.isHidden = true
            locationView.isHidden = true
            desxTxtView.delegate = self
            
            self.title = "Contact"
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
         currentTabBar?.setBar(hidden: true, animated: true)
        navigationController?.isNavigationBarHidden = false
    }
       
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
         currentTabBar?.setBar(hidden: false, animated: true)
       // navigationController?.isNavigationBarHidden = true
    }
    
}
extension CreateAdvertisementViewController: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView == desxTxtView {
            desxTxtView.textColor = UIColor.init(hexString: Appcolors.buttons)
            desxTxtView.text = ""
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if desxTxtView.text.isEmpty {
            desxTxtView.text = "Write your description"
            desxTxtView.textColor = UIColor.lightGray
        }
    }
}
