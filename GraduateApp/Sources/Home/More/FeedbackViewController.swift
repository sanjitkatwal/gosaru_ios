//
//  FeedbackViewController.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 8/10/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import UIKit

class FeedbackViewController: UIViewController {

    @IBOutlet weak var closebtn: UIButton!
    @IBOutlet weak var reviewContainerView: CustomView!
    @IBOutlet weak var ratingBtn: AnimatedButton!
    @IBOutlet weak var reviewTxtView: UITextView!
    @IBOutlet weak var sendBtn: AnimatedButton!
    @IBOutlet var headerView: UIView!
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    
       
        self.title = "Feedbacks"
        reviewTxtView.delegate = self
       settingUpViews()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        currentTabBar?.setBar(hidden: true, animated: true)
    }
    func settingUpViews() {
        tableView.tableHeaderView = headerView
               ratingBtn.leftImage(image: UIImage(named: "star")!)
               ratingBtn.centerTextAndImage(spacing: 1,top: 6, bottom: 6)
               ratingBtn.tintColor = UIColor.init(hexString: Appcolors.buttons)
               reviewTxtView.layer.cornerRadius = 12
               reviewContainerView.isHidden = true
        reviewTxtView.text = "Write a review"
        reviewTxtView.textColor = .lightGray
        reviewTxtView.textColor = UIColor.init(hexString: Appcolors.buttons)
        closebtn.setImage(UIImage(named: "close"), for: .normal)
        closebtn.tintColor = UIColor.init(hexString: Appcolors.buttons)
               tableView.layoutTableHeaderView()
        tableView.dataSource = self
               tableView.reloadData()
    }
    
    @IBAction func sendBtnAction(_ sender: Any) {
        reviewTxtView.textColor = .lightGray
        reviewTxtView.text = "Write a review"
    }
    @IBAction func ratingBtnAction(_ sender: Any) {
        popingBottomVC()
    }
    @IBAction func reviewBtnAction(_ sender: Any) {
        reviewContainerView.isHidden = false
        tableView.layoutTableHeaderView()
    }
    @IBAction func closeBtnAction(_ sender: Any) {
        reviewContainerView.isHidden = true
        tableView.layoutTableHeaderView()
    }
    func popingBottomVC() {
       let popupVC = UIStoryboard.timeLineStoryboard.instantiateRatingVC()
              //popupVC.delegate = self
           //popupVC.selectedIndex = 3
                     popupVC.height = 200
                     popupVC.topCornerRadius = 15
              popupVC.presentDuration = 0.25
                     popupVC.dismissDuration = 0.25
                     popupVC.shouldDismissInteractivelty = true
                    // popupVC.popupDelegate = self
      
        
        popupVC.controllerType = "Rating Review"
                     present(popupVC, animated: true, completion: nil)
       }
    
}
extension FeedbackViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FeedbackTableViewCell", for: indexPath) as! FeedbackTableViewCell
        cell.delegate = self
        return cell
    }
    
    
}
extension FeedbackViewController: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView == reviewTxtView {
            reviewTxtView.text = ""
            reviewTxtView.textColor = UIColor.init(hexString: Appcolors.text)
        }
    }
}
extension FeedbackViewController: SendingActionFromFeedBackTVC {
    func pushingTOProfileVC() {
        let vc = UIStoryboard.moreStoryboard.instantiateUserProfileVC()
        navigationController?.pushViewController(vc, animated: true)
    }
    
    
}
