//
//  MoreViewController.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 6/24/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import UIKit

class MoreViewController: UIViewController {
    var dataSource : [String]? {
        didSet {
            tableView.reloadData()
        }
    }
    @IBOutlet weak var NameLabel: UILabel!
    @IBOutlet var topView: UIView!
    @IBOutlet weak var topImage: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        currentTabBar?.selectedColor = UIColor.init(hexString: Appcolors.tabBarSelectedColor)
        topImage.setURLImage(imageURL: ((GraduateUrl.imageUrl + ((UserDefaultsHandler.getUDValue(key: .profileImage) as? String ?? "")))))
        tableView.dataSource = self
        tableView.delegate = self
        tableView.tableHeaderView = topView
        tableView.tableFooterView = UIView()
        tableView.backgroundColor = UIColor.init(hexString: Appcolors.background)
        // Do any additional setup after loading the view.
        addData()
        tappingImageAction()
       
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
         addGesturInNameLabel()
        //currentTabBar?.tabBarHeight = 50
        currentTabBar?.setBar(hidden: false, animated: true)
        navigationController?.setNavigationBarHidden(true, animated: true)
        NameLabel.text = UserDefaultsHandler.getUDValue(key: .fullName) as? String
    }
    override func viewDidAppear(_ animated: Bool) {
        NameLabel.layer.shadowColor = UIColor.black.cgColor
        NameLabel.layer.shadowRadius = 3.0
        NameLabel.layer.shadowOpacity = 1.0
        NameLabel.layer.shadowOffset = CGSize(width: 4, height: 4)
        NameLabel.layer.masksToBounds = false
        NameLabel.isUserInteractionEnabled = true
    }
    override func viewWillDisappear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(false, animated: true)
    }
    func tappingImageAction() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(tapName))
        topImage.addGestureRecognizer(tap)
    }
   
    func addData() {
        dataSource = ["My Uni", "My Post", "Notification", "Chats", "Invite Friends", "Ads", "Rate Graduation", "Report An Issue", "Settings and Privacy", "LOG OUT"]
    }
    @IBAction func profileNameTapped(_ sender: Any) {
        
       
    }
    @objc func thoughtsDetails() {
       
    }
    @IBAction func midBtnAction(_ sender: UIButton) {
       // sharePopOver(sender: sender)
    }
    func addGesturInNameLabel() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(tapName))
        NameLabel.isUserInteractionEnabled = true
        NameLabel.addGestureRecognizer(tap)
    }
    @objc func tapName() {
        let vc = UIStoryboard.timeLineStoryboard.instantiateUserProfileVC()
              // vc.profileControllerType = "OwnProfile"
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func showAlert() {
        let alert = UIAlertController(title: "Alert", message: "Coming Soon", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
              switch action.style{
              case .default:
                    print("default")

              case .cancel:
                    print("cancel")

              case .destructive:
                    print("destructive")


        }}))
        self.present(alert, animated: true, completion: nil)
    }
    
}
extension MoreViewController: UITableViewDataSource {
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tableView {
            return dataSource?.count ?? 0
            
        } else {
            return 0
        }
    }
    
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == tableView {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MoreMenuTableViewCell") as! MoreMenuTableViewCell
        cell.titleLabel.text = dataSource?[indexPath.row]
        cell.titleLabel.textColor = UIColor(hexString: Appcolors.text)
        cell.backgroundColor = UIColor(hexString: Appcolors.light2)
        return cell
        } else {
            return UITableViewCell()
        }
    }
    
    
}
extension MoreViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if dataSource?[indexPath.row] == MoreList.MyUni.rawValue {
            let vc = UIStoryboard.timeLineStoryboard.instantiatePostAndUniVC()
            vc.controllerType = "Uni"
            navigationController?.pushViewController(vc, animated: true)
              tableView.deselectRow(at: indexPath, animated: false)
        } else if dataSource?[indexPath.row] == MoreList.MyPost.rawValue{
               let vc = UIStoryboard.timeLineStoryboard.instantiatePostAndUniVC()
            vc.controllerType = "Post"
                navigationController?.pushViewController(vc, animated: true)
              tableView.deselectRow(at: indexPath, animated: false)
            } else if dataSource?[indexPath.row] == MoreList.Notification.rawValue {
            let vc = UIStoryboard.moreStoryboard.instantiateNotificationViewController()
            navigationController?.pushViewController(vc, animated: true)
              tableView.deselectRow(at: indexPath, animated: false)
                } else if dataSource?[indexPath.row] == MoreList.Chats.rawValue{
            let vc = UIStoryboard.timeLineStoryboard.instantiateMessageVC()
            navigationController?.pushViewController(vc, animated: true)
              tableView.deselectRow(at: indexPath, animated: false)
        } else if dataSource?[indexPath.row] == MoreList.InviteFriends.rawValue {
           let shareText = "Hello, world!"

                        let image = UIImage(named: "iconArrowDown")
                       let vc = UIActivityViewController(activityItems: [shareText, image!], applicationActivities: [])
                           present(vc, animated: true)
              tableView.deselectRow(at: indexPath, animated: false)
        } else if dataSource?[indexPath.row] == MoreList.Ads.rawValue {
           
            let vc = UIStoryboard.moreStoryboard.instantiateAdvertiseWithUsViewController()
            navigationController?.pushViewController(vc, animated: true)
             tableView.deselectRow(at: indexPath, animated: false)
              tableView.deselectRow(at: indexPath, animated: false)
        } else if dataSource?[indexPath.row] == MoreList.RateGraduation.rawValue {
            let vc = UIStoryboard.moreStoryboard.instantiateDummyVC()
            vc.dummyString = "Rate Graduation"
                     navigationController?.pushViewController(vc, animated: true)
             tableView.deselectRow(at: indexPath, animated: false)
        } else if dataSource?[indexPath.row] == MoreList.ReportAnIssue.rawValue {
            let vc = UIStoryboard.moreStoryboard.instantiateDummyVC()
             vc.dummyString = "Report An Issue"
                      navigationController?.pushViewController(vc, animated: true)
              tableView.deselectRow(at: indexPath, animated: false)
        } else if dataSource?[indexPath.row] == MoreList.SettingAndPricay.rawValue {
            let vc = UIStoryboard.moreStoryboard.instantiateSettingViewController()
            navigationController?.pushViewController(vc, animated: true)
              tableView.deselectRow(at: indexPath, animated: false)
        } else if dataSource?[indexPath.row] == MoreList.LogOut.rawValue {
            let vc = UIStoryboard.timeLineStoryboard.instantiateDeletePostPopUpVC()
            vc.popUpType = "Logout"
                                        let cardPopup = SBCardPopupViewController(contentViewController: vc)

                                        cardPopup.show(onViewController: self)
                           
//            let vc = UIStoryboard(name: "more", bundle: nil).instantiateViewController(withIdentifier: "TestSectionHeaderViewController")
//            navigationController?.pushViewController(vc, animated: true)
        }
            }
        
    
     func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
}

