//
//  AccountSettingReponse.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 9/16/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import Foundation
import ObjectMapper
class AccountSettingResponse: DefaultResponse {
    var data: LoginResponseModel?
    var success: Bool?
    var occupationList: [String] = []
override func mapping(map: Map) {
  super.mapping(map: map)
success <- map["success"]
}
// MARK: - GET USER FULL DETAILS
    class func requestToDeleteUser(showHud: Bool = true ,completionHandler:@escaping ((UserFullDetailsResponseModel?) -> Void)) {
        APIManager(urlString: GraduateUrl.deleteUserURL, method: .get).handleResponse(showProgressHud: showHud,completionHandler: { (response: UserFullDetailsResponseModel) in
              print(response)
                  completionHandler(response)
              }, failureBlock: {
                completionHandler(nil)
              })
      }
    // MARK: - GET USER SETTINGS
    class func requestToUserSettings(showHud: Bool = true ,completionHandler:@escaping ((UserSettingResponseModel?) -> Void)) {
      APIManager(urlString: GraduateUrl.userSettingsURL, method: .get).handleResponse(showProgressHud: showHud,completionHandler: { (response: UserSettingResponseModel) in
            print(response)
                completionHandler(response)
            }, failureBlock: {
              completionHandler(nil)
            })
    }
    // MARK: - GET BUSINESS SETTINGS
    class func requestToUserSettings(url: String,showHud: Bool = true ,completionHandler:@escaping ((UserSettingResponseModel?) -> Void)) {
      APIManager(urlString: url, method: .get).handleResponse(showProgressHud: showHud,completionHandler: { (response: UserSettingResponseModel) in
            print(response)
                completionHandler(response)
            }, failureBlock: {
              completionHandler(nil)
            })
    }
    //MARK:- UPDATE USER SETTINGS
    class func requestToUpdateUserSettings(params: [String: Any],showHud: Bool = true ,completionHandler:@escaping ((UserSettingResponseModel?) -> Void)) {
        APIManager(urlString: GraduateUrl.updateUserSettingsURL,parameters: params ,method: .post).handleResponse(showProgressHud: showHud,completionHandler: { (response: UserSettingResponseModel) in
              print(response)
                  completionHandler(response)
              }, failureBlock: {
                completionHandler(nil)
              })
      }
    //MARK:- UPDATE BUSINESS SETTINGS
    class func requestToUpdateBusinessSettings(url: String,params: [String: Any],showHud: Bool = true ,completionHandler:@escaping ((UserSettingResponseModel?) -> Void)) {
        APIManager(urlString: url,parameters: params ,method: .post).handleResponse(showProgressHud: showHud,completionHandler: { (response: UserSettingResponseModel) in
              print(response)
                  completionHandler(response)
              }, failureBlock: {
                completionHandler(nil)
              })
      }
    //MARK:- USERNAME CHECK API
       class func requestToUsernameCheck(params: [String: Any],showHud: Bool = true ,completionHandler:@escaping ((AccountSettingResponse?) -> Void)) {
           APIManager(urlString: GraduateUrl.usernameCheckURL,parameters: params ,method: .get).handleResponse(showProgressHud: showHud,completionHandler: { (response: AccountSettingResponse) in
                 print(response)
                     completionHandler(response)
                 }, failureBlock: {
                   completionHandler(nil)
                 })
         }
    
    //MARK:- USERNAME UPDATE API
          class func requestToUsernameUpdate(params: [String: Any],showHud: Bool = true ,completionHandler:@escaping ((AccountSettingResponse?) -> Void)) {
              APIManager(urlString: GraduateUrl.updateUsernameURL,parameters: params ,method: .post).handleResponse(showProgressHud: showHud,completionHandler: { (response: AccountSettingResponse) in
                    print(response)
                        completionHandler(response)
                    }, failureBlock: {
                      completionHandler(nil)
                    })
            }
}

 class UserSettingResponseModel: DefaultResponse {

// MARK: Declaration for string constants to be used to decode and also serialize.
internal let kUserSettingResponseModelSettingsKey: String = "settings"


// MARK: Properties
public var settings: [Settings]?



// MARK: ObjectMapper Initalizers
/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/

/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/
    override func mapping(map: Map) {
    settings <- map[kUserSettingResponseModelSettingsKey]

}
}
public class Settings: Mappable {
    public required init?(map: Map) {
        
    }
    

// MARK: Declaration for string constants to be used to decode and also serialize.
internal let kSettingsSettingValueKey: String = "setting_value"
internal let kSettingsUserIdKey: String = "user_id"
internal let kSettingsCreatedAtKey: String = "created_at"
internal let kSettingsInternalIdentifierKey: String = "id"
internal let kSettingsUpdatedAtKey: String = "updated_at"
internal let kSettingsSettingNameKey: String = "setting_name"


// MARK: Properties
public var settingValue: String?
public var userId: Int?
public var createdAt: String?
public var internalIdentifier: Int?
public var updatedAt: String?
public var settingName: String?



// MARK: ObjectMapper Initalizers
/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/


/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/
    public func mapping(map: Map) {
    settingValue <- map[kSettingsSettingValueKey]
    userId <- map[kSettingsUserIdKey]
    createdAt <- map[kSettingsCreatedAtKey]
    internalIdentifier <- map[kSettingsInternalIdentifierKey]
    updatedAt <- map[kSettingsUpdatedAtKey]
    settingName <- map[kSettingsSettingNameKey]

}
}
