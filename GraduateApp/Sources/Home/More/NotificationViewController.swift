//
//  NotificationViewController.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 7/2/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import UIKit
import Popover
struct NotificationModel {
    var notificationData: NotificationResponseModel?
    var numOfPage: Int
    var loadMore: Bool
}
class NotificationViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var popupTableView = UITableView(frame: CGRect(x: 0, y: 0, width: 90, height: 120))
    var texts = ["Share", "Edit", "Delete"]
    var deleteingIndex: IndexPath!
    var popover: Popover!
    fileprivate var popoverOptions: [PopoverOption] = [
        .type(.left),
        .blackOverlayColor(UIColor(white: 0.0, alpha: 0.6))
    ]
    var dataSource: NotificationModel?{
        didSet {
            tableView.reloadData()
        }
    }
    var notificationId: String?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        
        
        self.title = "Notification"
        
       
        registerView()
        tableView.dataSource = self
        tableView.delegate = self
        tableView.tableFooterView = UIView()
        dataSource = NotificationModel(notificationData: nil, numOfPage: 1, loadMore: false)
        requestToUserNotifications(pageNo: 1, isLoadMore: false, showHud: true)
        pullToRefresh()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: true)
        currentTabBar?.setBar(hidden: true, animated: true)
    }
    func registerView() {
        tableView.register(UINib(nibName: "PostCreationTableViewCell", bundle: nil), forCellReuseIdentifier: "PostCreationTableViewCell")
    }
    //MARK:- PULL TO REFRESH
        func pullToRefresh() {
            self.tableView.es.addPullToRefresh {
                [unowned self] in
              
              requestToUserNotifications(pageNo: 1, isLoadMore: false, showHud: false)
            }
        }
       
//    func addData() {
//        dataSource = [NotificationModel(image: "images", Title: "Someone gave u a diamond", desc: "1 minute ago"), NotificationModel(image: "profilePicture2", Title: "Emily commented on your post", desc: "25 minutes ago"), NotificationModel(image: "profileImage1", Title: "Michael commented on your post", desc: "2 days ago")]
//    }
    //MARK:- GET USER NOTIFICATIONS
    func requestToUserNotifications(pageNo: Int,isLoadMore: Bool = false, showHud: Bool) {
        let param:[String : Any] = ["skip_read_notification" : "false", "page" : pageNo]
        NotificationsResponse.requestToUserNotifications(param: param,showHud: showHud) { [weak self](response) in
            guard let strongSelf = self else { return }
            strongSelf.tableView.es.stopPullToRefresh()
            print(response)
            strongSelf.tableView.tableFooterView = nil
            strongSelf.tableView.tableFooterView?.isHidden = true
            if !isLoadMore {
                strongSelf.dataSource?.notificationData = response
              strongSelf.tableView.reloadData()
            } else {
              if  var allData = response?.data {
                print(allData)
                for data in allData {
                    strongSelf.dataSource?.notificationData?.data?.append(data)
                }
                strongSelf.tableView.reloadData()
              }
            }
            print(strongSelf.dataSource?.notificationData?.data?.count, response?.meta?.total)
            if strongSelf.dataSource?.notificationData?.data?.count ?? 0 < response?.meta?.total ?? 0 {
                     strongSelf.dataSource?.loadMore = true
                     strongSelf.dataSource?.numOfPage += 1
                   } else {
                     strongSelf.dataSource?.loadMore = false
                   }
        }
    }
    
    //MARK:- REQUEST TO DELETE NOTIFICAITON
    func requestToDeleteNotification(id: String) {
        NotificationsResponse.requestToDeleteNotifications(id: notificationId ?? "") { [weak self](response) in
            guard let strongSelf = self else { return }
            strongSelf.tableView.reloadData()
        }
    }
}

//MARK:- TABLE VIEW DATASOURCE
extension NotificationViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
      
        return dataSource?.notificationData?.data?.count ?? 0
      
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(tapProfileImage))
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationTableViewCell", for: indexPath) as! NotificationTableViewCell
        cell.mainImage.setURLImage(imageURL: ((GraduateUrl.imageUrl) + (dataSource?.notificationData?.data?[indexPath.row].sender?.profile?.profileImage ?? "")), placeHolder: "defaultPicture")
//        cell.mainImage.setURLImage(imageURL: ((GraduateUrl.imageUrl) + (dataSource?.notificationData?.data?[indexPath.row].sender?.profile?.profileImage ?? "")))
        let newDate = HelperFunctions.timeInterval(timeAgo: dataSource?.notificationData?.data?[indexPath.row].createdAt ?? "")
                  cell.descLabel.text = newDate
        print(dataSource?.notificationData?.data?[indexPath.row].internalIdentifier)
        cell.titleLabel.text = dataSource?.notificationData?.data?[indexPath.row].data?.message_title
        cell.index = indexPath
        cell.delegate = self
        cell.mainImage.addGestureRecognizer(tap)
        cell.notificationId = dataSource?.notificationData?.data?[indexPath.row].internalIdentifier
        return cell
        
    }
    @objc func tapProfileImage() {
        let vc = UIStoryboard.moreStoryboard.instantiateUserProfileVC()
        
        navigationController?.pushViewController(vc, animated: true)
    }
    func bottomSelection(type: String, height: Int) {
        let popupVC = UIStoryboard.timeLineStoryboard.instantiateSelectionBottomPopUpVC()
               popupVC.delegate = self
               popupVC.textFieldType = type
        popupVC.height = CGFloat(height)
               popupVC.topCornerRadius = 15
               popupVC.presentDuration = 0.25
               popupVC.dismissDuration = 0.25
               popupVC.shouldDismissInteractivelty = true
               // popupVC.popupDelegate = self
               present(popupVC, animated: true, completion: nil)
    }
    
}

//MARK:- TABLE VIEW DELEGATE
extension NotificationViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if dataSource?.notificationData?.data?[indexPath.row].type == NotificationType.postInteraction.rawValue || dataSource?.notificationData?.data?[indexPath.row].type == NotificationType.commentInteration.rawValue{
            
              let nav = UIStoryboard.moreStoryboard.instantiateNotificaitonDetailsVC()
            print(dataSource?.notificationData?.data?[indexPath.row].data?.post?.id)
            print(dataSource?.notificationData?.data?[indexPath.row].data?.associatedUser?.email)
            nav.interactionType = dataSource?.notificationData?.data?[indexPath.row].data?.interaction_type
            nav.postId = dataSource?.notificationData?.data?[indexPath.row].data?.post?.id
              navigationController?.pushViewController(nav, animated: true)
        } else if dataSource?.notificationData?.data?[indexPath.row].type == NotificationType.followed.rawValue || dataSource?.notificationData?.data?[indexPath.row].type == NotificationType.friendRequest.rawValue {
            let vc = UIStoryboard.timeLineStoryboard.instantiateOtherUserProfileVC()
            vc.userId = dataSource?.notificationData?.data?[indexPath.row].sender?.internalIdentifier
            navigationController?.pushViewController(vc, animated: true)
        } else if dataSource?.notificationData?.data?[indexPath.row].type == NotificationType.businessMessageReceived.rawValue {
            let vc = UIStoryboard.moreStoryboard.instantiateMessageVC()
            vc.id = dataSource?.notificationData?.data?[indexPath.row].data?.businessId
            navigationController?.pushViewController(vc, animated: true)
        } else if dataSource?.notificationData?.data?[indexPath.row].type == NotificationType.uniprofile
            .rawValue || dataSource?.notificationData?.data?[indexPath.row].type == NotificationType.businessPostInteraction
            .rawValue{
            let vc = UIStoryboard.graduateStoryboard.instantiateGraduateDetailsViewController()
            vc.businessId = dataSource?.notificationData?.data?[indexPath.row].data?.business?.id
                  // vc.delegate = self
                   navigationController?.pushViewController(vc, animated: true)
        }
//
//
//
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let allData = self.dataSource
               print(allData?.loadMore ?? false)
               print(indexPath.row,"indexPath")
               print(dataSource?.notificationData?.data?.count,"dataCount")
               guard allData?.loadMore == true, let dataCount = allData?.notificationData?.data?.count, indexPath.row == dataCount - 1  else {return}
                 print(dataCount)
                 let lastSectionIndex = tableView.numberOfSections - 1
                 let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
                 if indexPath.section == lastSectionIndex && indexPath.row == lastRowIndex {
                   // print("this is the last cell")
                   let spinner = UIActivityIndicatorView(style: .gray)
                   spinner.startAnimating()
                   spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
                   self.tableView.tableFooterView = spinner
                   self.tableView.tableFooterView?.isHidden = false
                 }
                // print(allData.url)
        requestToUserNotifications(pageNo: dataSource!.numOfPage, isLoadMore: dataSource!.loadMore, showHud: false)
    }
    
}
extension NotificationViewController: RemovingNotificationFromList {
    func deletingNotification(index: IndexPath, id: String?) {
        notificationId = id
        deleteingIndex = index
               bottomSelection(type: "Notification Delete", height: 50)
    }
    
   
    func actionShet(index: IndexPath) {
        let optionMenu = UIAlertController(title: nil, message: "Choose Option", preferredStyle: .actionSheet)
        
        let deleteAction = UIAlertAction(title: "Delete", style: .default, handler:
        {
            (alert: UIAlertAction!) -> Void in
            self.dataSource?.notificationData?.data?.remove(at: index.row)
            
        })
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel
            , handler:
            {
                (alert: UIAlertAction!) -> Void in
                self.dismiss(animated: true, completion: nil)
        })
        
        
        optionMenu.addAction(deleteAction)
        optionMenu.addAction(cancelAction)
        self.present(optionMenu, animated: true, completion: nil)
    }
      
}
extension NotificationViewController: SendingMultipleSeclections{
    func postAsActionFromInside(value: Int?, bool: Bool, index: Int?, type: CreateSpotVCTypes, posterIds: [Int], postData: CreateEventModel) {
        
    }
    func gettingTheSelectedValueFromTheList(value: Int?, bool: Bool, index: Int?, type: CreateSpotVCTypes, posterIds: [Int]) {
        
    }
    
    

    
    func sendingUserId(id: Int, index: Int) {
        
    }
    
    func sendingMediaType(value: Int) {
        
    }
    
    func sendingMultipleVlaues(values: [String]) {
        
    }
    
    func sendingDiscountType(value: String) {
        //MARK-: NOTIFICATION DELETE ACTION
        self.dataSource?.notificationData?.data?.remove(at: deleteingIndex.row)
        print(notificationId)
        requestToDeleteNotification(id: notificationId ?? "")
        
    }
    
    
    
    func sendingUniCategory(value: String) {
        
    }
    
    func gettingSelectedVaueFromProfileMoreBtn(value: Int, bool: Bool, index: Int) {
        
    }
    
    func gettingFullScreenCoverPic() {
        
    }
    
    func sendingEvnetType(value: String, type: String) {
        
    }
    
    
}
enum NotificationType: String {
    case postInteraction = "App\\Notifications\\PostInteractionNotification"
    case friendRequest = "App\\Notifications\\FriendRequestReceivedNotification"
    case uniprofile = "App\\Notifications\\BusinessInteractionNotification"
    case commentInteration =  "App\\Notifications\\CommentInteractionNotification"
    case businessMessageReceived = "App\\Notifications\\BusinessMessageReceivedNotification"
    case businessPostInteraction = "App\\Notifications\\BusinessPostInteractionNotification"
    case followed = "App\\Notifications\\UserInteractionNotification"
}

