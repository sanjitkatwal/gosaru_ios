//
//  UserProfileViewController.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 7/28/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import UIKit
import AVFoundation
import AMPopTip
import MobileCoreServices
import YYCalendar
import Popover
import MediaPlayer
import SwiftAudio
import Alamofire
import SVProgressHUD
import GooglePlaces
import AssetsLibrary
import MobileCoreServices
struct UserDetailsModel {
    var about: UserFullDetailsResponseModel?
    var uni: Any?
    var post: Any?
    var friends: FriendsLisTypes?
}
struct FriendsLisTypes {
    var friends: FriendsResponseModel?
    var allFriendship: FriendshipResponseModel?
    var pending: Any?
    var declined: Any?
    var blocked: Any?
}
class UserProfileViewController: UIViewController {
    
    @IBOutlet weak var tableViewTopConststraints: NSLayoutConstraint!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var friendsBtn: AnimatedButton!
    @IBOutlet weak var chatBtn: AnimatedButton!
    @IBOutlet weak var postBtn: AnimatedButton!
    @IBOutlet weak var uniBtn: AnimatedButton!
    @IBOutlet weak var aboutBtn: AnimatedButton!
    @IBOutlet weak var headerBtn: AnimatedButton!
    @IBOutlet weak var tableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var tableViewHeightConstraint: UITableView!
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var categoryBtn: AnimatedButton!
    @IBOutlet weak var inviteBtn: AnimatedButton!
    @IBOutlet weak var searchBtn: AnimatedButton!
    @IBOutlet weak var inviteContainerView: UIView!
    @IBOutlet weak var followersLabel: UILabel!
    @IBOutlet weak var followersBtn: AnimatedButton!
    @IBOutlet weak var audioBtn: AnimatedButton!
    @IBOutlet weak var moreBtn: UIButton!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var parentScrollView: UIScrollView!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var buttonsContainerView: UIView!
    @IBOutlet weak var coverImage: UIImageView!
    @IBOutlet weak var audioContainerView: UIView!
    @IBOutlet weak var tableView: UITableView!
    var soundFileURL:URL!
     var audio_data: Data? = nil
    var navigationBarOriginalOffset : CGFloat?
    var childScrollingDownDueToParent = false
    var tableViewscrollWhole: Bool?
    var audioPlayer: AVAudioPlayer?
    var audioBtnState: Bool?
    var searchState: Bool?
    var tableViewHeights = 0
    var tableviewFrame: CGRect?
    var defaultMusic: [String: Any] = [:]
    var filteredTableData = [String]()
    var screenheight = UIScreen.main.bounds.height
    var requestCell: Bool?
    var tableData: TableDataType?
    var names = ["Michael","Peter","Jhon","Billy","Dev","Paul","Dimebag"]
    var texts = ["Share", "Edit", "Delete"]
    var friendsCategoryView = FriendListCategory() as FriendListCategory
    let friendPopTip = PopTip()
    var coverImageStorage: UIImage?
    var mediaPicker: MPMediaPickerController?
    var previousView: Bool?
    var popover: Popover!
    var userName: UILabel!
    var updated: Bool = false
    var uniDataSource: GraduateUniversityResponseModel?
    var userType: String?
    var playing: Bool? = false
    var defaultMusicTitle: String?
    var friendsData: FriendsResponseModel? {
        didSet {
            tableView.reloadData()
        }
    }
    var postTypes: GraduateDetails? //used for the editing the post
    var editedIndex: IndexPath! //index path that is edited
    var postDataSource: PostAndUniModel? {
        didSet {
            tableView.reloadData()
        }
    }
    var anyData: Any? {
        didSet {
            tableView.reloadData()
        }
    }
    var popupTableView = UITableView(frame: CGRect(x: 0, y: 0, width: 90, height: 120))
    fileprivate var popoverOptions: [PopoverOption] = [
        .type(.left),
        .blackOverlayColor(UIColor(white: 0.0, alpha: 0.6))
    ]
    let swiftAudioPlayer = AudioPlayer()
    var heightAtIndexPath = NSMutableDictionary()
    var dataSource: UserFullDetailsResponseModel? {
        didSet {
            tableView.reloadData()
        }
    }
    var duplicateDataSouce: UserFullDetailsResponseModel?
    var sound: Any?
    var mediaItems = [MPMediaItem]()
    var professionList: [String] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        swiftAudioPlayer.event.stateChange.addListener(self, handleAudioPlayerStateChange)
        
        self.title = "Profile"
        initialView()
        registeriViews()
        addingBarButtons()
        settingPopTip()
        tableviewFrame = tableView.frame
        searchBar.delegate = self
        filteredTableData = names
        parentScrollView.delegate = self
        tableView.delegate = self
        tableView.dataSource = self
        tableView.isScrollEnabled = false
        tableViewscrollWhole = true
        tableData = .about
        if #available(iOS 13.0, *) {
            searchBar.searchTextField.textColor = UIColor.init(hexString: Appcolors.text)
        } else {
            // Fallback on earlier versions
            let textFieldInsideSearchBar = searchBar.value(forKey: "searchField") as? UITextField
            
            textFieldInsideSearchBar?.textColor = UIColor.init(hexString: Appcolors.text)
        }
        getFullUserDetails()
        getProfessionList()
        registerView()
        tableView.reloadData()
        postDataSource = PostAndUniModel(uniData: nil, myPost: nil, numOfPage: 1, loadMore: false)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
        navigationController?.setNavigationBarHidden(false, animated: true)
        
        currentTabBar?.setBar(hidden: true, animated: true)
       
        
        //        if previousView == true {
        //            btnscollectionActon(postBtn)
        //           // posActionCallBak()
        //        }
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.isNavigationBarHidden = false
        swiftAudioPlayer.stop()
        audioPlayer?.stop()
        currentTabBar?.setBar(hidden: true, animated: true)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        navigationController?.isNavigationBarHidden = false
        currentTabBar?.setBar(hidden: true, animated: true)
       
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        switch tableData {
        case .about:
            tableView.tableHeaderView = nil
            parentScrollView.isScrollEnabled = true
            self.tableViewHeight?.constant = self.tableView.intrinsicContentSize.height
            tableViewHeight.constant = tableView.contentSize.height
            tableView.isScrollEnabled = false
            
        case .uni:
            print("")
            tableViewHeight.constant = screenheight - 180
            //              DispatchQueue.main.async {
            //
            //                self.tableViewHeight?.constant = self.tableView.intrinsicContentSize.height
            //                self.view.layoutIfNeeded()
            //            //tableViewHeight.constant = screenheight - 100
        //            }
        case .post:
            tableViewHeight.constant = screenheight - 120
            //tableViewHeight.constant = screenheight - 100
        //super.updateViewConstraints()
        case .chats:
            tableViewHeight.constant = screenheight - 30
        case .friends:
            
            tableViewHeight.constant = screenheight - 100
        case .pushedUni:
            topView.isHidden = true
            tableViewHeight.constant = screenheight - 100
        case .pushedPost:
            topView.isHidden = true
            tableViewHeight.constant = screenheight - 100
            
        default:
            tableViewHeight.constant = tableView.contentSize.height
        }
    }
//MARK:- REGISTER VIEWS
    func registerView() {
        tableView.register(UINib(nibName: "GraduateTableViewCell", bundle: nil), forCellReuseIdentifier: "GraduateTableViewCell")
        tableView.register(UINib(nibName: "PostCreationTableViewCell", bundle: nil), forCellReuseIdentifier: "PostCreationTableViewCell")
        tableView.register(UINib(nibName: "PostCreationTableViewCell", bundle: nil), forCellReuseIdentifier: "PostCreationTableViewCell")
        tableView.register(UINib(nibName: "PortFolioTableViewCell", bundle: nil), forCellReuseIdentifier: "PortFolioTableViewCell")
        tableView.register(UINib(nibName: "EventTableViewCell", bundle: nil), forCellReuseIdentifier: "EventTableViewCell")
        tableView.register(UINib(nibName: "DiscountTableViewCell", bundle: nil), forCellReuseIdentifier: "DiscountTableViewCell")
        tableView.register(UINib(nibName: "LiveTableViewCell", bundle: nil), forCellReuseIdentifier: "LiveTableViewCell")
    }
    func handleAudioPlayerStateChange(state: AudioPlayerState) {
        // Handle the event
    }
    //MARK:- GETTING ALL THE FULL USERDETAILS
    func getFullUserDetails() {
        UserProfileFullDetails.requestToUserFullDetails { [weak self](response) in
            guard let strongSelf = self else {return}
            strongSelf.dataSource = response
            strongSelf.duplicateDataSouce = response
            
            strongSelf.followersLabel.text = "\(strongSelf.dataSource?.data?.followsCount.optionalUnWrapped ?? "")" + " followers"
            if response?.data?.profile?.profileImage == nil {
                strongSelf.coverImage.image = UIImage(named: "defaultPicture")
            } else {
                strongSelf.coverImage.setURLImage(imageURL: GraduateUrl.imageUrl + (strongSelf.dataSource?.data?.profile?.profileImage ?? ""))
                if response?.data?.profile?.defaultMusic == nil {
                    strongSelf.audioBtn.setImage(UIImage(named: "upload"), for: .normal)
                    strongSelf.audioBtn.imageView?.contentMode = .center
                }
                
            }
        }
    }
    
//MARK:- GET USERPOST
    func gettingAllPost(pageNo: Int = 1, showLoader: Bool = true, isLoadMore: Bool = false) {
        let posterId = UserDefaultsHandler.getUDValue(key: .userID)
        let params: [String : Any] = ["poster_type" : "user", "poster_id" : "\(posterId ?? 0)", "page" : "\(pageNo)"]
        
        PostAndUniResponse.requestToMyPost(params: params, showHud: showLoader) { [weak self](response) in
            guard let strongSelf = self else { return }
            print(response,isLoadMore)
            strongSelf.tableView.tableFooterView = nil
            strongSelf.tableView.tableFooterView?.isHidden = true
            print(strongSelf.postDataSource?.loadMore)
            if !isLoadMore {
                strongSelf.postDataSource?.myPost = response
                strongSelf.tableView.reloadData()
            } else {
                if let allData = response?.data {
                    for data in allData {
                        strongSelf.postDataSource?.myPost?.data?.append(data)
                    }
                }
                
            }
            if strongSelf.postDataSource?.myPost?.data?.count ?? 0 < response?.meta?.total ?? 0 {
                strongSelf.postDataSource?.loadMore = true
                strongSelf.postDataSource?.numOfPage += 1
            } else {
                strongSelf.postDataSource?.loadMore = false
            }
        }
    }
    

//    func getOtherUserFullDetails() {
//        UserProfileFullDetails.requestToOtherUserDetails(id: userId) { [weak self](response) in
//            guard let strongSelf = self else { return }
//            strongSelf.dataSource = response
//                       strongSelf.duplicateDataSouce = response
//
//                       strongSelf.followersLabel.text = "\(strongSelf.dataSource?.data?.followsCount.optionalUnWrapped ?? "")" + " followers"
//                       if response?.data?.profile?.profileImage == nil {
//                           strongSelf.coverImage.image = UIImage(named: "defaultPicture")
//                       } else {
//                           strongSelf.coverImage.setURLImage(imageURL: GraduateUrl.imageUrl + (strongSelf.dataSource?.data?.profile?.profileImage ?? ""))
//                           if response?.data?.profile?.defaultMusic == nil {
//                               strongSelf.audioBtn.setImage(UIImage(named: "upload"), for: .normal)
//                               strongSelf.audioBtn.imageView?.contentMode = .center
//                           }
//
//                       }
//        }
//    }
    
    //MARK:- REQUEST TO GET FRIRENDS
    func getFriendsList() {
        UserProfileFullDetails.requestToFriends { [weak self](response) in
            guard let strongSelf = self else { return }
            strongSelf.anyData = response
        }
    }
    
    //MARK:- FOLLOW API FOR UNIVERSITY
    func followUniversity(id: Int) {
        GraduateUniversityResponse.requestToFollowUniversity(userId: id) { (response) in
            
        }
}
    
    //MARK:- UNFOLLOW API FOR UNIVERSITY
    func unfollowUniversity(id: Int) {
        GraduateUniversityResponse.requestToUnfollowUniversity(userId: id) { [weak self](response) in
           
            
        }
    }
    
    //MARK:- GET USER UNIVERSITY
    func getUserUiversity() {
        GraduateUniversityResponse.requestToGetUserUni { [weak self](response) in
            guard let strongSelf = self else { return }
            strongSelf.uniDataSource = response
            strongSelf.tableView.reloadData()
            }
        }
//MARK:- GETTING RROFESSIONAL LIST
    func getProfessionList() {
        var header = [String : Any]()
        if  let token = UserDefaultsHandler.getUDValue(key: UDkey.token) as? String {
            header = ["Authorization": "Bearer \(token)", "Accept": "application/json"]
        } else {
            header = ["Accept" : "application/json"]
        }
        debugPrint(header)
        Alamofire.request(GraduateUrl.occupationListURL,
                          method: .get, encoding: JSONEncoding.default, headers: header as! HTTPHeaders)
            .validate()
            .responseJSON { response in
                guard response.result.isSuccess else {
                    //            print("Error while fetching remote rooms: \(String(describing: response.result.error)"))
                    print(response.error)
                    
                    return
                }
                print(response)
                if let value = response.result.value as? [[String]] {
                    for item in value[0] {
                        print(item)
                        self.professionList.append(item)
                    }
                } else {
                    print(response.error)
                }
                
        }
    }

    
    func downloadFileFromURL(url:NSURL) {
        var downloadTask:URLSessionDownloadTask
        downloadTask = URLSession.shared.downloadTask(with: url as URL, completionHandler: { [weak self](URL, response, error) -> Void in
            // let urlstrings = NSURL(string: URL?.path ?? "")
            //
            print(URL?.path)
            // self?.sound = URL?.path
            // let url = NSURL(string: urlstring)
            self?.play(url: URL as Any)
            //self?.sound = URL! as NSURL
        })
        
        downloadTask.resume()
        
    }
    func play(url:Any) {
        print("playing \(url)")
        sound = url as! URL
        do {
            
            //            audioPlayer = try AVAudioPlayer(contentsOf: sound as! URL)
            //            audioPlayer?.prepareToPlay()
            //            audioPlayer?.volume = 1.0
            //            audioPlayer?.play()
        } catch let error as NSError {
            //self.player = nil
            print(error.localizedDescription)
        } catch {
            print("AVAudioPlayer init failed")
        }
        
    }
    
    @objc func settingBtnAction() {
        let vc = UIStoryboard.moreStoryboard.instantiateaccountSettingViewController()
        navigationController?.pushViewController(vc, animated: true)
    }
    func addingBarButtons() {
        let button = UIButton()
        button.setImage(UIImage(named: "iconSetting"), for: .normal)
        button.imageView?.tintColor = UIColor.init(hexString: Appcolors.buttons)
        button.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        button.addTarget(self, action: #selector(settingBtnAction), for: .touchUpInside)
        let stackview = UIStackView.init(arrangedSubviews: [button])
        stackview.distribution = .equalSpacing
        stackview.axis = .horizontal
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: stackview)
    }
    func initialView() {
        searchView.isHidden = true
        inviteContainerView.isHidden = true
        audioContainerView.isHidden = false
        highlightingButtons(button: aboutBtn)
        audioBtn.setImage(UIImage(named: "play1"), for: .normal)
        moreBtn.setImage(UIImage(named: "more-3"), for: .normal)
        searchBtn.setImage(UIImage(named: "iconSearch"), for: .normal)
        searchBtn.imageView?.tintColor = UIColor.init(hexString: Appcolors.buttons)
        categoryBtn.setImage(UIImage(named: "iconMenu"), for: .normal)
        categoryBtn.imageView?.tintColor = UIColor.init(hexString: Appcolors.buttons)
        moreBtn.imageView?.contentMode = .center
        coverImage.isHidden = false
        
        tableView.tableHeaderView = UIView()
    }
    func settingPopTip() {
        friendsCategoryView = (Bundle.main.loadNibNamed(String(describing: FriendListCategory.self), owner: self, options: nil)?.first as? FriendListCategory)!
        //friendsCategoryView?.friendsBtn.leftImage(image: "iconUnchecked")
        friendsCategoryView.delegate = self
        friendsCategoryView.frame.size = CGSize(width: 170, height: 238)
    }
    func registeriViews() {
        tableView.register(UINib(nibName: "GraduateTableViewCell", bundle: nil), forCellReuseIdentifier: "GraduateTableViewCell")
        tableView.register(UINib(nibName: "PostCreationTableViewCell", bundle: nil), forCellReuseIdentifier: "PostCreationTableViewCell")
        tableView.register(UINib(nibName: "ProfileInformationTableViewCell", bundle: nil), forCellReuseIdentifier: "ProfileInformationTableViewCell")
        tableView.register(UINib(nibName: "FriendListTableViewCell", bundle: nil), forCellReuseIdentifier: "FriendListTableViewCell")
    }
    func sharePopOver(sender: UIButton) {
        popupTableView.reloadData()
        if texts.count == 2 {
            popupTableView.frame = CGRect(x: 0, y: 0, width: 90, height: 80)
            
        } else {
            popupTableView.frame = CGRect(x: 0, y: 0, width: 90, height: 120)
            
        }
        popupTableView.delegate = self
        popupTableView.dataSource = self
        popupTableView.isScrollEnabled = false
        self.popover = Popover(options: self.popoverOptions)
        self.popover.willShowHandler = {
            print("willShowHandler")
        }
        self.popover.didShowHandler = {
            print("didDismissHandler")
        }
        self.popover.willDismissHandler = {
            print("willDismissHandler")
        }
        self.popover.didDismissHandler = {
            print("didDismissHandler")
        }
        
        self.popover.show(popupTableView, fromView: sender)
    }
   
    func closeView() {
        let transition = CATransition()
        transition.duration = 0.5
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        transition.type = CATransitionType.reveal
        transition.subtype = CATransitionSubtype.fromBottom
        navigationController?.view.layer.add(transition, forKey: nil)
        _ = navigationController?.popViewController(animated: false)
    }
    @IBAction func moreBtnAction(_ sender: Any) {
        let vc = UIStoryboard.timeLineStoryboard.instantiateSelectionBottomPopUpVC()
        vc.delegate = self
        vc.textFieldType = "ProfileMoreBtn"
        vc.height = 100
        vc.topCornerRadius = 15
        vc.presentDuration = 0.25
        vc.dismissDuration = 0.25
        vc.shouldDismissInteractivelty = true
        // popupVC.popupDelegate = self
        present(vc, animated: true, completion: nil)
    }
    @IBAction func headerBtnAction(_ sender: Any) {
        if headerBtn.titleLabel?.text == "Create Uni" {
            let vc = UIStoryboard.timeLineStoryboard.instantiateCreateUniversityVC()
            vc.callbackClosure = {[weak self] in
                self?.btnscollectionActon((self?.uniBtn)!)
            }
            navigationController?.pushViewController(vc, animated: true)
            
        } else if headerBtn.titleLabel?.text == "Create Post" {
            let vc = UIStoryboard.timeLineStoryboard.instantiateCreateSpotViewController()
           // vc.controllerType = "Create Post"
            vc.vcType = .createPostFromOutside
            vc.callbackClosure = {[weak self] in
                self?.btnscollectionActon((self?.postBtn)!)
                //                self?.posActionCallBak()
                //                self?.previousView = true
                //self?.postBtn.sendActions(for: .touchUpInside)
                //                self?.tableData = .post
                //                print(self?.tableData)
                //                self?.tableView.reloadData()
                
                
                
            }
            navigationController?.pushViewController(vc, animated: true)
        }
    }
    @IBAction func postAction(_ sender: Any) {
        coverImage.isHidden = true
        audioContainerView.isHidden = true
        parentScrollView.scrollToTop()
        parentScrollView.isScrollEnabled = false
        tableView.isScrollEnabled = true
        tableView.tableHeaderView = headerView
        //
        // navigationBarOriginalOffset = navigationController?.navigationBar.frame.origin.y
        self.view.layoutIfNeeded()
        tableViewscrollWhole = false
        //        tableView.setContentOffset(.zero, animated:true)
        //       tableView.scroll(to: .top, animated: true)
        //        func scrollToFirstRow() {
        //            let indexPath = NSIndexPath(row: 0, section: 0)
        //            self.tableView.scrollToRow(at: [0,0], at: .top, animated: true)
        //        }
        // tableView.tableHeaderView = UIView()
        
    }
    
    @IBAction func chatAction(_ sender: Any) {
        audioContainerView.isHidden = false
    }
    
    @IBAction func audioBtnAction(_ sender: UIButton) {
        if (audioBtn.currentImage?.isEqual(UIImage(named: "upload")))! {
//            showAudioPicker(sender: sender)
            showDocumentPicker()
        } else {
            if mediaItems.count != 0 {
                if audioBtnState == true {
                   
                    audioBtn.setImage(UIImage(named: "play1"), for: .normal)
                    audioPlayer?.pause()
                    audioBtnState = false
                } else {
                    //updatePlayer()
                    audioPlayer?.play()
                    audioBtnState = true
                    audioBtn.setImage(UIImage(named: "pause1"), for: .normal)
                }
            } else {
            if audioBtnState == true {
                audioBtn.setImage(UIImage(named: "play1"), for: .normal)
                //sender.isSelected = false
                audioBtnState = false
                swiftAudioPlayer.pause()
                //audioPlayer!.stop()
            } else {
                audioBtnState = true
                audioBtn.isHighlighted = false
                audioBtn.adjustsImageWhenHighlighted = false
                // sender.isSelected = true
                audioBtn.setImage(UIImage(named: "pause1"), for: .normal)
                audioBtn.backgroundColor = .clear
                // audioBtn.setImage(UIImage(named: "pause1"), for: .normal)
                // audioBtn.imageView?.tintColor = UIColor.init(hexString: Appcolors.buttons)
                // let sound = Bundle.main.path(forResource: "sampleAudio", ofType: "mp3")
                let urlstring =  (GraduateUrl.imageUrl + (dataSource?.data?.profile?.defaultMusic ?? ""))
                print(urlstring)
                print(sound)
                

                let url = NSURL(string: urlstring)
                //strongSelf.sound = urlstring
               // print("the url = \(url!)")
                //let url = sound
                if playing == false {
                    let audioItem = DefaultAudioItem(audioUrl: urlstring, sourceType: .stream)
                    
                    //downloadFileFromURL(url: url!)
                    do {
                        print(sound)
                        try swiftAudioPlayer.load(item: audioItem, playWhenReady: true)
                        playing = true
                        //let url = NSURL(string: sound ?? "")
                        // self.play(url: sound as! NSURL)
                        //                audioPlayer = try AVAudioPlayer(contentsOf: sound!)
                        //                audioPlayer?.prepareToPlay()
                        //                audioPlayer?.play()
                    }
                    catch {
                        print("Error audio not found")
                        
                    }
                } else {
                    swiftAudioPlayer.play()
                }
            }
            }
        }
    }
    func showDocumentPicker() {
        let types: [String] = [kUTTypeMP3 as String, kUTTypeMPEG as String]
        let documentPicker = UIDocumentPickerViewController(documentTypes: types, in: .import)
        documentPicker.delegate = self
        self.present(documentPicker, animated: true, completion: nil)
    }
    @IBAction func followersAction(_ sender: UIButton) {
        let vc = UIStoryboard.moreStoryboard.instantiateFollowersVC()
        navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func btnscollectionActon(_ sender: UIButton) {
        switch sender.tag {
        //MARK:- ABOUT BUTTON
        case 0:
            friendPopTip.hide()
            viewWillLayoutSubviews()
            parentScrollView.isScrollEnabled = true
            highlightingButtons(button: sender)
            coverImage.isHidden = false
            audioContainerView.isHidden = false
            inviteContainerView.isHidden = true
            searchView.isHidden = true
            tableView.isScrollEnabled = true
            tableData = .about
            tableView.reloadData()
        //MARK:- UNI BUTTON
        case 1:
            // tableViewHeight.constant = screenheight - 100
            friendPopTip.hide()
            tableView.isHidden = false
            highlightingButtons(button: sender)
            
            parentScrollView.scrollToTop()
            tableData = .uni
            coverImage.isHidden = true
            audioContainerView.isHidden = true
            searchView.isHidden = true
            inviteContainerView.isHidden = true
            tableView.tableHeaderView = headerView
            tableView.isScrollEnabled = true
            headerBtn.setTitle("Create Uni", for: .normal)
            
        //MARK:- POST BUTTON
        case 2:
            friendPopTip.hide()
            highlightingButtons(button: sender)
            tableData = .post
            coverImage.isHidden = true
            audioContainerView.isHidden = true
            searchView.isHidden = true
            inviteContainerView.isHidden = true
            audioContainerView.isHidden = true
            tableView.isScrollEnabled = true
            headerBtn.setTitle("Create Post", for: .normal)
            self.tableView.setContentOffset( CGPoint(x: 0, y: 0) , animated: true)
            let indexPath = NSIndexPath(item: 0, section: 0)
            tableView.scrollToRow(at: indexPath as IndexPath, at: UITableView.ScrollPosition.top, animated: true)
            self.tableView.setContentOffset( CGPoint(x: 0, y: 0) , animated: true)
            parentScrollView.isScrollEnabled = false
            headerView.isHidden = false
            tableView.tableHeaderView = headerView
            gettingAllPost(pageNo: 1, showLoader: true, isLoadMore: false)
            tableView.reloadData()
        //MARK:- CHAT BUTTON
        case 3:
            friendPopTip.hide()
            //highlightingButtons(button: sender)
            parentScrollView.isScrollEnabled = false
            tableData = .chats
            parentScrollView.scrollToTop()
            let vc = UIStoryboard.timeLineStoryboard.instantiateMessageVC()
            vc.callbackClosure = {[weak self] in
                self?.btnscollectionActon((self?.aboutBtn)!)
            }
            navigationController?.pushViewController(vc, animated: true)
        //MARK:- FRIENDS BUTTON
        case 4:
            searchState = false
            filteredTableData = names
            searchBtn.backgroundColor = .white
            searchBtn.tintColor = UIColor.init(hexString: Appcolors.buttons)
            if #available(iOS 13.0, *) {
                searchBar.searchTextField.text = nil
            } else {
                // Fallback on earlier versions
                let textFieldInsideSearchBar = searchBar.value(forKey: "searchField") as? UITextField
                
                textFieldInsideSearchBar?.text = nil
            }
            highlightingButtons(button: sender)
            parentScrollView.scrollToTop()
            tableData = .friends
            coverImage.isHidden = true
            audioContainerView.isHidden = true
            searchView.isHidden = true
            inviteContainerView.isHidden = false
            parentScrollView.isScrollEnabled = false
            tableView.isScrollEnabled = true
            tableView.tableHeaderView = UIView()
            tableView.reloadData()
            getFriendsList()  //MARK:- REQUESTING TO THE GET FRIENDS API
        default:
            break
        }
    }
    @IBAction func invitationBtnCollection(_ sender: UIButton) {
        switch sender.tag{
        case 0:
            //MARK:- SEARCH BTN
            if searchState == true {
                searchState = false
                searchBtn.backgroundColor = .white
                searchBtn.tintColor = UIColor.init(hexString: Appcolors.buttons)
                searchView.isHidden = true
                
            } else {
                searchState = true
                searchBtn.backgroundColor = UIColor.init(hexString: Appcolors.buttons)
                searchBtn.tintColor = .white
                searchView.isHidden = false
            }
        case 1:
            //MARK:- INVITE BUTTON
            let text = "http://test-graduation.captainau.com.au/"
            let textShare = [ text ]
            let activityViewController = UIActivityViewController(activityItems: textShare , applicationActivities: nil)
            activityViewController.popoverPresentationController?.sourceView = self.view
            self.present(activityViewController, animated: true, completion: nil)
        case 2:
            //MARK:- FRIENDS LIST CATEGORY
            inviteContainerView.clipsToBounds = false
            tableView.clipsToBounds = false
            //friendPopTip.offset = -132
            friendPopTip.padding = 0
            friendPopTip.arrowSize = CGSize(width: 0.0, height: 0.0)
            friendPopTip.constrainInContainerView = false
            let windw = UIApplication.shared.windows.first { $0.isKeyWindow }
            var frame = sender.convert(sender.bounds, to: windw)
            frame.origin.y += 140
            frame.origin.x += 77
            
            friendPopTip.show(customView: friendsCategoryView, direction: .left, in: windw!, from: frame)
            
            friendPopTip.shouldDismissOnTap  = false
            
        default:
            break
        }
    }
    func highlightingButtons(button: UIButton) {
        postBtn.backgroundColor = .white
        postBtn.setTitleColor(UIColor.init(hexString: Appcolors.buttons), for: .normal)
        aboutBtn.backgroundColor = .white
        aboutBtn.setTitleColor(UIColor.init(hexString: Appcolors.buttons), for: .normal)
        uniBtn.backgroundColor = .white
        uniBtn.setTitleColor(UIColor.init(hexString: Appcolors.buttons), for: .normal)
        friendsBtn.backgroundColor = .white
        friendsBtn.setTitleColor(UIColor.init(hexString: Appcolors.buttons), for: .normal)
        button.backgroundColor = UIColor.init(hexString: Appcolors.buttons)
        button.setTitleColor(.white, for: .normal)
    }
    func popingBottomVC(height: CGFloat, textFieldType: String, data: [String], id: Int) {
        print(id)
        let popupVC = UIStoryboard.timeLineStoryboard.instantiateSelectionBottomPopUpVC()
        popupVC.textFieldType = textFieldType
        popupVC.delegate = self
        //popupVC.selectedIndex = 3
        popupVC.height = height
        popupVC.topCornerRadius = 15
        popupVC.presentDuration = 0.25
        popupVC.dismissDuration = 0.25
        popupVC.shouldDismissInteractivelty = true
        popupVC.stringDataSource = data
        popupVC.userId = id
        // popupVC.popupDelegate = self
        self.present(popupVC, animated: true) {
            print("")
        }
        // present(popupVC, animated: true, completion: nil)
    }

//MARK:- RATING SELECTION
    
        func ratingSelection(id: Int, indexToReload: Int, selectionType: GraduateDetails,starredValue: String) {
            let vc = HelperFunctions.ratingSelection(id: id, starredVlaue: "", reloadingIndex: indexToReload)
            vc.delegate = self
            vc.selectionType = selectionType
            vc.id = id
            vc.selectedIndex = Int(starredValue)
            vc.indexToReload = indexToReload
            present(vc, animated: true, completion: nil)
        }
    
    func posActionCallBak() {
        highlightingButtons(button: postBtn)
        tableData = .post
        coverImage.isHidden = true
        audioContainerView.isHidden = true
        searchView.isHidden = true
        audioContainerView.isHidden = true
        parentScrollView.scrollToTop()
        parentScrollView.isScrollEnabled = false
        tableView.isScrollEnabled = true
        tableView.tableHeaderView = headerView
        self.view.layoutIfNeeded()
        tableView.isScrollEnabled = true
        headerBtn.setTitle("Create Post", for: .normal)
        tableView.reloadData()
    }
    //MARK:- POPUP
    func popUp(type: ErrorType, message: String) {
        let vc = UIStoryboard.timeLineStoryboard.instantiateErrorCarMsgPopVC()
        vc.popupType = type
        vc.messageString = message
        vc.delegaet = self
        let cardPopup = SBCardPopupViewController(contentViewController: vc)
        cardPopup.show(onViewController: self)
    }
    //MARK:- THOUGHTS DETAILS
    func thoughtsDetails(commentId: Int, index: IndexPath!) {
        print(index)
       let vc = UIStoryboard.timeLineStoryboard.instantiateThoughtsVC()
       vc.commentId = commentId
       vc.index = index
       vc.delegate = self
       navigationController?.pushViewController(vc, animated: true)
   }
    //MARK:- RATING POP UP
    func popingBottomVC(id: Int, indexToReload: Int, selectionType: GraduateDetails,starredValue: String) {
        let vc = HelperFunctions.ratingSelection(id: id, starredVlaue: "", reloadingIndex: indexToReload)
        vc.delegate = self
        vc.selectionType = selectionType
        vc.id = id
        vc.selectedIndex = Int(starredValue)
        vc.indexToReload = indexToReload
        present(vc, animated: true, completion: nil)
    }
    //MARK:- RATING DETAILS
    func ratingDetailsAction(ratingId: Int, starredVlaue: String, starCount: Int) {
           let vc = UIStoryboard.timeLineStoryboard.instantiateRatingDetailsVC()
           vc.ratingId = ratingId
       vc.starredValue = starredVlaue
       vc.starCount = starCount
           navigationController?.pushViewController(vc, animated: true)
          }
    
}

//MARK:- TABLEVIEW DATASOURCE
extension UserProfileViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == popupTableView {
            return texts.count
        } else if tableView == tableView {
            switch tableData{
            case .about:
                return 1
            case .uni:
                return uniDataSource?.data?.count ?? 0
            case .post:
                return postDataSource?.myPost?.data?.count ?? 0
            case .chats:
                return 0
            case .friends:
                if let friends = anyData as? FriendsResponseModel {
                    return friends.data?.count ?? 0
                }
                if let allFriendship = anyData as? FriendshipResponseModel {
                    return allFriendship.data?.count ?? 0
                }
                if let pending = anyData as? PendingFriendsModel {
                    return pending.data?.count ?? 0
                }
                if let blocked = anyData as? BlockedFriendsModel {
                    return blocked.data?.count ?? 0
                }
                if let request = anyData as? FriendRequestModel {
                    return request.data?.count ?? 0
                }
                if let delcined = anyData as? DeclinedRequestModel {
                    return delcined.data?.count ?? 0
                }
                
            case .pushedUni:
                return 5
            case .pushedPost:
                return postDataSource?.myPost?.data?.count ?? 0
            default:
                break
            }
        }
        return 1
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == popupTableView {
            let cell = UITableViewCell(style: .default, reuseIdentifier: nil)
            cell.textLabel?.text = self.texts[(indexPath as NSIndexPath).row]
            cell.textLabel?.textColor = UIColor.init(hexString: Appcolors.text)
            return cell
            
            
        } else if tableView == tableView {
            switch tableData {
            case .about:
                let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileInformationTableViewCell", for: indexPath) as! ProfileInformationTableViewCell
                cell.descTxtiew.textColor = UIColor.init(hexString: Appcolors.text)
                cell.descTxtiew.text = dataSource?.data?.profile?.summary
                cell.descTxtiew.textColor = UIColor.init(hexString: Appcolors.text)
                cell.mailTxtField.text = dataSource?.data?.email
                cell.codeTxtField.text = dataSource?.data?.profile?.phone
                cell.genderTxtField.text = dataSource?.data?.profile?.gender
                cell.professionTxtField.text = dataSource?.data?.profile?.profession
                cell.dateTxtField.text = dataSource?.data?.profile?.dob
                cell.addressTxtField.text = dataSource?.data?.profile?.addressString
                cell.statusTxtField.text = dataSource?.data?.profile?.maritalStatus
                cell.countryTxtField.text = dataSource?.data?.profile?.country
                cell.professionList = professionList
                cell.nickNameTxtField.text = dataSource?.data?.profile?.nickname
                cell.delegate = self
                
                return cell
            case .uni:
                let cell = tableView.dequeueReusableCell(withIdentifier: "GraduateTableViewCell", for: indexPath) as! GraduateTableViewCell
                cell.titleLabel.text = uniDataSource?.data?[indexPath.row].businessName
                cell.typeLabel.text = uniDataSource?.data?[indexPath.row].businessType
                if uniDataSource?.data?[indexPath.row].businessCoverImage == nil {
                    cell.mainImage.image = UIImage(named: "defaultPicture")
                } else {
                    cell.mainImage.setURLImage(imageURL: (GraduateUrl.imageUrl + (uniDataSource?.data?[indexPath.row].businessCoverImage ?? "")))
                }
                
                cell.index = indexPath.row
                cell.delegate = self
                cell.userid = uniDataSource?.data?[indexPath.row].internalIdentifier
                if uniDataSource?.data?[indexPath.row].isFollowed == true {
                    cell.followBtn.setTitle("Followed")
                    cell.followBtn.backgroundColor = UIColor.init(hexString: Appcolors.buttons)
                    cell.followBtn.setTitleColor(.white, for: .normal)
                } else if uniDataSource?.data?[indexPath.row].isOwnBusiness == true {
                    cell.followBtn.setTitle("Edit")
                    cell.followBtn.backgroundColor = .white
                    cell.followBtn.setTitleColor(UIColor.init(hexString: Appcolors.buttons), for: .normal)
                } else if uniDataSource?.data?[indexPath.row].isOwnBusiness == false {
                    cell.followBtn.setTitle("Follow")
                    cell.followBtn.backgroundColor = .white
                    cell.followBtn.setTitleColor(UIColor.init(hexString: Appcolors.buttons), for: .normal)
                }
                return cell
                
            case .post:
                let cell = tableView.dequeueReusableCell(withIdentifier: "PostCreationTableViewCell", for: indexPath) as! PostCreationTableViewCell
                cell.delegate = self
                
                //MARK:- POST CREATION CELL
                if postDataSource?.myPost?.data?[indexPath.row].portfolio != nil {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "PortFolioTableViewCell", for: indexPath) as! PortFolioTableViewCell
                
                    var portData = postDataSource?.myPost?.data?.filter({ $0.portfolio != nil })
                    cell.subHeaderlabel.text = postDataSource?.myPost?.data?[indexPath.row].portfolio?.addressString
                    
                    cell.delegate = self
                    cell.selectionType = .myPost
                    return cell.portFolioCell(cell: cell, indexPath: indexPath, dataSource: postDataSource?.myPost?.data)
                } else if postDataSource?.myPost?.data?[indexPath.row].offer != nil {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "DiscountTableViewCell", for: indexPath) as! DiscountTableViewCell
                    
                    cell.delegate = self
                    cell.selectionType = .offer
                    cell.postType = .myPost
                    return cell.DiscountTVC(cell: cell, indexPath: indexPath, dataSource: postDataSource?.myPost?.data)
                } else if postDataSource?.myPost?.data?[indexPath.row].event != nil {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "EventTableViewCell", for: indexPath) as! EventTableViewCell
                    let eventData = postDataSource?.myPost?.data?.filter {
                        $0.event != nil
                    }
                   // print(dataSource?.gradSpot?.data)
                    cell.delegate = self
                    cell.postType = .myPost
                    cell.selectionType = .gradSpot
                    cell.eventTypeLabel.text = postDataSource?.myPost?.data?[indexPath.row].event?.eventType
                    return cell.eventTVC(cell: cell, indexPath: indexPath, dataSource: postDataSource?.myPost?.data)
                }  else if postDataSource?.myPost?.data?[indexPath.row].hasLiveEnded == true {
                    //print(dataSource?.data?.data?[indexPath.row].hasLiveEnded)
                     //MARK:- MYPOST CELL WITH LIVE TYPE VIDEOS
                     print("")
                     let cell = tableView.dequeueReusableCell(withIdentifier: "LiveTableViewCell", for: indexPath) as! LiveTableViewCell
                      cell.videoType = .liveVideo
                    cell.name = postDataSource?.myPost?.data?[indexPath.row].postingAs?.name
                     
                     cell.delegate = self
                     cell.selectionType = .myPost
                      cell.postType = .live
                     let newDate = HelperFunctions.timeInterval(timeAgo: postDataSource?.myPost?.data?[indexPath.row].createdAt ?? "")
                                            print(newDate)
                     cell.timeStampLabel.text = newDate
                    if postDataSource?.myPost?.data?[indexPath.row].hasStarred == true {
                        cell.ratingBtn.setImage(UIImage(named: "star3"))
                    } else {
                        cell.ratingBtn.setImage(UIImage(named: "star"))
                    }
                     cell.locationLabel.text = postDataSource?.myPost?.data?[indexPath.row].business?.businessName
                     cell.viewersLabel.text = "\(postDataSource?.myPost?.data?[indexPath.row].viewsCount ?? 0)"
                    let filrurls = NSURL(string: (GraduateUrl.imageUrl + (postDataSource?.myPost?.data?[indexPath.row].liveVideoMp4 ?? "")))
                               let firstAsset = AVURLAsset(url: filrurls! as URL)
                     cell.videoPlayer.videoAssets = [firstAsset]
                     cell.postData = postDataSource?.myPost?.data?[indexPath.row]
                      cell.ratingNumLabel.text = "\(postDataSource?.myPost?.data?[indexPath.row].starsCount ?? 0)"
                      cell.thoughtsNumLabel.text = "\(postDataSource?.myPost?.data?[indexPath.row].comments?.count ?? 0)"
                      cell.commentId = postDataSource?.myPost?.data?[indexPath.row].internalIdentifier
                      cell.index = indexPath
                      cell.captionLabel.text = postDataSource?.myPost?.data?[indexPath.row].message
                     return cell
                     
                 } else if postDataSource?.myPost?.data?[indexPath.row].hasLiveEnded == false {
                     let cell = tableView.dequeueReusableCell(withIdentifier: "LiveStreamingTableViewCell", for: indexPath) as! LiveStreamingTableViewCell
                  cell.camera = Camera(name: postDataSource?.myPost?.data?[indexPath.row].message ?? "", isConnected: true, isLive: true, url: postDataSource?.myPost?.data?[indexPath.row].liveVideoRtmp ?? "")
                  cell.setupPlayer()
                 // cell.delegate = self
                  cell.selectionType = .live
                  cell.videoType = .liveVideo
                  cell.backgroundColor = .black
                  return cell.liveStreamingTVC(cell: cell, indexPath: indexPath, dataSource: postDataSource?.myPost?.data)
                
            
        
      
    }else if postDataSource?.myPost?.data?[indexPath.row].event == nil && postDataSource?.myPost?.data?[indexPath.row].offer == nil && postDataSource?.myPost?.data?[indexPath.row].portfolio == nil && postDataSource?.myPost?.data?[indexPath.row].business == nil {
     //MARK:- CREATE POST WITH GALLERIES VIDEOS CELL
                    print(postDataSource?.myPost?.data?[indexPath.row].galleries?.count)
                    
                    if postDataSource?.myPost?.data?[indexPath.row].galleries?.count != 0 {
                        
    //MARK:- POST CREATION GALLERTIES VIDEO POST
                        if let index = postDataSource?.myPost?.data?[indexPath.row].galleries?.firstIndex(where: { $0.mediaType == "Video"
                        }) {
                            let cell = tableView.dequeueReusableCell(withIdentifier: "LiveTableViewCell", for: indexPath) as! LiveTableViewCell
                            cell.index = indexPath
                            cell.postData = postDataSource?.myPost?.data?[indexPath.row]
                            cell.delegate = self
                            cell.videoType = .postVideo
                            cell.starCount = postDataSource?.myPost?.data?[indexPath.row].starsCount
                            print(postDataSource?.myPost?.data?[indexPath.row].starredValue)
                             cell.starreVlaue = postDataSource?.myPost?.data?[indexPath.row].starredValue
                            let newDate = HelperFunctions.timeInterval(timeAgo: postDataSource?.myPost?.data?[indexPath.row].createdAt ?? "")
                                                   print(newDate)
                            cell.timeStampLabel.text = newDate
                            cell.universityId = postDataSource?.myPost?.data?[indexPath.row].internalIdentifier
                           
                            if postDataSource?.myPost?.data?[indexPath.row].postingAsType == "business" {
                                  cell.name = postDataSource?.myPost?.data?[indexPath.row].postingAs?.businessName
                            } else if postDataSource?.myPost?.data?[indexPath.row].postingAsType == "user" {
                                 cell.name = postDataSource?.myPost?.data?[indexPath.row].postingAs?.name
                            }
                            if postDataSource?.myPost?.data?[indexPath.row].hasStarred == true {
                                  cell.ratingBtn.setImage(UIImage(named: "star3"))
                            } else {
                                 cell.ratingBtn.setImage(UIImage(named: "star"))
                            }
                          
                            cell.selectionType = .myPost
                            cell.locationLabel.text = postDataSource?.myPost?.data?[indexPath.row].business?.businessName
                            cell.viewersLabel.text = "\(postDataSource?.myPost?.data?[indexPath.row].viewsCount ?? 0)"
                            //cell.videoType = .postVideo
                            cell.captionLabel.text = postDataSource?.myPost?.data?[indexPath.row].message
                            
                            let filrurls = NSURL(string: (GraduateUrl.imageUrl + (postDataSource?.myPost?.data?[indexPath.row].galleries?[index].filepath ?? "")))
                            print(filrurls)
                            let firstAsset = AVURLAsset(url: filrurls! as URL)
                            cell.videoPlayer.videoAssets = [firstAsset]
                            cell.ratingNumLabel.text = "\(postDataSource?.myPost?.data?[indexPath.row].starsCount ?? 0)"
                            cell.thoughtsNumLabel.text = "\(postDataSource?.myPost?.data?[indexPath.row].comments?.count ?? 0)"
                            cell.proImage.setURLImage(imageURL: (GraduateUrl.imageUrl + (postDataSource?.myPost?.data?[indexPath.row].postingAs?.profile?.profileImage ?? "")))
                            cell.delegate = self
                            cell.postType = .myPost
                            return cell
                            
                        }
    //MARK: GALLERY IMAGE POST
                        if let _ = postDataSource?.myPost?.data?[indexPath.row].galleries?.firstIndex(where: { $0.mediaType == "Image"
                        }) {
                            let cell = tableView.dequeueReusableCell(withIdentifier: "PostCreationTableViewCell", for: indexPath) as! PostCreationTableViewCell
                            cell.delegate = self
                            cell.imageDataSource = postDataSource?.myPost?.data?[indexPath.row].galleries ?? []
                            cell.selectionType = .myPost
                            cell.postType = .myPost
                            return cell.postTVC(cell: cell, indexPath: indexPath, dataSource: postDataSource?.myPost?.data)
                        }
                    } else if postDataSource?.myPost?.data?[indexPath.row].liveVideoMp4 != nil {
                        //MARK:- MYPOST CELL WITH LIVE TYPE VIDEOS
                        print("")
                        let cell = tableView.dequeueReusableCell(withIdentifier: "LiveTableViewCell", for: indexPath) as! LiveTableViewCell
                        cell.delegate = self
                        cell.selectionType = .myPost
                        let newDate = HelperFunctions.timeInterval(timeAgo: postDataSource?.myPost?.data?[indexPath.row].createdAt ?? "")
                                               print(newDate)
                        cell.timeStampLabel.text = newDate
                        cell.name = postDataSource?.myPost?.data?[indexPath.row].postingAs?.name
                        cell.locationLabel.text = postDataSource?.myPost?.data?[indexPath.row].business?.businessName
                        cell.viewersLabel.text = "\(postDataSource?.myPost?.data?[indexPath.row].viewsCount ?? 0)"
                       let filrurls = NSURL(string: (GraduateUrl.imageUrl + (postDataSource?.myPost?.data?[indexPath.row].liveVideoMp4 ?? "")))
                                  let firstAsset = AVURLAsset(url: filrurls! as URL)
                        cell.videoPlayer.videoAssets = [firstAsset]
                        cell.videoType = .liveVideo
                        cell.postData = postDataSource?.myPost?.data?[indexPath.row]
                        cell.postType = .myPost
                        return cell
                    } else {
                        let cell = tableView.dequeueReusableCell(withIdentifier: "PostCreationTableViewCell", for: indexPath) as! PostCreationTableViewCell
                        cell.postType = .myPost
                        cell.delegate = self
                        cell.selectionType = .myPost
                        return cell.postTVC(cell: cell, indexPath: indexPath, dataSource: postDataSource?.myPost?.data)
                    }
                    
                }
                return cell
            case .chats:
                
                return UITableViewCell()
            case .friends:
                let cell = tableView.dequeueReusableCell(withIdentifier: "FriendListTableViewCell", for: indexPath) as! FriendListTableViewCell
                cell.delegate = self
                //MARK:- FRIENDS CELL
                if let friends = anyData as? FriendsResponseModel {
                    cell.nameLabel.text = friends.data?[indexPath.row].name
                    cell.mainImage.setURLImage(imageURL: (GraduateUrl.imageUrl + (friends.data?[indexPath.row].profile?.profileImage ?? "")))
                    cell.requestStacView.isHidden = true
                    cell.userid = friends.data?[indexPath.row].internalIdentifier
                    
                    return cell
                }
                //MARK:- FRIENDSHIP CELL
                if let allFriendship = anyData as? FriendshipResponseModel {
                    cell.nameLabel.text = allFriendship.data?[indexPath.row].friend?.name
                    //cell.mainImage.setURLImage(imageURL: )
                    cell.mainImage.setURLImage(imageURL: (GraduateUrl.imageUrl + (allFriendship.data?[indexPath.row].friend?.profile?.profileImage ?? "")), placeHolder: "defaultPicture")
                    cell.userid = allFriendship.data?[indexPath.row].friend?.internalIdentifier
                    cell.requestStacView.isHidden = true
                    return cell
                }
                //MARK:- PENDING CELL
                if let pending = anyData as? PendingFriendsModel {
                    cell.nameLabel.text = pending.data?[indexPath.row].friend?.name
                    
                    cell.mainImage.setURLImage(imageURL: (GraduateUrl.imageUrl + (pending.data?[indexPath.row].friend?.profile?.profileImage ?? "")), placeHolder: "defaultPicture")
                    cell.requestStacView.isHidden = true
                    cell.userid = pending.data?[indexPath.row].friend?.internalIdentifier
                    return cell
                }
                //MARK:- BLOCKED CELL
                if let blocked = anyData as? BlockedFriendsModel {
                    cell.nameLabel.text = blocked.data?[indexPath.row].friend?.name
                    
                    cell.mainImage.setURLImage(imageURL: (GraduateUrl.imageUrl + (blocked.data?[indexPath.row].friend?.profile?.profileImage ?? "")), placeHolder: "defaultPicture")
                    cell.requestStacView.isHidden = true
                    cell.userid = blocked.data?[indexPath.row].friend?.internalIdentifier
                    return cell
                }
                
                //MARK:- FRIEND REQUEST
                if let request = anyData as? FriendRequestModel {
                    cell.nameLabel.text = request.data?[indexPath.row].friend?.name
                    
                    cell.mainImage.setURLImage(imageURL: (GraduateUrl.imageUrl + (request.data?[indexPath.row].friend?.profile?.profileImage ?? "")), placeHolder: "defaultPicture")
                    cell.userid = request.data?[indexPath.row].friend?.internalIdentifier
                    cell.requestStacView.isHidden = true
                    return cell
                }
                
                //MARK:- DECLINED REQUEST
                
                if let delcined = anyData as? DeclinedRequestModel {
                    
                    cell.nameLabel.text = delcined.data?[indexPath.row].friend?.name
                    
                    cell.mainImage.setURLImage(imageURL: (GraduateUrl.imageUrl + (delcined.data?[indexPath.row].friend?.profile?.profileImage ?? "")), placeHolder: "defaultPicture")
                    cell.requestStacView.isHidden = true
                    cell.userid = delcined.data?[indexPath.row].friend?.internalIdentifier
                    return cell
                    
                }
                
            case .pushedPost:
                let cell = tableView.dequeueReusableCell(withIdentifier: "PostCreationTableViewCell", for: indexPath) as! PostCreationTableViewCell
                return cell
                
            default:
                break
            }
        }
        return UITableViewCell()
        
    }
    
    
}

//MARK:- TABLEVIEW DELEGATE
extension UserProfileViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == popupTableView {
            if texts[indexPath.row] ==  "Share"{
                let shareText = "http://test-graduation1.captainau.com.au/"
                
                let image = UIImage(named: "iconArrowDown")
                let vc = UIActivityViewController(activityItems: [shareText, image!], applicationActivities: [])
                present(vc, animated: true)
                self.popover.dismiss()
            } else if texts[indexPath.row] == "Edit" {
                let vc = UIStoryboard.timeLineStoryboard.instantiateCreateSpotViewController()
                vc.vcType = .editPost
                vc.delegate = self
                navigationController?.pushViewController(vc, animated: true)
                self.popover.dismiss()
            } else if texts[indexPath.row] == "Delete" {
                switch postTypes {
                case .portfolio:
                    popUp(type: .deletePortFolio, message: "")
                case .gradSpot:
                    popUp(type: .deleteEvent, message: "")
                case .myPost:
                popUp(type: .deletePost, message: "")
                case .offer:
                    popUp(type: .deleteOffer, message: "")
                default:
                break
                }
               
//                let vc = UIStoryboard.timeLineStoryboard.instantiateDeletePostPopUpVC()
//                let cardPopup = SBCardPopupViewController(contentViewController: vc)
//                cardPopup.show(onViewController: self)
//                self.popover.dismiss()
            }else {
                let vc = UIStoryboard.graduateStoryboard.instantiateReportVC()
                vc.view.isOpaque = false
                vc.view.backgroundColor = UIColor.black.withAlphaComponent(0.8)
                vc.modalPresentationStyle = .custom
                
                
                
                self.present(vc, animated: true, completion: nil)
                popover.dismiss()
            }
        } else {
            switch tableData {
            case .uni:
            let vc = UIStoryboard.graduateStoryboard.instantiateGraduateDetailsViewController()
            vc.businessId = uniDataSource?.data?[indexPath.row].internalIdentifier
            //vc.delegate = self
            navigationController?.pushViewController(vc, animated: true)
            default:
            break
            }
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == popupTableView {
            return 40
        } else {
            return  UITableView.automaticDimension
        }
        
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 800
    }
    //    func scrollViewDidScroll(_ scrollView: UIScrollView) {
    //        //MARK-: FOR NOT BOUNCING WHILE PULL TO REFRESH
    //
    //    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if tableView == popupTableView {
            
        } else {
            switch tableData {
            case .post:
                let allData = self.postDataSource
                print(allData?.loadMore ?? false)
                print(indexPath.row,"indexPath",indexPath.section)
                print(postDataSource?.myPost?.data?.count,"dataCount")
                guard allData?.loadMore == true, let dataCount = allData?.myPost?.data?.count, indexPath.row == dataCount - 1 else {return}
                print(dataCount)
                let lastSectionIndex = tableView.numberOfSections - 1
                print(lastSectionIndex)
                let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
                if indexPath.section == lastSectionIndex && indexPath.row == lastRowIndex {
                    // print("this is the last cell")
                    let spinner = UIActivityIndicatorView(style: .gray)
                    spinner.startAnimating()
                    spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
                    tableView.tableFooterView = spinner
                    tableView.tableFooterView?.isHidden = false
                }
                // print(allData.url)
                
                gettingAllPost(pageNo: postDataSource!.numOfPage, showLoader: false, isLoadMore: postDataSource!.loadMore)
            default:
                break
            }
        }
    }
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if tableView == popupTableView {
            
        } else {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "LiveTableViewCell", for: indexPath) as? LiveTableViewCell {
            if let players = cell.videoPlayer {
                if players.preferredRate != 0 {
                    players.videoPlayerControls.pause()
                }
            }
        }

    }
    }
}
extension UITableView {
    
    public func reloadData(_ completion: @escaping ()->()) {
        UIView.animate(withDuration: 0, animations: {
            self.reloadData()
        }, completion:{ _ in
            completion()
        })
    }
    
    func scroll(to: scrollsTo, animated: Bool) {
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(300)) {
            let numberOfSections = self.numberOfSections
            let numberOfRows = self.numberOfRows(inSection: numberOfSections-1)
            switch to{
            case .top:
                if numberOfRows > 0 {
                    let indexPath = IndexPath(row: 0, section: 0)
                    self.scrollToRow(at: indexPath, at: .top, animated: animated)
                }
                break
            case .bottom:
                if numberOfRows > 0 {
                    let indexPath = IndexPath(row: numberOfRows-1, section: (numberOfSections-1))
                    self.scrollToRow(at: indexPath, at: .bottom, animated: animated)
                }
                break
            }
        }
    }
    
    enum scrollsTo {
        case top,bottom
    }
}
extension UIScrollView {
    func scrollToTop() {
        let desiredOffset = CGPoint(x: 0, y: -contentInset.top)
        setContentOffset(desiredOffset, animated: true)
    }
}
enum TableDataType {
    case about
    case uni
    case post
    case chats
    case friends
    case pushedUni
    case pushedPost
}
extension UserProfileViewController: SendingMultipleSeclections {
    func postAsActionFromInside(value: Int?, bool: Bool, index: Int?, type: CreateSpotVCTypes, posterIds: [Int], postData: CreateEventModel) {
        
    }
    func gettingTheSelectedValueFromTheList(value: Int?, bool: Bool, index: Int?, type: CreateSpotVCTypes, posterIds: [Int]) {
        
    }
        
    func gettingSelectedVaueFromProfileMoreBtn(value: Int, bool: Bool, index: Int) {
        print(value)
        let cameraVc = CustomImagePicker()
        cameraVc.delegate = self
        if value == 0 {
            
            //MARK:- CHANGE PROFILE PICTURE FORM CAMERA
            cameraVc.delegate = self
            cameraVc.sourceType = UIImagePickerController.SourceType.camera
            self.present(cameraVc, animated: true, completion: nil)
            
        } else {
            //MARK:- CHANGE PROFILE PICTURE FROM GALLERY
            if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
                print("Button capture")
                
                
                cameraVc.sourceType = .savedPhotosAlbum
                cameraVc.allowsEditing = false
                
                self.present(cameraVc, animated: true) {
                    
                }
            }
        }
    }
    
    func sendingUserId(id: Int, index: Int) {
        
        if index == 0 {
            //MARK:- UNFRIEND FROM THE FRIENDLIST
            UserProfileFullDetails.requestTUnfriend(id: id) { [weak self](response) in
                if response?.success == false {
                    let vc = UIStoryboard.timeLineStoryboard.instantiateErrorCarMsgPopVC()
                    vc.popupType = .somethingWentWrong
                    vc.messageString = ""
                    let cardPopup = SBCardPopupViewController(contentViewController: vc)
                    cardPopup.show(onViewController: vc)
                }
            }
            
        } else if index == 1 {
            
            //MARK:- BLOCK FROM THE FRIENDLIST
            UserProfileFullDetails.requestToBlock(id: id) { [weak self](response) in
                if response?.success == false {
                    let vc = UIStoryboard.timeLineStoryboard.instantiateErrorCarMsgPopVC()
                    vc.popupType = .somethingWentWrong
                    vc.messageString = ""
                    let cardPopup = SBCardPopupViewController(contentViewController: vc)
                    cardPopup.show(onViewController: vc)
                }
            }
        } else if index == 2
            //MARK:- UNFOLLOW FROM THE FRIENDLIST
        {
            let url = GraduateUrl.baseURL + "user/" + "\(id)" + "/remove-follow"
            FollowersResponse.requestFollowUnfollowOthers(url: url) { (response) in
                
            }
        }
    }
    
    
    
    
    
    func sendingEvnetType(value: String, type: String) {
        if type == "professionTextField" {
            dataSource?.data?.profile?.profession = value
            tableView.reloadData()
        } else if type == "genderTextField" {
            dataSource?.data?.profile?.gender = value
            tableView.reloadData()
        } else if type == "statusTextField" {
            dataSource?.data?.profile?.maritalStatus = value
            tableView.reloadData()
        }
    }
    
    func sendingMediaType(value: Int) {
        //MARk-: Music selection from device
        if value == 0 {
            showDocumentPicker()
        } else {
            //MARK:- DELETE DEFAULT MUSIC
            UserProfileFullDetails.requestToDeleteUserDefaultMusic { (response) in
                let vc = UIStoryboard.timeLineStoryboard.instantiateErrorCarMsgPopVC()
                vc.popupType = .musicDeleted
                // vc.delegate = self
                let cardPopup = SBCardPopupViewController(contentViewController: vc)
                
                cardPopup.show(onViewController: self)
            }
        }
    }
    func gettingFullScreenCoverPic() {
        
    }
    func sendingMultipleVlaues(values: [String]) {
        print(values)
        
    }
    
    func sendingDiscountType(value: String) {
    }
    
    
    
    func sendingUniCategory(value: String) {
    }
    
    
    
    
}
extension UserProfileViewController:UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true)
        
        if let image = info[.editedImage] as? UIImage  {
            let vc = UIStoryboard.timeLineStoryboard.instantiateReusablePopUpVC()
            vc.modalPresentationStyle = .fullScreen
            vc.view.isOpaque = false
            vc.type = "Select Profile Image"
            vc.delegate = self
            vc.view.backgroundColor = UIColor.black.withAlphaComponent(0.8)
            vc.modalPresentationStyle = .custom
            coverImageStorage = image
            self.present(vc, animated: true, completion: nil)
        } else {
            if let galleryImage = info[.originalImage] as? UIImage  {
                let vc = UIStoryboard.timeLineStoryboard.instantiateReusablePopUpVC()
                // vc.modalPresentationStyle = .fullScreen
                vc.type = "Select Profile Image"
                vc.delegate = self
                coverImageStorage = galleryImage
                vc.modalPresentationStyle = .custom
                self.present(vc, animated: true, completion: nil)
            }
            
            // print out the image size as a test
            
        }
    }
    func showpopUp(type: ErrorType) {
        let vc = UIStoryboard.timeLineStoryboard.instantiateErrorCarMsgPopVC()
        vc.popupType = type
        let cardPopup = SBCardPopupViewController(contentViewController: vc)
        
        cardPopup.show(onViewController: self)
    }
}
extension UserProfileViewController:
SendingTxtFieldActionFromProfileInformation {
    
    //MARK:- UPDATING THE VALUES AS THE USER CHANGES IN THE EDIT FORM OF USER PROFILE
    func changingtheInfoValue(text: String, type: ProfileInfoListType) {
        switch type {
        case .address:
            dataSource?.data?.profile?.addressString = text
        case .name:
            dataSource?.data?.name = text
        case .nickname:
            dataSource?.data?.profile?.nickname = text
        case .phoneNum:
            dataSource?.data?.profile?.phone = text
        case .description:
            dataSource?.data?.profile?.summary = text
        default:
            break
        }
        tableView.reloadData()
    }
    
    func changingtheAddressValue(text: String) {
        dataSource?.data?.profile?.addressString = text
        tableView.reloadData()
    }
//MARK:- UPDATE USER PROFILE
    func requestingUpdateProfile(params: [String : Any]) {
        var param: [String : Any] = params
        let imgData = coverImage.image!.jpegData(compressionQuality: 0.2)!
        
        //Optional for extra parameter
        var header: [String : Any]
        if let token = UserDefaultsHandler.getUDValue(key: UDkey.token) as? String {
            header = ["Authorization": "Bearer \(token)", "Accept": "application/json"]
        } else {
            header = ["Accept": "application/json"]
            
        }
        
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            multipartFormData.append(imgData, withName: "profile_image",fileName: "file.jpg", mimeType: "image/jpg")
            ProgressHud.showProgressHUD()
            for (key, value) in param {
                if key == "default_music" {
                    multipartFormData.append((value as! Data), withName: "default_music", fileName: self.defaultMusicTitle ?? "", mimeType: "audio/mp3")
                } else {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
                }
            } //Optional for extra parameters
        }, to:"http://test-graduation1.captainau.com.au/api/user/update/profile", method: .post, headers: (header as! HTTPHeaders))
        { (result) in
            
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { [weak self](progress) in
                    guard let strongSelf = self else {return}
                    print("Upload Progress: \(progress.fractionCompleted)")
                })
                
                upload.responseJSON { response in
                    print(response.result.value)
                    ProgressHud.hideProgressHUD()

                    self.updated = true
                    self.showpopUp(type: .profileUpdated)
                }
                
            case .failure(let encodingError):
                print(encodingError)
            }
        }
    }
    func textFieldAction(textField: String) {
        if textField == "genderTextField" {
            self.popingBottomVC(height: 150, textFieldType: "genderTextField", data: [], id: 0)
        } else if textField == "professionTextField" {
            self.popingBottomVC(height: 150, textFieldType: "professionTextField", data: professionList, id: 0)
            
        } else if textField == "statusTextField" {
            self.popingBottomVC(height: 250, textFieldType: "statusTextField", data: [], id: 0)
        } else if textField == "dateTextField" {
            let calendar = YYCalendar(normalCalendarLangType: .ENG,
                                      date: "07/01/2019",
                                      format: "MM/dd/yyyy") { [weak self] date in
                                        // self?..text = date
            }
        } else if textField == "countryTxtField" {
            let acController = GMSAutocompleteViewController()
            acController.delegate = self
            present(acController, animated: true, completion: nil)
            
        } else if textField == "cancelButtonPressed" {
            dataSource = duplicateDataSouce
            tableView.reloadData()
        }
        
    }
}
extension UserProfileViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        filteredTableData = searchText.isEmpty ? names : names.filter({(dataString: String) -> Bool in
            // If dataItem matches the searchText, return true to include it
            return dataString.range(of: searchText, options: .caseInsensitive) != nil
        })
        
        tableView.reloadData()
    }
}

//MARK:- ACTION FROM FRIENDS CATEGORY POP UP
extension UserProfileViewController: ActionsFromFriednsCatView {
    func friendsAction() {
        requestCell = false
        friendPopTip.hide()
        tableView.reloadData()
    }
    
    //MARK:- REQUEST TO ALL FRIENDSHIP API
    func allAction() {
        friendPopTip.hide()
        UserProfileFullDetails.requestToFriendship { [weak self](response) in
            guard let strongSelf = self else { return }
            strongSelf.anyData = response
            
        }
    }
    //MARK:- REQUEST API
    func requestAction() {
        friendPopTip.hide()
        UserProfileFullDetails.requestToFriendRequest { [weak self](response) in
            guard let strongSelf = self else { return }
            strongSelf.anyData = response
        }
    }
    //MARK:- PENDING API
    func pendingAction() {
        friendPopTip.hide()
        UserProfileFullDetails.requestToPendingFriends { [weak self](response) in
            guard let strongSelf = self else { return }
            strongSelf.anyData = response
        }
    }
    //MARK:- DECLINED API
    func declinedAction() {
        friendPopTip.hide()
        UserProfileFullDetails.requestToDeclinedRequest { [weak self](response) in
            guard let strongSelf = self else { return }
            strongSelf.anyData = response
        }
    }
    //MARK:- BLOCKED API
    func blockedAction() {
        friendPopTip.hide()
        UserProfileFullDetails.requestToBlockedFriends { [weak self](response) in
            guard let strongSelf = self else { return }
            strongSelf.anyData = response
        }
    }
    
    
}
//MARK:- PCTVC DELEGATE
extension UserProfileViewController: SendingRatingFromPCTVC {
    
    func moreOptionFromPostTVC(sender: UIButton, id: Int, index: IndexPath, cell: PostCreationTableViewCell, isOwner: Bool, postType: GraduateDetails) {
        self.postTypes = postType
        self.editedIndex = index
        sharePopOver(sender: sender)
    }
    func ratingDetailFromPCTVC(ratingId: Int, starredValue: String, starCount: Int) {
        
    }
    func outsideCommentPostTVC(comment: Comments?, index: IndexPath) {
        postDataSource?.myPost?.data?[index.row].comments?.append(comment!)
        tableView.reloadRows(at: [index], with: .automatic)
    }
    func pushingTOProfileVC(id: Int, userType: String, isOwner: Bool, postAsType: String) {
        pushToProfile(id: id, userType: userType, isOwner: isOwner, postAsType: postAsType)
    }
  
    
    func PCTVCRating(id: Int, starValue: String, selectedindexToReload: Int, selectionType: GraduateDetails) {
      ratingSelection(id: id, indexToReload: selectedindexToReload, selectionType: selectionType, starredValue: starValue)
    
    }
    
    func feedbackActionPCTVC(commentId: Int, outsideComment: Bool, index: IndexPath) {
        if outsideComment == false {
            thoughtsDetails(commentId: commentId, index: index)
        } else {
            
        }
      
    }
    func pushingTOProfileVC(id: Int) {
        
    }
    func thoughtsAction() {
         
    }
   
    func PCTVCRating() {
        
    }
  
    func fullScreenImage(vc: UIViewController) {
        currentTabBar?.setBar(hidden: true, animated: true)
        present(vc, animated: true, completion: nil)
    }
    //MARK:- PUSH TO PROFILE FUNC
    func pushToProfile(id: Int, userType: String, isOwner: Bool, postAsType: String) {
        if postAsType == "user" {
                    if isOwner == true {
                                 let vc = UIStoryboard.timeLineStoryboard.instantiateUserProfileVC()
                                 navigationController?.pushViewController(vc, animated: true)
                                 } else {
                                     let vc = UIStoryboard.timeLineStoryboard.instantiateOtherUserProfileVC()
                               vc.userId = id
                                            navigationController?.pushViewController(vc, animated: true)
                                 }
                    } else {
                        let vc = UIStoryboard.timeLineStoryboard.instantiateGraduateDetailsViewController()
                        vc.businessId = id
                        navigationController?.pushViewController(vc, animated: true)
                    }
    }
    
    
  
    
    
}
//MARK:- ACTION FROM FRIENDLIST CELL
extension UserProfileViewController: ActionFromFLTVC {
    func bottomPopUp(userId: Int?) {
        popingBottomVC(height: 150, textFieldType: "FriendsCategoryAction", data: [], id: userId ?? 0)
    }
    
    
}
class IntrinsicTableView: UITableView {
    
    override var contentSize:CGSize {
        didSet {
            self.invalidateIntrinsicContentSize()
        }
    }
    
    override var intrinsicContentSize: CGSize {
        self.layoutIfNeeded()
        return CGSize(width: UIView.noIntrinsicMetric, height: contentSize.height)
    }
    
}
extension UserProfileViewController: SendingActionFromReusablepopUp {
    func sendingVlaue(value: Bool) {
        if !value {
            
        } else {
            coverImage.image = coverImageStorage
        }
    }
}
extension UserProfileViewController: GMSAutocompleteViewControllerDelegate {
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        // Get the place name from 'GMSAutocompleteViewController'
        // Then display the name in textField
        // addressTxtField.text = place.name
        dataSource?.data?.profile?.country = place.addressComponents?.first(where: { $0.name == "country" })?.name
        if let addressComponents = place.addressComponents,let countryComponent = (addressComponents.filter { $0.type == "country" }.first) {
            let countryName = countryComponent.name
            dataSource?.data?.profile?.country = countryName
            
            print("Country::::::::\(countryName)")
            
        }
        // print(place.addressComponents?.first(where: { $0.type == "country" })?.first)
        tableView.reloadData()
        
        
        // Dismiss the GMSAutocompleteViewController when something is selected
        dismiss(animated: true, completion: nil)
    }
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // Handle the error
        print("Error: ", error.localizedDescription)
    }
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        // Dismiss when the user canceled the action
        dismiss(animated: true, completion: nil)
    }
}
//MARK: RECEIVING RATING FROM THE PFTVC
extension UserProfileViewController: SendingRatingFromPFTVC{
    func pftvcthoughtsDetails(commentId: Int, outsideComment: Bool, index: IndexPath) {
        if outsideComment == false {
        thoughtsDetails(commentId: commentId, index: index)
        } else {
            
        }
    }
    func moreOPtionFromPortTVC(sender: UIButton, id: Int, index: IndexPath, cell: PortFolioTableViewCell, isOwner: Bool, postType: GraduateDetails) {
        postTypes = postType
        editedIndex = index
        if isOwner == true {
            texts.removeAll()
            texts = ["Share", "Edit","Delete"]
            sharePopOver(sender: sender)
        } else {
            texts.removeAll()
            texts = ["Share", "Report"]
            sharePopOver(sender: sender)
        }
    }
    func outsideCommentPortFolioTVC(comment: Comments?, index: IndexPath) {
        
    }
    
    func pushingTOProfileVCFromPFTVC(id: Int, userType: String, isOwner: Bool, postAsType: String) {
        pushToProfile(id: id, userType: userType, isOwner: isOwner, postAsType: postAsType)
    }
    
//    func moreOPtionFromPortTVC(sender: UIButton, id: Int, index: IndexPath, cell: PortFolioTableViewCell, isOwner: Bool) {
//        if isOwner == true {
//            sharePopOver(sender: sender)
//        } else {
//            texts.removeAll()
//            texts = ["Share", "Report"]
//            sharePopOver(sender: sender)
//        }
//    }
    func pftvcRating(id: Int, starValue: String, selectedindexToReload: Int, selectionType: GraduateDetails) {
        popingBottomVC(id: id, indexToReload: selectedindexToReload, selectionType: .portfolio, starredValue: starValue)
    }
    func pftvcratingDetails(ratingId: Int, starredValue: String, starCount: Int) {
        let vc = UIStoryboard.timeLineStoryboard.instantiateRatingDetailsVC()
        vc.ratingId = ratingId
        vc.starredValue = starredValue
        vc.starCount = starCount
        navigationController?.pushViewController(vc, animated: true)
    }
   
    func sendingThoughts(id: Int, param: [String : Any]) {
        
    }
    func pftvcthoughtsDetails(commentId: Int) {
        thoughtsDetails(commentId: commentId, index: [])
    }
   
    
    func fullScreen(vc: UIViewController) {
        currentTabBar?.setBar(hidden: true, animated: false)
        present(vc, animated: true, completion: nil)
    }
    func feedbackActionPFTVC() {
        thoughtsDetails(commentId: 0, index: [])
    }
    
    func moreOPtionFromPortTVC(sender: UIButton, index: IndexPath, cell: PortFolioTableViewCell) {
        
    }
}
//MARK:- DELEGATE FROM EVENT
extension UserProfileViewController: SendingRatingFromEventTVC {
    func moreOPtionFromEventTVC(sender: UIButton, id: Int, index: IndexPath, cell: EventTableViewCell, isOwner: Bool, postType: GraduateDetails) {
        postTypes = postType
        editedIndex = index
        if isOwner == true {
            texts.removeAll()
            texts = ["Share", "Edit","Delete"]
            sharePopOver(sender: sender)
        } else {
            texts.removeAll()
            texts = ["Share", "Report"]
            sharePopOver(sender: sender)
        }
    }
    
    func outsideCommentEventTVC(comment: Comments?, index: IndexPath) {
        postDataSource?.myPost?.data?[index.row].comments?.append(comment!)
        tableView.reloadRows(at: [index], with: .automatic)
    }
    
    func pushingToProfileFromSpot(id: Int, userType: String, isOwner: Bool, postAsType: String) {
        pushToProfile(id: id, userType: userType, isOwner: isOwner, postAsType: postAsType)
       
    }
    func eventFeedbackAction(commentId: Int, index: IndexPath) {
         thoughtsDetails(commentId: commentId, index: index)
    }
   
    func sendingRatingEventTVC(id: Int, starValue: String, selectedindexToReload: Int, selectionType: GraduateDetails) {
        print(id)
        popingBottomVC(id: id, indexToReload: selectedindexToReload, selectionType: selectionType, starredValue: starValue)
    }
    
    func attendIgnoreEventTVC(universityId: Int, attendState: Bool, index: IndexPath!, attendeesCount: Int) {
        let url: String?
        print(universityId)
        let id = postDataSource?.myPost?.data?[index.row].event?.internalIdentifier
        var attendeeCount = attendeesCount
        if attendState == true {
            url = (GraduateUrl.attendEventURL + "\(id ?? 0)")
            
            //attendeeCount = attendeesCount + 1
            
        } else {
            url = (GraduateUrl.unAttendEventURL + "\(id ?? 0)")
            //attendeeCount = attendeesCount - 1
        }
        
        GraduateDetailsResponse.requestToAttendIgnoreEvents(url: url ?? "") { [weak self](response) in
            guard let strongSelf = self else { return }
           print(response)
            
        }
    }
  
    func eventRatingDetails(ratingId: Int, starredVlaue: String, starCount: Int) {
       ratingDetailsAction(ratingId: ratingId, starredVlaue: starredVlaue, starCount: starCount)
    }
 
    func fullScreenAction(vc: UIViewController) {
        currentTabBar?.setBar(hidden: true, animated: true)
        present(vc, animated: true, completion: nil)
    }
   
    
    func feedbackActionEventTVC() {
        thoughtsDetails(commentId: 0, index: [])
    }
    func sendingRatingEventTVC() {
        
    }
}
//MARK: RECEIVING RATING FROM LIVE TVC
extension UserProfileViewController: SendingRatingFromLiveTVC {
    func moreOptionsFromLiveTVC(sender: UIButton, index: IndexPath, isOwner: Bool, postType: GraduateDetails) {
        postTypes = postType
        editedIndex = index
        if isOwner == true {
            texts.removeAll()
            texts = ["Share", "Edit","Delete"]
            sharePopOver(sender: sender)
        } else {
            texts.removeAll()
            texts = ["Share", "Report"]
            sharePopOver(sender: sender)
        }
    }
    
    
    func commentOutsideLiveTVC(comment: Comments?, index: IndexPath) {
        postDataSource?.myPost?.data?[index.row].comments?.append(comment!)
        tableView.reloadRows(at: [index], with: .automatic)
    }
    func pushingTOProfileVC(id: Int, userType: String, isOwner: Bool, postasType: String) {
        print(id,userType,isOwner)
        if postasType == "user" {
        if isOwner == true {
                     let vc = UIStoryboard.timeLineStoryboard.instantiateUserProfileVC()
                     navigationController?.pushViewController(vc, animated: true)
                     } else {
                         let vc = UIStoryboard.timeLineStoryboard.instantiateOtherUserProfileVC()
                   vc.userId = id
                                navigationController?.pushViewController(vc, animated: true)
                     }
        } else {
            let vc = UIStoryboard.timeLineStoryboard.instantiateGraduateDetailsViewController()
            vc.businessId = id
            navigationController?.pushViewController(vc, animated: true)
        }
    }
    
   
    
    func feedbackActionLiveTVC(commentId: Int, index: IndexPath!) {
       thoughtsDetails(commentId: commentId, index: index)
                  
    }
    func sendingRatings(id: Int, starValue: String, selectedindexToReload: Int, selectionType: GraduateDetails) {
        popingBottomVC(id: id, indexToReload: selectedindexToReload, selectionType: selectionType, starredValue: starValue)
    }
    
    
    func liveRatingDetails(ratingId: Int, starredVlaue: String, starCount: Int) {
      ratingDetailsAction(ratingId: ratingId, starredVlaue: starredVlaue, starCount: starCount)
    }
    
 
    
    func sendingVideoUrlForFullScreen() {
        
    }
    func sendingRatings() {
        popingBottomVC(id: 0, indexToReload: 0, selectionType: .live, starredValue: "")
    }
}
//MARK: REVCEIVING RATING FROM THE DISCOUNTTVC
extension UserProfileViewController: SedingRatingsToTimeLineVC {
    func moreOptionFromDiscountTVC(sender: UIButton, id: Int, isOwnBusiness: Bool, postType: GraduateDetails, index: IndexPath) {
        self.postTypes = postType
        self.editedIndex = index
        if isOwnBusiness == true {
            sharePopOver(sender: sender)
        } else {
            texts.removeAll()
            texts = ["Share", "Report"]
            sharePopOver(sender: sender)
        }
    }

    func commentOutsideDiscountTVC(comment: Comments?, index: IndexPath) {
        postDataSource?.myPost?.data?[index.row].comments?.append(comment!)
        tableView.reloadRows(at: [index], with: .automatic)
    }
    func pushingTOProfileVCFromDisc(id: Int, userType: String, isOwner: Bool, postAsType: String) {
        pushToProfile(id: id, userType: userType, isOwner: isOwner, postAsType: postAsType)
    }
    func feedbackAction(commentId: Int, outsideComment: Bool, index: IndexPath) {
        if outsideComment == false {
                  thoughtsDetails(commentId: commentId, index: index)
                  
              } else {
                  
              }
    }
    

    func sendingRating(id: Int, starValue: String, selectedindexToReload: Int, selectionType: GraduateDetails) {
        popingBottomVC(id: id, indexToReload: selectedindexToReload, selectionType: selectionType, starredValue: starValue)
    }
    
    
    
  
    
    func discratingDetails(ratingId: Int, starredValue: String, starCount: Int) {
       ratingDetailsAction(ratingId: ratingId, starredVlaue: starredValue, starCount: starCount)
    }
    
    
    func fullscreen(vc: UIViewController) {
        currentTabBar?.setBar(hidden: true, animated: true)
        present(vc, animated: true, completion: nil)
    }
   
}
//MARK:- THOUGHTS DELEGATE
extension UserProfileViewController: SendingDataFromThoughtsVC {
    
    func sendingData(index: IndexPath, thoughtsCount: Int, commentsData: [Comments]) {
        print(index,commentsData)
        postDataSource?.myPost?.data?[index.row].comments = commentsData
        tableView.reloadRows(at: [index], with: .automatic)
    }
    
    
}
//MARK:- DELEGATE RATING ACTION
extension UserProfileViewController: SendingActionFromRatingVC {
    func sendingActionWithType(type: RatingActionTypes, id: Int, reloadingIndex: Int, ratingNum: Int, selectionType: GraduateDetails) {
        switch type{
        case .requestforRating:
           ratingAfterApiCallOfEvent(reloadingIndex: reloadingIndex, ratingNum: ratingNum)
            reqestForPostRating(id: id, reloadingIndex: reloadingIndex, ratingNum: ratingNum, selectionType: selectionType)
        case .reuestForUnratingPost:
            unratingAfterApiCallOfEvent(reloadingIndex: reloadingIndex, ratingNum: ratingNum)
            reqestForPostUnRating(id: id, reloadingIndex: reloadingIndex, ratingNum: ratingNum, selectionType: selectionType)
        default:
            break
        }
    }
//MARK:- RATE POST
          func reqestForPostRating(id: Int,reloadingIndex: Int,ratingNum: Int,selectionType: GraduateDetails) {
              var headers: [String : Any]?
              if let token = UserDefaultsHandler.getUDValue(key: UDkey.token) as? String {
                  if headers == nil {
                      headers = ["Authorization": "Bearer \(token)", "Accept": "application/json"]
                  } else {
                      headers!["Authorization"] = "Bearer \(token)"
                      headers!["Accept"] = "application/json"
                  }
              }
              let url = (GraduateUrl.ratePostURL + "\(id)")
               print(url,ratingNum,selectionType)
              Alamofire.request(url,
                                method: .post,
                                parameters: ["star_value": "\(ratingNum + 1)"],headers: (headers as! HTTPHeaders))
                  .validate()
                  .responseJSON { [weak self]response in
                      guard let strongSelf = self else { return }
                      if let value = response.result.value as? [String : Any] {
                       
//                        switch selectionType {
//                        case .gradSpot:
//                             strongSelf.ratingAfterApiCallOfEvent(reloadingIndex: reloadingIndex, ratingNum: ratingNum)
//                        case .offer:
//                            ratingAfterApiCallOFOffer(reloadingIndex: reloadingIndex, ratingNum: ratingNum)
//                        default:
//                            break
//                        }
                      
                          
                      }
                      
              }
          }
       
//MARK:- REQUEST FOR UNRATING POST
       func reqestForPostUnRating(id: Int,reloadingIndex: Int,ratingNum: Int,selectionType: GraduateDetails) {
           var headers: [String : Any]?
           if let token = UserDefaultsHandler.getUDValue(key: UDkey.token) as? String {
               if headers == nil {
                   headers = ["Authorization": "Bearer \(token)", "Accept": "application/json"]
               } else {
                   headers!["Authorization"] = "Bearer \(token)"
                   headers!["Accept"] = "application/json"
               }
           }
           let url = (GraduateUrl.unratePostURL + "\(id)")
           print(url,ratingNum,selectionType)
           Alamofire.request(url,method: .delete,headers: (headers as! HTTPHeaders))
               .validate()
               .responseJSON { [weak self]response in
                   guard let strongSelf = self else { return }
                   if let value = response.result.value as? [String : Any] {
                   
//                    switch selectionType {
//                    case .gradSpot:
//                         strongSelf.unratingAfterApiCallOfEvent(reloadingIndex: reloadingIndex, ratingNum: ratingNum)
//                    case .offer:
//                        strongSelf.unratingAfterApiCallOfOffer(reloadingIndex: reloadingIndex, ratingNum: ratingNum)
//                    default:
//                        break
//                    }
                      
                       print(value)
                       print(reloadingIndex)
                       
                   }
                   
           }
       }
       func ratingAfterApiCallOfEvent(reloadingIndex: Int, ratingNum: Int) {
           print(postDataSource?.myPost?.data?[reloadingIndex].hasStarred)
           if postDataSource?.myPost?.data?[reloadingIndex].hasStarred == true {
              
           } else {
               
               postDataSource?.myPost?.data?[reloadingIndex].starsCount! += 1
           }
        postDataSource?.myPost?.data?[reloadingIndex].starredValue = "\((ratingNum + 1))"
           print(postDataSource?.myPost?.data?[reloadingIndex].starredValue)
           print(reloadingIndex)
        postDataSource?.myPost?.data?[reloadingIndex].hasStarred = true
           tableView.reloadData()
       }
       func unratingAfterApiCallOfEvent(reloadingIndex: Int, ratingNum: Int) {
        postDataSource?.myPost?.data?[reloadingIndex].hasStarred = false
           if  postDataSource?.myPost?.data?[reloadingIndex].starsCount ?? 0 > 0 {
            postDataSource?.myPost?.data?[reloadingIndex].starsCount! -= 1
           } else {
               
           }
        postDataSource?.myPost?.data?[reloadingIndex].starredValue = "\(0)"
           tableView.reloadData()
       }
//    func ratingAfterApiCallOFOffer(reloadingIndex: Int, ratingNum: Int) {
//        if dataSource?.data?.data?[reloadingIndex].hasStarred == true {
//            return
//        } else {
//
//            dataSource?.data?.data?[reloadingIndex].starsCount! += 1
//        }
//        dataSource?.Offer?.data?[reloadingIndex].starredValue = "\((ratingNum + 1))"
//        print(reloadingIndex)
//        dataSource?.Offer?.data?[reloadingIndex].hasStarred = true
//        tableView.reloadData()
//    }
//    func unratingAfterApiCallOfOffer(reloadingIndex: Int, ratingNum: Int) {
//        dataSource?.Offer?.data?[reloadingIndex].hasStarred = false
//        if  dataSource?.Offer?.data?[reloadingIndex].starsCount ?? 0 > 0 {
//            dataSource?.Offer?.data?[reloadingIndex].starsCount! -= 1
//        } else {
//
//        }
//        dataSource?.Offer?.data?[reloadingIndex].starredValue = "\(0)"
//        tableView.reloadData()
//    }
}
extension UserProfileViewController: FollowBtnActionFrmGTBC {
    func indexPath(index: Int, btnState: String, userId: Int) {
        if btnState == "Follow" {
                   followUniversity(id: userId)
               } else if btnState == "Followed" {
                   unfollowUniversity(id: userId)
               } else if btnState == "Edit" {
                   let vc = UIStoryboard.graduateStoryboard.instantiateGraduateDetailsViewController()
                          navigationController?.pushViewController(vc, animated: true)
               }
    }
    
    func followAndEditAction() {
        let vc = UIStoryboard.graduateStoryboard.instantiateGraduateDetailsViewController()
        navigationController?.pushViewController(vc, animated: true)
    }
    func indexPath(index: Int, btnState: String) {
       
    }
}
//MARK:- CREATE SPOT DELEGATE
extension UserProfileViewController: ActionFromCreateSpotVC {
    func reloadTableViewData() {
        
    }
    
    func reloadWithType(type: ErrorType) {
        switch type {
        case .postEdited:
            self.tableView.reloadData()
        default:
        break
        }
    }
    
    
}
extension UserProfileViewController: SendingActionFromErrorChat {
    func sendingAction() {
        
    }
    
    func sendingCancelAction() {
        
    }
    
    func adctionwithType(type: ErrorType) {
        switch type {
        case .deletePortFolio:
            requestToDeletePortfolio()
        case .deleteOffer:
            requestToDeleteOffer()
        case .deleteEvent:
            requestToDeleteEvent()
        case .deletePost:
            requestToDeletePost()
        case .uploadDefaultMusic:
            print(soundFileURL)
            guard let datas = try? Data(contentsOf:soundFileURL) else { return }
            print(datas)
            let param: [String : Any] = ["default_music" : datas]
            print(param)
           // let param: [String : Any] = ["default_music" : ""]
            requestingUpdateProfile(params: param)
        case .profileUpdated:
            getFullUserDetails()
        default:
            break
        }
    }
    //MARK:- REQUEST TO DELTE POST
        func requestToDeletePost() {
            WatchResponse.requestToDeletePost(id: uniDataSource?.data?[editedIndex.row].internalIdentifier ?? 0,showHud: true) { [weak self](response) in
                guard let strongSelf = self else { return }
               
            }
        }
    //MARK:- REQUEST TO DELTE PORTFOLIO
    func requestToDeletePortfolio() {
        let url = GraduateUrl.baseURL + "graduate_university/delete/portfolio/"
        GraduateDetailsResponse.requestToDeletePortfolio(id: postDataSource?.myPost?.data?[editedIndex.row].portfolio?.internalIdentifier ?? 0, url: url) { [weak self](response) in
            guard let strongSelf = self else { return }
            strongSelf.gettingAllPost(pageNo: 1, showLoader: true, isLoadMore: false)
        }
    }
//MARK:- REQUEST TO DELETE EVENT
    func requestToDeleteEvent() {
        let url = GraduateUrl.baseURL + "graduate_spot/"
        GraduateDetailsResponse.requestToDeleteEvent(id: postDataSource?.myPost?.data?[editedIndex.row].event?.internalIdentifier ?? 0, url: url) { [weak self](response) in
            guard let strongSelf = self else { return }
            strongSelf.gettingAllPost(pageNo: 1, showLoader: true, isLoadMore: false)
        }
    }
//MARK:- REQUEST TO DELETE OFFER
    func requestToDeleteOffer() {
        GraduateDetailsResponse.requestToDeleteOffer(id: postDataSource?.myPost?.data?[editedIndex.row].offer?.internalIdentifier ?? 0, url: GraduateUrl.deleteOfferURL) { [weak self](response) in
            guard let strongSelf = self else { return }
            strongSelf.gettingAllPost(pageNo: 1, showLoader: true, isLoadMore: false)
        }
    }
}
//MARK:- DOCUMENT PICKER
extension UserProfileViewController: UIDocumentPickerDelegate {
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        guard let myURL = urls.first else {
               return
           }
        defaultMusicTitle = myURL.lastPathComponent
        soundFileURL = myURL
        print(soundFileURL)
        popUp(type: .uploadDefaultMusic, message: "")
    }
}
