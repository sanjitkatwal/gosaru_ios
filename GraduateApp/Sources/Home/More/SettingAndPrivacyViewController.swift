//
//  SettingAndPrivacyViewController.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 7/2/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import UIKit
import AMPopTip

class SettingAndPrivacyViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var createPoptip = PopTip()
     var createPopOpen: Bool = false
    var timeLinCustomView: TimeLineCreate?
    var dataSource: [String]? = [] {
        didSet {
            tableView.reloadData()
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Settings"
        // Do any additional setup after loading the view.
         addData()
        addingBarButtons()
//         tableView.register(UINib(nibName: "ImageAndLabelTableViewCell", bundle: nil), forCellReuseIdentifier: "ImageAndLabelTableViewCell")
        tableView.dataSource = self
        tableView.delegate = self
       
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: true)
        currentTabBar?.setBar(hidden: true, animated: true)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
         createPopOpen = false
         navigationController?.setNavigationBarHidden(false, animated: true)
        currentTabBar?.setBar(hidden: false, animated: true)
    }
    func addData() {
        dataSource = ["Account Settings", "Password", "Privacy Policy", "FAQ", "Terms & Conditions", "Terms Of Use", "Contact Us", "Feedback", "Advertise With Us", "About Us"]
    }
    func addingBarButtons() {
        let searcbBtn = UIButton()
        searcbBtn.setImage(UIImage(named: "searchVector"), for: .normal)
         searcbBtn.adjustsImageWhenHighlighted = false
                searcbBtn.frame = CGRect(x: 0, y: 0, width: 70, height: 70)
               searcbBtn.addTarget(self, action: #selector(didTapSearchButton), for: .touchUpInside)
       
                let addBtn = UIButton()
                        //addBtn.frame = CGRect(x: 0, y: 0, width: 70, height: 70)
                      addBtn.setImage(UIImage(named: "iconAddVector"), for: .normal)
        addBtn.isHighlighted = false
        addBtn.adjustsImageWhenHighlighted = false
        addBtn.backgroundColor = UIColor.init(hexString: Appcolors.background)
        addBtn.tintColor = UIColor.init(hexString: Appcolors.background)
                      addBtn.addTarget(self, action: #selector(didTapaddBtn), for: .touchUpInside)
               let button = [ searcbBtn, addBtn]
               
        //       if self is HomeViewController {
        //         button.remove(at: 0)
        //       }
        //       if Utilities.isGuestLogin() {
        //         button = [cartBtn]
        //       }
               let stackview = UIStackView.init(arrangedSubviews: button)
               stackview.distribution = .equalSpacing
               stackview.axis = .horizontal
               //stackview.alignment = .center
               stackview.spacing = 16
                let barBtn = UIBarButtonItem(customView: stackview)
               barBtn.customView?.translatesAutoresizingMaskIntoConstraints = false
        //        barBtn.customView?.hie
               //barBtn.customView?.widthAnchor.constraint(equalToConstant: size.width).isActive = true
                
               self.navigationItem.rightBarButtonItem = barBtn
    }
    @objc func didTapaddBtn(sender: UIButton) {
    //MARK: POPTIP
        timeLinCustomView = Bundle.main.loadNibNamed(String(describing: TimeLineCreate.self), owner: self, options: nil)?.first as? TimeLineCreate
               timeLinCustomView?.delagate = self
               timeLinCustomView?.frame.size = CGSize(width: 220, height: 250)
    createPoptip.offset = -37
    createPoptip.clipsToBounds = true
    createPoptip.constrainInContainerView = false
    createPoptip.shouldDismissOnTap = true
           createPoptip.tapOutsideHandler = { _ in
             print("tap outside")
            self.createPopOpen = false
           }
    createPoptip.shouldDismissOnTapOutside = true
    if createPopOpen == false {
     


       // createPoptip.clipsToBounds = false
    createPoptip.cornerRadius = 20
        createPoptip.padding = 0
    createPoptip.shouldDismissOnTap = true
    createPoptip.padding = CGFloat(0.0)
    createPoptip.borderWidth = 0.2
      createPoptip.shouldDismissOnTap = false
    createPoptip.arrowSize = CGSize(width: 0, height: 0)
        createPoptip.constrainInContainerView = false
        let windw = UIApplication.shared.windows.first { $0.isKeyWindow }
        var frame = sender.convert(sender.bounds, to: windw)
        frame.origin.y += 130
       
        createPoptip.show(customView: timeLinCustomView!, direction: .left, in: windw!, from: frame)
        createPopOpen = true
    } else {
        createPoptip.hide()
        createPopOpen = false
    }
    
}
    @objc func didTapSearchButton() {
       let vc = UIStoryboard.timeLineStoryboard.instantiateSearchVC()
        navigationController?.pushViewController(vc, animated: true)
    }
}
extension SettingAndPrivacyViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SettingTableViewCell", for: indexPath) as! SettingTableViewCell
       
        cell.titleLabel.text = dataSource?[indexPath.row]
//        cell.rightImage.image = UIImage(named: "iconNext")
//        cell.rightImage.contentMode = .center
        cell.rightImage.tintColor = UIColor.init(hexString: Appcolors.buttons)
        return cell
    }
    
    
}
extension SettingAndPrivacyViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if dataSource?[indexPath.row] == SettingsList.AccountSetting.rawValue {
            let vc = UIStoryboard.moreStoryboard.instantiateaccountSettingViewController()
            navigationController?.pushViewController(vc, animated: true)
             tableView.deselectRow(at: indexPath, animated: false)
        } else if dataSource?[indexPath.row] == SettingsList.AccountSetting.rawValue  {
            
        } else if dataSource?[indexPath.row] == SettingsList.Password.rawValue  {
            let vc = UIStoryboard.moreStoryboard.instantiateaccountPasswordSettingViewController()
            navigationController?.pushViewController(vc, animated: true)
             tableView.deselectRow(at: indexPath, animated: false)
        } else if dataSource?[indexPath.row] == SettingsList.PrivacyPolicy.rawValue  {
            let vc = UIStoryboard.moreStoryboard.instantiateDummyVC()
                       vc.dummyString = "Privacy Policy"
                                navigationController?.pushViewController(vc, animated: true)
             tableView.deselectRow(at: indexPath, animated: false)
        } else if dataSource?[indexPath.row] == SettingsList.FAQ.rawValue  {
            let vc = UIStoryboard.moreStoryboard.instantiateFaqVC()
            navigationController?.pushViewController(vc, animated: true)
             tableView.deselectRow(at: indexPath, animated: false)
        } else if dataSource?[indexPath.row] == SettingsList.TermsAndConditions.rawValue  {
            let vc = UIStoryboard.moreStoryboard.instantiateDummyVC()
                                 vc.dummyString = "Terms & Conditions"
                                          navigationController?.pushViewController(vc, animated: true)
             tableView.deselectRow(at: indexPath, animated: false)
        } else if dataSource?[indexPath.row] == SettingsList.TermsOfUse.rawValue  {
            let vc = UIStoryboard.moreStoryboard.instantiateDummyVC()
                                           vc.dummyString = "Terms Of Use"
                                                    navigationController?.pushViewController(vc, animated: true)
             tableView.deselectRow(at: indexPath, animated: false)
        } else if dataSource?[indexPath.row] == SettingsList.ContactUs.rawValue  {
            let vc = UIStoryboard.moreStoryboard.instantiateAdvertiseWithUsViewController()
            vc.controlleryType = "Contact"
            navigationController?.pushViewController(vc, animated: true)
             tableView.deselectRow(at: indexPath, animated: false)
        } else if dataSource?[indexPath.row] == SettingsList.FeedBack.rawValue  {
            let vc = UIStoryboard.moreStoryboard.instantiateFeedBackVC()
            navigationController?.pushViewController(vc, animated: true)
             tableView.deselectRow(at: indexPath, animated: false)
        } else if dataSource?[indexPath.row] == SettingsList.AdvertiseWithUs.rawValue  {
            let vc = UIStoryboard.moreStoryboard.instantiateAdvertiseWithUsViewController()
            navigationController?.pushViewController(vc, animated: true)
             tableView.deselectRow(at: indexPath, animated: false)
        }else if dataSource?[indexPath.row] == SettingsList.AboutUs.rawValue  {
            let vc = UIStoryboard.moreStoryboard.instantiateDummyVC()
            vc.dummyString = "About Us"
            navigationController?.pushViewController(vc, animated: true)
             tableView.deselectRow(at: indexPath, animated: false)
        }
    }
}
extension SettingAndPrivacyViewController: RecevingActionFromTimelineCreate {
    func postAction() {
        let vc = UIStoryboard.timeLineStoryboard.instantiateCreateSpotViewController()
        vc.currentTab?.setBar(hidden: true, animated: true)
        vc.vcType = .createPostFromOutside
       // vc.delegate = self
        navigationController?.pushViewController(vc, animated: true)
        createPoptip.hide()
    }
    
    func goLiveAction() {
        createPoptip.hide()
        let vc = UIStoryboard.watchStoryboard.instantiateLiveViewController()
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func gradutaeSpotAction() {
        let vc = UIStoryboard.timeLineStoryboard.instantiateCreateSpotViewController()
        vc.tabBarController?.tabBar.isHidden = true
        vc.vcType = .createSpotFromOutside
        //vc.delegate = self
        navigationController?.pushViewController(vc, animated: true)
        createPoptip.hide()
    }
    
    func graduateUniAction() {
        let vc = UIStoryboard.timeLineStoryboard.instantiateCreateUniversityVC()
        vc.hidesBottomBarWhenPushed = true
        navigationController?.pushViewController(vc, animated: false)
        createPoptip.hide()
    }
    
    func dealAction() {
        let vc = UIStoryboard.timeLineStoryboard.instantiateCreateSpotViewController()
        vc.currentTab?.setBar(hidden: true, animated: true)
        vc.vcType = .createDealFromOutside
        navigationController?.pushViewController(vc, animated: true)
        createPoptip.hide()
    }
    
    
}

