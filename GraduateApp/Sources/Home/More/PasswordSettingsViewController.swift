//
//  PasswordSettingsViewController.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 7/2/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import UIKit

class PasswordSettingsViewController: UIViewController {

    @IBOutlet weak var mailBtn: AnimatedButton!
    @IBOutlet weak var updateBtn: AnimatedButton!
    @IBOutlet weak var newPasswordTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    var oldpasswordSecured: Bool? = false
    var newpasswordSecured: Bool? = false
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.title = "Password Settings"
        addingEyeBtn(textField: passwordTextField, image: "secured")
        addingEyeBtn(textField: newPasswordTextField, image: "secured")
    }
    func addingEyeBtn(textField: UITextField,image: String) {
           let button = UIButton(type: .custom)
           let image = UIImage(named: image)?.withRenderingMode(.alwaysTemplate)
           button.setImage(image, for: .normal)
           button.tintColor = UIColor(hexString: "A58E52")
           button.imageEdgeInsets = UIEdgeInsets(top: 0, left: -16, bottom: 0, right: 0)
           button.frame = CGRect(x: CGFloat(textField.frame.size.width - 25), y: CGFloat(5), width: CGFloat(25), height: CGFloat(25))
        if textField == passwordTextField {
              button.addTarget(self, action: #selector(self.refresh), for: .touchUpInside)
        } else if textField == newPasswordTextField {
             button.addTarget(self, action: #selector(self.refresh2), for: .touchUpInside)
        }
         
           textField.rightView = button
           textField.rightViewMode = .always
       }
       @IBAction func refresh(_ sender: UIButton) {
        if oldpasswordSecured == true {
            oldpasswordSecured = false
             passwordTextField.isSecureTextEntry = true
            addingEyeBtn(textField: passwordTextField, image: "secured")
        } else {
            oldpasswordSecured = true
             addingEyeBtn(textField: passwordTextField, image: "unsecured")
            passwordTextField.isSecureTextEntry = false
        }
       }
    @IBAction func refresh2(_ sender: UIButton) {
           if newpasswordSecured == true{
            newpasswordSecured = false
             addingEyeBtn(textField: newPasswordTextField, image: "secured")
                       newPasswordTextField.isSecureTextEntry = true
                  } else {
            newpasswordSecured = true
             addingEyeBtn(textField: newPasswordTextField, image: "unsecured")
                      newPasswordTextField.isSecureTextEntry = false
                  }
           }
    @IBAction func updateBtnAction(_ sender: Any) {
        if passwordTextField.text == "" || newPasswordTextField.text == "" {
            popUPErrorMessageVC(message: "", popType: .registerEmptyField)
        } else {
            ChangePasswordResponse.requestToChangePassword(oldPassword: passwordTextField.text ?? "", newPassword: newPasswordTextField.text ?? "",email: UserDefaultsHandler.getUDValue(key: .userEmail)!) { [weak self](response) in
                print(response?.errors?.newPassword?[0])
                
                guard let strongSelf = self else { return }
                if response?.errors?.newPassword?.count ?? 0 > 0 {
                     strongSelf.popUPErrorMessageVC(message: response?.errors?.newPassword?[0] ?? "", popType: .passwordValidation)
//
                } else if response?.changePasswordState == true{
                    strongSelf.popUPErrorMessageVC(message: "Old password not matched", popType: .oldPasswordNotMatched)
                } else if response?.success == true {
                     strongSelf.popUPErrorMessageVC(message: "password changed", popType: .passwordChanged)
                }
               
            }
        }
    }
    
    @IBAction func mailBtnAction(_ sender: Any) {
    }
    func popUPErrorMessageVC(message: String, popType: ErrorType) {
           let vc = UIStoryboard.timeLineStoryboard.instantiateErrorCarMsgPopVC()
           vc.popupType = popType
                  vc.messageString = message
                                 let cardPopup = SBCardPopupViewController(contentViewController: vc)
                                 cardPopup.show(onViewController: self)
       }
    
}
