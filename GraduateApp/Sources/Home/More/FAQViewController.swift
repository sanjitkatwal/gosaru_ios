//
//  FAQViewController.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 8/11/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import UIKit

class FAQViewController: UIViewController {

    @IBOutlet weak var tableViw: UITableView!
    var dataSource: [FaqModel]?
     var selectedRow: Int?
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        tableViw.dataSource = self
        tableViw.delegate = self
        
        self.title = "FAQ"
        addData()
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.isNavigationBarHidden = false
        currentTabBar?.setBar(hidden: true, animated: true)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    
              currentTabBar?.setBar(hidden: false, animated: true)
    }
    func addData() {
        dataSource = [FaqModel(question: "What is the main aim of the aplication", answer: "To earn money", selected: false),FaqModel(question: "Who is the developer of the app?", answer: "The team", selected: false), FaqModel(question: "Who is the best actor?", answer: "Jack Black", selected: false),FaqModel(question: "Which is the best band?", answer: "Led Zepplin", selected: false),FaqModel(question: "Where do you live?", answer: "In Nepal", selected: false), FaqModel(question: "How much money do you make ?", answer: "Its all private", selected: false), FaqModel(question: "Why the chicken crossed the road?", answer: "Ask the chicken", selected: false)]
        tableViw.reloadData()
    }
    

}
extension FAQViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        let cell = tableView.dequeueReusableCell(withIdentifier: "FaqTableViewCell", for: indexPath) as! FaqTableViewCell
        
        cell.questionLabel.text = dataSource?[indexPath.row].question
        cell.answerLabel.text = dataSource?[indexPath.row].answer
//        cell.dropDownImage.tranform = CGAffineTransformMakeRotation(CGFloat((selectedIndex == indexPath.row) ? M_PI : 0));

        if dataSource?[indexPath.row].selected == true {
            cell.answerView.isHidden = false
           // cell.dropDownImage.transform = cell.dropDownImage.transform.rotated(by: (180.0 * .pi) / 180.0)
           
                           cell.dropDownImage.transform = CGAffineTransform(rotationAngle: .pi)
                       
            
        } else {
            cell.answerView.isHidden = true
           cell.dropDownImage.transform = CGAffineTransform.identity

          //  cell.dropDownImage.image = UIImage(named: "iconArrowDown")
             

        }
//        if indexPath.row == selectedRow {
//
//            UIView.animate(withDuration: 1) {
//                cell.dropDownImage.transform = CGAffineTransform(rotationAngle: .pi)
//            }
//
//        } else {
//            cell.dropDownImage.image = UIImage(named: "iconArrowDown")
//        }
        return cell
    }
    
    
}
extension FAQViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
        selectedRow = indexPath.row
        if dataSource?[indexPath.row].selected == false {
             dataSource?[indexPath.row].selected = true
            tableView.reloadRows(at: [indexPath], with: .automatic)
        } else {
            dataSource?[indexPath.row].selected = false
                       tableView.reloadRows(at: [indexPath], with: .automatic)
        }
       
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

struct FaqModel {
    var question: String?
    var answer: String?
    var selected: Bool?
}
extension UIImage {
    func rotate(radians: CGFloat) -> UIImage {
        let rotatedSize = CGRect(origin: .zero, size: size)
            .applying(CGAffineTransform(rotationAngle: CGFloat(radians)))
            .integral.size
        UIGraphicsBeginImageContext(rotatedSize)
        if let context = UIGraphicsGetCurrentContext() {
            let origin = CGPoint(x: rotatedSize.width / 2.0,
                                 y: rotatedSize.height / 2.0)
            context.translateBy(x: origin.x, y: origin.y)
            context.rotate(by: radians)
            draw(in: CGRect(x: -origin.y, y: -origin.x,
                            width: size.width, height: size.height))
            let rotatedImage = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()

            return rotatedImage ?? self
        }

        return self
    }
}
