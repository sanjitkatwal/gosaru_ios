//
//  MediaPickerViewController.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 1/7/21.
//  Copyright © 2021 Mousham Pradhan. All rights reserved.
//

import UIKit
import MediaPlayer

class MediaPickerViewController: UIViewController, UIViewControllerTransitioningDelegate {
//    var mediaPicker: MPMediaPickerController?
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(true, animated: true)
    }
    @IBAction func acx(_ sender: Any) {
        var mediaPicker: MPMediaPickerController = MPMediaPickerController.self(mediaTypes:MPMediaType.music)

        mediaPicker = MPMediaPickerController(mediaTypes: .music)
        mediaPicker.delegate = self
        mediaPicker.allowsPickingMultipleItems = false
        self.definesPresentationContext = true
        mediaPicker.modalPresentationStyle = .overCurrentContext
       // mediaPicker?.modalTransitionStyle = .coverVertical
        mediaPicker.transitioningDelegate = self
        //self.present(mediaPicker!, animated: true, completion: nil)
        self.navigationController?.pushViewController(mediaPicker, animated: true)
        //self.present(mediaPicker, animated: true, completion: nil)
//        self.presentationControllerForPresentedViewController(presented: self, presentingViewController: mediaPicker, sourceViewController: self)
       

        
    }
    func presentationControllerForPresentedViewController(presented: UIViewController, presentingViewController presenting: UIViewController!, sourceViewController source: UIViewController) -> UIPresentationController? {
        return HalfSizePresentationController(presentedViewController: presented, presenting: presentingViewController)
       }
}
class HalfSizePresentationController : UIPresentationController {
     func frameOfPresentedViewInContainerView() -> CGRect {
        return CGRect(x: 0, y: (containerView?.bounds.height)!/2, width: containerView!.bounds.width, height: containerView!.bounds.height/2)
    }
}
extension MediaPickerViewController: MPMediaPickerControllerDelegate {
    func mediaPicker(_ mediaPicker: MPMediaPickerController, didPickMediaItems mediaItemCollection: MPMediaItemCollection) {
        let musicPlayer = MPMusicPlayerController.systemMusicPlayer
        musicPlayer.setQueue(with: mediaItemCollection)
        mediaPicker.dismiss(animated: true)
        print(musicPlayer)
        print(mediaItemCollection)
        // Begin playback.
       // musicPlayer.play()
        self.navigationController?.popViewController(animated: true)
    }
    func mediaPickerDidCancel(_ mediaPicker: MPMediaPickerController) {
        navigationController?.popViewController(animated: true)
    }
}
