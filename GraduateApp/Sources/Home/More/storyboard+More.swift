//
//  storyboard+More.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 6/24/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import Foundation
import UIKit
extension UIStoryboard {
    private struct Constants {
        static let moreStoryboard = "more"
        static let moreIdentifier = "MoreViewController"
        static let notificationIdentifier = "NotificationViewController"
        static let settingAndPrivacyIdentifier = "SettingAndPrivacyViewController"
        static let accountSettingsIdentifier = "AccountSettingsViewController"
        static let passwordSettingsIdentifier = "PasswordSettingsViewController"
        static let createAdvertiseIdentifier = "CreateAdvertisementViewController"
        static let followesIdentifier = "FollowersViewController"
        static let dummyuIdentifier = "DummyViewController"
        static let feedbackIdentifier = "FeedbackViewController"
        static let faqIdentifier = "FAQViewController"
        static let notificationDetailsIdentifier = "NotificationDetailsViewController"
        static let chatIdentifier = "ChatViewController"
        static let searchDetailsIdentifier = "SearchDetailsViewController"

        
    }
    static var moreStoryboard: UIStoryboard {
        return UIStoryboard(name: Constants.moreStoryboard, bundle: nil)
    }
    func instantiateMoreViewController() -> MoreViewController {
        guard let viewController = UIStoryboard.moreStoryboard.instantiateViewController(withIdentifier: Constants.moreIdentifier) as? MoreViewController else {
            fatalError("Couldn't instantiate MoreViewController")
        }
        return viewController
    }
    func instantiateNotificationViewController() -> NotificationViewController {
        guard let viewController = UIStoryboard.moreStoryboard.instantiateViewController(withIdentifier: Constants.notificationIdentifier) as? NotificationViewController else {
            fatalError("Couldn't instantiate NotificationViewController")
        }
        return viewController
    }
    func instantiateSettingViewController() -> SettingAndPrivacyViewController {
        guard let viewController = UIStoryboard.moreStoryboard.instantiateViewController(withIdentifier: Constants.settingAndPrivacyIdentifier) as? SettingAndPrivacyViewController else {
            fatalError("Couldn't instantiate SettingAndPrivacyViewController")
        }
        return viewController
    }
    func instantiateaccountSettingViewController() -> AccountSettingsViewController {
        guard let viewController = UIStoryboard.moreStoryboard.instantiateViewController(withIdentifier: Constants.accountSettingsIdentifier) as? AccountSettingsViewController else {
            fatalError("Couldn't instantiate AccountSettingsViewController")
        }
        return viewController
    }
    func instantiateaccountPasswordSettingViewController() -> PasswordSettingsViewController {
        guard let viewController = UIStoryboard.moreStoryboard.instantiateViewController(withIdentifier: Constants.passwordSettingsIdentifier) as? PasswordSettingsViewController else {
            fatalError("Couldn't instantiate PasswordSettingsViewController")
        }
        return viewController
    }
    func instantiateAdvertiseWithUsViewController() -> CreateAdvertisementViewController {
        guard let viewController = UIStoryboard.moreStoryboard.instantiateViewController(withIdentifier: Constants.createAdvertiseIdentifier) as? CreateAdvertisementViewController else {
            fatalError("Couldn't instantiate CreateAdvertisementViewController")
        }
        return viewController
    }
    func instantiateFollowersVC() -> FollowersViewController {
        guard let viewController = UIStoryboard.moreStoryboard.instantiateViewController(withIdentifier: Constants.followesIdentifier) as? FollowersViewController else {
            fatalError("Couldn't instantiate FollowersViewController")
        }
        return viewController
    }
    
    func instantiateDummyVC() -> DummyViewController {
        guard let viewController = UIStoryboard.moreStoryboard.instantiateViewController(withIdentifier: Constants.dummyuIdentifier) as? DummyViewController else {
            fatalError("Couldn't instantiate DummyViewController")
        }
        return viewController
    }
    func instantiateFeedBackVC() -> FeedbackViewController {
        guard let viewController = UIStoryboard.moreStoryboard.instantiateViewController(withIdentifier: Constants.feedbackIdentifier) as? FeedbackViewController else {
            fatalError("Couldn't instantiate FeedbackViewController")
        }
        return viewController
    }
    func instantiateFaqVC() -> FAQViewController {
        guard let viewController = UIStoryboard.moreStoryboard.instantiateViewController(withIdentifier: Constants.faqIdentifier) as? FAQViewController else {
            fatalError("Couldn't instantiate FAQViewController")
        }
        return viewController
    }
    func instantiateNotificaitonDetailsVC() -> NotificationDetailsViewController {
           guard let viewController = UIStoryboard.moreStoryboard.instantiateViewController(withIdentifier: Constants.notificationDetailsIdentifier) as? NotificationDetailsViewController else {
               fatalError("Couldn't instantiate NotificationDetailsViewController")
           }
           return viewController
       }
    func instantiateChatVC() -> ChatViewController {
        guard let viewController = UIStoryboard.moreStoryboard.instantiateViewController(withIdentifier: Constants.chatIdentifier) as? ChatViewController else {
            fatalError("Couldn't instantiate ChatViewController")
        }
        return viewController
    }
    func instantiateMediaPickerVC() -> MediaPickerViewController {
        guard let viewController = UIStoryboard.moreStoryboard.instantiateViewController(withIdentifier: "MediaPickerViewController") as? MediaPickerViewController else {
            fatalError("Couldn't instantiate MediaPickerViewController")
        }
        return viewController
    }
   
}

