//
//  ChangePasswordResponse.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 9/11/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import Foundation
import ObjectMapper
class ChangePasswordResponse: DefaultResponse {
   
override func mapping(map: Map) {
  super.mapping(map: map)
}
// MARK: - login request
    class func requestToChangePassword(oldPassword: String,newPassword: String, email: Any ,completionHandler:@escaping ((ChangePasswordResponseModel?) -> Void)) {
        APIManager(urlString: GraduateUrl.changePassowrdURL, parameters: ["old_password" : oldPassword, "new_password" : newPassword, "email" : email], method: .post ).handleResponse(completionHandler: { (response: ChangePasswordResponseModel) in
              print(response)
                  completionHandler(response)
              }, failureBlock: {
                completionHandler(nil)
              })
      }
}
 class ChangePasswordResponseModel: DefaultResponse {

// MARK: Declaration for string constants to be used to decode and also serialize.
internal let kChangePasswordResponseModelErrorsKey: String = "errors"
internal let kChangePasswordResponseModelMessageKey: String = "message"


// MARK: Properties
public var errors: ChangePassowrdErrors?
    public var error: Bool?
    public var changePasswordState: Bool?
    public var success: Bool?
   



// MARK: ObjectMapper Initalizers
/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/


/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/
    override func mapping(map: Map) {
    errors <- map[kChangePasswordResponseModelErrorsKey]
    message <- map[kChangePasswordResponseModelMessageKey]
   
        changePasswordState <- map["error"]
        success <- map["success"]

}
}
class ChangePassowrdErrors: Mappable {
    required init?(map: Map) {
        
    }
    

// MARK: Declaration for string constants to be used to decode and also serialize.
internal let kErrorsNewPasswordKey: String = "new_password"


// MARK: Properties
public var newPassword: [String]?



// MARK: ObjectMapper Initalizers
/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/

/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/
func mapping(map: Map) {
    newPassword <- map[kErrorsNewPasswordKey]

}
}

