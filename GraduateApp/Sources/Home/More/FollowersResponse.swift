//
//  FollowersResponse.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 9/16/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import Foundation
import ObjectMapper
class FollowersResponse: DefaultResponse {
    var data: LoginResponseModel?
override func mapping(map: Map) {
  super.mapping(map: map)

}
// MARK: - USER FOLLOWERS request
    class func requestToAFollowersList(showHud: Bool = true ,completionHandler:@escaping ((FollowersReponseModel?) -> Void)) {
        APIManager(urlString: GraduateUrl.followersListURL, method: .get ).handleResponse(showProgressHud: showHud,completionHandler: { (response: FollowersReponseModel) in
              print(response)
                  completionHandler(response)
              }, failureBlock: {
                completionHandler(nil)
              })
      }
    
    //MARK:- FOLLOW  UNFOLLOW REQUEST
    class func requestFollowUnfollowOthers(url: String,showHud: Bool = false ,completionHandler:@escaping ((FollowersReponseModel?) -> Void)) {
      APIManager(urlString: url, method: .post).handleResponse(showProgressHud: showHud,completionHandler: { (response: FollowersReponseModel) in
            print(response)
                completionHandler(response)
            }, failureBlock: {
              completionHandler(nil)
            })
    }
}

 class FollowersReponseModel: DefaultResponse {

// MARK: Declaration for string constants to be used to decode and also serialize.
internal let kFollowersReponseModelDataKey: String = "data"
internal let kFollowersReponseModelLinksKey: String = "links"
internal let kFollowersReponseModelMetaKey: String = "meta"


// MARK: Properties
public var data: [FollowersData]?
public var links: FollowersLinks?
public var meta: FollowersMeta?



// MARK: ObjectMapper Initalizers
/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/


/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/
    public override func mapping(map: Map) {
    data <- map[kFollowersReponseModelDataKey]
    links <- map[kFollowersReponseModelLinksKey]
    meta <- map[kFollowersReponseModelMetaKey]

}
}
public class FollowersLinks: Mappable {
    public required init?(map: Map) {
        
    }
    

// MARK: Declaration for string constants to be used to decode and also serialize.
internal let kLinksNextKey: String = "next"
internal let kLinksLastKey: String = "last"
internal let kLinksPrevKey: String = "prev"
internal let kLinksFirstKey: String = "first"


// MARK: Properties
public var next: String?
public var last: String?
public var prev: String?
public var first: String?



// MARK: ObjectMapper Initalizers
/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/


/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/
    public func mapping(map: Map) {
    next <- map[kLinksNextKey]
    last <- map[kLinksLastKey]
    prev <- map[kLinksPrevKey]
    first <- map[kLinksFirstKey]

}
}
public class FollowersData: Mappable {
    public required init?(map: Map) {
        
    }
    

// MARK: Declaration for string constants to be used to decode and also serialize.
internal let kDataProfileImageKey: String = "profile_image"
internal let kDataTypeKey: String = "type"
internal let kDataIsFollowedKey: String = "is_followed"
internal let kDataInternalIdentifierKey: String = "id"
internal let kDataNameKey: String = "name"


// MARK: Properties
public var profileImage: String?
public var type: String?
public var isFollowed: Bool = false
public var internalIdentifier: Int?
public var name: String?



// MARK: ObjectMapper Initalizers
/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/


/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/
    public func mapping(map: Map) {
    profileImage <- map[kDataProfileImageKey]
    type <- map[kDataTypeKey]
    isFollowed <- map[kDataIsFollowedKey]
    internalIdentifier <- map[kDataInternalIdentifierKey]
    name <- map[kDataNameKey]
    }
}
public class FollowersMeta: Mappable {
    public required init?(map: Map) {
        
    }
    

// MARK: Declaration for string constants to be used to decode and also serialize.
internal let kMetaFromKey: String = "from"
internal let kMetaLastPageKey: String = "last_page"
internal let kMetaPerPageKey: String = "per_page"
internal let kMetaCurrentPageKey: String = "current_page"
internal let kMetaPathKey: String = "path"
internal let kMetaTotalKey: String = "total"
internal let kMetaToKey: String = "to"


// MARK: Properties
public var from: Int?
public var lastPage: Int?
public var perPage: Int?
public var currentPage: Int?
public var path: String?
public var total: Int?
public var to: Int?



// MARK: ObjectMapper Initalizers
/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/


/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/
    public func mapping(map: Map) {
    from <- map[kMetaFromKey]
    lastPage <- map[kMetaLastPageKey]
    perPage <- map[kMetaPerPageKey]
    currentPage <- map[kMetaCurrentPageKey]
    path <- map[kMetaPathKey]
    total <- map[kMetaTotalKey]
    to <- map[kMetaToKey]

}
}
