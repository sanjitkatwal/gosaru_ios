//
//  UserProfileResponse.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 9/15/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import Foundation
import ObjectMapper

class UserProfileFullDetails: DefaultResponse {
    var data: LoginResponseModel?
    var occupationList: [String] = []
    var datas: [FriendshipData]?
override func mapping(map: Map) {
  super.mapping(map: map)
datas <- map["data"]
}
// MARK: - GET USER FULL DETAILS
    class func requestToUserFullDetails(showHud: Bool = true ,completionHandler:@escaping ((UserFullDetailsResponseModel?) -> Void)) {
        APIManager(urlString: GraduateUrl.fullUserDetalsURL, method: .get ).handleResponse(showProgressHud: showHud,completionHandler: { (response: UserFullDetailsResponseModel) in
              print(response)
                  completionHandler(response)
              }, failureBlock: {
                completionHandler(nil)
              })
      }
    
    //MARK:- GET USER FULL DETAILS BY ID
      class func requestToOtherUserDetails(id: Int?,showHud: Bool = true ,completionHandler:@escaping ((UserFullDetailsResponseModel?) -> Void)) {
             APIManager(urlString: GraduateUrl.removeFollowURL + "\(id ?? 0)" + "/block", method: .get ).handleResponse(showProgressHud: showHud,completionHandler: { (response: UserFullDetailsResponseModel) in
                 print(response)
                     completionHandler(response)
                 }, failureBlock: {
                   completionHandler(nil)
                 })
         }
    //MARK:- DELETE THE USER DEFAULT MUSCI
    class func requestToDeleteUserDefaultMusic(showHud: Bool = true ,completionHandler:@escaping ((UserProfileFullDetails?) -> Void)) {
           APIManager(urlString: GraduateUrl.deleteDefaultMusicURL, method: .delete ).handleResponse(showProgressHud: showHud,completionHandler: { (response: UserProfileFullDetails) in
                 print(response)
                     completionHandler(response)
                 }, failureBlock: {
                   completionHandler(nil)
                 })
         }
    //MARK:- REQUEST FOR FRIENDS
    class func requestToFriends(showHud: Bool = true ,completionHandler:@escaping ((FriendsResponseModel?) -> Void)) {
              APIManager(urlString: GraduateUrl.friendsURL, method: .get ).handleResponse(showProgressHud: showHud,completionHandler: { (response: FriendsResponseModel) in
                    print(response)
                        completionHandler(response)
                    }, failureBlock: {
                      completionHandler(nil)
                    })
            }
    
    //MARK:- REQUEST FOR FRIENDSHIP

    class func requestToFriendship(showHud: Bool = true ,completionHandler:@escaping ((FriendshipResponseModel?) -> Void)) {
                 APIManager(urlString: GraduateUrl.friendshipURL, method: .get ).handleResponse(showProgressHud: showHud,completionHandler: { (response: FriendshipResponseModel) in
                       print(response)
                           completionHandler(response)
                       }, failureBlock: {
                         completionHandler(nil)
                       })
               }
    
      //MARK:- REQUEST FOR PENDING FRIENDS
   class func requestToPendingFriends(showHud: Bool = true ,completionHandler:@escaping ((PendingFriendsModel?) -> Void)) {
     APIManager(urlString: GraduateUrl.pendingFriendsURL, method: .get ).handleResponse(showProgressHud: showHud,completionHandler: { (response: PendingFriendsModel) in
           print(response)
               completionHandler(response)
           }, failureBlock: {
             completionHandler(nil)
           })
   }
    
    //MARK:- REQUEST FOR Blocked FRIENDS
    class func requestToBlockedFriends(showHud: Bool = true ,completionHandler:@escaping ((BlockedFriendsModel?) -> Void)) {
      APIManager(urlString: GraduateUrl.blockedFriendsURL, method: .get ).handleResponse(showProgressHud: showHud,completionHandler: { (response: BlockedFriendsModel) in
            print(response)
                completionHandler(response)
            }, failureBlock: {
              completionHandler(nil)
            })
    }
    
    //MARK:- REQUEST FOR FRIENDS REQUEST
       class func requestToFriendRequest(showHud: Bool = true ,completionHandler:@escaping ((FriendRequestModel?) -> Void)) {
         APIManager(urlString: GraduateUrl.friendRequestURL, method: .get ).handleResponse(showProgressHud: showHud,completionHandler: { (response: FriendRequestModel) in
               print(response)
                   completionHandler(response)
               }, failureBlock: {
                 completionHandler(nil)
               })
       }
    
    //MARK:- REQUEST FOR DECLINED REQUEST
          class func requestToDeclinedRequest(showHud: Bool = true ,completionHandler:@escaping ((DeclinedRequestModel?) -> Void)) {
            APIManager(urlString: GraduateUrl.declinedRequestURL, method: .get ).handleResponse(showProgressHud: showHud,completionHandler: { (response: DeclinedRequestModel) in
                  print(response)
                      completionHandler(response)
                  }, failureBlock: {
                    completionHandler(nil)
                  })
          }
     //MARK:- REQUEST FOR UNFRIEND
    class func requestTUnfriend(id: Int?,showHud: Bool = true ,completionHandler:@escaping ((DeclinedRequestModel?) -> Void)) {
        APIManager(urlString: GraduateUrl.unfriendURL + "\(id ?? 0)" + "/unfriend", method: .post ).handleResponse(showProgressHud: showHud,completionHandler: { (response: DeclinedRequestModel) in
            print(response)
                completionHandler(response)
            }, failureBlock: {
              completionHandler(nil)
            })
    }
      //MARK:- REQUEST FOR BLOCKING OTHERS
    class func requestToBlock(id: Int?,showHud: Bool = true ,completionHandler:@escaping ((DeclinedRequestModel?) -> Void)) {
           APIManager(urlString: GraduateUrl.unfriendURL + "\(id ?? 0)" + "/block", method: .post ).handleResponse(showProgressHud: showHud,completionHandler: { (response: DeclinedRequestModel) in
               print(response)
                   completionHandler(response)
               }, failureBlock: {
                 completionHandler(nil)
               })
       }
  
}
 class UserFullDetailsResponseModel: DefaultResponse {

// MARK: Declaration for string constants to be used to decode and also serialize.
internal let kUserFullDetailsResponseModelDataKey: String = "data"


// MARK: Properties
public var data: FullDetailsUserData?
    var links: TimeLineLinks?
    var meta: TimeLineMeta?



// MARK: ObjectMapper Initalizers
/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/


/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/
    public override func mapping(map: Map) {
    data <- map[kUserFullDetailsResponseModelDataKey]
    links <- map["links"]
    meta <- map["meta"]

}
}
public class FullDetailsUserData: Mappable {
    public required init?(map: Map) {
        
    }
    

// MARK: Declaration for string constants to be used to decode and also serialize.
internal let kDataFollowingCountKey: String = "following_count"
internal let kDataIsFollowedKey: String = "is_followed"
internal let kDataFriendStatusKey: String = "friend_status"
internal let kDataUsernameKey: String = "username"
internal let kDataInternalIdentifierKey: String = "id"
internal let kDataFollowsCountKey: String = "follows_count"
internal let kDataPhoneKey: String = "phone"
internal let kDataProfileKey: String = "profile"
internal let kDataEmailKey: String = "email"
internal let kDataViewsCountKey: String = "views_count"
internal let kDataIsFriendKey: String = "is_friend"
internal let kDataNameKey: String = "name"


// MARK: Properties
public var followingCount: Int?
public var isFollowed: Bool = false
public var friendStatus: String?
public var username: String?
public var internalIdentifier: Int?
public var followsCount: Int?
public var phone: String?
public var profile: FullDetailsUserProfile?
public var email: String?
public var viewsCount: String?
public var isFriend: Bool = false
public var name: String?



// MARK: ObjectMapper Initalizers
/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/


/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/
    public func mapping(map: Map) {
    followingCount <- map[kDataFollowingCountKey]
    isFollowed <- map[kDataIsFollowedKey]
    friendStatus <- map[kDataFriendStatusKey]
    username <- map[kDataUsernameKey]
    internalIdentifier <- map[kDataInternalIdentifierKey]
    followsCount <- map[kDataFollowsCountKey]
    phone <- map[kDataPhoneKey]
    profile <- map[kDataProfileKey]
    email <- map[kDataEmailKey]
    viewsCount <- map[kDataViewsCountKey]
    isFriend <- map[kDataIsFriendKey]
    name <- map[kDataNameKey]

}
}
public class FullDetailsUserProfile: Mappable {
    public required init?(map: Map) {
        
    }
    

// MARK: Declaration for string constants to be used to decode and also serialize.
internal let kProfileStateKey: String = "state"
internal let kProfileCoverImageKey: String = "cover_image"
internal let kProfileSuburbKey: String = "suburb"
internal let kProfileDobKey: String = "dob"
internal let kProfileCountryKey: String = "country"
internal let kProfilePostcodeKey: String = "postcode"
internal let kProfileInstagramKey: String = "instagram"
internal let kProfileLatKey: String = "lat"
internal let kProfileAddressStringKey: String = "address_string"
internal let kProfileSummaryKey: String = "summary"
internal let kProfileProfileImageKey: String = "profile_image"
internal let kProfileNicknameKey: String = "nickname"
internal let kProfileDefaultMusicKey: String = "default_music"
internal let kProfileGenderKey: String = "gender"
internal let kProfileInternalIdentifierKey: String = "id"
internal let kProfileStreetKey: String = "street"
internal let kProfileMaritalStatusKey: String = "marital_status"
internal let kProfilePhoneKey: String = "phone"
internal let kProfileLonKey: String = "lon"
internal let kProfileUserIdKey: String = "user_id"
internal let kProfileTwitterKey: String = "twitter"
internal let kProfileFacebookKey: String = "facebook"
internal let kProfileProfessionKey: String = "profession"


// MARK: Properties
public var state: String?
public var coverImage: String?
public var suburb: String?
public var dob: String?
public var country: String?
public var postcode: String?
public var instagram: String?
public var lat: String?
public var addressString: String?
public var summary: String?
public var profileImage: String?
public var nickname: String?
public var defaultMusic: String?
public var gender: String?
public var internalIdentifier: Int?
public var street: String?
public var maritalStatus: String?
public var phone: String?
public var lon: String?
public var userId: Int?
public var twitter: String?
public var facebook: String?
public var profession: String?



// MARK: ObjectMapper Initalizers
/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/

/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/
    public func mapping(map: Map) {
    state <- map[kProfileStateKey]
    coverImage <- map[kProfileCoverImageKey]
    suburb <- map[kProfileSuburbKey]
    dob <- map[kProfileDobKey]
    country <- map[kProfileCountryKey]
    postcode <- map[kProfilePostcodeKey]
    instagram <- map[kProfileInstagramKey]
    lat <- map[kProfileLatKey]
    addressString <- map[kProfileAddressStringKey]
    summary <- map[kProfileSummaryKey]
    profileImage <- map[kProfileProfileImageKey]
    nickname <- map[kProfileNicknameKey]
    defaultMusic <- map[kProfileDefaultMusicKey]
    gender <- map[kProfileGenderKey]
    internalIdentifier <- map[kProfileInternalIdentifierKey]
    street <- map[kProfileStreetKey]
    maritalStatus <- map[kProfileMaritalStatusKey]
    phone <- map[kProfilePhoneKey]
    lon <- map[kProfileLonKey]
    userId <- map[kProfileUserIdKey]
    twitter <- map[kProfileTwitterKey]
    facebook <- map[kProfileFacebookKey]
    profession <- map[kProfileProfessionKey]

}
}
 class UpdateResponseModel: DefaultResponse {
    

// MARK: Declaration for string constants to be used to decode and also serialize.
internal let kUpdateResponseModelDataKey: String = "data"
internal let kUpdateResponseModelUpdatedKey: String = "updated"


// MARK: Properties
public var data: UpdateData?
public var updated: ProfileUpdated?



// MARK: ObjectMapper Initalizers
/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/

/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/
    public override func mapping(map: Map) {
    data <- map[kUpdateResponseModelDataKey]
    updated <- map[kUpdateResponseModelUpdatedKey]

}
}
public class ProfileUpdated: Mappable {
    public required init?(map: Map) {
        
    }
    

// MARK: Declaration for string constants to be used to decode and also serialize.
internal let kUpdatedStateKey: String = "state"
internal let kUpdatedCoverImageKey: String = "cover_image"
internal let kUpdatedSuburbKey: String = "suburb"
internal let kUpdatedDobKey: String = "dob"
internal let kUpdatedCountryKey: String = "country"
internal let kUpdatedPostcodeKey: String = "postcode"
internal let kUpdatedInstagramKey: String = "instagram"
internal let kUpdatedLatKey: String = "lat"
internal let kUpdatedAddressStringKey: String = "address_string"
internal let kUpdatedSummaryKey: String = "summary"
internal let kUpdatedProfileImageKey: String = "profile_image"
internal let kUpdatedNicknameKey: String = "nickname"
internal let kUpdatedDefaultMusicKey: String = "default_music"
internal let kUpdatedGenderKey: String = "gender"
internal let kUpdatedInternalIdentifierKey: String = "id"
internal let kUpdatedStreetKey: String = "street"
internal let kUpdatedMaritalStatusKey: String = "marital_status"
internal let kUpdatedPhoneKey: String = "phone"
internal let kUpdatedLonKey: String = "lon"
internal let kUpdatedUserIdKey: String = "user_id"
internal let kUpdatedTwitterKey: String = "twitter"
internal let kUpdatedFacebookKey: String = "facebook"
internal let kUpdatedProfessionKey: String = "profession"


// MARK: Properties
public var state: String?
public var coverImage: String?
public var suburb: String?
public var dob: String?
public var country: String?
public var postcode: String?
public var instagram: String?
public var lat: String?
public var addressString: String?
public var summary: String?
public var profileImage: String?
public var nickname: String?
public var defaultMusic: String?
public var gender: String?
public var internalIdentifier: Int?
public var street: String?
public var maritalStatus: String?
public var phone: String?
public var lon: String?
public var userId: Int?
public var twitter: String?
public var facebook: String?
public var profession: String?



// MARK: ObjectMapper Initalizers
/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/


/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/
    public func mapping(map: Map) {
    state <- map[kUpdatedStateKey]
    coverImage <- map[kUpdatedCoverImageKey]
    suburb <- map[kUpdatedSuburbKey]
    dob <- map[kUpdatedDobKey]
    country <- map[kUpdatedCountryKey]
    postcode <- map[kUpdatedPostcodeKey]
    instagram <- map[kUpdatedInstagramKey]
    lat <- map[kUpdatedLatKey]
    addressString <- map[kUpdatedAddressStringKey]
    summary <- map[kUpdatedSummaryKey]
    profileImage <- map[kUpdatedProfileImageKey]
    nickname <- map[kUpdatedNicknameKey]
    defaultMusic <- map[kUpdatedDefaultMusicKey]
    gender <- map[kUpdatedGenderKey]
    internalIdentifier <- map[kUpdatedInternalIdentifierKey]
    street <- map[kUpdatedStreetKey]
    maritalStatus <- map[kUpdatedMaritalStatusKey]
    phone <- map[kUpdatedPhoneKey]
    lon <- map[kUpdatedLonKey]
    userId <- map[kUpdatedUserIdKey]
    twitter <- map[kUpdatedTwitterKey]
    facebook <- map[kUpdatedFacebookKey]
    profession <- map[kUpdatedProfessionKey]

}
}
public class UpdateData: Mappable {
    public required init?(map: Map) {
        
    }
    

// MARK: Declaration for string constants to be used to decode and also serialize.
internal let kDataSuburbKey: String = "suburb"
internal let kDataCoverImageKey: String = "cover_image"
internal let kDataStateKey: String = "state"
internal let kDataCountryKey: String = "country"
internal let kDataPostcodeKey: String = "postcode"
internal let kDataNicknameKey: String = "nickname"
internal let kDataSummaryKey: String = "summary"
internal let kDataProfileImageKey: String = "profile_image"
internal let kDataNameKey: String = "name"
internal let kDataAddressStringKey: String = "address_string"
internal let kDataDefaultMusicKey: String = "default_music"
internal let kDataGenderKey: String = "gender"
internal let kDataStreetKey: String = "street"
internal let kDataMaritalStatusKey: String = "marital_status"
internal let kDataPhoneKey: String = "phone"
internal let kDataProfessionKey: String = "profession"
internal let kDataDobKey: String = "dob"


// MARK: Properties
public var suburb: String?
public var coverImage: String?
public var state: String?
public var country: String?
public var postcode: String?
public var nickname: String?
public var summary: String?
public var profileImage: String?
public var name: String?
public var addressString: String?
public var defaultMusic: String?
public var gender: String?
public var street: String?
public var maritalStatus: String?
public var phone: String?
public var profession: String?
public var dob: String?



// MARK: ObjectMapper Initalizers
/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/


/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/
    public func mapping(map: Map) {
    suburb <- map[kDataSuburbKey]
    coverImage <- map[kDataCoverImageKey]
    state <- map[kDataStateKey]
    country <- map[kDataCountryKey]
    postcode <- map[kDataPostcodeKey]
    nickname <- map[kDataNicknameKey]
    summary <- map[kDataSummaryKey]
    profileImage <- map[kDataProfileImageKey]
    name <- map[kDataNameKey]
    addressString <- map[kDataAddressStringKey]
    defaultMusic <- map[kDataDefaultMusicKey]
    gender <- map[kDataGenderKey]
    street <- map[kDataStreetKey]
    maritalStatus <- map[kDataMaritalStatusKey]
    phone <- map[kDataPhoneKey]
    profession <- map[kDataProfessionKey]
    dob <- map[kDataDobKey]

}
}

 class FriendsResponseModel: DefaultResponse {

// MARK: Declaration for string constants to be used to decode and also serialize.
internal let kFriendsResponseModelDataKey: String = "data"
internal let kFriendsResponseModelLinksKey: String = "links"
internal let kFriendsResponseModelMetaKey: String = "meta"


// MARK: Properties
public var data: [FriendsData]?
 var links: TimeLineLinks?
 var meta: TimeLineMeta?



// MARK: ObjectMapper Initalizers
/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/


/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/
    override func mapping(map: Map) {
    data <- map[kFriendsResponseModelDataKey]
    links <- map[kFriendsResponseModelLinksKey]
    meta <- map[kFriendsResponseModelMetaKey]

}
}
public class FriendsData: Mappable {
    public required init?(map: Map) {
        
    }
    

// MARK: Declaration for string constants to be used to decode and also serialize.
internal let kDataUsernameKey: String = "username"
internal let kDataInternalIdentifierKey: String = "id"
internal let kDataPhoneKey: String = "phone"
internal let kDataProfileKey: String = "profile"
internal let kDataIsVerifiedKey: String = "is_verified"
internal let kDataEmailKey: String = "email"
internal let kDataNameKey: String = "name"


// MARK: Properties
public var username: String?
public var internalIdentifier: Int?
public var phone: String?
public var profile: FriendsProfile?
public var isVerified: Bool = false
public var email: String?
public var name: String?



// MARK: ObjectMapper Initalizers
/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/

/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/
    public func mapping(map: Map) {
    username <- map[kDataUsernameKey]
    internalIdentifier <- map[kDataInternalIdentifierKey]
    phone <- map[kDataPhoneKey]
    profile <- map[kDataProfileKey]
    isVerified <- map[kDataIsVerifiedKey]
    email <- map[kDataEmailKey]
    name <- map[kDataNameKey]

}
}
public class FriendsProfile: Mappable {
    public required init?(map: Map) {
        
    }
    

// MARK: Declaration for string constants to be used to decode and also serialize.
internal let kProfileProfileImageKey: String = "profile_image"
internal let kProfileNicknameKey: String = "nickname"
internal let kProfileGenderKey: String = "gender"


// MARK: Properties
public var profileImage: String?
public var nickname: String?
public var gender: String?



// MARK: ObjectMapper Initalizers
/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/


/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/
    public func mapping(map: Map) {
    profileImage <- map[kProfileProfileImageKey]
    nickname <- map[kProfileNicknameKey]
    gender <- map[kProfileGenderKey]

}
}

 class FriendshipResponseModel: DefaultResponse {

// MARK: Declaration for string constants to be used to decode and also serialize.
internal let kFriendshipResponseModelDataKey: String = "data"


// MARK: Properties
public var data: [FriendshipData]?



// MARK: ObjectMapper Initalizers
/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/


/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/
    override func mapping(map: Map) {
    data <- map[kFriendshipResponseModelDataKey]

}
}
public class FriendshipData: Mappable {
    public required init?(map: Map) {
        
    }
    

// MARK: Declaration for string constants to be used to decode and also serialize.
internal let kDataFriendKey: String = "friend"
internal let kDataCreatedAtKey: String = "created_at"
internal let kDataStatusKey: String = "status"
internal let kDataUpdatedAtKey: String = "updated_at"


// MARK: Properties
 var friend: FriendshipFriend?
public var createdAt: String?
public var status: String?
public var updatedAt: String?



// MARK: ObjectMapper Initalizers
/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/


/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/
    public func mapping(map: Map) {
    friend <- map[kDataFriendKey]
    createdAt <- map[kDataCreatedAtKey]
    status <- map[kDataStatusKey]
    updatedAt <- map[kDataUpdatedAtKey]

}
}
class FriendshipFriend: Mappable {
    required init?(map: Map) {
        
    }
    

// MARK: Declaration for string constants to be used to decode and also serialize.
internal let kFriendNameKey: String = "name"
internal let kFriendEmailKey: String = "email"
internal let kFriendInternalIdentifierKey: String = "id"
internal let kFriendPhoneKey: String = "phone"
internal let kFriendProfileKey: String = "profile"


// MARK: Properties
public var name: String?
public var email: String?
public var internalIdentifier: Int?
public var phone: String?
public var profile: FriendshipProfile?



// MARK: ObjectMapper Initalizers
/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/


/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/
    public func mapping(map: Map) {
    name <- map[kFriendNameKey]
    email <- map[kFriendEmailKey]
    internalIdentifier <- map[kFriendInternalIdentifierKey]
    phone <- map[kFriendPhoneKey]
    profile <- map[kFriendProfileKey]

}
}
class FriendshipProfile: Mappable {
    required init?(map: Map) {
        
    }
    

// MARK: Declaration for string constants to be used to decode and also serialize.
internal let kProfileProfileImageKey: String = "profile_image"
internal let kProfileNicknameKey: String = "nickname"
internal let kProfileGenderKey: String = "gender"


// MARK: Properties
public var profileImage: String?
public var nickname: String?
public var gender: String?



// MARK: ObjectMapper Initalizers
/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/

/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/
func mapping(map: Map) {
    profileImage <- map[kProfileProfileImageKey]
    nickname <- map[kProfileNicknameKey]
    gender <- map[kProfileGenderKey]

}
}
class PendingFriendsModel: DefaultResponse {
    var data: [FriendshipData]?
    
     public override func mapping(map: Map) {
        data <- map["data"]

    }
}
class BlockedFriendsModel: DefaultResponse {
    var data: [FriendshipData]?
    
     public override func mapping(map: Map) {
        data <- map["data"]

    }
}
class FriendRequestModel: DefaultResponse {
    var data: [FriendshipData]?
    
     public override func mapping(map: Map) {
        data <- map["data"]

    }
}
class DeclinedRequestModel: DefaultResponse {
    var data: [FriendshipData]?
    var success: Bool?
     public override func mapping(map: Map) {
        data <- map["data"]
        success <- map["success"]

    }
}



