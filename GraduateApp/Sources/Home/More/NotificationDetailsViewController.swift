//
//  NotificationDetailsViewController.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 8/17/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import UIKit
import Popover
class NotificationDetailsViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    var popupTableView = UITableView(frame: CGRect(x: 0, y: 0, width: 90, height: 120))
           var texts = ["Share", "Edit", "Delete"]
          var popover: Popover!
          fileprivate var popoverOptions: [PopoverOption] = [
            .type(.left),
            .blackOverlayColor(UIColor(white: 0.0, alpha: 0.6))
          ]
    var postId: Int? {
        didSet {
            requestForPostId(id: postId ?? 0)
        }
    }
      var data:[uniMyPostData]? = []
    var dataSource: NotificationsDetailsResponse? {
        didSet {
            tableView.reloadData()
        }
    }
    var interactionType: String?
    override func viewDidLoad() {
        super.viewDidLoad()
        registerView()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        currentTabBar?.setBar(hidden: true, animated: true)
        navigationController?.isNavigationBarHidden = false
    }
    
    func registerView() {
            tableView.register(UINib(nibName: "PostCreationTableViewCell", bundle: nil), forCellReuseIdentifier: "PostCreationTableViewCell")
        tableView.register(UINib(nibName: "EventTableViewCell", bundle: nil), forCellReuseIdentifier: "EventTableViewCell")
        tableView.dataSource = self
        tableView.delegate = self
       }
    //MARK:- REQUEST FOR POST WITH ID
    func requestForPostId(id: Int) {
        NotificationsDetailsResponse.requestToUserNotifications(id: id) { [weak self](response) in
             guard let strongSelf = self else { return }
            strongSelf.dataSource = response
            print(response?.data?.postingAsType)
            if response?.data != nil {
                
                strongSelf.data?.append((response?.data)!)
                strongSelf.data?.forEach {
                    print($0)
                }
                
                
                strongSelf.tableView.reloadData()
            }
           
           
        }
       

}
    //MARK:- RATING DETAILS
    func ratingDetailsAction(ratingId: Int, starredVlaue: String, starCount: Int) {
           let vc = UIStoryboard.timeLineStoryboard.instantiateRatingDetailsVC()
           vc.ratingId = ratingId
       vc.starredValue = starredVlaue
       vc.starCount = starCount
           navigationController?.pushViewController(vc, animated: true)
          }
}
extension NotificationDetailsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == popupTableView {
            return texts.count
        } else if tableView == tableView {
            print(data?.count)
            return data?.count ?? 0
        } else {
            return 0
        }
       
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == popupTableView {
            let cell = UITableViewCell(style: .default, reuseIdentifier: nil)
                                  cell.textLabel?.text = self.texts[(indexPath as NSIndexPath).row]
                               cell.textLabel?.textColor = UIColor.init(hexString: Appcolors.text)
            cell.selectionStyle = .none
                                   return cell
           
            
        } else if tableView == tableView {
            if interactionType == "attend" {
             let cell = tableView.dequeueReusableCell(withIdentifier: "EventTableViewCell", for: indexPath) as! EventTableViewCell
               // cell.delegate = self
                cell.selectionType = .gradSpot
                cell.index = indexPath
             
                //print(data?[0].postingAsType)
            
                return cell.eventTVC(cell: cell, indexPath: indexPath, dataSource: data)
            
            } else {
                
            
        let cell = tableView.dequeueReusableCell(withIdentifier: "PostCreationTableViewCell", for: indexPath) as! PostCreationTableViewCell
                   cell.delegate = self
            cell.index = indexPath
            cell.ratingNumLabel.text = "\(dataSource?.data?.starsCount ?? 0)"
            print(dataSource?.data?.comments?.count)
            cell.thoughtsNumLabel.text = "\(dataSource?.data?.comments?.count ?? 0)"
            cell.name = dataSource?.data?.postingAs?.name
            cell.captionLabel.text = dataSource?.data?.message
//            let newDate = HelperFunctions.timeInterval(timeAgo: dataSource?.data?.createdAt ?? "")
            cell.imageDataSource = dataSource?.data?.galleries
           //  cell.timeStampLabel.text = newDate
            if dataSource?.data?.hasShared == true {
                cell.ratingBtn.setImage(UIImage(named: "star3"))
            } else {
                cell.ratingBtn.setImage(UIImage(named: "star"))
            }
            cell.universityId = dataSource?.data?.internalIdentifier
            
            return cell
            }
        } else {
            return UITableViewCell()
        }
    }
    @objc func tapProfileImage() {
        let vc = UIStoryboard.moreStoryboard.instantiateUserProfileVC()
    
        navigationController?.pushViewController(vc, animated: true)
        }
    @objc func thoughtsDetails() {
       
           }
    @objc func ratingDetailsAction() {
           let vc = UIStoryboard.timeLineStoryboard.instantiateRatingDetailsVC()
           navigationController?.pushViewController(vc, animated: true)
       }
    func popingBottomVC(id: Int, starValue: String, selectedindexToReload: Int, selectionType: GraduateDetails) {
      let popupVC = UIStoryboard.timeLineStoryboard.instantiateRatingVC()
             //popupVC.delegate = self
          //popupVC.selectedIndex = 3
        popupVC.id = id
        popupVC.selectedIndex = Int(starValue)
        popupVC.indexToReload = selectedindexToReload
        popupVC.selectionType = selectionType
                    popupVC.height = 200
                    popupVC.topCornerRadius = 15
             popupVC.presentDuration = 0.25
                    popupVC.dismissDuration = 0.25
                    popupVC.shouldDismissInteractivelty = true
                   // popupVC.popupDelegate = self
                    present(popupVC, animated: true, completion: nil)
      }
    func sharePopOver(sender: UIButton) {
       
                  popupTableView.delegate = self
                  popupTableView.dataSource = self
                  popupTableView.isScrollEnabled = false
                  self.popover = Popover(options: self.popoverOptions)
                  self.popover.willShowHandler = {
                    print("willShowHandler")
                  }
                  self.popover.didShowHandler = {
                    print("didDismissHandler")
                  }
                  self.popover.willDismissHandler = {
                    print("willDismissHandler")
                  }
                  self.popover.didDismissHandler = {
                    print("didDismissHandler")
                  }
        
               self.popover.show(popupTableView, fromView: sender)
    }
    func pushingToThoughtsVC(commentId: Int, outsideComment: Bool, index: IndexPath) {
        if outsideComment == false {
            let vc = UIStoryboard.timeLineStoryboard.instantiateThoughtsVC()
                   vc.commentId = commentId
                   vc.index = index
            vc.delegate = self
                                navigationController?.pushViewController(vc, animated: true)
        } else {
            
        }
       
    }
}
extension NotificationDetailsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == popupTableView {
            return 40
        } else {
            return UITableView.automaticDimension
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == popupTableView {
            
                if texts[indexPath.row] ==  "Share"{
                let shareText = "Hello, world!"

                 let image = UIImage(named: "iconArrowDown")
                let vc = UIActivityViewController(activityItems: [shareText, image!], applicationActivities: [])
                    present(vc, animated: true)
                self.popover.dismiss()
                } else if texts[indexPath.row] == "Edit" {
                    let vc = UIStoryboard.timeLineStoryboard.instantiateCreateSpotViewController()
                    vc.controllerType = "Create Post"
                    navigationController?.pushViewController(vc, animated: true)
                    self.popover.dismiss()
                } else if texts[indexPath.row] == "Delete" {
                   let vc = UIStoryboard.timeLineStoryboard.instantiateDeletePostPopUpVC()
                                 let cardPopup = SBCardPopupViewController(contentViewController: vc)
                                 cardPopup.show(onViewController: self)
                    self.popover.dismiss()
                }else {
                    let vc = UIStoryboard.graduateStoryboard.instantiateReportVC()
                                              vc.view.isOpaque = false
                                              vc.view.backgroundColor = UIColor.black.withAlphaComponent(0.8)
                                              vc.modalPresentationStyle = .custom
                                        
                    
                    
                    self.present(vc, animated: true, completion: nil)
                                              popover.dismiss()
                }
            
        } else {
       
    }
    }
}

//MARK:- DELGATE FROM POST TVC
extension NotificationDetailsViewController: SendingRatingFromPCTVC{
    func moreOptionFromPostTVC(sender: UIButton, id: Int, index: IndexPath, cell: PostCreationTableViewCell, isOwner: Bool, postType: GraduateDetails) {
        sharePopOver(sender: sender)
    }
    func ratingDetailFromPCTVC(ratingId: Int, starredValue: String, starCount: Int) {
       ratingDetailsAction(ratingId: ratingId, starredVlaue: starredValue, starCount: starCount)
    }
    func outsideCommentPostTVC(comment: Comments?, index: IndexPath) {
        
    }
    
    func pushingTOProfileVC(id: Int, userType: String, isOwner: Bool, postAsType: String) {
        pushToProfile(id: id, userType: userType, isOwner: isOwner, postAsType: postAsType)
    }
   
    
   
    func PCTVCRating(id: Int, starValue: String, selectedindexToReload: Int, selectionType: GraduateDetails) {
        popingBottomVC(id: id, starValue: starValue, selectedindexToReload: selectedindexToReload, selectionType: selectionType)
    }
    func feedbackActionPCTVC(commentId: Int, outsideComment: Bool, index: IndexPath) {
        pushingToThoughtsVC(commentId: commentId, outsideComment: outsideComment, index: index)
    }
   
   
    func moreOptionFromPostTVC(sender: UIButton) {
        
           
    }
    
    func fullScreenImage(vc: UIViewController) {
         currentTabBar?.setBar(hidden: true, animated: true)
              present(vc, animated: true, completion: nil)
    }
    
    func thoughtsAction() {
       
    }
    
    func ratingDetail() {
        let vc = UIStoryboard.timeLineStoryboard.instantiateRatingDetailsVC()
                      navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK:- PUSH TO PROFILE FUNC
    func pushToProfile(id: Int, userType: String, isOwner: Bool, postAsType: String) {
        if postAsType == "user" {
                    if isOwner == true {
                                 let vc = UIStoryboard.timeLineStoryboard.instantiateUserProfileVC()
                                 navigationController?.pushViewController(vc, animated: true)
                                 } else {
                                     let vc = UIStoryboard.timeLineStoryboard.instantiateOtherUserProfileVC()
                               vc.userId = id
                                            navigationController?.pushViewController(vc, animated: true)
                                 }
                    } else {
                        let vc = UIStoryboard.timeLineStoryboard.instantiateGraduateDetailsViewController()
                        vc.businessId = id
                        navigationController?.pushViewController(vc, animated: true)
                    }
    }
}
extension NotificationDetailsViewController: SendingDataFromThoughtsVC {
    func sendingData(index: IndexPath, thoughtsCount: Int, commentsData: [Comments]) {
        dataSource?.data?.comments = commentsData
        tableView.reloadData()
    }
    
    
}
