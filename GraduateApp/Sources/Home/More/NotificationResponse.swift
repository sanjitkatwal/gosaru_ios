//
//  NotificationResponse.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 10/14/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import Foundation
import ObjectMapper

class NotificationsResponse: DefaultResponse {
override func mapping(map: Map) {
  super.mapping(map: map)

}
// MARK: - GET USER NOTIFICATIONS
    class func requestToUserNotifications(param: [String : Any],showHud: Bool = true ,completionHandler:@escaping ((NotificationResponseModel?) -> Void)) {
        APIManager(urlString: GraduateUrl.userNotificationsURL,parameters: param, method: .get).handleResponse(showProgressHud: showHud,completionHandler: { (response: NotificationResponseModel) in
              print(response)
                  completionHandler(response)
              }, failureBlock: {
                completionHandler(nil)
              })
      }
    
    // MARK: - DELETE NOTIFICATIONS
    class func requestToDeleteNotifications(id: String,showHud: Bool = true ,completionHandler:@escaping ((NotificationResponseModel?) -> Void)) {
        APIManager(urlString: (GraduateUrl.deleteNotificationURL + id), method: .delete).handleResponse(showProgressHud: showHud,completionHandler: { (response: NotificationResponseModel) in
              print(response)
                  completionHandler(response)
              }, failureBlock: {
                completionHandler(nil)
              })
      }
}
 class NotificationResponseModel: DefaultResponse {

// MARK: Declaration for string constants to be used to decode and also serialize.
internal let kNotificationResponseModelDataKey: String = "data"
internal let kNotificationResponseModelLinksKey: String = "links"
internal let kNotificationResponseModelMetaKey: String = "meta"


// MARK: Properties
public var data: [NotificationData]?
public var links: TimeLineLinks?
public var meta: TimeLineMeta?



// MARK: ObjectMapper Initalizers
/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/

/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/
    override func mapping(map: Map) {
    data <- map[kNotificationResponseModelDataKey]
    links <- map[kNotificationResponseModelLinksKey]
    meta <- map[kNotificationResponseModelMetaKey]

}
}
public class NotificationData: Mappable {
    public required init?(map: Map) {
        
    }
    

// MARK: Declaration for string constants to be used to decode and also serialize.
internal let kDataCreatedAtKey: String = "created_at"
internal let kDataSenderKey: String = "sender"
internal let kDataInternalIdentifierKey: String = "id"
internal let kDataReadAtKey: String = "read_at"
internal let kDataDataKey: String = "data"
internal let kDataTypeKey: String = "type"


// MARK: Properties
public var createdAt: String?
public var sender: Sender?
public var internalIdentifier: String?
public var readAt: String?
public var data: NotificationInsideData?
public var type: String?




// MARK: ObjectMapper Initalizers
/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/


/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/
    public func mapping(map: Map) {
    createdAt <- map[kDataCreatedAtKey]
    sender <- map[kDataSenderKey]
    internalIdentifier <- map[kDataInternalIdentifierKey]
    readAt <- map[kDataReadAtKey]
    data <- map[kDataDataKey]
    type <- map[kDataTypeKey]

}
}
public class NotificationAssociatedUser: Mappable {
    public required init?(map: Map) {
        
    }
    

// MARK: Declaration for string constants to be used to decode and also serialize.
internal let kAssociatedUserNameKey: String = "name"
internal let kAssociatedUserInternalIdentifierKey: String = "id"
internal let kAssociatedUserPhoneKey: String = "phone"
internal let kAssociatedUserProfileKey: String = "profile"
internal let kAssociatedUserIsVerifiedKey: String = "is_verified"
internal let kAssociatedUserEmailKey: String = "email"
internal let kAssociatedUserUsernameKey: String = "username"


// MARK: Properties
public var name: String?
public var internalIdentifier: Int?
public var phone: String?
public var profile: NotificationProfile?
public var isVerified: Bool = false
public var email: String?
public var username: String?



// MARK: ObjectMapper Initalizers
/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/


/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/
    public func mapping(map: Map) {
    name <- map[kAssociatedUserNameKey]
    internalIdentifier <- map[kAssociatedUserInternalIdentifierKey]
    phone <- map[kAssociatedUserPhoneKey]
    profile <- map[kAssociatedUserProfileKey]
    isVerified <- map[kAssociatedUserIsVerifiedKey]
    email <- map[kAssociatedUserEmailKey]
    username <- map[kAssociatedUserUsernameKey]

}
}

public class Sender: Mappable {
    public required init?(map: Map) {
        
    }
    

// MARK: Declaration for string constants to be used to decode and also serialize.
internal let kSenderNameKey: String = "name"
internal let kSenderInternalIdentifierKey: String = "id"
internal let kSenderPhoneKey: String = "phone"
internal let kSenderProfileKey: String = "profile"
internal let kSenderIsVerifiedKey: String = "is_verified"
internal let kSenderEmailKey: String = "email"
internal let kSenderUsernameKey: String = "username"


// MARK: Properties
public var name: String?
public var internalIdentifier: Int?
public var phone: String?
public var profile: NotificationProfile?
public var isVerified: Bool = false
public var email: String?
public var username: String?



// MARK: ObjectMapper Initalizers
/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/


/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/
    public func mapping(map: Map) {
    name <- map[kSenderNameKey]
    internalIdentifier <- map[kSenderInternalIdentifierKey]
    phone <- map[kSenderPhoneKey]
    profile <- map[kSenderProfileKey]
    isVerified <- map[kSenderIsVerifiedKey]
    email <- map[kSenderEmailKey]
    username <- map[kSenderUsernameKey]

}
}
public class NotificationProfile: Mappable {
    public required init?(map: Map) {
        
    }
    

// MARK: Declaration for string constants to be used to decode and also serialize.
internal let kProfileProfileImageKey: String = "profile_image"
internal let kProfileNicknameKey: String = "nickname"
internal let kProfileGenderKey: String = "gender"


// MARK: Properties
public var profileImage: String?
public var nickname: String?
public var gender: String?



// MARK: ObjectMapper Initalizers
/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/


/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/
    public func mapping(map: Map) {
    profileImage <- map[kProfileProfileImageKey]
    nickname <- map[kProfileNicknameKey]
    gender <- map[kProfileGenderKey]

}
}
public class NotificationInsideData: Mappable {
    public required init?(map: Map) {
        
    }

    
  
    public var associatedUser: AssociatedUser?
    public var post: Post?
    public var message_title: String?
    public var message_body: String?
    public var interaction_type: String?
    public var businessId: Int?
    public var businesProfileImage: String?
    public var conversationId: Int?
    public var messageTitle: String?
    public var messageBody: String?
    public var business: NotificationBusiness?
     public func mapping(map: Map) {
        associatedUser <- map["associated_user"]
        post <- map["post"]
     
            message_title <- map["message_title"]
               message_body <- map["message_body"]
               interaction_type <- map["interaction_type"]
       businessId <- map["business_id"]
        businesProfileImage <- map["business_profile_image"]
        conversationId <- map["conversation_id"]
        messageTitle <- map["message_title"]
        messageBody <- map["message_body"]
        business <- map["business"]
    }
    
}
public class AssociatedUser: Mappable {
    public required init?(map: Map) {
        
    }
    

// MARK: Declaration for string constants to be used to decode and also serialize.
internal let kProfileProfileImageKey: String = "profile_image"
internal let kProfileNicknameKey: String = "nickname"
internal let kProfileGenderKey: String = "gender"


// MARK: Properties
public var id: Int?
public var name: String?
public var email: String?
public var phone: String?
public var username: String?
public var profile: String?
public var is_verified: Bool?



// MARK: ObjectMapper Initalizers
/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/


/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/
    public func mapping(map: Map) {
    id <- map["id"]
    name <- map["name"]
    email <- map["email"]
        phone <- map["phone"]
           username <- map["username"]
           profile <- map["profile"]
        is_verified <- map["is_verified"]

}
}
public class Post: Mappable {
    public required init?(map: Map) {
        
    }
    
    public var id: Int?
    
     public func mapping(map: Map) {
        id <- map["id"]

    }
}
public class NotificationBusiness: Mappable {
    public required init?(map: Map) {
        
    }
    
    public var id: Int?
    
     public func mapping(map: Map) {
        id <- map["id"]

    }
}
