//
//  AccountSettingsViewController.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 7/2/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import UIKit

class AccountSettingsViewController: UIViewController {
    @IBOutlet var userNameCheckBtn: LoadingButton!
    @IBOutlet weak var userNameTextField: AfterOneSecondTextField!
    @IBOutlet weak var hideFollowingSwitch: UISwitch!
    @IBOutlet weak var socialMediaSwitch: UISwitch!
    @IBOutlet weak var hideChatSwitch: UISwitch!
    @IBOutlet weak var hideAddFriendSwitch: UISwitch!
    @IBOutlet weak var hideMediaSwitch: UISwitch!
    @IBOutlet weak var updateBtn: AnimatedButton!
    @IBOutlet weak var hideFriendsSwitch: UISwitch!
    @IBOutlet weak var userNameStackView: UIStackView!
    @IBOutlet weak var deleteBtn: AnimatedButton!
     var params = [String: Any]()
//    private var indicators: [IndicatorType] = [
//           .sysDefault, .material, .ballPulse,
//           .ballPulseSync, .ballSpinFade, .lineScale,
//           .lineScalePulse, .ballBeat, .lineSpin
//       ]
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Account Settings"
        // Do any additional setup after loading the view.
        requestToUserSettings()
        addingCornerRaius()
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.navigationController?.hidesBarsOnSwipe = false
        userNameTextField.actionClosure = {
            self.userNameCheckBtn.showLoader(userInteraction: false)
            self.requestForUsernamecheck()
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        currentTabBar?.setBar(hidden: true, animated: true)
        self.navigationController?.isNavigationBarHidden = false
        navigationController?.setNavigationBarHidden(false, animated: true)
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        currentTabBar?.setBar(hidden: false, animated: true)
    }
    func addingCornerRaius() {
        
        userNameTextField.layer.cornerRadius = 12
        userNameTextField.clipsToBounds = true
        userNameTextField.layer.maskedCorners = [.layerMinXMaxYCorner,.layerMinXMinYCorner]
        userNameCheckBtn.layer.cornerRadius = 12
        userNameCheckBtn.clipsToBounds = true
        userNameCheckBtn.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMaxXMinYCorner]
        userNameCheckBtn.setImage(UIImage(named: "cross"))
        userNameCheckBtn.imageView?.contentMode = .center
        userNameCheckBtn.tintColor = UIColor.init(hexString: Appcolors.primary)
        userNameTextField.delegate = self
        if #available(iOS 13.0, *) {
        
        } else {
            userNameCheckBtn = LoadingButton(text: "Button", textColor: .black, bgColor: .white)
            // Fallback on earlier versions
        }
        userNameCheckBtn.indicator = MaterialLoadingIndicator(radius: 20, color: UIColor.init(hexString: Appcolors.primary))
      
    }
    func requestToUserSettings() {
        AccountSettingResponse.requestToUserSettings(showHud: false) { [weak self](response) in
            guard let strongSelf = self else { return }
            let hideFriends = response?.settings?.filter {
                $0.settingName == "HIDE_FRIENDS"
               }
            
            if hideFriends?[0].settingValue == "1" {
                strongSelf.hideFriendsSwitch.isOn = true
            } else {
                strongSelf.hideFriendsSwitch.isOn = false
            }
            let hideMedia = response?.settings?.filter {
             $0.settingName == "HIDE_MEDIA"
            }
            if hideMedia?[0].settingValue == "1" {
                strongSelf.hideMediaSwitch.isOn = true
            } else {
                strongSelf.hideMediaSwitch.isOn = false
            }
            let hideaddFriends = response?.settings?.filter {
                        $0.settingName == "HIDE_ADD_FRIEND"
                       }
            if hideaddFriends?[0].settingValue == "1" {
                strongSelf.hideAddFriendSwitch.isOn = true
                   
                } else {
                    strongSelf.hideAddFriendSwitch.isOn = false
                }
            let hideFollowing = response?.settings?.filter {
             $0.settingName == "HIDE_FOLLOWING"
            }
            if hideFollowing?[0].settingValue == "1" {
                strongSelf.hideFollowingSwitch.isOn = true
            } else {
                strongSelf.hideFollowingSwitch.isOn = false
            }
            let hideChat = response?.settings?.filter {
             $0.settingName == "HIDE_CHAT"
            }
            if hideChat?[0].settingValue == "1" {
                strongSelf.hideChatSwitch.isOn = true
            } else {
                strongSelf.hideChatSwitch.isOn = false
            }
            let hideSocialAccount = response?.settings?.filter {
                        $0.settingName == "HIDE_SOCIAL_ACCOUNT"
                       }
                       if hideSocialAccount?[0].settingValue == "1" {
                           strongSelf.socialMediaSwitch.isOn = true
                       } else {
                           strongSelf.socialMediaSwitch.isOn = false
                       }
            }
        }
    //MARK:- REQUEST FOR USERNAME CHECK
    func requestForUsernamecheck() {
       
            let params: [String: Any] = ["username" : userNameTextField.text ?? ""]
            AccountSettingResponse.requestToUsernameCheck(params: params, showHud: false) { [weak self](response) in
                guard let strongSelf = self else { return }
                if response?.success == true {
                    strongSelf.userNameCheckBtn.hideLoader()
                    strongSelf.userNameCheckBtn.setImage(UIImage(named: "iconCheck"))
                    strongSelf.updateBtn.backgroundColor = .white
                    
                } else if response?.success == false {
                     strongSelf.userNameCheckBtn.hideLoader()
                    
                    strongSelf.userNameCheckBtn.setImage(UIImage(named: "cross"))
                }
                
            }
            
        
    }
    
    

    @IBAction func deleteAction(_ sender: Any) {
        let vc = UIStoryboard.timeLineStoryboard.instantiateErrorCarMsgPopVC()
        vc.popupType = .deleteAccout
              // vc.delegate = self
                                                      let cardPopup = SBCardPopupViewController(contentViewController: vc)
              
                                                      cardPopup.show(onViewController: self)
    }
   
    @IBAction func hideFriendsAction(_ sender: Any) {
        //MARK: - HIDE FRIENDS API
       
        if hideFriendsSwitch.isOn == true {
              params = ["setting_name" : "HIDE_FRIENDS", "setting_value" : "1"]
        } else {
              params = ["setting_name" : "HIDE_FRIENDS", "setting_value" : "0"]
        }
        requesToUpdatSetting(params: params)
       
    }
    
    @IBAction func hideMediaAction(_ sender: Any) {
         //MARK: - HIDE MEDIA API
        if hideMediaSwitch.isOn == true {
             params = ["setting_name" : "HIDE_MEDIA", "setting_value" : "1"]
        } else {
             params = ["setting_name" : "HIDE_MEDIA", "setting_value" : "0"]
        }
        requesToUpdatSetting(params: params)
    }
    @IBAction func hideAddFriendsAction(_ sender: Any) {
        //MARK:- HIDE ADD FRIENDS
        if hideAddFriendSwitch.isOn == true {
                    params = ["setting_name" : "HIDE_ADD_FRIEND", "setting_value" : "1"]
               } else {
                    params = ["setting_name" : "HIDE_ADD_FRIEND", "setting_value" : "0"]
               }
               requesToUpdatSetting(params: params)
    }
    @IBAction func hideFollowingAction(_ sender: Any) {
        //MARK:- HIDE FOLLOWINGS
               if hideFollowingSwitch.isOn == true {
                           params = ["setting_name" : "HIDE_FOLLOWING", "setting_value" : "1"]
                      } else {
                           params = ["setting_name" : "HIDE_FOLLOWING", "setting_value" : "0"]
                      }
                      requesToUpdatSetting(params: params)
    }
    @IBAction func hideChatAction(_ sender: Any) {
        //MARK:- HIDE CHAT
                      if hideChatSwitch.isOn == true {
                                  params = ["setting_name" : "HIDE_CHAT", "setting_value" : "1"]
                             } else {
                                  params = ["setting_name" : "HIDE_CHAT", "setting_value" : "0"]
                             }
                             requesToUpdatSetting(params: params)
    }
    @IBAction func shareAction(_ sender: Any) {
        //MARK:- HIDE SHARING
                             if socialMediaSwitch.isOn == true {
                                         params = ["setting_name" : "HIDE_SOCIAL_ACCOUNT", "setting_value" : "1"]
                                    } else {
                                         params = ["setting_name" : "HIDE_SOCIAL_ACCOUNT", "setting_value" : "0"]
                                    }
                                    requesToUpdatSetting(params: params)
    }
    func requesToUpdatSetting(params: [String: Any]) {
        //MARK:- REQUESTING UPDATE USERNAME
        AccountSettingResponse.requestToUpdateUserSettings(params: params) { [weak self](response) in
            guard let strogSelf = self else { return }
            
               }
    }
    @IBAction func updateAction(_ sender: Any) {
        if updateBtn.backgroundColor == UIColor.white {
            
        
        let params: [String: Any] = ["username" : userNameTextField.text ?? ""]
        if userNameCheckBtn.currentImage == UIImage(named:"iconCheck") {
            AccountSettingResponse.requestToUsernameUpdate(params: params) { (response) in
                
            }
            }
        } else {
            
        }
    }
}
extension AccountSettingsViewController: SendingActionFromDeletePostPopUP {
    func reportNowResult(value: Bool) {
        if !value {
            
        } else {
            navigationController?.popViewController(animated: true)
        }
    }
    
    
}
extension AccountSettingsViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == userNameTextField {
            userNameCheckBtn.showLoader(userInteraction: false)
        }
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == userNameTextField {
            userNameCheckBtn.setImage(UIImage(named: "cross"))
                   userNameCheckBtn.hideLoader()
            if userNameTextField.text == "" {
                updateBtn.backgroundColor = UIColor.init(hexString: Appcolors.background)
            } else {
                updateBtn.backgroundColor = UIColor.white

            }
               }
    }
    func textFieldDidChangeSelection(_ textField: UITextField) {
        if textField == userNameTextField {
            print("")
        }
    }
}
