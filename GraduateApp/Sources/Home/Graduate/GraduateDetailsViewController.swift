//
//  GraduateDetailsViewController.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 7/15/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import UIKit
import Popover
import FlagPhoneNumber
import Alamofire
import ObjectMapper
import AVFoundation

protocol ActoinFromGradutaeDetailsVC: class {
    func action()
}
struct GraduateDetailsModel {
    var profile: CreateUniversityResponseModel?
    var portfolio: UniversityPortFolioResponseModel?
    var Offer: UniOfferModel?
    var gradSpot: UniEventResponseModel?
    var myPost: UniMyPostResponseModel?
    var numOfPage: Int
    var loadMore: Bool
}

class GraduateDetailsViewController: UIViewController {
    @IBOutlet var headerBtnLabel: UILabel!
    @IBOutlet var footerView3: UIView!
    @IBOutlet var footerView2: UIView!
    @IBOutlet var footerView: UIView!
    @IBOutlet var addPictureLabel: UILabel!
    @IBOutlet var footerBtn: AnimatedButton!
    @IBOutlet var footerLabel: UILabel!
    @IBOutlet var profileImageContainerView: UIView!
    @IBOutlet var coverImageContainerView: UIView!
    @IBOutlet var topContainerView: UIView!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var viewCountlabel: UILabel!
    @IBOutlet weak var headerBtnView: UIView!
    @IBOutlet weak var headerBtn: AnimatedButton!
    @IBOutlet weak var messageBtn: AnimatedButton!
    @IBOutlet weak var followerLabel: UILabel!
    @IBOutlet weak var followBtn: AnimatedButton!
    @IBOutlet weak var gradSpotBtn: AnimatedButton!
    @IBOutlet weak var myPostBtn: AnimatedButton!
    @IBOutlet weak var offerBtn: AnimatedButton!
    @IBOutlet weak var portFolioBtn: AnimatedButton!
    @IBOutlet weak var protileBtn: AnimatedButton!
    @IBOutlet weak var moreBtn: UIButton!
    @IBOutlet weak var coverImage: UIImageView!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet var topView: UIView!
    var newImageView = UIImageView()
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var containerView: UIView!
    var profilePictureSelected: Bool?
    var profileImageStrogage: UIImage?
    var coverImageStorage: UIImage?
    
    var followers: Int? {
        didSet {
            followerLabel.text = "\(followers ?? 0)"
        }
    }
    var postTypes: GraduateDetails? //used for the editing the post
    var editedIndex: IndexPath! //index path that is edited
    //var userData: CreateUniversityResponseModel?
    weak var delegate: ActoinFromGradutaeDetailsVC?
    var reportId: Int?
    var dataSource: GraduateDetailsModel? {
        didSet {
            ///tableView.reloadData()
//            businessId = dataSource?.profile?.data?.internalIdentifier
//            getUniversityDetails()
        }
    }
    
    
    var detailsType: GraduateDetails? {
        didSet {
            tableView.reloadData()
        }
    }
    var businessId: Int? {
        didSet {
            getUniversityDetails()
        }
    }
    var selectedBtn: Bool?
    var followState: Bool?
    var popupTableView = UITableView(frame: CGRect(x: 0, y: 0, width: 90, height: 120))
    var texts = ["Share", "Edit", "Delete"]
    var popover: Popover!
    fileprivate var popoverOptions: [PopoverOption] = [
        .type(.left),
        .blackOverlayColor(UIColor(white: 0.0, alpha: 0.6))
    ]
    var listController: FPNCountryListViewController = FPNCountryListViewController(style: .grouped)
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        dataSource = GraduateDetailsModel(profile: nil, portfolio: nil, Offer: nil, gradSpot: nil, myPost: nil, numOfPage: 1, loadMore: false)
        detailsType = .profile
        initialDetailsData()
        protileBtn.backgroundColor = UIColor.init(hexString: Appcolors.buttons)
        protileBtn.setTitleColor(.white, for: .normal)
        registeringViews()
        nameLabel.lineBreakMode = .byWordWrapping
        coverImage.applyshadowWithCorner(containerView: coverImageContainerView, cornerRadious: 0)
        profileImage.applyshadowWithCorner(containerView: profileImageContainerView, cornerRadious: 8)
        tableView.tableHeaderView = topView
        tableView.dataSource = self
        tableView.delegate = self
        tableView.reloadData()
        flagPhoneNumbersetup()
        headerBtnView.isHidden = true
        if profileImage.image?.size.width == 0 {
            profileImage.isHidden = true
            addPictureLabel.isHidden = false
        } else {
            profileImage.isHidden = false
            addPictureLabel.isHidden = true
        }
        tableView.layoutTableHeaderView()
        addingBarButton()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        moreBtn.setImage(UIImage(named: "more-3"), for: .normal)
        moreBtn.imageView?.contentMode = .center
        self.navigationController?.isNavigationBarHidden = false
        currentTabBar?.setBar(hidden: true, animated: true)
        coverImage.dropShadow()
        profileImage.layer.cornerRadius = 8
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.isNavigationBarHidden = false
        currentTabBar?.setBar(hidden: true, animated: true)
    }
    func addingBarButton() {
//        let button: UIButton = UIButton(type: UIButton.ButtonType.custom) as! UIButton
//                //set image for button
//        button.imageView?.tintColor = UIColor.init(hexString: Appcolors.buttons)
//        button.setImage(UIImage(named: "iconSetting"), for: UIControl.State.normal)
//                //add function for button
//        button.addTarget(self, action: #selector(addTapped), for: UIControl.Event.touchUpInside)
//                //set frame
//        button.frame = CGRect(x: 0, y: 0, width: 53, height: 31)
//
//                let barButton = UIBarButtonItem(customView: button)
//                //assign button to navigationbar
//                self.navigationItem.rightBarButtonItem = barButton
        
        let viewFN = UIView(frame: CGRect(x: 0, y: 0, width: 40,height: 40))
            viewFN.backgroundColor = UIColor.clear
        let button1 = UIButton(frame: CGRect(x: 0,y: 8, width: 40, height: 20))
        button1.setImage(UIImage(named: "iconSetting"), for: UIControl.State.normal)
        button1.imageView?.contentMode = .center
       // button1.setTitle("one", forState: .normal)

        button1.addTarget(self, action: #selector(self.addTapped), for: UIControl.Event.touchUpInside)

    

        viewFN.addSubview(button1)
      


        let rightBarButton = UIBarButtonItem(customView: viewFN)
        self.navigationItem.rightBarButtonItem = rightBarButton


    }
    @objc func addTapped(sender: AnyObject) {
        let vc = UIStoryboard.graduateStoryboard.instantiateGraduateSettingVC()
        vc.businessId = dataSource?.profile?.data?.internalIdentifier
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func registeringViews() {
        tableView.register(UINib(nibName: "ProfileTableViewCell", bundle: nil), forCellReuseIdentifier: "ProfileTableViewCell")
        tableView.register(UINib(nibName: "PortFolioTableViewCell", bundle: nil), forCellReuseIdentifier: "PortFolioTableViewCell")
        tableView.register(UINib(nibName: "EventTableViewCell", bundle: nil), forCellReuseIdentifier: "EventTableViewCell")
        tableView.register(UINib(nibName: "DiscountTableViewCell", bundle: nil), forCellReuseIdentifier: "DiscountTableViewCell")
        tableView.register(UINib(nibName: "LiveTableViewCell", bundle: nil), forCellReuseIdentifier: "LiveTableViewCell")
        tableView.register(UINib(nibName: "PostCreationTableViewCell", bundle: nil), forCellReuseIdentifier: "PostCreationTableViewCell")
        tableView.reloadData()
    }
    func initialDetailsData() {
        self.title = dataSource?.profile?.data?.businessName
        profileImage.setURLImage(imageURL: (GraduateUrl.imageUrl + (dataSource?.profile?.data?.businessProfileImage ?? "")))
        coverImage.setURLImage(imageURL: (GraduateUrl.imageUrl + (dataSource?.profile?.data?.businessCoverImage ?? "")))
        nameLabel.text = dataSource?.profile?.data?.businessName
        
        followerLabel.text = "\(dataSource?.profile?.data?.followsCount ?? 0)"
        // viewCountlabel.text = "\(dataSource?.profile?.data?.viewsCount ?? 0)"
        typeLabel.text = dataSource?.profile?.data?.businessType
        
    }
    func addingGestureInaddPictureTxt() {
        let labelTapGesture = UITapGestureRecognizer(target:self,action:#selector(self.doSomethingOnTap))
        nameLabel.isUserInteractionEnabled = true
        nameLabel.addGestureRecognizer(labelTapGesture)
        
    }
    
    //MARK:- POPUP
    func popUp(type: ErrorType, message: String) {
        let vc = UIStoryboard.timeLineStoryboard.instantiateErrorCarMsgPopVC()
        vc.popupType = type
        vc.messageString = message
        vc.delegaet = self
        let cardPopup = SBCardPopupViewController(contentViewController: vc)
        cardPopup.show(onViewController: self)
    }
    
    @IBAction func tappingAddPicture(_ sender: Any) {
        addPictureLabel.isUserInteractionEnabled = true
        let vc = UIStoryboard.timeLineStoryboard.instantiateSelectionBottomPopUpVC()
        vc.delegate = self
        vc.textFieldType = "Change Profile Picture"
        vc.height = 100
        vc.topCornerRadius = 15
        vc.presentDuration = 0.25
        vc.dismissDuration = 0.25
        vc.shouldDismissInteractivelty = true
        // popupVC.popupDelegate = self
        present(vc, animated: true, completion: nil)
    }
    @objc func doSomethingOnTap() {
        
    }
    func flagPhoneNumbersetup() {
        //        p.setFlag(key: .NP)
        //               countryTxtField.displayMode = .list
        //               listController.setup(repository: countryTxtField.countryRepository)
        //
        //               listController.didSelect = { [weak self] country in
        //                   self?.countryTxtField.setFlag(countryCode: country.code)
        //               }
        //               self.countryTxtField.set(phoneNumber: "20394203948")
        //               countryTxtField.attributedPlaceholder = "Phone(optional)".toAttributed(alignment: .center)
        //
        //               countryTxtField.delegate = self
    }
    func popUp(type: ErrorType, messsage: String) {
        let vc = UIStoryboard.timeLineStoryboard.instantiateErrorCarMsgPopVC()
        vc.popupType = type
        vc.delegaet = self
        vc.messageString = messsage
        let cardPopup = SBCardPopupViewController(contentViewController: vc)
        cardPopup.show(onViewController: self)
    }
    
    
    //MARK:- GET THE DETAILS OF THE UNIVERSITY
    func getUniversityDetails() {
        print(businessId)
        UniversityResponse.requestToUniversityDetails(userid: businessId ?? 0) { [weak self](response) in
            guard let strongSelf = self else { return }
            strongSelf.dataSource?.profile = response
           
            strongSelf.nameLabel.text = strongSelf.dataSource?.profile?.data?.businessName
            strongSelf.followers = strongSelf.dataSource?.profile?.data?.followsCount ?? 0
            strongSelf.followerLabel.text = "\(strongSelf.followers ?? 0)"
            strongSelf.viewCountlabel.text = strongSelf.dataSource?.profile?.data?.viewsCount ?? ""
            strongSelf.typeLabel.text = strongSelf.dataSource?.profile?.data?.businessType
            strongSelf.tableView.reloadData()
            if strongSelf.dataSource?.profile?.data?.businessCoverImage != nil {
                strongSelf.coverImage.setURLImage(imageURL: (GraduateUrl.imageUrl + (strongSelf.dataSource?.profile?.data?.businessCoverImage ?? "")))
                
            } else {
                
            }
            if strongSelf.dataSource?.profile?.data?.businessProfileImage != nil {
                strongSelf.profileImage.setURLImage(imageURL: (GraduateUrl.imageUrl + (strongSelf.dataSource?.profile?.data?.businessProfileImage ?? "")))
                strongSelf.addPictureLabel.isHidden = true
            } else {
                strongSelf.addPictureLabel.isHidden = false
            }
            strongSelf.title = strongSelf.dataSource?.profile?.data?.businessName
        }
    }
    @IBAction func moreBtnAction(_ sender: Any) {
        let vc = UIStoryboard.timeLineStoryboard.instantiateSelectionBottomPopUpVC()
        vc.delegate = self
        vc.textFieldType = "Graduate Details More Selection"
        vc.height = 100
        vc.topCornerRadius = 15
        vc.presentDuration = 0.25
        vc.dismissDuration = 0.25
        vc.shouldDismissInteractivelty = true
        // popupVC.popupDelegate = self
        present(vc, animated: true, completion: nil)
    }
    @IBAction func headerBtnAction(_ sender: Any) {
        switch detailsType {
        case .portfolio:
            let vc = UIStoryboard.timeLineStoryboard.instantiateCreateSpotViewController()
            vc.delegate = self
            vc.vcType = .addPortFolio
            vc.delegate = self
            vc.profileData = dataSource?.profile
            vc.userID = dataSource?.profile?.data?.internalIdentifier
            navigationController?.pushViewController(vc, animated: true)
        case .offer:
            let vc = UIStoryboard.timeLineStoryboard.instantiateCreateSpotViewController()
            vc.vcType = .createDeal
            vc.delegate = self
            print(dataSource?.profile?.data?.businessName)
            print(dataSource?.profile?.data?.businessProfileImage)
            vc.postFromInsideData = dataSource?.profile?.data
            vc.profileData = dataSource?.profile
            vc.profileImage = dataSource?.profile?.data?.businessProfileImage
            vc.universityName = dataSource?.profile?.data?.businessName
            vc.userID = dataSource?.profile?.data?.internalIdentifier
            navigationController?.pushViewController(vc, animated: true)
        case .gradSpot:
            let vc = UIStoryboard.timeLineStoryboard.instantiateCreateSpotViewController()
            vc.vcType = .createEvent
            vc.delegate = self
            vc.profileData = dataSource?.profile
            print(dataSource?.profile?.data?.internalIdentifier)
            vc.businesName = dataSource?.profile?.data?.businessName
            vc.businessProfilePic = dataSource?.profile?.data?.businessProfileImage
            vc.profileImage = dataSource?.profile?.data?.businessProfileImage
            vc.universityName = dataSource?.profile?.data?.businessName
            vc.businesId = dataSource?.profile?.data?.internalIdentifier
            vc.userID = dataSource?.profile?.data?.internalIdentifier
            navigationController?.pushViewController(vc, animated: true)
        case .myPost:
            let vc = UIStoryboard.timeLineStoryboard.instantiateCreateSpotViewController()
            vc.vcType = .createPost
            vc.delegate = self
            vc.profileData = dataSource?.profile
            vc.businesName = dataSource?.profile?.data?.businessName
            vc.businessProfilePic = dataSource?.profile?.data?.businessProfileImage
            vc.profileImage = dataSource?.profile?.data?.businessProfileImage
            vc.universityName = dataSource?.profile?.data?.businessName
            vc.businesId = dataSource?.profile?.data?.internalIdentifier
            vc.userID = dataSource?.profile?.data?.internalIdentifier
            navigationController?.pushViewController(vc, animated: true)
        default:
            break
        }
        if headerBtn.titleLabel?.text == "Create Uni" {
            let vc = UIStoryboard.timeLineStoryboard.instantiateCreateUniversityVC()
            navigationController?.pushViewController(vc, animated: true)
            
        } else if headerBtn.titleLabel?.text == "Create Post" {
            let vc = UIStoryboard.timeLineStoryboard.instantiateCreateSpotViewController()
            navigationController?.pushViewController(vc, animated: true)
        }
    }
    func thoughtDetails(commentid: Int,index: IndexPath) {
        let vc = UIStoryboard.timeLineStoryboard.instantiateThoughtsVC()
        vc.commentId = commentid
        vc.index = index
        vc.delegate = self
        navigationController?.pushViewController(vc, animated: true)
    }
    func pusingToProfileVC(id: Int) {
        let vc = UIStoryboard.timeLineStoryboard.instantiateUserProfileVC()
        navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func buttonAction(_ sender: UIButton) {
        sender.backgroundColor = .red
        switch sender.tag {
        case 0:
            headerBtnView.isHidden = true
            tableView.layoutTableHeaderView()
            portFolioBtn.backgroundColor = .white
            portFolioBtn.setTitleColor(UIColor.init(hexString: Appcolors.buttons), for:.normal)
            offerBtn.backgroundColor = .white
            offerBtn.setTitleColor(UIColor.init(hexString: Appcolors.buttons), for:.normal)
            gradSpotBtn.backgroundColor = .white
            gradSpotBtn.setTitleColor(UIColor.init(hexString: Appcolors.buttons), for:.normal)
            
            myPostBtn.backgroundColor = .white
            myPostBtn.setTitleColor(UIColor.init(hexString: Appcolors.buttons), for:.normal)
            
            detailsType = .profile
            
            protileBtn.setTitleColor(.white, for: .normal)
            protileBtn.backgroundColor = UIColor.init(hexString: Appcolors.buttons)
            selectedBtn = true
            tableView.reloadData()
        case 1:
            //MARK:- PORTFOLIO BTN TAPPED
            
            detailsType = .portfolio
            
            selectedBtn = true
            protileBtn.backgroundColor = .white
            protileBtn.setTitleColor(UIColor.init(hexString: Appcolors.buttons), for: .normal)
            offerBtn.backgroundColor = .white
            offerBtn.setTitleColor(UIColor.init(hexString: Appcolors.buttons), for: .normal)
            gradSpotBtn.backgroundColor = .white
            gradSpotBtn.setTitleColor(UIColor.init(hexString: Appcolors.buttons), for: .normal)
            myPostBtn.backgroundColor = .white
            myPostBtn.setTitleColor(UIColor.init(hexString: Appcolors.buttons), for: .normal)
            portFolioBtn.backgroundColor = UIColor.init(hexString: Appcolors.buttons)
            portFolioBtn.setTitleColor(.white, for: .normal)
            requestingForPortfolio(pageNo: 1, isLoadMore: false, showHud: true)
            tableView.reloadData()
        case 2:
            //MARK:- OFFER BTN TAPPED
            detailsType = .offer
            requestingForOffers(pageNo: 1, isLoadMore: false, showHud: true)
            sender.setTitleColor(UIColor.init(hexString: Appcolors.buttons), for: .selected)
            portFolioBtn.backgroundColor = .white
            portFolioBtn.setTitleColor(UIColor.init(hexString: Appcolors.buttons), for: .normal)
            protileBtn.backgroundColor = .white
            protileBtn.setTitleColor(UIColor.init(hexString: Appcolors.buttons), for: .normal)
            gradSpotBtn.backgroundColor = .white
            gradSpotBtn.setTitleColor(UIColor.init(hexString: Appcolors.buttons), for: .normal)
            myPostBtn.backgroundColor = .white
            myPostBtn.setTitleColor(UIColor.init(hexString: Appcolors.buttons), for: .normal)
            offerBtn.backgroundColor = UIColor.init(hexString: Appcolors.buttons)
            offerBtn.setTitleColor(.white, for: .normal)
           
            tableView.reloadData()
        case 3:
            //MARK:- EVENT BTN TAPPED
            detailsType = .gradSpot
            sender.setTitleColor(UIColor.init(hexString: Appcolors.buttons), for: .selected)
            portFolioBtn.backgroundColor = .white
            portFolioBtn.setTitleColor(UIColor.init(hexString: Appcolors.buttons), for: .normal)
            protileBtn.backgroundColor = .white
            protileBtn.setTitleColor(UIColor.init(hexString: Appcolors.buttons), for: .normal)
            offerBtn.backgroundColor = .white
            offerBtn.setTitleColor(UIColor.init(hexString: Appcolors.buttons), for: .normal)
            myPostBtn.backgroundColor = .white
            myPostBtn.setTitleColor(UIColor.init(hexString: Appcolors.buttons), for: .normal)
            gradSpotBtn.backgroundColor = UIColor.init(hexString: Appcolors.buttons)
            gradSpotBtn.setTitleColor(.white, for: .normal)
            requestingForEvents(pageNo: 1, isLoadMore: false, showHud: true)
            tableView.reloadData()
        case 4:
            //MARK:- MYPOST BTN TAPPED
            detailsType = .myPost
            sender.setTitleColor(UIColor.init(hexString: Appcolors.buttons), for: .selected)
            portFolioBtn.backgroundColor = .white
            portFolioBtn.setTitleColor(UIColor.init(hexString: Appcolors.buttons), for: .normal)
            protileBtn.backgroundColor = .white
            protileBtn.setTitleColor(UIColor.init(hexString: Appcolors.buttons), for: .normal)
            offerBtn.backgroundColor = .white
            offerBtn.setTitleColor(UIColor.init(hexString: Appcolors.buttons), for: .normal)
            gradSpotBtn.backgroundColor = .white
            gradSpotBtn.setTitleColor(UIColor.init(hexString: Appcolors.buttons), for: .normal)
            myPostBtn.setTitleColor(.white, for: .normal)
            myPostBtn.backgroundColor = UIColor.init(hexString: Appcolors.buttons)
            
            requestingForMyPost(pageNo: 1, isLoadMore: false, showHud: true)
            tableView.reloadData()
        default:
            break
        }
        
    }
    
    @IBAction func messageAction(_ sender: Any) {
        let vc = UIStoryboard.moreStoryboard.instantiateMessageVC()
        vc.id = dataSource?.profile?.data?.internalIdentifier
                   navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func followAction(_ sender: Any) {
        if followBtn.titleLabel?.text == "Unfollow" {
            followBtn.backgroundColor = .white
            followBtn.setTitleColor(UIColor.init(hexString: Appcolors.buttons), for: .normal)
            followBtn.setTitle("Follow", for: .normal)
            followUniversity(id: dataSource?.profile?.data?.internalIdentifier ?? 0)
            followers! -= 1
            
        } else if followBtn.titleLabel?.text == "Follow" {
            followBtn.backgroundColor = UIColor.init(hexString: Appcolors.buttons)
            followBtn.setTitleColor(.white, for: .normal)
            followBtn.setTitle("Unfollow", for: .normal)
            unfollowUniversity(id: dataSource?.profile?.data?.internalIdentifier ?? 0)
            followers! += 1
        }
    }
    @IBAction func tapingCoverImage(_ sender: Any) {
        
    }
    func coverImageFullScreen(){
        // let imageView = sender.view as! UIImageView
        newImageView = UIImageView(image: coverImage.image)
        newImageView.frame = UIScreen.main.bounds
        newImageView.backgroundColor = .black
        newImageView.contentMode = .scaleAspectFit
        newImageView.isUserInteractionEnabled = true
        
        self.view.addSubview(newImageView)
        self.navigationController?.isNavigationBarHidden = true
        self.tabBarController?.tabBar.isHidden = true
        let closeBtn = UIButton()
        closeBtn.frame = CGRect(x: 16, y: 16, width: 25, height: 25)
        closeBtn.setImage(UIImage(named: "cross"), for: .normal)
        closeBtn.addTarget(self, action: #selector(dismissFullscreenImage), for: .touchUpInside)
        newImageView.addSubview(closeBtn)
    }
    @objc func dismissFullscreenImage(sender: UIButton) {
        self.navigationController?.isNavigationBarHidden = false
        self.tabBarController?.tabBar.isHidden = false
        newImageView.removeFromSuperview()
    }
    @IBAction func footerBtnAction(_ sender: Any) {
        switch detailsType {
        case .portfolio:
            let vc = UIStoryboard.timeLineStoryboard.instantiateCreateSpotViewController()
            vc.vcType = .addPortFolio
            vc.userID = dataSource?.profile?.data?.internalIdentifier
            self.navigationController?.pushViewController(vc, animated: true)
            //case .offer:
            
            
        default:
            break
        }
    }
    //MARK:- FOLLOW UNIVERSITY
    func followUniversity(id: Int) {
        GraduateUniversityResponse.requestToFollowUniversity(userId: id) { (response) in
            
        }
    }
    //MARK:- UNFOLLOW API FOR UNIVERSITY
    func unfollowUniversity(id: Int) {
        GraduateUniversityResponse.requestToUnfollowUniversity(userId: id) { [weak self](response) in
            
            
        }
    }
    
    func profileImageFullScreen(){
        // let imageView = sender.view as! UIImageView
        newImageView = UIImageView(image: profileImage.image)
        newImageView.frame = UIScreen.main.bounds
        newImageView.backgroundColor = .black
        newImageView.contentMode = .scaleAspectFit
        newImageView.isUserInteractionEnabled = true
        
        self.view.addSubview(newImageView)
        self.navigationController?.isNavigationBarHidden = true
        self.tabBarController?.tabBar.isHidden = true
        let closeBtn = UIButton()
        closeBtn.frame = CGRect(x: 16, y: 16, width: 25, height: 25)
        closeBtn.setImage(UIImage(named: "cross"), for: .normal)
        closeBtn.addTarget(self, action: #selector(dismissFullscreenProfileImage), for: .touchUpInside)
        newImageView.addSubview(closeBtn)
    }
    @objc func dismissFullscreenProfileImage(sender: UIButton) {
        self.navigationController?.isNavigationBarHidden = false
        self.tabBarController?.tabBar.isHidden = false
        newImageView.removeFromSuperview()
    }
    func sharePopOver(sender: UIButton) {
        popupTableView.reloadData()
        if texts.count == 2 {
            popupTableView.frame = CGRect(x: 0, y: 0, width: 90, height: 80)
            
        } else {
            popupTableView.frame = CGRect(x: 0, y: 0, width: 90, height: 120)
            
        }
        popupTableView.delegate = self
        popupTableView.dataSource = self
        popupTableView.isScrollEnabled = false
        self.popover = Popover(options: self.popoverOptions)
        self.popover.willShowHandler = {
            print("willShowHandler")
        }
        self.popover.didShowHandler = {
            print("didDismissHandler")
        }
        self.popover.willDismissHandler = {
            print("willDismissHandler")
        }
        self.popover.didDismissHandler = {
            print("didDismissHandler")
        }
        
        self.popover.show(popupTableView, fromView: sender)
    }
    
    //MARK:- PORTFOLIO REQUEST
    func requestingForPortfolio(pageNo: Int,isLoadMore: Bool = false, showHud: Bool = true) {
        let params: [String: Any]  = ["page" : pageNo,"poster_id" : "\(dataSource?.profile?.data?.internalIdentifier ?? 0)", "only_portfolio_post" : "true"]
        GraduateDetailsResponse.requestToUniPortfolio(param: params, url: GraduateUrl.uniPortFolioURL) { [weak self](response) in
            guard let strongSelf = self else { return }
            strongSelf.dataSource?.portfolio = response
            strongSelf.tableView.reloadData()
            strongSelf.headerBtnView.isHidden = false
            strongSelf.headerBtn.setTitle("Add PortFolio")
            if strongSelf.dataSource?.portfolio?.data?.count == 0{
                if strongSelf.dataSource?.profile?.data?.isOwnBusiness == false {
                    strongSelf.headerBtn.isHidden = true
                    strongSelf.headerBtnLabel.isHidden = false
                    strongSelf.tableView.layoutTableHeaderView()
                }
                strongSelf.headerBtnLabel.isHidden = false
                strongSelf.headerBtnLabel.text = "No PortFolio Found"
                strongSelf.tableView.layoutTableHeaderView()
            } else {
                if strongSelf.dataSource?.profile?.data?.isOwnBusiness == false {
                    strongSelf.headerBtnView.isHidden = true
                    strongSelf.tableView.layoutTableHeaderView()
                } else {
                    strongSelf.headerBtnLabel.isHidden = true
                    strongSelf.tableView.layoutTableHeaderView()
                }
            }
        }
    }
    //MARK:- OFFER REQUEST
    func requestingForOffers(pageNo: Int,isLoadMore: Bool = false, showHud: Bool = true) {
        let params: [String: Any]  = ["page" : pageNo,"poster_id" : "\(dataSource?.profile?.data?.internalIdentifier ?? 0)", "only_offer_post" : "true"]
        GraduateDetailsResponse.requestToUniOffers(param: params, url: GraduateUrl.uniPortFolioURL) { [weak self](response) in
            guard let strongSelf = self else { return }
            strongSelf.dataSource?.Offer = response
            strongSelf.tableView.reloadData()
            strongSelf.headerBtnView.isHidden = false
            strongSelf.headerBtn.setTitle("Add Offer")
            if strongSelf.dataSource?.Offer?.data?.count == 0 {
                strongSelf.headerBtnLabel.isHidden = false
                strongSelf.headerBtnLabel.text = "No Offers Found"
                strongSelf.tableView.layoutTableHeaderView()
            } else {
                if strongSelf.dataSource?.profile?.data?.isOwnBusiness == false {
                    strongSelf.headerBtnView.isHidden = true
                    strongSelf.tableView.layoutTableHeaderView()
                } else {
                    strongSelf.headerBtnLabel.isHidden = true
                    strongSelf.tableView.layoutTableHeaderView()
                }
            }
        }
        tableView.reloadData()
    }
    //MARK:- EVENT REQUEST
    func requestingForEvents(pageNo: Int,isLoadMore: Bool = false, showHud: Bool = true) {
        let params: [String: Any]  = ["page" : pageNo,"poster_id" : "\(dataSource?.profile?.data?.internalIdentifier ?? 0)", "only_event_post" : "true"]
        GraduateDetailsResponse.requestToEvents(param: params, url: GraduateUrl.uniPortFolioURL) { [weak self](response) in
            guard let strongSelf = self else { return }
            strongSelf.dataSource?.gradSpot = response
            strongSelf.tableView.reloadData()
            strongSelf.headerBtnView.isHidden = false
            strongSelf.headerBtn.setTitle("Add Event")
            if strongSelf.dataSource?.gradSpot?.data?.count == 0 {
                strongSelf.headerBtnLabel.isHidden = false
                strongSelf.headerBtnLabel.text = "No Events Found"
                strongSelf.tableView.layoutTableHeaderView()
            } else {
                if strongSelf.dataSource?.profile?.data?.isOwnBusiness == false {
                    strongSelf.headerBtnView.isHidden = true
                    strongSelf.tableView.layoutTableHeaderView()
                } else {
                    strongSelf.headerBtnLabel.isHidden = true
                    strongSelf.tableView.layoutTableHeaderView()
                }
                
            }
        }
        
    }
    //MARK:- POST REQUEST
    func requestingForMyPost(pageNo: Int,isLoadMore: Bool = false, showHud: Bool = true) {
        let params: [String: Any]  = ["page" : pageNo,"poster_id" : "\(dataSource?.profile?.data?.internalIdentifier ?? 0)"]
        GraduateDetailsResponse.requestToMypost(param: params, url: GraduateUrl.uniPortFolioURL) { [weak self](response) in
            guard let strongSelf = self else { return }
           // strongSelf.dataSource?.myPost = response
            strongSelf.headerBtnView.isHidden = false
            strongSelf.headerBtn.setTitle("Add Post")
            if strongSelf.dataSource?.myPost?.data?.count == 0 {
                strongSelf.headerBtnLabel.isHidden = false
                
                
                strongSelf.headerBtnLabel.text = "No Posts Found"
                strongSelf.tableView.layoutTableHeaderView()
            } else {
                if strongSelf.dataSource?.profile?.data?.isOwnBusiness == false {
                    strongSelf.headerBtnView.isHidden = true
                    strongSelf.tableView.layoutTableHeaderView()
                } else {
                    strongSelf.headerBtnLabel.isHidden = true
                    strongSelf.tableView.layoutTableHeaderView()
                }
            }
            
            strongSelf.tableView.tableFooterView = nil
            strongSelf.tableView.tableFooterView?.isHidden = true
            if !isLoadMore {
                strongSelf.dataSource?.myPost = response
                
                strongSelf.tableView.reloadData()
            } else {
                if let allData = response?.data {
                    print(allData)
                    for data in allData {
                        strongSelf.dataSource?.myPost?.data?.append(data)
                    }
                    strongSelf.tableView.reloadData()
                }
            }
            if strongSelf.dataSource?.myPost?.data?.count ?? 0 < response?.meta?.total ?? 0 {
                strongSelf.dataSource?.loadMore = true
                strongSelf.dataSource?.numOfPage += 1
            } else {
                strongSelf.dataSource?.loadMore = false
            }
        }
    }
//MARK:- DELETE PORTFOLIO
    func deletePortfolio(id: Int) {
        GraduateDetailsResponse.requestToDeletePortfolio(id: id, url: "") { [weak self](response) in
            guard let strongSelf = self else { return }
            strongSelf.requestingForPortfolio(pageNo: 1, isLoadMore: false, showHud: true)
           // strongSelf.popUp(type: .portfolioDeleted, message: "")
        }
    }
    
    //MARK:- DELETE THE UNIVERSITY PROFILE
    func requestForUniversityDelte() {
        let url = ((GraduateUrl.universityDetalsURL) + ("\(dataSource?.profile?.data?.internalIdentifier ?? 0)"))
        print(url)
        GraduateDetailsResponse.requestToDeleteUni(url: url) { [weak self](response) in
            guard let strongSelf = self else { return }
            strongSelf.navigationController?.popViewController(animated: true)
            strongSelf.delegate?.action()
        }
    }
//MARK:- DELTE OFFER
    func requestToDeleteOFfer(id: Int) {
        GraduateDetailsResponse.requestToDeleteOffer(id: id, url: "") { [weak self](response) in
            guard let strongSelf = self else { return }
            strongSelf.requestingForOffers(pageNo: 1, isLoadMore: false, showHud: true)
            //strongSelf.popUp(type: .offerDeleted, message: "")
        }
    }
    //MARK:- REQUEST TO DELTE POST
//        func requestToDeletePost() {
//            WatchResponse.requestToDeletePost(id: dataSource?.watchData?.data?[editedIndex.row].internalIdentifier ?? 0,showHud: true) { [weak self](response) in
//                guard let strongSelf = self else { return }
//                strongSelf.requestingToVides(pageNo: 1, isLoadMore: false, showHud: true)
//            }
//        }
    //MARK:- DELTE EVENT
    func requestToDeleteEvent(id: Int) {
            GraduateDetailsResponse.requestToDeleteOffer(id: id, url: "") { [weak self](response) in
                guard let strongSelf = self else { return }
                strongSelf.requestingForEvents(pageNo: 1, isLoadMore: false, showHud: true)
                //strongSelf.popUp(type: .offerDeleted, message: "")
            }
        }
    //MARK:- REQUESTING FOR THE PROFILE UPDATE
    func requestingForUniupdateProfile(params: [String : Any]) {
        var coverimage: Data?
        var profileImages: Data?
        //  let newparams: [String : Any] = params
        let params: [String : String] = ["business_name" : dataSource?.profile?.data?.businessName ?? "", "business_type" :dataSource?.profile?.data?.businessType ?? "", "address_string" : dataSource?.profile?.data?.addressString ?? "", "phone" : dataSource?.profile?.data?.phone ?? "", "email" : dataSource?.profile?.data?.email ?? "", "website_link" : dataSource?.profile?.data?.websiteLink ?? "", "about_business" : dataSource?.profile?.data?.aboutBusiness ?? "", "facebook" : dataSource?.profile?.data?.facebook ?? "", "_method" : "PUT"]
        if coverImage.image?.size.width != nil {
            coverimage = coverImage.image?.jpegData(compressionQuality: 0.2)!
        } else {
            coverimage = nil
        }
        if profileImage.image?.size.width != nil {
            profileImages = profileImage.image?.jpegData(compressionQuality: 0.2)!
        } else {
            profileImages = nil
        }
        //        UniversityResponse.requestToUpdateUniPro(coverImageName: "cover_image", profileImageName: "profile_image", coverImage: coverimage!, profileImage: profileImages!, params: newparams, userid: userData?.data?.internalIdentifier ?? 0) { (response) in
        //            print(response?.data?.email)
        //            print(response?.success)
        //        }
        var header: [String : Any]
        if let token = UserDefaultsHandler.getUDValue(key: UDkey.token) as? String {
            header = ["Authorization": "Bearer \(token)", "Accept": "application/json"]
        } else {
            header = ["Accept": "application/json"]
            
        }
        
        
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            
            
            
            if coverimage != nil {
                multipartFormData.append(coverimage!, withName: "cover_image", fileName: "coverImage2.jpeg", mimeType: "coverImage/jpg")
            }
            if profileImages != nil {
                multipartFormData.append(profileImages! , withName: "profile_image",fileName: "pforileImage2.jpg", mimeType: "image/jpeg")
                
            }
            print(GraduateUrl.universityDetalsURL,self.dataSource?.profile?.data?.internalIdentifier,GraduateUrl.universityDetalsURL + "\(self.dataSource?.profile?.data?.internalIdentifier ?? 0)")
            ProgressHud.showProgressHUD()
            for (key, value) in params {
                print(key ,value)
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            } //Optional for extra parameters
        }, to: GraduateUrl.universityDetalsURL + "\(self.dataSource?.profile?.data?.internalIdentifier ?? 0)", method: .post, headers: header as? HTTPHeaders, encodingCompletion: {
            encodingResult in
            switch encodingResult {
            case .success(let upload, _, _):
                upload.responseJSON { [weak self]response in
                    guard let strongSelf = self else {return }
                    if let jsonResponse = response.result.value as? [String: Any] {
                        ProgressHud.hideProgressHUD()
                        strongSelf.popUp(type: .profileUpdated, messsage: "")
                        print(jsonResponse)
                    }
                }
            case .failure(let encodingError):
                ProgressHud.hideProgressHUD()
                print(encodingError)
            }
        })
        //                    { (result) in
        //
        //                        switch result {
        //                        case .success(let upload, _, _):
        //
        //                            upload.uploadProgress(closure: { [weak self](progress) in
        //                                guard let strongSelf = self else {return}
        //                                print("Upload Progress: \(progress.fractionCompleted)")
        //                            })
        //                            upload.responseJSON { (response) in
        //                                if let jsonData = response.result.value as? NSDictionary {
        //                                    ProgressHud.hideProgressHUD()
        //                                    print(jsonData)
        //                                }
        //                            }
        ////                            upload.responseObject { [weak self](response: DataResponse<CreateUniversityResponseModel>) in
        ////                                guard let strongSelf = self else { return }
        ////                                print(response.result.value?.data?.email)
        ////                                if let data = response.result.value as? NSDictionary {
        ////                                    print(data)
        ////                                }
        ////                                ProgressHud.hideProgressHUD()
        ////
        ////                                 //strongSelf.ErrorMessagePopup(message: "", popUptype: .universityRegistered)
        ////                            }
        //        //
        //                        case .failure(let encodingError):
        //
        //                            print(encodingError)
        //
        //                    }
        //                }
    }
    //    func updateTheUniversityProfile() {
    //        UniversityResponse.requestToUpdateUniPro(params: , userid: userData?.data?.internalIdentifier) { (response) in
    //            guard let strongSelf = self else { return }
    //        }
    //    }
    func addFooter() {
        tableView.tableFooterView = footerView3
    }
}
extension GraduateDetailsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == popupTableView {
            return texts.count
        } else if tableView == tableView {
            switch detailsType {
            case .profile:
                return 1
            case .portfolio:
                if dataSource?.portfolio?.data?.count == 0 {
                    ////                    footerBtn.setTitle("Add PortFolio")
                    ////                    footerLabel.text = "No PortFolio Found"
                    ////                    tableView.tableFooterView = footerView
                    //                      return dataSource?.portfolio?.data?.count ?? 0
                    
                } else {
                    // tableView.tableFooterView = UIView()
                    return dataSource?.portfolio?.data?.count ?? 0
                }
            case .offer:
                
                return dataSource?.Offer?.data?.count ?? 0
                
            case .gradSpot:
                
                return dataSource?.gradSpot?.data?.count ?? 0
                
            case .myPost:
                return dataSource?.myPost?.data?.count ?? 0
            default:
                break
            }
            
        }
        return 0
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == popupTableView {
            let cell = UITableViewCell(style: .default, reuseIdentifier: nil)
            cell.textLabel?.text = self.texts[(indexPath as NSIndexPath).row]
            cell.textLabel?.textColor = UIColor.init(hexString: Appcolors.text)
            return cell
        } else {
            switch detailsType {
            case .profile:
                
                //MARK:- PROFILE CELL
                let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileTableViewCell", for: indexPath) as! ProfileTableViewCell
                cell.businessNameTxtField.text
                    = dataSource?.profile?.data?.businessName
                if dataSource?.profile?.data?.aboutBusiness != nil {
                    cell.aboutBusinessTxtView.text = dataSource?.profile?.data?.aboutBusiness
                    cell.aboutBusinessTxtView.textColor = UIColor.init(hexString: Appcolors.primary)
                } else {
                    cell.aboutBusinessTxtView.text = "About Business"
                    cell.aboutBusinessTxtView.textColor = .lightGray
                    
                }
                if dataSource?.profile?.data?.businessType != nil {
                    cell.uniTypeTxtField.text = dataSource?.profile?.data?.businessType
                } else {
                    cell.uniTypeTxtField.placeholder = "Business Type"
                }
                if dataSource?.profile?.data?.phone != nil {
                    cell.phoneNumberTxtField.text = dataSource?.profile?.data?.phone
                } else {
                    cell.phoneNumberTxtField.placeholder = "Phone No"
                }
                if dataSource?.profile?.data?.websiteLink != nil {
                    cell.websiteTxtFeidl.text = dataSource?.profile?.data?.websiteLink.optionalUnWrapped
                } else {
                    cell.websiteTxtFeidl.placeholder = "Website Link"
                }
                
                if dataSource?.profile?.data?.addressString != nil {
                    cell.locationTxtField.text = dataSource?.profile?.data?.addressString.optionalUnWrapped
                } else {
                    cell.locationTxtField.placeholder = "Address"
                }
                
                if dataSource?.profile?.data?.openingHour != nil {
                    cell.openingHourTxtField.text = dataSource?.profile?.data?.openingHour.optionalUnWrapped
                } else {
                    cell.openingHourTxtField.placeholder = "Opening Hour"
                }
                if dataSource?.profile?.data?.openingHour != nil {
                    cell.openingHourTxtField.text = dataSource?.profile?.data?.openingHour.optionalUnWrapped
                } else {
                    cell.openingHourTxtField.placeholder = "Opening Hour"
                }
                if dataSource?.profile?.data?.email != nil {
                    cell.emailTxtField.text = dataSource?.profile?.data?.email.optionalUnWrapped
                } else {
                    cell.emailTxtField.placeholder = "Email"
                }
                if dataSource?.profile?.data?.facebook != nil {
                    cell.facebookTxtField.text = dataSource?.profile?.data?.facebook.optionalUnWrapped
                } else {
                    cell.facebookTxtField.placeholder = "Facebook"
                }
                if dataSource?.profile?.businessNameHidden == true {
                    cell.nameContainerView.isHidden = true
                } else {
                    cell.nameContainerView.isHidden = false
                }
                if dataSource?.profile?.data?.isFollowed == true {
                    followBtn.setTitle("Unfollow")
                    followBtn.backgroundColor = UIColor.init(hexString: Appcolors.primary)
                    followBtn.setTitleColor(.white, for: .normal)
                    
                } else {
                    followBtn.setTitle("Follow")
                    followBtn.backgroundColor = .white
                    followBtn.setTitleColor(UIColor.init(hexString: Appcolors.primary), for: .normal)
                }
                
                
                if dataSource?.profile?.data?.isOwnBusiness == true {
                    cell.footerBtnStackView.isHidden = false
                } else {
                    cell.footerBtnStackView.isHidden = true
                }
                cell.delegate = self
                return cell
            case .portfolio:
                //MARK:- PORTFOLIO CELL
                let cell = tableView.dequeueReusableCell(withIdentifier: "PortFolioTableViewCell", for: indexPath) as! PortFolioTableViewCell
                cell.delegate = self
                cell.selectionType = .portfolio
                cell.postType = .portfolio
                cell.commentId = dataSource?.portfolio?.data?[indexPath.row].internalIdentifier
                return cell.portFolioCell(cell: cell, indexPath: indexPath, dataSource: dataSource?.portfolio?.data)
                
            case .offer:
                //MARK:- OFFER CELL
                let cell = tableView.dequeueReusableCell(withIdentifier: "DiscountTableViewCell", for: indexPath) as! DiscountTableViewCell
                cell.delegate = self
                cell.selectionType = .offer
                cell.postType = .offer
                return cell.DiscountTVC(cell: cell, indexPath: indexPath, dataSource: dataSource?.Offer?.data)
            case .gradSpot:
                //MARK:- EVENT CELL
                let cell = tableView.dequeueReusableCell(withIdentifier: "EventTableViewCell", for: indexPath) as! EventTableViewCell
                cell.selectionType = .gradSpot
                cell.delegate = self
                cell.postType = .gradSpot
                return cell.eventTVC(cell: cell, indexPath: indexPath, dataSource: dataSource?.gradSpot?.data)
            case .myPost:
                //MARK:- PORT FOLIO CELL
                
                
                
                if dataSource?.myPost?.data?[indexPath.row].portfolio != nil {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "PortFolioTableViewCell", for: indexPath) as! PortFolioTableViewCell
                    
                    //    print(filtered_list?[indexPath.row].portfolio?.addressString)
                    //                    var filtered_lists = filtered_list?.filter({$0.portfolio?.internalIdentifier != nil})
                    //                    print(filtered_lists?[0].portfolio?.addressString)
                    //                dataSource?.portfolio?.data = dataSource?.myPost?.data?.filter {
                    //                    $0.portfolio != nil
                    //}
                    var portData = dataSource?.myPost?.data?.filter({ $0.portfolio != nil })
                    cell.subHeaderlabel.text = dataSource?.myPost?.data?[indexPath.row].portfolio?.addressString
                    cell.postType = .portfolio
                    cell.delegate = self
                    cell.selectionType = .myPost
                    return cell.portFolioCell(cell: cell, indexPath: indexPath, dataSource: dataSource?.myPost?.data)
                } else if dataSource?.myPost?.data?[indexPath.row].offer != nil {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "DiscountTableViewCell", for: indexPath) as! DiscountTableViewCell
                    cell.postType = .offer
                    cell.delegate = self
                    cell.selectionType = .myPost
                    return cell.DiscountTVC(cell: cell, indexPath: indexPath, dataSource: dataSource?.myPost?.data)
                } else if dataSource?.myPost?.data?[indexPath.row].event != nil {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "EventTableViewCell", for: indexPath) as! EventTableViewCell
                    let eventData = dataSource?.myPost?.data?.filter {
                        $0.event != nil
                    }
                    print(dataSource?.gradSpot?.data)
                    cell.delegate = self
                    cell.postType = .gradSpot
                    cell.selectionType = .myPost
                    cell.eventTypeLabel.text = dataSource?.myPost?.data?[indexPath.row].event?.eventType
                    return cell.eventTVC(cell: cell, indexPath: indexPath, dataSource: dataSource?.myPost?.data)
                } else if dataSource?.myPost?.data?[indexPath.row].event == nil && dataSource?.myPost?.data?[indexPath.row].offer == nil && dataSource?.myPost?.data?[indexPath.row].portfolio == nil && dataSource?.myPost?.data?[indexPath.row].business == nil {
                    //MARK:- CREATE POST WITH GALLERIES VIDEOS CELL
                    print(dataSource?.myPost?.data?[indexPath.row].galleries?.count)
                    
                    if dataSource?.myPost?.data?[indexPath.row].galleries?.count != 0 {
                        
                        //MARK:- POST CREATION GALLERTIES VIDEO POST
                        if let index = dataSource?.myPost?.data?[indexPath.row].galleries?.firstIndex(where: { $0.mediaType == "Video"
                        }) {
                            let cell = tableView.dequeueReusableCell(withIdentifier: "LiveTableViewCell", for: indexPath) as! LiveTableViewCell
                            cell.postType = .myPost
                            cell.name = dataSource?.myPost?.data?[indexPath.row].sourceCountry as? String
                            cell.selectionType = .myPost
                           // cell.delegate = self
                            cell.locationLabel.text = dataSource?.myPost?.data?[indexPath.row].business?.businessName
                            cell.viewersLabel.text = "\(dataSource?.myPost?.data?[indexPath.row].viewsCount ?? 0)"
                            //cell.videoType = .postVideo
                            cell.captionLabel.text = dataSource?.myPost?.data?[indexPath.row].message
                            
                            let filrurls = NSURL(string: (GraduateUrl.imageUrl + (dataSource?.myPost?.data?[indexPath.row].galleries?[index].filepath ?? "")))
                            
                            //                            let url = (GraduateUrl.imageUrl + (dataSource?.myPost?.data?[indexPath.row].galleries?[index].filepath ?? ""))
                            let firstAsset = AVURLAsset(url: filrurls! as URL)
                            cell.videoPlayer.videoAssets = [firstAsset]
                            //                            let newDate = HelperFunctions.getDate(input: dataSource?[indexPath.row].portfolio?.business?.createdAt ?? "")
                            // cell.timeStampLabel.text = newDate?.timeAgo()
                            //cell.timeStampLabel.text = newDate
                            return cell
                            
                        }
                        //MARK: GALLERY IMAGE POST
                        if let imageIndex = dataSource?.myPost?.data?[indexPath.row].galleries?.firstIndex(where: { $0.mediaType == "Image"
                        }) {
                            let cell = tableView.dequeueReusableCell(withIdentifier: "PostCreationTableViewCell", for: indexPath) as! PostCreationTableViewCell
                            cell.imageDataSource = dataSource?.myPost?.data?[indexPath.row].galleries ?? []
                            cell.selectionType = .myPost
                            cell.postType = .myPost
                            cell.delegate = self
                            return cell.postTVC(cell: cell, indexPath: indexPath, dataSource: dataSource?.myPost?.data)
                        }
                    } else if dataSource?.myPost?.data?[indexPath.row].liveVideoMp4 != nil {
                        //MARK:- MYPOST CELL WITH LIVE TYPE VIDEOS
                        print("")
                        let cell = tableView.dequeueReusableCell(withIdentifier: "LiveTableViewCell", for: indexPath) as! LiveTableViewCell
                        cell.selectionType = .myPost
                        cell.name = dataSource?.myPost?.data?[indexPath.row].sourceCountry as? String
                        cell.locationLabel.text = dataSource?.myPost?.data?[indexPath.row].business?.businessName
                        cell.viewersLabel.text = "\(dataSource?.myPost?.data?[indexPath.row].viewsCount ?? 0)"
                        let fileUrl = URL(fileURLWithPath: ((GraduateUrl.imageUrl) + (dataSource?.myPost?.data?[indexPath.row].liveVideoMp4 ?? "")))
                        cell.videoPlayer.videoURLs.append(fileUrl)
                        return cell
                    } else {
                        let cell = tableView.dequeueReusableCell(withIdentifier: "PostCreationTableViewCell", for: indexPath) as! PostCreationTableViewCell
                        cell.selectionType = .myPost
                        cell.postType = .myPost
                        cell.delegate = self
                        return cell.postTVC(cell: cell, indexPath: indexPath, dataSource: dataSource?.myPost?.data)
                    }
                    
                }
                
                
                
            default:
                break
            }
            
            return UITableViewCell()
        }
    }
    
    
}

//MARK:- TABLE DELEGATE
extension GraduateDetailsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == popupTableView {
            return 40
        } else {
            return UITableView.automaticDimension
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        popover.dismiss()
        if tableView == popupTableView {
            if texts[indexPath.row] ==  "Share"{
                let shareText = "Hello, world!"
                
                let image = UIImage(named: "iconArrowDown")
                let vc = UIActivityViewController(activityItems: [shareText, image!], applicationActivities: [])
                present(vc, animated: true)
                self.popover.dismiss()
            } else if texts[indexPath.row] == "Edit" {
                self.popover.dismiss()
                let vc = UIStoryboard.timeLineStoryboard.instantiateCreateSpotViewController()
                switch detailsType {
                case .portfolio:
                    vc.vcType = .editPortFolio
                    vc.editData = dataSource?.portfolio?.data?[editedIndex.row]
                case .offer:
                    vc.vcType = .editOffer
                    vc.editData = dataSource?.Offer?.data?[editedIndex.row]
                case .gradSpot:
                    vc.vcType = .editSpot
                    vc.editData = dataSource?.gradSpot?.data?[editedIndex.row]
                case .myPost:
                    switch postTypes {
                    case .portfolio:
                        vc.vcType = .editPortFolio
                        vc.editData = dataSource?.myPost?.data?[editedIndex.row]
                    case .offer:
                        vc.vcType = .editOffer
                        vc.editData = dataSource?.myPost?.data?[editedIndex.row]
                    case .gradSpot:
                      
                        print(dataSource?.myPost?.data?[indexPath.row])
                        vc.editData = dataSource?.myPost?.data?[editedIndex.row]
                      
                    default:
                        break
                    }
                default:
                    break
                }
                vc.delegate = self
                navigationController?.pushViewController(vc, animated: true)
                
            } else if texts[indexPath.row] == "Delete" {
                switch detailsType {
                case .portfolio:
                popUp(type: .deletePortFolio, message: "")
                case .offer:
                    popUp(type: .deleteOffer, message: "")
                case .gradSpot:
                    popUp(type: .deleteEvent, message: "")
                case .myPost:
                    switch postTypes {
                    case .portfolio:
                        popUp(type: .deletePortFolio, message: "")
                    case .offer:
                        popUp(type: .deleteOffer, message: "")
                    case .gradSpot:
                        popUp(type: .deleteEvent, message: "")
                  
                    default:
                        break
                    }
                default:
                    break
                }
//                let vc = UIStoryboard.timeLineStoryboard.instantiateDeletePostPopUpVC()
//                let cardPopup = SBCardPopupViewController(contentViewController: vc)
//                cardPopup.show(onViewController: self)
//                self.popover.dismiss()
            }else {
                
                let vc = UIStoryboard.graduateStoryboard.instantiateReportVC()
                vc.reportId = reportId
                vc.modalPresentationStyle = .overFullScreen
                present(vc, animated: true, completion: nil)
                popover.dismiss()
            }
        } else {
            
        }
    }
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        switch detailsType {
        case .portfolio:
            let allData = self.dataSource
            print(allData?.loadMore ?? false)
            print(indexPath.row,"indexPath")
            print(dataSource?.portfolio?.data?.count,"dataCount")
            guard allData?.loadMore == true, let dataCount = allData?.portfolio?.data?.count, indexPath.row == dataCount - 1 else {return}
            print(dataCount)
            let lastSectionIndex = tableView.numberOfSections - 1
            let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
            if indexPath.section == lastSectionIndex && indexPath.row == lastRowIndex {
                // print("this is the last cell")
                let spinner = UIActivityIndicatorView(style: .gray)
                spinner.startAnimating()
                spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
                self.tableView.tableFooterView = spinner
                self.tableView.tableFooterView?.isHidden = false
            }
            // print(allData.url)
            requestingForPortfolio(pageNo: dataSource?.numOfPage ?? 0, isLoadMore: dataSource!.loadMore, showHud: false)
        case .myPost:
            let allData = self.dataSource
            print(allData?.loadMore ?? false)
            print(indexPath.row,"indexPath")
            print(dataSource?.myPost?.data?.count,"dataCount")
            guard allData?.loadMore == true, let dataCount = allData?.myPost?.data?.count, indexPath.row == dataCount - 1 else {return}
            print(dataCount)
            let lastSectionIndex = tableView.numberOfSections - 1
            let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
            if indexPath.section == lastSectionIndex && indexPath.row == lastRowIndex {
                print("this is the last cell")
                let spinner = UIActivityIndicatorView(style: .gray)
                spinner.startAnimating()
                spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
                self.tableView.tableFooterView = spinner
                self.tableView.tableFooterView?.isHidden = false
            }
            requestingForMyPost(pageNo: dataSource?.numOfPage ?? 0, isLoadMore: dataSource!.loadMore, showHud: false)
        default:
            break
        }
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 300
    }
}
extension GraduateDetailsViewController: SendingMultipleSeclections {
    func postAsActionFromInside(value: Int?, bool: Bool, index: Int?, type: CreateSpotVCTypes, posterIds: [Int], postData: CreateEventModel) {
        
    }
    
    func gettingTheSelectedValueFromTheList(value: Int?, bool: Bool, index: Int?, type: CreateSpotVCTypes, posterIds: [Int]) {
    
        
    }
    
    
    
    func gettingSelectedVaueFromProfileMoreBtn(value: Int, bool: Bool, index: Int) {
        profilePictureSelected = false
        let cameraVc = CustomImagePicker()
        cameraVc.delegate = self
        if value == 0 {
            
            //MARK:- CHANGE COVER PICTURE FORM CAMERA
            cameraVc.delegate = self
            cameraVc.sourceType = UIImagePickerController.SourceType.camera
            self.present(cameraVc, animated: true, completion: nil)
            
        } else {
            //MARK:- CHANGE COVER PICTURE FROM GALLERY
            if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
                print("Button capture")
                cameraVc.sourceType = .savedPhotosAlbum
                cameraVc.allowsEditing = false
                
                self.present(cameraVc, animated: true) {
                    
                }
            }
        }
    }
    
    func sendingUserId(id: Int, index: Int) {
        
    }
    
    func sendingMediaType(value: Int) {
        //MARK:- SHOW COVER PICTURE
        coverImageFullScreen()
        
    }
    func sendingMultipleVlaues(values: [String]) {
        //MARK:- SHOW PROFILE PICTURE
        profileImageFullScreen()
    }
    
    func sendingDiscountType(value: String) {
        //MARK:- TAKE PICTTURE FROM CAM FOR PORILE PIC
        profilePictureSelected = true
        let cameraVc = CustomImagePicker()
        cameraVc.delegate = self
        if value == "Take Picture" {
            //MARK:- CHANGE COVER PICTURE FORM CAMERA
            cameraVc.delegate = self
            cameraVc.sourceType = UIImagePickerController.SourceType.camera
            self.present(cameraVc, animated: true, completion: nil)
        } else if value == "Select from gallery" {
            //MARK:- TAKE PICTTURE FROM GALLERY FOR PORILE PIC
            //MARK:- CHANGE COVER PICTURE FROM GALLERY
            if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
                print("Button capture")
                cameraVc.sourceType = .savedPhotosAlbum
                cameraVc.allowsEditing = false
                
                self.present(cameraVc, animated: true) {
                    
                }
            }
        }
    }
    
    func sendingEvnetType(value: String, type: String) {
        
    }
    
    func sendingUniCategory(value: String) {
        
    }
    
    
    
    func gettingFullScreenCoverPic() {
        
        
    }
    
    
}
extension GraduateDetailsViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true)
        
        if let image = info[.editedImage] as? UIImage  {
            if profilePictureSelected == true {
                profileImageStrogage = image
                popUp(type: .profilePictureSelected, messsage: "")
            } else if profilePictureSelected == false {
                coverImageStorage = image
                popUp(type: .coverPictureSelected, messsage: "")
            }
            
        } else {
            if let galleryImage = info[.originalImage] as? UIImage  {
                if profilePictureSelected == true {
                    profileImageStrogage = galleryImage
                    popUp(type: .profilePictureSelected, messsage: "")
                } else if profilePictureSelected == false {
                    coverImageStorage = galleryImage
                    popUp(type: .coverPictureSelected, messsage: "")
                }
            }
            
            // print out the image size as a test
            
        }
    }
}
extension GraduateDetailsViewController: SendingActionFromReusablepopUp {
    func sendingVlaue(value: Bool) {
        if !value {
            
        } else {
            coverImage.image = coverImageStorage
        }
    }
    
    
}
//MARK:-ACTIONS FROM PORTFOLIO
extension GraduateDetailsViewController: SendingRatingFromPFTVC {
    
    func outsideCommentPortFolioTVC(comment: Comments?, index: IndexPath) {
        if case .myPost = detailsType {
            dataSource?.myPost?.data?[index.row].comments?.append(comment!)
            tableView.reloadRows(at: [index], with: .automatic)
        } else {
        dataSource?.portfolio?.data?[index.row].comments?.append(comment!)
        tableView.reloadRows(at: [index], with: .automatic)
        }
    }
    func pftvcthoughtsDetails(commentId: Int, outsideComment: Bool, index: IndexPath) {
        if outsideComment == false {
           thoughtDetails(commentid: commentId, index: index)
        } else {
            
        }
    }
    
  
    
    func moreOPtionFromPortTVC(sender: UIButton, id: Int, index: IndexPath, cell: PortFolioTableViewCell, isOwner: Bool, postType: GraduateDetails) {
        postTypes = postType
        editedIndex = index
        reportId = id
        if isOwner == true {
            texts.removeAll()
            texts = ["Share", "Edit","Delete"]
            sharePopOver(sender: sender)
        } else {
            texts.removeAll()
            texts = ["Share", "Report"]
            sharePopOver(sender: sender)
        }
    }
  
    func pushingTOProfileVCFromPFTVC(id: Int, userType: String, isOwner: Bool, postAsType: String) {
         pusingToProfileVC(id: reportId ?? 0)
    }
//    func moreOPtionFromPortTVC(sender: UIButton, id: Int, index: IndexPath, cell: PortFolioTableViewCell, isOwner: Bool) {
//        reportId = id
//        if isOwner == true {
//                  sharePopOver(sender: sender)
//              } else {
//                  texts.removeAll()
//                  texts = ["Share", "Report"]
//                  sharePopOver(sender: sender)
//              }
//    }
    
    func pftvcRating(id: Int, starValue: String, selectedindexToReload: Int, selectionType: GraduateDetails) {
        self.poppingRatingVC(id: id, starredVlaue: starValue, indexToReload: selectedindexToReload, selectionType: selectionType)
    }
//    func pftvcRating(id: Int, starValue: String, selectedindexToReload: Int) {
//        poppingRatingVC(id: id, starredVlaue: starValue, indexToReload: selectedindexToReload, selectionType: .portfolio)
//    }
    func sendingThoughts(id: Int, param: [String : Any]) {
        ThoughtsResponse.requestToComment(id: "\(id)", param: param) { [weak self](response) in
            
        }
    }
 
    func pftvcthoughtsDetails(commentId: Int, index: IndexPath) {
        let vc = UIStoryboard.timeLineStoryboard.instantiateThoughtsVC()
        vc.commentId = commentId
        vc.index = index
        vc.delegate = self
        navigationController?.pushViewController(vc, animated: true)
    }
    func pftvcratingDetails(ratingId: Int, starredValue: String, starCount: Int) {
        let vc = UIStoryboard.timeLineStoryboard.instantiateRatingDetailsVC()
        vc.ratingId = ratingId
        vc.starredValue = starredValue
        vc.starCount = starCount
        navigationController?.pushViewController(vc, animated: true)
    }
   
    func feedbackActionPFTVC() {
        
    }
    
    func fullScreen(vc: UIViewController) {
        currentTabBar?.setBar(hidden: true, animated: false)
        present(vc, animated: true, completion: nil)
    }
    
   
    func poppingRatingVC(id: Int, starredVlaue: String,indexToReload: Int, selectionType: GraduateDetails) {
        let vc = HelperFunctions.ratingSelection(id: id, starredVlaue: starredVlaue, reloadingIndex: indexToReload )
        vc.delegate = self
        vc.indexToReload = indexToReload
        vc.selectedIndex = Int(starredVlaue)
        vc.selectionType = selectionType
        present(vc, animated: true, completion: nil)
    }
}
//MARK: REVCEIVING RATING FROM THE DISCOUNTTVC
extension GraduateDetailsViewController: SedingRatingsToTimeLineVC {
    func moreOptionFromDiscountTVC(sender: UIButton, id: Int, isOwnBusiness: Bool, postType: GraduateDetails, index: IndexPath) {
        self.postTypes = postType
        self.editedIndex = index
        reportId = id
               if isOwnBusiness == true {
                         sharePopOver(sender: sender)
                     } else {
                         texts.removeAll()
                         texts = ["Share", "Report"]
                         sharePopOver(sender: sender)
                     }
    }
    
    func commentOutsideDiscountTVC(comment: Comments?, index: IndexPath) {
        dataSource?.Offer?.data?[index.row].comments?.append(comment!)
        tableView.reloadRows(at: [index], with: .automatic)
    }
    func pushingTOProfileVCFromDisc(id: Int, userType: String, isOwner: Bool, postAsType: String) {
          pusingToProfileVC(id: reportId ?? 0)
    }
    func feedbackAction(commentId: Int, outsideComment: Bool, index: IndexPath) {
        if outsideComment == false {
                   pftvcthoughtsDetails(commentId: commentId,index: index)
               } else {
                   
               }
    }
    
   
 
    func sendingRating(id: Int, starValue: String, selectedindexToReload: Int, selectionType: GraduateDetails) {
        poppingRatingVC(id: id, starredVlaue: starValue, indexToReload: selectedindexToReload, selectionType: selectionType)
    }
    
    
  
   
    func discratingDetails(ratingId: Int, starredValue: String, starCount: Int) {
        print(ratingId)
        pftvcratingDetails(ratingId: ratingId, starredValue: starredValue, starCount: starCount)
    }
    func moreOptionFromDiscountTVC(sender: UIButton) {
        
    }
    
    func fullscreen(vc: UIViewController) {
        currentTabBar?.setBar(hidden: true, animated: true)
        present(vc, animated: true, completion: nil)
    }
   
}
//MARK:- ACTIONS FROM EVENT VC
extension GraduateDetailsViewController: SendingRatingFromEventTVC {
    func moreOPtionFromEventTVC(sender: UIButton, id: Int, index: IndexPath, cell: EventTableViewCell, isOwner: Bool, postType: GraduateDetails) {
        self.postTypes = postType
        self.editedIndex = index
        reportId = id
                    if isOwner == true {
                              sharePopOver(sender: sender)
                          } else {
                              texts.removeAll()
                              texts = ["Share", "Report"]
                              sharePopOver(sender: sender)
                          }
    }

    func outsideCommentEventTVC(comment: Comments?, index: IndexPath) {
        dataSource?.gradSpot?.data?[index.row].comments?.append(comment!)
        tableView.reloadRows(at: [index], with: .automatic)
    }
    func pushingToProfileFromSpot(id: Int, userType: String, isOwner: Bool, postAsType: String) {
         pusingToProfileVC(id: reportId ?? 0)
    }
    func eventFeedbackAction(commentId: Int, index: IndexPath) {
        thoughtDetails(commentid: commentId, index: index)
    }
    
  
    
    func sendingRatingEventTVC(id: Int, starValue: String, selectedindexToReload: Int, selectionType: GraduateDetails) {
        poppingRatingVC(id: id, starredVlaue: starValue, indexToReload: selectedindexToReload, selectionType: selectionType)
    }
    
    func attendIgnoreEventTVC(universityId: Int, attendState: Bool, index: IndexPath!, attendeesCount: Int) {
        let url: String?
        print(universityId)
        var attendeeCount = attendeesCount
        if attendState == true {
            url = (GraduateUrl.attendEventURL + "\(universityId)")
            
            //attendeeCount = attendeesCount + 1
            
        } else {
            url = (GraduateUrl.unAttendEventURL + "\(universityId)")
            //attendeeCount = attendeesCount - 1
        }
        
        GraduateDetailsResponse.requestToAttendIgnoreEvents(url: url ?? "") { [weak self](response) in
            guard let strongSelf = self else { return }
            //            let cell = strongSelf.tableView.dequeueReusableCell(withIdentifier: "EventTableViewCell", for: index) as! EventTableViewCell
            //            cell.attendingLabel.text = "\(attendeeCount)"
            //            strongSelf.tableView.rectForRow(at: index)
            
        }
    }
    
    
    
    
    
    func eventRatingDetails(ratingId: Int, starredVlaue: String, starCount: Int) {
        let vc = UIStoryboard.timeLineStoryboard.instantiateRatingDetailsVC()
        vc.starredValue = starredVlaue
        vc.ratingId = ratingId
        vc.starCount = starCount
        navigationController?.pushViewController(vc, animated: true)
    }
 
   
    func feedbackActionEventTVC() {
        
    }
    
    func fullScreenAction(vc: UIViewController) {
        currentTabBar?.setBar(hidden: true, animated: true)
        present(vc, animated: true, completion: nil)
    }
 
}
//MARK:-ACTIONS FROM POST CRREATEION
extension GraduateDetailsViewController:  SendingRatingFromPCTVC {
    func outsideCommentPostTVC(comment: Comments?, index: IndexPath) {
        switch detailsType {
        case .myPost:
            dataSource?.myPost?.data?[index.row].comments?.append(comment!)
            tableView.reloadRows(at: [index], with: .automatic)
        case .portfolio:
            dataSource?.portfolio?.data?[index.row].comments?.append(comment!)
            tableView.reloadRows(at: [index], with: .automatic)
        case .offer:
            dataSource?.Offer?.data?[index.row].comments?.append(comment!)
            tableView.reloadRows(at: [index], with: .automatic)
        case .gradSpot:
            dataSource?.gradSpot?.data?[index.row].comments?.append(comment!)
            tableView.reloadRows(at: [index], with: .automatic)
        default:
            break
        }
    }
    
    func thoughtsAction() {
        
    }
    
    func moreOptionFromPostTVC(sender: UIButton, id: Int, index: IndexPath, cell: PostCreationTableViewCell, isOwner: Bool, postType: GraduateDetails) {
        postTypes = postType
        editedIndex = index
        if isOwner == true {
            texts.removeAll()
            texts = ["Share", "Edit","Delete"]
            sharePopOver(sender: sender)
        } else {
            texts.removeAll()
            texts = ["Share", "Report"]
            sharePopOver(sender: sender)
        }
    }
    
    func ratingDetailFromPCTVC(ratingId: Int, starredValue: String, starCount: Int) {
        pftvcratingDetails(ratingId: ratingId, starredValue: starredValue, starCount: starCount)
    }
   
    func pushingTOProfileVC(id: Int, userType: String, isOwner: Bool, postAsType: String) {
        pushToProfile(id: id, userType: userType, isOwner: isOwner, postAsType: postAsType)
    }
    
  
    func PCTVCRating(id: Int, starValue: String, selectedindexToReload: Int, selectionType: GraduateDetails) {
         poppingRatingVC(id: id, starredVlaue: starValue, indexToReload: selectedindexToReload, selectionType: selectionType)
    }
    func feedbackActionPCTVC(commentId: Int, outsideComment: Bool, index: IndexPath) {
        if outsideComment == false {
            pftvcthoughtsDetails(commentId: commentId, index: index)
        } else {
            
        }
       
    }
  
    func fullScreenImage(vc: UIViewController) {
        currentTabBar?.setBar(hidden: true, animated: true)
        present(vc, animated: true, completion: nil)
    }
    //MARK:- PUSH TO PROFILE FUNC
    func pushToProfile(id: Int, userType: String, isOwner: Bool, postAsType: String) {
        if postAsType == "user" {
                    if isOwner == true {
                                 let vc = UIStoryboard.timeLineStoryboard.instantiateUserProfileVC()
                                 navigationController?.pushViewController(vc, animated: true)
                                 } else {
                                     let vc = UIStoryboard.timeLineStoryboard.instantiateOtherUserProfileVC()
                               vc.userId = id
                                            navigationController?.pushViewController(vc, animated: true)
                                 }
                    } else {
                        let vc = UIStoryboard.timeLineStoryboard.instantiateGraduateDetailsViewController()
                        vc.businessId = id
                        navigationController?.pushViewController(vc, animated: true)
                    }
    }
    
}
extension GraduateDetailsViewController: FPNTextFieldDelegate {
    /// The place to present/push the listController if you choosen displayMode = .list
    func fpnDisplayCountryList() {
        let navigationViewController = UINavigationController(rootViewController: listController)
        present(navigationViewController, animated: true, completion: nil)
    }
    
    /// Lets you know when a country is selected
    func fpnDidSelectCountry(name: String, dialCode: String, code: String) {
        print(name, dialCode, code) // Output "France", "+33", "FR"
    }
    
    /// Lets you know when the phone number is valid or not. Once a phone number is valid, you can get it in severals formats (E164, International, National, RFC3966)
    func fpnDidValidatePhoneNumber(textField: FPNTextField, isValid: Bool) {
        textField.rightViewMode = .always
        //textField.rightView = UIImageView(image: isValid ? #imageLiteral(resourceName: "success") : #imageLiteral(resourceName: "error"))
        
        print(
            isValid,
            textField.getFormattedPhoneNumber(format: .E164) ?? "E164: nil",
            textField.getFormattedPhoneNumber(format: .International) ?? "International: nil",
            textField.getFormattedPhoneNumber(format: .National) ?? "National: nil",
            textField.getFormattedPhoneNumber(format: .RFC3966) ?? "RFC3966: nil",
            textField.getRawPhoneNumber() ?? "Raw: nil"
        )
    }
    
}
extension GraduateDetailsViewController: SendingActionFromProfileTVC {
    func deleteBtnAction() {
        popUp(type: .deleteUniversity, messsage: "")
        
    }
    
    func sendingTextFieldText(text: String, textFiledType: universityProfileInfo) {
        switch textFiledType {
        case .businessname:
            dataSource?.profile?.data?.businessName = text
        case .businesType:
            dataSource?.profile?.data?.businessType = text
        case .phone:
            dataSource?.profile?.data?.phone = text
        case .website:
            dataSource?.profile?.data?.websiteLink = text
        case .address:
            dataSource?.profile?.data?.addressString = text
        case .openingHour:
            dataSource?.profile?.data?.openingHour = text
        case .email:
            dataSource?.profile?.data?.email = text
        case .facebook:
            dataSource?.profile?.data?.facebook = text
        case .businessNameHidden:
            dataSource?.profile?.businessNameHidden = false
        case .aboutbusinesss:
            dataSource?.profile?.data?.aboutBusiness = text
        default:
            break
        }
        tableView.reloadData()
    }
    
    func sendingActionFromTxtField(textField: UITextField) {
        
    }
    func requestingForupdateProfile(params: [String: Any]) {
        requestingForUniupdateProfile(params: params)
    }
}
extension GraduateDetailsViewController: SendingActionFromErrorChat {
    func adctionwithType(type: ErrorType) {
        
        if case .myPost = detailsType {
            switch type {
            case .deletePortFolio:
                deletePortfolio(id: dataSource?.myPost?.data?[editedIndex.row].portfolio?.internalIdentifier ?? 0)
            case .deleteOffer:
                requestToDeleteOFfer(id: dataSource?.myPost?.data?[editedIndex.row].offer?.internalIdentifier ?? 0)
            case .deleteEvent:
                requestToDeleteEvent(id: dataSource?.myPost?.data?[editedIndex.row].event?.internalIdentifier ?? 0)
            default:
                break
            }
        } else {
        switch type {
        case .deleteUniversity:
            requestForUniversityDelte()
        case .deletePortFolio:
            deletePortfolio(id: dataSource?.Offer?.data?[editedIndex.row].portfolio?.internalIdentifier ?? 0)
        case .deleteOffer:
            requestToDeleteOFfer(id: dataSource?.Offer?.data?[editedIndex.row].offer?.internalIdentifier ?? 0)
        case .deleteEvent:
            requestToDeleteEvent(id: dataSource?.gradSpot?.data?[editedIndex.row].event?.internalIdentifier ?? 0)
        case .eventCreated:
            requestingForEvents(pageNo: 1, isLoadMore: false, showHud: true)
        case .portFolioCreated:
            requestingForPortfolio(pageNo: 1, isLoadMore: false, showHud: true)
        case .offerCreated:
            requestingForOffers(pageNo: 1, isLoadMore: false, showHud: true)
        default:
            break
        }
        }
    }
    
    
    func sendingAction() {
        addPictureLabel.isHidden = true
        profileImage.image = profileImageStrogage
    }
    
    func sendingCancelAction() {
        coverImage.image = coverImageStorage
    }
}

//MARK:- RATING VC DELEGATE
extension GraduateDetailsViewController: SendingActionFromRatingVC {
    
    func sendingActionWithType(type: RatingActionTypes, id: Int, reloadingIndex: Int, ratingNum: Int,selectionType: GraduateDetails) {
        switch type {
        case .requestforRating:
            //                    reqestForPostRating(id: id, reloadingIndex: reloadingIndex)
            //                   let cell = tableView.dequeueReusableCell(withIdentifier: "DiscountTableViewCell") as! DiscountTableViewCell
            //                   cell.ratingBtn.setImage(UIImage(named: "star2"))
            //                  let indexPath = IndexPath(item: reloadingIndex, section: 0)
            //                   tableView.reloadRows(at: [indexPath], with: .automatic)
            switch selectionType {
            case .offer:
                ratingAfterApiCallOFOffer(reloadingIndex: reloadingIndex, ratingNum: ratingNum)
            case .portfolio:
                ratingAfterApiCallOfPortFolio(reloadingIndex: reloadingIndex, ratingNum: ratingNum)
            case .gradSpot:
                ratingAfterApiCallOfEvent(reloadingIndex: reloadingIndex, ratingNum: ratingNum)
            case .myPost:
               ratingAfterApiCallOfMyPost(reloadingIndex: reloadingIndex, ratingNum: ratingNum)
            default:
                break
            }
            reqestForPostRating(id: id, reloadingIndex: reloadingIndex, ratingNum: ratingNum, selectionType: selectionType)
        case .reuestForUnratingPost:
            switch selectionType {
            case .offer:
                unratingAfterApiCallOfOffer(reloadingIndex: reloadingIndex, ratingNum: ratingNum)
            case .portfolio:
                unratingAfterApiCallOfPortFolio(reloadingIndex: reloadingIndex, ratingNum: ratingNum)
            case .gradSpot:
               unratingAfterApiCallOfEvent(reloadingIndex: reloadingIndex, ratingNum: ratingNum)
            case .myPost:
               unratingAfterApiCallOfMyPost(reloadingIndex: reloadingIndex, ratingNum: ratingNum)
            default:
                break
            }
            reqestForPostUnRating(id: id, reloadingIndex: reloadingIndex, ratingNum: 0, selectionType: selectionType)
        default:
            break
        }
    }
    
    //MARK:- RATE POST
    func reqestForPostRating(id: Int,reloadingIndex: Int,ratingNum: Int,selectionType: GraduateDetails) {
        var headers: [String : Any]?
        if let token = UserDefaultsHandler.getUDValue(key: UDkey.token) as? String {
            if headers == nil {
                headers = ["Authorization": "Bearer \(token)", "Accept": "application/json"]
            } else {
                headers!["Authorization"] = "Bearer \(token)"
                headers!["Accept"] = "application/json"
            }
        }
        let url = (GraduateUrl.ratePostURL + "\(id)")
         print(url,ratingNum,selectionType)
        Alamofire.request(url,
                          method: .post,
                          parameters: ["star_value": "\(ratingNum + 1)"],headers: (headers as! HTTPHeaders))
            .validate()
            .responseJSON { [weak self]response in
                guard let strongSelf = self else { return }
                if let value = response.result.value as? [String : Any] {
                    print(value)
//                    switch selectionType {
//                    case .offer:
//                        strongSelf.ratingAfterApiCallOFOffer(reloadingIndex: reloadingIndex, ratingNum: ratingNum)
//                    case .portfolio:
//                        strongSelf.ratingAfterApiCallOfPortFolio(reloadingIndex: reloadingIndex, ratingNum: ratingNum)
//                    case .gradSpot:
//                        strongSelf.ratingAfterApiCallOfEvent(reloadingIndex: reloadingIndex, ratingNum: ratingNum)
//                    case .myPost:
//                        strongSelf.ratingAfterApiCallOfMyPost(reloadingIndex: reloadingIndex, ratingNum: ratingNum)
//                    default:
//                        break
//                    }
                    
                }
                
        }
    }
    
    //MARK:- UNRATE POST
    func reqestForPostUnRating(id: Int,reloadingIndex: Int,ratingNum: Int,selectionType: GraduateDetails) {
        var headers: [String : Any]?
        if let token = UserDefaultsHandler.getUDValue(key: UDkey.token) as? String {
            if headers == nil {
                headers = ["Authorization": "Bearer \(token)", "Accept": "application/json"]
            } else {
                headers!["Authorization"] = "Bearer \(token)"
                headers!["Accept"] = "application/json"
            }
        }
        let url = (GraduateUrl.unratePostURL + "\(id)")
        print(url,ratingNum,selectionType)
        Alamofire.request(url,method: .delete,headers: (headers as! HTTPHeaders))
            .validate()
            .responseJSON { [weak self]response in
                guard let strongSelf = self else { return }
                if let value = response.result.value as? [String : Any] {
//                    switch selectionType {
//                    case .offer:
//                        strongSelf.unratingAfterApiCallOfOffer(reloadingIndex: reloadingIndex, ratingNum: ratingNum)
//                    case .portfolio:
//                        strongSelf.unratingAfterApiCallOfPortFolio(reloadingIndex: reloadingIndex, ratingNum: ratingNum)
//                    case .gradSpot:
//                        strongSelf.unratingAfterApiCallOfEvent(reloadingIndex: reloadingIndex, ratingNum: ratingNum)
//                    case .myPost:
//                        strongSelf.unratingAfterApiCallOfMyPost(reloadingIndex: reloadingIndex, ratingNum: ratingNum)
//                    default:
//                        break
//                    }
                    print(value)
                    print(reloadingIndex)
                    
                }
                
        }
    }
    func ratingAfterApiCallOFOffer(reloadingIndex: Int, ratingNum: Int) {
        if dataSource?.Offer?.data?[reloadingIndex].hasStarred == true {
            
        } else {
            
            dataSource?.Offer?.data?[reloadingIndex].starsCount! += 1
        }
        dataSource?.Offer?.data?[reloadingIndex].starredValue = "\((ratingNum + 1))"
        print(reloadingIndex)
        dataSource?.Offer?.data?[reloadingIndex].hasStarred = true
        tableView.reloadData()
    }
    func unratingAfterApiCallOfOffer(reloadingIndex: Int, ratingNum: Int) {
        dataSource?.Offer?.data?[reloadingIndex].hasStarred = false
        if  dataSource?.Offer?.data?[reloadingIndex].starsCount ?? 0 > 0 {
            dataSource?.Offer?.data?[reloadingIndex].starsCount! -= 1
        } else {
            
        }
        dataSource?.Offer?.data?[reloadingIndex].starredValue = "\(0)"
        tableView.reloadData()
    }
    func ratingAfterApiCallOfPortFolio(reloadingIndex: Int, ratingNum: Int) {
        if dataSource?.portfolio?.data?[reloadingIndex].hasStarred == true {
           
        } else {
            
            dataSource?.portfolio?.data?[reloadingIndex].starsCount! += 1
        }
        dataSource?.portfolio?.data?[reloadingIndex].starredValue = "\((ratingNum + 1))"
        print(reloadingIndex)
        dataSource?.portfolio?.data?[reloadingIndex].hasStarred = true
        tableView.reloadData()
    }
    func unratingAfterApiCallOfPortFolio(reloadingIndex: Int, ratingNum: Int) {
        dataSource?.portfolio?.data?[reloadingIndex].hasStarred = false
        if  dataSource?.portfolio?.data?[reloadingIndex].starsCount ?? 0 > 0 {
            dataSource?.portfolio?.data?[reloadingIndex].starsCount! -= 1
        } else {
            
        }
        dataSource?.portfolio?.data?[reloadingIndex].starredValue = "\(0)"
        tableView.reloadData()
    }
    func ratingAfterApiCallOfEvent(reloadingIndex: Int, ratingNum: Int) {
        if dataSource?.gradSpot?.data?[reloadingIndex].hasStarred == true {
          
        } else {
            
            dataSource?.gradSpot?.data?[reloadingIndex].starsCount! += 1
        }
        dataSource?.gradSpot?.data?[reloadingIndex].starredValue = "\((ratingNum + 1))"
        print(reloadingIndex)
        dataSource?.gradSpot?.data?[reloadingIndex].hasStarred = true
        tableView.reloadData()
    }
    func unratingAfterApiCallOfEvent(reloadingIndex: Int, ratingNum: Int) {
        dataSource?.gradSpot?.data?[reloadingIndex].hasStarred = false
        if  dataSource?.gradSpot?.data?[reloadingIndex].starsCount ?? 0 > 0 {
            dataSource?.gradSpot?.data?[reloadingIndex].starsCount! -= 1
        } else {
            
        }
        dataSource?.gradSpot?.data?[reloadingIndex].starredValue = "\(0)"
        tableView.reloadData()
    }
    func ratingAfterApiCallOfMyPost(reloadingIndex: Int, ratingNum: Int) {
           if dataSource?.myPost?.data?[reloadingIndex].hasStarred == true {
              
           } else {
               
               dataSource?.myPost?.data?[reloadingIndex].starsCount! += 1
           }
           dataSource?.myPost?.data?[reloadingIndex].starredValue = "\((ratingNum + 1))"
           print(reloadingIndex)
           dataSource?.myPost?.data?[reloadingIndex].hasStarred = true
           tableView.reloadData()
       }
       func unratingAfterApiCallOfMyPost(reloadingIndex: Int, ratingNum: Int) {
        //dataSource?.myPost?.data?[reloadingIndex].hasStarred
           dataSource?.myPost?.data?[reloadingIndex].hasStarred = false
           if  dataSource?.myPost?.data?[reloadingIndex].starsCount ?? 0 > 0 {
               dataSource?.myPost?.data?[reloadingIndex].starsCount! -= 1
           } else {
               
           }
           dataSource?.myPost?.data?[reloadingIndex].starredValue = "\(0)"
           tableView.reloadData()
       }
    
}
extension GraduateDetailsViewController: ActionFromCreateSpotVC {
    func reloadWithType(type: ErrorType) {
        switch type {
        case .portFolioCreated:
            requestingForPortfolio(pageNo: 1, isLoadMore: false, showHud: true)
        case .eventCreated:
        requestingForEvents(pageNo: 1, isLoadMore: false, showHud: true)
        case .offerCreated:
            requestingForOffers(pageNo: 1, isLoadMore: false, showHud: true)
        case .postCreated:
            requestingForMyPost(pageNo: 1, isLoadMore: false, showHud: true)
        default:
            break
        }
    }
    
    func reloadTableViewData() {
        tableView.reloadData()
    }
    
    
}
//MARK:- THOUGHTS DELEGATE
extension GraduateDetailsViewController: SendingDataFromThoughtsVC {
    func sendingData(index: IndexPath, thoughtsCount: Int, commentsData: [Comments]) {
        if case .myPost = detailsType {
            print(index)
            dataSource?.myPost?.data?[index.row].comments = commentsData
            tableView.reloadRows(at: [index], with: .automatic)
        } else if case .portfolio = detailsType {
            dataSource?.portfolio?.data?[index.row].comments = commentsData
            tableView.reloadRows(at: [index], with: .automatic)
        }  else if case .offer = detailsType {
            dataSource?.Offer?.data?[index.row].comments = commentsData
            tableView.reloadRows(at: [index], with: .automatic)
        }  else if case .gradSpot = detailsType {
            dataSource?.gradSpot?.data?[index.row].comments = commentsData
            tableView.reloadRows(at: [index], with: .automatic)
        }
//        switch detailsType {
//        case .portfolio:
//            requestingForPortfolio(pageNo: 1, isLoadMore: false, showHud: true)
//        case .offer:
//            requestingForOffers(pageNo: 1, isLoadMore: false, showHud: true)
//        default:
//            break
//        }
   }
    
}

