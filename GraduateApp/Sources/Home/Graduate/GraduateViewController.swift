//
//  GraduateViewController.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 6/24/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import UIKit
import Popover
import AMPopTip
struct Model {
    var data: [UniversityListData] = []
}
struct UniTypesModel {
    var data: String?
    var isSelected: Bool?
}

var uniBtnSelected: Bool = false
struct GraduateUniversityModel {
    var uniData: GraduateUniversityResponseModel?
     var numOfPage: Int
     var loadMore: Bool
}
class GraduateViewController: UIViewController {
   
   
    @IBOutlet weak var okBtn: AnimatedButton!
    @IBOutlet weak var universityTableViw: UITableView!
    @IBOutlet weak var universityListView: CustomView!
    @IBOutlet weak var topStackView: UIStackView!
    @IBOutlet weak var myUniversitybtn: AnimatedButton!
    @IBOutlet weak var menuBtn: UIButton!
    @IBOutlet weak var searchBtn: AnimatedButton!
    @IBOutlet weak var tableView: UITableView!
    var universityPopView: Bool?
    var soucrce: Model?
    var myuni: Bool = false
    var universityData = [UniTypesModel]()
    var dataSource: GraduateUniversityModel? {
        didSet {
            tableView.reloadData()
                
            
        }
    }
    var hideWhenScroll: Bool?
    var universityList: GraduateUniversityModel?
    var popover: Popover!
    var myUniversityState: Bool?
    fileprivate var popoverOptions: [PopoverOption] = [
        .type(.left),
        .blackOverlayColor(UIColor(white: 0.0, alpha: 0.6))
    ]
    let popTip = PopTip()
   var graduateMenuView = UniversityMenuList()
    private var lastContentOffset: CGFloat = 0

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        registeringTableViewCell()
        getUniversityList(pageNo: 1,showHud: true)
        currentTabBar?.selectedColor = UIColor.init(hexString: Appcolors.tabBarSelectedColor)
        
        dataSource = GraduateUniversityModel(uniData: nil, numOfPage: 1, loadMore: false)
       // settingPopTip()
        pullToRefresh()
        addData()
      
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.isNavigationBarHidden = true
              cusomizingTopView()
        universityListView.isHidden = true
        currentTabBar?.setBar(hidden: false, animated: true)
        
    }
 
    override func viewDidDisappear(_ animated: Bool) {
        navigationController?.isNavigationBarHidden = false
        
    }
    //MARK:- PULL TO REFRESH
        func pullToRefresh() {
            self.tableView.es.addPullToRefresh {
                [unowned self] in
        getUniversityList(pageNo: 1, isLoadMore: false, showHud: false)
            }
        }
//MARK:- ADDING DATA IN UNIVERSITY LIST
    func addData() {
        universityData = [UniTypesModel(data: "University1", isSelected: false),UniTypesModel(data: "University2", isSelected: false),UniTypesModel(data: "University3", isSelected: false),UniTypesModel(data: "University4", isSelected: false),UniTypesModel(data: "University5", isSelected: false),UniTypesModel(data: "University6", isSelected: false),UniTypesModel(data: "University7", isSelected: false),UniTypesModel(data: "University8", isSelected: false),UniTypesModel(data: "University9", isSelected: false),UniTypesModel(data: "University10", isSelected: false),UniTypesModel(data: "University11", isSelected: false),UniTypesModel(data: "University12", isSelected: false)]
    }
    //MARK:- GET UNIVERSITY LIST
    func getUniversityList(pageNo: Int,isLoadMore: Bool = false, showHud: Bool = true) {
        GraduateUniversityResponse.requestToGraduateUniversityList(pageNo: pageNo, showLoader: showHud, showHud: showHud) { [weak self](response) in
            guard let strongSelf = self else { return }
            strongSelf.tableView.es.stopPullToRefresh()
            //strongSelf.dataSource?.uniData = response
            print(strongSelf.dataSource?.uniData)
            strongSelf.tableView.tableFooterView = nil
            strongSelf.tableView.tableFooterView?.isHidden = true
            if !isLoadMore {
                strongSelf.dataSource?.uniData = response
                print(strongSelf.universityList?.uniData)
              strongSelf.tableView.reloadData()
            } else {
              if  var allData = response?.data {
                print(allData)
                for data in allData {
                    strongSelf.dataSource?.uniData?.data?.append(data)
                }
                strongSelf.tableView.reloadData()
              }
            }
            print(strongSelf.dataSource?.uniData?.data?.count, response?.meta?.total)
            if strongSelf.dataSource?.uniData?.data?.count ?? 0 < response?.meta?.total ?? 0 {
                     strongSelf.dataSource?.loadMore = true
                     strongSelf.dataSource?.numOfPage += 1
                   } else {
                     strongSelf.dataSource?.loadMore = false
                   }
        }
    }
    //MARK:- GET USER UNIVERSITY
    func getUserUiversity() {
        GraduateUniversityResponse.requestToGetUserUni { [weak self](response) in
            guard let strongSelf = self else { return }
            strongSelf.dataSource?.uniData = response
            }
        }
    
    func cusomizingTopView() {
        searchBtn.setImage(UIImage(named: "iconSearch"), for: .normal)
        searchBtn.imageView?.contentMode = .center
        searchBtn.imageView?.tintColor = UIColor.init(hexString: Appcolors.buttons)
        menuBtn.setImage(UIImage(named: "iconMenu"), for: .normal)
        menuBtn.imageView?.contentMode = .center
        menuBtn.imageView?.tintColor = UIColor.init(hexString: Appcolors.buttons)
     
        universityTableViw.backgroundColor = UIColor.init(hexString: Appcolors.light2)
        
        
    }
    func registeringTableViewCell() {
        tableView.register(UINib(nibName: "GraduateTableViewCell", bundle: nil), forCellReuseIdentifier: "GraduateTableViewCell")
         universityTableViw.register(UINib(nibName: "UniversityListTableViewCell", bundle: nil), forCellReuseIdentifier: "UniversityListTableViewCell")
        tableView.dataSource = self
        tableView.delegate = self
        tableView.reloadData()
    }
    
    
    @IBAction func createAction(_ sender: Any) {
        let vc = UIStoryboard.timeLineStoryboard.instantiateCreateUniversityVC()
        navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func uniAction(_ sender: UIButton) {
        if uniBtnSelected == true {
            myuni = false
            uniBtnSelected = false
            myUniversitybtn.backgroundColor = .white
            myUniversitybtn.setTitleColor(UIColor.init(hexString: Appcolors.buttons), for: .normal)
            getUniversityList(pageNo: 1, isLoadMore: false, showHud: true)
        } else {
            uniBtnSelected = true
            myUniversitybtn.backgroundColor = UIColor.init(hexString: Appcolors.buttons)
            myUniversitybtn.setTitleColor(.white, for: .normal)
            myuni = true
                   getUserUiversity()
        }
       
        
       
    }
    @IBAction func menuAction(_ sender: UIButton) {

        if universityPopView == true{
            hideWhenScroll  = true
            universiyListPopView(hidden: true, backgroundColor: .white, tintColor: UIColor.init(hexString: Appcolors.buttons), popViewState: false)
          
        } else {
            hideWhenScroll = false
            universiyListPopView(hidden: false, backgroundColor: UIColor.init(hexString: Appcolors.buttons), tintColor: .white, popViewState: true)
                  universityTableViw.dataSource = self
                  universityTableViw.delegate = self
                  universityTableViw.reloadData()
          
                
        }
      
    }
    @IBAction func searchBtnAction(_ sender: Any) {
        let vc = UIStoryboard.timeLineStoryboard.instantiateSearchVC()
        navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func okBtnAction(_ sender: Any) {
        if okBtn.titleLabel?.text == "OK" {
            
            
            UIView.transition(with: okBtn, duration: 1.5, options: .transitionCrossDissolve, animations: {
              self.okBtn.setTitle("Reset", for: .normal)
                           // self.view.layoutIfNeeded()
            }, completion: { [weak self] finished in
                self?.universiyListPopView(hidden: true, backgroundColor: .white, tintColor: UIColor.init(hexString: Appcolors.buttons), popViewState: false)
                
            })
          
        } else {
           
            
            UIView.transition(with: okBtn, duration: 1.5, options: .transitionCrossDissolve, animations: {
                         self.okBtn.setTitle("OK", for: .normal)
                                      // self.view.layoutIfNeeded()
                       }, completion: { [weak self] finished in
                           self?.universiyListPopView(hidden: true, backgroundColor: .white, tintColor: UIColor.init(hexString: Appcolors.buttons), popViewState: false)
                           
                       })
            
           
    }
    }
    func universiyListPopView(hidden: Bool,backgroundColor: UIColor, tintColor: UIColor, popViewState: Bool) {
        UIView.transition(with: universityListView, duration: 0.4,
                                           options: .transitionCrossDissolve,
                             animations: {
                                 self.universityListView.isHidden = hidden
                         })
                   menuBtn.backgroundColor = backgroundColor
                   menuBtn.tintColor = tintColor
                         universityTableViw.dataSource = self
                         universityTableViw.delegate = self
                         universityTableViw.reloadData()
                  universityPopView = popViewState
    }
    //MARK:- UNFOLLOW API FOR UNIVERSITY
    func unfollowUniversity(id: Int) {
        GraduateUniversityResponse.requestToUnfollowUniversity(userId: id) { [weak self](response) in
           
            
        }
    }
    //MARK:- FOLLOW API FOR UNIVERSITY
    func followUniversity(id: Int) {
        GraduateUniversityResponse.requestToFollowUniversity(userId: id) { (response) in
            
        }
}
}
//MARK:- TABLE DATASOURCE
extension GraduateViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == universityTableViw {
            return universityData.count
        } else {
        return dataSource?.uniData?.data?.count ?? 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == universityTableViw {
            let cell = tableView.dequeueReusableCell(withIdentifier: "UniversityListTableViewCell") as! UniversityListTableViewCell
            cell.contentView.backgroundColor = UIColor.init(hexString: Appcolors.background)
            if universityData[indexPath.row].isSelected == true {
                cell.mainImage.image = UIImage(named: "iconChecked")
            } else {
                cell.mainImage.image = UIImage(named: "iconUnchecked")
            }
            cell.titleLabel.text = universityData[indexPath.row].data
            return cell
        } else {
        let cell = tableView.dequeueReusableCell(withIdentifier: "GraduateTableViewCell", for: indexPath) as! GraduateTableViewCell
        cell.titleLabel.text = dataSource?.uniData?.data?[indexPath.row].businessName
        cell.typeLabel.text = dataSource?.uniData?.data?[indexPath.row].businessType
        if dataSource?.uniData?.data?[indexPath.row].businessCoverImage == nil {
            cell.mainImage.image = UIImage(named: "defaultPicture")
        } else {
            cell.mainImage.setURLImage(imageURL: (GraduateUrl.imageUrl + (dataSource?.uniData?.data?[indexPath.row].businessCoverImage ?? "")))
        }
        
        cell.index = indexPath.row
        cell.delegate = self
        cell.userid = dataSource?.uniData?.data?[indexPath.row].internalIdentifier
        if dataSource?.uniData?.data?[indexPath.row].isFollowed == true {
            cell.followBtn.setTitle("Followed")
            cell.followBtn.backgroundColor = UIColor.init(hexString: Appcolors.buttons)
            cell.followBtn.setTitleColor(.white, for: .normal)
        } else if dataSource?.uniData?.data?[indexPath.row].isOwnBusiness == true {
            cell.followBtn.setTitle("Edit")
            cell.followBtn.backgroundColor = .white
            cell.followBtn.setTitleColor(UIColor.init(hexString: Appcolors.buttons), for: .normal)
        } else if dataSource?.uniData?.data?[indexPath.row].isOwnBusiness == false {
            cell.followBtn.setTitle("Follow")
            cell.followBtn.backgroundColor = .white
            cell.followBtn.setTitleColor(UIColor.init(hexString: Appcolors.buttons), for: .normal)
        }
        return cell
        }
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 273
    }
    
}
extension GraduateViewController: UITableViewDelegate {
//    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//        let allData = self.dataSource
//        print(allData?.loadMore ?? false)
//        print(indexPath.row,"indexPath",indexPath.section)
//        print(dataSource?.uniData?.data?.count,"dataCount")
//        guard allData?.loadMore == true, let dataCount = allData?.uniData?.data?.count, indexPath.row == dataCount - 1, myuni == false  else {return}
//          print(dataCount)
//          let lastSectionIndex = tableView.numberOfSections - 1
//        print(lastSectionIndex)
//          let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
//          if indexPath.section == lastSectionIndex && indexPath.row == lastRowIndex {
//            // print("this is the last cell")
//            let spinner = UIActivityIndicatorView(style: .gray)
//            spinner.startAnimating()
//            spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
//            self.tableView.tableFooterView = spinner
//            self.tableView.tableFooterView?.isHidden = false
//          }
//         // print(allData.url)
//        getUniversityList(pageNo: dataSource?.numOfPage ?? 0, isLoadMore: dataSource!.loadMore, showHud: false)
//          //getNewsBlogData(url: allData.url, pageNo: allData.numOfPage, isLoadMore: allData.loadMore, showHud: false)
//        }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == universityTableViw {
            if universityData[indexPath.row].isSelected == true {
                universityData[indexPath.row].isSelected = false
            } else {
                universityData[indexPath.row].isSelected = true
            }
            universityTableViw.reloadData()
        } else {
        let vc = UIStoryboard.graduateStoryboard.instantiateGraduateDetailsViewController()
        vc.businessId = dataSource?.uniData?.data?[indexPath.row].internalIdentifier
        vc.delegate = self
        navigationController?.pushViewController(vc, animated: true)
    }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
       
        return UITableView.automaticDimension
        
    }
    
   
}

extension GraduateViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if hideWhenScroll == true {
        if tableView == universityTableViw {
            return
        } else if tableView == tableView {
         //MARK-: FOR NOT BOUNCING WHILE PULL TO REFRESH
         if scrollView.contentOffset.y < 0.0 {
             return
         }

         if (scrollView.contentOffset.y >= scrollView.contentSize.height - scrollView.frame.size.height) {
             scrollView.setContentOffset(CGPoint(x: scrollView.contentOffset.x, y: scrollView.contentSize.height - scrollView.frame.size.height), animated: false)
             //
         }
         let delta = scrollView.contentOffset.y - lastContentOffset
         if delta < 0 {
             currentTabBar?.tabBarHeight = min(50, (currentTabBar?.buttonsContainerHeightConstraintInitialConstant)! - delta)

             self.view.layoutIfNeeded()
         } else {

             currentTabBar?.tabBarHeight = max(-35, (currentTabBar?.buttonsContainerHeightConstraintInitialConstant)! - delta)

             self.view.layoutIfNeeded()

         }
         // This makes the + or - number quite small.
         lastContentOffset = scrollView.contentOffset.y
     }
        } else {
            return
        }
    
}
}
extension GraduateViewController: FollowBtnActionFrmGTBC {
    func indexPath(index: Int, btnState: String, userId: Int) {
        if btnState == "Follow" {
                   followUniversity(id: userId)
               } else if btnState == "Followed" {
                   unfollowUniversity(id: userId)
               } else if btnState == "Edit" {
                   let vc = UIStoryboard.graduateStoryboard.instantiateGraduateDetailsViewController()
                          navigationController?.pushViewController(vc, animated: true)
               }
    }
    
    func followAndEditAction() {
        let vc = UIStoryboard.graduateStoryboard.instantiateGraduateDetailsViewController()
        navigationController?.pushViewController(vc, animated: true)
    }
    func indexPath(index: Int, btnState: String) {
       
    }
    
    
}
extension GraduateViewController: ActoinFromGradutaeDetailsVC {
    func action() {
         getUniversityList(pageNo: 1,showHud: true)
    }
    
    
}
struct Universitymodel {
    var title: String?
    var selectionState: Bool?
}

