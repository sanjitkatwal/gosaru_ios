//
//  storyboard+Graduate.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 6/24/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import Foundation
import UIKit
extension UIStoryboard {
private struct Constants {
    static let graduateStoryboard = "Graduate"
    static let graduateIdentifier = "GraduateViewController"
    static let graduateDetailsIdentifier = "GraduateDetailsViewController"
    static let attendingIdentifier = "AttendingViewController"
    static let reportIdentifier = "ReportViewController"


   
}
static var graduateStoryboard: UIStoryboard {
    return UIStoryboard(name: Constants.graduateStoryboard, bundle: nil)
}
func instantiateGraduateViewController() -> GraduateViewController {
    guard let viewController = UIStoryboard.graduateStoryboard.instantiateViewController(withIdentifier: Constants.graduateIdentifier) as? GraduateViewController else {
        fatalError("Couldn't instantiate GraduateViewController")
    }
    return viewController
}
    func instantiateGraduateDetailsViewController() -> GraduateDetailsViewController {
        guard let viewController = UIStoryboard.graduateStoryboard.instantiateViewController(withIdentifier: Constants.graduateDetailsIdentifier) as? GraduateDetailsViewController else {
            fatalError("Couldn't instantiate GraduateDetailsViewController")
        }
        return viewController
    }
    func instantiateAttendingVC() -> AttendingViewController {
           guard let viewController = UIStoryboard.graduateStoryboard.instantiateViewController(withIdentifier: Constants.attendingIdentifier) as? AttendingViewController else {
               fatalError("Couldn't instantiate AttendingViewController")
           }
           return viewController
       }
    func instantiateReportVC() -> ReportViewController {
        guard let viewController = UIStoryboard.graduateStoryboard.instantiateViewController(withIdentifier: Constants.reportIdentifier) as? ReportViewController else {
            fatalError("Couldn't instantiate ReportViewController")
        }
        return viewController
    }
    func instantiateGraduateSettingVC() -> GraduateSettingViewController {
        guard let viewController = UIStoryboard.graduateStoryboard.instantiateViewController(withIdentifier: "GraduateSettingViewController") as? GraduateSettingViewController else {
            fatalError("Couldn't instantiate GraduateSettingViewController")
        }
        return viewController
    }
}

