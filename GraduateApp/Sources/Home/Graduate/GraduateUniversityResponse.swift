//
//  GraduateUniversityResponse.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 9/21/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import Foundation
import ObjectMapper

class GraduateUniversityResponse: DefaultResponse {
    var success: Bool?
override func mapping(map: Map) {
  super.mapping(map: map)
success <- map["success"]
}
// MARK: - GET UNIVERSITY LIST
    class func requestToGraduateUniversityList(pageNo: Int = 1, showLoader: Bool = true,showHud: Bool = true ,completionHandler:@escaping ((GraduateUniversityResponseModel?) -> Void)) {
         let params = ["page": pageNo]
        APIManager(urlString: GraduateUrl.universityListURL, parameters: params,method: .get ).handleResponse(showProgressHud: showHud,completionHandler: { (response: GraduateUniversityResponseModel) in
              print(response)
                  completionHandler(response)
              }, failureBlock: {
                completionHandler(nil)
              })
      }
    
    // MARK: - UNFOLLOW UNIVERSITY
    class func requestToUnfollowUniversity(userId: Int,completionHandler:@escaping ((GraduateUniversityResponse?) -> Void)) {
       
        APIManager(urlString: (GraduateUrl.unfollorUniURL + "\(userId)"), method: .post ).handleResponse(showProgressHud: false, completionHandler: { (response: GraduateUniversityResponse) in
              print(response)
                  completionHandler(response)
              }, failureBlock: {
                completionHandler(nil)
              })
      }
    
    // MARK: - FOLLOW UNIVERSITY
    class func requestToFollowUniversity(userId: Int,completionHandler:@escaping ((GraduateUniversityResponse?) -> Void)) {
          
           APIManager(urlString: (GraduateUrl.followUniURL + "\(userId)"), method: .post ).handleResponse(showProgressHud: false, completionHandler: { (response: GraduateUniversityResponse) in
                 print(response)
                     completionHandler(response)
                 }, failureBlock: {
                   completionHandler(nil)
                 })
         }
    
    // MARK: - GET USER UNIVERSITY
    class func requestToGetUserUni(completionHandler:@escaping ((GraduateUniversityResponseModel?) -> Void)) {
          
           APIManager(urlString: GraduateUrl.userUniURL, method: .get ).handleResponse(showProgressHud: false, completionHandler: { (response: GraduateUniversityResponseModel) in
                 print(response)
                     completionHandler(response)
                 }, failureBlock: {
                   completionHandler(nil)
                 })
         }

}
  class GraduateUniversityResponseModel: DefaultResponse {

// MARK: Declaration for string constants to be used to decode and also serialize.
internal let kGraduateUniversityResponseModelDataKey: String = "data"
internal let kGraduateUniversityResponseModelLinksKey: String = "links"
internal let kGraduateUniversityResponseModelMetaKey: String = "meta"


// MARK: Properties
public var data: [UniversityListData]?
public var links: TimeLineLinks?
public var meta: TimeLineMeta?



// MARK: ObjectMapper Initalizers
/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/

/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/
    override func mapping(map: Map) {
    data <- map[kGraduateUniversityResponseModelDataKey]
    links <- map[kGraduateUniversityResponseModelLinksKey]
    meta <- map[kGraduateUniversityResponseModelMetaKey]

}
}
public class UniversityListData: Mappable {
    public required init?(map: Map) {
        
    }
    

// MARK: Declaration for string constants to be used to decode and also serialize.
internal let kDataBusinessTypeKey: String = "business_type"
internal let kDataHasStarredKey: String = "has_starred"
internal let kDataBusinessCoverImageKey: String = "business_cover_image"
internal let kDataStarredValueKey: String = "starred_value"
internal let kDataInternalIdentifierKey: String = "id"
internal let kDataBusinessNameKey: String = "business_name"
internal let kDataIsFollowedKey: String = "is_followed"
internal let kDataIsOwnBusinessKey: String = "is_own_business"
internal let kDataCanFeatureKey: String = "can_feature"
internal let kDataStarsCountKey: String = "stars_count"
internal let kDataStarsKey: String = "stars"
internal let kDataBusinessProfileImageKey: String = "business_profile_image"


// MARK: Properties
public var businessType: String?
public var hasStarred: Bool = false
public var businessCoverImage: String?
public var starredValue: String?
public var internalIdentifier: Int?
public var businessName: String?
public var isFollowed: Bool = false
public var isOwnBusiness: Bool = false
public var canFeature: Int?
public var starsCount: Int?
public var stars: [TimeLineStars]?
public var businessProfileImage: String?
public var isSelected: Bool? = false
    


// MARK: ObjectMapper Initalizers
/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/

/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/
    public func mapping(map: Map) {
    businessType <- map[kDataBusinessTypeKey]
    hasStarred <- map[kDataHasStarredKey]
    businessCoverImage <- map[kDataBusinessCoverImageKey]
    starredValue <- map[kDataStarredValueKey]
    internalIdentifier <- map[kDataInternalIdentifierKey]
    businessName <- map[kDataBusinessNameKey]
    isFollowed <- map[kDataIsFollowedKey]
    isOwnBusiness <- map[kDataIsOwnBusinessKey]
    canFeature <- map[kDataCanFeatureKey]
    starsCount <- map[kDataStarsCountKey]
    stars <- map[kDataStarsKey]
    businessProfileImage <- map[kDataBusinessProfileImageKey]

}
}
