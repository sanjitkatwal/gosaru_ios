//
//  GraduateSettingViewController.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 12/25/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import UIKit

class GraduateSettingViewController: UIViewController {

    @IBOutlet var hideSocialMedia: UISwitch!
    @IBOutlet var hideSpot: UISwitch!
    @IBOutlet var hideChat: UISwitch!
    @IBOutlet var hideFollowing: UISwitch!
    @IBOutlet var hideMedia: UISwitch!
    var businessId: Int? {
        didSet {
            requestToBusinessSettings()
        }
    }
    var params = [String: Any]()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Settings"
        // Do any additional setup after loading the view.
    }
    func requestToBusinessSettings() {
        let url = GraduateUrl.baseURL + "graduate_university/" + "\(businessId ?? 0)" + "/settings"
        AccountSettingResponse.requestToUserSettings(url: url ,showHud: false) { [weak self](response) in
            guard let strongSelf = self else { return }
            let hideFriends = response?.settings?.filter {
                $0.settingName == "HIDE_MEDIA"
               }
            if hideFriends?[0].settingValue == "1" {
                strongSelf.hideMedia.isOn = true
            } else {
                strongSelf.hideMedia.isOn = false
            }
            let hideMedia = response?.settings?.filter {
             $0.settingName == "HIDE_FOLLOWING"
            }
            if hideMedia?[0].settingValue == "1" {
                strongSelf.hideFollowing.isOn = true
            } else {
                strongSelf.hideFollowing.isOn = false
            }
            let hideaddFriends = response?.settings?.filter {
                        $0.settingName == "HIDE_CHAT"
                       }
            if hideaddFriends?[0].settingValue == "1" {
                strongSelf.hideChat.isOn = true
                   
                } else {
                    strongSelf.hideChat.isOn = false
                }
//            let hideFollowing = response?.settings?.filter {
//             $0.settingName == "HIDE_FOLLOWING"
//            }
//            if hideFollowing?[0].settingValue == "1" {
//                strongSelf.hideSpot.isOn = true
//            } else {
//                strongSelf.hideSpot.isOn = false
//            }
            let hideChat = response?.settings?.filter {
             $0.settingName == "HIDE_SPOT"
            }
            if hideChat?[0].settingValue == "1" {
                strongSelf.hideSpot.isOn = true
            } else {
                strongSelf.hideSpot.isOn = false
            }
            let hideSocialAccount = response?.settings?.filter {
                        $0.settingName == "HIDE_SOCIAL_ACCOUNT"
                       }
                       if hideSocialAccount?[0].settingValue == "1" {
                           strongSelf.hideSocialMedia.isOn = true
                       } else {
                           strongSelf.hideSocialMedia.isOn = false
                       }
            }
        }
    @IBAction func hideMediaAction(_ sender: Any) {
        //MARK: - HIDE MEDIA API
       if hideMedia.isOn == true {
            params = ["setting_name" : "HIDE_MEDIA", "setting_value" : "1"]
       } else {
            params = ["setting_name" : "HIDE_MEDIA", "setting_value" : "0"]
       }
       requesToUpdatSetting(params: params)
    }
    
    @IBAction func hideFollowingAction(_ sender: Any) {
        //MARK:- HIDE FOLLOWINGS
               if hideFollowing.isOn == true {
                           params = ["setting_name" : "HIDE_FOLLOWING", "setting_value" : "1"]
                      } else {
                           params = ["setting_name" : "HIDE_FOLLOWING", "setting_value" : "0"]
                      }
                      requesToUpdatSetting(params: params)
    }
    @IBAction func hideChatAction(_ sender: Any) {
        //MARK:- HIDE CHAT
                      if hideChat.isOn == true {
                                  params = ["setting_name" : "HIDE_CHAT", "setting_value" : "1"]
                             } else {
                                  params = ["setting_name" : "HIDE_CHAT", "setting_value" : "0"]
                             }
                             requesToUpdatSetting(params: params)
    }
    @IBAction func hideSpotAction(_ sender: Any) {
        //MARK:- HIDE SHARING
                             if hideSpot.isOn == true {
                                         params = ["setting_name" : "HIDE_SOCIAL_ACCOUNT", "setting_value" : "1"]
                                    } else {
                                         params = ["setting_name" : "HIDE_SOCIAL_ACCOUNT", "setting_value" : "0"]
                                    }
                                    requesToUpdatSetting(params: params)
    }
    @IBAction func hideSocialMediaAction(_ sender: Any) {
        //MARK:- HIDE SHARING
                             if hideSocialMedia.isOn == true {
                                         params = ["setting_name" : "HIDE_SOCIAL_ACCOUNT", "setting_value" : "1"]
                                    } else {
                                         params = ["setting_name" : "HIDE_SOCIAL_ACCOUNT", "setting_value" : "0"]
                                    }
                                    requesToUpdatSetting(params: params)
    }
    func requesToUpdatSetting(params: [String: Any]) {
        //MARK:- REQUESTING UPDATE USERNAME
        let url = GraduateUrl.baseURL + "graduate_university/" + "\(businessId ?? 0)" + "/settings/update"
        AccountSettingResponse.requestToUpdateBusinessSettings(url: url, params: params) { (response) in
            
        }
    }
}
