//
//  AttendingViewController.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 7/27/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import UIKit

class AttendingViewController: UIViewController {

    @IBOutlet weak var footerView: UIView!
    @IBOutlet weak var closeBtn: UIButton!
    @IBOutlet weak var tableView: UITableView!
    var dataSource: Int? 
       
    override func viewDidLoad() {
        super.viewDidLoad()
        closeBtn.setImage(UIImage(named: "closeButton"), for: .normal)
        // Do any additional setup after loading the view.
        tableView.dataSource = self
        
    }
    @IBAction func closeAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
extension AttendingViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if dataSource == nil || dataSource == 0 {
            return 0
        } else {
            return dataSource!
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if dataSource == nil || dataSource == 0 {
            tableView.tableFooterView = footerView
            return UITableViewCell()
        } else {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AttendingTableViewCell", for: indexPath) as! AttendingTableViewCell
        return cell
        }
    }
    
    
}
