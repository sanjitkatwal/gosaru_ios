//
//  GraduateTableViewCell.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 7/8/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import UIKit
protocol FollowBtnActionFrmGTBC: class {
    func followAndEditAction()
    func indexPath(index: Int, btnState: String, userId: Int)
}
class GraduateTableViewCell: UITableViewCell {

    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var mainImage: UIImageView!
    @IBOutlet weak var followBtn: AnimatedButton!
    weak var delegate: FollowBtnActionFrmGTBC?
    var index: Int?
    var userid: Int?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        containerView.layer.cornerRadius = 15
        mainImage.layer.cornerRadius = 15
        containerView.layer.masksToBounds = false
//        containerView.layer.shadowColor = UIColor.darkGray.cgColor
//                 containerView.layer.shadowOffset = CGSize(width: 1, height: 1.0)
//              containerView.layer.shadowRadius = 3
//              containerView.layer.shadowOpacity = 0.5
//         containerView.layer.shadowPath = UIBezierPath(roundedRect: mainImage.bounds, cornerRadius: 15).cgPath
        titleLabel.lineBreakMode = .byWordWrapping
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func followAction(_ sender: Any) {
        if followBtn.titleLabel?.text == "Follow" {
            followBtn.backgroundColor = UIColor.init(hexString: Appcolors.buttons)
            followBtn.setTitleColor(.white, for: .normal)
            followBtn.setTitle("Followed", for: .normal)
            delegate?.indexPath(index: index!, btnState: "Follow", userId: userid ?? 0)
        } else if followBtn.titleLabel?.text == "Followed" {
            followBtn.backgroundColor = .white
            followBtn.setTitleColor(UIColor.init(hexString: Appcolors.buttons), for: .normal)
            followBtn.setTitle("Follow", for: .normal)
            delegate?.indexPath(index: index!, btnState: "Followed", userId: userid ?? 0)

        } else if followBtn.titleLabel?.text == "Edit"{
            delegate?.indexPath(index: index!, btnState: "Edit", userId: userid ?? 0)
        }
    }
}
