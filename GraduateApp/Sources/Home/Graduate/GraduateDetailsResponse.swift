//
//  GraduateDetailsResponse.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 9/24/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import Foundation
import ObjectMapper
class GraduateDetailsResponse: DefaultResponse {
   
override func mapping(map: Map) {
  super.mapping(map: map)

}
// MARK: - REQUEST TO UNIVERSITY PORTFOLIO LIST
    class func requestToUniPortfolio(param: [String : Any],url: String,showHud: Bool = true ,completionHandler:@escaping ((UniversityPortFolioResponseModel?) -> Void)) {
        APIManager(urlString: url, parameters: param,method: .get ).handleResponse(showProgressHud: showHud,completionHandler: { (response: UniversityPortFolioResponseModel) in
              print(response)
                  completionHandler(response)
              }, failureBlock: {
                completionHandler(nil)
              })
      }
    // MARK: - REQUEST TO UNIVERSITY OFFRES LIST
    class func requestToUniOffers(param: [String : Any],url: String,showHud: Bool = true ,completionHandler:@escaping ((UniOfferModel?) -> Void)) {
        APIManager(urlString: url, parameters: param,method: .get ).handleResponse(showProgressHud: showHud,completionHandler: { (response: UniOfferModel) in
              print(response)
                  completionHandler(response)
              }, failureBlock: {
                completionHandler(nil)
              })
      }
     // MARK: - REQUEST TO EVENTS  LIST
    class func requestToEvents(param: [String : Any],url: String,showHud: Bool = true ,completionHandler:@escaping ((UniEventResponseModel?) -> Void)) {
      APIManager(urlString: url, parameters: param,method: .get ).handleResponse(showProgressHud: showHud,completionHandler: { (response: UniEventResponseModel) in
            print(response)
                completionHandler(response)
            }, failureBlock: {
              completionHandler(nil)
            })
    }
    // MARK: - REQUEST TO MYPOST  LIST
    class func requestToMypost(param: [String : Any],url: String,showHud: Bool = true ,completionHandler:@escaping ((UniMyPostResponseModel?) -> Void)) {
      APIManager(urlString: url, parameters: param,method: .get ).handleResponse(showProgressHud: showHud,completionHandler: { (response: UniMyPostResponseModel) in
            print(response)
                completionHandler(response)
            }, failureBlock: {
              completionHandler(nil)
            })
    }
    // MARK: - REQUEST TO MYPOST  LIST
       class func requestToDeleteUni(url: String,showHud: Bool = true ,completionHandler:@escaping ((UniMyPostResponseModel?) -> Void)) {
         APIManager(urlString: url,method: .delete ).handleResponse(showProgressHud: showHud,completionHandler: { (response: UniMyPostResponseModel) in
               print(response)
                   completionHandler(response)
               }, failureBlock: {
                 completionHandler(nil)
               })
       }
    // MARK: - REQUEST TO CREATE PORTFOLIO
          class func requestToCreatePortFolio(url: String,showHud: Bool = true ,completionHandler:@escaping ((UniMyPostResponseModel?) -> Void)) {
            APIManager(urlString: url,method: .delete ).handleResponse(showProgressHud: showHud,completionHandler: { (response: UniMyPostResponseModel) in
                  print(response)
                      completionHandler(response)
                  }, failureBlock: {
                    completionHandler(nil)
                  })
          }
    // MARK: - REQUEST TO ATTENDIGNORE EVENTS
             class func requestToAttendIgnoreEvents(url: String,showHud: Bool = false ,completionHandler:@escaping ((UniMyPostResponseModel?) -> Void)) {
               APIManager(urlString: url,method: .post ).handleResponse(showProgressHud: showHud,completionHandler: { (response: UniMyPostResponseModel) in
                     print(response)
                         completionHandler(response)
                     }, failureBlock: {
                       completionHandler(nil)
                     })
             }
    // MARK: - REQUEST TO DELETE PORTOLIO
    class func requestToDeletePortfolio(id: Int,url: String,showHud: Bool = false ,completionHandler:@escaping ((UniMyPostResponseModel?) -> Void)) {
        APIManager(urlString: GraduateUrl.deletePortFolioURL + "\(id)",method: .delete ).handleResponse(showProgressHud: showHud,completionHandler: { (response: UniMyPostResponseModel) in
                     print(response)
                         completionHandler(response)
                     }, failureBlock: {
                       completionHandler(nil)
                     })
             }
    // MARK: - REQUEST TO DELETE OFFER
    class func requestToDeleteOffer(id: Int,url: String,showHud: Bool = false ,completionHandler:@escaping ((UniMyPostResponseModel?) -> Void)) {
        APIManager(urlString: GraduateUrl.deleteEventURL + "\(id)",method: .delete ).handleResponse(showProgressHud: showHud,completionHandler: { (response: UniMyPostResponseModel) in
                     print(response)
                         completionHandler(response)
                     }, failureBlock: {
                       completionHandler(nil)
                     })
             }
    // MARK: - REQUEST TO DELETE EVENT
    class func requestToDeleteEvent(id: Int,url: String,showHud: Bool = false ,completionHandler:@escaping ((UniMyPostResponseModel?) -> Void)) {
        APIManager(urlString: GraduateUrl.deleteEventURL + "\(id)",method: .delete ).handleResponse(showProgressHud: showHud,completionHandler: { (response: UniMyPostResponseModel) in
                     print(response)
                         completionHandler(response)
                     }, failureBlock: {
                       completionHandler(nil)
                     })
             }
}
 class UniversityPortFolioResponseModel: DefaultResponse {

// MARK: Declaration for string constants to be used to decode and also serialize.
internal let kUniversityPortFolioResponseDataKey: String = "data"
internal let kUniversityPortFolioResponseLinksKey: String = "links"
internal let kUniversityPortFolioResponseMetaKey: String = "meta"


// MARK: Properties
public var data: [uniMyPostData]?
public var links: TimeLineLinks?
public var meta: TimeLineMeta?



// MARK: ObjectMapper Initalizers
/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/


/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/
    override func mapping(map: Map) {
    data <- map[kUniversityPortFolioResponseDataKey]
    links <- map[kUniversityPortFolioResponseLinksKey]
    meta <- map[kUniversityPortFolioResponseMetaKey]

}
}
public class UniPortFolioData: Mappable {
    public required init?(map: Map) {
        
    }
    
// MARK: Declaration for string constants to be used to decode and also serialize.
internal let kDataHasStarredKey: String = "has_starred"
internal let kDataPostingAsKey: String = "posting_as"
internal let kDataBusinessKey: String = "business"
internal let kDataGalleriesKey: String = "galleries"
internal let kDataHasSharedKey: String = "has_shared"
internal let kDataEventKey: String = "event"
internal let kDataViewsCountKey: String = "views_count"
internal let kDataHasCommentedKey: String = "has_commented"
internal let kDataIsSharedPostKey: String = "is_shared_post"
internal let kDataInternalIdentifierKey: String = "id"
internal let kDataUpdatedAtKey: String = "updated_at"
internal let kDataSharedCountKey: String = "shared_count"
internal let kDataCommentsKey: String = "comments"
internal let kDataStarsCountKey: String = "stars_count"
internal let kDataPortfolioKey: String = "portfolio"
internal let kDataCreatedAtKey: String = "created_at"
internal let kDataPostingAsTypeKey: String = "posting_as_type"
internal let kDataSourceCountryKey: String = "source_country"
internal let kDataVisibilityTypeKey: String = "visibility_type"
internal let kDataIsOwnerKey: String = "is_owner"
internal let kDataOfferKey: String = "offer"
internal let kDataStarsKey: String = "stars"
internal let kDataStarredValueKey: String = "starred_value"
internal let kDataMessageKey: String = "message"
internal let kDataUserIdKey: String = "user_id"


// MARK: Properties
public var hasStarred: Bool = false
public var postingAs: TimeLinePostingAs?
public var business: Business?
 var galleries: [TimeLineGalleries]?
public var hasShared: Bool = false
public var event: Event?
public var viewsCount: String?
public var hasCommented: Bool = false
public var isSharedPost: Bool = false
public var internalIdentifier: Int?
public var updatedAt: String?
public var sharedCount: Int?
 var comments: [Comments]?
public var starsCount: Int?
public var portfolio: UniPortfolio?
public var createdAt: String?
public var postingAsType: String?
public var sourceCountry: String?
public var visibilityType: String?
public var isOwner: Bool = false
 var offer: Offer?
public var stars: TimeLineStars?
public var starredValue: String?
public var message: String?
public var userId: Int?



// MARK: ObjectMapper Initalizers
/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/


/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/
    public func mapping(map: Map) {
    hasStarred <- map[kDataHasStarredKey]
    postingAs <- map[kDataPostingAsKey]
    business <- map[kDataBusinessKey]
    galleries <- map[kDataGalleriesKey]
    hasShared <- map[kDataHasSharedKey]
    event <- map[kDataEventKey]
    viewsCount <- map[kDataViewsCountKey]
    hasCommented <- map[kDataHasCommentedKey]
    isSharedPost <- map[kDataIsSharedPostKey]
    internalIdentifier <- map[kDataInternalIdentifierKey]
    updatedAt <- map[kDataUpdatedAtKey]
    sharedCount <- map[kDataSharedCountKey]
    comments <- map[kDataCommentsKey]
    starsCount <- map[kDataStarsCountKey]
    portfolio <- map[kDataPortfolioKey]
    createdAt <- map[kDataCreatedAtKey]
    postingAsType <- map[kDataPostingAsTypeKey]
    sourceCountry <- map[kDataSourceCountryKey]
    visibilityType <- map[kDataVisibilityTypeKey]
    isOwner <- map[kDataIsOwnerKey]
    offer <- map[kDataOfferKey]
    stars <- map[kDataStarsKey]
    starredValue <- map[kDataStarredValueKey]
    message <- map[kDataMessageKey]
    userId <- map[kDataUserIdKey]

}
}
public class UniPortFolioBusiness: Mappable {
    public required init?(map: Map) {
        
    }
    

// MARK: Declaration for string constants to be used to decode and also serialize.
internal let kBusinessBusinessTypeKey: String = "business_type"
internal let kBusinessFacebookKey: String = "facebook"
internal let kBusinessBusinessCoverImageKey: String = "business_cover_image"
internal let kBusinessStateKey: String = "state"
internal let kBusinessEstablishedKey: String = "established"
internal let kBusinessOpeningHourKey: String = "opening_hour"
internal let kBusinessOperatingRadiusKey: String = "operating_radius"
internal let kBusinessWebsiteLinkKey: String = "website_link"
internal let kBusinessInstagramKey: String = "instagram"
internal let kBusinessAddressStringKey: String = "address_string"
internal let kBusinessBusinessNameKey: String = "business_name"
internal let kBusinessModeratedAtKey: String = "moderated_at"
internal let kBusinessDefaultMusicKey: String = "default_music"
internal let kBusinessInternalIdentifierKey: String = "id"
internal let kBusinessUpdatedAtKey: String = "updated_at"
internal let kBusinessPhoneKey: String = "phone"
internal let kBusinessLonKey: String = "lon"
internal let kBusinessMaxCustomerHandleKey: String = "max_customer_handle"
internal let kBusinessTwitterKey: String = "twitter"
internal let kBusinessAboutBusinessKey: String = "about_business"
internal let kBusinessStatusKey: String = "status"
internal let kBusinessCreatedAtKey: String = "created_at"
internal let kBusinessStreetAddressKey: String = "street_address"
internal let kBusinessCountryKey: String = "country"
internal let kBusinessPostcodeKey: String = "postcode"
internal let kBusinessAbnAcnKey: String = "abn_acn"
internal let kBusinessEmailKey: String = "email"
internal let kBusinessLatKey: String = "lat"
internal let kBusinessUsernameKey: String = "username"
internal let kBusinessCanFeatureKey: String = "can_feature"
internal let kBusinessTotalEmployeesKey: String = "total_employees"
internal let kBusinessUserIdKey: String = "user_id"
internal let kBusinessSuburbKey: String = "suburb"
internal let kBusinessBusinessProfileImageKey: String = "business_profile_image"


// MARK: Properties
public var businessType: String?
public var facebook: String?
public var businessCoverImage: String?
public var state: String?
public var established: String?
public var openingHour: String?
public var operatingRadius: String?
public var websiteLink: String?
public var instagram: String?
public var addressString: String?
public var businessName: String?
public var moderatedAt: String?
public var defaultMusic: String?
public var internalIdentifier: Int?
public var updatedAt: String?
public var phone: String?
public var lon: String?
public var maxCustomerHandle: String?
public var twitter: String?
public var aboutBusiness: String?
public var status: Int?
public var createdAt: String?
public var streetAddress: String?
public var country: String?
public var postcode: String?
public var abnAcn: String?
public var email: String?
public var lat: String?
public var username: String?
public var canFeature: Int?
public var totalEmployees: String?
public var userId: Int?
public var suburb: String?
public var businessProfileImage: String?



// MARK: ObjectMapper Initalizers
/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/


/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/
    public func mapping(map: Map) {
    businessType <- map[kBusinessBusinessTypeKey]
    facebook <- map[kBusinessFacebookKey]
    businessCoverImage <- map[kBusinessBusinessCoverImageKey]
    state <- map[kBusinessStateKey]
    established <- map[kBusinessEstablishedKey]
    openingHour <- map[kBusinessOpeningHourKey]
    operatingRadius <- map[kBusinessOperatingRadiusKey]
    websiteLink <- map[kBusinessWebsiteLinkKey]
    instagram <- map[kBusinessInstagramKey]
    addressString <- map[kBusinessAddressStringKey]
    businessName <- map[kBusinessBusinessNameKey]
    moderatedAt <- map[kBusinessModeratedAtKey]
    defaultMusic <- map[kBusinessDefaultMusicKey]
    internalIdentifier <- map[kBusinessInternalIdentifierKey]
    updatedAt <- map[kBusinessUpdatedAtKey]
    phone <- map[kBusinessPhoneKey]
    lon <- map[kBusinessLonKey]
    maxCustomerHandle <- map[kBusinessMaxCustomerHandleKey]
    twitter <- map[kBusinessTwitterKey]
    aboutBusiness <- map[kBusinessAboutBusinessKey]
    status <- map[kBusinessStatusKey]
    createdAt <- map[kBusinessCreatedAtKey]
    streetAddress <- map[kBusinessStreetAddressKey]
    country <- map[kBusinessCountryKey]
    postcode <- map[kBusinessPostcodeKey]
    abnAcn <- map[kBusinessAbnAcnKey]
    email <- map[kBusinessEmailKey]
    lat <- map[kBusinessLatKey]
    username <- map[kBusinessUsernameKey]
    canFeature <- map[kBusinessCanFeatureKey]
    totalEmployees <- map[kBusinessTotalEmployeesKey]
    userId <- map[kBusinessUserIdKey]
    suburb <- map[kBusinessSuburbKey]
    businessProfileImage <- map[kBusinessBusinessProfileImageKey]

}
}
public class UniPortfolio: Mappable {
    public required init?(map: Map) {
        
    }
    

// MARK: Declaration for string constants to be used to decode and also serialize.
internal let kPortfolioCreatedAtKey: String = "created_at"
internal let kPortfolioBusinessKey: String = "business"
internal let kPortfolioGalleriesKey: String = "galleries"
internal let kPortfolioCountryKey: String = "country"
internal let kPortfolioDateKey: String = "date"
internal let kPortfolioDescriptionValueKey: String = "description"
internal let kPortfolioPostcodeKey: String = "postcode"
internal let kPortfolioSuburbKey: String = "suburb "
internal let kPortfolioBusinessIdKey: String = "business_id"
internal let kPortfolioLatKey: String = "lat"
internal let kPortfolioAddressStringKey: String = "address_string"
internal let kPortfolioTypeKey: String = "type"
internal let kPortfolioNameKey: String = "name"
internal let kPortfolioInternalIdentifierKey: String = "id"
internal let kPortfolioStreetKey: String = "street"
internal let kPortfolioUpdatedAtKey: String = "updated_at"
internal let kPortfolioLonKey: String = "lon"
internal let kPortfolioStateKey: String = "state"


// MARK: Properties
public var createdAt: String?
public var business: Business?
 var galleries: [TimeLineGalleries]?
public var country: String?
public var date: String?
public var descriptionValue: String?
public var postcode: String?
public var suburb: String?
public var businessId: Int?
public var lat: String?
public var addressString: String?
public var type: String?
public var name: String?
public var internalIdentifier: Int?
public var street: String?
public var updatedAt: String?
public var lon: String?
public var state: String?



// MARK: ObjectMapper Initalizers
/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/


/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/
    public func mapping(map: Map) {
    createdAt <- map[kPortfolioCreatedAtKey]
    business <- map[kPortfolioBusinessKey]
    galleries <- map[kPortfolioGalleriesKey]
    country <- map[kPortfolioCountryKey]
    date <- map[kPortfolioDateKey]
    descriptionValue <- map[kPortfolioDescriptionValueKey]
    postcode <- map[kPortfolioPostcodeKey]
    suburb <- map[kPortfolioSuburbKey]
    businessId <- map[kPortfolioBusinessIdKey]
    lat <- map[kPortfolioLatKey]
    addressString <- map[kPortfolioAddressStringKey]
    type <- map[kPortfolioTypeKey]
    name <- map[kPortfolioNameKey]
    internalIdentifier <- map[kPortfolioInternalIdentifierKey]
    street <- map[kPortfolioStreetKey]
    updatedAt <- map[kPortfolioUpdatedAtKey]
    lon <- map[kPortfolioLonKey]
    state <- map[kPortfolioStateKey]

}
}
class UniversityOfferMode: UniversityPortFolioResponseModel {
    
}
 class UniOfferModel: DefaultResponse {

// MARK: Declaration for string constants to be used to decode and also serialize.
internal let kUniOfferModelDataKey: String = "data"
internal let kUniOfferModelLinksKey: String = "links"
internal let kUniOfferModelMetaKey: String = "meta"


// MARK: Properties
public var data: [uniMyPostData]?
 var links: TimeLineLinks?
 var meta: TimeLineMeta?



// MARK: ObjectMapper Initalizers
/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/


/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/
    public override func mapping(map: Map) {
    data <- map[kUniOfferModelDataKey]
    links <- map[kUniOfferModelLinksKey]
    meta <- map[kUniOfferModelMetaKey]

}
}
public class UniOfferData: Mappable {
    public required init?(map: Map) {
        
    }
    

// MARK: Declaration for string constants to be used to decode and also serialize.
internal let kDataHasStarredKey: String = "has_starred"
internal let kDataPostingAsKey: String = "posting_as"
internal let kDataBusinessKey: String = "business"
internal let kDataGalleriesKey: String = "galleries"
internal let kDataHasSharedKey: String = "has_shared"
internal let kDataEventKey: String = "event"
internal let kDataViewsCountKey: String = "views_count"
internal let kDataHasCommentedKey: String = "has_commented"
internal let kDataIsSharedPostKey: String = "is_shared_post"
internal let kDataInternalIdentifierKey: String = "id"
internal let kDataUpdatedAtKey: String = "updated_at"
internal let kDataSharedCountKey: String = "shared_count"
internal let kDataCommentsKey: String = "comments"
internal let kDataStarsCountKey: String = "stars_count"
internal let kDataPortfolioKey: String = "portfolio"
internal let kDataCreatedAtKey: String = "created_at"
internal let kDataPostingAsTypeKey: String = "posting_as_type"
internal let kDataSourceCountryKey: String = "source_country"
internal let kDataVisibilityTypeKey: String = "visibility_type"
internal let kDataIsOwnerKey: String = "is_owner"
internal let kDataOfferKey: String = "offer"
internal let kDataStarsKey: String = "stars"
internal let kDataStarredValueKey: String = "starred_value"
internal let kDataMessageKey: String = "message"
internal let kDataUserIdKey: String = "user_id"


// MARK: Properties
public var hasStarred: Bool = false
public var postingAs: TimeLinePostingAs?
public var business: Business?
 var galleries: [TimeLineGalleries]?
public var hasShared: Bool = false
public var event: Event?
public var viewsCount: Int?
public var hasCommented: Bool = false
public var isSharedPost: Bool = false
public var internalIdentifier: Int?
public var updatedAt: String?
public var sharedCount: Int?
 var comments: [Comments]?
public var starsCount: Int?
public var portfolio: String?
public var createdAt: String?
public var postingAsType: String?
public var sourceCountry: String?
public var visibilityType: String?
public var isOwner: Bool = false
 var offer: Offer?
public var stars: [TimeLineStars]?
public var starredValue: Bool?
public var message: String?
public var userId: Int?



// MARK: ObjectMapper Initalizers
/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/


/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/
    public func mapping(map: Map) {
    hasStarred <- map[kDataHasStarredKey]
    postingAs <- map[kDataPostingAsKey]
    business <- map[kDataBusinessKey]
    galleries <- map[kDataGalleriesKey]
    hasShared <- map[kDataHasSharedKey]
    event <- map[kDataEventKey]
    viewsCount <- map[kDataViewsCountKey]
    hasCommented <- map[kDataHasCommentedKey]
    isSharedPost <- map[kDataIsSharedPostKey]
    internalIdentifier <- map[kDataInternalIdentifierKey]
    updatedAt <- map[kDataUpdatedAtKey]
    sharedCount <- map[kDataSharedCountKey]
    comments <- map[kDataCommentsKey]
    starsCount <- map[kDataStarsCountKey]
    portfolio <- map[kDataPortfolioKey]
    createdAt <- map[kDataCreatedAtKey]
    postingAsType <- map[kDataPostingAsTypeKey]
    sourceCountry <- map[kDataSourceCountryKey]
    visibilityType <- map[kDataVisibilityTypeKey]
    isOwner <- map[kDataIsOwnerKey]
    offer <- map[kDataOfferKey]
    stars <- map[kDataStarsKey]
    starredValue <- map[kDataStarredValueKey]
    message <- map[kDataMessageKey]
    userId <- map[kDataUserIdKey]

}
}
 class UniEventResponseModel: DefaultResponse {

// MARK: Declaration for string constants to be used to decode and also serialize.
internal let kUniEventResponseModelDataKey: String = "data"
internal let kUniEventResponseModelLinksKey: String = "links"
internal let kUniEventResponseModelMetaKey: String = "meta"


// MARK: Properties
public var data: [uniMyPostData]?
public var links: TimeLineLinks?
public var meta: TimeLineMeta?



// MARK: ObjectMapper Initalizers
/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/

/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/
    override func mapping(map: Map) {
    data <- map[kUniEventResponseModelDataKey]
    links <- map[kUniEventResponseModelLinksKey]
    meta <- map[kUniEventResponseModelMetaKey]

}
}
public class UniEventsData: Mappable {
    public required init?(map: Map) {
        
    }
    

    // MARK: Declaration for string constants to be used to decode and also serialize.
    internal let kDataHasStarredKey: String = "has_starred"
    internal let kDataPostingAsKey: String = "posting_as"
    internal let kDataBusinessKey: String = "business"
    internal let kDataGalleriesKey: String = "galleries"
    internal let kDataHasSharedKey: String = "has_shared"
    internal let kDataEventKey: String = "event"
    internal let kDataViewsCountKey: String = "views_count"
    internal let kDataHasCommentedKey: String = "has_commented"
    internal let kDataIsSharedPostKey: String = "is_shared_post"
    internal let kDataInternalIdentifierKey: String = "id"
    internal let kDataUpdatedAtKey: String = "updated_at"
    internal let kDataSharedCountKey: String = "shared_count"
    internal let kDataCommentsKey: String = "comments"
    internal let kDataStarsCountKey: String = "stars_count"
    internal let kDataPortfolioKey: String = "portfolio"
    internal let kDataCreatedAtKey: String = "created_at"
    internal let kDataPostingAsTypeKey: String = "posting_as_type"
    internal let kDataSourceCountryKey: String = "source_country"
    internal let kDataVisibilityTypeKey: String = "visibility_type"
    internal let kDataIsOwnerKey: String = "is_owner"
    internal let kDataOfferKey: String = "offer"
    internal let kDataStarsKey: String = "stars"
    internal let kDataStarredValueKey: String = "starred_value"
    internal let kDataMessageKey: String = "message"
    internal let kDataUserIdKey: String = "user_id"


    // MARK: Properties
    public var hasStarred: Bool = false
    public var postingAs: TimeLinePostingAs?
    public var business: Business?
     var galleries: [TimeLineGalleries]?
    public var hasShared: Bool = false
    public var event: Event?
    public var viewsCount: Int?
    public var hasCommented: Bool = false
    public var isSharedPost: Bool = false
    public var internalIdentifier: Int?
    public var updatedAt: String?
    public var sharedCount: Int?
     var comments: [Comments]?
    public var starsCount: Int?
    public var portfolio: String?
    public var createdAt: String?
    public var postingAsType: String?
    public var sourceCountry: String?
    public var visibilityType: String?
    public var isOwner: Bool = false
    public var offer: Offer?
    public var stars: [TimeLineStars]?
    public var starredValue: Bool?
    public var message: String?
    public var userId: Int?



    // MARK: ObjectMapper Initalizers
    /**
    Map a JSON object to this class using ObjectMapper
    - parameter map: A mapping from ObjectMapper
    */
  

    /**
    Map a JSON object to this class using ObjectMapper
    - parameter map: A mapping from ObjectMapper
    */
        public func mapping(map: Map) {
        hasStarred <- map[kDataHasStarredKey]
        postingAs <- map[kDataPostingAsKey]
        business <- map[kDataBusinessKey]
        galleries <- map[kDataGalleriesKey]
        hasShared <- map[kDataHasSharedKey]
        event <- map[kDataEventKey]
        viewsCount <- map[kDataViewsCountKey]
        hasCommented <- map[kDataHasCommentedKey]
        isSharedPost <- map[kDataIsSharedPostKey]
        internalIdentifier <- map[kDataInternalIdentifierKey]
        updatedAt <- map[kDataUpdatedAtKey]
        sharedCount <- map[kDataSharedCountKey]
        comments <- map[kDataCommentsKey]
        starsCount <- map[kDataStarsCountKey]
        portfolio <- map[kDataPortfolioKey]
        createdAt <- map[kDataCreatedAtKey]
        postingAsType <- map[kDataPostingAsTypeKey]
        sourceCountry <- map[kDataSourceCountryKey]
        visibilityType <- map[kDataVisibilityTypeKey]
        isOwner <- map[kDataIsOwnerKey]
        offer <- map[kDataOfferKey]
        stars <- map[kDataStarsKey]
        starredValue <- map[kDataStarredValueKey]
        message <- map[kDataMessageKey]
        userId <- map[kDataUserIdKey]

    }
}
 class UniMyPostResponseModel: DefaultResponse {

// MARK: Declaration for string constants to be used to decode and also serialize.
internal let kUniMyPostResponseModelDataKey: String = "data"
internal let kUniMyPostResponseModelLinksKey: String = "links"
internal let kUniMyPostResponseModelMetaKey: String = "meta"


// MARK: Properties
public var data: [uniMyPostData]?
 var links: TimeLineLinks?
 var meta: TimeLineMeta?



// MARK: ObjectMapper Initalizers
/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/


/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/
    public override func mapping(map: Map) {
    data <- map[kUniMyPostResponseModelDataKey]
    links <- map[kUniMyPostResponseModelLinksKey]
    meta <- map[kUniMyPostResponseModelMetaKey]

}
}
public class uniMyPostData: Mappable {
    public required init?(map: Map) {
        
    }
    

// MARK: Declaration for string constants to be used to decode and also serialize.
internal let kDataHasStarredKey: String = "has_starred"
internal let kDataPostingAsKey: String = "posting_as"
internal let kDataBusinessKey: String = "business"
internal let kDataGalleriesKey: String = "galleries"
internal let kDataHasSharedKey: String = "has_shared"
internal let kDataEventKey: String = "event"
internal let kDataViewsCountKey: String = "views_count"
internal let kDataHasCommentedKey: String = "has_commented"
internal let kDataIsSharedPostKey: String = "is_shared_post"
internal let kDataInternalIdentifierKey: String = "id"
internal let kDataUpdatedAtKey: String = "updated_at"
internal let kDataSharedCountKey: String = "shared_count"
internal let kDataCommentsKey: String = "comments"
internal let kDataStarsCountKey: String = "stars_count"
internal let kDataPortfolioKey: String = "portfolio"
internal let kDataCreatedAtKey: String = "created_at"
internal let kDataPostingAsTypeKey: String = "posting_as_type"
internal let kDataSourceCountryKey: String = "source_country"
internal let kDataVisibilityTypeKey: String = "visibility_type"
internal let kDataIsOwnerKey: String = "is_owner"
internal let kDataOfferKey: String = "offer"
internal let kDataStarsKey: String = "stars"
internal let kDataStarredValueKey: String = "starred_value"
internal let kDataMessageKey: String = "message"
internal let kDataUserIdKey: String = "user_id"


// MARK: Properties
public var hasStarred: Bool = false
public var postingAs: TimeLinePostingAs?
public var business: Business?
 var galleries: [TimeLineGalleries]?
public var hasShared: Bool = false
public var event: Event?
public var viewsCount: Int?
public var hasCommented: Bool = false
public var isSharedPost: Bool = false
public var internalIdentifier: Int?
public var updatedAt: String?
public var sharedCount: Int?
 var comments: [Comments]?
public var starsCount: Int?
public var portfolio: UniPortfolio?
public var createdAt: String?
public var postingAsType: String?
public var sourceCountry: String?
public var visibilityType: String?
public var isOwner: Bool = false
public var offer: Offer?
public var stars: TimeLineStars?
public var starredValue: String?
public var message: String?
public var userId: Int?
    public var liveKey: String?
    public var liveVideoMp4: String?
    public var liveVideoRtmp: String?
    public var hasLiveEnded: Bool?
    public var attendCount: Int?



// MARK: ObjectMapper Initalizers
/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/

/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/
    public func mapping(map: Map) {
    hasStarred <- map[kDataHasStarredKey]
    postingAs <- map[kDataPostingAsKey]
    business <- map[kDataBusinessKey]
    galleries <- map[kDataGalleriesKey]
    hasShared <- map[kDataHasSharedKey]
    event <- map[kDataEventKey]
    viewsCount <- map[kDataViewsCountKey]
    hasCommented <- map[kDataHasCommentedKey]
    isSharedPost <- map[kDataIsSharedPostKey]
    internalIdentifier <- map[kDataInternalIdentifierKey]
    updatedAt <- map[kDataUpdatedAtKey]
    sharedCount <- map[kDataSharedCountKey]
    comments <- map[kDataCommentsKey]
    starsCount <- map[kDataStarsCountKey]
    portfolio <- map[kDataPortfolioKey]
    createdAt <- map[kDataCreatedAtKey]
    postingAsType <- map[kDataPostingAsTypeKey]
    sourceCountry <- map[kDataSourceCountryKey]
    visibilityType <- map[kDataVisibilityTypeKey]
    isOwner <- map[kDataIsOwnerKey]
    offer <- map[kDataOfferKey]
    stars <- map[kDataStarsKey]
    starredValue <- map[kDataStarredValueKey]
    message <- map[kDataMessageKey]
    userId <- map[kDataUserIdKey]
     liveKey <- map["live_key"]
        liveVideoMp4 <- map["live_video_mp4"]
        liveVideoRtmp <- map["live_video_rtmp"]
        attendCount <- map["attend_count"]
        hasLiveEnded <- map["has_live_ended"]

}
}
public class Offer: Mappable {
    public required init?(map: Map) {
        
    }
    

// MARK: Declaration for string constants to be used to decode and also serialize.
internal let kOfferCreatedAtKey: String = "created_at"
internal let kOfferGalleriesKey: String = "galleries"
internal let kOfferCountryKey: String = "country"
internal let kOfferDescriptionValueKey: String = "description"
internal let kOfferPostcodeKey: String = "postcode"
internal let kOfferBusinessIdKey: String = "business_id"
internal let kOfferLatKey: String = "lat"
internal let kOfferAddressStringKey: String = "address_string"
internal let kOfferTypeKey: String = "type"
internal let kOfferInternalIdentifierKey: String = "id"
internal let kOfferTermsKey: String = "terms"
internal let kOfferStreetKey: String = "street"
internal let kOfferUpdatedAtKey: String = "updated_at"
internal let kOfferLonKey: String = "lon"
internal let kOfferDiscountPercentageKey: String = "discount_percentage"
internal let kOfferDiscountAmountKey: String = "discount_amount"
internal let kOfferExpiryDateKey: String = "expiry_date"
internal let kOfferSuburbKey: String = "suburb"
internal let kOfferStateKey: String = "state"


// MARK: Properties
public var createdAt: String?
 var galleries: [TimeLineGalleries]?
public var country: String?
public var descriptionValue: String?
public var postcode: String?
public var businessId: Int?
public var lat: String?
public var addressString: String?
public var type: String?
public var internalIdentifier: Int?
public var terms: String?
public var street: String?
public var updatedAt: String?
public var lon: String?
public var discountPercentage: String?
public var discountAmount: String?
public var expiryDate: String?
public var suburb: String?
public var state: String?



// MARK: ObjectMapper Initalizers
/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/


/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/
    public func mapping(map: Map) {
    createdAt <- map[kOfferCreatedAtKey]
    galleries <- map[kOfferGalleriesKey]
    country <- map[kOfferCountryKey]
    descriptionValue <- map[kOfferDescriptionValueKey]
    postcode <- map[kOfferPostcodeKey]
    businessId <- map[kOfferBusinessIdKey]
    lat <- map[kOfferLatKey]
    addressString <- map[kOfferAddressStringKey]
    type <- map[kOfferTypeKey]
    internalIdentifier <- map[kOfferInternalIdentifierKey]
    terms <- map[kOfferTermsKey]
    street <- map[kOfferStreetKey]
    updatedAt <- map[kOfferUpdatedAtKey]
    lon <- map[kOfferLonKey]
    discountPercentage <- map[kOfferDiscountPercentageKey]
    discountAmount <- map[kOfferDiscountAmountKey]
    expiryDate <- map[kOfferExpiryDateKey]
    suburb <- map[kOfferSuburbKey]
    state <- map[kOfferStateKey]

}
}
public class CreateErrors: Mappable {
    public required init?(map: Map) {
           
       }
    public var eventStartDate: [String]?
    public var eventStartTime: [String]?
    public var eventTitle: [String]?
    public var postAs: postAs?
    public var message: String?
    
    public func mapping(map: Map) {
        eventStartDate <- map["event_start_date"]
        eventStartTime <- map["event_start_time"]
        eventTitle <- map["event_title"]
        message <- map["message"]
    }
}
public class postAs: Mappable {
    public required init?(map: Map) {
           
       }
    public var postAs: [String]?
    
    public func mapping(map: Map) {
        postAs <- map["post_as"]
    }
}
