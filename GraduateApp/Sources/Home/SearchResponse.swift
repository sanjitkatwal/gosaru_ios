//
//  SearchResponse.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 12/8/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import Foundation
import ObjectMapper

class SearchResponse: DefaultResponse {
    
override func mapping(map: Map) {
  super.mapping(map: map)

}
// MARK: - GET MESSAGE CONVERSATION
    class func requestToSearch(search: String,showHud: Bool,completionHandler:@escaping ((SearchResponseModel?) -> Void)) {
        let param = ["search" : search]
        APIManager(urlString: (GraduateUrl.searchURL),parameters: param,method: .get).handleResponse(showProgressHud: showHud,completionHandler: { (response: SearchResponseModel) in
              print(response)
                  completionHandler(response)
              }, failureBlock: {
                completionHandler(nil)
              })
      }
}

 class SearchResponseModel: DefaultResponse {

    // MARK: Declaration for string constants to be used to decode and also serialize.
    internal let kSearchResponseModelPostsKey: String = "posts"
    internal let kSearchResponseModelBusinessKey: String = "business"
    internal let kSearchResponseModelUsersKey: String = "users"


    // MARK: Properties
    public var posts: [SearchPosts]?
    public var business: [SearchBusiness]?
    public var users: [SearchUsers]?



    // MARK: ObjectMapper Initalizers
    /**
    Map a JSON object to this class using ObjectMapper
    - parameter map: A mapping from ObjectMapper
    */
   

    /**
    Map a JSON object to this class using ObjectMapper
    - parameter map: A mapping from ObjectMapper
    */
    override func mapping(map: Map) {
        posts <- map[kSearchResponseModelPostsKey]
        business <- map[kSearchResponseModelBusinessKey]
        users <- map[kSearchResponseModelUsersKey]

    }
}
public class SearchPosts: Mappable {
    public required init?(map: Map) {
        
    }
    

    // MARK: Declaration for string constants to be used to decode and also serialize.
    internal let kPostsCreatedAtKey: String = "created_at"
    internal let kPostsSharedIdKey: String = "shared_id"
    internal let kPostsPostingAsIdKey: String = "posting_as_id"
    internal let kPostsPostingAsTypeKey: String = "posting_as_type"
    internal let kPostsSourceCountryKey: String = "source_country"
    internal let kPostsLiveEndedAtKey: String = "live_ended_at"
    internal let kPostsVisibilityTypeKey: String = "visibility_type"
    internal let kPostsBusinessOfferIdKey: String = "business_offer_id"
    internal let kPostsBusinessPortfolioIdKey: String = "business_portfolio_id"
    internal let kPostsExpiresAtKey: String = "expires_at"
    internal let kPostsInternalIdentifierKey: String = "id"
    internal let kPostsMessageKey: String = "message"
    internal let kPostsUpdatedAtKey: String = "updated_at"
    internal let kPostsUserIdKey: String = "user_id"
    internal let kPostsLiveKeyKey: String = "live_key"
    internal let kPostsStatusKey: String = "status"
    internal let kPostsEventIdKey: String = "event_id"


    // MARK: Properties
    public var createdAt: String?
    public var sharedId: String?
    public var postingAsId: Int?
    public var postingAsType: String?
    public var sourceCountry: String?
    public var liveEndedAt: String?
    public var visibilityType: String?
    public var businessOfferId: String?
    public var businessPortfolioId: String?
    public var expiresAt: String?
    public var internalIdentifier: Int?
    public var message: String?
    public var updatedAt: String?
    public var userId: Int?
    public var liveKey: String?
    public var status: Int?
    public var eventId: String?
   



    // MARK: ObjectMapper Initalizers
    /**
    Map a JSON object to this class using ObjectMapper
    - parameter map: A mapping from ObjectMapper
    */
  

    /**
    Map a JSON object to this class using ObjectMapper
    - parameter map: A mapping from ObjectMapper
    */
    public func mapping(map: Map) {
        createdAt <- map[kPostsCreatedAtKey]
        sharedId <- map[kPostsSharedIdKey]
        postingAsId <- map[kPostsPostingAsIdKey]
        postingAsType <- map[kPostsPostingAsTypeKey]
        sourceCountry <- map[kPostsSourceCountryKey]
        liveEndedAt <- map[kPostsLiveEndedAtKey]
        visibilityType <- map[kPostsVisibilityTypeKey]
        businessOfferId <- map[kPostsBusinessOfferIdKey]
        businessPortfolioId <- map[kPostsBusinessPortfolioIdKey]
        expiresAt <- map[kPostsExpiresAtKey]
        internalIdentifier <- map[kPostsInternalIdentifierKey]
        message <- map[kPostsMessageKey]
        updatedAt <- map[kPostsUpdatedAtKey]
        userId <- map[kPostsUserIdKey]
        liveKey <- map[kPostsLiveKeyKey]
        status <- map[kPostsStatusKey]
        eventId <- map[kPostsEventIdKey]
       

    }
}
public class SearchUsers: Mappable {
    public required init?(map: Map) {
        
    }
    

    // MARK: Declaration for string constants to be used to decode and also serialize.
    internal let kSearchUsersNameKey: String = "name"
    internal let kSearchUsersInternalIdentifierKey: String = "id"
    internal let kSearchUsersPhoneKey: String = "phone"
    internal let kSearchUsersProfileKey: String = "profile"
    internal let kSearchUsersIsVerifiedKey: String = "is_verified"
    internal let kSearchUsersEmailKey: String = "email"
    internal let kSearchUsersUsernameKey: String = "username"


    // MARK: Properties
    public var name: String?
    public var internalIdentifier: Int?
    public var phone: String?
    public var profile: String?
    public var isVerified: Bool = false
    public var email: String?
    public var username: String?



    // MARK: ObjectMapper Initalizers
    /**
    Map a JSON object to this class using ObjectMapper
    - parameter map: A mapping from ObjectMapper
    */
  

    /**
    Map a JSON object to this class using ObjectMapper
    - parameter map: A mapping from ObjectMapper
    */
    public func mapping(map: Map) {
        name <- map[kSearchUsersNameKey]
        internalIdentifier <- map[kSearchUsersInternalIdentifierKey]
        phone <- map[kSearchUsersPhoneKey]
        profile <- map[kSearchUsersProfileKey]
        isVerified <- map[kSearchUsersIsVerifiedKey]
        email <- map[kSearchUsersEmailKey]
        username <- map[kSearchUsersUsernameKey]

    }
}
public class SearchBusiness: Mappable {
    public required init?(map: Map) {
        
    }
    

    // MARK: Declaration for string constants to be used to decode and also serialize.
    internal let kSearchBusinessBusinessTypeKey: String = "business_type"
    internal let kSearchBusinessFacebookKey: String = "facebook"
    internal let kSearchBusinessBusinessCoverImageKey: String = "business_cover_image"
    internal let kSearchBusinessStateKey: String = "state"
    internal let kSearchBusinessEstablishedKey: String = "established"
    internal let kSearchBusinessOpeningHourKey: String = "opening_hour"
    internal let kSearchBusinessOperatingRadiusKey: String = "operating_radius"
    internal let kSearchBusinessWebsiteLinkKey: String = "website_link"
    internal let kSearchBusinessInstagramKey: String = "instagram"
    internal let kSearchBusinessAddressStringKey: String = "address_string"
    internal let kSearchBusinessBusinessNameKey: String = "business_name"
    internal let kSearchBusinessModeratedAtKey: String = "moderated_at"
    internal let kSearchBusinessDefaultMusicKey: String = "default_music"
    internal let kSearchBusinessInternalIdentifierKey: String = "id"
    internal let kSearchBusinessUpdatedAtKey: String = "updated_at"
    internal let kSearchBusinessPhoneKey: String = "phone"
    internal let kSearchBusinessLonKey: String = "lon"
    internal let kSearchBusinessMaxCustomerHandleKey: String = "max_customer_handle"
    internal let kSearchBusinessTwitterKey: String = "twitter"
    internal let kSearchBusinessAboutBusinessKey: String = "about_business"
    internal let kSearchBusinessStatusKey: String = "status"
    internal let kSearchBusinessCreatedAtKey: String = "created_at"
    internal let kSearchBusinessStreetAddressKey: String = "street_address"
    internal let kSearchBusinessCountryKey: String = "country"
    internal let kSearchBusinessPostcodeKey: String = "postcode"
    internal let kSearchBusinessAbnAcnKey: String = "abn_acn"
    internal let kSearchBusinessEmailKey: String = "email"
    internal let kSearchBusinessLatKey: String = "lat"
    internal let kSearchBusinessUsernameKey: String = "username"
    internal let kSearchBusinessCanFeatureKey: String = "can_feature"
    internal let kSearchBusinessTotalEmployeesKey: String = "total_employees"
    internal let kSearchBusinessUserIdKey: String = "user_id"
    internal let kSearchBusinessSuburbKey: String = "suburb"
    internal let kSearchBusinessBusinessProfileImageKey: String = "business_profile_image"


    // MARK: Properties
    public var businessType: String?
    public var facebook: String?
    public var businessCoverImage: String?
    public var state: String?
    public var established: String?
    public var openingHour: String?
    public var operatingRadius: String?
    public var websiteLink: String?
    public var instagram: String?
    public var addressString: String?
    public var businessName: String?
    public var moderatedAt: String?
    public var defaultMusic: String?
    public var internalIdentifier: Int?
    public var updatedAt: String?
    public var phone: String?
    public var lon: String?
    public var maxCustomerHandle: String?
    public var twitter: String?
    public var aboutBusiness: String?
    public var status: Int?
    public var createdAt: String?
    public var streetAddress: String?
    public var country: String?
    public var postcode: String?
    public var abnAcn: String?
    public var email: String?
    public var lat: String?
    public var username: String?
    public var canFeature: Int?
    public var totalEmployees: String?
    public var userId: Int?
    public var suburb: String?
    public var businessProfileImage: String?



    // MARK: ObjectMapper Initalizers
    /**
    Map a JSON object to this class using ObjectMapper
    - parameter map: A mapping from ObjectMapper
    */
    
    /**
    Map a JSON object to this class using ObjectMapper
    - parameter map: A mapping from ObjectMapper
    */
    public func mapping(map: Map) {
        businessType <- map[kSearchBusinessBusinessTypeKey]
        facebook <- map[kSearchBusinessFacebookKey]
        businessCoverImage <- map[kSearchBusinessBusinessCoverImageKey]
        state <- map[kSearchBusinessStateKey]
        established <- map[kSearchBusinessEstablishedKey]
        openingHour <- map[kSearchBusinessOpeningHourKey]
        operatingRadius <- map[kSearchBusinessOperatingRadiusKey]
        websiteLink <- map[kSearchBusinessWebsiteLinkKey]
        instagram <- map[kSearchBusinessInstagramKey]
        addressString <- map[kSearchBusinessAddressStringKey]
        businessName <- map[kSearchBusinessBusinessNameKey]
        moderatedAt <- map[kSearchBusinessModeratedAtKey]
        defaultMusic <- map[kSearchBusinessDefaultMusicKey]
        internalIdentifier <- map[kSearchBusinessInternalIdentifierKey]
        updatedAt <- map[kSearchBusinessUpdatedAtKey]
        phone <- map[kSearchBusinessPhoneKey]
        lon <- map[kSearchBusinessLonKey]
        maxCustomerHandle <- map[kSearchBusinessMaxCustomerHandleKey]
        twitter <- map[kSearchBusinessTwitterKey]
        aboutBusiness <- map[kSearchBusinessAboutBusinessKey]
        status <- map[kSearchBusinessStatusKey]
        createdAt <- map[kSearchBusinessCreatedAtKey]
        streetAddress <- map[kSearchBusinessStreetAddressKey]
        country <- map[kSearchBusinessCountryKey]
        postcode <- map[kSearchBusinessPostcodeKey]
        abnAcn <- map[kSearchBusinessAbnAcnKey]
        email <- map[kSearchBusinessEmailKey]
        lat <- map[kSearchBusinessLatKey]
        username <- map[kSearchBusinessUsernameKey]
        canFeature <- map[kSearchBusinessCanFeatureKey]
        totalEmployees <- map[kSearchBusinessTotalEmployeesKey]
        userId <- map[kSearchBusinessUserIdKey]
        suburb <- map[kSearchBusinessSuburbKey]
        businessProfileImage <- map[kSearchBusinessBusinessProfileImageKey]

    }
}
