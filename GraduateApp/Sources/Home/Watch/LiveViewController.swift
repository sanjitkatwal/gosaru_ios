//
//  LiveViewController.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 12/3/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import UIKit
import AVFoundation
import LFLiveKit



class LiveViewController: UIViewController {
   
    @IBOutlet var postAsView: CustomView!
    @IBOutlet var visibilityTypeLabel: UILabel!
    @IBOutlet var captionTextView: UITextView!
    @IBOutlet var captionView: UIView!
    @IBOutlet var captionCloseBtn: UIButton!
    @IBOutlet var postBtn: AnimatedButton!
    @IBOutlet var captionBtn: AnimatedButton!
    @IBOutlet var closeBtn: UIButton!
    @IBOutlet var goLiveBtn: AnimatedButton!
    @IBOutlet var publicImage: UIImageView!
    @IBOutlet var cameraRotationBtn: UIButton!
    @IBOutlet var timerLabel: UILabel!
    var publicVisibility: Bool = true
    var dataSource: GraduateUniversityResponseModel?
    var postingAs: String?
    var selectedRow: Int?
    var posterId: Int?
    var temptoken: String?
    var url: String?
    var caption: String?
    var isLive: Bool = false
//    var timer: Timer?
    var streamKey: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getUserUniversity()
        captionView.isHidden = true
        captionCloseBtn.isHidden = true
        captionCloseBtn.setImage(UIImage(named: "cross"))
        cameraRotationBtn.setImage(UIImage(named: "rotate"))
        publicImage.image = UIImage(named: "iconChecked")
        captionBtn.setImage(UIImage(named: "comment"))
        closeBtn.setImage(UIImage(named: "iconClose"))
        captionTextView.text = "Type Your Caption Here"
        postingAs = UserDefaultsHandler.getUDValue(key: .fullName) as? String
        selectedRow = 0
        posterId = UserDefaultsHandler.getUDValue(key: .userID) as! Int
        captionTextView.delegate = self
        posterId = UserDefaultsHandler.getUDValue(key: .userID) as! Int
//        postBtn.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
//        postBtn.titleLabel?.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
//        postBtn.imageView?.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
//        postBtn.semanticContentAttribute = UIApplication.shared
//            .userInterfaceLayoutDirection == .rightToLeft ? .forceLeftToRight : .forceRightToLeft
//        postBtn.imageView?.image = UIImage(named: "iconArrowDown")
//       // postBtn.backgroundColor = UIColor.white.withAlphaComponent(0.4)
//        postBtn.layer.opacity = 1
       // postBtn.backgroundColor = UIColor.init(displayP3Red: 0.0, green: 0.0, blue: 0.0, alpha: 0.5)
        session.delegate = self
        session.preView = self.view
        
        self.requestAccessForVideo()
        self.requestAccessForAudio()
        self.view.backgroundColor = UIColor.clear
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: true)
        currentTabBar?.setBar(hidden: true, animated: true)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: true)
    }
    //MARK:- REQUEST FOR USER UNIVERSITIES
        func getUserUniversity() {
               GraduateUniversityResponse.requestToGetUserUni { [weak self](response) in
                   guard let strongSelf = self else { return }
                   print(response)
                strongSelf.dataSource = response
    }
        }
    //MARK: - Getters and Setters
    
    //  默认分辨率368 ＊ 640  音频：44.1 iphone6以上48  双声道  方向竖屏
    var session: LFLiveSession = {
        let audioConfiguration = LFLiveAudioConfiguration.defaultConfiguration(for: LFLiveAudioQuality.high)
        let videoConfiguration = LFLiveVideoConfiguration.defaultConfiguration(for: LFLiveVideoQuality.low3)
        let session = LFLiveSession(audioConfiguration: audioConfiguration, videoConfiguration: videoConfiguration)
        return session!
    }()
    func startLive() -> Void {
        let stream = LFLiveStreamInfo()
        
        var visibility: String
        let randomSTring = HelperFunctions.randomTextGenerator(length: 9)
        streamKey = randomSTring
        self.url = "rtmp://13.236.133.67/graduation_live/" + randomSTring + "?message=" + (caption ?? "")
    
        if publicVisibility == true {
            self.url = self.url! + "&visibility_type=" + "public"
        } else {
            self.url = self.url! + "&visibility_type=" + "private"
        }
        self.url = self.url! + "&post_as[]=" + "\(posterId ?? 0)"
        self.url = self.url! + ":user&_token=" + (temptoken ?? "")
//        stream.url = "rtmp://13.236.133.67/graduation_live/" + randomSTring + "?message=" + captionTextView.text + "&visibility_type=" + visibility + " &post_as[]=" + "\(posterId ?? 0)" + ":user&_token=" + temptoken
        let finalUrl = self.url
        print(finalUrl)
        stream.url = finalUrl
        session.startLive(stream)
    }
//MARK:- GET TEMP KEY
    func getTempKeyForLive() {
        LiveControllerResponse.requestToTempToken(showHud: false) { [weak self](response) in
            guard let strongSelf  = self else { return }
            print(response?.token)
            strongSelf.temptoken = response?.token
            strongSelf.downardCounter()
            strongSelf.startLive()
        }
    }
    
//MARK:- DELETE LIVE VIDEO
    func deleteLiveVideo() {
        LiveControllerResponse.requestToNotAllowLiveInTimeline(streamKey: streamKey ?? "",showHud: false) { [weak self](response) in
            guard let strongSelf = self else { return }
            print(response)
            strongSelf.changingRootToHome()
        }
    }
//MARK:- CHANGING ROOT TO TIMELINE
    func changingRootToHome() {
        let vc = UIStoryboard.watchStoryboard.instantiateWatchViewController()
        if #available(iOS 13.0, *) {

            let mySceneDelegate = self.view.window?.windowScene?.delegate as? SceneDelegate

            mySceneDelegate?.loadTabbarWithIndex(index: 2)
          
        } else {
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.loadTabbarWithIndex(index: 2)
            // Fallback on earlier versions
        }
        
      
                
        
    }
//MARK:- DOWNARD COUNTER
    
    func downardCounter() {
        var seconds = 60
        Timer.scheduledTimer(withTimeInterval: 1, repeats: true) { timer in
            seconds -= 1
            if seconds == -1 {
                print("Go!")
                
                self.stopLive()
                timer.invalidate()
                self.popupLiveConformationVC()
            } else {
                print(seconds)
                self.timerLabel.text = "\(seconds)"
            }
        }
    }
    

    func stopLive() -> Void {
        session.stopLive()
    }

    @IBAction func cameraAction(_ sender: Any) {
        startLive()
    }
    func requestAccessForVideo() -> Void {
        let status = AVCaptureDevice.authorizationStatus(for: AVMediaType.video);
        switch status  {
        // 许可对话没有出现，发起授权许可
        case AVAuthorizationStatus.notDetermined:
            AVCaptureDevice.requestAccess(for: AVMediaType.video, completionHandler: { (granted) in
                if(granted){
                    DispatchQueue.main.async {
                        self.session.running = true
                    }
                }
            })
            break;
        // 已经开启授权，可继续
        case AVAuthorizationStatus.authorized:
            session.running = true;
            break;
        // 用户明确地拒绝授权，或者相机设备无法访问
        case AVAuthorizationStatus.denied: break
        case AVAuthorizationStatus.restricted:break;
        default:
            break;
        }
    }
    
    func requestAccessForAudio() -> Void {
        let status = AVCaptureDevice.authorizationStatus(for:AVMediaType.audio)
        switch status  {
        // 许可对话没有出现，发起授权许可
        case AVAuthorizationStatus.notDetermined:
            AVCaptureDevice.requestAccess(for: AVMediaType.audio, completionHandler: { (granted) in
                
            })
            break;
        // 已经开启授权，可继续
        case AVAuthorizationStatus.authorized:
            break;
        // 用户明确地拒绝授权，或者相机设备无法访问
        case AVAuthorizationStatus.denied: break
        case AVAuthorizationStatus.restricted:break;
        default:
            break;
        }
    }
    @IBAction func publicPostAction(_ sender: Any) {
        if publicVisibility == true {
            publicImage.image = UIImage(named: "iconUnchecked")
            publicVisibility = false
        } else {
            publicImage.image = UIImage(named: "iconChecked")
            publicVisibility = true
        }
    }
    
    @IBAction func goliveAction(_ sender: Any) {
        if isLive == true {
            isLive = false
            timerLabel.isHidden = true
            postAsView.isHidden = false
            publicImage.isHidden = false
            if publicVisibility == true {
                visibilityTypeLabel.text = "Public"
            } else {
                visibilityTypeLabel.text = "Private"
            }
            popupLiveConformationVC()
        } else {
            isLive = true
            if publicVisibility == true {
                visibilityTypeLabel.text = "Public"
            } else {
                visibilityTypeLabel.text = "Private"
            }
            postAsView.isHidden = true
            publicImage.isHidden = true
            goLiveBtn.setTitle("End Live")
           getTempKeyForLive()
        }
      
    }
    @IBAction func captionBtnAction(_ sender: Any) {
        captionBtn.isHidden = true
        captionCloseBtn.isHidden = true
        captionView.isHidden = false
        captionCloseBtn.isHidden = false
    }
    @IBAction func closeBtnAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    @IBAction func cameraRotationAction(_ sender: Any) {
        let devicePositon = session.captureDevicePosition;
        session.captureDevicePosition = (devicePositon == AVCaptureDevice.Position.back) ? AVCaptureDevice.Position.front : AVCaptureDevice.Position.back;    }
    @IBAction func postAsBtnAction(_ sender: Any) {
        let vc = UIStoryboard.watchStoryboard.instantiateUniversityListVC()
        vc.delegate = self
        vc.dataSource = dataSource
        vc.postingAs = postingAs
        vc.selectedCell = selectedRow
        let cardPopup = SBCardPopupViewController(contentViewController: vc)
        cardPopup.show(onViewController: self)
    }
 
    @IBAction func captionCloseAction(_ sender: Any) {
        captionView.isHidden = true
        captionCloseBtn.isHidden = true
        captionBtn.isHidden = false
        caption = captionTextView.text
        captionTextView.text = "Enter Your Caption Here"
        captionTextView.textColor = .lightGray
        
    }
    func popupLiveConformationVC() {
        let vc = UIStoryboard.watchStoryboard.instantiateLiveConformationVC()
        vc.delegate = self
        let cardPopup = SBCardPopupViewController(contentViewController: vc)
        cardPopup.show(onViewController: self)
    }
    
}
extension LiveViewController: LFLiveSessionDelegate {
    //MARK: - Callbacks
    
    // 回调
    func liveSession(_ session: LFLiveSession?, debugInfo: LFLiveDebug?) {
        print("debugInfo: \(debugInfo?.currentBandwidth)")
    }
    
    func liveSession(_ session: LFLiveSession?, errorCode: LFLiveSocketErrorCode) {
        print("errorCode: \(errorCode.rawValue)")
    }
    
    func liveSession(_ session: LFLiveSession?, liveStateDidChange state: LFLiveState) {
        print("liveStateDidChange: \(state.rawValue)")
        switch state {
        case LFLiveState.ready:
            //stateLabel.text = "未连接"
            break;
        case LFLiveState.pending:
            //stateLabel.text = "连接中"
            break;
        case LFLiveState.start:
            //stateLabel.text = "已连接"
            break;
        case LFLiveState.error:
          ///  stateLabel.text = "连接错误"
            break;
        case LFLiveState.stop:
           // stateLabel.text = "未连接"
            break;
        default:
                break;
        }
    }
}
//MARK:- DELEGATE FROM UNIVERSITYLISTVC
extension LiveViewController: actionFromUniversityListVC {
    func sendingUniversityID(index: IndexPath!, id: Int, postingAs: String, selectedRow: Int) {
        self.postingAs = postingAs
        self.selectedRow = selectedRow
        self.posterId = id
    }
 
}
//MARK:- TEXT VIEW DELEGATE
extension LiveViewController: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView == captionTextView {
            captionTextView.text = ""
            captionTextView.textColor = .black
        }
    }
}
//MARK:- DELEGATE LIVE CONFORMATION
extension LiveViewController: actionFromLiveConformationVC {
    func postOnTimeline(value: Bool) {
        if value == true {
            changingRootToHome()
        } else {
            deleteLiveVideo()
           
        }
    }
    
    
}

