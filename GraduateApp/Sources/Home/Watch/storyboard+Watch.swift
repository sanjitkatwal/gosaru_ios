//
//  storyboard+Watch.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 6/24/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import Foundation
import UIKit
extension UIStoryboard {
private struct Constants {
    static let watchStoryboard = "Watch"
    static let watchIdentifier = "WatchViewController"
    static let liveIdentifier = "LiveViewController"
    static let universityListIdentifier = "UniversityListViewController"
    static let liveConformationIdentifier = "LiveConformationViewController"
}
    
    
static var watchStoryboard: UIStoryboard {
    return UIStoryboard(name: Constants.watchStoryboard, bundle: nil)
}
func instantiateWatchViewController() -> WatchViewController {
    guard let viewController = UIStoryboard.watchStoryboard.instantiateViewController(withIdentifier: Constants.watchIdentifier) as? WatchViewController else {
        fatalError("Couldn't instantiate WatchViewController")
    }
    return viewController
}
    
    func instantiateLiveViewController() -> LiveViewController {
        guard let viewController = UIStoryboard.watchStoryboard.instantiateViewController(withIdentifier: Constants.liveIdentifier) as? LiveViewController else {
            fatalError("Couldn't instantiate LiveViewController")
        }
        return viewController
    }
    
    func instantiateUniversityListVC() -> UniversityListViewController {
        guard let viewController = UIStoryboard.watchStoryboard.instantiateViewController(withIdentifier: Constants.universityListIdentifier) as? UniversityListViewController else {
            fatalError("Couldn't instantiate UniversityListViewController")
        }
        return viewController
    }
    func instantiateLiveConformationVC() -> LiveConformationViewController {
        guard let viewController = UIStoryboard.watchStoryboard.instantiateViewController(withIdentifier: Constants.liveConformationIdentifier) as? LiveConformationViewController else {
            fatalError("Couldn't instantiate LiveConformationViewController")
        }
        return viewController
    }
}
