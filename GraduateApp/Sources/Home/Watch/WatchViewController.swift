//
//  WatchViewController.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 6/24/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import UIKit
import Popover
import AVFoundation
import ASPVideoPlayer
import Alamofire
import ESPullToRefresh
struct WatchModel {
    var watchData: SpotReponseModel?
    var numOfPage: Int
    var loadMore: Bool
}
class WatchViewController: UIViewController {
    @IBOutlet weak var keyboardHeightLayoutConstraint: NSLayoutConstraint!
    @IBOutlet weak var tableview: UITableView!
    var keyboardOpen = false
    var popupTableView = UITableView(frame: CGRect(x: 0, y: 0, width: 90, height: 120))
    var dataSource: WatchModel? {
        didSet {
            tableview.reloadData()
        }
    }
    var postTypes: GraduateDetails? //used for the editing the post
    var editedIndex: IndexPath! //index path that is edited
    var texts = ["Share", "Edit", "Delete"]
    // var dataSource: [WatchData]?
    var popover: Popover!
    fileprivate var popoverOptions: [PopoverOption] = [
        .type(.left),
        .blackOverlayColor(UIColor(white: 0.0, alpha: 0.6))
    ]
    private var lastContentOffset: CGFloat = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        registeringTableViewCell()
        dataSource = WatchModel(watchData: nil, numOfPage: 1, loadMore: false)
         requestingToVides(pageNo: 1, isLoadMore: false, showHud: true)
        self.tableview.es.addPullToRefresh {
            [unowned self] in
          
           requestingToVides(pageNo: 1, isLoadMore: false, showHud: false)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector:#selector(keyboardWillAppear), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector:#selector(keyboardWillDisappear), name: UIResponder.keyboardWillHideNotification, object: nil)
        navigationController?.isNavigationBarHidden = true
        currentTabBar?.setBar(hidden: false, animated: true)
    }
    override func viewWillDisappear(_ animated: Bool) {
        navigationController?.isNavigationBarHidden = false
        NotificationCenter.default.removeObserver(self)
        stopVideo()
       
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        //        HelperFunctions.unhidingViewFromTabBar(height: currentTabBar!.tabBarHeight + 32, tableView: tableview)
    }
    
    @objc func keyboardWillAppear(notification: NSNotification){
        // Do something here
        keyboardOpen = true
        if let userInfo = notification.userInfo,
            // 3
            let keyboardRectangle = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? CGRect {
            print(Int(keyboardRectangle.height))
            let view = UIView()
            view.frame = CGRect(x: 0, y: 0, width: 414, height: Int(keyboardRectangle.height))
            tableview.tableFooterView = view
        }
    }
    
    @objc func keyboardWillDisappear(notification: NSNotification){
        // Do something here
        tableview.tableFooterView = nil
        currentTabBar?.setBar(hidden: true, animated: true)
        keyboardOpen = false
    }
    @IBAction func goLiveAction(_ sender: Any) {
//        let cameraVc = UIImagePickerController()
//        cameraVc.sourceType = UIImagePickerController.SourceType.camera
//        navigationController?.isNavigationBarHidden = true
//        cameraVc.modalPresentationStyle = .custom
//
//
       // self.present(cameraVc, animated: true, completion: nil)
        let vc = UIStoryboard.watchStoryboard.instantiateLiveViewController()
        navigationController?.pushViewController(vc, animated: true)
        
        
    }
    //MARK:- POPUP
    func popUp(type: ErrorType, message: String) {
        let vc = UIStoryboard.timeLineStoryboard.instantiateErrorCarMsgPopVC()
        vc.popupType = type
        vc.messageString = message
        vc.delegaet = self
        let cardPopup = SBCardPopupViewController(contentViewController: vc)
        cardPopup.show(onViewController: self)
    }
    //MARK:- REQUESTING TO VIDEOS
    func requestingToVides(pageNo: Int,isLoadMore: Bool = false, showHud: Bool = true) {
        WatchResponse.requestToWatch(pageNo: pageNo, showHud: showHud) { [weak self](response) in
            guard let strongSelf = self else { return }
            strongSelf.tableview.es.stopPullToRefresh()
            if !isLoadMore {
                strongSelf.dataSource?.watchData = response
                          
                          strongSelf.tableview.reloadData()
                      } else {
                          if var allData = response?.data {
                              print(allData)
                              for data in allData {
                                strongSelf.dataSource?.watchData?.data?.append(data)
                              }
                              strongSelf.tableview.reloadData()
                          }
                      }
            if strongSelf.dataSource?.watchData?.data?.count ?? 0 < response?.meta?.total ?? 0 {
                          strongSelf.dataSource?.loadMore = true
                          strongSelf.dataSource?.numOfPage += 1
                      } else {
                          strongSelf.dataSource?.loadMore = false
                      }
        }
    }
//MARK:- REQUEST TO DELTE POST
    func requestToDeletePost() {
        WatchResponse.requestToDeletePost(id: dataSource?.watchData?.data?[editedIndex.row].internalIdentifier ?? 0,showHud: true) { [weak self](response) in
            guard let strongSelf = self else { return }
            strongSelf.requestingToVides(pageNo: 1, isLoadMore: false, showHud: true)
        }
    }
    
    //MARK:- STOP VIDEO WHEN THE CONTROLLER IS CHANGED
    func stopVideo() {
        
        if let indexPath = self.tableview.indexPathsForVisibleRows {
            for i in indexPath {
                
                let cell : LiveTableViewCell = self.tableview.cellForRow(at: i) as! LiveTableViewCell
                
                cell.videoPlayer.videoPlayerControls.stop()
                
            }
        }
    }
    
    //    func addData() {
    //        dataSource = [WatchData(title: "video2", state: true), WatchData(title: "video2", state: true),WatchData(title: "video", state: true),WatchData(title: "video", state: true),WatchData(title: "video", state: true)]
    //        tableview.reloadData()
    //    }
    func registeringTableViewCell() {
        tableview.register(UINib(nibName: "LiveTableViewCell", bundle: nil), forCellReuseIdentifier: "LiveTableViewCell")
        tableview.dataSource = self
        tableview.delegate = self
        //        tableview.reloadData()
    }
    func sharePopOver(sender: UIButton) {
        popupTableView.reloadData()
        tableview.reloadData()
        if texts.count == 2 {
            popupTableView.frame = CGRect(x: 0, y: 0, width: 90, height: 80)
        } else {
            popupTableView.frame = CGRect(x: 0, y: 0, width: 90, height: 120)
        }
        popupTableView.delegate = self
        popupTableView.dataSource = self
        popupTableView.isScrollEnabled = false
        self.popover = Popover(options: self.popoverOptions)
        self.popover.willShowHandler = {
            print("willShowHandler")
        }
        self.popover.didShowHandler = {
            print("didDismissHandler")
        }
        self.popover.willDismissHandler = {
            print("willDismissHandler")
        }
        self.popover.didDismissHandler = {
            print("didDismissHandler")
        }
        
        self.popover.show(popupTableView, fromView: sender)
    }
    
    
}

//MARK:- TABLE VIEW DATASOURCE
extension WatchViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == popupTableView {
            return texts.count
        } else {
            return dataSource?.watchData?.data?.count ?? 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == popupTableView {
            let cell = UITableViewCell(style: .default, reuseIdentifier: nil)
            cell.textLabel?.text = self.texts[(indexPath as NSIndexPath).row]
            cell.textLabel?.textColor = UIColor.init(hexString: Appcolors.text)
            return cell
        } else {
            //            if dataSource?[indexPath.row].state == true {
            //                let cell = tableView.dequeueReusableCell(withIdentifier: "LiveTableViewCell", for: indexPath) as! LiveTableViewCell
            //            print("nothing")
            //            //MARK: TAPPING RATING DETAILS
            //
            //                                    let tapGesture2 = UITapGestureRecognizer(target: self, action: #selector(ratingDetails))
            //                                  cell.ratingStackView.addGestureRecognizer(tapGesture2)
            //
            //                       //MARK: TAPPING TO THOUGHTS VC
            //                                            let tapGesture3 = UITapGestureRecognizer(target: self, action: #selector(thoughtsDetails))
            //                                                       cell.thoughtsStackView.addGestureRecognizer(tapGesture3)
            //
            //            //MARK: TAPPING RATING POP UP
            //                                 let tapGesture = UITapGestureRecognizer(target: self, action: #selector(ratingPopUp))
            //                                 cell.ratingBtn.addGestureRecognizer(tapGesture)
            //
            //
            //            //MARK: TAPPING TO THOUGHTS BUTTON
            //                       cell.messageBtn.addTarget(self, action: #selector(thoughtsDetails), for: .touchUpInside)
            //        cell.delegate = self
            //
            ////                cell.videoPlayer.resizeClosure = .some({ (true) in
            ////                    cell.rotate(isExpanded: true)
            ////                })
            //
            //
            //
            //
            //                let firstLocalVideoURL = Bundle.main.url(forResource: dataSource?[indexPath.row].title, withExtension: "mp4")
            //                          let firstAsset = AVURLAsset(url: firstLocalVideoURL!)
            //
            //
            //
            //                /////MARK:  FOR DISBABLING THE REPEATING PLAYING VIDEO
            //                if let player = cell.videoPlayer.videoPlayerControls.videoPlayer {
            //                    if (player.playingVideo != nil) {
            //                        player.stopVideo()
            //                    }                }
            //                cell.videoPlayer.configuration = .init(videoGravity: .aspectFit, shouldLoop: false, startPlayingWhenReady: false, controlsInitiallyHidden: true, allowBackgroundPlay: false)
            //
            //
            //        return cell
            //    }else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "LiveTableViewCell", for: indexPath) as! LiveTableViewCell
            cell.delegate = self
            cell.postType = .live
            cell.selectionType = .live
            cell.videoType = .liveVideo
            cell.index = indexPath
            cell.universityId = dataSource?.watchData?.data?[indexPath.row].internalIdentifier
            cell.starreVlaue = dataSource?.watchData?.data?[indexPath.row].starredValue
            cell.starCount = dataSource?.watchData?.data?[indexPath.row].starsCount
            cell.name = dataSource?.watchData?.data?[indexPath.row].postingAs?.name
            cell.viewersLabel.text = "\(dataSource?.watchData?.data?[indexPath.row].viewsCount ?? 0)"
            if dataSource?.watchData?.data?[indexPath.row].hasStarred == true {
                cell.ratingBtn.setImage(UIImage(named: "star3"))
            } else {
                cell.ratingBtn.setImage(UIImage(named: "star"))
            }
            let newDate = HelperFunctions.timeInterval(timeAgo: dataSource?.watchData?.data?[indexPath.row].createdAt ?? "")
            cell.timeStampLabel.text = newDate
            cell.ratingNumLabel.text = "\(dataSource?.watchData?.data?[indexPath.row].starsCount ?? 0)"
            cell.thoughtsNumLabel.text = "\(dataSource?.watchData?.data?[indexPath.row].comments?.count ?? 0)"
           
            let filrurls = NSURL(string: (GraduateUrl.imageUrl + (dataSource?.watchData?.data?[indexPath.row].liveVideoMp4 ?? "")))
            print(filrurls)
            let firstAsset = AVURLAsset(url: filrurls! as URL)
            cell.videoPlayer.videoAssets = [firstAsset]
            cell.postData = dataSource?.watchData?.data?[indexPath.row]
            cell.selectionType = .live
        
           
            return cell
            
        }
        
    }
   
   func ratingDetails(ratingId: Int, starredVlaue: String, starCount: Int) {
        let vc = UIStoryboard.timeLineStoryboard.instantiateRatingDetailsVC()
        vc.ratingId = ratingId
         vc.starredValue = starredVlaue
         vc.starCount = starCount
        navigationController?.pushViewController(vc, animated: true)
    }
    func thoughtsDetails(id: Int,index: IndexPath) {
        let vc = UIStoryboard.timeLineStoryboard.instantiateThoughtsVC()
        vc.commentId = id
        vc.index = index
        vc.delegate = self
        navigationController?.pushViewController(vc, animated: true)
    }
    func popingBottomVC(id: Int, starValue: String, selectedindexToReload: Int, selectionType: GraduateDetails) {
        let popupVC = UIStoryboard.timeLineStoryboard.instantiateRatingVC()
        //popupVC.delegate = self
        //popupVC.selectedIndex = 3
        popupVC.id = id
        popupVC.selectedIndex = Int(starValue)
        popupVC.indexToReload = selectedindexToReload
        popupVC.selectionType = selectionType
        popupVC.height = 200
        popupVC.delegate = self
        popupVC.topCornerRadius = 15
        popupVC.presentDuration = 0.25
        popupVC.dismissDuration = 0.25
        popupVC.shouldDismissInteractivelty = true
        // popupVC.popupDelegate = self
        present(popupVC, animated: true, completion: nil)
    }
}
//MARK:- TABLEVIEW DELEGATE
extension WatchViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == popupTableView {
            return 40
        } else {
            return UITableView.automaticDimension
        }
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 505
    }
    //    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
    //        let cell = tableView.dequeueReusableCell(withIdentifier: "LiveTableViewCell") as! LiveTableViewCell
    //        cell.stoppedVideo()
    //    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        popover.dismiss()
        if tableView == popupTableView {
            popover.dismiss()
            if texts[indexPath.row] ==  "Share"{
                let shareText = "http://test-graduation1.captainau.com.au/"
                
                let image = UIImage(named: "iconArrowDown")
                let vc = UIActivityViewController(activityItems: [shareText, image!], applicationActivities: [])
                present(vc, animated: true)
                self.popover.dismiss()
            } else if texts[indexPath.row] == "Edit" {
                let vc = UIStoryboard.timeLineStoryboard.instantiateCreateSpotViewController()
                vc.vcType = .editPost
                vc.delegate = self
                navigationController?.pushViewController(vc, animated: true)
                self.popover.dismiss()
            } else if texts[indexPath.row] == "Delete" {
                popUp(type: .deletePost, message: "")
//                let vc = UIStoryboard.timeLineStoryboard.instantiateDeletePostPopUpVC()
//                let cardPopup = SBCardPopupViewController(contentViewController: vc)
//                cardPopup.show(onViewController: self)
//                self.popover.dismiss()
            } else {
                let vc = UIStoryboard.graduateStoryboard.instantiateReportVC()
                vc.reportId = dataSource?.watchData?.data?[editedIndex.row].internalIdentifier
                vc.view.isOpaque = false
                vc.view.backgroundColor = UIColor.black.withAlphaComponent(0.8)
                vc.modalPresentationStyle = .custom
                self.present(vc, animated: true, completion: nil)
                popover.dismiss()
            }
        } else {
            
        }
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if tableView == popupTableView {
            
        } else {
            let allData = dataSource
            print(allData?.loadMore ?? false)
            print(indexPath.row,"indexPath")
            print(dataSource?.watchData?.data?.count,"dataCount")
            guard allData?.loadMore == true, let dataCount = allData?.watchData?.data?.count, indexPath.row == dataCount - 1 else {return}
            print(dataCount)
            let lastSectionIndex = tableView.numberOfSections - 1
            let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
            if indexPath.section == lastSectionIndex && indexPath.row == lastRowIndex {
                // print("this is the last cell")
                let spinner = UIActivityIndicatorView(style: .gray)
                spinner.startAnimating()
                spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
                self.tableview.tableFooterView = spinner
                self.tableview.tableFooterView?.isHidden = false
            }

            requestingToVides(pageNo: dataSource?.numOfPage ?? 0, isLoadMore: dataSource!.loadMore, showHud: false)
           
        }
    }
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if tableView == tableView {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "LiveTableViewCell", for: indexPath) as? LiveTableViewCell {
            if let players = cell.videoPlayer {
                if players.preferredRate != 0 {
                    players.videoPlayerControls.pause()
                }
            }
        }

        } else {
            
        }
    }
    
    
}

//MARK:- SCROLL VIEW DELEGATE
extension WatchViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        //MARK-: FOR NOT BOUNCING WHILE PULL TO REFRESH
        if scrollView.contentOffset.y < 0.0 {
            return
        }
        
        if (scrollView.contentOffset.y >= scrollView.contentSize.height - scrollView.frame.size.height) {
            scrollView.setContentOffset(CGPoint(x: scrollView.contentOffset.x, y: scrollView.contentSize.height - scrollView.frame.size.height), animated: false)
            //
        }
        if keyboardOpen == false {
            let delta = scrollView.contentOffset.y - lastContentOffset
            if delta < 0 {
                currentTabBar?.tabBarHeight = min(50, (currentTabBar?.buttonsContainerHeightConstraintInitialConstant)! - delta)
                
                //self.view.layoutIfNeeded()
            } else {
                
                currentTabBar?.tabBarHeight = max(-35, (currentTabBar?.buttonsContainerHeightConstraintInitialConstant)! - delta)
                
                //  self.view.layoutIfNeeded()
                
            }
            // This makes the + or - number quite small.
            lastContentOffset = scrollView.contentOffset.y
        } else {
            
        }
    }
}

//MARK:- LIVE DELEGATE
extension WatchViewController: SendingRatingFromLiveTVC {
    func moreOptionsFromLiveTVC(sender: UIButton, index: IndexPath, isOwner: Bool, postType: GraduateDetails) {
        postTypes = postType
        editedIndex = index
        print(isOwner)
        if isOwner == true {
            texts.removeAll()
            texts = ["Share", "Edit","Delete"]
            print(texts)
            sharePopOver(sender: sender)
            tableview.reloadData()
        } else {
            texts.removeAll()
            texts = ["Share", "Report"]
            sharePopOver(sender: sender)
        }
    }
    
  
    
    func commentOutsideLiveTVC(comment: Comments?, index: IndexPath) {
        dataSource?.watchData?.data?[index.row].comments?.append(comment!)
        tableview.reloadRows(at: [index], with: .automatic)
    }
    func pushingTOProfileVC(id: Int, userType: String, isOwner: Bool, postasType: String) {
        if isOwner == true {
            let vc = UIStoryboard.timeLineStoryboard.instantiateUserProfileVC()
          
                          navigationController?.pushViewController(vc, animated: true)
        } else {
            let vc = UIStoryboard.timeLineStoryboard.instantiateOtherUserProfileVC()
            vc.userId = id
                          navigationController?.pushViewController(vc, animated: true)
        }
       
    }

    func feedbackActionLiveTVC(commentId: Int, index: IndexPath!) {
        thoughtsDetails(id: commentId, index: index)
    }
    
    func sendingRatings(id: Int, starValue: String, selectedindexToReload: Int, selectionType: GraduateDetails) {
        popingBottomVC(id: id, starValue: starValue, selectedindexToReload: selectedindexToReload, selectionType: selectionType)
    }
    
    func liveRatingDetails(ratingId: Int, starredVlaue: String, starCount: Int) {
        ratingDetails(ratingId: ratingId, starredVlaue: starredVlaue, starCount: starCount)
    }
   
   
    func sendingVideoUrlForFullScreen() {
    }
}
struct WatchData {
    var title: String?
    var state: Bool?
}
extension DispatchQueue {
    
    static func background(delay: Double = 0.0, background: (()->Void)? = nil, completion: (() -> Void)? = nil) {
        DispatchQueue.global(qos: .background).async {
            background?()
            if let completion = completion {
                DispatchQueue.main.asyncAfter(deadline: .now() + delay, execute: {
                    completion()
                })
            }
        }
    }
    
}
extension WatchViewController: SendingActionFromRatingVC {
    func sendingActionWithType(type: RatingActionTypes, id: Int, reloadingIndex: Int, ratingNum: Int, selectionType: GraduateDetails) {
        switch type {
        case .requestforRating:
        ratingAfterApiCallOfEvent(reloadingIndex: reloadingIndex, ratingNum: ratingNum)
    reqestForPostRating(id: id, reloadingIndex: reloadingIndex, ratingNum: ratingNum, selectionType: selectionType)
        case .reuestForUnratingPost:
           unratingAfterApiCallOfEvent(reloadingIndex: reloadingIndex, ratingNum: ratingNum)
            reqestForPostUnRating(id: id, reloadingIndex: reloadingIndex, ratingNum: ratingNum, selectionType: selectionType)
        }
    }
    //MARK:- RATE POST
       func reqestForPostRating(id: Int,reloadingIndex: Int,ratingNum: Int,selectionType: GraduateDetails) {
           var headers: [String : Any]?
           if let token = UserDefaultsHandler.getUDValue(key: UDkey.token) as? String {
               if headers == nil {
                   headers = ["Authorization": "Bearer \(token)", "Accept": "application/json"]
               } else {
                   headers!["Authorization"] = "Bearer \(token)"
                   headers!["Accept"] = "application/json"
               }
           }
           let url = (GraduateUrl.ratePostURL + "\(id)")
            print(url,ratingNum,selectionType)
           Alamofire.request(url,
                             method: .post,
                             parameters: ["star_value": "\(ratingNum + 1)"],headers: (headers as! HTTPHeaders))
               .validate()
               .responseJSON { [weak self]response in
                   guard let strongSelf = self else { return }
                   if let value = response.result.value as? [String : Any] {
                       print(value)
//                    strongSelf.ratingAfterApiCallOfEvent(reloadingIndex: reloadingIndex, ratingNum: ratingNum)
                       
                   }
                   
           }
       }
    
    //MARK:- REQUEST FOR UNRATING POST
    func reqestForPostUnRating(id: Int,reloadingIndex: Int,ratingNum: Int,selectionType: GraduateDetails) {
        var headers: [String : Any]?
        if let token = UserDefaultsHandler.getUDValue(key: UDkey.token) as? String {
            if headers == nil {
                headers = ["Authorization": "Bearer \(token)", "Accept": "application/json"]
            } else {
                headers!["Authorization"] = "Bearer \(token)"
                headers!["Accept"] = "application/json"
            }
        }
        let url = (GraduateUrl.unratePostURL + "\(id)")
        print(url,ratingNum,selectionType)
        Alamofire.request(url,method: .delete,headers: (headers as! HTTPHeaders))
            .validate()
            .responseJSON { [weak self]response in
                guard let strongSelf = self else { return }
                if let value = response.result.value as? [String : Any] {
//                    strongSelf.unratingAfterApiCallOfEvent(reloadingIndex: reloadingIndex, ratingNum: ratingNum)
                    print(value)
                    print(reloadingIndex)
                    
                }
                
        }
    }
    func ratingAfterApiCallOfEvent(reloadingIndex: Int, ratingNum: Int) {
        print(dataSource?.watchData?.data?[reloadingIndex].hasStarred)
        if dataSource?.watchData?.data?[reloadingIndex].hasStarred == true {
           
        } else {
            
            dataSource?.watchData?.data?[reloadingIndex].starsCount! += 1
        }
        dataSource?.watchData?.data?[reloadingIndex].starredValue = "\((ratingNum + 1))"
        print(dataSource?.watchData?.data?[reloadingIndex].starredValue)
        print(reloadingIndex)
        dataSource?.watchData?.data?[reloadingIndex].hasStarred = true
        tableview.reloadData()
    }
    func unratingAfterApiCallOfEvent(reloadingIndex: Int, ratingNum: Int) {
        dataSource?.watchData?.data?[reloadingIndex].hasStarred = false
        if  dataSource?.watchData?.data?[reloadingIndex].starsCount ?? 0 > 0 {
            dataSource?.watchData?.data?[reloadingIndex].starsCount! -= 1
        } else {
            
        }
        dataSource?.watchData?.data?[reloadingIndex].starredValue = "\(0)"
        tableview.reloadData()
    }
    
}
//MARK:- THOUGHTS DELEGATE
extension WatchViewController: SendingDataFromThoughtsVC {
    func sendingData(index: IndexPath, thoughtsCount: Int, commentsData: [Comments]) {
        dataSource?.watchData?.data?[index.row].comments = commentsData
        tableview.reloadRows(at: [index], with: .automatic)
    }
}
//MARK:- ERROR CHAT VC DELEGATE
extension WatchViewController: SendingActionFromErrorChat {
    func sendingAction() {
        
    }
    func sendingCancelAction() {
        
    }
    func adctionwithType(type: ErrorType) {
        switch type {
        case .deletePost:
            requestToDeletePost()
        default:
            break
        }
    }
}
extension WatchViewController: ActionFromCreateSpotVC {
    func reloadTableViewData() {
        
    }
    
    func reloadWithType(type: ErrorType) {
        switch type {
        case .postEdited:
           requestingToVides(pageNo: 1, isLoadMore: false, showHud: true)
        default:
            break
        }
    }
    
    
}
