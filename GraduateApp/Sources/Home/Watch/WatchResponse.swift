//
//  WatchResponse.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 10/12/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import Foundation
import ObjectMapper

class WatchResponse: DefaultResponse {
    var data: LoginResponseModel?
    var success: Bool?
    var occupationList: [String] = []
override func mapping(map: Map) {
  super.mapping(map: map)

}
// MARK: - GET SPOT ONLY
    class func requestToWatch(pageNo: Int,showHud: Bool ,completionHandler:@escaping ((SpotReponseModel?) -> Void)) {
        let param:[String : Any] = ["my_post" : "true", "page" : "\(pageNo)", "event_post" : "false", "offer_post" : "false" , "only_live_videos" : "true"]
        APIManager(urlString: GraduateUrl.watchURL,parameters: param ,method: .get).handleResponse(showProgressHud: showHud,completionHandler: { (response: SpotReponseModel) in
              print(response)
                  completionHandler(response)
              }, failureBlock: {
                completionHandler(nil)
              })
      }
//MARK:- DELETE OWNPOST
  
    class func requestToDeletePost(id: Int,showHud: Bool ,completionHandler:@escaping ((SpotReponseModel?) -> Void)) {
            APIManager(urlString: (GraduateUrl.deleteOwnPostURL + "\(id)"),method: .delete).handleResponse(showProgressHud: showHud,completionHandler: { (response: SpotReponseModel) in
                  print(response)
                      completionHandler(response)
                  }, failureBlock: {
                    completionHandler(nil)
                  })
          }
}
