//
//  UniversityListViewController.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 12/7/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import UIKit
protocol actionFromUniversityListVC: class {
    func sendingUniversityID(index: IndexPath!, id: Int,postingAs: String, selectedRow: Int)
}
class UniversityListViewController: UIViewController {
    @IBOutlet var closeBtn: UIButton!
    
    @IBOutlet var postAsLabel: UILabel!
    @IBOutlet var tableView: UITableView!
    var postingAs: String?
    var selectedCell: Int?
    var dataSource: GraduateUniversityResponseModel? {
        didSet {
            getAllUniversitiesInArray()
           
        }
    }
    var universityArray = [String]()
    weak var delegate: actionFromUniversityListVC?
  
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        tableView.delegate = self
        closeBtn.setImage(UIImage(named: "close"))
        postAsLabel.text = ("post as " + (postingAs ?? ""))
        // Do any additional setup after loading the view.
        tableView.reloadData()
    }
    func getAllUniversitiesInArray() {
       
        universityArray.append((UserDefaultsHandler.getUDValue(key: .fullName)) as! String)
        dataSource?.data?.forEach {
            print($0)
            universityArray.append($0.businessName ?? "")
        }
    
    }
    
    @IBAction func closeBtnAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
extension UniversityListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print(universityArray.count)
        return universityArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "UniversityTableViewCell", for: indexPath) as! UniversityTableViewCell
        cell.titleLabel.text = universityArray[indexPath.row]
        if indexPath.row == selectedCell {
            cell.leftImage.image = UIImage(named: "iconChecked")
        } else {
            cell.leftImage.image = UIImage(named: "iconUnchecked")
        }
        cell.index = indexPath
        return cell
    }
}
extension UniversityListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedCell = indexPath.row
        postingAs = universityArray[indexPath.row]
        if indexPath.row == 0 {
            postAsLabel.text = ("post as " + (postingAs ?? ""))
            delegate?.sendingUniversityID(index: indexPath, id: UserDefaultsHandler.getUDValue(key: .userID) as! Int, postingAs: postingAs ?? "", selectedRow: selectedCell ?? 0)
        } else {
        postAsLabel.text = ("post as" + (postingAs ?? ""))
        delegate?.sendingUniversityID(index: indexPath, id: dataSource?.data?[indexPath.row - 1].internalIdentifier ?? 0, postingAs: postingAs ?? "", selectedRow: selectedCell ?? 0)
        }
        tableView.reloadData()
    }
}
