//
//  LiveControllerResponse.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 12/7/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import Foundation
import ObjectMapper
class LiveControllerResponse: DefaultResponse {
    var token: String?
override func mapping(map: Map) {
  super.mapping(map: map)
token <- map["token"]
//sentData <- map["data"]
}
// MARK: - GET MESSAGE CONVERSATION
    class func requestToTempToken(showHud: Bool,completionHandler:@escaping ((LiveControllerResponse?) -> Void)) {
        APIManager(urlString: (GraduateUrl.liveTempTokenURL),method: .get).handleResponse(showProgressHud: showHud,completionHandler: { (response: LiveControllerResponse) in
              print(response)
                  completionHandler(response)
              }, failureBlock: {
                completionHandler(nil)
              })
      }
    // MARK: - NOT ALLOW LIVE VIDEOS IN TIME LINE
    class func requestToNotAllowLiveInTimeline(streamKey: String,showHud: Bool,completionHandler:@escaping ((LiveControllerResponse?) -> Void)) {
            APIManager(urlString: (GraduateUrl.deleteLiveVideoURL + streamKey),method: .delete).handleResponse(showProgressHud: showHud,completionHandler: { (response: LiveControllerResponse) in
                  print(response)
                      completionHandler(response)
                  }, failureBlock: {
                    completionHandler(nil)
                  })
          }
}
