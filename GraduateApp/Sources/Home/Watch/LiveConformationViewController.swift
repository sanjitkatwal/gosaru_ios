//
//  LiveConformationViewController.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 12/7/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import UIKit
protocol actionFromLiveConformationVC: class {
    func postOnTimeline(value: Bool)
}
class LiveConformationViewController: UIViewController {

    @IBOutlet var closeBtn: UIButton!
    @IBOutlet var descLabel: UILabel!
    weak var delegate: actionFromLiveConformationVC?
    override func viewDidLoad() {
        super.viewDidLoad()
        closeBtn.setImage(UIImage(named: "cross"))
        descLabel.text = "You have successfully gone live \nDo you want to add this live in timeline?"
        // Do any additional setup after loading the view.
    }
    
    @IBAction func yesAction(_ sender: Any) {
        delegate?.postOnTimeline(value: true)
    }
    
    @IBAction func noAction(_ sender: Any) {
        delegate?.postOnTimeline(value: false)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func closeBtnAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
