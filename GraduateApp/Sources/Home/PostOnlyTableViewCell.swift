//
//  PostOnlyTableViewCell.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 12/11/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import UIKit

class PostOnlyTableViewCell: UITableViewCell {

    @IBOutlet var backView: UIView!
    @IBOutlet var titleLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
       // backView.addBorder(toSide: .Top, withColor: UIColor.lightGray.cgColor, andThickness: 0.5)
       // backView.addBorder(toSide: .Bottom, withColor: UIColor.lightGray.cgColor, andThickness: 0.5)
        let thickness: CGFloat = 0.5
          let topBorder = CALayer()
          let bottomBorder = CALayer()
          topBorder.frame = CGRect(x: 0.0, y: 0.0, width: self.backView.frame.size.width, height: thickness)
          topBorder.backgroundColor = UIColor.red.cgColor
          bottomBorder.frame = CGRect(x:0, y: self.backView.frame.size.height - thickness, width: self.backView.frame.size.width, height:thickness)
          bottomBorder.backgroundColor = UIColor.red.cgColor
          backView.layer.addSublayer(topBorder)
         // centerView.layer.addSublayer(bottomBorder)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
