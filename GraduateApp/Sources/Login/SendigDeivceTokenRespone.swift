//
//  SendigDeivceTokenRespone.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 9/14/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import Foundation
import ObjectMapper
class SendingDeviceTokenResponse: DefaultResponse {
    var xmsg: String?
override func mapping(map: Map) {
  super.mapping(map: map)
   xmsg <- map["success"]
}
// MARK: - login request
    class func requestToSendingDeviceToken(token: Any,completionHandler:@escaping ((SendingDeviceTokenResponse?) -> Void)) {
        print(token)
        APIManager(urlString: GraduateUrl.sendingDeviceTokenURL, parameters: ["device_token" : token], method: .post ).handleResponse(completionHandler: { (response: SendingDeviceTokenResponse) in
              print(response)
                  completionHandler(response)
              }, failureBlock: {
                completionHandler(nil)
              })
      }
}
