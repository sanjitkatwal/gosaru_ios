//
//  EmailVerificationResponse.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 9/11/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import Foundation
import ObjectMapper
class EmailVerificationResponse: DefaultResponse {
    var emailVerified: Bool?
override func mapping(map: Map) {
  super.mapping(map: map)
emailVerified <- map["email_verified"]
}
// MARK: - login request
    class func requestToEmailVerification(completionHandler:@escaping ((EmailVerificationResponse?) -> Void)) {
        APIManager(urlString: GraduateUrl.emailVerificationURL, method: .get ).handleResponse(completionHandler: { (response: EmailVerificationResponse) in
              print(response)
                  completionHandler(response)
              }, failureBlock: {
                completionHandler(nil)
              })
      }
}
