//
//  WelcomeViewController.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 6/22/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import UIKit

class WelcomeViewController: UIViewController {

   
    
    @IBOutlet weak var nxtBtn: AnimatedButton!
    @IBOutlet weak var topTitle: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var topView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "GRADUATE"
        
        navigationController?.navigationBar.barTintColor = UIColor(hexString: "#f5f1e5")
//        topView.backgroundColor = UIColor(white: 1, alpha: 0.5)
        // Do any additional setup after loading the view.
         //topView.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        topTitle.text = "WELCOME\nTO\nGRADUATE"
        descLabel.text = "Welcome to graduate.\nWhere you can plan and organize event all in one plan.This is the simples you are ever going to use.\nFeel free to try."
        //nxtBtn.titleLabel?.isOpaque = true
      //  nxtBtn.backgroundColor = UIColor.white
    }
    override func viewDidAppear(_ animated: Bool) {
       //topView.backgroundColor = UIColor.black.withAlphaComponent(0.5)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func nxtButtonAction(_ sender: Any) {
        let vc = UIStoryboard.init(name: "Login", bundle: Bundle.main).instantiateViewController(withIdentifier: "RegisterViewController") as? RegisterViewController
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    @IBAction func createAction(_ sender: Any) {
        let vc = UIStoryboard.init(name: "Login", bundle: Bundle.main).instantiateViewController(withIdentifier: "RegisterViewController") as? RegisterViewController
        self.navigationController?.pushViewController(vc!, animated: true)
    }
}
extension UIColor {

      convenience init(hexString: String) {
          let hex = hexString.trimmingCharacters(in: CharacterSet.alphanumerics.inverted)
          var int = UInt64()
          Scanner(string: hex).scanHexInt64(&int)
          let a, r, g, b: UInt64
          switch hex.count {
          case 3: // RGB (12-bit)
              (a, r, g, b) = (255, (int >> 8) * 17, (int >> 4 & 0xF) * 17, (int & 0xF) * 17)
          case 6: // RGB (24-bit)
              (a, r, g, b) = (255, int >> 16, int >> 8 & 0xFF, int & 0xFF)
          case 8: // ARGB (32-bit)
              (a, r, g, b) = (int >> 24, int >> 16 & 0xFF, int >> 8 & 0xFF, int & 0xFF)
          default:
              (a, r, g, b) = (255, 0, 0, 0)
          }
          self.init(red: CGFloat(r) / 255, green: CGFloat(g) / 255, blue: CGFloat(b) / 255, alpha: CGFloat(a) / 255)
      }
  }
