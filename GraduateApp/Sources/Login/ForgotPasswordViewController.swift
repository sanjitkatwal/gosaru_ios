//
//  ForgotPasswordViewController.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 1/14/21.
//  Copyright © 2021 Mousham Pradhan. All rights reserved.
//

import UIKit

class ForgotPasswordViewController: UIViewController {
    @IBOutlet var passwordTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Forgot Password"
        // Do any additional setup after loading the view.
    }
    @IBAction func sendAction(_ sender: Any) {
        if passwordTextField.text == "" {
            
        } else {
            let param: [String : Any] = ["email" : passwordTextField.text ?? ""]
            ForgotPasswordResponse.requestToForgotPassword(param: param, password: "") { [weak self](response) in
                guard let strongSelf = self else { return }
            }
        }
    }
    @IBAction func cancelAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
