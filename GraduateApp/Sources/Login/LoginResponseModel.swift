//
//  LoginResponseModel.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 9/10/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import Foundation
import ObjectMapper
class LoginResponse: DefaultResponse {
    var data: LoginResponseModel?
override func mapping(map: Map) {
  super.mapping(map: map)

}
// MARK: - login request
    class func requestToLogin(email: String,password: String ,completionHandler:@escaping ((LoginResponseModel?) -> Void)) {
        APIManager(urlString: GraduateUrl.loginURL, parameters: ["email" : email, "password" : password], method: .post ).handleResponse(completionHandler: { (response: LoginResponseModel) in
              print(response)
                  completionHandler(response)
              }, failureBlock: {
                completionHandler(nil)
              })
      }
}
 class LoginResponseModel: DefaultResponse {

// MARK: Declaration for string constants to be used to decode and also serialize.
internal let kLoginResponseModelSuccessKey: String = "success"


// MARK: Properties

    public var errorMessage: String?
     var success: Success?



// MARK: ObjectMapper Initalizers
/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/


/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/
    override func mapping(map: Map) {
    success <- map["success"]
    errorMessage <- map[kLoginResponseModelSuccessKey]

}
}
class Success: Mappable {
    required init?(map: Map) {
        
    }
    

// MARK: Declaration for string constants to be used to decode and also serialize.
internal let kSuccessTokenKey: String = "token"
internal let kSuccessUserKey: String = "user"
internal let kSuccessChatableIdKey: String = "chatable_id"


// MARK: Properties
public var token: String?
public var user: User?
public var chatableId: Int?




// MARK: ObjectMapper Initalizers
/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/


/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/
func mapping(map: Map) {
    token <- map[kSuccessTokenKey]
    user <- map[kSuccessUserKey]
    chatableId <- map[kSuccessChatableIdKey]
   

}
}
class User: Mappable {
    required init?(map: Map) {
        
    }
    

// MARK: Declaration for string constants to be used to decode and also serialize.
internal let kUserNameKey: String = "name"
internal let kUserInternalIdentifierKey: String = "id"
internal let kUserPhoneKey: String = "phone"
internal let kUserProfileKey: String = "profile"
internal let kUserIsVerifiedKey: String = "is_verified"
internal let kUserEmailKey: String = "email"
internal let kUserUsernameKey: String = "username"


// MARK: Properties
public var name: String?
public var internalIdentifier: Int?
public var phone: String?
public var profile: Profile?
public var isVerified: Bool = false
public var email: String?
public var username: String?



// MARK: ObjectMapper Initalizers
/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/


/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/
func mapping(map: Map) {
    name <- map[kUserNameKey]
    internalIdentifier <- map[kUserInternalIdentifierKey]
    phone <- map[kUserPhoneKey]
    profile <- map[kUserProfileKey]
    isVerified <- map[kUserIsVerifiedKey]
    email <- map[kUserEmailKey]
    username <- map[kUserUsernameKey]

}
}
class Profile: Mappable {
    required init?(map: Map) {
        
    }
    

// MARK: Declaration for string constants to be used to decode and also serialize.
internal let kProfileProfileImageKey: String = "profile_image"
internal let kProfileNicknameKey: String = "nickname"
internal let kProfileGenderKey: String = "gender"


// MARK: Properties
public var profileImage: String?
public var nickname: String?
public var gender: String?



// MARK: ObjectMapper Initalizers
/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/

/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/
func mapping(map: Map) {
    profileImage <- map[kProfileProfileImageKey]
    nickname <- map[kProfileNicknameKey]
    gender <- map[kProfileGenderKey]

    }
}

