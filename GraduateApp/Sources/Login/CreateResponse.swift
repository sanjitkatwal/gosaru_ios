//
//  CreateResponse.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 9/10/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import Foundation
import ObjectMapper
class RegisterResponse: DefaultResponse {
   
override func mapping(map: Map) {
  super.mapping(map: map)

}
// MARK: - login request
    class func requestToRegister(params : [String: Any] ,completionHandler:@escaping ((RegisterResponseModel?) -> Void)) {
        APIManager(urlString: GraduateUrl.registerURL, parameters: params, method: .post ).handleResponse(completionHandler: { (response: RegisterResponseModel) in
              print(response)
                  completionHandler(response)
              }, failureBlock: {
                completionHandler(nil)
              })
      }
}
 class RegisterResponseModel: DefaultResponse {

// MARK: Declaration for string constants to be used to decode and also serialize.
internal let kRegisterResponseModelSuccessKey: String = "success"
    internal let kRegisterResponseModelErrorsKey: String = "errors"


// MARK: Properties
public var success: RegisterSuccess?
    public var errors: RegisterErrors?



// MARK: ObjectMapper Initalizers
/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/


/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/
    override func mapping(map: Map) {
    success <- map[kRegisterResponseModelSuccessKey]
     errors <- map[kRegisterResponseModelErrorsKey]
}
}
class RegisterSuccess: Mappable {
    required init?(map: Map) {
    }
    

// MARK: Declaration for string constants to be used to decode and also serialize.
internal let kSuccessTokenKey: String = "token"
internal let kSuccessUserKey: String = "user"


// MARK: Properties
public var token: String?
public var user: RegisterUser?



// MARK: ObjectMapper Initalizers
/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/


/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/
    public func mapping(map: Map) {
    token <- map[kSuccessTokenKey]
    user <- map[kSuccessUserKey]

}
}
class RegisterUser: Mappable {
    required init?(map: Map) {
        
    }
    

// MARK: Declaration for string constants to be used to decode and also serialize.
internal let kUserProfileKey: String = "profile"
internal let kUserNameKey: String = "name"
internal let kUserEmailKey: String = "email"
internal let kUserInternalIdentifierKey: String = "id"
internal let kUserPhoneKey: String = "phone"
internal let kUserIsVerifiedKey: String = "is_verified"


// MARK: Properties
public var profile: Profile?
public var name: String?
public var email: String?
public var internalIdentifier: Int?
public var phone: String?
public var isVerified: Bool = false



// MARK: ObjectMapper Initalizers
/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/


/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/
    public func mapping(map: Map) {
    profile <- map[kUserProfileKey]
    name <- map[kUserNameKey]
    email <- map[kUserEmailKey]
    internalIdentifier <- map[kUserInternalIdentifierKey]
    phone <- map[kUserPhoneKey]
    isVerified <- map[kUserIsVerifiedKey]

}
}
class RegisterErrors: Mappable {
    required init?(map: Map) {
        
    }
    

// MARK: Declaration for string constants to be used to decode and also serialize.
internal let kErrorsEmailKey: String = "email"
internal let kErrorsPhoneKey: String = "phone"
internal let kErrorsAgreeTermsKey: String = "agree_terms"
internal let kErrorsPasswordTermsKey: String = "password"


// MARK: Properties
public var email: [String]?
public var phone: [String]?
public var agreeTerms: [String]?
public var password: [String]?



// MARK: ObjectMapper Initalizers
/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/


/**
Map a JSON object to this class using ObjectMapper
- parameter map: A mapping from ObjectMapper
*/
func mapping(map: Map) {
    email <- map[kErrorsEmailKey]
    phone <- map[kErrorsPhoneKey]
    agreeTerms <- map[kErrorsAgreeTermsKey]
    password <- map[kErrorsPasswordTermsKey]

}
}
