//
//  LoginViewController.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 6/22/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    
    @IBOutlet weak var passwordTxtField: UITextField!
    @IBOutlet weak var emailTxtField: UITextField!
    var iconClick = true
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Graduate"
        emailTxtField.text = "karkiashok5@yahoo.com"
        passwordTxtField.text = "Testing!1"
        // passwordTxtField.isSecureTextEntry = true
        //        emailTxtField.layer.cornerRadius = 16
        //        emailTxtField.layer.borderWidth = 0.2
        //        passwordTxtField.layer.cornerRadius = 16
        //        passwordTxtField.layer.borderWidth = 0.2
        addingEyeBtn(image: "secured")
        
        // Do any additional setup after loading the view.
    }
    func addingEyeBtn(image: String) {
        let button = UIButton(type: .custom)
        let image = UIImage(named: image)?.withRenderingMode(.alwaysTemplate)
        button.setImage(image, for: .normal)
        button.tintColor = UIColor(hexString: "A58E52")
        button.imageEdgeInsets = UIEdgeInsets(top: 0, left: -16, bottom: 0, right: 0)
        button.frame = CGRect(x: CGFloat(passwordTxtField.frame.size.width - 25), y: CGFloat(5), width: CGFloat(25), height: CGFloat(25))
        button.addTarget(self, action: #selector(self.refresh), for: .touchUpInside)
        passwordTxtField.rightView = button
        passwordTxtField.rightViewMode = .always
    }
    @IBAction func refresh(_ sender: Any) {
        if(iconClick == true) {
            addingEyeBtn(image: "unsecured")
            passwordTxtField.isSecureTextEntry = false
        } else {
             addingEyeBtn(image: "secured")
            passwordTxtField.isSecureTextEntry = true
        }
        
        iconClick = !iconClick
    }
    
    
    @IBAction func createAction(_ sender: Any) {
        let vc = UIStoryboard.init(name: "Login", bundle: Bundle.main).instantiateViewController(withIdentifier: "CreateViewController") as? CreateViewController
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    @IBAction func loginAction(_ sender: Any) {
        if emailTxtField.text == "" && passwordTxtField.text == "" {
            let vc = UIStoryboard.timeLineStoryboard.instantiateErrorCarMsgPopVC()
            vc.popupType = .loginEmptyFields
            let cardPopup = SBCardPopupViewController(contentViewController: vc)
            
            cardPopup.show(onViewController: self)
        } else {
            LoginResponse.requestToLogin(email: emailTxtField.text ?? "", password: passwordTxtField.text ?? "") { [weak self](response) in
                guard let strongSelf = self else { return }
                print(response?.errorMessage)
                print(response?.success?.token)
                if response?.success?.token != nil {
                    if response?.success?.user?.isVerified == false {
                         UserDefaultsHandler.userSetToData(userData: response)
                        let vc = UIStoryboard.loginStoryboard.instantiateEmailVerificationVC()
                                              UserDefaultsHandler.setToUD(key: .token, value: response?.success?.token ?? "")
                                              strongSelf.navigationController?.pushViewController(vc, animated: true)
                    } else {
                         UserDefaultsHandler.userSetToData(userData: response)
                        strongSelf.changingRootToHome()
                    }
                   
                } else {
                    print(response?.errorMessage)
                    
                    let vc = UIStoryboard.timeLineStoryboard.instantiateErrorCarMsgPopVC()
                    vc.popupType = .loginUnAuthorised
                    let cardPopup = SBCardPopupViewController(contentViewController: vc)
                    cardPopup.show(onViewController: strongSelf)
                }
                
            }
        }
        
        
    }
    
    @IBAction func skipBtnAction(_ sender: Any) {
        changingRootToHome()
    }
    @IBAction func forgotPasswordAction(_ sender: Any) {
        let vc = UIStoryboard.loginStoryboard.instantiateForgotPasswordVC()
        vc.modalPresentationStyle = .fullScreen
        let navigationController = UINavigationController(rootViewController: vc)

        self.present(navigationController, animated: true, completion: nil)
       
    }
    func changingRootToHome() {
        if #available(iOS 13.0, *) {
            
            let mySceneDelegate = self.view.window?.windowScene?.delegate as? SceneDelegate
            mySceneDelegate?.loadTabbar()
        } else {
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.loadTabbar()
            // Fallback on earlier versions
        }
    }
}
