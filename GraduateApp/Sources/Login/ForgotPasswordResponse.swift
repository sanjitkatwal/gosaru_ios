//
//  ForgotPasswordResponse.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 1/14/21.
//  Copyright © 2021 Mousham Pradhan. All rights reserved.
//

import Foundation
import ObjectMapper
class ForgotPasswordResponse: DefaultResponse {
    var data: LoginResponseModel?
override func mapping(map: Map) {
  super.mapping(map: map)

}
// MARK: - login request
    class func requestToForgotPassword(param: [String : Any],password: String ,completionHandler:@escaping ((LoginResponseModel?) -> Void)) {
        APIManager(urlString: GraduateUrl.forgotPasswordURL, parameters: param, method: .post ).handleResponse(completionHandler: { (response: LoginResponseModel) in
              print(response)
                  completionHandler(response)
              }, failureBlock: {
                completionHandler(nil)
              })
      }
}
