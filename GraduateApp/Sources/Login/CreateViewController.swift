//
//  CreateViewController.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 6/23/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import UIKit
import FlagPhoneNumber


class CreateViewController: UIViewController {
    
    @IBOutlet weak var phoneTextField: FPNTextField!
    @IBOutlet weak var createBtn: AnimatedButton!
    @IBOutlet weak var checkbox: UIButton!
    @IBOutlet weak var passwordTxtFeidl: UITextField!
    @IBOutlet weak var emailTxtField: UITextField!
    @IBOutlet weak var nameTxtField: UITextField!
    @IBOutlet weak var countryTxtField: FPNTextField!
    var listController: FPNCountryListViewController = FPNCountryListViewController(style: .grouped)
    var iconClick = true
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Graduate"
        // Do any additional setup after loading the view.
        //aggrementBox.boxType =
        countryTxtField.setFlag(key: .NP)
        countryTxtField.displayMode = .list
        listController.setup(repository: countryTxtField.countryRepository)
        
        listController.didSelect = { [weak self] country in
            self?.countryTxtField.setFlag(countryCode: country.code)
        }
        self.countryTxtField.set(phoneNumber: "20394203948")
        countryTxtField.attributedPlaceholder = "Phone(optional)".toAttributed(alignment: .center)
        
        countryTxtField.delegate = self
        cornerRadiusForTxtField()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        createBtn.backgroundColor = UIColor.init(hexString: Appcolors.background)
        createBtn.setTitleColor(.lightGray, for: .normal)
        
    }
    func cornerRadiusForTxtField() {
        passwordTxtFeidl.layer.cornerRadius = 20
        emailTxtField.layer.cornerRadius = 20
        countryTxtField.layer.cornerRadius = 20
        nameTxtField.layer.cornerRadius = 20
    }
    func addingEyeBtn() {
        let button = UIButton(type: .custom)
        let image = UIImage(named: "secured")?.withRenderingMode(.alwaysTemplate)
        button.setImage(image, for: .normal)
        button.tintColor = UIColor(hexString: "A58E52")
        button.imageEdgeInsets = UIEdgeInsets(top: 0, left: -16, bottom: 0, right: 0)
        button.frame = CGRect(x: CGFloat(passwordTxtFeidl.frame.size.width - 25), y: CGFloat(5), width: CGFloat(25), height: CGFloat(25))
        button.addTarget(self, action: #selector(self.refresh), for: .touchUpInside)
        passwordTxtFeidl.rightView = button
        passwordTxtFeidl.rightViewMode = .always
    }
    @IBAction func refresh(_ sender: Any) {
        if(iconClick == true) {
            passwordTxtFeidl.isSecureTextEntry = false
        } else {
            passwordTxtFeidl.isSecureTextEntry = true
        }
        
        iconClick = !iconClick
    }
    
    
    
    @IBAction func checkboxAction(_ sender: Any) {
        //        print(checkbox.state)
        if checkbox.isSelected == true {
            checkbox.setImage(UIImage(named: ""), for: .normal)
            checkbox.backgroundColor = .white
            checkbox.isSelected = false
            createBtn.backgroundColor = UIColor.init(hexString: Appcolors.background)
            createBtn.setTitleColor(.lightGray, for: .normal)
        } else {
            checkbox.isSelected = true
            let image = UIImage(named: "iconCheck")?.withRenderingMode(.alwaysTemplate)
            checkbox.setImage(image, for: .normal)
            checkbox.tintColor = .white
            checkbox.backgroundColor = UIColor.init(hexString: Appcolors.buttons)
            createBtn.backgroundColor = .white
            createBtn.setTitleColor(UIColor.init(hexString: Appcolors.buttons), for: .normal)
        }
        
    }
    @IBAction func termsAndConitionAction(_ sender: Any) {
        let vc = UIStoryboard.moreStoryboard.instantiateDummyVC()
        vc.dummyString = "Terms & Conditions"
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func privacyPolicyAction(_ sender: Any) {
        let vc = UIStoryboard.moreStoryboard.instantiateDummyVC()
        vc.dummyString = "Privacy Policy"
        navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func termsOfUseAction(_ sender: Any) {
        let vc = UIStoryboard.moreStoryboard.instantiateDummyVC()
        vc.dummyString = "Terms Of Use"
        navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func loginAction(_ sender: Any) {
        let vc = UIStoryboard.loginStoryboard.instantiateLoginViewController()
        navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func createAction(_ sender: Any) {
        if checkbox.isSelected == true {
            createBtn.animateInteraction = true
            if nameTxtField.text == "" || passwordTxtFeidl.text == "" || phoneTextField.text == "" || emailTxtField.text == "" {
                let vc = UIStoryboard.timeLineStoryboard.instantiateErrorCarMsgPopVC()
                vc.popupType = .registerEmptyField
                let cardPopup = SBCardPopupViewController(contentViewController: vc)
                cardPopup.show(onViewController: self)
            } else {

                let params: [String: Any] = ["name" : nameTxtField.text ?? "", "phone" : phoneTextField.getRawPhoneNumber() ?? "", "password" : passwordTxtFeidl.text ?? "", "email" : emailTxtField.text ?? "","agree_terms" : "true"]
                RegisterResponse.requestToRegister(params: params) { [weak self](response) in
                     guard let strongSelf = self else {return}
                    if response?.success?.token == nil {
                        if response?.errors?.email?[0] != nil {
                            print(response?.errors?.email?[0])
//                            HelperFunctions.pushingToErrorMsgVC(popUptype: .emailAuth, vc: self!, message: response?.errors?.email?[0] ?? "")
                            let vc = UIStoryboard.timeLineStoryboard.instantiateErrorCarMsgPopVC()
                            vc.popupType = .emailAuth
                                   vc.messageString = response?.errors?.email?[0]
                                                  let cardPopup = SBCardPopupViewController(contentViewController: vc)
                                                  cardPopup.show(onViewController: strongSelf)
                        } else if response?.errors?.phone?[0] != nil {
                             let vc = UIStoryboard.timeLineStoryboard.instantiateErrorCarMsgPopVC()
                                                        vc.popupType = .pohneAuth
                                                               vc.messageString = response?.errors?.phone?[0]
                                                                              let cardPopup = SBCardPopupViewController(contentViewController: vc)
                                                                              cardPopup.show(onViewController: strongSelf)
                        } else if response?.errors?.password?[0] != nil {
                            strongSelf.popUPErrorMessageVC(message: response?.errors?.password?[0] ?? "", popType: .passwordValidation)
                        }
                    } else {
                        let vc = UIStoryboard.loginStoryboard.instantiateEmailVerificationVC()
                        UserDefaultsHandler.setToUD(key: .token, value: response?.success?.token ?? "")
                        strongSelf.navigationController?.pushViewController(vc, animated: true)
                    }
                    
                    
                }
            }
            
            
            
            
        } else {
            createBtn.animateInteraction = false
            print("Do nothing when the terms and condition are not selected")
           let vc = UIStoryboard.timeLineStoryboard.instantiateErrorCarMsgPopVC()
                               vc.popupType = .termsAndCondition
                               let cardPopup = SBCardPopupViewController(contentViewController: vc)
                               cardPopup.show(onViewController: self)
        }
    }
    func popUPErrorMessageVC(message: String, popType: ErrorType) {
        let vc = UIStoryboard.timeLineStoryboard.instantiateErrorCarMsgPopVC()
        vc.popupType = .pohneAuth
               vc.messageString = message
                              let cardPopup = SBCardPopupViewController(contentViewController: vc)
                              cardPopup.show(onViewController: self)
    }
}
extension CreateViewController: FPNTextFieldDelegate {
    
    /// The place to present/push the listController if you choosen displayMode = .list
    func fpnDisplayCountryList() {
        let navigationViewController = UINavigationController(rootViewController: listController)
        
        present(navigationViewController, animated: true, completion: nil)
    }
    
    /// Lets you know when a country is selected
    func fpnDidSelectCountry(name: String, dialCode: String, code: String) {
        print(name, dialCode, code) // Output "France", "+33", "FR"
    }
    
    /// Lets you know when the phone number is valid or not. Once a phone number is valid, you can get it in severals formats (E164, International, National, RFC3966)
    func fpnDidValidatePhoneNumber(textField: FPNTextField, isValid: Bool) {
        textField.rightViewMode = .always
        //textField.rightView = UIImageView(image: isValid ? #imageLiteral(resourceName: "success") : #imageLiteral(resourceName: "error"))
        
        print(
            isValid,
            textField.getFormattedPhoneNumber(format: .E164) ?? "E164: nil",
            textField.getFormattedPhoneNumber(format: .International) ?? "International: nil",
            textField.getFormattedPhoneNumber(format: .National) ?? "National: nil",
            textField.getFormattedPhoneNumber(format: .RFC3966) ?? "RFC3966: nil",
            textField.getRawPhoneNumber() ?? "Raw: nil"
        )
    }
    
}
extension String {
    func toAttributed(alignment: NSTextAlignment) -> NSAttributedString {
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = .center
        return toAttributed(attributes: [.paragraphStyle: paragraphStyle])
    }
    
    func toAttributed(attributes: [NSAttributedString.Key : Any]? = nil) -> NSAttributedString {
        return NSAttributedString(string: self, attributes: attributes)
    }
}
