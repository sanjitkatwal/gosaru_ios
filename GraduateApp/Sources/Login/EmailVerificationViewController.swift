//
//  EmailVerificationViewController.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 9/11/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import UIKit

class EmailVerificationViewController: UIViewController {

    @IBOutlet weak var messageLabel: UILabel!
    var token: String?
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: true)
    }
    

  
     @IBAction func refreshAction(_ sender: Any) {
    
        EmailVerificationResponse.requestToEmailVerification { [weak self](response) in
            guard let strongSelf = self else { return }
            //MARK:- EMAIL VERIFICATION 1 DEFINES THAT THE EMAIL IS VERIFIED AND 0 REPRESENTS UNVERIFIED
            if response?.emailVerified == true {
                strongSelf.changingRootToHome()
            } else {
                //MARK:- WHEN THE EMAIL IS NOT VERIFIED POP UP COMES
                let vc = UIStoryboard.timeLineStoryboard.instantiateErrorCarMsgPopVC()
                vc.popupType = .emailVerification
                               let cardPopup = SBCardPopupViewController(contentViewController: vc)
                               cardPopup.show(onViewController: strongSelf)
            }
            
        }
        
     }
    
    
    @IBAction func logoutAction(_ sender: Any) {
        //MARK:- REMOVING USER TOKEN AFTER LOGGIN OUT
        UserDefaultsHandler.removeUD(key: .token)
        
    }
    
    //MARK:- AFTER LOGGING OUT THE USE REDIRECTS TO THE WELCOME VC
    func changingRootToHome() {
        let vc = WelcomeViewController()
        let navVc = CustomNavigationController(rootViewController: vc)
        if #available(iOS 13.0, *) {
            
            let mySceneDelegate = self.view.window?.windowScene?.delegate as? SceneDelegate
            mySceneDelegate?.changeRootViewController(with: navVc)
        } else {
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.changeRootViewController(with: navVc)
            // Fallback on earlier versions
        }
    }
}
