//
//  storyboard+Login.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 6/24/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import Foundation
import UIKit
extension UIStoryboard {
private struct Constants {
    static let loginStoryboard = "Login"
    static let welcomeVCIdentifier = "WelcomeViewController"
     static let registerVCIdentifier = "WelcomeViewController"
     static let loginVCIdentifier = "LoginViewController"
     static let createVCIdentifier = "CreateViewController"
     static let emailVerficatoinVCIdentifier = "EmailVerificationViewController"
    
    static let forgotPasswordVCIdentifier = "ForgotPasswordViewController"
}
static var loginStoryboard: UIStoryboard {
    return UIStoryboard(name: Constants.loginStoryboard, bundle: nil)
}
func instantiateWelcomeViewController() -> WelcomeViewController {
    guard let viewController = UIStoryboard.loginStoryboard.instantiateViewController(withIdentifier: Constants.welcomeVCIdentifier) as? WelcomeViewController else {
        fatalError("Couldn't instantiate WelcomeViewController")
    }
    return viewController
}
    func instantiateRegisterViewController() -> RegisterViewController {
        guard let viewController = UIStoryboard.loginStoryboard.instantiateViewController(withIdentifier: Constants.registerVCIdentifier) as? RegisterViewController else {
            fatalError("Couldn't instantiate RegisterViewController")
        }
        return viewController
    }
    func instantiateLoginViewController() -> LoginViewController {
           guard let viewController = UIStoryboard.loginStoryboard.instantiateViewController(withIdentifier: Constants.loginVCIdentifier) as? LoginViewController else {
               fatalError("Couldn't instantiate LoginViewController")
           }
           return viewController
       }
    func instantiateCreateViewController() -> CreateViewController {
        guard let viewController = UIStoryboard.loginStoryboard.instantiateViewController(withIdentifier: Constants.createVCIdentifier) as? CreateViewController else {
            fatalError("Couldn't instantiate CreateViewController")
        }
        return viewController
    }
    func instantiateEmailVerificationVC() -> EmailVerificationViewController {
           guard let viewController = UIStoryboard.loginStoryboard.instantiateViewController(withIdentifier: Constants.emailVerficatoinVCIdentifier) as? EmailVerificationViewController else {
               fatalError("Couldn't instantiate EmailVerificationViewController")
           }
           return viewController
       }
    func instantiateForgotPasswordVC() -> ForgotPasswordViewController {
           guard let viewController = UIStoryboard.loginStoryboard.instantiateViewController(withIdentifier: Constants.forgotPasswordVCIdentifier) as? ForgotPasswordViewController else {
               fatalError("Couldn't instantiate ForgotPasswordViewController")
           }
           return viewController
       }
}
