//
//  AppDelegate.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 6/22/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import GooglePlaces
import FirebaseCore
import FirebaseInstanceID
import Messages
import FirebaseMessaging
import UserNotifications

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

var gcmMessageIDKey = "gcMessage"
var window: UIWindow?
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
         self.window = UIWindow(frame: UIScreen.main.bounds)
        FirebaseApp.configure()
//        let manager = PushNotificationManager(userID: "")
//        manager.registerForPushNotification(application: application)
        if #available(iOS 10.0, *) {
          // For iOS 10 display notification (sent via APNS)
          UNUserNotificationCenter.current().delegate = self

          let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
          UNUserNotificationCenter.current().requestAuthorization(
            options: authOptions,
            completionHandler: {_, _ in })
        } else {
          let settings: UIUserNotificationSettings =
          UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
          application.registerUserNotificationSettings(settings)
        }

        application.registerForRemoteNotifications()
        Messaging.messaging().delegate = self

       
      //  Messaging.messaging().delegate = self
//registerForPushNotification(application: application)
        

        GMSPlacesClient.provideAPIKey(GraduateUrl.googleApiKey)
        let textColor = [NSAttributedString.Key.foregroundColor:UIColor(hexString: Appcolors.primary)]
          UINavigationBar.appearance().titleTextAttributes = textColor
        let BarButtonItemAppearance = UIBarButtonItem.appearance()
        BarButtonItemAppearance.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.clear], for: .normal)
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
         loadinitialVC()
        self.window?.makeKeyAndVisible()
        
        return true
    }


    // MARK: UISceneSession Lifecycle

    @available(iOS 13.0, *)
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    @available(iOS 13.0, *)
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }
    class func shared() -> AppDelegate? {
      return UIApplication.shared.delegate as? AppDelegate
    }
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        Messaging.messaging().apnsToken = deviceToken
        Messaging.messaging().isAutoInitEnabled = true
       
        InstanceID.instanceID().instanceID(handler: { (result, error) in

            if let error = error {
              print("Error fetching remote instange ID: (error)")
            } else if let result = result {
             // UserDefaultsHandler.setToUD(key: UDkey.fcmToken, value: result.token)
              print("Remote instance ID token: (result.token)")
                print(result.token)
                print(Messaging.messaging().fcmToken)
               
             // LoginResponseModel.requestToSendFCMToken()
    }
        })
        let deviceTokenString = deviceToken.hexString
           print(deviceTokenString)
       
        
       // UserDefaultsHandler.setToUD(key: .deviceToken, value: token)
        let tokens = Messaging.messaging().fcmToken
        print(tokens)
        UserDefaultsHandler.setToUD(key: .deviceToken, value: tokens)

        }
    func changeRootViewController(with desiredViewController: UIViewController) {
      // var window: UIWindow?
       var mainWindow: UIWindow?
       if #available(iOS 13.0, *) {
          let windowScene = UIApplication.shared.connectedScenes.first as? UIWindowScene
          mainWindow = (windowScene?.delegate as? SceneDelegate)?.window
           //drawerContainer = mainWindow?.rootViewController as? MMDrawerController
       } else {
           //drawerContainer = mainWindow?.rootViewController as! MMDrawerController
         mainWindow = AppDelegate.shared()?.window
        }
      let snapshot: UIView = (self.window?.snapshotView(afterScreenUpdates: true))!
      desiredViewController.view.addSubview(snapshot)
      
      self.window?.rootViewController = desiredViewController
      UIView.animate(withDuration: 0.3, animations: {() in
        snapshot.layer.opacity = 0
        snapshot.layer.transform = CATransform3DMakeScale(1.5, 1.5, 1.5)
      }, completion: { (_: Bool) in
        snapshot.removeFromSuperview()
      })
    }
    func loadinitialVC() {
         var newWindow: UIWindow?
       
        if #available(iOS 13, *) {
            
            //var mainWindow: UIWindow?
//            self.window = UIWindow()
//                                  let vc = WelcomeViewController()
//                                  self.window!.rootViewController = vc
//                                  self.window!.makeKeyAndVisible()
//            let windowScene = UIApplication.shared.connectedScenes.first as? UIWindowScene
//                  let mainWindow = (windowScene?.delegate as? SceneDelegate)?.window
//            let vc = WelcomeViewController()
//            mainWindow?.rootViewController = vc
//            mainWindow?.makeKeyAndVisible()
            let windowScene = UIApplication.shared.connectedScenes.first as? UIWindowScene
               newWindow = (windowScene?.delegate as? SceneDelegate)?.window
            
                       // do only pure app launch stuff, not interface stuff
                   } else {
             newWindow = AppDelegate.shared()?.window
                   }
        let storyboard = UIStoryboard(name: "Login", bundle: nil)
         let rootVC = storyboard.instantiateWelcomeViewController()
        let rootNC = UINavigationController(rootViewController: rootVC)
       // let vc = WelcomeViewController()
        newWindow?.rootViewController = rootNC
        newWindow?.makeKeyAndVisible()
    }
//    private func registerForPushNotification(application: UIApplication) {
//      // Messaging.messaging().delegate = self
//       if #available(iOS 10.0, *) {
//         // For iOS 10 display notification (sent via APNS)
//         UNUserNotificationCenter.current().delegate = self
//         let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
//         UNUserNotificationCenter.current().requestAuthorization(
//           options: authOptions,
//           completionHandler: {_, _ in })
//       } else {
//         let settings: UIUserNotificationSettings =
//           UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
//         application.registerUserNotificationSettings(settings)
//       }
//       application.registerForRemoteNotifications()
//     }
    func loadTabbar() {
      var mainWindow: UIWindow?
      if #available(iOS 13.0, *) {
        let windowScene = UIApplication.shared.connectedScenes.first as? UIWindowScene
        mainWindow = (windowScene?.delegate as? SceneDelegate)?.window
      } else {
        mainWindow = AppDelegate.shared()?.window
      }
    //MARK: BEFORE TAB BAR
      //let controller = CustomTabbarController()
        
       // MARK: AFTER TAB BAR
        //let control = CustomTabbarController()
            let controller = CustomManyTabBar()
       // controller.tabController.setIndex(3)
      //    changeRootViewController(with: drawerContainer!)
        changeRootViewController(with: controller)
     // mainWindow?.rootViewController = controller
      mainWindow?.makeKeyAndVisible()
    }
    func loadTabbarWithIndex(index: Int) {
      var mainWindow: UIWindow?
      if #available(iOS 13.0, *) {
        let windowScene = UIApplication.shared.connectedScenes.first as? UIWindowScene
        mainWindow = (windowScene?.delegate as? SceneDelegate)?.window
      } else {
        mainWindow = AppDelegate.shared()?.window
      }
    //MARK: BEFORE TAB BAR
      //let controller = CustomTabbarController()
        
       // MARK: AFTER TAB BAR
        //let control = CustomTabbarController()
            let controller = CustomManyTabBar()
        controller.tabController.setIndex(index)
      //    changeRootViewController(with: drawerContainer!)
        changeRootViewController(with: controller)
     // mainWindow?.rootViewController = controller
      mainWindow?.makeKeyAndVisible()
    }
    func loadLoginPage() {
      var mainWindow: UIWindow?
      if #available(iOS 13.0, *) {
        let windowScene = UIApplication.shared.connectedScenes.first as? UIWindowScene
        mainWindow = (windowScene?.delegate as? SceneDelegate)?.window
      } else {
        mainWindow = AppDelegate.shared()?.window
      }
    //MARK: BEFORE TAB BAR
      //let controller = CustomTabbarController()
        
       // MARK: AFTER TAB BAR
       
        let controller = UIStoryboard.loginStoryboard.instantiateLoginViewController()
      //    changeRootViewController(with: drawerContainer!)
       /// mainWindow?.rootViewController = controller
       changeRootViewController(with: controller)
     // mainWindow?.rootViewController = controller
      mainWindow?.makeKeyAndVisible()
    }
    func changingRootViewController(vc: UIViewController) {
         var mainWindow: UIWindow?
         if #available(iOS 13.0, *) {
           let windowScene = UIApplication.shared.connectedScenes.first as? UIWindowScene
           mainWindow = (windowScene?.delegate as? SceneDelegate)?.window
         } else {
           mainWindow = AppDelegate.shared()?.window
         }
      
           let controller = vc
        
          changeRootViewController(with: controller)
         mainWindow?.makeKeyAndVisible()
       }
}
extension AppDelegate: MessagingDelegate {
  func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
    print("Firebase registration token: \(fcmToken)")
   // UserDefaultsHandler.setToUD(key: .deviceToken, value: fcmToken)
    let dataDict: [String: String] = ["token": fcmToken]
    // Note: This callback is fired at each app startup and whenever a new token is generated.
  
  }
  // The callback to handle data message received via FCM for devices running iOS 10 or above.
  func application(received remoteMessage: MessagingRemoteMessage) {
    print(remoteMessage.appData)
  }
}
extension AppDelegate: UNUserNotificationCenterDelegate {
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        if let messageID = userInfo[gcmMessageIDKey] {
          print("Message ID: \(messageID)")
        }

        // Print full message.
        print(userInfo)
    }
  // Receive displayed notifications for iOS 10 devices.
  func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
    let userInfo = notification.request.content.userInfo
    // With swizzling disabled you must let Messaging know about the message, for Analytics
    // Messaging.messaging().appDidReceiveMessage(userInfo)
    // Print message ID.
    // Print full message.
    print(userInfo)
//    setToDataBase(data: userInfo)
    return completionHandler([.alert, .badge, .sound])//completionHandler(UNNotificationPresentationOptions.alert)
  }
  func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
    let userInfo = response.notification.request.content.userInfo
    // Print message ID.
    //    handleNotificaiton(userInfo: userInfo)
    print(userInfo)
//    setToDataBase(data: userInfo)
    return completionHandler()
  }
    
}
extension Data {
    var hexString: String {
        let hexString = map { String(format: "%02.2hhx", $0) }.joined()
        return hexString
    }
}

