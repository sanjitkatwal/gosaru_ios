//
//  CustomNavigationController.swift
//  OtoTrack
//
//  Created by Prashant Ghimire on 8/7/18.
//  Copyright © 2018 prashantghimire@gmail.com. All rights reserved.
//

import UIKit

class CustomNavigationController: UINavigationController {
  override func viewDidLoad() {
    super.viewDidLoad()
//   self.navigationBar.setBackgroundImage(UIImage(), for: .default)
//    self.navigationBar.shadowImage = UIImage()
    self.navigationBar.isTranslucent = true
    //self.hero.isEnabled = true
    self.view.backgroundColor = .black
    self.navigationBar.tintColor = UIColor.init(hexString: Appcolors.buttons)
  self.navigationBar.barTintColor = UIColor.init(hexString: Appcolors.background)
    self.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.init(hexString: Appcolors.text)]
    let yourBackImage = UIImage(named: "iconBack")
    self.navigationBar.backIndicatorImage = yourBackImage// will remove back "<" from back8
    //self.navigationBar.backIndicatorTransitionMaskImage = UIImage()
   // self.navigationBar.barTintColor = UIColor.init(hexString: Appcolors.background)
  }
}

extension UINavigationController {
  func containsViewController(ofKind kind: AnyClass) -> Bool {
    return self.viewControllers.contains(where: { $0.isKind(of: kind) })
  }
}
