//
//  CustomManyTabBar.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 6/25/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import UIKit


class CustomManyTabBar: UIViewController {
var tabController:AZTabBarController!
    var chhosetab: Bool = false
    override func viewDidLoad() {
        super.viewDidLoad()
//        self.modalTransitionStyle = .crossDissolve
//           self.modalPresentationStyle = .overCurrentContext
          // self.tabBar.backgroundImage = UIImage()
       
        // Do any additional setup after loading the view.
         var icons = [UIImage]()
        icons.append(UIImage(named: "timelineUnselected")!)
              icons.append(UIImage(named: "liveUnselected")!)
              icons.append(UIImage(named: "iconSpot")!)
              icons.append(UIImage(named: "iconOffer")!)
              icons.append(UIImage(named: "iconGraduate")!)
               icons.append(UIImage(named: "iconMore")!)

        //The icons that will be displayed for each tab once they are selected.
        var sIcons = [UIImage]()
               sIcons.append(UIImage(named: "timeline4")!)
               sIcons.append(UIImage(named: "live6")!)
               sIcons.append(UIImage(named: "iconSpot")!)
               sIcons.append(UIImage(named: "iconOffer")!)
               sIcons.append(UIImage(named: "iconGraduate")!)
               sIcons.append(UIImage(named: "iconMore")!)
        //tabController = .insert(into: self, withTabIconNames: icons, andSelectedIconNames: selectedIcons)
                tabController = .insert(into: self, withTabIcons: icons, andSelectedIcons: sIcons)
//         tabController.setIndex(1, animated: true)
//         tabController.setIndex(2, animated: true)
     
        tabController.setAction(atIndex: 3, action: change)
        tabController.animateTabChange = true
        tabController.defaultColor = UIColor.init(hexString: Appcolors.buttons)
       // tabController.selectedColor = UIColor.init(hexString: Appcolors.tabBarSelectedColor)
        tabController.selectedColor = .black

    

       
        tabController.delegate = self
        
        tabController.selectedColor = UIColor(hexString: Appcolors.buttons)
       // tabController.highlightColor = UIColor(hexString: Appcolors.tabBarSelectedColor)

        tabController.highlightColor = .black
        //loadControllers()
        tabController.setBar(hidden: true, animated: true, duration: 1) { (Bool) in
            //self.tabController.tabBarController?.tabBar.layer.zPosition = -1
           
           
        }
        tabController.setBar(hidden: true, animated: true)
        self.tabBarController?.tabBar.isHidden = false
        //tabController.tabBarHeight = -10
        tabController.separatorLineVisible = false
        tabController.separatorLineColor = UIColor.init(hexString: Appcolors.buttons)
        //tabController.controllersContainer.isHidden = true
        
       
      
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        loadControllers()
        
    }
    override func viewDidAppear(_ animated: Bool) {
        self.tabController.buttonsContainer.borders(for: [UIRectEdge.top], width: 1, color: UIColor.init(hexString: Appcolors.buttons))
        self.tabController.selectionIndicator = .none
        self.tabController.selectionIndicatorColor = UIColor.init(hexString: Appcolors.buttons)
        self.tabController.separatorLineVisible = false
     
    
    }
    func loadControllers() {
//        let timeLine = UIStoryboard.timeLineStoryboard.instantiateTimeLineViewController()
//               let watch = UIStoryboard.watchStoryboard.instantiateWatchViewController()
//               let spot = UIStoryboard.spotStoryboard.instantiateSpotViewController()
//               let offer = UIStoryboard.offersStoryboard.instantiateOffersViewController()
//               let graduate = UIStoryboard.offersStoryboard.instantiateGraduateViewController()
//               let more = UIStoryboard.moreStoryboard.instantiateMoreViewController()
        let timeLineNav = CustomNavigationController(rootViewController: UIStoryboard.timeLineStoryboard.instantiateTimeLineViewController())
           let watchNav = CustomNavigationController(rootViewController: UIStoryboard.watchStoryboard.instantiateWatchViewController())
           let spotNav = CustomNavigationController(rootViewController: UIStoryboard.spotStoryboard.instantiateSpotViewController())
           let offersNav = CustomNavigationController(rootViewController: UIStoryboard.offersStoryboard.instantiateOffersViewController())
           let graduateNav = CustomNavigationController(rootViewController: UIStoryboard.graduateStoryboard.instantiateGraduateViewController())
           let moreNav = CustomNavigationController(rootViewController: UIStoryboard.moreStoryboard.instantiateMoreViewController())
        
        tabController.setViewController(timeLineNav, atIndex: 0)
         tabController.setViewController(watchNav, atIndex: 1)
         tabController.setViewController(spotNav, atIndex: 2)
         tabController.setViewController(offersNav, atIndex: 3)
         tabController.setViewController(graduateNav, atIndex: 4)
         tabController.setViewController(moreNav, atIndex: 5)
        
        tabController.setTitle("Timeline", atIndex: 0)
               tabController.setTitle("Watch", atIndex: 1)
               tabController.setTitle("Spot", atIndex: 2)
               tabController.setTitle("Offer", atIndex: 3)
               tabController.setTitle("Graduate", atIndex: 4)
                tabController.setTitle("More", atIndex: 5)
               tabController.font = UIFont(name: "AvenirNext-Regular", size: 12)
        if chhosetab == true {
            tabController.setIndex(1)
        }
        //tabController.selectedIndex = 2
    }
    @objc func change () {
        self.modalTransitionStyle = .crossDissolve
                 self.modalPresentationStyle = .overCurrentContext
    }
  func addTopBorder(with color: UIColor?, andWidth borderWidth: CGFloat) {
      let border = UIView()
      border.backgroundColor = color
      border.autoresizingMask = [.flexibleWidth, .flexibleBottomMargin]
//      border.frame = CGRect(x: 0, y: 0, width: frame.size.width, height: borderWidth)
//    self.addSubview(border)
  }

}
extension CustomManyTabBar: AZTabBarDelegate {
    func tabBar(_ tabBar: AZTabBarController, shouldAnimateButtonInteractionAtIndex index: Int) -> Bool {
        return true
    }
}
extension UIImage {
    class func colorForNavBar(color: UIColor) -> UIImage {
        let rect = CGRect(x: 0.0, y: 0.0, width: 1.0, height: 1.0)
        //    Or if you need a thinner border :
        //    let rect = CGRect(x: 0.0, y: 0.0, width: 1.0, height: 0.5)
        UIGraphicsBeginImageContext(rect.size)
        let context = UIGraphicsGetCurrentContext()

        context!.setFillColor(color.cgColor)
        context!.fill(rect)

        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        return image!
    }
    
}
