//
//  CustomTabbarController.swift
//  ServiceIdol
//
//  Created by Prashant Ghimire on 4/2/19.
//  Copyright © 2019 prashantghimire@gmail.com. All rights reserved.
//

import Foundation
import UIKit

class CustomTabbarController: UITabBarController {
    var indexSelected: Int?
  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view.
//    self.modalTransitionStyle = .crossDissolve
//    self.modalPresentationStyle = .overCurrentContext
    self.tabBar.backgroundImage = UIImage()
    self.delegate = self
    self.tabBar.tintColor = UIColor.init(hexString: Appcolors.primary)
    self.tabBar.unselectedItemTintColor = UIColor(red: 0.502, green: 0.533, blue: 0.557, alpha: 1.00)
    self.tabBar.backgroundColor = UIColor(red: 0.980, green: 0.980, blue: 0.980, alpha: 1.00)
    self.loadViewControllers()
    tabBarController?.selectedIndex = 2
  }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       
//        if indexSelected != nil {
//            tabBarController?.selectedIndex = (indexSelected ?? 0)
//        } else {
//            return
//        }
    }
  func loadViewControllers() {
    let timeLineNav = CustomNavigationController(rootViewController: UIStoryboard.timeLineStoryboard.instantiateTimeLineViewController())
    let watchNav = CustomNavigationController(rootViewController: UIStoryboard.watchStoryboard.instantiateWatchViewController())
    let spotNav = CustomNavigationController(rootViewController: UIStoryboard.spotStoryboard.instantiateSpotViewController())
    let offersNav = CustomNavigationController(rootViewController: UIStoryboard.offersStoryboard.instantiateOffersViewController())
    let graduateNav = CustomNavigationController(rootViewController: UIStoryboard.graduateStoryboard.instantiateGraduateViewController())
    let moreNav = CustomNavigationController(rootViewController: UIStoryboard.moreStoryboard.instantiateMoreViewController())
   
    self.viewControllers = [timeLineNav, watchNav, spotNav, offersNav, graduateNav, moreNav]
  }
}
// MARK: - UITabBarControllerDelegate
extension CustomTabbarController: UITabBarControllerDelegate {
  /*
   Called to allow the delegate to return a UIViewControllerAnimatedTransitioning delegate object for use during a noninteractive tab bar view controller transition.
   ref: https://developer.apple.com/documentation/uikit/uitabbarcontrollerdelegate/1621167-tabbarcontroller
   */
  func tabBarController(_ tabBarController: UITabBarController, animationControllerForTransitionFrom fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
    return TabBarAnimatedTransitioning()
  }
//    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
//        let vcindex = tabBarController.viewControllers!.index(of: viewController)
//        if vcindex == 2 {
//            let popup = PopUpViewController.create()
//                                    let sbPopup = SBCardPopupViewController(contentViewController: popup)
//           popup.delegate = self
//                                    sbPopup.show(onViewController: self)
//            
//            return false
//        }
////        if viewController.isKind(of: UploadsViewController.self) {
////            print("mausam")
////            let vc =  UploadsViewController()
////            vc.modalPresentationStyle = .overFullScreen
////            self.present(vc, animated: true, completion: nil)
//////            let popup = PopUpViewController.create()
//////                        let sbPopup = SBCardPopupViewController(contentViewController: popup)
//////                        sbPopup.show(onViewController: self)
//////
////                        return false
////
////        }
////        if viewController == viewControllers?[3] {
//////            self.definesPresentationContext = true
//////                        let vc =  UploadsViewController()
//////                        vc.modalPresentationStyle = .overCurrentContext
//////                        vc.modalTransitionStyle = .crossDissolve
//////                        self.present(vc, animated: false, completion: nil)
//////            return true
////            let button1 = UIButton()
////            button1.setTitle("Cancel", for: .normal)
////            button1.layer.cornerRadius = 5
////            button1.setTitleColor(UIColor.white, for: .normal)
////            //button1.addTarget(self, action: #selector(cancelButtonPressed(_:)), for: .touchUpInside)
////            button1.backgroundColor = UIColor.lightGray
////
////            let button2 = UIButton()
////            button2.setTitle("Settings", for: .normal)
////            button2.layer.cornerRadius = 5
////            button2.backgroundColor = UIColor.lightGray
////            button2.setTitleColor(UIColor.white, for: .normal)
////           // button2.addTarget(self, action: #selector(settingButtonPresssed(_:)), for: .touchUpInside)
////
////            let sv = UIStackView(arrangedSubviews: [button1,button2])
////            sv.distribution = .equalSpacing
////
////            let currentView = tabBarController.selectedViewController!.view!
////            sv.frame = CGRect(x: currentView.bounds.midX - 80, y: currentView.bounds.midY + 150, width: 160, height: 40)
////            currentView.addSubview(sv)
////            currentView.bringSubviewToFront(sv)
////            return true
////        }
////        if viewController == viewControllers?[2]
////        {
////        
////            let popup = PopUpViewController.create()
////            let sbPopup = SBCardPopupViewController(contentViewController: popup)
////            sbPopup.show(onViewController: self)
////        
////            return false
////        }
//        
//        return true
//    }
}
final class TabBarAnimatedTransitioning: NSObject, UIViewControllerAnimatedTransitioning {
  /*
   Tells your animator object to perform the transition animations.
   */
  func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
    guard let destination = transitionContext.view(forKey: UITransitionContextViewKey.to) else { return }
    destination.alpha = 0.0
    destination.transform = .init(scaleX: 1.5, y: 1.5)
    transitionContext.containerView.addSubview(destination)
    UIView.animate(withDuration: transitionDuration(using: transitionContext), animations: {
      destination.alpha = 1.0
      destination.transform = .identity
    }, completion: { transitionContext.completeTransition($0) })
  }
  /*
   Asks your animator object for the duration (in seconds) of the transition animation.
   */
  func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
    return 0.25
  }
//    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
//        if viewController.title == "Uploads" {
//            print("mausam")
////            self.definesPresentationContext = true
////            let vc =  UploadsViewController()
////            vc.modalPresentationStyle = .overCurrentContext
////            vc.modalTransitionStyle = .crossDissolve
////            self.present(vc, animated: false, completion: nil)
//            return true
//        }
//        return true
//    }
}


