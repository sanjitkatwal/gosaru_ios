//
//  PhotoUploadManager.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 9/23/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireObjectMapper

class PhotoUploadManager {
    class func uploadPhoto<T: DefaultResponse>(url: String,method: HTTPMethod,profileImage: UIImage,coverImg: UIImage, profileImageName: String,coverImageName: String, param: [String : Any],multiplePhoto:[ UIImage], completionHandler: @escaping (T) -> Void, failureBlock: @escaping (() -> Void)) {
        let profileImgData: Data?
        var coverImgData: Data?
        if coverImg.size.width != 0 {
            coverImgData = coverImg.jpegData(compressionQuality: 0.2)!
        } else {
            coverImgData = nil
        }
        
        if profileImage.size.width != 0 {
            profileImgData = profileImage.jpegData(compressionQuality: 0.2)!
        } else {
            profileImgData = nil
        }
        var header: [String : Any]
        if let token = UserDefaultsHandler.getUDValue(key: UDkey.token) as? String {
            header = ["Authorization": "Bearer \(token)", "Accept": "application/json"]
        } else {
            header = ["Accept": "application/json"]
            
        }
        Alamofire.upload(multipartFormData: { multipartFormData in
            
            
            if coverImgData != nil {
                multipartFormData.append(coverImgData!, withName: coverImageName,fileName: "file.jpg", mimeType: "image/jpg")
            }
            if profileImgData != nil {
                
                
                multipartFormData.append(profileImgData!, withName: profileImageName,fileName: "file.jpg", mimeType: "image/jpg")
                
            }
            
            //multipartFormData.append(coverImg, withName: coverImageName,fileName: "file.jpg", mimeType: "image/jpg")
            print(param,url,coverImgData,profileImgData,method,header)
            ProgressHud.showProgressHUD()
            for (key, value) in param {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            } //Optional for extra parameters
        }, to: url, method: method, headers: (header as! HTTPHeaders))
        { (result) in
            
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    // guard let strongSelf = self else {return}
                    print("Upload Progress: \(progress.fractionCompleted)")
                })
                
                //                    upload.responseJSON { response in
                //                        print(response.result.value)
                //                        ProgressHud.hideProgressHUD()
                //    //                    self.updated = true
                //    //                    self.showpopUp(type: .profileUpdated)
                //                    }
                upload.responseObject { (response: DataResponse<T>) in
                    print(response.result.value)
                    completionHandler(response.result.value!)
                }
                
            case .failure(let encodingError):
                print(encodingError)
            }
        }
    }
    
}
class ImageUploader {
    
    static func uploadImage<T: DefaultResponse>(url: String,videos: [URL],imageName : String,
                                                parameters: [String: Any],
                                                imagesDataSource: [UIImage],completionSuccess: @escaping (T) -> Void,
                                                completionError: @escaping (() -> Void)) {
        var header: [String: Any]
        if let token = UserDefaultsHandler.getUDValue(key: UDkey.token) as? String {
            header = ["Authorization": "Bearer \(token)", "Accept": "application/json"]
        } else {
            header = ["Accept": "application/json"]
            
        }
        // let imageData = image.jpegData(compressionQuality: 0.5)
         let timestamp = NSDate().timeIntervalSince1970 // just for some random name.
        Alamofire.upload(multipartFormData: { multipartFormData in
            // import image to request
            print(url,parameters,header)
            ProgressHud.showProgressHUD()
            if imagesDataSource != []{
                for imageData in imagesDataSource {
                    let data = imageData.jpegData(compressionQuality: 0.9)
                    multipartFormData.append(data!, withName: "images", fileName: "\(Date().timeIntervalSince1970).jpeg", mimeType: "image/jpeg")
                    
                }
            }
            if videos != [] {
                for videos in videos {
                multipartFormData.append(videos, withName: "videos[]", fileName: "\(timestamp).mp4", mimeType: "\(timestamp)/mp4")
                }
            }
            
            for (key, value) in parameters {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
        }, to: url, method: .post ,headers: (header as! HTTPHeaders),
           
           encodingCompletion: { encodingResult in
            
            switch encodingResult {
            case .success(let upload, _, _):
                upload.responseObject { (response: DataResponse<T>) in
                   
                        //                                                          guard let strongSelf = self else { return }
                        switch response.result {
                        case .success(let dataX):
                            ProgressHud.hideProgressHUD()
                            print(response.result.value)
                            print(response)
                            let data = response.result.value as? NSDictionary
                                print(data)
                            
//                            if showBanner {
//                                //          appDelegate?.window?.rootViewController?.showBanner(title: dataX.message ?? "", isError: response.response?.statusCode == 200 ? false:true)
//                            }
                            if response.response?.statusCode == 200 || response.response?.statusCode == 201 || response.response?.statusCode == 202 {
                               
                                completionSuccess(dataX)
                            } else {
                                
                              
                                ProgressHud.hideProgressHUD()
                                completionSuccess(dataX)
                            }
                        case .failure(let error):
                            ProgressHud.hideProgressHUD()
                            print(error)
                        }
                        print(response.result.value)
                       /// completionSuccess(response)                               //completionSuccess(result:JSON(data: data!))
                        
                        //}
                        //                          upload.responseJSON { [weak self]response in
                        //                              guard let strongSelf = self else { return }
                        //                                if (error != nil) {
                        //                                                           completionError(error: error!)
                        //                                                       }else{
                        //                                                           completionSuccess(result:JSON(data: data!))
                        //                                                       }
                        //                          }
                        
                        
                        
                    }
                
                
            case .failure(_):
                print("")
            }
        })
        
    }
}
class ImageUploaderWithObjectArrayId {
    
    static func uploadImage<T: DefaultResponse>(url: String,videos: [URL],imageName : String,
                                                parameters: [String: Any],arrayObjects: [[String : Any]],
                                                imagesDataSource: [UIImage],completionSuccess: @escaping (T) -> Void,
                                                completionError: @escaping (() -> Void)) {
        var header: [String: Any]
        if let token = UserDefaultsHandler.getUDValue(key: UDkey.token) as? String {
            header = ["Authorization": "Bearer \(token)", "Accept": "application/json"]
        } else {
            header = ["Accept": "application/json"]
            
        }
        // let imageData = image.jpegData(compressionQuality: 0.5)
         let timestamp = NSDate().timeIntervalSince1970 // just for some random name.
        Alamofire.upload(multipartFormData: { multipartFormData in
            // import image to request
            print(url,parameters,header,arrayObjects)
            ProgressHud.showProgressHUD()
            if imagesDataSource != []{
                for imageData in imagesDataSource {
                    let data = imageData.jpegData(compressionQuality: 0.9)
                    multipartFormData.append(data!, withName: "images[]", fileName: "\(Date().timeIntervalSince1970).jpeg", mimeType: "image/jpeg")
                    
                }
            }
            if videos != [] {
                for videos in videos {
                multipartFormData.append(videos, withName: "videos[]", fileName: "\(timestamp).mp4", mimeType: "\(timestamp)/mp4")
                }
            }
            
            for (key, value) in parameters {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
            if arrayObjects.count != 0 {
                for item in arrayObjects {
                    for (key, value) in item {
                        multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
                    }
                }
            }
        }, to: url, method: .post ,headers: (header as! HTTPHeaders),
           
           encodingCompletion: { encodingResult in
            
            switch encodingResult {
            case .success(let upload, _, _):
                upload.responseObject { (response: DataResponse<T>) in
                   
                        //                                                          guard let strongSelf = self else { return }
                        switch response.result {
                        case .success(let dataX):
                            ProgressHud.hideProgressHUD()
                            print(response.result.value)
                            print(response.error)
                            print(response)
                            let data = response.result.value as? NSDictionary
                                print(data)
                            
//                            if showBanner {
//                                //          appDelegate?.window?.rootViewController?.showBanner(title: dataX.message ?? "", isError: response.response?.statusCode == 200 ? false:true)
//                            }
                            if response.response?.statusCode == 200 || response.response?.statusCode == 201 || response.response?.statusCode == 202 || response.response?.statusCode == 500 {
                               
                                completionSuccess(dataX)
                            } else {
                                
                              
                                ProgressHud.hideProgressHUD()
                                completionSuccess(dataX)
                            }
                        case .failure(let error):
                            ProgressHud.hideProgressHUD()
                            print(error)
                        }
                        print(response.result.value)
                       /// completionSuccess(response)                               //completionSuccess(result:JSON(data: data!))
                        
                        //}
                        //                          upload.responseJSON { [weak self]response in
                        //                              guard let strongSelf = self else { return }
                        //                                if (error != nil) {
                        //                                                           completionError(error: error!)
                        //                                                       }else{
                        //                                                           completionSuccess(result:JSON(data: data!))
                        //                                                       }
                        //                          }
                        
                        
                        
                    }
                
                
            case .failure(_):
                ProgressHud.hideProgressHUD()
                completionError()
               
                print("")
            }
        })
        
    }
}
class ImageUploaderInChat {
    
    static func uploadImage<T: DefaultResponse>(url: String,videos: [URL],imageName : String,
                                                parameters: [String: Any],
                                                imagesDataSource: [UIImage],showHUD: Bool,completionSuccess: @escaping (T) -> Void,
                                                completionError: @escaping (() -> Void)) {
        var header: [String: Any]
        if let token = UserDefaultsHandler.getUDValue(key: UDkey.token) as? String {
            header = ["Authorization": "Bearer \(token)", "Accept": "application/json"]
        } else {
            header = ["Accept": "application/json"]
            
        }
        // let imageData = image.jpegData(compressionQuality: 0.5)
         let timestamp = NSDate().timeIntervalSince1970 // just for some random name.
        Alamofire.upload(multipartFormData: { multipartFormData in
            // import image to request
            print(url,parameters,header)
            if showHUD == true {
                 ProgressHud.showProgressHUD()
            }
           
            if imagesDataSource != []{
                for imageData in imagesDataSource {
                    let data = imageData.jpegData(compressionQuality: 0.9)
                    multipartFormData.append(data!, withName: "image", fileName: "\(Date().timeIntervalSince1970).jpeg", mimeType: "image/jpeg")
                    
                }
            }
            if videos != [] {
                for videos in videos {
                multipartFormData.append(videos, withName: "video", fileName: "\(timestamp).mp4", mimeType: "\(timestamp)/mp4")
                }
            }
            
            for (key, value) in parameters {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
        }, to: url, method: .post ,headers: (header as! HTTPHeaders),
           
           encodingCompletion: { encodingResult in
            
            switch encodingResult {
            case .success(let upload, _, _):
                upload.responseObject { (response: DataResponse<T>) in
                   
                        //                                                          guard let strongSelf = self else { return }
                        switch response.result {
                        case .success(let dataX):
                            if showHUD == true {
                                  ProgressHud.hideProgressHUD()
                            }
                          
                            print(response.result.value)
                            print(response)
                            let data = response.result.value as? NSDictionary
                                print(data)
                            
//                            if showBanner {
//                                //          appDelegate?.window?.rootViewController?.showBanner(title: dataX.message ?? "", isError: response.response?.statusCode == 200 ? false:true)
//                            }
                            if response.response?.statusCode == 200 || response.response?.statusCode == 201 || response.response?.statusCode == 202 {
                               
                                completionSuccess(dataX)
                            } else {
                                
                              
                                ProgressHud.hideProgressHUD()
                                completionSuccess(dataX)
                            }
                        case .failure(let error):
                            ProgressHud.hideProgressHUD()
                            print(error)
                        }
                        print(response.result.value)
                       /// completionSuccess(response)                               //completionSuccess(result:JSON(data: data!))
                        
                        //}
                        //                          upload.responseJSON { [weak self]response in
                        //                              guard let strongSelf = self else { return }
                        //                                if (error != nil) {
                        //                                                           completionError(error: error!)
                        //                                                       }else{
                        //                                                           completionSuccess(result:JSON(data: data!))
                        //                                                       }
                        //                          }
                        
                        
                        
                    }
                
                
            case .failure(_):
                print("")
            }
        })
        
    }
}
class ImageUploaderFinal {
    
    static func uploadImage<T: DefaultResponse>(url: String,videoName: String,videos: [URL],imageName : String,
                                                parameters: [String: Any],
                                                imagesDataSource: [UIImage],showHUD: Bool,completionSuccess: @escaping (T) -> Void,
                                                completionError: @escaping (() -> Void)) {
        var header: [String: Any]
        if let token = UserDefaultsHandler.getUDValue(key: UDkey.token) as? String {
            header = ["Authorization": "Bearer \(token)", "Accept": "application/json"]
        } else {
            header = ["Accept": "application/json"]
            
        }
        // let imageData = image.jpegData(compressionQuality: 0.5)
         let timestamp = NSDate().timeIntervalSince1970 // just for some random name.
        Alamofire.upload(multipartFormData: { multipartFormData in
            // import image to request
            print(url,parameters,header)
            if showHUD == true {
                 ProgressHud.showProgressHUD()
            }
           
            if imagesDataSource != []{
                for imageData in imagesDataSource {
                    let data = imageData.jpegData(compressionQuality: 0.9)
                    multipartFormData.append(data!, withName: imageName, fileName: "\(Date().timeIntervalSince1970).jpeg", mimeType: "image/jpeg")
                    
                }
            }
            if videos != [] {
                for videos in videos {
                multipartFormData.append(videos, withName: videoName, fileName: "\(timestamp).mp4", mimeType: "\(timestamp)/mp4")
                }
            }
            
            for (key, value) in parameters {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
        }, to: url, method: .post ,headers: (header as! HTTPHeaders),
           
           encodingCompletion: { encodingResult in
            
            switch encodingResult {
           
            case .success(let upload, _, _):
                upload.responseObject { (response: DataResponse<T>) in
                   
                        //                                                          guard let strongSelf = self else { return }
                    print(response.response?.statusCode)
                        switch response.result {
                        case .success(let dataX):
                            if showHUD == true {
                                  ProgressHud.hideProgressHUD()
                            }
                          
                            print(response.result.value)
                            print(response)
                            let data = response.result.value as? NSDictionary
                                print(data)
                            
//                            if showBanner {
//                                //          appDelegate?.window?.rootViewController?.showBanner(title: dataX.message ?? "", isError: response.response?.statusCode == 200 ? false:true)
//                            }
                            if response.response?.statusCode == 200 || response.response?.statusCode == 201 || response.response?.statusCode == 202 || response.response?.statusCode == 500{
                               
                                completionSuccess(dataX)
                            } else {
                                
                              
                                ProgressHud.hideProgressHUD()
                                completionSuccess(dataX)
                            }
                        case .failure(let error):
                            ProgressHud.hideProgressHUD()
                            print(error)
                        }
                        print(response.result.value)
                       /// completionSuccess(response)                               //completionSuccess(result:JSON(data: data!))
                        
                        //}
                        //                          upload.responseJSON { [weak self]response in
                        //                              guard let strongSelf = self else { return }
                        //                                if (error != nil) {
                        //                                                           completionError(error: error!)
                        //                                                       }else{
                        //                                                           completionSuccess(result:JSON(data: data!))
                        //                                                       }
                        //                          }
                        
                        
                        
                    }
                
                
            case .failure(_):
                print("")
            }
        })
        
    }
}

