//
//  AppUrl.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 7/1/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import Foundation
class GraduateUrl: NSObject {
    static let baseURL = "http://test-graduation1.captainau.com.au/api/"
    static let googleApiKey = "AIzaSyBUfA6Bv9gvD_cSQdpNW7elCtgvx85X_co"
    static let imageUrl = "http://test-graduation1.captainau.com.au" + "/storage/"
 static let shorImageUrl = "http://test-graduation1.captainau.com.au"
    
    static var loginURL: String = {
        let url = baseURL + "login"
        return url
    }()
    static var registerURL: String = {
        let url = baseURL + "register"
        return url
    }()
    static var emailVerificationURL: String = {
        let url = baseURL + "email-verification/status"
        return url
    }()
    static var changePassowrdURL: String = {
        let url = baseURL + "password/change"
        return url
    }()
    static var sendingDeviceTokenURL: String = {
        let url = baseURL + "user/save-device-token"
        return url
    }()
    static var postListURL: String = {
        let url = baseURL + "post/list-for-timeline-non-random"
        return url
    }()
    static var fullUserDetalsURL: String = {
        let url = baseURL + "user/full-details"
        return url
    }()
    static var occupationListURL: String = {
        let url = baseURL + "user/friend/occupation-list"
        return url
    }()
    static var updateUserProfileURL: String = {
        let url = baseURL + "user/update/profile"
        return url
    }()
    static var deleteUserURL: String = {
        let url = baseURL + "user/"
        return url
    }()
    static var deleteDefaultMusicURL: String = {
        let url = baseURL + "user/delete/default-music"
        return url
    }()
    static var followersListURL: String = {
        let url = baseURL + "user/get/followers"
        return url
    }()
    
    ///mark:- "userId/remove-follow"
    static var removeFollowURL: String = {
        let url = baseURL + "user/"
        return url
    }()
    static var FollowURL: String = {
        let url = baseURL + "user/"
        return url
    }()
    static var messagesURL: String = {
        let url = baseURL + "user/chat/user-recent-conversations"
        return url
    }()
    static var friendsURL: String = {
        let url = baseURL + "user/friends"
        return url
    }()
    static var friendshipURL: String = {
        let url = baseURL + "user/friendships"
        return url
    }()
    static var pendingFriendsURL: String = {
        let url = baseURL + "get/pending-friends"
        return url
    }()
    static var blockedFriendsURL: String = {
        let url = baseURL + "get/blocked-friends"
        return url
    }()
    
    static var friendRequestURL: String = {
        let url = baseURL + "get/sent-friend-requests"
        return url
    }()
    
    static var declinedRequestURL: String = {
        let url = baseURL + "get/declined-friends"
        return url
    }()
    static var unfriendURL: String = {
        let url = baseURL + "user/friend/"
        return url
    }()
    static var userSettingsURL: String = {
        let url = baseURL + "user/settings"
        return url
    }()
    static var updateUserSettingsURL: String = {
        let url = baseURL + "user/settings/update"
        return url
    }()
    static var usernameCheckURL: String = {
        let url = baseURL + "check-username"
        return url
    }()
    static var updateUsernameURL: String = {
        let url = baseURL + "user/username/add-update"
        return url
    }()
    static var universityListURL: String = {
        let url = baseURL + "graduate_university/list"
        return url
    }()
    static var unfollorUniURL: String = {
        let url = baseURL + "graduate_university/unfollow/"
        return url
    }()
    
    static var followUniURL: String = {
        let url = baseURL + "graduate_university/follow/"
        return url
    }()
    static var userUniURL: String = {
        let url = baseURL + "graduate_university/user-businesses"
        return url
    }()
    
    static var createUniURL: String = {
        let url = baseURL + "graduate_university"
        return url
        
    }()
    
    static var universityDetalsURL: String = {
        let url = baseURL + "graduate_university/"
        return url
        
    }()
    static var uniPortFolioURL: String = {
        let url = baseURL + "post/list"
        return url
        
    }()
    static var addEventURL: String = {
        let url = baseURL + "graduate_spot"
        return url
        
    }()
    static var createPostURL: String = {
        let url = baseURL + "post"
        return url
    }()
    static var getStarURL: String = {
        let url = baseURL + "post/stars/"
        return url
    }()
    static var getCommentsURL: String = {
        let url = baseURL + "post/comments/"
        return url
    }()
    static var CommentsURL: String = {
        let url = baseURL + "post/comment/"
        return url
    }()
    static var attendEventURL: String = {
        let url = baseURL + "graduate_spot/attend/"
        return url
    }()
    static var unAttendEventURL: String = {
        let url = baseURL + "graduate_spot/unattend/"
        return url
    }()
    static var ratePostURL: String = {
        let url = baseURL + "post/star/"
        return url
    }()
    static var unratePostURL: String = {
        let url = baseURL + "post/remove-star/"
        return url
    }()
    static var reportPostURL: String = {
        let url = baseURL + "report/post/"
        return url
    }()
    static var offersURL: String = {
        let url = baseURL + "post/list"
        return url
    }()
    
    static var watchURL: String = {
        let url = baseURL + "post/list-for-timeline-non-random"
        return url
    }()
    static var userNotificationsURL: String = {
        let url = baseURL + "user/notification"
        return url
    }()
    static var deleteNotificationURL: String = {
           let url = baseURL + "user/notification/"
           return url
       }()
    static var navigatePostURL: String = {
              let url = baseURL + "post/"
              return url
          }()
    static var otherUserDetailsURL: String = {
                 let url = baseURL + "user/"
                 return url
             }()
    static var getFriendsByIdURL: String = {
                   let url = baseURL + "user/"
                   return url
               }()
    static var getBusinessRecentMessageIdURL: String = {
                      let url = baseURL + "user/chat/business-recent-conversations/"
                      return url
                  }()
    static var getConverationMessageURL: String = {
                         let url = baseURL + "user/chat/get-messages/"
                         return url
                     }()
    
    static var rateCommentURL: String = {
        let url = baseURL + "comment/star/"
        return url
    }()
    static var UnrateCommentURL: String = {
        let url = baseURL + "comment/remove-star/"
        return url
    }()
    static var deleteCommentURL: String = {
        let url = baseURL + "post/comment/"
        return url
    }()
    static var liveTempTokenURL: String = {
        let url = baseURL + "token/temporary"
        return url
    }()
    static var deleteLiveVideoURL: String = {
        let url = baseURL + "post/live/"
        return url
    }()
    static var searchURL: String = {
        let url = baseURL + "search"
        return url
    }()
    static var updatePortFolioURL: String = {
        let url = baseURL + "graduate_university/update/portfolio/"
        return url
    }()
    static var deleteOwnPostURL: String = {
        let url = baseURL + "post/"
        return url
    }()
    static var deletePortFolioURL: String = {
        let url = baseURL + "graduate_university/delete/portfolio/"
        return url
    }()
    static var deleteOfferURL: String = {
        let url = baseURL + "graduate_university/delete/offer/"
        return url
    }()
    static var deleteEventURL: String = {
        let url = baseURL + "graduate_spot/"
        return url
    }()
    static var forgotPasswordURL: String = {
        let url = baseURL + "password/email"
        return url
    }()
    
}





