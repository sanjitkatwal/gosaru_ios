//
//  DefaultResponse.swift
//  Pashupati
//
//  Created by Prashant Ghimire on 3/27/19.
//  Copyright © 2018 prashantghimire@gmail.com. All rights reserved.
//

import Foundation
import ObjectMapper
class DefaultResponse: NSObject, NSCoding, Mappable {
  
    var status: String?
    var message: String?
    var messageTitle: String?
//    var error: String?
    var email: String?
    var phone: String?
    var agreeTerms: String?
    var emailVerification: Int?
    var postAsErrors: PostAsError?
    required init?(coder aDecoder: NSCoder) {
    }
    override init() {
        super.init()
    }
    func encode(with aCoder: NSCoder) {
    }
    required init?(map: Map) {
    }
    func mapping(map: Map) {
       
        status <- map["status"]
        message <- map["message"]
        messageTitle <- map["title"]
        email <- map["email"]
        emailVerification <- map["email_verified"]
        phone <- map["phone"]
        agreeTerms <- map["agree_terms"]
        postAsErrors <- map["errors"]
        if messageTitle == nil {
               messageTitle <- map["error_description"]
        }
        if messageTitle == nil {
            messageTitle = message
        }
    }
}
class PostAsError: NSObject, NSCoding, Mappable {
    public var postAsErrors: [String]?
   
    required init?(coder aDecoder: NSCoder) {
    }
    override init() {
        super.init()
    }
    func encode(with aCoder: NSCoder) {
    }
    required init?(map: Map) {
    }
    func mapping(map: Map) {
        postAsErrors <- map ["post_as"]
    }
}

