//
//  PushNotificationManager.swift
//  GraduateApp
//
//  Created by M̵̢̩̲͂̅̍̍͐͒͂̀͠â̶͎̙̯͇͉͔̌̽̀̕u̴̦͓̫̝̘̟͈͛̽͛ͅs̸͎̭̣̳̬̘̫̎̌ȃ̶̗͉̪̑͋̋̈̇͜m̶̳̏̔ on 9/14/20.
//  Copyright © 2020 Mousham Pradhan. All rights reserved.
//

import Foundation
import Firebase



import UIKit

class PushNotificationManager: NSObject, UIApplicationDelegate {
    let userID: String
       init(userID: String) {
           self.userID = userID
           super.init()
       }

     func registerForPushNotification(application: UIApplication) {
          UIApplication.shared.registerForRemoteNotifications()
          Messaging.messaging().delegate = self
          if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
              options: authOptions,
              completionHandler: {_, _ in })
          } else {
            let settings: UIUserNotificationSettings =
              UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
          }
          application.registerForRemoteNotifications()
        application.delegate = self
       // updateFirestorePushTokenIfNeeded()

        }
    func updateFirestorePushTokenIfNeeded() {
           if let token = Messaging.messaging().fcmToken {
               let usersRef = Firestore.firestore().collection("users_table").document(userID)
               usersRef.setData(["fcmToken": token], merge: true)
           }
       }

   
}
extension PushNotificationManager: MessagingDelegate {
  func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
    print("Firebase registration token: \(fcmToken)")
    let dataDict: [String: String] = ["token": fcmToken]
    
    //MARK:- SAVING THE DEVICE TOKEN
    UserDefaultsHandler.setToUD(key: .deviceToken, value: dataDict["token"] ?? "")
    // Note: This callback is fired at each app startup and whenever a new token is generated.
    let dataDicts :[String: String] = ["token": fcmToken ]
      NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDicts)
  }
  // The callback to handle data message received via FCM for devices running iOS 10 or above.
  func application(received remoteMessage: MessagingRemoteMessage) {
    print(remoteMessage.appData)
  }
}
extension PushNotificationManager: UNUserNotificationCenterDelegate {
  // Receive displayed notifications for iOS 10 devices.
  func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
    let userInfo = notification.request.content.userInfo
    // With swizzling disabled you must let Messaging know about the message, for Analytics
    // Messaging.messaging().appDidReceiveMessage(userInfo)
    // Print message ID.
    // Print full message.
    print(userInfo)
//    setToDataBase(data: userInfo)
    return completionHandler([.alert, .badge, .sound])//completionHandler(UNNotificationPresentationOptions.alert)
  }
  func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
    let userInfo = response.notification.request.content.userInfo
    // Print message ID.
    //    handleNotificaiton(userInfo: userInfo)
    print(userInfo)
//    setToDataBase(data: userInfo)
    return completionHandler()
  }
}

